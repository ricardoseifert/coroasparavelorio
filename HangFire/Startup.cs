﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.Logging;
using Hangfire.Logging.LogProviders;
using Hangfire.SqlServer;
using HangFire.Jobs;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangFire
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var options = new DashboardOptions
            {
                AuthorizationFilters = new[]
                {
                    new LocalRequestsOnlyAuthorizationFilter()
                }
            };

            JobStorage.Current = new SqlServerStorage("HangFire_CPV");
            app.UseHangfireDashboard("", options);

            var CPVJobs = new CPV();
            var LCJobs = new LC();

            RecurringJob.AddOrUpdate(() => LCJobs.RoboCobrancaLC(), Cron.Daily(9, 45), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(() => CPVJobs.RoboCobrancaCPV(), Cron.Daily(9, 40), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(() => CPVJobs.RoboCobrancaCPV_SMS(), Cron.Daily(9, 35), TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => CPVJobs.AtualizaLogMandrill(), Cron.HourInterval(4));

            LogProvider.SetCurrentLogProvider(new ColouredConsoleLogProvider());

            app.UseHangfireServer();
        }
    }
}