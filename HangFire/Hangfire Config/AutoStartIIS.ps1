import-module Microsoft.PowerShell.Security 
import-module Microsoft.PowerShell.Management 
import-module webadministration

$WebSiteName = "Default Web Site" # your web site name
$WebSiteFullName = "IIS:\Sites\" + $WebSiteName
$ApplicationPool = Get-Item $WebSiteFullName | Select-Object applicationPool
$ApplicationPoolFullName = "IIS:\AppPools\" + $ApplicationPool.applicationPool

Set-WebConfiguration -filter '/system.applicationHost/serviceAutoStartProviders' -value (@{name="ApplicationPreload";type="WebApplication1.ApplicationPreload, WebApplication1"})
set-itemproperty $WebSiteFullName -name applicationDefaults.serviceAutoStartEnabled -value True
set-itemproperty $WebSiteFullName -name applicationDefaults.serviceAutoStartProvider -value 'ApplicationPreload'
set-itemproperty $ApplicationPoolFullName -name autoStart -value True
set-itemproperty $ApplicationPoolFullName -name startMode -value 1 #1 = AlwaysRunning, 0 = OnDemand