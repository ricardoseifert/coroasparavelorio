﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcExtensions.Infrastructure;
using Ninject;
using Domain.Core;
using System.Web.Mvc;

namespace Site_Lacos.Factories
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel = new StandardKernel(new NinjectModule());

        public NinjectControllerFactory()
        {
            //SITE
            kernel.Bind<Site_Lacos.Controllers.HomeController>().ToSelf();


            kernel.Bind<Site_Lacos.Areas.Empresas.Controllers.HomeController>().ToSelf();

        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, string.Format("The controller for path '{0}' could not be found or it does not implement IController.", requestContext.HttpContext.Request.Path));
            }

            return (IController)kernel.Get(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;

            if (disposable != null)
                disposable.Dispose();
        }

    }
}