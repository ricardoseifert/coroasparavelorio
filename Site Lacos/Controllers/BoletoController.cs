﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using Domain.Service;

namespace Site_Lacos.Controllers
{
    public class BoletoController : Controller
    { 
        private IPersistentRepository<Faturamento> faturamentoRepository;

        public BoletoController(ObjectContext context)
        { 
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
        }

        public ActionResult Index(string codigo)
        {
            var id = Convert.ToInt32(Domain.Core.Criptografia.Decrypt(codigo));
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.Boleto != null)
                {
                    var html = BoletoService.Gerar(fatura.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                    return Content("");
                }
                else
                    return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Empresa(string codigo)
        {
            var id = Convert.ToInt32(Domain.Core.Criptografia.Decrypt(codigo));
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.Boleto != null)
                {
                    var html = BoletoService.Gerar(fatura.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                    return Content("");
                }
                else
                    return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Index", "Home");
        }

    }
}
