﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Entities;
using MvcExtensions.Security.Models;
using System.Web.Security;
using Web.Models;


namespace Site_Lacos.Controllers
{
    public class LoginController : BaseController
    {
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private ColaboradorModel clienteModel;
        private IPersistentRepository<Parametro> parametroRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;

        public LoginController(ObjectContext context)
            : base(context)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            clienteModel = new ColaboradorModel(context);
            parametroRepository = new PersistentRepository<Parametro>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);

        }

        public ActionResult Index()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Token"]))
                {
                    string Token = Request.QueryString["Token"];

                    var Parametro = parametroRepository.GetByExpression(r => r.Chave == "LoginToken,LC" && r.Val1 == Token).First();
                    if (Parametro != null)
                    {
                        var Span = DateTime.Now - DateTime.Parse(Parametro.Val2);
                        if (Span.TotalMinutes <= 0)
                        {
                            int ColaboradorID = int.Parse(Parametro.Val3.Split('|')[0]);
                            Colaborador colaborador = colaboradorRepository.GetByExpression(r => r.ID == ColaboradorID).First();

                            if (colaborador != null)
                            {
                                if (Logar(null, colaborador)) {
                                    try
                                    {
                                        parametroRepository.Delete(Parametro);
                                    }
                                    catch { }

                                    #region REDIRECT AUTOMATICO PARA PRODUTO 

                                    try
                                    {
                                        var ProdId = int.Parse(Parametro.Val3.Split('|')[1].Split(',')[0].ToString());
                                        var Produtos = produtoTamanhoRepository.GetByExpression(r => r.ID == ProdId).ToList();

                                        if (Produtos.Count > 0)
                                        {
                                            var Produto = Produtos.First().Produto;

                                            return Redirect("/empresas/pedido/novo/" + Produto.ID + "/" + Produto.RotuloUrl);
                                        }
                                    }
                                    catch { }

                                    #endregion


                                    return Redirect("/empresas");
                                }
                                    
                            }
                        }
                    }
                }
            }
            catch {}

            return View();
        }

        public ActionResult Login()
        {
            return View(clienteModel);
        }

        [HttpPost]
        public JsonResult Enviar_Senha(string email)
        {
            var colaborador = colaboradorRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (colaborador != null)
            {
                var mensagem = "Olá " + colaborador.Nome + ",\n\nEstes são seus dados para acesso:";
                mensagem += "\nLogin: " + colaborador.Login;
                mensagem += "\nSenha: " + colaborador.Senha;

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "LAÇOS CORPORATIVOS", email, email, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "LAÇOS CORPORATIVOS", null, "[LAÇOS CORPORATIVOS] - Acesso ao laços corporativos", mensagem, false))
                {
                    return Json("Seus dados de acesso foram enviados para seu email.");
                }
                else
                {
                    return Json("Não foi possível enviar seus dados de acesso. Tente novamente mais tarde.");
                }
            }
            else
            {
                return Json("Nenhum acesso cadastrado com esse endereço de email.");
            } 
        }

        [HttpPost]
        public ActionResult Index(string login, string senha, int? empresa, string returnUrl)
        {
            if (empresa.HasValue)
            {
                var colaborador = colaboradorRepository.GetByExpression(c => ((c.Email == login && c.Senha == senha && c.EmpresaID == empresa) || (c.Login == login && c.Senha == senha && c.EmpresaID == empresa)) && !c.Removido && c.Empresa.Ativa).FirstOrDefault();
                if (colaborador != null)
                {
                    if(Logar(empresa, colaborador))
                        return Redirect(returnUrl);

                    ViewBag.Mensagem = "Erro no Login";
                    return View("Index");
                }
                else
                {
                    ViewBag.Mensagem = "Login e/ou Senha inválido(s). Digite seu e-mail, empresa e senha corretamente.";
                    return View("Index");
                }
            }
            else
            {
                ViewBag.Mensagem = "Selecione a empresa.";
                return View("Index");
            }
        }

        public bool Logar(int? empresa, Colaborador colaborador)
        {
            var dados = new Dictionary<string, string>();
            if (empresa.HasValue)
                dados.Add("empresaID", empresa.Value.ToString());
            else
                dados.Add("empresaID", colaborador.EmpresaID.ToString());
            var user = new LoggedUser();
            user.ID = colaborador.ID;
            user.Name = colaborador.Nome;
            user.Username = colaborador.Codigo.ToString();
            user.ExpiresIn = DateTime.Now.AddHours(2);
            user.AcessTime = DateTime.Now;
            user.AditionalInfo = dados;
            user.AuthenticatedAreaName = "Empresas";
            user.AccessGroups.Add("Empresas" + colaborador.Tipo.ToString());

            colaborador.DataUltimoLogin = DateTime.Now;
            colaboradorRepository.Save(colaborador);

            FormsAuthentication.SetAuthCookie(user.ToJSON(), true);

            return true;
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}
