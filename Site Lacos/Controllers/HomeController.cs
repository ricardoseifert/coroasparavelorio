﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service; 

namespace Site_Lacos.Controllers
{
    public class HomeController : BaseController
    {
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        public HomeController(ObjectContext context)
            : base(context)
        {
            estadoRepository = new PersistentRepository<Estado>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
        }

        public void Index()
        {
            Response.Redirect("/Login");
        }

        [HttpPost]
        public JsonResult GetCidades(int estadoid, string selected = "")
        {
            var cidades = estadoRepository.GetByExpression(c => c.ID == estadoid).FirstOrDefault().Cidades.OrderBy(c => c.Nome).ToList();

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Cidade cidade in cidades)
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult GetCidadesUF(string uf, string selected = "")
        {
            var cidades = estadoRepository.GetByExpression(c => c.Sigla.ToUpper().Trim() == uf.ToUpper().Trim()).FirstOrDefault().Cidades.OrderBy(c => c.Nome).ToList();

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Cidade cidade in cidades)
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result);
        }
        
        [HttpPost]
        public JsonResult GetCEP(string cep)
        {
            var resultado = new CEP.CEP(cep);
            return Json(new { resultado = resultado.Resultado, resultado_txt = resultado.ResultadoTXT, uf = resultado.UF, cidade = resultado.Cidade, bairro = resultado.Bairro, tipo_logradouro = resultado.TipoLogradouro, logradouro = resultado.Logradouro });
        }

        [HttpPost]
        public JsonResult GetEmpresa(string email)
        {
            var empresas = empresaRepository.GetByExpression(c => c.Colaboradors.Any(e => e.Login == email && !e.Removido)).ToList();

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var empresa in empresas)
            {
                result.Add(new SelectListItem { Text = empresa.RazaoSocial.ToUpper(), Value = empresa.ID.ToString()});
            }
            return Json(result);
        }
    }
}
