﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Models;
using System.Web.Security;
using Web.Models;


namespace Site_Lacos.Controllers
{
    public class Cadastre_seController : BaseController
    {
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private ColaboradorModel clienteModel;

        public Cadastre_seController(ObjectContext context)
            : base(context)
        {
            estadoRepository = new PersistentRepository<Estado>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            clienteModel = new ColaboradorModel(context);
        }

        public ActionResult Index()
        {
            if (clienteModel.Autenticado)
            {
                var returlUrl = String.IsNullOrEmpty(Request.QueryString["returnUrl"]) ? "/empresas" : Request.QueryString["returnUrl"];
                return Redirect(returlUrl);
            }
            else
            {
                ViewBag.Estados = estadoRepository.GetAll();
                return View();
            }
        }

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var razaosocial = form["razaosocial"].ToUpper();
                var nomefantasia = form["nomefantasia"].ToUpper();
                var cnpj = form["cnpj"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                var inscricaoestadual = form["inscricaoestadual"].Trim();
                var inscricaomunicipal = form["inscricaomunicipal"].Trim();
                var cep = form["cep"].Trim();
                var estado = Convert.ToInt32(form["estado"].Trim());
                var cidade = Convert.ToInt32(form["cidade"].Trim());
                var bairro = form["bairro"].ToUpper();
                var numero = form["numero"].ToUpper();
                var logradouro = form["logradouro"].ToUpper();
                var complemento = form["complemento"].ToUpper();
                var referencia = form["referencia"].ToUpper();
                var nome = form["nome"].ToUpper();
                var email = form["email"].Trim().ToLower();
                var telefone = form["telefone"];
                var telefonecelular = form["telefonecelular"];
                var senha = form["senha"].Trim();

                var empresa = empresaRepository.GetByExpression(c => c.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == cnpj).FirstOrDefault();
                var colaborador = colaboradorRepository.GetByExpression(c => c.Email.Trim() == email).FirstOrDefault();
                if (empresa == null && colaborador == null)
                {

                    empresa = new Empresa();
                    empresa.CidadeID = cidade;
                    empresa.EstadoID = estado;
                    empresa.Codigo = Guid.NewGuid();
                    empresa.Logo = "";
                    empresa.NomeFantasia = nomefantasia;
                    empresa.RazaoSocial = razaosocial;
                    empresa.EmailContato = email;
                    empresa.PessoaContato = nome;
                    empresa.TelefoneContato = telefone;
                    empresa.CEP = cep;
                    empresa.Logradouro = logradouro;
                    empresa.Numero = numero;
                    empresa.Complemento = complemento;
                    empresa.Bairro = bairro;
                    empresa.CNPJ = cnpj;
                    empresa.InscricaoEstadual = inscricaoestadual;
                    empresa.InscricaoMunicipal = inscricaomunicipal;
                    empresa.Referencia = referencia;
                    empresa.Observacao = "";
                    empresa.Ativa = true;
                    empresa.FraseFixa = false;
                    empresa.DataCadastro = DateTime.Now;
                    empresaRepository.Save(empresa);

                    colaborador = new Colaborador();
                    colaborador.EmpresaID = empresa.ID;
                    colaborador.Tipo = Colaborador.Tipos.Master;
                    colaborador.Codigo = Guid.NewGuid();
                    colaborador.Nome = nome;
                    colaborador.Email = email;
                    colaborador.Login = email;
                    colaborador.Senha = senha;
                    colaborador.Telefone = telefone;
                    colaborador.TelefoneCelular = telefonecelular;
                    colaborador.Departamento = "";
                    colaborador.DataCadastro = DateTime.Now;
                    colaborador.Removido = false;
                    colaborador.RecebeEmailPedido = true;
                    colaborador.RecebeEmailNFe = true;
                    colaboradorRepository.Save(colaborador);

                    var user = new LoggedUser();
                    user.ID = colaborador.ID;
                    user.Name = colaborador.Nome;
                    user.Username = colaborador.Codigo.ToString();
                    user.ExpiresIn = DateTime.Now.AddHours(2);
                    user.AcessTime = DateTime.Now;
                    user.AditionalInfo = new Dictionary<string, string>();
                    user.AuthenticatedAreaName = "Empresas";
                    user.AccessGroups.Add("Empresas" + colaborador.Tipo.ToString());

                    FormsAuthentication.SetAuthCookie(user.ToJSON(), false);

                    return Json(new { status = "OK" }); 
                }
                else
                {
                    if(empresa != null)
                        return Json(new { status = "ERRO", mensagem = "O CNPJ digitado já está cadastrado em nosso sistema." });
                    
                    if(colaborador != null)
                        return Json(new { status = "ERRO", mensagem = "O e-mail digitado já está cadastrado em nosso sistema." });

                    return Json(new { status = "ERRO", mensagem = "Erro desconhecido" });
                }
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }


        [HttpPost]
        public ActionResult EnviarNew(FormCollection form)
        {
            var razaosocial = form["razaosocial"].ToUpper();
            var nomefantasia = form["nomefantasia"].ToUpper();
            var cnpj = form["cnpj"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            var inscricaoestadual = form["inscricaoestadual"].Trim();
            var inscricaomunicipal = form["inscricaomunicipal"].Trim();
            var cep = form["cep"].Trim();
            var estado = Convert.ToInt32(form["estado"].Trim());
            var cidade = Convert.ToInt32(form["cidade"].Trim());
            var bairro = form["bairro"].ToUpper();
            var numero = form["numero"].ToUpper();
            var logradouro = form["logradouro"].ToUpper();
            var complemento = form["complemento"].ToUpper();
            var referencia = string.Empty;
            var nome = form["nome"].ToUpper();
            var email = form["email"].Trim().ToLower();
            var telefone = form["telefone"];
            var telefonecelular = form["telefonecelular"];
            var senha = form["senha"].Trim();

                var empresa = empresaRepository.GetByExpression(c => c.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == cnpj).FirstOrDefault();
                var colaborador = colaboradorRepository.GetByExpression(c => c.Email.Trim() == email).FirstOrDefault();
                if (empresa == null && colaborador == null)
                {

                    empresa = new Empresa();
                    empresa.CidadeID = cidade;
                    empresa.EstadoID = estado;
                    empresa.Codigo = Guid.NewGuid();
                    empresa.Logo = "";
                    empresa.NomeFantasia = nomefantasia;
                    empresa.RazaoSocial = razaosocial;
                    empresa.EmailContato = email;
                    empresa.PessoaContato = nome;
                    empresa.TelefoneContato = telefone;
                    empresa.CEP = cep;
                    empresa.Logradouro = logradouro;
                    empresa.Numero = numero;
                    empresa.Complemento = complemento;
                    empresa.Bairro = bairro;
                    empresa.CNPJ = cnpj;
                    empresa.InscricaoEstadual = inscricaoestadual;
                    empresa.InscricaoMunicipal = inscricaomunicipal;
                    empresa.Referencia = referencia;
                    empresa.Observacao = "";
                    empresa.Ativa = true;
                    empresa.FraseFixa = false;
                    empresa.DataCadastro = DateTime.Now;
                    empresaRepository.Save(empresa);

                    colaborador = new Colaborador();
                    colaborador.EmpresaID = empresa.ID;
                    colaborador.Tipo = Colaborador.Tipos.Master;
                    colaborador.Codigo = Guid.NewGuid();
                    colaborador.Nome = nome;
                    colaborador.Email = email;
                    colaborador.Login = email;
                    colaborador.SenhasAnteriores = senha;
                    colaborador.Senha = senha;
                    colaborador.Telefone = telefone;
                    colaborador.TelefoneCelular = telefonecelular;
                    colaborador.Departamento = "";
                    colaborador.DataCadastro = DateTime.Now;
                    colaborador.Removido = false;
                    colaborador.RecebeEmailPedido = true;
                    colaborador.RecebeEmailNFe = true;
                    colaboradorRepository.Save(colaborador);

                    colaborador.EnviarEmailCadastro();

                    var user = new LoggedUser();
                    user.ID = colaborador.ID;
                    user.Name = colaborador.Nome;
                    user.Username = colaborador.Codigo.ToString();
                    user.ExpiresIn = DateTime.Now.AddHours(2);
                    user.AcessTime = DateTime.Now;
                    user.AditionalInfo = new Dictionary<string, string>();
                    user.AuthenticatedAreaName = "Empresas";
                    user.AccessGroups.Add("Empresas" + colaborador.Tipo.ToString());

                    FormsAuthentication.SetAuthCookie(user.ToJSON(), false);

                    return Json(new { status = "OK" });
                }
                else
                {
                    if (empresa != null)
                        return Json(new { status = "error", mensagem = "O CNPJ digitado já está cadastrado em nosso sistema." });

                    if (colaborador != null)
                        return Json(new { status = "error", mensagem = "O e-mail digitado já está cadastrado em nosso sistema." });

                    return Json(new { status = "error", mensagem = "Erro desconhecido" });
                }
        }

        [HttpPost]
        public ActionResult Entrar(string login, string senha)
        {
            var colaborador = colaboradorRepository.GetByExpression(c => c.Login == login && c.Senha == senha && !c.Removido && c.Empresa.Ativa).FirstOrDefault();
            if (colaborador != null)
            {
                var user = new LoggedUser();
                user.ID = colaborador.ID;
                user.Name = colaborador.Nome;
                user.Username = colaborador.Codigo.ToString();
                user.ExpiresIn = DateTime.Now.AddHours(2);
                user.AcessTime = DateTime.Now;
                user.AditionalInfo = new Dictionary<string, string>();
                user.AuthenticatedAreaName = "Empresas";
                user.AccessGroups.Add("Empresas" + colaborador.Tipo.ToString());

                colaborador.DataUltimoLogin = DateTime.Now;
                colaboradorRepository.Save(colaborador);

                FormsAuthentication.SetAuthCookie(user.ToJSON(), true);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Mensagem = "Login e/ou Senha inválido(s)";
                return View("Index");
            }
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

    }
}
