﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Site_Lacos.Controllers
{
    public class DetalhesController : Controller
    {
        //
        // GET: /Detalhes/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Coroas()
        {
            return View();
        }

        public ActionResult Arranjos()
        {
            return View();
        }

        public ActionResult Aniversario()
        {
            return View();
        }

        public ActionResult Escritorio()
        {
            return View();
        }

        public ActionResult Demandas()
        {
            return View();
        }

        public ActionResult Eventos()
        {
            return View();
        }

        public ActionResult Contato_Obrigado()
        {
            return View();
        }

    }
}
