﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Site_Lacos.Controllers
{ 
    public class BaseController : Controller
    {

        private IPersistentRepository<Configuracao> configuracaoRepository;

        public BaseController(ObjectContext context)
        {
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
            ViewBag.Configuracoes = configuracao;
        }

    }
}
