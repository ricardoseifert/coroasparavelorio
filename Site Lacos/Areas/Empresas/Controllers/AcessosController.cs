﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Web.Models;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Security.Filters;

namespace Site_Lacos.Areas.Empresas.Controllers
{
    public class AcessosController : BaseController
    {

        private IPersistentRepository<Colaborador> colaboradorRepository;
        private ColaboradorModel clienteModel;
        public AcessosController(ObjectContext context)
            : base(context)
        {
            clienteModel = new ColaboradorModel(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
        }

        [AllowGroup("EmpresasMaster", "/empresas?acesso=false")]
        public ActionResult Index()
        {

            var colaborador = clienteModel.CurrentColaborador;
            var colaboradores = colaboradorRepository.GetByExpression(c => c.EmpresaID == colaborador.EmpresaID && !c.Removido).OrderBy(c => c.Nome).ToList();

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View(colaboradores);
        }

        public ActionResult Meus_Dados()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View(colaborador);
        }

        public ActionResult Alterar_Senha()
        {
            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View();
        }

        public ContentResult Salvar_Senha(string senhaAtual, string senhaNova)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            if (colaborador.Senha == senhaAtual)
            {
                colaborador.Senha = senhaNova;
                colaboradorRepository.Save(colaborador);
                return Content("OK");
            }
            return Content("A senha atual não confere.");
        }

        public ContentResult Enviar_Modificacao(string mensagem)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var body = "O colaborador " + colaborador.Nome + " da empresa " + colaborador.Empresa.NomeFantasia + " solicitou a modificação de dados cadastrais.\n\n" + mensagem;
            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "LAÇOS CORPORATIVOS", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", colaborador.Email, colaborador.Nome, null, "[LAÇOS CORPORATIVOS] - Solicitação de Modificação de Dados", body, false);

            return Content("OK");
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ContentResult Excluir(int id)
        {
            var colaborador = clienteModel.CurrentColaborador;
            var excluir = colaboradorRepository.Get(id);
            if (excluir == null)
            {
                return Content("Acesso inválido");
            }
            else if (excluir.EmpresaID != colaborador.EmpresaID)
            {
                return Content("Você pode excluir apenas acesso da sua empresa");
            }
            else if (excluir.ID == colaborador.ID)
            {
                return Content("Você não pode excluir você mesmo");
            }
            else if (excluir.Tipo == Colaborador.Tipos.Master && excluir.Empresa.Colaboradors.Count(c => c.TipoID == (int)Colaborador.Tipos.Master) <= 1)
            {
                return Content("É necessário ao menos um acesso com o perfil Master");
            }
            excluir.Removido = true;
            colaboradorRepository.Save(excluir);
            return Content("OK");
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ActionResult Modificar(int id)
        {
            var colaborador = clienteModel.CurrentColaborador;
            var modificar = colaboradorRepository.Get(id);
            if (modificar == null)
            {
                return RedirectToAction("Index");
            }
            else if (modificar.EmpresaID != colaborador.EmpresaID)
            {
                return RedirectToAction("Index");
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View("Editar", modificar);
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ActionResult Novo()
        {
            ViewBag.Link = "empresas/acessos";

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View("Editar", null);
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        [HttpPost]
        public JsonResult Salvar(FormCollection form)
        {
            var colaborador = clienteModel.CurrentColaborador;

            var codigo = Guid.Empty;
            var id = 0;
            Int32.TryParse(form["ID"], out id);

            Colaborador salvar = colaboradorRepository.GetByExpression(c => c.ID == id && c.Removido == false).FirstOrDefault(); 

            var email = form["acesso-email"].Trim().ToLower();
            var valido = colaboradorRepository.GetByExpression(c => c.Email.Trim().ToLower() == email && c.Removido == false).Count() == 0;

            if (salvar == null)
            {
                salvar = new Colaborador()
                {
                    Codigo = Guid.NewGuid(),
                    DataCadastro = DateTime.Now,
                    EmpresaID = colaborador.EmpresaID,
                    SenhasAnteriores = ""
                };
            }
            else
            {
                valido = colaboradorRepository.GetByExpression(c => c.Email.Trim().ToLower() == email && c.ID != salvar.ID && c.Removido == false).Count() == 0;
            }
            salvar.Departamento = form["acesso-departamento"].ToUpper();
            salvar.Email = form["acesso-email"].Trim().ToLower();
            salvar.Login = salvar.Email;
            salvar.Nome = form["acesso-nome"];
            salvar.Senha = form["acesso-senha"];
            salvar.Telefone = form["acesso-telefone"];
            salvar.TipoID = int.Parse(form["acesso-tipo"]);

            salvar.RecebeEmailPedido = (form["email-pedido"] == "on");
            salvar.RecebeEmailNFe = (form["email-nf"] == "on");

            if (valido)
            {
                salvar.Removido = false;
                colaboradorRepository.Save(salvar);

                return Json(new { status = "OK"});
            }
            else
            {
                return Json(new { status = "ERRO", mensagem = "Este e-mail já está cadastrado para outro colaborador." });
            }
        }
    }
}
