﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Models;
using MvcExtensions.Security.Filters; 

namespace Site_Lacos.Areas.Empresas.Controllers
{ 
    public class FaturasController : BaseController
    {

        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private ColaboradorModel clienteModel;
        public FaturasController(ObjectContext context)
            : base(context)
        {
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            clienteModel = new ColaboradorModel(context);
        }

        public ActionResult Index()
        {
            var colaborador = clienteModel.CurrentColaborador;
            var faturas = faturamentoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID).OrderByDescending(p => p.DataVencimento).ToList();
            if (colaborador.Tipo != Colaborador.Tipos.Master)
            {
                faturas = faturas.Where(p => p.PedidoEmpresas.Count(c => c.ColaboradorID == colaborador.ID) > 0).ToList();
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }


            return View(faturas);
        }

        public ActionResult Detalhes(int id)
        {
            var colaborador = clienteModel.CurrentColaborador;
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if ((fatura.EmpresaID == colaborador.EmpresaID && colaborador.Tipo == Colaborador.Tipos.Master) || fatura.PedidoEmpresas.Count(c => c.ColaboradorID == colaborador.ID) > 0)
                {
                    return View(fatura);
                }
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }


            return View();
        }

        public ContentResult Boleto(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var html = BoletoService.Gerar(fatura.Boleto);
                Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                return Content("");
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
        
        public ActionResult CartaoCredito(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                return View(fatura);
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }


            return RedirectToAction("Faturas");
        }

        public JsonResult PagarCartaoCredito(int bandeira, string nome, string numero, string validade, string codigo, int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                try
                {
                    var data = validade.Split('/');
                    var mes = 0;
                    var ano = 0;
                    if (data.Length < 2)
                    {
                        return Json("Validade do cartão inválida");
                    }
                    else if (!int.TryParse(data[0], out mes) || !int.TryParse(data[1], out ano))
                    {
                        return Json("Validade do cartão inválida");
                    }
                    numero = numero.Replace(".", "").Trim();
                    var boleto = fatura.Boleto;

                    string Ambiente = string.Empty;
                    RetornoRedeCard retornoRede = new RetornoRedeCard();
                    if (bool.Parse(Domain.Core.Configuracoes.HOMOLOGACAO.ToString()))
                    {
                        Ambiente = "Homolog";
                        retornoRede = RedeCardService.PagarHomolog(fatura.ID, fatura.ValorTotal, bandeira, nome, numero, mes, ano, codigo, 1);
                    }
                    else
                    {
                        Ambiente = "Producao";
                        retornoRede = RedeCardService.PagarProducao(fatura.ID, fatura.ValorTotal, bandeira, nome, numero, mes, ano, codigo, 1);
                    }

                    switch (retornoRede.Codret)
                    {
                        case 0:
                            fatura.DataProcessamento = DateTime.Now;
                            fatura.DataPagamento = DateTime.Now;
                            fatura.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                            fatura.Status = Faturamento.TodosStatus.Paga;
                            fatura.FormaPagamento = Pedido.FormasPagamento.CartaoCredito;
                            fatura.MeioPagamento = Pedido.MeiosPagamento.RedeCard;
                            fatura.ValorPago = fatura.ValorTotal;
                            fatura.TID = retornoRede.TID;
                            fatura.Retorno = retornoRede.Codret.ToString();
                            fatura.Mensagem = retornoRede.MsgRet;
                            fatura.Parcela = 1;
                            faturamentoRepository.Save(fatura);

                            if (boleto != null)
                            {
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.Observacao += "\nBoleto substituído por pagamento via cartão de crédito.";
                                if (boleto.RemessaBoletoes.Count == 0)
                                {
                                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                                    boleto.Processado = true;
                                }
                                else
                                {
                                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                    boleto.Processado = false;
                                }
                                boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                                boletoRepository.Save(boleto);
                            }
                            return Json(new { Retorno = 0, Mensagem = retornoRede.MsgRet });

                        case 80:
                        case 58:
                        case 84:
                            return Json(new { Retorno = retornoRede.Codret, Mensagem = retornoRede.MsgRet });
                        case 99:
                        default:
                            return Json(new { Retorno = retornoRede.Codret, Mensagem = retornoRede.MsgRet });
                    } 
                }
                catch (Exception e)
                {
                    return Json(new { Retorno = e.Message, Mensagem = "Não foi possível efetuar o pagamento com o cartão. Entre em contato com nossa central ou efetue o pagamento do boleto bancário." });
                }
            }
            else
                return Json(new { Retorno = "Erro", Mensagem = "Não foi possível efetuar o pagamento com o cartão. Entre em contato com nossa central ou efetue o pagamento do boleto bancário." });
        }

    }
}
