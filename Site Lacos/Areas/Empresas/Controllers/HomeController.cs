﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Web.Models;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service; 
using MvcExtensions.Security.Filters; 

namespace Site_Lacos.Areas.Empresas.Controllers
{
    public class HomeController : BaseController
    {

        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private ColaboradorModel clienteModel;
        public HomeController(ObjectContext context)
            : base(context)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            clienteModel = new ColaboradorModel(context);
        }

        public ActionResult Index()
        {
            ViewBag.Departamentos = colaboradorRepository.GetByExpression(c => c.EmpresaID == clienteModel.EmpresaID && !String.IsNullOrEmpty(c.Departamento)).OrderBy(c => c.Departamento).Select(c => c.Departamento).Distinct().ToList();
            var pedidos = clienteModel.CurrentColaborador.Empresa.PedidoEmpresas.ToList();

            var labelPeriodo = "Pedidos realizados";             

            if (!String.IsNullOrEmpty(Request["de"]) || !String.IsNullOrEmpty(Request["ate"]))
            {
                labelPeriodo = "Pedidos";
            }

            if (!String.IsNullOrEmpty(Request["de"]))
            {
                var de = Request["de"];
                DateTime _de;
                if (DateTime.TryParse(de, out _de))
                {
                    _de = new DateTime(_de.Year, _de.Month, _de.Day, 0, 0, 0);
                    pedidos = pedidos.Where(p => p.DataCriacao >= _de).ToList();
                    labelPeriodo += " de <b>" + Request["de"] + "</b>";
                }
            }


            if (!String.IsNullOrEmpty(Request["ate"]))
            {
                var ate = Request["ate"];
                DateTime _ate;
                if (DateTime.TryParse(ate, out _ate))
                {
                    _ate = new DateTime(_ate.Year, _ate.Month, _ate.Day, 23, 59, 59, 999);
                    pedidos = pedidos.Where(p => p.DataCriacao <= _ate).ToList();
                    labelPeriodo += " até <b>" + Request["ate"] + "</b>";
                }
            }
            else
            {
                labelPeriodo += " até o momento";
            }

            if (!String.IsNullOrEmpty(Request["departamento"]))
            {
                pedidos = pedidos.Where(p => p.Colaborador.Departamento == Request["departamento"]).ToList(); 
            }
            ViewBag.LabelPeriodo = labelPeriodo;

            ViewBag.TotalCoroas = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.CoroaFlor);
            ViewBag.TotalArranjo = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.ArranjoFlor);
            ViewBag.TotalBuques = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.OrquideaBuque);
            ViewBag.TotalAniversario = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Aniversario);
            ViewBag.TotalDatas = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Datas);
            ViewBag.TotalEventos = pedidos.Count(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Eventos);

            ViewBag.ValorTotal = pedidos.Sum(c => c.Valor);
            ViewBag.ValorTotalCoroas = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.CoroaFlor).Sum(c => c.Valor);
            ViewBag.ValorTotalArranjo = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.ArranjoFlor).Sum(c => c.Valor);
            ViewBag.ValorTotalBuques = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.OrquideaBuque).Sum(c => c.Valor);
            ViewBag.ValorTotalAniversario = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Aniversario).Sum(c => c.Valor);
            ViewBag.ValorTotalDatas = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Datas).Sum(c => c.Valor);
            ViewBag.ValorTotalEventos = pedidos.Where(c => c.ProdutoTamanho.Produto.TipoID == (int)Produto.Tipos.Eventos).Sum(c => c.Valor);
            ViewBag.NomeColaborador = clienteModel.CurrentColaborador.PrimeiroNome;

            if(!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View(pedidos.ToList());
        }
         
        public ActionResult Saudacao()
        {
            return View(clienteModel.CurrentColaborador);
        }

        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetEndereco(string cep)
        {
            if (cep.Length < 8) {
                return Json(new
                {
                    Erro = "CEP Invalido"
                }, JsonRequestBehavior.AllowGet);
            }

            var Result = new Domain.Core.WebCEP(cep);

            if (Result != null && Result.Resultado != "0")
                return Json(Result, JsonRequestBehavior.AllowGet);

            return Json(new
            {
                Erro = "CEP não encontrado"
            }, JsonRequestBehavior.AllowGet);
        }

        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetCotacaoEntrega(string cep)
        {
            var Mandae = new Domain.Entities.Integracoes.MandaeFrete();
            Mandae.declaredValue = "100";
            Mandae.height = "10";
            Mandae.width = "10";
            Mandae.length = "10";
            Mandae.weight = "1";

            var Result = Domain.Core.Mandae.CotacaoEntrega(cep, Mandae);

            if(Result != null)
                return Json(Result, JsonRequestBehavior.AllowGet);

            return Json(new
            {
                Erro = "Houte um erro"
            }, JsonRequestBehavior.AllowGet);
        }

        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetStatusEntrega(string NrRastreamento)
        {
            var Result = Domain.Core.Mandae.RastrearEntrega(NrRastreamento);

            if (Result != null)
                return Json(Result, JsonRequestBehavior.AllowGet);

            return Json(new
            {
                Erro = "Houte um erro"
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
