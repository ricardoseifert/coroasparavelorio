﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Web.Models; 

namespace Site_Lacos.Areas.Empresas.Controllers
{
    public class SuporteController : BaseController
    {

        private ColaboradorModel clienteModel;
        public SuporteController(ObjectContext context)
            : base(context)
        {
            clienteModel = new ColaboradorModel(context);
        }

        public ActionResult Index()
        { 
            return View();
        }

        public ContentResult Enviar(string assunto, string telefone, string mensagem)
        {
            var colaborador = (Domain.Entities.Colaborador)ViewBag.Colaborador;

            var body = "Empresa: " + colaborador.Empresa.NomeFantasia;
            body += "\nColaborador: " + colaborador.Nome;
            body += "\nEmail: " + colaborador.Email;
            body += "\nAssunto: " + assunto;
            body += "\nTelefone: " + telefone;
            body += "\nMensagem: " + mensagem;

            if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Corporativos", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", colaborador.Email, colaborador.Nome, null, "[LAÇOS CORPORATIVOS] - Acesso a Empresas", body, false))
            {
                return Content("OK");
            }
            else
            {
                return Content("ERRO");
            }
        }

    }
}
