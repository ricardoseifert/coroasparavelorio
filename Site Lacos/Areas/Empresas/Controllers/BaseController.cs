﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Filters;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Models;
using System.Web.Security;

namespace Site_Lacos.Areas.Empresas.Controllers
{

    //[RequireHttps]
    [AreaAuthorization("Empresas", "/login")]
    public class BaseController : Controller
    { 

        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;

        public BaseController(ObjectContext context)
        {
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
            ViewBag.Configuracoes = configuracao;

            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            var colaborador = new Colaborador();

            var loggedUser = LoggedUser.GetFromJSON(System.Web.HttpContext.Current.User.Identity.Name);
            if (loggedUser != null)
            {
                colaborador = colaboradorRepository.GetByExpression(c => c.ID == loggedUser.ID).FirstOrDefault();
                if (colaborador == null)
                {
                    colaborador = new Colaborador();
                }
            }

            ViewBag.Colaborador = colaborador;
        }

    }
}
