﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Models;

namespace Site_Lacos.Areas.Empresas.Controllers
{
    public class PedidosController : BaseController
    {

        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private ColaboradorModel clienteModel;
        public PedidosController(ObjectContext context)
            : base(context)
        {
            clienteModel = new ColaboradorModel(context);
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }

        public ActionResult Index()
        {
            var colaborador = clienteModel.CurrentColaborador;
            var pedidos = pedidoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID).OrderByDescending(p => p.DataCriacao).ToList();
            if (colaborador.Tipo != Colaborador.Tipos.Master)
            {
                pedidos = pedidos.Where(p => p.ColaboradorID == colaborador.ID).ToList();
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View(pedidos);
        }

        public ActionResult Detalhes(Guid codigo)
        {
            var colaborador = clienteModel.CurrentColaborador;
            var pedido = pedidoRepository.GetByExpression(p => p.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
            if (pedido != null)
            {
                if (pedido.EmpresaID == colaborador.EmpresaID && (colaborador.Tipo == Colaborador.Tipos.Master || pedido.ColaboradorID == colaborador.ID))
                {
                    return View(pedido);
                }
            }

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            return View();
        }

        public ContentResult Solicitar(Guid codigo)
        {
            var colaborador = clienteModel.CurrentColaborador;
            var pedido = pedidoRepository.GetByExpression(p => p.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
            if (pedido != null)
            {
                if (pedido.EmpresaID == colaborador.EmpresaID && (colaborador.Tipo == Colaborador.Tipos.Master || pedido.ColaboradorID == colaborador.ID))
                {
                    var body = "O colaborador " + colaborador.Nome + " da empresa " + colaborador.Empresa.NomeFantasia + " solicitou o faturamento do pedido ID #" + pedido.ID + " criado em " + pedido.DataCriacao.ToString();

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "LAÇOS CORPORATIVOS", "cobranca@coroasparavelorio.com.br", "COBRANÇA", colaborador.Email, colaborador.Nome, null, "[LAÇOS CORPORATIVOS] - Solicitação de Faturamento", body, false);

                    return Content("OK");
                }
            }
            return Content("ERRO");
        }

        public void Exportar()
        {
            var colaborador = clienteModel.CurrentColaborador;
            var pedidos = pedidoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID).OrderByDescending(p => p.DataCriacao).ToList();

            var de = "";
            var ate = "";
            var status = 0;
            if (!String.IsNullOrEmpty(Request["de"]))
            {
                de = Request["de"];
                DateTime _de;
                if (DateTime.TryParse(de, out _de))
                {
                    _de = new DateTime(_de.Year, _de.Month, _de.Day, 0, 0, 0);
                    pedidos = pedidos.Where(p => p.DataCriacao >= _de).ToList();
                }
            }


            if (!String.IsNullOrEmpty(Request["ate"]))
            {
                ate = Request["ate"];
                DateTime _ate;
                if (DateTime.TryParse(ate, out _ate))
                {
                    _ate = new DateTime(_ate.Year, _ate.Month, _ate.Day, 23, 59, 59, 999);
                    pedidos = pedidos.Where(p => p.DataCriacao <= _ate).ToList();
                }
            }

            if (!String.IsNullOrEmpty(Request["status"]))
            {
                if (int.TryParse(Request["status"], out status))
                {
                    pedidos = pedidos.Where(p => p.StatusID == status).ToList();
                }
            }

            var _result = "<table style='border: 1px solid #999;'>";
            _result += "<tr style='height:43px;font-weight:bold;'><th>Produto</th><th>Data Criação</th><th>Faturado</th><th>Criado por</th><th>Status</th><th>Valor</th><th>Data/Hora Solicitada</th><th>Nome do Homenageado</th><th>Frase</th><th>Nome do Funcionário</th><th>Parentesco</th><th>Departamento</th><th>Local de Entrega</th><th>Estado</th><th>Cidade</th><th>Observação</th><th>Telefone Contato</th></tr>";
            foreach (var item in pedidos)
            {
                _result += "<tr><td>" + item.ProdutoTamanho.Produto.Tipo + " " + item.ProdutoTamanho.Produto.Nome + "(" + item.ProdutoTamanho.Tamanho.Nome + ")" + "</td><td>" + item.DataCriacao.ToString() + "</td><td>" + (item.Faturamento == null ? "Não" : "Sim") + "</td><td>" + item.Colaborador.Nome + "</td><td>" + item.Status + "</td><td>" + item.Valor.ToString("c2") + "</td><td>" + item.DataSolicitada.ToString("dd/MM/yyyy HH'h'mm") + "</td><td>" + item.PessoaHomenageada + "</td><td>" + item.Mensagem + "</td><td>" + item.NomeFuncionario + "</td><td>" + item.Parentesco + "</td><td>" + item.Departamento + "</td><td>" + item.LocalEntrega + "</td><td>" + item.Estado.Nome + "</td><td>" + item.Cidade.Nome + "</td><td>" + item.Observacoes + "</td><td>" + item.TelefoneContato + "</td></tr>";
            }
            _result += "</table> ";

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "utf-8";
            Response.BufferOutput = true;
            Response.AddHeader("content-disposition", "attachment; filename = pedidos." + DateTime.Now.ToString("yyyyMMdd") + ".xls");

            Response.ContentEncoding = System.Text.Encoding.GetEncoding("Windows-1252");
            Response.Write(_result);

            Response.End();
        }
    }
}
