﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Data.Objects;
using System.Linq.Expressions;
using Web.Models;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Repositories;
using Domain.Service;
using Domain.Core;

namespace Site_Lacos.Areas.Empresas.Controllers {
    public class PedidoController : BaseController {

        #region Variaveis
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<EmpresaProdutoTamanho_X_Colaborador> empresaProdutoTamanho_X_ColaboradorRepository;
        private IPersistentRepository<Estado> estadoRepository;
        //private IPersistentRepository<Cidade> cidadeRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private RelatorioRepository relatorioRepository;
        private IPersistentRepository<Local> localRepository; 
        private IPersistentRepository<Frase> fraseRepository;
        private IPersistentRepository<EmpresaProdutoTamanho> empresaProdutoTamanhoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<PedidoEmpresaNota> pedidoNotaRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private CupomRepository cupomRepository;
        private CupomService cupomService;
        private ColaboradorModel clienteModel;
        private EmpresaModel empresaModel;
        #endregion

        public PedidoController(ObjectContext context)
            : base(context) {
            clienteModel = new ColaboradorModel(context);

            empresaRepository = new PersistentRepository<Empresa>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            //cidadeRepository = new PersistentRepository<Cidade>(context);
            localRepository = new PersistentRepository<Local>(context);
            empresaProdutoTamanho_X_ColaboradorRepository = new PersistentRepository<EmpresaProdutoTamanho_X_Colaborador>(context);
            fraseRepository = new PersistentRepository<Frase>(context);
            empresaProdutoTamanhoRepository = new PersistentRepository<EmpresaProdutoTamanho>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            relatorioRepository = new RelatorioRepository(context);
            pedidoNotaRepository = new PersistentRepository<PedidoEmpresaNota>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            cupomRepository = new CupomRepository(context);
            cupomService = new CupomService(context);
        }

        public ActionResult Index() {

            ViewBag.EmpresaID = clienteModel.EmpresaID;
            ViewBag.TravaKitBEBE = false;

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            var KitsBebeRestritos = empresaRepository.Get(clienteModel.EmpresaID).EmpresaProdutoTamanhoes.Where(p => p.RestritoColaboradores).ToList();
            if(!KitsBebeRestritos.Where(r =>  r.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe && r.EmpresaProdutoTamanho_X_Colaborador.Where(p => p.ColaboradorID == clienteModel.CurrentColaborador.ID).Any()).Any() && KitsBebeRestritos.Count > 0)
            {
                ViewBag.TravaKitBEBE = true;
            }
            
            return View();
        }

        public ActionResult Produtos(string rotuloUrl) {
            var colaborador = clienteModel.CurrentColaborador;
            var Empresa = empresaRepository.Get(colaborador.EmpresaID);

            string strProdutoRestritoGrupos = ConfigurationManager.AppSettings["ProdutoRestritoGrupoEconomico"];
            string strProdutoRestritoCNPJs = ConfigurationManager.AppSettings["ProdutoRestritoCNPJ"];

            #region PRODUTOS PARA EXCLUIR - PRODUTOS PENTENCEN A OUTRAS EMPRESAS
            
            List<int> ProdutosRestitosOutrasEmpresas = new List<int>();
            foreach (var item in (strProdutoRestritoGrupos + "|" + strProdutoRestritoCNPJs).Split('|'))
            {
                foreach (var item_3 in item.Split(':')[1].ToString().Split(','))
                {
                    ProdutosRestitosOutrasEmpresas.Add(int.Parse(item_3.ToString()));
                }
            }

            #endregion

            

            Expression<Func<EmpresaProdutoTamanho, bool>> exp =
                p => p.EmpresaID == colaborador.EmpresaID && p.Disponivel;

            ViewBag.ProdutosTamanhos = empresaProdutoTamanhoRepository.GetByExpression(exp).ToList();

            ViewBag.Link = "empresas/pedido";
            Expression<Func<Produto, bool>> expProd = c => c.DisponivelLacos && c.ProdutoTamanhoes.Count > 0;

            var produtos = produtoRepository.GetByExpression(expProd).ToList();

            // REMOVER PRODUTOS RESTRITOS PARA OUTRAS EMPRESAS, E A EMPRESA ATUAL NAO TIVER A FLAG SoExibeProdutosConveniados MARCADA
            if (!clienteModel.CurrentColaborador.Empresa.SoExibeProdutosConveniados)
                produtos.RemoveAll(r => ProdutosRestitosOutrasEmpresas.Contains(r.ID));

            ViewBag.SoExibeProdutosConveniados = false;

            if (clienteModel.CurrentColaborador.Empresa.SoExibeProdutosConveniados) {
                ViewBag.SoExibeProdutosConveniados = true;
                Expression<Func<EmpresaProdutoTamanho, bool>> exp2 = 
                    c => c.EmpresaID == clienteModel.EmpresaID 
                    && 
                    c.ProdutoTamanho.Disponivel 
                    && 
                    c.ProdutoTamanho.Produto.DisponivelLacos 
                    && c.Disponivel;
                produtos = empresaProdutoTamanhoRepository.GetByExpression(exp2).Select(c => c.ProdutoTamanho.Produto).ToList().Distinct().ToList();
            }

            ViewBag.Tipo = rotuloUrl;
            switch (rotuloUrl) {
                case "arranjo-maternidade":
                    ViewBag.Titulo = "Arranjo Maternidade";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.ArranjoFlor).ToList();
                    
                    try{
                        #region New

                        if (!string.IsNullOrWhiteSpace(strProdutoRestritoGrupos)) {
                            string[] arrGrupo = strProdutoRestritoGrupos.Split('|');
                            foreach (string grupo in arrGrupo){
                                string[] arrIds = grupo.Split(':');
                                if (Empresa.GrupoEconomico != arrIds[0]){
                                    int[] ids = arrIds[1].Split(',').Select(i=> int.Parse(i)).ToArray();
                                    produtos = produtos.Where(p => !ids.Contains(p.ID)).ToList();
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(strProdutoRestritoCNPJs)) {
                            string[] arrCNPJ = strProdutoRestritoCNPJs.Split('|');
                            foreach (string cnpj in arrCNPJ) {
                                string[] arrIds = cnpj.Split(':');
                                if (Empresa.CNPJ != arrIds[0]) {
                                    int[] ids = arrIds[1].Split(',').Select(i => int.Parse(i)).ToArray();
                                    produtos = produtos.Where(p => !ids.Contains(p.ID)).ToList();
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex){
                        #region Old
                        if (Empresa.GrupoEconomico != "PWC") {
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 251));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 252));
                        }

                        if (Empresa.CNPJ != "57755217001010") {//KPMG
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 254));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 255));
                        }
                        if (Empresa.CNPJ != "09324949000111") {//Autopista
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 257));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 258));
                        }
                        if (Empresa.CNPJ != "13198370000127") {//AGC Vidros
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 259));
                            //produtos.Remove(produtos.FirstOrDefault(r => r.ID == 258));
                        }
                        #endregion

                    }
                    break;
                case "kits-bebe":
                    ViewBag.Titulo = "Kits Bebê";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Kitsbebe).ToList();

                    try
                    {
                        #region New

                        if (!string.IsNullOrWhiteSpace(strProdutoRestritoGrupos))
                        {
                            string[] arrGrupo = strProdutoRestritoGrupos.Split('|');
                            foreach (string grupo in arrGrupo)
                            {
                                string[] arrIds = grupo.Split(':');
                                if (Empresa.GrupoEconomico.Trim() != arrIds[0])
                                {
                                    int[] ids = arrIds[1].Split(',').Select(i => int.Parse(i)).ToArray();
                                    produtos = produtos.Where(p => !ids.Contains(p.ID)).ToList();
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(strProdutoRestritoCNPJs))
                        {
                            string[] arrCNPJ = strProdutoRestritoCNPJs.Split('|');
                            foreach (string cnpj in arrCNPJ)
                            {
                                string[] arrIds = cnpj.Split(':');
                                if (Empresa.CNPJ != arrIds[0])
                                {
                                    int[] ids = arrIds[1].Split(',').Select(i => int.Parse(i)).ToArray();
                                    produtos = produtos.Where(p => !ids.Contains(p.ID)).ToList();
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        #region Old
                        if (Empresa.GrupoEconomico != "PWC")
                        {
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 251));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 252));
                        }

                        if (Empresa.CNPJ != "57755217001010")
                        {//KPMG
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 254));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 255));
                        }
                        if (Empresa.CNPJ != "09324949000111")
                        {//Autopista
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 257));
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 258));
                        }
                        if (Empresa.CNPJ != "13198370000127")
                        {//AGC Vidros
                            produtos.Remove(produtos.FirstOrDefault(r => r.ID == 259));
                            //produtos.Remove(produtos.FirstOrDefault(r => r.ID == 258));
                        }
                        #endregion

                    }
                    break;
                case "arranjos":
                    ViewBag.Titulo = "Arranjos";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Aniversario).ToList();
                break;
                case "decoracao-de-escritorio":
                    ViewBag.Titulo = "Decoração de Escritório";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.OrquideaBuque).ToList();
                break;
                case "coroas-de-flores":
                    ViewBag.Titulo = "Coroas de Flores";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.CoroaFlor).OrderBy(c => c.PrecoMinimo).ToList();
                break;
                case "flores-para-presentes":
                    ViewBag.Titulo = "Flores para presentes";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Datas).ToList();
                break;
                case "decoracao-de-evento":
                    ViewBag.Titulo = "Decoração de Evento";
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Eventos).ToList();
                break;
            }


            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }



            #region PRODUTOS RESTRITO A COLABORADORES

            List<int> lstToRemove = new List<int>();

            var produtoEmpresa = Empresa.EmpresaProdutoTamanhoes.ToList();
            foreach (var produto in produtoEmpresa)
            {
                // se produto for restrito e nao estiver disponivel para colaborador
                if (produto.RestritoColaboradores && produto.EmpresaProdutoTamanho_X_Colaborador.Where(r => r.ColaboradorID == colaborador.ID).Count() == 0)
                    lstToRemove.Add(produto.ProdutoTamanho.Produto.ID);
            }

            if(lstToRemove.Count > 0)
                produtos.RemoveAll((x) => lstToRemove.Contains(x.ID));

            #endregion

            return View(produtos);
        }
        
        public ActionResult Novo(int id) {
            var colaborador = clienteModel.CurrentColaborador;
            var Produto = produtoRepository.Get(id);

            ViewBag.Produto = Produto;
            ViewBag.ProdutosTamanhos = empresaProdutoTamanhoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID && p.ProdutoTamanho.ProdutoID == id && p.Disponivel).ToList();
            ViewBag.Empresa = colaborador.Empresa;
            ViewBag.SoExibeProdutosConveniados = false;

            #region ESTADOS LIST

            var estados = estadoRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
            List<Estado> lstEstados = new List<Estado>();
            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }

            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));
            ViewBag.Estados = lstEstados;

            #endregion

            if (clienteModel.CurrentColaborador.Empresa.SoExibeProdutosConveniados)
                ViewBag.SoExibeProdutosConveniados = true;

            if (!string.IsNullOrEmpty(clienteModel.CurrentColaborador.Empresa.Logo))
            {
                ViewBag.LogoEmpresa = true;
                ViewBag.Logo = "http://admin.coroasparavelorio.com.br/content/empresas/" + clienteModel.CurrentColaborador.Empresa.ID + "/" + clienteModel.CurrentColaborador.Empresa.Logo;
            }

            switch (Produto.Tipo) {
                case Produto.Tipos.Kitsbebe:
                    ViewBag.Titulo = "Kits Bebê";
                    ViewBag.Link = "kits-bebe";
                    return View("~/Areas/Empresas/Views/Pedido/NovoKitBebe.cshtml");
                case Produto.Tipos.CoroaFlor:
                    ViewBag.Titulo = "Coroas de Flores";
                    ViewBag.Link = "coroas-de-flores";
                    return View("~/Areas/Empresas/Views/Pedido/NovoCoroas.cshtml");
            }

            return View();
        }

        public ActionResult Orcamento(string rotuloUrl) {
            var colaborador = clienteModel.CurrentColaborador;

            ViewBag.Relacionamentos = relacionamentoRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome).ToList();
            ViewBag.Frases = colaborador.Empresa.EmpresaFrases.ToList();
            ViewBag.Tipo = rotuloUrl;
            ViewBag.Empresa = colaborador.Empresa;
            ViewBag.ProdutoNome = "Novo Orçamento";
            switch (rotuloUrl) {
                case "flores-para-presentes":
                ViewBag.Titulo = "Flores para presentes";
                ViewBag.Link = "flores-para-presentes";
                break;
                case "decoracao-de-eventos":
                ViewBag.Titulo = "Decoração de Evento";
                ViewBag.Link = "decoracao-de-evento";
                break;
            }
            return View();
        }

        [HttpPost]
        public ContentResult Criar(int produto, string ordemdecompra, string pessoaHomenageada, string mensagem, string nomeFuncionario, string parentesco, string departamento,int? localId, string localEntrega, string localEntregaComplemento,int? estado, int? cidade, string data, string hora,string horario, string observacoes, string telefoneContato, string telefoneContatoAlternativo, string codigo, string ContatoHomenageado)
        {
            var colaborador = clienteModel.CurrentColaborador;
            dynamic horario_opt = null;

            if (horario != null && horario != "Outro") {
                horario_opt = "Horário de Entrega: " + horario + "\n\n";
            }

            var dataHora = data + " " + hora;
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada)) {
                return Content("Data e/ou Hora invalida(s)");
            }

            decimal valor = 0;
            decimal valorept = 0;
            decimal valorpt = 0;
            int? empresaprodutotamanhoid = null;
            int? produtotamanhoid = produto;
            var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produto && c.EmpresaID == colaborador.EmpresaID).FirstOrDefault();
            var produtotamanho = produtoTamanhoRepository.Get(produto);

            if (empresaprodutoTamanho != null) {
                valorept = empresaprodutoTamanho.Valor;
                empresaprodutotamanhoid = empresaprodutoTamanho.ID;
            }
            else if (produtotamanho != null)
            {
                valorpt = produtotamanho.Preco;
                produtotamanhoid = produtotamanho.ID;
            }

            if (valorept > 0)
                valor = valorept;
            else
                valor = valorpt;

            var valororiginal = valor;
            var cupom = cupomRepository.GetByCodigo(codigo);
            int? cupomID = null;
            if (cupom != null) {
                if (cupomService.PodeUtilizar(colaborador.Empresa, cupom)) {
                    cupomID = cupom.ID;
                    if (cupom.Tipo == Cupom.Tipos.Valor) {
                        valor = valor - cupom.Valor;
                    }
                    else if (cupom.Tipo == Cupom.Tipos.Porcentagem) {
                        valor = valor * (1 - cupom.Valor / 100);
                    }
                    if (valor < 0)
                        valor = 0;
                }
            }


            var cidadeid = 8570; //SP
            var estadoid = 26; //SP

            if (cidade.HasValue && estado.HasValue) {
                cidadeid = cidade.Value;
                estadoid = estado.Value;
            }
            else if (colaborador.Empresa.CidadeID.HasValue && colaborador.Empresa.EstadoID.HasValue) {
                cidadeid = colaborador.Empresa.CidadeID.Value;
                estadoid = colaborador.Empresa.EstadoID.Value;
            }

            var pedido = new PedidoEmpresa()
            {
                CidadeID = cidadeid,
                Codigo = Guid.NewGuid(),
                ColaboradorID = colaborador.ID,
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                Departamento = departamento,
                EmpresaID = colaborador.EmpresaID,
                EmpresaProdutoTamanhoID = empresaprodutotamanhoid,
                ProdutoTamanhoID = produtotamanhoid,
                EstadoID = estadoid,
                Origem = "SITE",
                OrigemForma = "ONLINE",
                OrigemSite = "LAÇOS CORPORATIVOS",
                ComplementoLocalEntrega = localEntregaComplemento.ToString().ToUpper(),
                LocalEntrega = localEntrega,
                Mensagem = mensagem,
                NomeFuncionario = nomeFuncionario,
                Observacoes = horario_opt + observacoes,
                Parentesco = parentesco,
                PessoaHomenageada = pessoaHomenageada,
                OrdemDeCompra = ordemdecompra,
                Status = PedidoEmpresa.TodosStatus.NovoPedido,
                StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                TelefoneContato = telefoneContato,
                Valor = valor,
                SubTotal = valororiginal,
                ValorDesconto = valororiginal - valor,
                CupomID = cupomID,
                QualidadeStatusContatoID = (int)PedidoEmpresa.TodosStatusContato.NI,
                QualidadeStatusContatoNaoID = (int)PedidoEmpresa.TodosStatusContatoNao.NI,
                TelefoneContatoAlternativo = telefoneContatoAlternativo,
                StatusFotoID = 1
            };



            var _LocalEntregaData = localRepository.GetByExpression(r => r.Titulo.ToUpper() == localEntrega.ToUpper() && r.CidadeID == cidadeid && r.EstadoID == estadoid).ToList();

            if (_LocalEntregaData.Count() > 0)
            {
                pedido.LocalID = _LocalEntregaData[0].ID;
            }

            #region ALOCAR PEDIDO

            int? ProxAlocadoID = 0;

            try
            {
                ProxAlocadoID = relatorioRepository.spRetornaProxAlocado();
            }
            catch { }

            if (ProxAlocadoID > 0)
            {
                pedido.AdministradorID = (int)ProxAlocadoID;
                pedidoRepository.Save(pedido);
            }
            else
            {
                pedido.AdministradorID = 1043;
                pedidoRepository.Save(pedido);
            }

            #endregion

            if (!string.IsNullOrEmpty(ContatoHomenageado))
            {
                var nota = new PedidoEmpresaNota();

                nota.DataCriacao = DateTime.Now;
                nota.PedidoEmpresaID = pedido.ID;
                nota.Observacoes = "Telefone de Contato do Homenageado: " + ContatoHomenageado;

                pedidoNotaRepository.Save(nota);
            }

            try
            {
                pedido.EnviarPedidoEmail();
            }
            catch{}
           

            if (cupomID.HasValue) {
                cupomService.Utilizar(cupom.Codigo);

                //CRIA UMA NOTA
                var pedidonota = new PedidoEmpresaNota();
                pedidonota.PedidoEmpresaID = pedido.ID;
                pedidonota.DataCriacao = DateTime.Now;
                pedidonota.Observacoes = "Empresa utilizou o cupom " + cupom.Codigo + " que garantiu " + (cupom.Tipo == Cupom.Tipos.Valor ? cupom.Valor.ToString("C2") : cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                pedidoNotaRepository.Save(pedidonota);
            }

            if (pedido.Empresa.FaturamentoInstantaneo && pedido.Empresa.OrdemDeCompra == false)
            {
                var fatura = new Faturamento();
                fatura.EmpresaID = pedido.EmpresaID;
                fatura.Titulo = "automática referente ao pedido #" + pedido.ID;
                fatura.Observacoes = "";
                fatura.Status = Faturamento.TodosStatus.AguardandoPagamento;
                fatura.StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento;
                fatura.FormaPagamento = Pedido.FormasPagamento.Boleto;
                fatura.MeioPagamento = Pedido.MeiosPagamento.Boleto;
                fatura.ValorTotal = pedido.Valor;
                fatura.ValorDesconto = pedido.ValorDesconto;
                fatura.ValorEstorno = 0;
                fatura.ValorPago = 0;
                fatura.NFeValor = pedido.Valor;
                fatura.DataCriacao = DateTime.Now;
                fatura.DataAtualizacao = DateTime.Now;
                fatura.DataVencimento = new Funcoes().DataVencimentoUtil(pedido.Empresa.DiasParaVencimentoBoleto);
                fatura.DataVencimentoOriginal = fatura.DataVencimento;
                faturamentoRepository.Save(fatura);

                pedido.FaturamentoID = fatura.ID;
                pedidoRepository.Save(pedido);

                if (fatura.ValorTotal > 0) {
                    if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto) {
                        var random = new Random();
                        var nossoNumero = "";
                        do {
                            nossoNumero = random.Next(99999999).ToString("00000000");
                        }
                        while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                        var numeroDocumento = "2" + DateTime.Now.Year + fatura.ID.ToString("00000");

                        var boleto = new Boleto();


                        boleto.FaturamentoID = fatura.ID;
                        boleto.TipoID = (int)Boleto.TodosTipos.PJ;

                        boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                        boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                        boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                        boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                        boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                        boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                        boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC;

                        boleto.DataCriacao = DateTime.Now;
                        boleto.DataVencimento = fatura.DataVencimento;
                        boleto.DataEnvioLembreteVencimento = null;
                        
                        boleto.NossoNumero = nossoNumero;
                        boleto.NumeroDocumento = numeroDocumento;
                        boleto.SacadoDocumento = fatura.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "").Trim();
                        boleto.SacadoNome = fatura.Empresa.RazaoSocial;
                        if (String.IsNullOrEmpty(fatura.Empresa.Bairro))
                            boleto.SacadoBairro = "";
                        else
                            boleto.SacadoBairro = fatura.Empresa.Bairro;

                        if (String.IsNullOrEmpty(fatura.Empresa.CEP))
                            boleto.SacadoCEP = "";
                        else
                            boleto.SacadoCEP = fatura.Empresa.CEP;

                        if (fatura.Empresa.Cidade == null)
                            boleto.SacadoCidade = "";
                        else
                            boleto.SacadoCidade = fatura.Empresa.Cidade.Nome;

                        if (String.IsNullOrEmpty(fatura.Empresa.Logradouro))
                            boleto.SacadoEndereco = "";
                        else
                            boleto.SacadoEndereco = fatura.Empresa.Logradouro + " " + fatura.Empresa.Numero + " " + fatura.Empresa.Complemento;

                        if (fatura.Empresa.Estado == null)
                            boleto.SacadoUF = "";
                        else
                            boleto.SacadoUF = fatura.Empresa.Estado.Sigla;

                        boleto.Comando = Boleto.Comandos.CriarRemessa;
                        boleto.Processado = false;
                        boleto.StatusPagamento = Boleto.TodosStatusPagamento.AguardandoPagamento;
                        boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Criado;
                        boleto.Valor = fatura.ValorTotal;
                        boleto.DataVencimentoOriginal = boleto.DataVencimento;

                        boletoRepository.Save(boleto);

                        fatura.EnviarBoleto(fatura.ID);
                        return Content("BOLETO");
                    }
                }
            }

            return Content("OK");
        }

        [HttpPost]
        public ContentResult CriarKitBB(int produto, string pessoaHomenageada, string ordemdecompra, string mensagem, string nomeFuncionario, string parentesco, int? localId, string localEntrega, string localEntregaComplemento, int? estado, int? cidade, string data, string hora, string horario, string observacoes, string telefoneContato, string telefoneContatoAlternativo, string codigo, string ContatoHomenageado, int NrEntrega, string CepEntrega, string LogradouroEntrega, string BairroEntrega, string CidadeEntrada)
        {
            var dataHora = data;
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada))
            {
                return Content("Data e/ou Hora invalida(s)");
            }

            var colaborador = clienteModel.CurrentColaborador;
            decimal valor = 0;
            decimal valorept = 0;
            decimal valorpt = 0;
            int? empresaprodutotamanhoid = null;
            int? produtotamanhoid = produto;
            var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produto && c.EmpresaID == colaborador.EmpresaID).FirstOrDefault();
            var produtotamanho = produtoTamanhoRepository.Get(produto);

            if (empresaprodutoTamanho != null)
            {
                valorept = empresaprodutoTamanho.Valor;
                empresaprodutotamanhoid = empresaprodutoTamanho.ID;
            }
            else if (produtotamanho != null)
            {
                valorpt = produtotamanho.Preco;
                produtotamanhoid = produtotamanho.ID;
            }

            if (valorept > 0)
                valor = valorept;
            else
                valor = valorpt;

            var valororiginal = valor;
            var cupom = cupomRepository.GetByCodigo(codigo);
            int? cupomID = null;
            if (cupom != null)
            {
                if (cupomService.PodeUtilizar(colaborador.Empresa, cupom))
                {
                    cupomID = cupom.ID;
                    if (cupom.Tipo == Cupom.Tipos.Valor)
                    {
                        valor = valor - cupom.Valor;
                    }
                    else if (cupom.Tipo == Cupom.Tipos.Porcentagem)
                    {
                        valor = valor * (1 - cupom.Valor / 100);
                    }
                    if (valor < 0)
                        valor = 0;
                }
            }

            Local local = new Local();

            try
            {
                #region BUSCAR LOCAL POR CEP

                var resultado = new CEP.CEP(CepEntrega);
                var CidadeNome = resultado.Cidade;

                if (string.IsNullOrEmpty(CidadeNome))
                    CidadeNome = CidadeEntrada;

                var EstadoNome = resultado.UF;

                // BUSCA COM NOME DA CIDADE COM ACENTOS
                try
                {
                    local = localRepository.GetByExpression(r => r.Cidade.Nome == CidadeNome && r.Estado.Sigla == EstadoNome).First();
                }
                catch {
                    local = null;
                }

                if(local == null)
                {
                    // BUSCA COM NOME DA CIDADE SEM ACENTOS
                    try
                    {
                        CidadeNome = Domain.Core.Funcoes.AcertaAcentos(CidadeNome);
                        local = localRepository.GetByExpression(r => r.Cidade.Nome == CidadeNome && r.Estado.Sigla == EstadoNome).First();
                    }
                    catch
                    {
                        local = null;
                    }
                }


                if (local == null)
                    return Content("ERRO: Local de entrega inválido. CEP: "+ CepEntrega);

                #endregion

            }
            catch (Exception ex)
            {
                return Content("ERRO: BUSCAR LOCAL POR CEP " + ex.ToString() + " " + ex.InnerException);                
            }

            
            PedidoEmpresa pedido;

            try
            {
                pedido = new PedidoEmpresa()
                {
                    CidadeID = (int)local.CidadeID,
                    EstadoID = (int)local.EstadoID,
                    Codigo = Guid.NewGuid(),
                    ColaboradorID = colaborador.ID,
                    DataAtualizacao = DateTime.Now,
                    DataCriacao = DateTime.Now,
                    DataSolicitada = dataSolicitada,
                    Departamento = "",
                    EmpresaID = colaborador.EmpresaID,
                    EmpresaProdutoTamanhoID = empresaprodutotamanhoid,
                    ProdutoTamanhoID = produtotamanhoid,
                    Origem = "SITE",
                    OrigemForma = "ONLINE",
                    OrigemSite = "LAÇOS CORPORATIVOS",
                    ComplementoLocalEntrega = localEntregaComplemento.ToString().ToUpper(),
                    LocalEntrega = "",
                    Mensagem = mensagem,
                    NomeFuncionario = "",
                    Observacoes = "",
                    Parentesco = "",
                    PessoaHomenageada = pessoaHomenageada,
                    OrdemDeCompra = ordemdecompra,
                    Status = PedidoEmpresa.TodosStatus.NovoPedido,
                    StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                    TelefoneContato = telefoneContato,
                    Valor = valor,
                    SubTotal = valororiginal,
                    ValorDesconto = valororiginal - valor,
                    CupomID = cupomID,
                    QualidadeStatusContatoID = (int)PedidoEmpresa.TodosStatusContato.NI,
                    QualidadeStatusContatoNaoID = (int)PedidoEmpresa.TodosStatusContatoNao.NI,
                    TelefoneContatoAlternativo = telefoneContatoAlternativo,
                    StatusFotoID = 1, 
                    CepEntrega = CepEntrega.Replace("-", ""),
                    NrEntrega = NrEntrega, 
                    LogradouroEntrega = LogradouroEntrega,
                    BairroEntrega = BairroEntrega
                };

                #region ALOCAR PEDIDO

                int? ProxAlocadoID = 0;

                try
                {
                    ProxAlocadoID = relatorioRepository.spRetornaProxAlocado();
                }
                catch { }

                if (ProxAlocadoID > 0)
                {
                    pedido.AdministradorID = (int)ProxAlocadoID;
                    pedidoRepository.Save(pedido);
                }
                else
                {
                    pedido.AdministradorID = 1043;
                    pedidoRepository.Save(pedido);
                }

                #endregion


                if (!string.IsNullOrEmpty(ContatoHomenageado))
                {
                    var nota = new PedidoEmpresaNota();

                    nota.DataCriacao = DateTime.Now;
                    nota.PedidoEmpresaID = pedido.ID;
                    nota.Observacoes = "Telefone de Contato do Homenageado: " + ContatoHomenageado;

                    pedidoNotaRepository.Save(nota);
                }

                try
                {
                    pedido.EnviarPedidoEmail();
                }
                catch { }


                if (cupomID.HasValue)
                {
                    cupomService.Utilizar(cupom.Codigo);

                    //CRIA UMA NOTA
                    var pedidonota = new PedidoEmpresaNota();
                    pedidonota.PedidoEmpresaID = pedido.ID;
                    pedidonota.DataCriacao = DateTime.Now;
                    pedidonota.Observacoes = "Empresa utilizou o cupom " + cupom.Codigo + " que garantiu " + (cupom.Tipo == Cupom.Tipos.Valor ? cupom.Valor.ToString("C2") : cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                    pedidoNotaRepository.Save(pedidonota);
                }

                if (pedido.Empresa.FaturamentoInstantaneo && pedido.Empresa.OrdemDeCompra == false)
                {
                    var fatura = new Faturamento();
                    fatura.EmpresaID = pedido.EmpresaID;
                    fatura.Titulo = "automática referente ao pedido #" + pedido.ID;
                    fatura.Observacoes = "";
                    fatura.Status = Faturamento.TodosStatus.AguardandoPagamento;
                    fatura.StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento;
                    fatura.FormaPagamento = Pedido.FormasPagamento.Boleto;
                    fatura.MeioPagamento = Pedido.MeiosPagamento.Boleto;
                    fatura.ValorTotal = pedido.Valor;
                    fatura.ValorDesconto = pedido.ValorDesconto;
                    fatura.ValorEstorno = 0;
                    fatura.ValorPago = 0;
                    fatura.NFeValor = pedido.Valor;
                    fatura.DataCriacao = DateTime.Now;
                    fatura.DataAtualizacao = DateTime.Now;
                    fatura.DataVencimento = new Funcoes().DataVencimentoUtil(pedido.Empresa.DiasParaVencimentoBoleto);
                    fatura.DataVencimentoOriginal = fatura.DataVencimento;
                    faturamentoRepository.Save(fatura);

                    pedido.FaturamentoID = fatura.ID;
                    pedidoRepository.Save(pedido);

                    if (fatura.ValorTotal > 0)
                    {
                        if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto)
                        {
                            var random = new Random();
                            var nossoNumero = "";
                            do
                            {
                                nossoNumero = random.Next(99999999).ToString("00000000");
                            }
                            while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                            var numeroDocumento = "2" + DateTime.Now.Year + fatura.ID.ToString("00000");

                            var boleto = new Boleto();


                            boleto.FaturamentoID = fatura.ID;
                            boleto.TipoID = (int)Boleto.TodosTipos.PJ;

                            boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                            boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                            boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                            boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                            boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                            boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                            boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC;

                            boleto.DataCriacao = DateTime.Now;
                            boleto.DataVencimento = fatura.DataVencimento;
                            boleto.DataEnvioLembreteVencimento = null;

                            boleto.NossoNumero = nossoNumero;
                            boleto.NumeroDocumento = numeroDocumento;
                            boleto.SacadoDocumento = fatura.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "").Trim();
                            boleto.SacadoNome = fatura.Empresa.RazaoSocial;
                            if (String.IsNullOrEmpty(fatura.Empresa.Bairro))
                                boleto.SacadoBairro = "";
                            else
                                boleto.SacadoBairro = fatura.Empresa.Bairro;

                            if (String.IsNullOrEmpty(fatura.Empresa.CEP))
                                boleto.SacadoCEP = "";
                            else
                                boleto.SacadoCEP = fatura.Empresa.CEP;

                            if (fatura.Empresa.Cidade == null)
                                boleto.SacadoCidade = "";
                            else
                                boleto.SacadoCidade = fatura.Empresa.Cidade.Nome;

                            if (String.IsNullOrEmpty(fatura.Empresa.Logradouro))
                                boleto.SacadoEndereco = "";
                            else
                                boleto.SacadoEndereco = fatura.Empresa.Logradouro + " " + fatura.Empresa.Numero + " " + fatura.Empresa.Complemento;

                            if (fatura.Empresa.Estado == null)
                                boleto.SacadoUF = "";
                            else
                                boleto.SacadoUF = fatura.Empresa.Estado.Sigla;

                            boleto.Comando = Boleto.Comandos.CriarRemessa;
                            boleto.Processado = false;
                            boleto.StatusPagamento = Boleto.TodosStatusPagamento.AguardandoPagamento;
                            boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Criado;
                            boleto.Valor = fatura.ValorTotal;
                            boleto.DataVencimentoOriginal = boleto.DataVencimento;

                            boletoRepository.Save(boleto);

                            fatura.EnviarBoleto(fatura.ID);
                            return Content("BOLETO");
                        }
                    }
                }
            }
            catch (Exception EX)
            {
                return Content("ERRO: SALVAR PEDIDO " + EX.Message + " " + EX.InnerException);
            }

            return Content("OK");
        }

        [HttpPost]
        public JsonResult ValidaCupom(string codigo) {
            var colaborador = clienteModel.CurrentColaborador;
            var cupom = cupomRepository.GetByCodigo(codigo);
            if (cupom == null) {
                return Json(new { mensagem = "Cupom inválido", retorno = "ERRO" });
            }
            else if (!cupomService.PodeUtilizar(colaborador.Empresa, cupom)) {
                return Json(new { mensagem = "Sua empresa já utilizou esse cupom", retorno = "ERRO" });
            }
            else {
                var retorno = "Este cupom garante ";
                if (cupom.Tipo == Cupom.Tipos.Porcentagem)
                    retorno += cupom.Valor + "% de desconto.";
                else
                    retorno += cupom.Valor.ToString("C2") + " de desconto.";
                return Json(new { mensagem = retorno, retorno = "OK", valor = cupom.Valor.ToString("N2").Replace(".", "").Replace(",", "."), tipo = cupom.TipoID });
            }
        }

        [HttpPost]
        public JsonResult CriarOrcamento(string tipo, string nome, string detalhes, string nomeFuncionario, string departamento, string LocalEntrega, string estadoEntrega, string cidadeEntrega, string data, string hora, string observacoes, string telefoneContato) {
            var colaborador = clienteModel.CurrentColaborador;

            var dataHora = data + " " + hora;
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada)) {
                return Json(new { status = "ERRO", mensagem = "Data e/ou Hora invalida(s)" });
            }

            var tiposolicitacao = "";
            if (tipo == "decoracao-de-evento")
                tiposolicitacao = "Decoração para Eventos";
            else if (tipo == "datas-comemorativas")
                tiposolicitacao = "Datas Comemorativas";

            //ENVIAR EMAIL
            var corpo = string.Format("O seguinte usuário entrou em contato solicitando um orçamento {0}:\n\nEmpresa: {1}\n\nNome do Colaborador: {2}\n\nEvento: {3}\n\nLocal: {4}\n\nCidade: {5}\n\nEstado: {6}\n\nData: {7}\n\nDetalhes: \n{8}\n\nFuncionário: {9}\n\nDepartamento: {10}\n\nObservações: \n{11}\n\nTelefone: {12}",
                                                                                               tiposolicitacao, colaborador.Empresa.RazaoSocial, colaborador.Nome, nome, LocalEntrega, cidadeEntrega, estadoEntrega, dataHora, detalhes, nomeFuncionario, departamento, observacoes, telefoneContato);

            var ValidaEmail = Funcoes.EnviaEmail
                (
                    Configuracoes.EMAIL_NOREPLY,
                    "Laços Corporativos", 
                    Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, 
                    "ORÇAMENTO", 
                    colaborador.Email, 
                    colaborador.Nome, 
                    Configuracoes.EMAIL_COPIA, 
                    "[LAÇOS CORPORATIVOS] - Orçamento",
                    corpo, 
                    false
                );

            if (ValidaEmail)
                return Json(new { status = "OK" });
            else
                return Json(new { status = "ERRO", mensagem = "Não foi possível enviar seu orçamento, tente novamente mais tarde ou entre em contato através de nossos telefones." });

        }

        public struct Cidade {
            public int ID;
            public string Nome;
        }

        public JsonResult Cidades(int estadoID) {
            var cidades = new List<Cidade>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null) {
                foreach (var _cidade in estado.Cidades.OrderBy(c => c.Nome))
                    cidades.Add(new Cidade() {
                        ID = _cidade.ID,
                        Nome = _cidade.Nome
                    });
            }
            return Json(cidades);
        }

        [HttpPost]
        public JsonResult GetLocais(int CidadeID, string TipoProduto = "")
        {
            List<SelectListItem> result = new List<SelectListItem>();

            var Locais = localRepository.GetByExpression(r => r.CidadeID == CidadeID).OrderBy(r => r.Titulo).ToList();

            if (TipoProduto == Produto.Tipos.Kitsbebe.ToString())
            {
                Locais = Locais.Where(r => r.Tipo != Local.Tipos.Cemiterio).ToList();
            }

            foreach (var Result in Locais)
            {
                result.Add(new SelectListItem { Text = Result.Titulo + " - " + Result.Tipo + " - " + Result.Telefone, Value = Result.ID.ToString() });
            }

            result.Insert(0, new SelectListItem { Text = "LOCAL NÃO CADASTRADO", Value = "NULL" });

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetLocalExiste(string strNmLocal, int cidadeId, int estadoId)
        {
            if (strNmLocal == "LOCAL NÃO CADASTRADO")
            {
                //var _localNaoCadastrado = Json(true + "," + "0", JsonRequestBehavior.AllowGet);

                string _localNaoCadastradolId = "0";
                string _localNaoCadastradoTitulo = strNmLocal;
                string _localNaoCadastradoTel = "LOCAL NÃO CADASTRADO";

                var _dataLocalNaoCadastrado = Json(true + "," + _localNaoCadastradolId + "," + _localNaoCadastradoTitulo + "," + _localNaoCadastradoTel, JsonRequestBehavior.AllowGet);

                return _dataLocalNaoCadastrado;
            }

            bool Existe = false;
            var Local = localRepository.GetByExpression(r => r.Titulo.ToUpper() == strNmLocal.ToUpper() && r.CidadeID == cidadeId && r.EstadoID == estadoId).ToList();

            if (Local.Count == 0)
            {
                Existe = false;
                return Json(Existe.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                Existe = true;

                var _localEncontrado = Local.First();
                string _localId = string.IsNullOrEmpty(_localEncontrado.ID.ToString()) ? "" : _localEncontrado.ID.ToString();
                string _localTitulo = string.IsNullOrEmpty(Local.First().Titulo) ? "" : _localEncontrado.Titulo.ToString();
                string _localTel = string.IsNullOrEmpty(Local.First().Telefone) ? "" : Local.First().Telefone.ToString();

                var _dataLocalEncontrado = Json(Existe.ToString() + "," + _localId + "," + _localTitulo + "," + _localTel, JsonRequestBehavior.AllowGet);

                return _dataLocalEncontrado;
            }
        }
    }
}
