﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Site_Lacos.Areas.Empresas
{
    public class EmpresasAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Empresas";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        { 
            context.MapRoute(
                "Pedido_default",
                "empresas/pedido/novo/{id}/{rotuloUrl}",
                new { action = "Novo", controller = "Pedido", rotuloUrl = UrlParameter.Optional },
                new string[] { "Site_Lacos.Areas.Empresas.Controllers" }
            );

            context.MapRoute(
                "PedidoProduto_default",
                "empresas/pedido/produtos/{rotuloUrl}",
                new { action = "Produtos", controller = "Pedido", rotuloUrl = UrlParameter.Optional },
                new string[] { "Site_Lacos.Areas.Empresas.Controllers" }
            );

            context.MapRoute(
                "Orcamento_default",
                "empresas/pedido/{rotuloUrl}/orcamento",
                new { action = "Orcamento", controller = "Pedido", rotuloUrl = UrlParameter.Optional },
                new string[] { "Site_Lacos.Areas.Empresas.Controllers" }
            );

            context.MapRoute(
                "Empresas_default",
                "empresas/{controller}/{action}/{id}",
                new { action = "Index", controller = "Home", id = UrlParameter.Optional },
                new string[] { "Site_Lacos.Areas.Empresas.Controllers" }
            );
        }
    }
}
