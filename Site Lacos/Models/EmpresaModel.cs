﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Repositories;
using Domain.Entities;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Models
{
    public class EmpresaModel
    {
        private IPersistentRepository<Empresa> EmpresaRepository;
        
        public EmpresaModel(ObjectContext context)
        {
            EmpresaRepository = new PersistentRepository<Empresa>(context);
        }

    }
}