﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Repositories;
using Domain.Entities;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Models
{
    public class ColaboradorModel
    {
        private IPersistentRepository<Colaborador> colaboradorRepository;

        public bool Autenticado { get; set; }
        public Colaborador CurrentColaborador { get; set; }

        public int ID
        {
            get { return CurrentColaborador != null ? CurrentColaborador.ID : 0; }
        }

        public int EmpresaID;

        public string Nome
        {
            get { return CurrentColaborador != null ? CurrentColaborador.Nome : ""; }
        }

        public ColaboradorModel(ObjectContext context)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var loggeduser = LoggedUser.GetFromJSON(HttpContext.Current.User.Identity.Name);
                var colaborador = colaboradorRepository.Get(loggeduser.ID);
                if (colaborador != null)
                {
                    try { EmpresaID = Convert.ToInt32(loggeduser.AditionalInfo["empresaID"]); }
                    catch { }
                    Autenticado = true;
                    CurrentColaborador = colaborador;
                }
            }
            else
                Autenticado = false;
        }

    }
}