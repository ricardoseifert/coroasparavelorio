		<header class="container-full default">
			<div class="container">
				<nav class="nav">
					<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
					<p class="telefone">Atendimento <br> 11 4097•9449</p>
					<a href="http://corporativo.lacoscorporativos.com.br/login" class="acesso-empresas btn btn-stroke">Acesso Empresas</a>
				</nav>
				<div class="container logo">
					<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-default.png" alt="" /></a>
				</div>

				<div class="row">
					<section class="col-md-12">
						<h1>O que <b>fazemos?</b></h1>
						<!--p>Somos uma facilitadora para você promover ações que reforçam os laços entre sua empresa e seus colaboradores, com ações e presentes <b>personalizados</b>.</p>
						<br>
						<p>Com <b>agilidade</b>, <b>logística nacional</b> e <b>criatividade</b>, fazemos com que sua empresa esteja presente nos principais momentos da vida das <b>pessoas</b>.</p-->
							
						<i class="scroll-down bounce"></i>
					</section>
<!--
					<section class="col-md-5">
						<div class="form-header">
							<form id="id-form-home">
								<p class="centered">Vamos <b>Juntos</b>?</p>
								<p class="centered subtitulo-form">Preencha o formulário e vamos começar</p>

								<div>
									<input type="text" placeholder="Nome*" name="nome" required />
								</div>

								<div>
									<input type="email" placeholder="Email*" name="email"  required />
								</div>

								<div>
									<input type="tel" placeholder="Telefone*" name="telefone" required  />
								</div>

								<div>
									<input type="text" placeholder="Empresa*" name="empresa"  required />
								</div>

								<button type="submit" class="btn btn-call-to-action">Comece <b>Agora!</b></button>
								<i class="loader"></i>
							</form>
						</div>
					</section> -->
				</div>
			</div>
		</header>
