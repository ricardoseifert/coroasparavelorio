<?php /* Template Name: Página de Orçamento */ ?>

<?php get_header(); ?>

<?php get_template_part('internal-header'); ?>

<section class="intro-products">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
				<?php if(isset($_GET['PID'])): ?>
					<?php
						$args = array(
							'post_type' => 'produtos',
							'posts_per_page' => -1,
							'post_status' => 'publish',
							'p' => $_GET['PID']
						);

						$my_query = null;
						$my_query = new WP_Query($args);
					?>

					<?php if($my_query->have_posts()): ?>
						<?php while($my_query->have_posts()) : $my_query->the_post(); ?>
							<h1>Orçamento para <?php the_title(); ?></h1>
						<?php endwhile; ?>
					<?php endif; wp_reset_query(); ?>
				<?php else: ?>
					<h1><b><?php the_title(); ?></b></h1>
				<?php endif; ?>
	      <p><?php echo $post->post_content; ?></p>
	    </div>
	  </div>
	</div>
</section>

<section class="container contact">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<form>
				<?php if(isset($_GET['PID'])): ?>
				<div class="row">
					<div class="col-md-8">
						<?php
	            $args = array(
	              'post_type' => 'produtos',
	              'posts_per_page' => -1,
	              'post_status' => 'publish',
								'p' => $_GET['PID']
	            );

	            $my_query = null;
	            $my_query = new WP_Query($args);
	          ?>
						<?php if($my_query->have_posts()): ?>
							<?php while($my_query->have_posts()) : $my_query->the_post(); ?>
								<input type="hidden" name="prod_nome" value="<?php the_title(); ?>">
								<u class="inline-title">
									<?php the_title(); ?>
								</u>
							<?php endwhile; ?>
						<?php endif; wp_reset_query(); ?>
					</div>
					<div class="col-md-4">
						<label>Quantidade*</label>
						<input type="text" name="prod_quantidade">
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-6">
						<label>Nome*</label>
						<input type="text" name="nome" id="afterinput" />
					</div>
					<div class="col-md-6">
						<label>Email*</label>
						<input type="email" name="email" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Empresa*</label>
						<input type="text" name="empresa" />
					</div>
					<div class="col-md-6">
						<label>Telefone*</label>
						<input type="tel" name="telefone" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label>Mensagem</label>
						<textarea name="mensagem" rows="6"></textarea>
					</div>
				</div>
				<div class="row centered">
					<div class="col-md-12">
						<button type="submit" class="btn btn-call-to-action">Enviar!</button>
						<i class="loader"></i>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<footer id="PageDefault" class="container-full centered default" role="contentinfo">
	<section class="copyright">
		<p>
      Atendimento: (11) 4097-9449
      <br><br>
        <a href="https://www.lacoscorporativos.com.br/politicas-de-cancelamento-e-devolucao">Políticas de Cancelamento e de Troca e Devolução</a>
        <br /><br />
      Copyright <?php echo date('Y'); ?> &copy; - <?php bloginfo('name'); ?>. Todos os direitos reservados
    </p>
	</section>
</footer>

<?php get_footer(); ?>
