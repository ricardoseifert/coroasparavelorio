<?php get_header(); ?>

<?php get_template_part('internal-header'); ?>

	<section class="container-full">
	  <section class="intro-products">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-12">
        		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	          		<h1><b><?php the_title(); ?></b></h1>
	          		<p><?php the_content(); ?></p>
          		<?php endwhile; ?>

          		<?php else: ?>
          			<h1><b>Oops! =(</b></h1>
	          		<p>Parece que a página que você estava procurando foi movida ou não está mais disponível. Mas você pode navegar em nosso site pelo menu superior ou conferir as nossas principais soluções abaixo:</p>
	          	<?php endif; ?>
	        </div>
	      </div>
	    </div>
	  </section>
  	</section>

<?php get_template_part('main-footer'); ?>

<?php get_footer(); ?>