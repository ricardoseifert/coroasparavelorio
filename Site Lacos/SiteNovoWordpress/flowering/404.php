<?php
	function loop_taxonomies($id) {
		$termdata =  get_term_by('id', $id, 'nossas-solucoes');
		$name = $termdata->name;
		$description = $termdata->description;
		$slug = $termdata->slug;
		$thumbnail = get_term_thumbnail($id);
		$termchildren  = get_term_children($id, 'nossas-solucoes');

		echo '<article class="col-md-4 '.$slug.'" style="background-image:url(\''.substr(str_replace("src=\"", "", explode(" ",$thumbnail)[3]), 0, -1).'\')">';
		echo '<div class="subcat-description">';
		echo '<h2>'.$name.'</h2>';
		echo '<p>'.$description.'</p>';
		echo '<div class="tags-home">';
		foreach ( $termchildren as $child ) {
			$term = get_term_by( 'id', $child, 'nossas-solucoes' );
			echo '<span>' . $term->name . '</span>';
			// get_term_link( $child, 'nossas-solucoes' ); Para inserir link
		}
		echo '...';
		echo '</div>';
		echo '<a href="'.get_term_link($id).'" class="btn btn-stroke">Saiba Mais</a>';
		echo '</div>';
		echo '</article>';
	}
?>

<?php get_header(); ?>

<?php get_template_part('internal-header'); ?>

	<section class="container-full">
	  <section class="intro-products">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-12">
	          <h1><b>Oops! =(</b></h1>
	          <p>Parece que a página que você estava procurando foi movida ou não está mais disponível. Mas você pode navegar em nosso site pelo menu superior ou conferir as nossas principais soluções abaixo:</p>
	        </div>
	      </div>
	    </div>
	  </section>

	  <section class="category-list category-list-home container-full">
			<div class="row row-no-padding ">
				<?php loop_taxonomies(2) ?>

				<?php loop_taxonomies(3) ?>

				<?php loop_taxonomies(4) ?>				
			</div>
		</section>
	</section>

<?php get_template_part('internal-footer'); ?>

<?php get_footer(); ?>