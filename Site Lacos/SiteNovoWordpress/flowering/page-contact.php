<?php /* Template Name: Página de Contato */ ?>

<?php get_header(); ?>

<?php get_template_part('internal-header'); ?>

<section class="intro-products">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1><b><?php the_title(); ?></b></h1>
	      <p><?php echo $post->post_content; ?></p>
				<p>Se preferir, entre em contato com nossa Central de Atendimento: (11) 4097-9449</p>
	    </div>
	  </div>
	</div>
</section>

<section class="container contact">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<form>
				<div class="row">
					<div class="col-md-6">
						<label>Nome*</label>
						<input type="text" name="nome" id="afterinput" />
					</div>
					<div class="col-md-6">
						<label>Empresa*</label>
						<input type="text" name="empresa" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Email*</label>
						<input type="email" name="email" />
					</div>
					<div class="col-md-6">
						<label>Telefone*</label>
						<input type="tel" name="telefone" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label>Mensagem</label>
						<textarea name="mensagem" rows="6"></textarea>
					</div>
				</div>
				<div class="row centered">
					<div class="col-md-12">
						<button type="submit" class="btn btn-call-to-action">Enviar!</button>
						<i class="loader"></i>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<footer class="container-full centered default" role="contentinfo">
	<section class="copyright">
		<p>
      Atendimento: (11) 4097-9449
      <br><br>
      Copyright <?php echo date('Y'); ?> &copy; - <?php bloginfo('name'); ?>. Todos os direitos reservados
    </p>
	</section>
</footer>

<?php get_footer(); ?>
