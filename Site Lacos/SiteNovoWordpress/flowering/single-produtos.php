<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12"><button type="button" class="btn-close" data-dismiss="modal">Close</button></div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
              <?php

              $images = get_field('galeria');

              if( $images ): ?>
                  <div class="slider slider-for">
                      <?php foreach( $images as $image ): ?>
                        <div>
                          <img src="<?php echo $image['sizes']['gallery-item']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" />
                        </div>
                      <?php endforeach; ?>
                  </div>
              <?php endif; ?>

              <?php

              $images = get_field('galeria');

              if( $images ): ?>
                  <div class="slider slider-nav">
                      <?php foreach( $images as $image ): ?>
                        <div>
                          <img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" />
                        </div>
                      <?php endforeach; ?>
                  </div>
              <?php endif; ?>
            </div>
            <div class="col-md-6 col-sm-6">
            	<h1 class="produto-detalhes-titulo"><?php the_title(); ?></h1>
            	<p class="produto-detalhes-subtitulo">
                <?php the_excerpt(); ?>
              </p>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    	<a href="<?php echo home_url(); ?>/solicite-seu-orcamento?PID=<?php echo($post->ID) ?>" class="btn btn-call-to-action btn-comprar">Solicitar Orçamento</a>
                    </div>
                </div>
                <br>
                <h3>Descrição</h3>
                <div class="descricao-produto-detalhes">
                    <p><?php echo $post->post_content; ?></p>

                    <?php if( have_rows('detalhes') ) : ?>
                      <ul>
                        <?php while( have_rows('detalhes') ) : the_row(); ?>
                          <li><?php the_sub_field('detalhe'); ?></li>
                        <?php endwhile; ?>
                      </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<?php if(has_term('', 'nossas-solucoes') && is_single()) : ?>
    <script>
         $('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          centerMode: true,
          asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
          slidesToShow: 4,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: false,
          arrows: false,
          centerMode: false,
          focusOnSelect: true
        });
    </script>
<?php endif ?>
