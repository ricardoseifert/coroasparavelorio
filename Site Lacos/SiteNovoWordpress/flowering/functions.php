<?php

/*------------------------------------*\
	Theme Support
\*------------------------------------*/
add_theme_support( 'post-thumbnails' );
add_image_size( 'gallery-thumb', 150, 150 );
add_image_size( 'gallery-item', 500, 500 );

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action( 'init', 'produtos', 0 );
add_action( 'init', 'build_taxonomies', 0 );
add_action( 'init', 'clientes', 0 );

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

function filter_plugin_updates( $value ) {
    unset( $value->response['advanced-custom-fields-pro/acf.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

// Add Filters
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/
// Register Custom Post Type
function produtos() {

    $labels = array(
        'name'                  => 'Produtos',
        'singular_name'         => 'Produto',
        'menu_name'             => 'Produtos',
        'name_admin_bar'        => 'Produto',
        'parent_item_colon'     => 'Categoria Mãe:',
        'all_items'             => 'Todos Produtos',
        'add_new_item'          => 'Adicionar Novo Produto',
        'add_new'               => 'Adicionar Produto',
        'new_item'              => 'Novo Produto',
        'edit_item'             => 'Editar Produto',
        'update_item'           => 'Atualizar Produto',
        'view_item'             => 'Ver Produto',
        'search_items'          => 'Buscar produtos',
        'not_found'             => 'Nada encontrado',
        'not_found_in_trash'    => 'Nada encontrado na lixeira',
        'featured_image'        => 'Imagem Destaque',
        'set_featured_image'    => 'Aplicar Imagem Destaque',
        'remove_featured_image' => 'Remover Imagem Destaque',
        'use_featured_image'    => 'Usar como Imagem Destaque',
        'insert_into_item'      => 'Inserir no Produto',
        'uploaded_to_this_item' => 'Upload para o Produto',
        'items_list'            => 'Lista de Produtos',
        'items_list_navigation' => 'Navegação pela Lista de Produtos',
        'filter_items_list'     => 'Filtrar Lista de Produtos',
    );
    $args = array(
        'label'                 => 'Produto',
        'description'           => 'Descrição do Produto',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'produtos', $args );
}

function build_taxonomies() {
    global $taxonomy_images_plugin;

    register_taxonomy(
        'nossas-solucoes',
        'produtos',
        array(
            'hierarchical' => true,
            'label' => 'Categorias de Produtos',
            'query_var' => true,
            'rewrite' => true
        )
    );
}

// Register Custom Post Type
function clientes() {

    $labels = array(
        'name'                  => 'Clientes',
        'singular_name'         => 'Cliente',
        'menu_name'             => 'Clientes',
        'name_admin_bar'        => 'Cliente',
        'parent_item_colon'     => 'Categoria Mãe:',
        'all_items'             => 'Todos Clientes',
        'add_new_item'          => 'Adicionar Novo Cliente',
        'add_new'               => 'Adicionar Cliente',
        'new_item'              => 'Novo Cliente',
        'edit_item'             => 'Editar Cliente',
        'update_item'           => 'Atualizar Cliente',
        'view_item'             => 'Ver Cliente',
        'search_items'          => 'Buscar clientes',
        'not_found'             => 'Nada encontrado',
        'not_found_in_trash'    => 'Nada encontrado na lixeira',
        'featured_image'        => 'Imagem Destaque',
        'set_featured_image'    => 'Aplicar Imagem Destaque',
        'remove_featured_image' => 'Remover Imagem Destaque',
        'use_featured_image'    => 'Usar como Imagem Destaque',
        'insert_into_item'      => 'Inserir no Cliente',
        'uploaded_to_this_item' => 'Upload para o Cliente',
        'items_list'            => 'Lista de Clientes',
        'items_list_navigation' => 'Navegação pela Lista de Clientes',
        'filter_items_list'     => 'Filtrar Lista de Clientes',
    );
    $args = array(
        'label'                 => 'Cliente',
        'description'           => 'Descrição do Cliente',
        'labels'                => $labels,
        'supports'              => array( 'title', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 6,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,

    );
    register_post_type( 'clientes', $args );
}
