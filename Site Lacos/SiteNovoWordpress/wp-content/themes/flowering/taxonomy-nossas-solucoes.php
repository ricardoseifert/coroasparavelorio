<?php

  // Retorna a slug de uma taxonomia quando fornecido o id da mesma

  function slugline($id) {

    $termdata =  get_term_by('id', $id, 'nossas-solucoes');

    $slug = $termdata->slug;

    return $slug;

  }



  // Classes auxiliares caso seja categoria mãe ou não

  if( is_tax('nossas-solucoes', array( strval(slugline(2) ), strval(slugline(3) ), strval(slugline(4) ) ) ) ) {

    $intro = 'intro-category';

    $list = 'category-list';

    $container = 'container-full';

    $row = 'row row-no-padding';

  }

    

  else {

    $intro = 'intro-products';

    $list = 'product-list';

    $container = 'container';

    $row = 'row';

  }

?>



<?php get_header(); ?>



<body>



<?php get_template_part('internal-header'); ?>



<section class="container-full">

  <section class="<?php echo $intro; ?>">

    <div class="container">

      <div class="row">

        <div class="col-md-12">

          <h1><b><?php single_cat_title(); ?></b></h1>

          <?php echo category_description(); ?>

        </div>

      </div>

    </div>

  </section>



  <section class="<?php echo $list . ' ' . $container; ?>">

  	<div class="<?php echo $row; ?>">



  		<?php

        // Se for uma das categorias mães, não deverá fazer o loop de produtos, mas sim de categorias filhas

        if( is_tax('nossas-solucoes', array( strval(slugline(2)), strval(slugline(3)), strval(slugline(4)) ) ) ) {



          loop_children(get_queried_object()->term_id);



        }

        // Senão, faça o loop de produtos

        else {



          $args = array(

            'nossas-solucoes' => strval(slugline(get_queried_object()->term_id)),

            'post_type' => 'produtos',

            'post_status' => 'publish',

            'posts_per_page' => -1,

            'caller_get_posts'=> 1

          );



          $my_query = null;

          $my_query = new WP_Query($args);



          // Se a query retornar true, exiba o loop de produtos

          if( $my_query->have_posts() ) {



            while ($my_query->have_posts()) : $my_query->the_post(); ?>



              <article class="col-lg-3 col-md-4 col-sm-6 <?php echo $post->post_name;?>" >

                <div class="products-image">

                    <a href="<?php the_permalink(); ?>" data-toggle="ajaxModal" data-target="#gridSystemModal">

                        <!--data-toggle="ajaxModal" data-target="#gridSystemModal"-->

                        <?php if ( has_post_thumbnail()) : ?>

                            <?php the_post_thumbnail('post-thumbnail', array( 'class' => 'img-responsive')); ?>

                        <?php else : ?>

                            <img src="<?php echo get_template_directory_uri(); ?>/img/products/sample.png" class="img-responsive">

                        <?php endif; ?>

                      </a>

                  </div>

                  <a href="<?php the_permalink(); ?>" class="link-description"  data-toggle="ajaxModal" data-target="#gridSystemModal">

                    <b><?php the_title(); ?></b>

                    <?php the_excerpt(); ?>

                  </a>

              </article>



              <?php

            endwhile;

          }

          else {

            echo "Nenhum post para ser exibido!";

          }

          

        }

      ?>



     </div>

  </section>

</section>



<?php get_template_part('internal-footer'); ?>



<?php get_footer(); ?>