    <nav id="navbar" class="navbar-collapse collapse">

      <ul class="nav navbar-nav">

        <li><a href="<?php echo home_url(); ?>">Início</a></li>

        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias <span class="caret"></span></a>

          <ul class="dropdown-menu">

            <?php link_taxonomy(2); ?>

            <?php link_taxonomy(3); ?>

            <?php link_taxonomy(4); ?>

          </ul>

        </li>

        <li><a href="<?php echo home_url(); ?>/blog">Blog</a></li>

      </ul>

      <ul class="nav navbar-nav navbar-right">

        <li><a href="http://corporativo.lacoscorporativos.com.br/login" class="acesso-empresas btn btn-stroke">Acesso Empresas</a></li>

      </ul>

    </nav>

