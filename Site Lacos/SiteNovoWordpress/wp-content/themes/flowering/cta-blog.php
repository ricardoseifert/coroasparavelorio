<section class="cta-blog container">
  <div class="row">
    <div class="col-md-12 centered">
    	<h2><b>Enquanto isso...</b></h2>
      	<p>Que tal dar aquela conferida no nosso blog? Lá você encontra todo o conteúdo que nossa equipe especializada em recursos humanos preparou especialmente para empresas como a sua!</p>
      	<a href="<?php echo home_url(); ?>/blog" class="btn btn-call-to-action">Me leva pra lá!</a>
    </div>
  </div>
</section>