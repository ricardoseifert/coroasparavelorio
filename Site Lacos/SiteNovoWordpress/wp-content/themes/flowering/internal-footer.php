<footer class="container-full centered others">
  <section class="container centered">
    <div class="row">
      <div class="col-md-12">
        <h2>Precisa de uma solução <b>personalizada?</b></h2>
        <a href="<?php echo get_page_link(31); ?>" class="btn btn-call-to-action">Vamos conversar!</a>
      </div>
    </div>
  </section>
  <section  id="PageDefault"  class="copyright">
    <div class="container">
      <p>
        Atendimento: (11) 4097-9449
        <br><br>
          <a href="https://www.lacoscorporativos.com.br/politicas-de-cancelamento-e-devolucao">Políticas de Cancelamento e de Troca e Devolução</a>
          <br /><br />
        Copyright <?php echo date('Y'); ?> &copy; - <?php bloginfo('name'); ?>. Todos os direitos reservados
      </p>
    </div>
  </section>
</footer>
