// JavaScript Document
$(document).ready( function(){
	$('.clients').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  arrows:false,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  responsive: [
	  	{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 5,
			slidesToScroll: 5,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 800,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});

	$('.features').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  arrows:false,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  responsive: [
	  	{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 1100,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 800,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});

	$('.features-home').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows:true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  adaptiveHeight: true
	});
	$('.features-home').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  active_slider(nextSlide);
	});
	$('.what,.slick-bullet-0, .seta-0').click( function(){
		$('.features-home').slick('slickPrev');
	});
	$('.how,.slick-bullet-1, .seta-1').click( function(){
		$('.features-home').slick('slickNext');
	});

	$('[data-toggle="ajaxModal"]').on('click',
	  function(e) {
	  	$('#ajaxModal').remove();
	  	e.preventDefault();

		var $this = $(this)
		  , $remote = $this.attr('href')
		  , $modal = $('<div class="modal" id="ajaxModal"></div>');

		$('body').append($modal);
		$modal.modal({backdrop: 'static', keyboard: false});
		$modal.load($remote);
	  }
	);

	$(".scroll-down").click(function(){
		$("html, body").stop().animate({scrollTop:$(".category-list-home").offset().top+50},600);
	});

	if($('.subcat-description').length > 1){
		equaliza_alturas();
		$(window).resize( function(){
			if($(window).width() > 974){
				equaliza_alturas();
			}else{
				$('.subcat-description').css('min-height','0px');
			};
		});
	};
});

function active_slider(index_slider){
	var index = index_slider;
	$('.tab-features > span, .tabs-features-bottom > span').removeClass('active');
	$('.slick-bullet-'+index_slider).addClass('active');
	$('.seta-'+index_slider).addClass('active');
}

function equaliza_alturas(){
	if($('.subcat-description').length > 1){
		var altura_box = 0;
		$('.subcat-description').css('min-height','0px');
		$('.subcat-description').each(function(){
			var altura_this = $(this).innerHeight();
			if(altura_this > altura_box){
				altura_box = altura_this;
			};
		})
		$('.subcat-description').css('min-height',altura_box+'px');
	};
};
