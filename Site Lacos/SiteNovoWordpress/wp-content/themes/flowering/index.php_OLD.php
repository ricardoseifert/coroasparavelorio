﻿<?php
	function loop_taxonomies($id) {
		$termdata =  get_term_by('id', $id, 'nossas-solucoes');
		$name = $termdata->name;
		$description = $termdata->description;
		$slug = $termdata->slug;
		$thumbnail = get_term_thumbnail($id);
		$termchildren  = get_term_children($id, 'nossas-solucoes');

		echo '<article class="col-md-4 '.$slug.'" style="background-image:url(\''.substr(str_replace("src=\"", "", explode(" ",$thumbnail)[3]), 0, -1).'\')">';
		echo '<div class="subcat-description">';
		echo '<h2>'.$name.'</h2>';
		echo '<p>'.$description.'</p>';
		/*echo '<div class="tags-home">';
		foreach ( $termchildren as $child ) {
			$term = get_term_by( 'id', $child, 'nossas-solucoes' );
			echo '<span><a href="'.get_term_link($child, 'nossas-solucoes').'">' . $term->name . '</a></span>';
			// get_term_link( $child, 'nossas-solucoes' ); Para inserir link
		}
		echo '</div>';*/
		echo '<a href="'.get_term_link($id).'" class="btn btn-stroke">Veja aqui</a>';
		echo '</div>';
		echo '</article>';
	}
?><?php
function loop_taxonomies0($id) {
		$termdata =  get_term_by('id', $id, 'nossas-solucoes');
		$name = $termdata->name;
		$description = $termdata->description;
		$slug = $termdata->slug;
		$thumbnail = get_term_thumbnail($id);
		$termchildren  = get_term_children($id, 'nossas-solucoes');

		echo '<article class="col-md-4 '.$slug.'" style="background-image:url(\''.substr(str_replace("src=\"", "", explode(" ",$thumbnail)[3]), 0, -1).'\')">';
		echo '<div class="subcat-description">';
		echo '<h2>'.$name.'</h2>';
		echo '<p>'.$description.'</p>';
		/*echo '<div class="tags-home">';
		foreach ( $termchildren as $child ) {
			$term = get_term_by( 'id', $child, 'nossas-solucoes' );
			echo '<span><a href="'.get_term_link($child, 'nossas-solucoes').'">' . $term->name . '</a></span>';
			// get_term_link( $child, 'nossas-solucoes' ); Para inserir link
		}
		echo '</div>';*/
		echo '<a href="http://www.lacoscorporativos.com.br/landing-pages/kit-novo-bebe/" class="btn btn-stroke">Veja aqui</a>';
		echo '</div>';
		echo '</article>';
	}
?>

<?php
function loop_taxonomies1($id) {
		$termdata =  get_term_by('id', $id, 'nossas-solucoes');
		$name = $termdata->name;
		$description = $termdata->description;
		$slug = $termdata->slug;
		$thumbnail = get_term_thumbnail($id);
		$termchildren  = get_term_children($id, 'nossas-solucoes');

		echo '<article class="col-md-4 '.$slug.'" style="background-image:url(\''.substr(str_replace("src=\"", "", explode(" ",$thumbnail)[3]), 0, -1).'\')">';
		echo '<div class="subcat-description">';
		echo '<h2>'.$name.'</h2>';
		echo '<p>'.$description.'</p>';
		/*echo '<div class="tags-home">';
		foreach ( $termchildren as $child ) {
			$term = get_term_by( 'id', $child, 'nossas-solucoes' );
			echo '<span><a href="'.get_term_link($child, 'nossas-solucoes').'">' . $term->name . '</a></span>';
			// get_term_link( $child, 'nossas-solucoes' ); Para inserir link
		}
		echo '</div>';*/
		echo '<a href="http://corporativo.lacoscorporativos.com.br//categorias//coroas-de-flores" class="btn btn-stroke">Veja aqui</a>';
		echo '</div>';
		echo '</article>';
	}
?>



<?php
	function loop_taxonomies2($id) {
		$termdata =  get_term_by('id', $id, 'nossas-solucoes');
		$name = $termdata->name;
		$description = $termdata->description;
		$slug = $termdata->slug;
		$thumbnail = get_term_thumbnail($id);
		$termchildren  = get_term_children($id, 'nossas-solucoes');

		/*echo '<article class="col-md-4 '.$slug.'" style="background-image:url(\''.substr(str_replace("src=\"", "", explode(" ",$thumbnail)[3]), 0, -1).'\')">';
		echo '<div class="subcat-description">';
		echo '<h2>'.$name.'</h2>';
		echo '<p>'.$description.'</p>';*/
		echo '<div class="tags-home">';
		foreach ( $termchildren as $child ) {
			$term = get_term_by( 'id', $child, 'nossas-solucoes' );
			echo '<span><a href="'.get_term_link($child, 'nossas-solucoes').'">' . $term->name . '</a></span>';
			// get_term_link( $child, 'nossas-solucoes' ); Para inserir link
		}
		echo '</div>';
		/*echo '<a href="'.get_term_link($id).'" class="btn btn-stroke">Veja aqui</a>';
		echo '</div>';
		echo '</article>';*/
	}
?>





<?php get_header(); ?>

		<?php get_template_part('main-header'); ?>

		<section class="category-list category-list-home container-full">
			<div class="row row-no-padding ">
				<div class="col-md-12 centered">
					<h2>Permitimos que sua empresa<br> esteja presente <span>Nos Momentos Mais Marcantes!</span></b></h2>
				</div>

				<?php loop_taxonomies(2) ?>

				<?php loop_taxonomies0(3) ?>

				<?php loop_taxonomies1(4) ?>
			</div>
		</section>

		<section class="container-features container-fluid">
			<div class="row">
				<div class="centered">
					<h2>Outras datas <b>importantes</b> para se fazer presente<br> com as pessoas que constroem sua empresa.</h2>
					 <?php loop_taxonomies2(4) ?>
					 <?php loop_taxonomies2(3) ?><br>
					<div class="tags-home-catalogo" align="center">
							<span>
								<a href="http://www.lacoscorporativos.com.br/arquivos/catalogo-comercial-lacos-corporativos.pdf">
								Veja Nosso Catálogo de Brindes
								</a>	
							</span>
								</div>
					<div class="bg">
						<ul class="features container">
							<li>
								<div>
									<!--div><i class="icon-feature icon-truck"></i></div-->
									<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/icon-personalizado.png" />
									<p><strong>Sempre<br >Personalizado</strong></p>
								</div>
							</li>
							<li>
								<div>
									<!--div><i class="icon-feature icon-24"></i></div-->
									<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/icon-entregas.png" />
									<p><strong>Entregas<br>Nacionais</strong></p>
								</div>
							</li>
							<li>
								<div>
									<!--div><i class="icon-feature icon-coins"></i></div-->
									<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/icon-confirmacao.png" />
									<p><strong>Confirmação<br>de Entrega</strong></p>
								</div>
							</li>
							<li>
								<div>
									<!--div><i class="icon-feature icon-chat"></i></div-->
									<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/icon-pedidos-online.png" />
									<p><strong>Portal de<br> Pedidos online</strong> </p>
								</div>
							</li>
							<li>
								<div>
									<!--div><i class="icon-feature icon-chart"></i></div-->
									<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/icon-faturamento.png" />
									<p><strong>Faturamento<br> e Nfe</strong></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="container-full quem-confia">
			<div class="container centered">
				<div class="row">
					<div class="col-md-12">
						<h2>Quem confia em <b>nosso trabalho</b></h2>
						<!--p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Morbi ut pellentesque turpis. Ut ut ultrices nisi. </p-->
						<div class="clients">
							<?php
							$args = array( 'post_type' => 'clientes', 'posts_per_page' => -1 );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
							  echo '<div>';
							  the_post_thumbnail();
							  echo '</div>';
							endwhile;
							?>
						</div>
						<!--p>Se você também acredita que <b>pessoas</b> constroem sua empresa, ajudamos você a demonstrar isso</p-->
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('main-footer'); ?>

<?php get_footer(); ?>
