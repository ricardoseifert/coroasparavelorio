		<?php wp_footer(); ?>
       
        <!-- Google Analytics -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-59636160-1', 'auto');
        ga('send', 'pageview');
        </script>
        <!-- Google Analytics -->
        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?3M1uZ7Va60B5p5xkKLVO3cxjtoZP8m9w";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>
        <!--End of Zendesk Chat Script-->
        <!--Tag RDStation-->
          <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/f60a3c08-2722-4367-a42f-0d763d779f50-loader.js"></script>
        <!--Tag RDStation-->

		<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>

		<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>

		<?php if(is_home() || is_page_template('page-contact.php')) : ?>
			<!-- jQuery Mask -->
      <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mask.min.js" type="text/javascript"></script>

      <!-- Sweet Alert -->
      <script src="<?php echo get_template_directory_uri(); ?>/js/sweetalert.min.js" type="text/javascript"></script>
      <link href="<?php echo get_template_directory_uri(); ?>/js/sweetalert.css" rel="stylesheet" type="text/css">

			<script>
        var get_template_directory_uri = '<?php echo get_template_directory_uri(); ?>',
            get_contato_obrigado = '<?php echo get_page_link(31); ?>';

				var SPMaskBehavior = function (val) {
				  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
				},
				spOptions = {
				  onKeyPress: function(val, e, field, options) {
				      field.mask(SPMaskBehavior.apply({}, arguments), options);
				    }
				};

        function validateName(name){
            var re = /^[a-zA-Z\s]+$/;
            return re.test(name);
        }

        function validateEmail(email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

				$('input[type=tel]').mask(SPMaskBehavior, spOptions).attr('maxlength', 15);

        $('input').focus(function(){
            $(this).siblings('span').remove();
        });

        $('button[type=submit]').click(function(event) {
        	event.preventDefault();

        	var nome = $('input[name=nome]').val(),
	        		email = $('input[name=email]').val(),
	        		telefone = $('input[name=telefone]').val(),
	        		empresa = $('input[name=empresa]').val(),
	        		mensagem = $('textarea[name=mensagem]').val(),
							produto = '',
							quantidade = '',
	            origem = window.location.href,
	            error_message = '',
	            checkName = false,
	            checkEmail = false,
	            checkTelefone = false,
	            checkEmpresa = false;

            <?php if(isset($_GET['origem'])): ?>
                origem = '<?php echo $_GET['origem']; ?>';
						<?php elseif(is_page_template('page-budget.php')): ?>
								origem = 'Página de Orçamento';
								<?php if(isset($_GET['PID'])): ?>
									produto = $('input[name=prod_nome]');
									quantidade = $('input[name=prod_quantidade]');
								<?php endif; ?>
            <?php endif ?>

    				if(validateName(nome)) {
                checkName = true;
            }
            else {
                error_message += '- Nome;\n';
                $('input[name=nome]').parent().append('<span>Revise</span>');
            }

    				if(validateEmail(email)) {
                checkEmail = true;
            }
            else {
                error_message += '- Email;\n';
                $('input[name=email]').parent().append('<span>Revise</span>');
            }

    				if(telefone.length == 14 || telefone.length == 15) {
                checkTelefone = true;
            }
            else {
                error_message += '- Telefone;\n';
                $('input[name=telefone]').parent().append('<span>Revise</span>');
            }

    				if(empresa != '') {
                checkEmpresa = true;
            }

            else {
                error_message += '- Empresa;\n';
                $('input[name=empresa]').parent().append('<span>Revise</span>');
            }


        		if(!checkName || !checkEmail || !checkTelefone || !checkEmpresa) {
        			sweetAlert('Revise os campos a seguir:', error_message, 'error');
        		}
    				else {
    					post_data = {
                'nome': nome,
                'email': email,
                'telefone': telefone,
                'empresa': empresa,
                'mensagem': mensagem,
                'origem': origem,
								'produto': produto,
								'quantidade': quantidade
              }

              $('button[type=submit]').hide();
              $('.loader').css('display', 'block');

              $.ajax({
                url: get_template_directory_uri + '/send-form.php',
                type: 'POST',
                data: post_data,
                success: function(){
                  window.location = '/obrigado-pelo-contato?origem=' + origem;
                },
                error: function(){
                  window.location = '/erro-inesperado?origem=' + origem;
                }
              });
    				}
        });
			</script>
		<?php endif ?>
	</body>
</html>
