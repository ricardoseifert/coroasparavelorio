		<footer class="container-full centered default" role="contentinfo">
			<section class="container centered">
				<div class="row">
					<div class="col-md-12">
					<h2>Precisa de um Projeto <b>Personalizado?</b></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Morbi ut pellentesque turpis. Ut ut ultrices nisi. </p>
					<a href="<?php echo get_page_link(31); ?>" class="btn btn-call-to-action">Vamos conversar?</a></div>
				</div>
			</section>
			<section id="PageDefault"  class="copyright">
				<div class="container">
					<p>					
						Copyright <?php echo date('Y'); ?> &copy; - <?php bloginfo('name'); ?>. Todos os direitos reservados
					</p>
				</div>
			</section>
		</footer>
