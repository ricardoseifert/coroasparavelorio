<?php /* Template Name: Página Padrão */ ?>

<?php get_header(); ?>

<?php get_template_part('internal-header'); ?>

<section class="intro-products">
	<div class="container">
	  <div class="row">
	    <div class="col-md-12">
	      <h1><b><?php the_title(); ?></b></h1>
	    </div>
	  </div>
	</div>
</section>

<section class="container">
	<div class="row default2">
		<div class="col-md-8 col-md-offset-2">
      <?php echo $post->post_content; ?>
		</div>
	</div>
</section>

<footer id="PageDefault" class="container-full centered default teste" role="contentinfo">
	<section class="copyright">
		<p>
      Atendimento: (11) 4097-9449
      <br /><br />
        <a href="https://www.lacoscorporativos.com.br/politicas-de-cancelamento-e-devolucao">Políticas de Cancelamento e de Troca e Devolução</a>
        <br /><br />
            Copyright <?php echo date('Y'); ?> &copy; - <?php bloginfo('name'); ?>. Todos os direitos reservados
    </p>
	</section>
</footer>

<?php get_footer(); ?>
