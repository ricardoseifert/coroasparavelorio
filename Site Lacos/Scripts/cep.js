﻿$(document).ready(function () {

    $("#cep").blur(function () {
        $("#cep").show();
        getEnderecoCadastroCEP($("#cep").val());
    }); 

});

//var url = "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=";
var url = "/Home/GetCEP?cep=";
 
function getEnderecoCadastroCEP(cep) {
    if ($.trim(cep) != "") {
        $.post(url + cep, function (resultadoCEP) {
            if (resultadoCEP.resultado == "1") {
                $("#logradouro").val(unescape(resultadoCEP.tipo_logradouro + ' ' + resultadoCEP.logradouro));
                $("#bairro").val(unescape(resultadoCEP.bairro));
                $('#estado option[text="' + unescape(resultadoCEP.uf) + '"]').attr({ selected: "selected" });

                $("#load_cep").show();

                $("#cidade").get(0).options.length = 0;
                $("#cidade").get(0).options[0] = new Option("-- selecione --", "");

                var uf = unescape(resultadoCEP.uf);
                $("#estado option:contains(" + uf + ")").attr('selected', 'selected');

                var cidade = unescape(resultadoCEP.cidade).toUpperCase().latinize();

                $.post('/Home/GetCidadesUF', { uf: uf, selected: cidade }, function (data) {
                    if (data != undefined && data != "") {
                        for (var i = 0; i < data.length; i++) {
                            $("#cidade").get(0).options[i + 1] = new Option(data[i].Text, data[i].Value, data[i].Selected);
                        }
                    }

                    $("#cidade").find("option:contains('" + cidade + "')").each(function () {
                        if ($(this).text() == cidade) {
                            $(this).attr("selected", "selected");
                        }
                    });
                    $("#load_cidade").hide();
                });
                $("#load_cep").hide();
                $("#numero").focus();
            } else {
                $("#load_cep").hide();
            }
        });

    } else {
        $("#load_cep").hide();
    }
}