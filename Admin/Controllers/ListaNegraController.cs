﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.Linq.Expressions;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using Domain.MetodosExtensao;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers {
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class ListaNegraController : CrudController<ListaNegra> {
        private IPersistentRepository<ListaNegra> listaNegraRepository;
        private AdminModel adminModel;


        public ListaNegraController(ObjectContext context)
            : base(context) {
            listaNegraRepository = new PersistentRepository<ListaNegra>(context);
            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args){

            Expression<Func<ListaNegra, bool>> exp = l => l.ID > 0;

            if (!string.IsNullOrWhiteSpace(Request.QueryString["datainicial"])){
                DateTime dataInicial = new DateTime();
                if (DateTime.TryParse(Request.QueryString["datainicial"],out dataInicial)){
                    exp = exp.And(l => l.DataCadastro >= dataInicial);
                }
            }

            if (!string.IsNullOrWhiteSpace(Request.QueryString["datafinal"])) {
                DateTime dataFinal = new DateTime();
                if (DateTime.TryParse(Request.QueryString["datafinal"], out dataFinal)) {
                    dataFinal = new DateTime(dataFinal.Year,dataFinal.Month,dataFinal.Day).AddDays(1).AddSeconds(-1);
                    exp = exp.And(l => l.DataCadastro <= dataFinal);
                }
            }
            if (!string.IsNullOrWhiteSpace(Request.QueryString["documento"])){
                string documento = Request.QueryString["documento"].Trim().Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
                if (!string.IsNullOrEmpty(documento)) {
                    exp = exp.And(l=>l.Documento.Trim().Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty).Contains(documento));
                }
            }

            var resultado = listaNegraRepository.GetByExpression(exp).OrderBy(p => p.Documento).ToList();
            //var documento = Request.QueryString["documento"];
            
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1) {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }


        [ValidateInput(false)]
        public override ActionResult Edit(ListaNegra listaNegra) {
            listaNegra.Documento = listaNegra.Documento.Replace("_", "").Trim();
            listaNegra.DataCadastro = DateTime.Now;
            listaNegraRepository.Save(listaNegra);
            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

        [HttpGet]
        public override ActionResult Edit(int? id) {
            ListaNegra listaNegra = null;
            if (id.HasValue) {
                listaNegra = listaNegraRepository.Get(id.Value);
            }
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            return View(listaNegra);
        }

        [HttpPost]
        public JsonResult isListaNegra(string documento) {
            var corp = listaNegraRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim()).FirstOrDefault();
            if (corp != null)
                return Json(new { situacao = "SIM", mensagem = corp.Observacao });
            else
                return Json(new { situacao = "NAO" });
        }
    }
}
