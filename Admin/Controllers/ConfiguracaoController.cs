﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;
using System.IO;
using System.Xml;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class ConfiguracaoController : CrudController<Configuracao>
    {
        string Path = "";
        XmlDocument doc = new XmlDocument();

        private IPersistentRepository<Configuracao> configuracaoRepository;

        public ConfiguracaoController(ObjectContext context) : base(context)
        {
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
        }

        public override ActionResult List(params object[] args)
        {
            var configuracao = this.Repository.GetAll().FirstOrDefault();

            if (configuracao != null)
            {
                return RedirectToAction("Edit", new { id = configuracao.ID });
            }
            else
            {
                return RedirectToAction("Edit");
            }
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\corporativo.lacoscorporativos.com.br\\Web.config";

            if (Domain.Core.Configuracoes.HOMOLOGACAO)
                Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Site Lacos\\Web.config";
            
            doc.Load(Path);

            ViewBag.ProdutoRestritoCNPJ = doc.SelectSingleNode("//add[@key='ProdutoRestritoCNPJ']").Attributes[1].Value;
            ViewBag.ProdutoRestritoGrupoEconomico = doc.SelectSingleNode("//add[@key='ProdutoRestritoGrupoEconomico']").Attributes[1].Value;

            return View(this.Repository.GetAll().FirstOrDefault());
        }

        public override ActionResult Edit(Configuracao configuracao)
        {
            Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\corporativo.lacoscorporativos.com.br\\Web.config";

            if (Domain.Core.Configuracoes.HOMOLOGACAO)
                Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Site Lacos\\Web.config";
            doc.Load(Path);

            var AtributeValue = doc.SelectSingleNode("//add[@key='ProdutoRestritoCNPJ']").Attributes[1];
            AtributeValue.Value = Request.Form["ProdutoRestritoCNPJ"];

            var ProdutoRestritoGrupoEconomico = doc.SelectSingleNode("//add[@key='ProdutoRestritoGrupoEconomico']").Attributes[1];
            ProdutoRestritoGrupoEconomico.Value = Request.Form["ProdutoRestritoGrupoEconomico"];

            doc.Save(Path);

            configuracaoRepository.Save(configuracao);
            return RedirectToAction("Edit", new { cod = "SaveSucess" });
        }
    }
}
