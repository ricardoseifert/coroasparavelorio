﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using System.Globalization;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
    public class LoteController : CrudController<Lote>
    {
        #region Variaveis
        private IPersistentRepository<Lote> loteRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<PagamentoGenerico> pagamentogenericoRepository;
        private IPersistentRepository<ItemLote> itemloteRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Repasse_X_ItemLote> repasse_X_ItemLoteRepository;
        private IPersistentRepository<FornecedorGenerico> fornecedorGenericoRepository;
        private IPersistentRepository<PagamentoGenerico> pagamentoGenericoRepository;
        private IPersistentRepository<PagamentoGenerico_X_ItemLote> pagamentoGenerico_X_ItemLoteRepository;
        private IPersistentRepository<CNAB> cnabRepository;
        private IPersistentRepository<CNAB_ItemCnab> cnabItemCnabRepository;
        private IPersistentRepository<HistoricoRetorno> historicoRepository;
        private IPersistentRepository<CNAB_ItemCnab_X_CNAB_StatusDeRetorno> cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository;
        private IPersistentRepository<CNAB_StatusDeRetorno> cNAB_StatusDeRetornoRepository;
        private IPersistentRepository<StatusPagamentoFornecedorGenerico> statusPagamentoFornecedorGenericoRepository;

        private AdminModel adminModel;
        #endregion

        public LoteController(ObjectContext context)
            : base(context)
        {
            adminModel = new AdminModel(context);
            loteRepository = new PersistentRepository<Lote>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            pagamentogenericoRepository = new PersistentRepository<PagamentoGenerico>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            itemloteRepository = new PersistentRepository<ItemLote>(context);
            repasse_X_ItemLoteRepository = new PersistentRepository<Repasse_X_ItemLote>(context);
            fornecedorGenericoRepository = new PersistentRepository<FornecedorGenerico>(context);
            pagamentoGenericoRepository = new PersistentRepository<PagamentoGenerico>(context);
            pagamentoGenerico_X_ItemLoteRepository = new PersistentRepository<PagamentoGenerico_X_ItemLote>(context);
            cnabRepository = new PersistentRepository<CNAB>(context);
            cnabItemCnabRepository = new PersistentRepository<CNAB_ItemCnab>(context);
            historicoRepository = new PersistentRepository<HistoricoRetorno>(context);
            cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository = new PersistentRepository<CNAB_ItemCnab_X_CNAB_StatusDeRetorno>(context);
            cNAB_StatusDeRetornoRepository = new PersistentRepository<CNAB_StatusDeRetorno>(context);
            statusPagamentoFornecedorGenericoRepository = new PersistentRepository<StatusPagamentoFornecedorGenerico>(context);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SalvarRetorno()
        {
            Lote NovoLote = new Lote();

            if (Request.Files.Count > 0)
            {
                var CNAB = Request.Files["CNAB"];
                if (CNAB.ContentLength > 0)
                {
                    string result = new StreamReader(CNAB.InputStream).ReadToEnd();

                    foreach (var Line in result.Split('\r'))
                    {
                        if (Line.Length > 8)
                        {
                            if (Line.Substring(8, 1) == "3")
                            {
                                string NrControle = Line.Substring(74, 20);
                                string DataCreditoStr = Line.Substring(155, 8);
                                DateTime dateDataCreditoStr;
                                string codOcorrencia = Line.Substring(231, 10).Trim();


                                var ItensCNAB = cnabItemCnabRepository.GetByExpression(r => r.NrControle == NrControle).ToList();

                                foreach (var Item in ItensCNAB)
                                {
                                    PagamentoGenerico NovoPagamentoGenerico = null;
                                    Repasse NovoRepasse = null;

                                    if (Item.RepasseID > 0)
                                    {
                                        historicoRepository.Save(new HistoricoRetorno { IdRepasse = Item.RepasseID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.ArquivRetornoImportado, Mensagem = "CNAB: " + Item.CNAB.ID });
                                        NovoRepasse = repasseRepository.Get(Item.RepasseID);

                                    }
                                    else
                                    {
                                        historicoRepository.Save(new HistoricoRetorno { IdPagamentoGenerico = Item.PagamentoGenericoID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.ArquivRetornoImportado, Mensagem = "CNAB: " + Item.CNAB.ID });
                                        NovoPagamentoGenerico = pagamentoGenericoRepository.Get(Item.PagamentoGenericoID);                                        
                                    }

                                    if (DateTime.TryParseExact(DataCreditoStr.Substring(0, 2) + "/" + DataCreditoStr.Substring(2, 2) + "/" + DataCreditoStr.Substring(4, 4), "dd'/'MM'/'yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateDataCreditoStr))
                                    {
                                        Item.DataDeCredito = dateDataCreditoStr;
                                    }

                                    cnabItemCnabRepository.Save(Item);

                                    #region ATUALIZA STATUS DE RETORNO

                                    double parts = 2;
                                    int k = 0;
                                    var output = codOcorrencia.ToLookup(c => Math.Floor(k++ / parts)).Select(e => new String(e.ToArray()));

                                    foreach (var Status in output)
                                    {
                                        var StatusDeRetorno = cNAB_StatusDeRetornoRepository.GetByExpression(r => r.CodString == Status).First();

                                        cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.Save(new CNAB_ItemCnab_X_CNAB_StatusDeRetorno
                                        {
                                            CNAB_StatusDeRetorno = StatusDeRetorno,
                                            CNAB_ItemCnab = Item,
                                            Data = DateTime.Now
                                        });

                                        //if (NovoPagamentoGenerico != null)
                                        //{
                                        //    if (StatusDeRetorno.CodString == "00")
                                        //    {
                                        //        NovoPagamentoGenerico.StatusPagamentoFornecedorGenerico = statusPagamentoFornecedorGenericoRepository.Get(2);
                                        //        pagamentogenericoRepository.Save(NovoPagamentoGenerico);
                                        //        historicoRepository.Save(new HistoricoRetorno { IdPagamentoGenerico = NovoPagamentoGenerico.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.PagamentoEfetuado, Mensagem = "Status do Pagamento alterado para Pagamento Efetuado." });
                                        //        cnabItemCnabRepository.Save(Item);

                                        //    }
                                        //    if (StatusDeRetorno.CodString == "DV")
                                        //    {
                                        //        NovoPagamentoGenerico.StatusPagamentoFornecedorGenerico = statusPagamentoFornecedorGenericoRepository.Get(1);
                                        //        pagamentogenericoRepository.Save(NovoPagamentoGenerico);
                                        //        historicoRepository.Save(new HistoricoRetorno { IdPagamentoGenerico = NovoPagamentoGenerico.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.PagamentoDevolvido, Mensagem = "Status do Pagamento alterado para Pagamento Devolvido." });

                                        //        #region REMOVE ITEM DO LOTE E DO CNAB

                                        //        // REMOVE ITEM DO LOTE
                                        //        pagamentoGenerico_X_ItemLoteRepository.Delete(pagamentoGenerico_X_ItemLoteRepository.GetByExpression(r => r.PagamentoGenerico.ID == NovoPagamentoGenerico.ID).First());

                                        //        // REVEMO HISTORICO DO CNAB
                                        //        var ListaHistoricos = cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.GetByExpression(r => r.CNAB_ItemCnab.ID == Item.ID).ToList();
                                        //        foreach (var Historico in ListaHistoricos)
                                        //        {
                                        //            cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.Delete(cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.GetByExpression(r => r.IDItemCnab == Item.ID).First());
                                        //        }

                                        //        // REMOVE ITEM DO CNAB
                                        //        cnabItemCnabRepository.Delete(cnabItemCnabRepository.Get(Item.ID));

                                        //        #endregion
                                        //    }
                                        //}


                                        if (NovoRepasse != null)
                                        {
                                            if (StatusDeRetorno.CodString == "00")
                                            {
                                                NovoRepasse.StatusID = (int)Repasse.TodosStatus.Pago;
                                                NovoRepasse.DataRepasse = dateDataCreditoStr;
                                                repasseRepository.Save(NovoRepasse);
                                                historicoRepository.Save(new HistoricoRetorno { IdRepasse = NovoRepasse.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.PagamentoEfetuado, Mensagem = "Status do Pagamento alterado para Pagamento Efetuado." });
                                                cnabItemCnabRepository.Save(Item);

                                            }
                                            else
                                            {
                                                if(StatusDeRetorno.CodString != "BD")
                                                {
                                                    NovoRepasse.DataParaPagamento = null;
                                                    NovoRepasse.LoteID = null;

                                                    repasseRepository.Save(NovoRepasse);
                                                    historicoRepository.Save(new HistoricoRetorno { IdRepasse = NovoRepasse.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.PagamentoDevolvido, Mensagem = StatusDeRetorno.Descricao });

                                                    #region REMOVE ITEM DO LOTE E DO CNAB

                                                    try
                                                    {
                                                        // REMOVE ITEM DO LOTE
                                                        repasse_X_ItemLoteRepository.Delete(repasse_X_ItemLoteRepository.GetByExpression(r => r.Repasse.ID == NovoRepasse.ID).First());
                                                    }
                                                    catch{}
                                                    try
                                                    {
                                                        // REVEMO HISTORICO DO CNAB
                                                        var ListaHistoricos = cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.GetByExpression(r => r.CNAB_ItemCnab.ID == Item.ID).ToList();
                                                        foreach (var Historico in ListaHistoricos)
                                                        {
                                                            cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.Delete(cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.GetByExpression(r => r.IDItemCnab == Item.ID).First());
                                                        }
                                                    }
                                                    catch{}
                                                    try
                                                    {
                                                        // REMOVE ITEM DO CNAB
                                                        cnabItemCnabRepository.Delete(cnabItemCnabRepository.Get(Item.ID));
                                                    }
                                                    catch { }

                                                    #endregion
                                                }
                                            }
                                        }
                                    }
#endregion

                                }
                            }
                        }
                    }
                }
            }

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Lote " + NovoLote.ID + " " + NovoLote.Descricao + "foi atualizado com sucesso!" });
        }

        public ActionResult Retorno()
        {
            return View();
        }
        public ActionResult Novo()
        {
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            var Lote = new Lote { Descricao = form["Descricao"].ToUpper(), DataCriacao = DateTime.Now, DataParaPagamento = DateTime.Parse(form["DataParaPagamento"]) };
            loteRepository.Save(Lote);

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Lote " + Lote.ID + " " + Lote.Descricao + "foi criado com sucesso!" });
        }
        public ActionResult ListRepasses(params object[] args)
        {
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<IRepasse> lstIRepasse = new List<IRepasse>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            List<Repasse> LstRepasses = new List<Repasse>();

            var NrPedido = Request.QueryString["NrPedido"];
            if (!string.IsNullOrEmpty(NrPedido))
            {
                int PedidoCORP;
                bool intResultTryParse = int.TryParse(NrPedido, out PedidoCORP);
                
                LstRepasses = repasseRepository.GetByExpression(p => (p.PedidoEmpresa != null && p.PedidoEmpresa.ID == PedidoCORP) || (p.PedidoItem.Pedido != null && p.PedidoItem.Pedido.Numero == NrPedido)).ToList();
            }
            else
            {
                LstRepasses = repasseRepository.GetByExpression(p => (p.PedidoEmpresa != null && p.PedidoEmpresa.DataCriacao >= datainicial && p.PedidoEmpresa.DataCriacao <= datafinal) || (p.PedidoItem.Pedido != null && p.PedidoItem.Pedido.DataCriacao >= datainicial && p.PedidoItem.Pedido.DataCriacao <= datafinal)).ToList();
                LstRepasses = LstRepasses.Where(p => p.StatusID == (int)Repasse.TodosStatus.AguardandoPagamento || p.StatusID == (int)Repasse.TodosStatus.Fechamento || p.StatusID == (int)Repasse.TodosStatus.PagamentoEmProcessamento).ToList();
            }

            

            foreach(var Repasse in LstRepasses)
            {
                Repasse.LstHistorico = historicoRepository.GetByExpression(r => r.IdRepasse == Repasse.ID).ToList();
            }

            var LstPagamentosGenericos = pagamentogenericoRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal && (p.StatusPagamentoFornecedorGenerico.ID != (int)PagamentoGenerico.TodosStatusPagamento.Cancelado && p.StatusPagamentoFornecedorGenerico.ID != (int)PagamentoGenerico.TodosStatusPagamento.Pago)).ToList();

            ViewBag.LstRepasses = LstRepasses;
            ViewBag.LstPagamentosGenericos = LstPagamentosGenericos;

            lstIRepasse.AddRange(LstRepasses);
            lstIRepasse.AddRange(LstPagamentosGenericos);

            lstIRepasse = lstIRepasse.OrderBy(r => r.NomeFornecedor).ToList();

            var fornecedor = Request.QueryString["nome"];
            var favorecido = Request.QueryString["nomefavorecido"];
            var banco = Request.QueryString["banco"];
            var OrigemFornecedor = Request.QueryString["OrigemFornecedor"];
            int fluxo = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["fluxo"]))
            {
                fluxo = int.Parse(Request.QueryString["fluxo"]);
            }

            if (!String.IsNullOrEmpty(fornecedor))
                lstIRepasse = lstIRepasse.Where(r => r.NomeFornecedor.ToLower().Contains(fornecedor.ToLower())).ToList();

            if (!String.IsNullOrEmpty(favorecido))
            {
                lstIRepasse = lstIRepasse.Where(r => r.NomeFavorecido.ToLower().Contains(favorecido.ToLower())).ToList();
            }

            if (!String.IsNullOrEmpty(OrigemFornecedor))
            {
                if (OrigemFornecedor == Domain.Entities.OrigemFornecedor.Floricultura.ToString())
                {
                    lstIRepasse = lstIRepasse.Where(r => r.TipoRepasse == Domain.Entities.OrigemFornecedor.Floricultura).ToList();
                }
                else
                {
                    lstIRepasse = lstIRepasse.Where(r => r.TipoRepasse == Domain.Entities.OrigemFornecedor.Generico).ToList();
                }
            }

            if (!String.IsNullOrEmpty(banco))
                lstIRepasse = lstIRepasse.Where(c => c.BancoFornecedor == banco).ToList();

            if (fluxo > 0)
                lstIRepasse = lstIRepasse.Where(r => r.IDFluxoDePagamento == fluxo).ToList();

            //PAGINAÇÃO
            //var paginaAtual = 0;
            //Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            //if (paginaAtual == 0)
            //    paginaAtual = 1;

            //var totalItens = lstIRepasses.Count;
            //var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            //if (totalPaginas > 1)
            //{
            //    lstIRepasse = lstIRepasse.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            //}
            //ViewBag.PaginaAtual = paginaAtual;
            //ViewBag.TotalItens = totalItens;
            //ViewBag.TotalPaginas = totalPaginas;
            //ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.lstIRepasse = lstIRepasse;

            return View();
        }

        [HttpPost]
        public JsonResult GetLotes()
        {
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var Result in loteRepository.GetAll().ToList())
            {
                result.Add(new SelectListItem { Text = Result.Descricao, Value = Result.ID.ToString() });
            }

            return Json(result);
        }

        public JsonResult AdicionarDataPagamento(string IdPagamentos, string DataPagamento)
        {
            foreach (var Pagamento in IdPagamentos.Split(','))
            {
                string Origem = Pagamento.Split('|')[1];
                int IdPagamento = int.Parse(Pagamento.Split('|')[0]);

                if (Origem == Domain.Entities.OrigemFornecedor.Floricultura.ToString())
                {
                    Repasse Repasse = repasseRepository.GetByExpression(r => r.ID == IdPagamento).FirstOrDefault();
                    Repasse.DataParaPagamento = DateTime.Parse(DataPagamento);
                    repasseRepository.Save(Repasse);
                }
                if (Origem == Domain.Entities.OrigemFornecedor.Generico.ToString())
                {

                }
            }
            return Json("OK");
        }

        public JsonResult GerarRemessa()
        {
            List<string> Pagamentos = new List<string>();

            #region  GERAR LISTA IDS

            // REPASSES

            foreach(var Iten in repasseRepository.GetByExpression(p => p.LoteID == null && p.DataParaPagamento != null && (p.StatusID == (int)Repasse.TodosStatus.AguardandoPagamento || p.StatusID == (int)Repasse.TodosStatus.Fechamento || p.StatusID == (int)Repasse.TodosStatus.PagamentoEmProcessamento)).ToList())
            {
                Pagamentos.Add(Iten.ID + "|" + "Floricultura");
            }

            // PAGAMENTO GENERICO

            #endregion

            #region CRIA LOTE

            Lote Lote = new Lote();
            Lote.DataCriacao = DateTime.Now;
            Lote.DataParaPagamento = DateTime.Now;
            Lote.Descricao = "D" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "H" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            loteRepository.Save(Lote);

            #endregion

            foreach (var Pagamento in Pagamentos)
            {
                string Origem = Pagamento.Split('|')[1];
                int IdPagamento = int.Parse(Pagamento.Split('|')[0]);

                if (Origem == Domain.Entities.OrigemFornecedor.Floricultura.ToString())
                {
                    Repasse Repasse = repasseRepository.GetByExpression(r => r.ID == IdPagamento).FirstOrDefault();
                    Repasse.LoteID = Lote.ID;
                    Repasse.StatusID = (int)Repasse.TodosStatus.PagamentoEmProcessamento;
                    repasseRepository.Save(Repasse);

                    var Fornecedor = Repasse.Fornecedor;

                    ItemLote novoItemLote = new ItemLote();

                    #region ADICIONAR ITEM LOTE

                    // VERIFICAR SE JÁ NÃO HÁ O MESMO FORNECEDOR ADICIONADO PARA ESSE ITEM DE LOTE
                    if (!itemloteRepository.GetByExpression(r => r.Fornecedor.ID == Fornecedor.ID && r.Lote.ID == Lote.ID).Any())
                    {
                        novoItemLote.Lote = Lote;
                        novoItemLote.Fornecedor = Fornecedor;
                        itemloteRepository.Save(novoItemLote);
                    }
                    else
                    {
                        novoItemLote = itemloteRepository.GetByExpression(r => r.Fornecedor.ID == Fornecedor.ID && r.Lote.ID == Lote.ID).First();
                    }

                    #endregion

                    #region ADICIONAR REPASSE X ITEM LOTE

                    Repasse_X_ItemLote novoRepasseItemLote = new Repasse_X_ItemLote();
                    novoRepasseItemLote.Repasse = Repasse;
                    novoRepasseItemLote.ItemLote = novoItemLote;

                    repasse_X_ItemLoteRepository.Save(novoRepasseItemLote);

                    #endregion
                }
                //if (Origem == Domain.Entities.OrigemFornecedor.Generico.ToString())
                //{
                //    var FornecedorGenerico = pagamentogenericoRepository.GetByExpression(r => r.ID == IdPagamento).FirstOrDefault().FornecedorGenerico;
                //    ItemLote novoItemLote = new ItemLote();

                //    #region ADICIONAR ITEM LOTE

                //    // VERIFICAR SE JÁ NÃO HÁ O MESMO FORNECEDOR ADICIONADO PARA ESSE ITEM DE LOTE
                //    if (!itemloteRepository.GetByExpression(r => r.Fornecedor.ID == FornecedorGenerico.ID && r.Lote.ID == Lote.ID).Any())
                //    {
                //        novoItemLote.Lote = Lote;
                //        novoItemLote.FornecedorGenerico = FornecedorGenerico;
                //        itemloteRepository.Save(novoItemLote);
                //    }
                //    else
                //    {
                //        novoItemLote = itemloteRepository.GetByExpression(r => r.Fornecedor.ID == FornecedorGenerico.ID && r.Lote.ID == Lote.ID).First();
                //    }

                //    #endregion

                //    #region ADICIONAR PAGAMENTO GENERICO X ITEM LOTE

                //    PagamentoGenerico PagamentoGenerico = pagamentogenericoRepository.GetByExpression(r => r.ID == IdPagamento).FirstOrDefault();

                //    PagamentoGenerico_X_ItemLote novoPagamentoGenerico_X_ItemLote = new PagamentoGenerico_X_ItemLote();
                //    novoPagamentoGenerico_X_ItemLote.PagamentoGenerico = PagamentoGenerico;
                //    novoPagamentoGenerico_X_ItemLote.ItemLote = novoItemLote;

                //    pagamentoGenerico_X_ItemLoteRepository.Save(novoPagamentoGenerico_X_ItemLote);

                //    #endregion
                //}
            }

            GerarCnab();

            return Json("OK");
        }
        public void GerarCnab()
        {
            var LstLotes = loteRepository.GetByExpression(p => p.CNABs.Count <= 0).ToList().Where(p => p.QtdPagamentos > 0).ToList();

            foreach (var Lote in LstLotes)
            {
                var CNAB = new CNAB();
                CNAB.DataCriacao = DateTime.Now;
                CNAB.Lote = Lote;
                CNAB.Guid = Guid.NewGuid();
                cnabRepository.Save(CNAB);

                CNABText NovoCNABText = new CNABText();

                int NumeroRegistro = 1;

                List<int> LstIdsFornecedoresGEnericos = new List<int>();
                List<int> LstIdsFornecedores = new List<int>();

                LstIdsFornecedoresGEnericos.AddRange(Lote.ItemLotes.Where(r => r.FornecedorGenerico != null).Select(f => f.FornecedorGenerico.ID));
                LstIdsFornecedores.AddRange(Lote.ItemLotes.Where(r => r.Fornecedor != null).Select(f => f.Fornecedor.ID));

                LstIdsFornecedoresGEnericos = LstIdsFornecedoresGEnericos.Distinct().ToList();
                LstIdsFornecedores = LstIdsFornecedores.Distinct().ToList();

                int countPagamentosINseridosCNAB = 0;
                foreach (var ID in LstIdsFornecedoresGEnericos)
                {
                    var LstPagamentos = pagamentoGenerico_X_ItemLoteRepository.GetByExpression(r => r.ItemLote.Lote.ID == Lote.ID && r.PagamentoGenerico.FornecedorGenerico.ID == ID).ToList();

                    // ADICIONAR NO CNAB SOMENTE OS PAGAMENTOS QUE AINDA ESTAO "EM ABERTO". ALGUEM PODE TER DADO BAIXA NO PAGAMENTO, MANUALMENTE.
                    foreach (var PagamentoGenerico in LstPagamentos.Where(r => r.PagamentoGenerico.Status == PagamentoGenerico.TodosStatusPagamento.AguardandoPagamento).ToList())
                    {
                        if (!cnabItemCnabRepository.GetByExpression(r => r.PagamentoGenericoID == PagamentoGenerico.PagamentoGenerico.ID).Any())
                        {
                            cnabItemCnabRepository.Save(new CNAB_ItemCnab { CNAB = CNAB, PagamentoGenericoID = PagamentoGenerico.PagamentoGenerico.ID });
                            countPagamentosINseridosCNAB++;
                            historicoRepository.Save(new HistoricoRetorno { IdPagamentoGenerico = PagamentoGenerico.PagamentoGenerico.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.AdicionadoAoCNAB, Mensagem = "CNAB: " + CNAB.ID });
                        }
                    }

                    //string NrPagamento = "";
                    //NrPagamento =  FornecedorGenerico.ID.ToString("D10") + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss").Replace("-", "").Replace(":", "");

                    NumeroRegistro++;
                }

                foreach (var ID in LstIdsFornecedores)
                {
                    var LstPagamentos = repasse_X_ItemLoteRepository.GetByExpression(r => r.ItemLote.Lote.ID == Lote.ID && r.Repasse.Fornecedor.ID == ID).ToList();

                    // ADICIONAR NO CNAB SOMENTE OS PAGAMENTOS QUE AINDA ESTAO "EM ABERTO". ALGUEM PODE TER DADO BAIXA NO PAGAMENTO, MANUALMENTE.
                    foreach (var Repasse in LstPagamentos.Where(r => r.Repasse.Status == Repasse.TodosStatus.AguardandoPagamento || r.Repasse.Status == Repasse.TodosStatus.Fechamento || r.Repasse.Status == Repasse.TodosStatus.PagamentoEmProcessamento).ToList())
                    {
                        if (!cnabItemCnabRepository.GetByExpression(r => r.RepasseID == Repasse.Repasse.ID).Any())
                        {
                            cnabItemCnabRepository.Save(new CNAB_ItemCnab { CNAB = CNAB, RepasseID = Repasse.Repasse.ID });
                            countPagamentosINseridosCNAB++;
                            historicoRepository.Save(new HistoricoRetorno { IdRepasse = Repasse.Repasse.ID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.AdicionadoAoCNAB, Mensagem = "CNAB: " + CNAB.ID });
                        }

                    }

                    NumeroRegistro++;
                }

                if (countPagamentosINseridosCNAB == 0)
                {
                    cnabRepository.Delete(CNAB);
                }
            }
        }
        public void BaixarCNAB(int CnabID)
        {

            #region VARIAVEIS 

            var CNAB = cnabRepository.Get(CnabID);

            string Download = "";
            string CodBanco = "341";
            int CountLoteHEader = 1;
            int TotalDeLotesFooterArquivo = 0;

            List<KeyValuePair<int, string>> LstIdsFornecedoresGEnericos = new List<KeyValuePair<int, string>>();
            List<KeyValuePair<int, int>> IdFornecedorGenerico_IdPagamentoGenerico = new List<KeyValuePair<int, int>>();

            List<KeyValuePair<int, string>> LstIdsFornecedores = new List<KeyValuePair<int, string>>();
            List<KeyValuePair<int, int>> IdFornecedor_IdRepasse = new List<KeyValuePair<int, int>>();

            #endregion

            List<string> LstMeioPgtDistinct = new List<string>();

            var ListaPagamentos = LoteService.GetRelatorioVendaAtendente(CNAB.LoteID).ToList();

            LstMeioPgtDistinct.AddRange(ListaPagamentos.Select(r => r.Operacao).Distinct());

            foreach (var Operacao in LstMeioPgtDistinct.Distinct().ToList())
            {
                CNABText NovoCNABText = new CNABText();
                NovoCNABText.Linhas = new List<CNABTextLinha>();
                int countQtdItensLote = 0;
                decimal ValorItensLOte = 0;
                int NumeroRegistro = 1;

                #region  ADICIONAR HEADER

                NovoCNABText.Header = new CNABTextLinhaHeader
                {
                    CodigoBanco = "341",
                    CodigoLote = CountLoteHEader.ToString("D4"),
                    TipoRegistro = "1",
                    TipoOperacao = "C",
                    TipoPagamento = "20",
                    FormaPagamento = Operacao,
                    LayoutLote = "040",
                    Brancos1 = " ",
                    Empresa = "2",
                    InscricaoNumero = "12404942000114",
                    IdentificacaoLancamento = "HP01",
                    Brancos2 = "                ",
                    Agencia = "00745",
                    Brancos3 = " ",
                    Conta = "000000060343",
                    Brancos4 = " ",
                    Dac = "1",
                    NomeEmpresa = "ESG COMERCIO ELETRONICO LTDA  ",
                    FinalidadeLote = "000000000000000000000000000001",
                    HistoricoCC = "          ",
                    EnderecoEmpresa = "AV LINS DE VASCONCELOS        ",
                    Numero = "01975",
                    Complemento = "               ",
                    Cidade = "SAO PAULO           ",
                    Cep = "01537001",
                    Estado = "SP",
                    Brancos5 = "        ",
                    Ocorrencias = "          ",
                };

                TotalDeLotesFooterArquivo++;

                #endregion

                #region LOOP LINHAS FORNECEDORED GENERICOS

                //foreach (var IDFornecedorGenerico in LstIdsFornecedoresGEnericos.Where(r => r.Value == MedioDePGT))
                //{
                //    #region GERAR LINHA CNAB

                //    var FornecedorGenerico = fornecedorGenericoRepository.Get(IDFornecedorGenerico.Key);
                //    string SeuNumero = IDFornecedorGenerico.Key + "921129" + CNAB.ID;

                //    // GERAR VALOR DO PAGAMENTO
                //    decimal ValorPagamento = 0;
                //    foreach (var FornecedorGenericoID in IdFornecedorGenerico_IdPagamentoGenerico.Where(p => p.Key == IDFornecedorGenerico.Key).ToList().Select(r => r.Key).ToList().Distinct().ToList())
                //    {
                //        foreach (var IDPagamentoGenerico in IdFornecedorGenerico_IdPagamentoGenerico.Where(r => r.Key == FornecedorGenericoID).Select(p => p.Value).ToList())
                //        {
                //            var PagamentoGenerico = pagamentogenericoRepository.Get(IDPagamentoGenerico);
                //            ValorPagamento += PagamentoGenerico.Valor;

                //            var CnabItem = cnabItemCnabRepository.GetByExpression(r => r.PagamentoGenericoID == IDPagamentoGenerico).First();
                //            CnabItem.NrControle = SeuNumero.PadLeft(20, '0');
                //            cnabItemCnabRepository.Save(CnabItem);
                //        }
                //    }

                //    string ValorFormatado = ValorPagamento.ToString().Replace(",", "").Replace(".", "");
                //    ValorItensLOte = ValorItensLOte + ValorPagamento;

                //    CNABTextLinha Linha = new CNABTextLinha();
                //    Linha.CodigoBanco = CodBanco;
                //    Linha.CodigoLote = 1.ToString("D4");
                //    Linha.TipoRegistro = "3";
                //    Linha.NumeroRegistro = NumeroRegistro.ToString("D5");
                //    Linha.Segmento = "A";
                //    Linha.TipoDeSegmento = "000";
                //    Linha.Camara = "000";
                //    Linha.NrBancoFavorecido = int.Parse(FornecedorGenerico.FornecedorGenericoConta.First().FornecedorGenericoBanco.NrBanco).ToString("D3");
                //    Linha.Agencia = int.Parse(FornecedorGenerico.FornecedorGenericoConta.First().Agencia).ToString("D5");
                //    Linha.Conta = int.Parse(FornecedorGenerico.FornecedorGenericoConta.First().Conta).ToString("D12");
                //    Linha.ContaDigito = int.Parse(FornecedorGenerico.FornecedorGenericoConta.First().ContaDigito).ToString();
                //    Linha.NomeFavorecido = FornecedorGenerico.FornecedorGenericoConta.First().Favorecido.TrimEnd().TrimStart().ToUpper().PadRight(30, ' ').Substring(0, 30);
                //    Linha.SeuNumero = SeuNumero.PadLeft(20, '0');
                //    Linha.DataPagamento = CNAB.Lote.DataParaPagamento.ToString("ddMMyyy");
                //    Linha.MoedaTipo = "REA";
                //    Linha.CodigoISPB = "00000000";
                //    Linha.Zeros = "0000000";
                //    Linha.Valor = int.Parse(ValorFormatado).ToString("D15");
                //    Linha.NossoNumero = "000000000000000";
                //    Linha.Brancos1 = "     ";
                //    Linha.DataEfetiva = "00000000";
                //    Linha.ValorEfetivo = "000000000000000";
                //    Linha.FinalidadeDetalhe = "                  ";
                //    Linha.Brancos2 = "  ";
                //    Linha.NumDocumento = "000000";
                //    Linha.DocumentoForn = FornecedorGenerico.FornecedorGenericoConta.First().FornecedorGenerico.Documento.PadLeft(14, '0');
                //    Linha.FinalidadeDocStatusFuncionario = "07";
                //    Linha.FinalidadeTed = "00005";
                //    Linha.Brancos3 = "     ";
                //    Linha.Aviso = "0";
                //    Linha.Ocorrencias = "          ";

                //    NovoCNABText.Linhas.Add(Linha);
                //    NumeroRegistro++;

                //    #endregion
                //    countQtdItensLote++;
                //}

                #endregion

                #region LOOP LINHAS FORNECEDORES

                foreach (var Pagamento in ListaPagamentos.Where(r => r.Operacao == Operacao).ToList())
                {
                    #region GERAR LINHA CNAB
                   
                    var Fornecedor = fornecedorRepository.Get((int)Pagamento.FornecedorID);

                    // OBTEM NR DO BANCO DO FORNECEDOR
                    string[] splitNrBanco = Fornecedor.BancoPrimario.Split('|');

                    //OBTEM A AGENCIA DO FORNECEDOR
                    string Agencia = "";
                    if (Fornecedor.AgenciaPrimario != null)
                    {
                        var Result = 0;
                        bool successfullyParsed = int.TryParse(Fornecedor.AgenciaPrimario.Replace("-", ""), out Result);
                        if (successfullyParsed)
                        {
                            Agencia = Result.ToString("D4");
                            if (Agencia.Length > 4)
                            {
                                Agencia = Agencia.Substring(0, 4);
                            }
                        }
                        else
                        {
                            Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - Agência contém caracteres não numéricos. Agência: " + Fornecedor.AgenciaPrimario + ".");
                            Response.End();
                            return;
                        }
                    }
                    else
                    {
                        Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - Agência Não Informada. Conta: " + Fornecedor.ContaPrimario + ". Adicione uma agência.");
                        Response.End();
                        return;
                    }
                   

                    //OBTEM O NR DA CONTA E O DIGITO
                    string[] splitConta = null;
                    if (Fornecedor.ContaPrimario.Replace(" ", "").Contains("-"))
                    {
                        splitConta = Fornecedor.ContaPrimario.Replace(" ", "").Replace(".", "").Split('-');
                    }
                    else
                    {
                        Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - Não foi possível obter o dígito da conta informado. Conta: " + Fornecedor.ContaPrimario + ". Adicione um dígito seguinte do caracter - .");
                        Response.End();
                        return;
                    }

                    Random generator = new Random();
                    string SeuNumero = (int)Pagamento.FornecedorID + CNAB.ID + DateTime.Now.ToString("MMddHHmmssFFF");

                    // ATUALIZA SEU NUMERO
                    foreach (var RepasseID in Pagamento.Repasses.Split(',').ToList())
                    {
                        int IdRepasse = int.Parse(RepasseID);
                        var CnabItem = cnabItemCnabRepository.GetByExpression(r => r.RepasseID == IdRepasse).First();

                        if (string.IsNullOrEmpty(CnabItem.NrControle))
                        {
                            CnabItem.NrControle = SeuNumero.PadLeft(20, '0');
                            cnabItemCnabRepository.Save(CnabItem);
                        }
                        else
                        {
                            SeuNumero = CnabItem.NrControle;
                        }
                       
                    }

                    // GERAR VALOR DO PAGAMENTO
                    decimal ValorPagamento = (decimal)Pagamento.Valor;

                    string ValorFormatado = ValorPagamento.ToString().Replace(",", "").Replace(".", "");
                    ValorItensLOte = ValorItensLOte + ValorPagamento;

                    DateTime DataPgt = new DateTime();
                    DataPgt = (DateTime)Pagamento.DataParaPagamento;

                    CNABTextLinha Linha = new CNABTextLinha();

                    Linha.CodigoBanco = CodBanco;
                    Linha.CodigoLote = CountLoteHEader.ToString("D4");
                    Linha.TipoRegistro = "3";
                    Linha.NumeroRegistro = NumeroRegistro.ToString("D5");
                    Linha.Segmento = "A";
                    Linha.TipoDeSegmento = "000";
                    Linha.Camara = "000";
                    Linha.NrBancoFavorecido = int.Parse(splitNrBanco.Last().Replace(" ", "")).ToString("D3");
                    Linha.Agencia = int.Parse(Agencia).ToString("D5");

                    if (splitConta == null)
                    {
                        Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - Não há um número de conta preenchido");
                        Response.End();
                        return;
                    }

                    Linha.Conta = int.Parse(splitConta.First()).ToString("D12");
                    Linha.ContaDigito = splitConta.Last();

                    if(Fornecedor.FavorecidoPrimario == null)
                    {
                        Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - Não há um favorecido primário cadastrado");
                        Response.End();
                        return;
                    }

                    Linha.NomeFavorecido = Fornecedor.FavorecidoPrimario.TrimEnd().TrimStart().ToUpper().PadRight(30, ' ').Substring(0, 30);
                    Linha.SeuNumero = SeuNumero.PadLeft(20, '0');
                    Linha.DataPagamento = DataPgt.ToString("ddMMyyy");
                    Linha.MoedaTipo = "REA";
                    Linha.CodigoISPB = "00000000";
                    Linha.Zeros = "0000000";
                    Linha.Valor = int.Parse(ValorFormatado).ToString("D15");
                    Linha.NossoNumero = "000000000000000";
                    Linha.Brancos1 = "     ";
                    Linha.DataEfetiva = "00000000";
                    Linha.ValorEfetivo = "000000000000000";
                    Linha.FinalidadeDetalhe = "                  ";
                    Linha.Brancos2 = "  ";
                    Linha.NumDocumento = "000000";

                    if (Fornecedor.CNPJ == null && Fornecedor.CPF == null)
                    {
                        Response.Write("ERRO: Fornecedor " + Fornecedor.Nome + " | ID " + Fornecedor.ID + " - CPF e/ou CNPJ não cadastrados.");
                        Response.End();
                        return;
                    }

                    Linha.DocumentoForn = (Fornecedor.CNPJ != null) ? Fornecedor.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").PadLeft(14, '0') : Fornecedor.CPF.Replace(".", "").Replace("-", "").Replace("/", "").PadLeft(14, '0');
                    Linha.FinalidadeDocStatusFuncionario = "07";
                    Linha.FinalidadeTed = "00005";
                    Linha.Brancos3 = "     ";
                    Linha.Aviso = "0";
                    Linha.Ocorrencias = "          ";

                    NovoCNABText.Linhas.Add(Linha);
                    NumeroRegistro++;

                    #endregion
                    countQtdItensLote++;
                }

                #endregion

                #region  ADICIONAR FOOTER

                countQtdItensLote = countQtdItensLote + 2;

                NovoCNABText.Footer = new CNABTextLinhaFooter
                {
                    CodigoBanco = "341",
                    CodigoLote = CountLoteHEader.ToString("D4"),
                    TipoRegistro = "5",
                    Brancos1 = "         ",
                    TotalQtdeRegistros = countQtdItensLote.ToString("D6"),
                    TotalValorPagtos = ValorItensLOte.ToString().Replace(",", "").Replace(".", "").PadLeft(18, '0'),
                    Zeros = "000000000000000000",
                    Brancos2 = "                                                                                                                                                                           ",
                    Ocorrencias = "          "
                };

                CountLoteHEader++;

                #endregion

                Download += NovoCNABText.Gerar();

            }

            #region GERAR HEADER E FOOTER DO ARQUIVO

            Download = new CNABText().GerarHeaderArquivo() + Download;
            Download = Download + new CNABText().GerarFooterArquivo(TotalDeLotesFooterArquivo, Download.Split('\n').Length);

            #endregion

            #region GERACAO E DOWNLOAD TXT

            Download = Domain.Core.Funcoes.AcertaAcentos(Download);

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + CnabID + ".txt");
            Response.AddHeader("content-type", "text/plain");

            using (StreamWriter writer = new StreamWriter(Response.OutputStream))
            {
                writer.WriteLine(Download);
            }

            Response.End();

            #endregion
        }

        public ActionResult Visualizar(int id)
        {
            var lote = loteRepository.Get(id);
            ViewBag.CNABs = lote.CNABs.ToList();


            #region PREENCHER LISTA DE HISTORICO  E CNAB

            foreach (var item in lote.ItemLotes.Where(r => r.Repasse_X_ItemLote.Count > 0).ToList())
            {
                foreach (var Item in item.Repasse_X_ItemLote)
                {
                    Item.Repasse.LstHistorico = historicoRepository.GetByExpression(r => r.IdRepasse == Item.Repasse.ID).ToList();
                    Item.Repasse.LstCNAB = cnabRepository.GetByExpression(r => r.CNAB_ItemCnab.Where(p => p.RepasseID == Item.Repasse.ID).Any()).ToList();

                    if (Item.Repasse.LstCNAB.Count > 0)
                    {
                        Item.Repasse.NrControle = cnabItemCnabRepository.GetByExpression(r => r.RepasseID == Item.Repasse.ID).First().NrControle;
                    }
                }
            }

            foreach (var item in lote.ItemLotes.Where(r => r.PagamentoGenerico_X_ItemLote.Count > 0).ToList())
            {
                foreach (var Item in item.PagamentoGenerico_X_ItemLote)
                {
                    Item.PagamentoGenerico.LstHistorico = historicoRepository.GetByExpression(r => r.IdPagamentoGenerico == Item.PagamentoGenerico.ID).ToList();
                    Item.PagamentoGenerico.LstCNAB = cnabRepository.GetByExpression(r => r.CNAB_ItemCnab.Where(p => p.PagamentoGenericoID == Item.PagamentoGenerico.ID).Any()).ToList();

                    if (Item.PagamentoGenerico.LstCNAB.Count > 0)
                    {
                        Item.PagamentoGenerico.NrControle = cnabItemCnabRepository.GetByExpression(r => r.PagamentoGenericoID == Item.PagamentoGenerico.ID).First().NrControle;
                    }
                }
            }

            #endregion

            return View(lote);
        }

        public JsonResult RemoverPagamentoRepasse(int ID, string Tipo, int LoteID)
        {
            var Lote = loteRepository.Get(LoteID);

            if (Tipo == "PagamentoGenerico")
            {
                var ItemLote = pagamentoGenerico_X_ItemLoteRepository.GetByExpression(r => r.PagamentoGenerico.ID == ID && r.ItemLote.Lote.ID == LoteID).First();
                pagamentoGenerico_X_ItemLoteRepository.Delete(ItemLote);
            }
            else
            {
                var ItemLote = repasse_X_ItemLoteRepository.GetByExpression(r => r.Repasse.ID == ID && r.ItemLote.Lote.ID == LoteID).First();

                Repasse repasse = ItemLote.Repasse;
                repasse.DataParaPagamento = null;
                repasse.LoteID = null;
                repasse.StatusID = (int)Repasse.TodosStatus.AguardandoPagamento;
                repasseRepository.Save(repasse);

                repasse_X_ItemLoteRepository.Delete(ItemLote);
            }

            return Json("OK");
        }

        public JsonResult RemoverDataParaPagamento(int RepasseID)
        {
            var Repasse = repasseRepository.Get(RepasseID);
            Repasse.DataParaPagamento = null;

            repasseRepository.Save(Repasse);

            return Json("OK");
        }

        public JsonResult RemoverCNAB(int CnabID)
        {
            var CNAB = cnabRepository.Get(CnabID);

            foreach (var CnabItemID in CNAB.CNAB_ItemCnab.Select(r => r.ID).ToList())
            {
                var CNABItem = cnabItemCnabRepository.Get(CnabItemID);

                if (CNABItem.RepasseID > 0)
                {
                    historicoRepository.Save(new HistoricoRetorno { IdRepasse = CNABItem.RepasseID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.RemovidoAoCNAB, Mensagem = "CNAB: " + CNAB.ID });

                    Repasse repasse = repasseRepository.Get(CNABItem.RepasseID);
                    repasse.DataParaPagamento = null;
                    repasse.LoteID = null;
                    repasse.StatusID = (int)Repasse.TodosStatus.AguardandoPagamento;
                    repasseRepository.Save(repasse);
                }
                else
                {
                    historicoRepository.Save(new HistoricoRetorno { IdPagamentoGenerico = CNABItem.PagamentoGenericoID, DataRetorno = DateTime.Now, Codigo = (int)Domain.Entities.HistoricoRetorno.TodosStatusProcessamento.RemovidoAoCNAB, Mensagem = "CNAB: " + CNAB.ID });
                }

                var LstStatus = CNABItem.CNAB_ItemCnab_X_CNAB_StatusDeRetorno.ToList();

                foreach (var Status in LstStatus)
                {
                    cNAB_ItemCnab_X_CNAB_StatusDeRetornoRepository.Delete(Status);
                }

                cnabItemCnabRepository.Delete(CNABItem);
            }

            var LoteID = CNAB.Lote.ID;

            cnabRepository.Delete(CNAB);

            var Lote = loteRepository.Get(LoteID);
            var LstLoteCopyItens = Lote.ItemLotes.ToList();

            foreach (var item in LstLoteCopyItens)
            {

                foreach (var ItenRepasseItemLote in repasse_X_ItemLoteRepository.GetByExpression(r => r.IdItemLote == item.ID).ToList())
                {
                    repasse_X_ItemLoteRepository.Delete(ItenRepasseItemLote);
                }
                
                itemloteRepository.Delete(item);
            }

            loteRepository.Delete(Lote);

            return Json("OK");
        }

       

        public override ActionResult List(params object[] args)
        {
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<Lote> lstLote = new List<Lote>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            lstLote = loteRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal).OrderByDescending(p => p.DataParaPagamento).ToList();

            // REMOVE LOTES SEM PAGAMENTOS
            lstLote = lstLote.Where(p => p.QtdPagamentos > 0 || p.QtdPagamentosPagos > 0).ToList();

            // CONTA QUANTIDADE DE REPASSES PENDENTES DE PAGAMENTO
            ViewBag.PagamentoParaGerar = repasseRepository.GetByExpression(p => p.LoteID == null && p.DataParaPagamento != null && ( p.StatusID == (int)Repasse.TodosStatus.AguardandoPagamento || p.StatusID == (int)Repasse.TodosStatus.Fechamento || p.StatusID == (int)Repasse.TodosStatus.PagamentoEmProcessamento) ).ToList().Count;

            var nome = Request.QueryString["nome"];
            var banco = Request.QueryString["banco"];
            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["numero"]))
            {
                id = int.Parse(Request.QueryString["numero"]);
            }

            if (id > 0)
                lstLote = lstLote.Where(c => c.ID == id).ToList();
            else
            {
                if (!String.IsNullOrEmpty(nome))
                    lstLote = lstLote.Where(c => c.Descricao.ToLower().Contains(nome.ToLower())).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstLote.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lstLote = lstLote.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(lstLote);
        }

        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var lote = loteRepository.Get(id);

            if (!string.IsNullOrEmpty(form["DataParaPagamento"]))
            {
                lote.DataParaPagamento = DateTime.Parse(form["DataParaPagamento"]);
            }

            loteRepository.Save(lote);

            Response.Redirect("/Lote/Visualizar/" + lote.ID);

            return null;
        }
    }
}