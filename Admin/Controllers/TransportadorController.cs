﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.IO;
using System.Collections.Specialized;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    //=================================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos|Financeiro", "/Home/AccessDenied")]
    public class TransportadorController : Controller
    {
        #region Variáveis
        //-----------------------------------------------------------------------------
        private IPersistentRepository<Transportador> transportadorRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;

        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public TransportadorController(ObjectContext context)
        {
            transportadorRepository = new PersistentRepository<Transportador>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }
        //-----------------------------------------------------------------------------

        #endregion
        [HttpGet]
        public ActionResult List(int? tipo)
        {
            List<Transportador> Transportadores = null;
            string _titulo = null;

            Transportadores = transportadorRepository.GetAll().OrderBy(p => p.Nome).ToList();

            if (!String.IsNullOrEmpty(Request.QueryString["titulo"]))
            {
                _titulo = Request.QueryString["titulo"];
                if (!String.IsNullOrEmpty(_titulo))
                    Transportadores = Transportadores.Where(c => c.Nome.ToLower().Contains(_titulo.ToLower()) || c.Nome.ToLower().Contains(_titulo.ToLower())).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = Transportadores.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                Transportadores = Transportadores.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            
            return View(Transportadores);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Transportador transportador = null;
            if (id.HasValue)
            {
                transportador = transportadorRepository.Get(id.Value);

                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == transportador.EstadoID).OrderBy(e => e.Nome);
            }
            else
            {
                ViewBag.Origens = new List<OrigemSite>();
                ViewBag.TodasOrigens = new List<OrigemSite>();

                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = new List<Cidade>();
            }

            return View(transportador);
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Transportador produto = null;
            if (id > 0)
            {
                produto = transportadorRepository.Get(id);
                transportadorRepository.Delete(produto);
                //Deletar fotos
                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "DeleteFailure" });
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Transportador transportador)
        {
            if (transportador.RazaoSocial == null)
                transportador.RazaoSocial = "";

            transportador.Documento = transportador.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim();

            if (transportador.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica)
            {
                transportador.RazaoSocial = "";
                transportador.IE = "";
            }

            transportadorRepository.Save(transportador);

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }
    }
    //=================================================================================
}
