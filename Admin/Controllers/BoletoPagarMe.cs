﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.IO;
using System.Globalization;
using Domain.Repositories;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using Domain.Factories;
using PagarMe;
using System.Text;

namespace Admin.Controllers
{
    public class BoletoPagarMeController : CrudController<BoletoPagarMe>
    {

        private IPersistentRepository<BoletoPagarMe> boletoRepository;
        //private IPersistentRepository<Faturamento> faturamentoRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private PedidoRepository pedidoRepository;
        private AdminModel adminModel;

        public BoletoPagarMeController(ObjectContext context)
            : base(context)
        {
            boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            //faturamentoRepository = new PersistentRepository<Faturamento>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            pedidoRepository = new PedidoRepository(context);
            adminModel = new AdminModel(context);
        }


        [HttpPost]
        public ActionResult Salva(int id, string observacao)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                boleto.Observacao = observacao;
                boletoRepository.Save(boleto);

                return Json(new { sucesso = true, observacao = boleto.Observacao.Replace("\n", "<br>") });
            }
            else
            {
                return Json(new { sucesso = false, mensagem = "Erro de processamento" });
            }
        }
        //----------------------------------------------------------------------
        public List<BoletoPagarMe> ListaBoletosPagarme()
        {
            var resultado = new List<BoletoPagarMe>();
            var boletoID = 0;
            Int32.TryParse(Request.QueryString["boletoID"], out boletoID);

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);
            datafinal = datafinal.AddDays(1).AddMilliseconds(-1);

            if (boletoID > 0)
                resultado = boletoRepository.GetByExpression(c => c.ID == boletoID).ToList();
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["datainicial"]) && !string.IsNullOrEmpty(Request.QueryString["datafinal"]))
                {
                    resultado = boletoRepository.GetByExpression(r => r.DataVencimento >= datainicial && r.DataVencimento <= datafinal).ToList();
                }
                else
                {
                    resultado = boletoRepository.GetByExpression(r => r.DataVencimento >= DateTime.Today && r.DataVencimento.Value.Day == DateTime.Today.Day && r.DataVencimento.Value.Month == DateTime.Today.Month && r.DataVencimento.Value.Year == DateTime.Today.Year).ToList();
                }
            }

            var datainicialcej = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialcej"], out datainicialcej);

            var datafinalcej = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalcej"], out datafinalcej);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var nossonumero = Request.QueryString["nossonumero"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var statusprocessamento = 0;
            Int32.TryParse(Request.QueryString["statusprocessamento"], out statusprocessamento);

            var origem = 0;
            Int32.TryParse(Request.QueryString["origem"], out origem);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statuspagamentoped = 0;
            Int32.TryParse(Request.QueryString["statuspagamentoped"], out statuspagamentoped);

            var statusretorno = 0;
            Int32.TryParse(Request.QueryString["statusretorno"], out statusretorno);

            if (datainicialcej != new DateTime())
                resultado = resultado.Where(c => (c.Pedido != null) ? Domain.Core.Funcoes.TruncateTime(c.Pedido.DataCriacao) >= datainicialcej : c.Faturamento.PedidoEmpresas.Count(f => Domain.Core.Funcoes.TruncateTime(f.DataCriacao) >= datainicialcej) > 0).ToList();

            if (datafinalcej != new DateTime())
                resultado = resultado.Where(c => (c.Pedido != null) ? Domain.Core.Funcoes.TruncateTime(c.Pedido.DataCriacao) <= datafinalcej.AddDays(1).AddMilliseconds(-1) : c.Faturamento.PedidoEmpresas.Count(f => Domain.Core.Funcoes.TruncateTime(f.DataCriacao) <= datafinalcej.AddDays(1).AddMilliseconds(-1)) > 0).ToList();

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.SacadoNome.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Pedido.Cliente.Email.ToLower().Contains(email.ToLower())).ToList();

            if (!String.IsNullOrEmpty(numero))
            {
                var id = 0;
                Int32.TryParse(numero, out id);
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Numero.Contains(numero) : c.Faturamento.PedidoEmpresas.Count(e => e.ID == id) > 0).ToList();
            }
            if (!String.IsNullOrEmpty(nossonumero))
            {
                resultado = boletoRepository.GetByExpression(c => c.TransactionID == nossonumero).ToList();
            }
            if (clienteID > 0)
                resultado = resultado.Where(c => (c.Pedido != null) ? c.Pedido.ClienteID == clienteID : c.Faturamento.EmpresaID == clienteID).ToList();

            if (statusprocessamento > 0)
                resultado = resultado.Where(c => c.StatusProcessamentoID == statusprocessamento).ToList();

            //if (statusretorno > 0)
            //    resultado = resultado.Where(c => c.RetornoID == statusretorno).ToList();

            if (statuspagamento > 0)
                resultado = resultado.Where(c => c.StatusPagamentoID == statuspagamento).ToList();

            if (statuspagamentoped > 0)
            {
                resultado = resultado.Where(c => (c.PedidoID.HasValue && c.Pedido.StatusPagamentoID == statuspagamentoped) || (c.FaturamentoID.HasValue && c.Faturamento.StatusPagamentoID == statuspagamentoped)).ToList();
            }

            if (origem == 1)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite != "LAÇOS FLORES").ToList();
            else if (origem == 2)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite == "LAÇOS FLORES").ToList();
            else if (origem == 3)
                resultado = resultado.Where(c => c.FaturamentoID.HasValue).ToList();

            resultado = resultado.Where(r => r.DataVencimento != null).ToList();

            resultado = resultado.OrderByDescending(c => c.ID).OrderBy(p => p.DataVencimento.Value.Day).OrderBy(p => p.DataVencimento.Value.Month).OrderBy(p => p.DataVencimento.Value.Year).ToList();

            return resultado;
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public void Exportar()
        {
            ExportExcel(ListaBoletosPagarme());
        }
        //----------------------------------------------------------------------
        public void ExportExcel(List<BoletoPagarMe> LstObjects)
        {
            string Csv = string.Empty;

            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms, Encoding.UTF8);

            string TBL = string.Empty;

            TBL += "<table>";
            TBL += "<tr>";
            TBL += "<td>TRANSACAO</td>";
            TBL += "<td>ID</td>";
            TBL += "<td>PEDIDO</td>";
            TBL += "<td>SACADO</td>";
            TBL += "<td>DOCUMENTO</td>";
            TBL += "<td>VALOR</td>";
            TBL += "<td>TELEFONE</td>";
            TBL += "<td>CELULAR</td>";
            TBL += "<td>OBS</td>";
            TBL += "<td>DATA DE VENC</td>";
            TBL += "<td>DATA DE CRIACAO</td>";
            TBL += "<td>STATUS</td>";
            TBL += "</tr>";

            foreach (var iten in LstObjects)
            {
                TBL += "<tr>";
                TBL += "<td>" + iten.TransactionID + "</td>";
                TBL += "<td>" + iten.ID + "</td>";
                TBL += "<td>" + iten.Pedido.Numero + "</td>";
                TBL += "<td>" + iten.SacadoNome + "</td>";
                TBL += "<td>" + iten.SacadoDocumento + "</td>";
                TBL += "<td>" + iten.Valor + "</td>";
                TBL += "<td>" + iten.Pedido.Cliente.TelefoneContato + "</td>";
                TBL += "<td>" + iten.Pedido.Cliente.CelularContato + "</td>";
                TBL += "<td>" + iten.Observacao + "</td>";
                TBL += "<td>" + iten.DataVencimento + "</td>";
                TBL += "<td>" + iten.DataCriacao + "</td>";
                TBL += "<td>" + Domain.Helpers.EnumHelper.GetDescription(iten.StatusPagamento) + "</td>";
                TBL += "</tr>";
            }

            TBL += "</table>";
            tw.Write(TBL);

            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();


            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", "attachment; filename=Export.xls");
            Response.BinaryWrite(bytes);
            Response.End();

        }

        public override ActionResult List(params object[] args)
        {
            //PagarMeService.DefaultApiKey = "ak_test_Jv9u8k9F0NLJYvzQOsHbYprJs1jQ4J";
            //var transaction = PagarMeService.GetDefaultService().Transactions.Find("1655722");
            //transaction.Status = TransactionStatus.Paid;
            //transaction.Save();

            var resultado = ListaBoletosPagarme();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 40);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * 40).Take(40).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros. Total " + resultado.Sum(c => c.Valor).ToString("C2");
             
            return View(resultado);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(BoletoPagarMe boleto)
        {
            var pedido = pedidoRepository.Get((int)boleto.PedidoID);

            #region ALTERAR BOLETO PAGAR ME

            BoletoPagarMeFactory.Alterar(boleto, Remessa.Empresa.CPV);

            #endregion

            return RedirectToAction("List", new { cod = "SaveSucess", boletoID = boleto.ID });
        }

        public JsonResult AlterarVencimentoBoletoPedido(int id, string data)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = pedido.BoletoPagarMeReturn;
                    boleto.DataVencimento = datavencimento;                    
                    
                    #region ALTERAR BOLETO PAGAR ME

                    BoletoPagarMeFactory.Alterar(boleto, Remessa.Empresa.CPV);

                    #endregion

                    return Json(new { retorno = "OK", id = boleto.ID, data = data });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "Data inválida" });

            }
            else
                return Json(new { retorno = "ERRO", mensagem = "O pedido não pode ser alterado. Tente novamente mais tarde" });
        }

    }
}
