﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using System.Globalization;
using Domain.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|RH", "/Home/AccessDenied")]
    public class AdministradorController : CrudController<Administrador>
    {

        private IPersistentRepository<Administrador> adminRepository;
        private IPersistentRepository<Ramai> ramaisRepository;
        private AdministradorPedidoRepository admPedidoRepository;

        private AdminModel adminModel;

        public AdministradorController(ObjectContext context)
            : base(context)
        {
            adminRepository = new PersistentRepository<Administrador>(context);
            ramaisRepository = new PersistentRepository<Ramai>(context);
            admPedidoRepository = new AdministradorPedidoRepository(context);
            adminModel = new AdminModel(context);
        }


        public override ActionResult List(params object[] args)
        {
            var resultado = adminRepository.GetByExpression(a => a.ID != adminModel.CurrentAdministrador.ID).OrderBy(p => p.Nome).ToList();

            var nome = Request.QueryString["nome"];
            var liberado = (Request.QueryString["liberado"] == "1");

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Nome.ToLower().Contains(nome.ToLower())).ToList();

            if (liberado)
                resultado = resultado.Where(c => c.Liberado).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        public ActionResult Relatorio(string mes)
        {
            var datainicial = new DateTime();
            DateTime.TryParse("01/" + mes, out datainicial);
            
            var datafinal = new DateTime(); 

            if (datainicial != new DateTime())
                datafinal = datainicial.AddMonths(1).AddDays(-1);

            if (datainicial != new DateTime() && datafinal != new DateTime())
            {
                var administradores = adminRepository.GetAll().OrderBy(c => c.Nome).ToList();
                string resultado = "";
                foreach (var admin in administradores)
                {
                    resultado += "<table><tr><td>";
                    resultado += "<table width='200' class='gridtable' style='margin-top:19px;'>";
                    resultado += "<tr style='height:78px;'><th>Semana</th><th>Data</th></tr>";
                    for (DateTime dia = datainicial; dia >= datainicial && dia <= datafinal; dia = dia.AddDays(1))
                    {
                        CultureInfo ptbr = new CultureInfo("pt-BR");

                        resultado += "<tr>";
                        resultado += "<td width='200'>" + dia.ToString("dddd", ptbr) + "</td>";
                        resultado += "<td width='50'>" + dia.ToString("dd/MM") + "</td>";

                        resultado += "</tr>";
                    }

                    resultado += "</td><tr><td colspan=2><b>TOTAL</b></td></tr></table>";
                    resultado += "<td><table width='100%'>";
                    resultado += "<tr>";
                    resultado += "<th>" + admin.Nome.ToUpper() + "</th>";
                    resultado += "<tr><td>";
                    resultado += "<table class='gridtable'><tr style='height:80px;'><th width='200'>Processou Pedido</th><th width='200'>Cancelou Pedido</th><th width='200'>Enviou E-mail de Entrega</th><th width='200'>Imprimiu Boleto</th><th width='200'>Preencheu NFe</th><th width='200'>Alt. Forma Pagto</th><th width='200'>Preencheu Repasse</th><th width='200'>Alt. Dt. Venc.</th><th width='200'>Concluiu Pedido</th><th width='200'>Alterou Floricultura </th><th width='200'>Alterou Repasse </th><th width='200'>Enviou E-mail Fornecedor </th><th width='200'>Reenviou E-mail Pedido </th><th width='200'>Enviou E-mail Boleto </th><th width='200'>Enviou E-mail NFe </th>";

                    var Processou_Pedido = 0;
                    var Cancelou_Pedido = 0;
                    var Enviou_Email_de_Entrega = 0;
                    var Imprimiu_Boleto = 0;
                    var Preencheu_NFe = 0;
                    var Alterou_Forma_Pagamento = 0;
                    var Preencheu_Repasse = 0;
                    var Alterou_Data_Vencimento = 0;
                    var Concluiu_Pedido = 0;
                    var Alterou_Floricultura = 0;
                    var Alterou_Repasse = 0;
                    var Enviou_Email_Fornecedor = 0;
                    var Reenviou_Email_Pedido = 0;
                    var Enviou_Email_Boleto = 0;
                    var Enviou_Email_NFe = 0;

                    for (DateTime dia = datainicial; dia >= datainicial && dia <= datafinal; dia = dia.AddDays(1))
                    {
                        var historico = admPedidoRepository.GetHistoricoAdministrador(admin.ID, dia, dia.AddDays(1).AddMilliseconds(-1));

                        foreach (var col in historico)
                        {
                            Processou_Pedido += col.Processou_Pedido.Value;
                            Cancelou_Pedido += col.Cancelou_Pedido.Value; ;
                            Enviou_Email_de_Entrega += col.Enviou_Email_de_Entrega.Value;
                            Imprimiu_Boleto += col.Imprimiu_Boleto.Value;
                            Preencheu_NFe += col.Preencheu_NFe.Value;
                            Alterou_Forma_Pagamento += col.Alterou_Forma_Pagamento.Value;
                            Preencheu_Repasse += col.Preencheu_Repasse.Value;
                            Alterou_Data_Vencimento += col.Alterou_Data_Vencimento.Value;
                            Concluiu_Pedido += col.Concluiu_Pedido.Value;
                            Alterou_Floricultura += col.Alterou_Floricultura.Value;
                            Alterou_Repasse += col.Alterou_Repasse.Value;
                            Enviou_Email_Fornecedor += col.Enviou_Email_Fornecedor.Value;
                            Reenviou_Email_Pedido += col.Reenviou_Email_Pedido.Value;
                            Enviou_Email_Boleto += col.Enviou_Email_Boleto.Value;
                            Enviou_Email_NFe += col.Enviou_Email_NFe.Value;

                            resultado += "<tr><td>" + col.Processou_Pedido + "</td><td>" + col.Cancelou_Pedido + "</td><td>" + col.Enviou_Email_de_Entrega + "</td><td>" + col.Imprimiu_Boleto + "</td><td>" + col.Preencheu_NFe + "</td><td>" + col.Alterou_Forma_Pagamento + "</td><td>" + col.Preencheu_Repasse + "</td><td>" + col.Alterou_Data_Vencimento + "</td><td>" + col.Concluiu_Pedido + "</td><td>" + col.Alterou_Floricultura + "</td><td>" + col.Alterou_Repasse + "</td><td>" + col.Enviou_Email_Fornecedor + "</td><td>" + col.Reenviou_Email_Pedido + "</td><td>" + col.Enviou_Email_Boleto + "</td><td>" + col.Enviou_Email_NFe + "</td>";
                        }
                    }
                    resultado += "<tr style='font-weight:bold;'><td>" + Processou_Pedido + "</td><td>" + Cancelou_Pedido + "</td><td>" + Enviou_Email_de_Entrega + "</td><td>" + Imprimiu_Boleto + "</td><td>" + Preencheu_NFe + "</td><td>" + Alterou_Forma_Pagamento + "</td><td>" + Preencheu_Repasse + "</td><td>" + Alterou_Data_Vencimento + "</td><td>" + Concluiu_Pedido + "</td><td>" + Alterou_Floricultura + "</td><td>" + Alterou_Repasse + "</td><td>" + Enviou_Email_Fornecedor + "</td><td>" + Reenviou_Email_Pedido + "</td><td>" + Enviou_Email_Boleto + "</td><td>" + Enviou_Email_NFe + "</td>";
                    resultado += "</table></td></tr>";

                    resultado += "</td></tr>";
                    resultado += "</table></td>";
                    resultado += "</tr></table>";
                }
                ViewBag.Resultado = resultado;
            }
            return View();
        }

        public ActionResult RelatorioExcel(string mes)
        {
            var datainicial = new DateTime();
            DateTime.TryParse("01/" + mes, out datainicial);
            
            var datafinal = new DateTime(); 

            if (datainicial != new DateTime())
                datafinal = datainicial.AddMonths(1).AddDays(-1);

            if (datainicial != new DateTime() && datafinal != new DateTime())
            {
                var administradores = adminRepository.GetAll().OrderBy(c => c.Nome).ToList();
                string resultado = "";

                foreach (var admin in administradores)
                {
                    resultado += "<table><tr><td>";
                    resultado += "<table width='200' class='gridtable' style='margin-top:19px;'>";
                    resultado += "<tr style='height:43px;'><th>Semana</th><th>Data</th></tr>";
                    for (DateTime dia = datainicial; dia >= datainicial && dia <= datafinal; dia = dia.AddDays(1))
                    {
                        CultureInfo ptbr = new CultureInfo("pt-BR");

                        resultado += "<tr>";
                        resultado += "<td width='200'>" + dia.ToString("dddd", ptbr) + "</td>";
                        resultado += "<td width='50'>" + dia.ToString("dd/MM") + "</td>";

                        resultado += "</tr>";
                    }

                    resultado += "</td><tr><td colspan=2><b>TOTAL</b></td></tr></table>";
                    resultado += "<td><table width='100%'>";
                    resultado += "<tr>";
                    resultado += "<th>" + admin.Nome.ToUpper() + "</th>";
                    resultado += "<tr><td>";
                    resultado += "<table class='gridtable'><tr style='height:80px;'><th width='200'>Processou Pedido</th><th width='200'>Cancelou Pedido</th><th width='200'>Enviou E-mail de Entrega</th><th width='200'>Imprimiu Boleto</th><th width='200'>Preencheu NFe</th><th width='200'>Alt. Forma Pagto</th><th width='200'>Preencheu Repasse</th><th width='200'>Alt. Dt. Venc.</th><th width='200'>Concluiu Pedido</th><th width='200'>Alterou Floricultura </th><th width='200'>Alterou Repasse </th><th width='200'>Enviou E-mail Fornecedor </th><th width='200'>Reenviou E-mail Pedido </th><th width='200'>Enviou E-mail Boleto </th><th width='200'>Enviou E-mail NFe </th>";

                    var Processou_Pedido = 0;
                    var Cancelou_Pedido = 0;
                    var Enviou_Email_de_Entrega = 0;
                    var Imprimiu_Boleto = 0;
                    var Preencheu_NFe = 0;
                    var Alterou_Forma_Pagamento = 0;
                    var Preencheu_Repasse = 0;
                    var Alterou_Data_Vencimento = 0;
                    var Concluiu_Pedido = 0;
                    var Alterou_Floricultura = 0;
                    var Alterou_Repasse = 0;
                    var Enviou_Email_Fornecedor = 0;
                    var Reenviou_Email_Pedido = 0;
                    var Enviou_Email_Boleto = 0;
                    var Enviou_Email_NFe = 0;

                    for (DateTime dia = datainicial; dia >= datainicial && dia <= datafinal; dia = dia.AddDays(1))
                    {
                        var historico = admPedidoRepository.GetHistoricoAdministrador(admin.ID, dia, dia.AddDays(1).AddMilliseconds(-1));

                        foreach (var col in historico)
                        {
                            Processou_Pedido += col.Processou_Pedido.Value;
                            Cancelou_Pedido += col.Cancelou_Pedido.Value; ;
                            Enviou_Email_de_Entrega += col.Enviou_Email_de_Entrega.Value;
                            Imprimiu_Boleto += col.Imprimiu_Boleto.Value;
                            Preencheu_NFe += col.Preencheu_NFe.Value;
                            Alterou_Forma_Pagamento += col.Alterou_Forma_Pagamento.Value;
                            Preencheu_Repasse += col.Preencheu_Repasse.Value;
                            Alterou_Data_Vencimento += col.Alterou_Data_Vencimento.Value;
                            Concluiu_Pedido += col.Concluiu_Pedido.Value;
                            Alterou_Floricultura += col.Alterou_Floricultura.Value;
                            Alterou_Repasse += col.Alterou_Repasse.Value;
                            Enviou_Email_Fornecedor += col.Enviou_Email_Fornecedor.Value;
                            Reenviou_Email_Pedido += col.Reenviou_Email_Pedido.Value;
                            Enviou_Email_Boleto += col.Enviou_Email_Boleto.Value;
                            Enviou_Email_NFe += col.Enviou_Email_NFe.Value;

                            resultado += "<tr><td>" + col.Processou_Pedido + "</td><td>" + col.Cancelou_Pedido + "</td><td>" + col.Enviou_Email_de_Entrega + "</td><td>" + col.Imprimiu_Boleto + "</td><td>" + col.Preencheu_NFe + "</td><td>" + col.Alterou_Forma_Pagamento + "</td><td>" + col.Preencheu_Repasse + "</td><td>" + col.Alterou_Data_Vencimento + "</td><td>" + col.Concluiu_Pedido + "</td><td>" + col.Alterou_Floricultura + "</td><td>" + col.Alterou_Repasse + "</td><td>" + col.Enviou_Email_Fornecedor + "</td><td>" + col.Reenviou_Email_Pedido + "</td><td>" + col.Enviou_Email_Boleto + "</td><td>" + col.Enviou_Email_NFe + "</td>";
                        }
                    }
                    resultado += "<tr style='font-weight:bold;'><td>" + Processou_Pedido + "</td><td>" + Cancelou_Pedido + "</td><td>" + Enviou_Email_de_Entrega + "</td><td>" + Imprimiu_Boleto + "</td><td>" + Preencheu_NFe + "</td><td>" + Alterou_Forma_Pagamento + "</td><td>" + Preencheu_Repasse + "</td><td>" + Alterou_Data_Vencimento + "</td><td>" + Concluiu_Pedido + "</td><td>" + Alterou_Floricultura + "</td><td>" + Alterou_Repasse + "</td><td>" + Enviou_Email_Fornecedor + "</td><td>" + Reenviou_Email_Pedido + "</td><td>" + Enviou_Email_Boleto + "</td><td>" + Enviou_Email_NFe + "</td>";
                    resultado += "</table></td></tr>";

                    resultado += "</td></tr>";
                    resultado += "</table></td>";
                    resultado += "</tr></table>";
                }

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = "iso-8859-1";
                Response.BufferOutput = true;
                Response.AddHeader("content-disposition", "attachment; filename = relatorio.xls");
                Response.Write(resultado);
                Response.End();
            }
            return View();
        }

        public ActionResult Historico()
        {
            var resultado = admPedidoRepository.GetAll().OrderByDescending(p => p.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var numero = Request.QueryString["numero"];

            var usuarioID = 0;
            Int32.TryParse(Request.QueryString["usuarioID"], out usuarioID);

            var acaoID = 0;
            Int32.TryParse(Request.QueryString["acaoID"], out acaoID);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataCriacao <= datafinal).ToList();
            }

            if (!String.IsNullOrEmpty(numero))
            {
                var pedidoID = 0;
                Int32.TryParse(numero, out pedidoID);
                if (pedidoID > 0)
                {
                    resultado = resultado.Where(c => c.PedidoEmpresaID.HasValue).Where(c => c.PedidoEmpresa.ID == pedidoID).ToList();
                }
                else
                {
                    resultado = resultado.Where(c => c.PedidoID.HasValue).Where(c => c.Pedido.Numero == numero).ToList();
                }
            }

            if (usuarioID > 0)
                resultado = resultado.Where(c => c.AdministradorID == usuarioID).ToList();

            if (acaoID > 0)
                resultado = resultado.Where(c => c.AcaoID == acaoID).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            ViewBag.Usuarios = adminRepository.GetAll().OrderBy(c => c.Nome).ToList();

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Administrador admin = null;
            ViewBag.Ramais = ramaisRepository.GetAll().OrderBy(e => e.NumeroRamal);

            if (id.HasValue)
            {
                admin = adminRepository.Get(id.Value);
            }
            ViewBag.Admin = adminModel.CurrentAdministrador;
            return View(admin);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Administrador admin)
        {
            if (adminRepository.GetByExpression(c => c.ID != admin.ID && c.Email == admin.Email).Count() == 0)
            {
                admin.RecebePedidos = false;
                adminRepository.Save(admin);
            }
            else
            {
                ViewBag.Erro = "O e-mail digitado já existe para outro administrador.";
                ViewBag.Admin = adminModel.CurrentAdministrador;
                return View(admin);
            }

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }
    }
}
