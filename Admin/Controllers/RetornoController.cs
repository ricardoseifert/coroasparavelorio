﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Domain.Service;
using System.Data.Objects;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
    public class RetornoController : Controller
    {
        private BoletoService boletoService;

        public RetornoController(ObjectContext context)
        {
            boletoService = new BoletoService(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Processar()
        {
            ViewBag.Resultado = MvcExtensions.Controllers.Message.SaveSucess;

            var arquivo = Request.Files[0];
            if (arquivo.ContentLength == 0)
            {
                ViewBag.Resultado = MvcExtensions.Controllers.Message.SaveFailure;
                ViewBag.Mensagem = "Envie um arquivo para processamento.";
            }
            else
            {
                try
                {
                    var retornos = boletoService.ProcessarRetorno(arquivo.InputStream);
                    ViewBag.Resultado = MvcExtensions.Controllers.Message.SaveSucess;
                    return View("Index", retornos);
                }
                catch (Exception e)
                {
                    ViewBag.Resultado = MvcExtensions.Controllers.Message.SaveFailure;
                    ViewBag.Mensagem = e.Message;
                }
            }
            return View("Index");
        }

    }
}
