﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Floricultura|FloriculturaMisto|Financeiro", "/Home/AccessDenied")]
    public class LoteFloriculturaController : CrudController<EstoqueLote>
    {
        #region Variaveis
        private IPersistentRepository<EstoqueFornecedor> estoqueFornecedorRepository;
        private IPersistentRepository<EstoqueLote> estoqueLoteRepository;
        private IPersistentRepository<EstoqueLoteItem> estoqueLoteItemRepository;
        private IPersistentRepository<EstoqueFornecedorBanco> estoqueFornecedorBancoRepository;

        private AdminModel adminModel;
        #endregion

        public LoteFloriculturaController(ObjectContext context)
            : base(context) {
            adminModel = new AdminModel(context);
            estoqueFornecedorRepository = new PersistentRepository<EstoqueFornecedor>(context);
            estoqueLoteRepository = new PersistentRepository<EstoqueLote>(context);
            estoqueLoteItemRepository = new PersistentRepository<EstoqueLoteItem>(context);
            estoqueFornecedorBancoRepository = new PersistentRepository<EstoqueFornecedorBanco>(context);


        }
        public ActionResult Novo()
        {
            ViewBag.Fornecedores = estoqueFornecedorRepository.GetAll().OrderBy(r => r.NomeFornecedor).ToList();
            ViewBag.Bancos = estoqueFornecedorBancoRepository.GetAll().OrderBy(r => r.NomeBanco).ToList();

            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            EstoqueLote novoLote = new EstoqueLote();
            novoLote.Numero = novoLote.CriarNumero();
            novoLote.DataCadastro = DateTime.Now;

            estoqueLoteRepository.Save(novoLote);

            #region INÍCIO DO LOOP DE PRODUTOS

            foreach (var Iten in Request.Form["ValoresProdutos"].Remove(Request.Form["ValoresProdutos"].Length - 1).Split('|'))
            {
                //var ProdutoTamanhoID = int.Parse(Iten.Split('º')[0].ToString());
                //var produto = estoqueProdutoRepository.Get(ProdutoTamanhoID);

                //var UnidadeMedidaID = int.Parse(Iten.Split('º')[1].ToString());
                //var unidadeMedida = unidadeMedidaRepository.Get(UnidadeMedidaID);

                //var Qtd = int.Parse(Iten.Split('º')[2].ToString());
                //var ValorUnitario = decimal.Parse(Iten.Split('º')[3].ToString());

                //EstoqueLoteItem novoLoteItem = new EstoqueLoteItem();
                //novoLoteItem.EstoqueLote = novoLote;
                //novoLoteItem.EstoqueProduto = produto;
                //novoLoteItem.EstoqueUnidadeMedida = unidadeMedida;
                //novoLoteItem.Qtd = Qtd;
                //novoLoteItem.ValorUnitario = ValorUnitario;

                //if (!string.IsNullOrEmpty(Iten.Split('º')[4]))
                //{
                //    novoLoteItem.DataVencimento = DateTime.Parse(Iten.Split('º')[4].ToString());
                //}
                //estoqueLoteItemRepository.Save(novoLoteItem);
            }
            
            #endregion

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Lote " + novoLote.Numero + "foi criado com sucesso!", datainicial = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, datafinal = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year });
        }
        public override ActionResult List(params object[] args)
        {
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<EstoqueLote> lstLote = new List<EstoqueLote>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            lstLote = estoqueLoteRepository.GetByExpression(p => p.DataCadastro >= datainicial && p.DataCadastro <= datafinal).OrderByDescending(p => p.DataCadastro).ToList();

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var meiopagamento = 0;
            Int32.TryParse(Request.QueryString["meiopagamento"], out meiopagamento);

            if (!String.IsNullOrEmpty(numero))
                lstLote = lstLote.Where(c => c.Numero.ToLower().Contains(numero.ToLower())).ToList();
            else
            {
                //if(statuspagamento == 3)
                //{
                //    lstLote = lstLote.Where(c => c.EstoqueFaturaPagamento_x_Lote.Count == 0).ToList();
                //}
                //else
                //{
                //    if (statuspagamento > 0)
                //        lstLote = lstLote.Where(c => c.EstoqueFaturaPagamento_x_Lote.Where(r => r.EstoqueFaturaPagamento.EstoqueFaturaStatusPagamento.ID == statuspagamento).Any()).ToList();
                //}

                if (!String.IsNullOrEmpty(nome))
                    lstLote = lstLote.Where(c => c.EstoqueFornecedor.NomeFornecedor.ToLower().Contains(nome.ToLower())).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstLote.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lstLote = lstLote.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(lstLote);
        }

        public ActionResult Visualizar(int id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var Lote = estoqueLoteRepository.Get(id);

            if (Lote != null)
            {
                return View(Lote);
            }
            else
            {
                return RedirectToAction("List");
            }             
        }
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {

            return View();
        }
        public JsonResult RemoverProduto(int LoteItemID, int LoteID)
        {
            var Item = estoqueLoteItemRepository.Get(LoteItemID);

            if (PodeAlterarItemLote(Item.ID))
            {
                estoqueLoteItemRepository.Delete(Item);

                RecalculaLote(LoteID);

                return Json("OK");
            }
            else
            {
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
            }

            
        }

        public void RecalculaLote(int LoteID)
        {
            var Lote = estoqueLoteRepository.Get(LoteID);

            //ALTERA PEDIDO
            decimal valortotal = 0;
            foreach (var item in Lote.EstoqueLoteItems)
            {
                valortotal += item.ValorUnitario * item.Qtd;
            }
            estoqueLoteRepository.Save(Lote);

            //// ALTERA FATURA SE TEM FATURA
            //if (Lote.EstoqueFaturaPagamento_x_Lote.Count() > 0)
            //{
            //    var Fatura = estoqueFaturaPagamentoRepository.Get(Lote.EstoqueFaturaPagamento_x_Lote.FirstOrDefault().EstoqueFaturaPagamento.ID);
            //    Fatura.ValorTotal = valortotal;
            //    estoqueFaturaPagamentoRepository.Save(Fatura);
            //}
        }

        public JsonResult AdicionarProduto(int LoteID, int ProdutoEstoqueID, int UniMedida, decimal valor, int NovaQtd, string NovaDtValidade)
        {
            var Lote = estoqueLoteRepository.Get(LoteID);

            EstoqueLoteItem novoItem = new EstoqueLoteItem();
            novoItem.EstoqueLote = Lote;
            //novoItem.EstoqueProduto = estoqueProdutoRepository.Get(ProdutoEstoqueID);
            //novoItem.EstoqueUnidadeMedida = unidadeMedidaRepository.Get(UniMedida);
            //novoItem.ValorUnitario = valor;
            novoItem.Qtd = NovaQtd;
            if (!string.IsNullOrEmpty(NovaDtValidade))
            {
                novoItem.DataVencimento = DateTime.Parse(NovaDtValidade);
            }
            else
            {
                novoItem.DataVencimento = null;
            }
            estoqueLoteItemRepository.Save(novoItem);

            return Json("OK");
        }

        public JsonResult AlterarProduto(int itemID, int ProdutoEstoqueID, int UniMedida, decimal valor, int NovaQtd, string NovaDtValidade)
        {
            var Item = estoqueLoteItemRepository.Get(itemID);
            var Lote = estoqueLoteRepository.Get(Item.EstoqueLote.ID);

           

            if (Item != null && PodeAlterarItemLote(Item.ID))
            {
                Item.Qtd = NovaQtd;
                Item.ValorUnitario = valor;
                if (!string.IsNullOrEmpty(NovaDtValidade))
                {
                    Item.DataVencimento = DateTime.Parse(NovaDtValidade);
                }
                else
                {
                    Item.DataVencimento = null;
                }
                //Item.EstoqueProduto = estoqueProdutoRepository.Get(ProdutoEstoqueID);
                //Item.EstoqueUnidadeMedida = unidadeMedidaRepository.Get(UniMedida);

                estoqueLoteItemRepository.Save(Item);

                RecalculaLote(Item.EstoqueLote.ID);

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }

        public bool PodeAlterarItemLote(int ItemID)
        {
            var Item = estoqueLoteItemRepository.Get(ItemID);

            bool PodeAlterarProduto = false;
            //if (Item.EstoqueLote.EstoqueFaturaPagamento_x_Lote.Count() > 0)
            //{
            //    var Fatura = Item.EstoqueLote.EstoqueFaturaPagamento_x_Lote.FirstOrDefault();
            //    if (Fatura.EstoqueFaturaPagamento.EstoqueFaturaStatusPagamento.ID == (int)Domain.Entities.EstoqueFaturaStatusPagamento.TodosStatusPagamento.EmAberto)
            //    {
            //        PodeAlterarProduto = true;
            //    }
            //}
            //else
            //{
            //    PodeAlterarProduto = true;
            //}

            return PodeAlterarProduto;
        }
    }
}