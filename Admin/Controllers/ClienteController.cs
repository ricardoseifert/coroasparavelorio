﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class ClienteController : CrudController<Cliente>
    {
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private AdminModel adminModel;


        public ClienteController(ObjectContext context)
            : base(context)
        {
            clienteRepository = new PersistentRepository<Cliente>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            adminModel = new AdminModel(context);
        }


        public ActionResult Caminho(string area, Cliente cliente)
        {
            ViewBag.Area = area;
            ViewBag.Cliente = cliente;
            return View();
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = clienteRepository.GetAll().OrderBy(p => p.Nome).ToList();

            int clienteid = 0;
            Int32.TryParse(Request.QueryString["clienteid"], out clienteid);
            if (clienteid > 0)
                resultado = resultado.Where(c => c.ID == clienteid).ToList();

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.Nome.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower()))).ToList();

            var cpf = Request.QueryString["cpf"];
            if (!String.IsNullOrEmpty(cpf))
                resultado = resultado.Where(c => Domain.Core.Funcoes.Formata_CNPJ(c.Documento).Contains(Domain.Core.Funcoes.Formata_CNPJ(cpf))).ToList();

            int pedidos = 0;
            Int32.TryParse(Request.QueryString["pedidos"], out pedidos);
            if (pedidos > 0)
                resultado = resultado.Where(c => c.Pedidoes.Count >= pedidos).ToList();
             
            bool migrados = false;
            Boolean.TryParse(Request.QueryString["migrados"], out migrados);
            if (migrados)
                resultado = resultado.Where(c => c.LacosDataMigracao.HasValue).ToList();
            else
                resultado = resultado.Where(c => !c.LacosDataMigracao.HasValue).ToList();
             
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Cliente cliente = null;
            if (id.HasValue)
            {
                cliente = clienteRepository.Get(id.Value);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.EstadoID).OrderBy(e => e.Nome);
            }
            else
            {
                ViewBag.Cidades = new List<Cidade>();
            }
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(cliente);
        }

        [ValidateInput(false)] 
        public override ActionResult Edit(Cliente cliente)
        {

            if (!string.IsNullOrEmpty(Request.Form["ProdutoTamanhoIDPadrao"]))
            {
                cliente.ProdutoTamanhoIDPadrao = int.Parse(Request.Form["ProdutoTamanhoIDPadrao"]);
            }
            else {
                cliente.ProdutoTamanhoIDPadrao = null;
            }

            if (!string.IsNullOrEmpty(Request.Form["DescontoPadrao"]))
            {
                cliente.DescontoPadrao = decimal.Parse(Request.Form["DescontoPadrao"]);
            }
            else
            {
                cliente.DescontoPadrao = null;
            }


            ModelState.Remove("RazaoSocial"); 
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            if (cliente.ID > 0)
            {
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.EstadoID).OrderBy(e => e.Nome); 
            }
            else
            {
                cliente.DataCadastro = DateTime.Now;
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = new List<Cidade>();
            }

            if (cliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica && !Domain.Core.Funcoes.Valida_CPF(cliente.Documento))
            {
                ViewBag.Erro = "CPF inválido";
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.EstadoID).OrderBy(e => e.Nome);
                return View(cliente);
            }
            else if (cliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica && !Domain.Core.Funcoes.Valida_CNPJ(cliente.Documento))
            {
                ViewBag.Erro = "CNPJ inválido";
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.EstadoID).OrderBy(e => e.Nome);
                return View(cliente);
            }
            else
            { 
                if (cliente.RazaoSocial == null)
                    cliente.RazaoSocial = "";

                if (cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.Isento)
                    cliente.InscricaoEstadual = "ISENTO";
                if (cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.NaoContribuinte)
                    cliente.InscricaoEstadual = "NÃO CONTRIBUINTE";

                cliente.Documento = cliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                if (cliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica)
                {
                    cliente.RazaoSocial = "";
                    cliente.InscricaoEstadual = "";
                }
                cliente.ProblemaEmail = false;
                clienteRepository.Save(cliente);
                return RedirectToAction("List", new { cod = "SaveSucess", clienteid = cliente.ID });
            }
        }

        public ActionResult TelaGuerra(int clienteID)
        {
            var cliente = clienteRepository.Get(clienteID);
            if (cliente != null)
            {
                return View(cliente);
            }
            else
                return RedirectToAction("List");
        }

        public ActionResult ErrosEmail(int id)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                return View(cliente.RetornoEmails.ToList());
            }
            else
                return RedirectToAction("List");
        }

        [HttpPost]
        public JsonResult SalvarNotaPlanoAcao(int id, string NotaPlanoAcao)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                cliente.NotaPlanoAcao = NotaPlanoAcao;
                clienteRepository.Save(cliente);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }

        [HttpPost]
        public JsonResult SalvarNotaUltimoContato(int id, string NotaUltimoContato)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                cliente.NotaUltimoContato = NotaUltimoContato;
                clienteRepository.Save(cliente);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }

        [HttpPost]
        public JsonResult GetCliente(string documento)
        {
            var corp = empresaRepository.GetByExpression(c => c.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim()).FirstOrDefault();
            if (corp == null)
            {
                var cliente = clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim()).FirstOrDefault();

                if(cliente != null)
                {
                    ProdutoTamanho ProdutoTamanhoIDPadrao = null;
                    if (cliente.ProdutoTamanhoIDPadrao != null)
                        ProdutoTamanhoIDPadrao = produtoTamanhoRepository.Get((int)cliente.ProdutoTamanhoIDPadrao);

                    string ProdutoTamanhoID = "";
                    string ProdutoTamanhoName = "";

                    decimal DescontoPadrao = 0;
                    if (cliente.DescontoPadrao != null)
                        DescontoPadrao = (decimal)cliente.DescontoPadrao;

                    if (ProdutoTamanhoIDPadrao != null)
                    {
                        ProdutoTamanhoID = ProdutoTamanhoIDPadrao.ID.ToString();
                        ProdutoTamanhoName = ProdutoTamanhoIDPadrao.Produto.Nome;
                    }

                    return Json(new { ProdutoTamanhoID = cliente.ProdutoTamanhoIDPadrao, ProdutoTamanhoPadraoNome = ProdutoTamanhoName, DescontoPadrao = DescontoPadrao, Corporativo = "NÃO", Tipo = "CLIENTE", Nome = cliente.Nome.ToUpper(), Email = cliente.Email.ToLower(), TelefoneContato = cliente.TelefoneContato, CelularContato = cliente.CelularContato, CEP = cliente.CEP, EstadoID = cliente.EstadoID, CidadeID = cliente.CidadeID, Logradouro = cliente.Logradouro, Numero = cliente.Numero, Complemento = cliente.Complemento, Bairro = cliente.Bairro, ComoConheceu = cliente.ComoConheceu, Exterior = cliente.Exterior, InscricaoEstadual = cliente.InscricaoEstadual, RazaoSocial = cliente.RazaoSocial });
                }
                else
                {
                    return Json("");
                }
                
            }
            else
                return Json(new { TipoPessoa = "PessoaFisica", Corporativo = "SIM", Tipo = "EMPRESA", ID = corp.ID });
        }
    }
}
