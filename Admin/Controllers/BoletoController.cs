﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    public class BoletoController : CrudController<Boleto>
    {

        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;

        public BoletoController(ObjectContext context)
            : base(context)
        {
            boletoRepository = new PersistentRepository<Boleto>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var boleto = boletoRepository.Get(id.Value);
                if (boleto != null)
                {
                    return View(boleto);
                }
                else
                {
                    return RedirectToAction("List");
                }
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Boleto boleto)
        {
            boletoRepository.Save(boleto);

            return RedirectToAction("List", new { cod = "SaveSucess", boletoID = boleto.ID });
        }

        [HttpGet]
        public ActionResult Cancelar(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                boleto.Comando = Boleto.Comandos.Cancelar;
                if (boleto.RemessaBoletoes.Count == 0)
                {
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processado;
                    boleto.Processado = true;
                }
                else
                {
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando;
                    boleto.Processado = false;
                }
                boleto.StatusPagamento = Boleto.TodosStatusPagamento.Cancelado;
                boletoRepository.Save(boleto);

                return RedirectToAction("List", new { cod = "SaveSucess", msg="O boleto foi cancelado com sucesso!"});
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [HttpGet]
        public ActionResult Protestar(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                if (boleto.RemessaBoletoes.Count > 0)
                {
                    boleto.Comando = Boleto.Comandos.Protestar;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando;
                    boletoRepository.Save(boleto);

                    return RedirectToAction("List", new { cod = "SaveSucess", msg = "O boleto foi protestado com sucesso!" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Você só pode protestar um boleto que já foi enviado em alguma remessa." });
                }
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [HttpGet]
        public ActionResult Cadastro(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                if (boleto.RemessaBoletoes.Count > 0)
                {
                    boleto.Comando = Boleto.Comandos.ModificarDados;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando;
                    boletoRepository.Save(boleto);

                    return RedirectToAction("List", new { cod = "SaveSucess", msg = "O boleto foi alterado com sucesso!" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Você só pode alterar os dados cadastrais um boleto que já foi enviado em alguma remessa." });
                }
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [HttpGet]
        public ActionResult ValorVencimento(int id)
        {
            var boleto = boletoRepository.Get(id);
            
            if (boleto != null)
            {
                if (boleto.RemessaBoletoes.Count > 0)
                {
                    boleto.Comando = Boleto.Comandos.ModificarVencimento;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando;
                    boletoRepository.Save(boleto);

                   

                    return RedirectToAction("List", new { cod = "SaveSucess", msg = "O boleto foi alterado com sucesso!" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Você só pode alterar os dados cadastrais um boleto que já foi enviado em alguma remessa." });
                }
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [AreaAuthorization("Admin", "/Home/Login")]
        [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
        public override ActionResult List(params object[] args)
        {
            var resultado = new List<Boleto>();
            var boletoID = 0;
            Int32.TryParse(Request.QueryString["boletoID"], out boletoID);
            if (boletoID > 0)
                resultado = boletoRepository.GetByExpression(c => c.ID == boletoID).ToList();
            else
            {
                var data = DateTime.Now.AddDays(-30);
                resultado = boletoRepository.GetByExpression
                    (c => (c.StatusPagamentoID != (int)Boleto.TodosStatusPagamento.Cancelado) 
                    || (c.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.Cancelado 
                    && (!c.DataRetorno.HasValue || c.DataRetorno.Value >= data))
                    )
                    .Take(2000)
                    .OrderByDescending(c=>c.DataCriacao)
                    .ToList();
            }
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var nossonumero = Request.QueryString["nossonumero"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

             
            var statusprocessamento = 0;
            Int32.TryParse(Request.QueryString["statusprocessamento"], out statusprocessamento);

            var origem = 0;
            Int32.TryParse(Request.QueryString["origem"], out origem);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statuspagamentoped = 0;
            Int32.TryParse(Request.QueryString["statuspagamentoped"], out statuspagamentoped);

            var statusretorno = 0;
            Int32.TryParse(Request.QueryString["statusretorno"], out statusretorno);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao <= datafinal.AddDays(1).AddMilliseconds(-1)).ToList();
            
            if (!String.IsNullOrEmpty(nome))
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Cliente.Nome.ToLower().Contains(nome.ToLower()) || c.Pedido.Cliente.RazaoSocial.ToLower().Contains(nome.ToLower()) : c.Faturamento.Empresa.RazaoSocial.ToLower().Contains(nome.ToLower()) || c.SacadoNome.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Cliente.Email.ToLower().Contains(email.ToLower()) : c.Faturamento.Empresa.EmailContato.ToLower().Contains(email.ToLower())).ToList();

            if (!String.IsNullOrEmpty(numero))
            {
                var id = 0;
                Int32.TryParse(numero, out id);
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Numero.Contains(numero) : c.Faturamento.PedidoEmpresas.Count(e => e.ID == id) > 0).ToList();
            }
            if (!String.IsNullOrEmpty(nossonumero))
            {
                resultado = boletoRepository.GetByExpression(c => c.NossoNumero == nossonumero).ToList();
            }
            if (clienteID > 0)
                resultado = resultado.Where(c => (c.Pedido != null)?c.Pedido.ClienteID == clienteID:c.Faturamento.EmpresaID == clienteID).ToList();
             
            if (statusprocessamento > 0)
                resultado = resultado.Where(c => c.StatusProcessamentoID == statusprocessamento).ToList();

            if (statuspagamento > 0)
            {
                if (statuspagamento != 3)
                    resultado = resultado.Where(c => c.StatusPagamentoID == statuspagamento).ToList();
                else
                    resultado = resultado.Where(c => (c.DataVencimento <= DateTime.Now.AddDays(-1) && c.StatusPagamento == Boleto.TodosStatusPagamento.AguardandoPagamento) || c.StatusPagamento == Boleto.TodosStatusPagamento.Vencido).ToList();
            }

            if (statuspagamentoped > 0)
            {
                resultado = resultado.Where(c => (c.PedidoID.HasValue && c.Pedido.StatusPagamentoID == statuspagamentoped) || (c.FaturamentoID.HasValue && c.Faturamento.StatusPagamentoID == statuspagamentoped)).ToList();
            }

            if (origem == 1)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite != "LAÇOS FLORES").ToList();
            else if (origem == 2)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite == "LAÇOS FLORES").ToList();
            else if (origem == 3)
                resultado = resultado.Where(c => c.FaturamentoID.HasValue).ToList();

            if (statusretorno > 0)
                resultado = resultado.Where(c => c.RetornoID == statusretorno).ToList();

            resultado = resultado.OrderBy(c => c.NossoNumero).OrderBy(p => p.DataVencimento.Day).OrderBy(p => p.DataVencimento.Month).OrderBy(p => p.DataVencimento.Year).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 40);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * 40).Take(40).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }
    }
}
