﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class ColaboradorController : CrudController<Colaborador>
    {
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<Empresa> empresaRepository;

        public ColaboradorController(ObjectContext context)
            : base(context)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = new List<Colaborador>();

            var empresaID = 0;
            Int32.TryParse(Request.QueryString["empresaID"], out empresaID);

            var empresa = empresaRepository.Get(empresaID);
            if (empresa != null)
            {
                resultado = empresa.Colaboradors.OrderBy(p => p.Nome).ToList();
                ViewBag.Empresa = empresa;
            }
            else
            {
                resultado = colaboradorRepository.GetAll().OrderBy(p => p.Nome).ToList();
            }


            int colaboradorid = 0;
            Int32.TryParse(Request.QueryString["colaboradorid"], out colaboradorid);
            if (colaboradorid > 0)
                resultado = resultado.Where(c => c.ID == colaboradorid).ToList();

            var razaosocial = Request.QueryString["razaosocial"];
            if (!String.IsNullOrEmpty(razaosocial))
                resultado = resultado.Where(c => c.Empresa.RazaoSocial.ToLower().Contains(razaosocial.ToLower()) || c.Empresa.NomeFantasia.ToLower().Contains(razaosocial.ToLower())).ToList();

            var cnpj = Request.QueryString["cnpj"];
            if (!String.IsNullOrEmpty(cnpj))
                resultado = resultado.Where(c => c.Empresa.CNPJ.ToLower().Contains(cnpj.ToLower())).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Nome.ToLower().Contains(nome.ToLower())).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Colaborador colaborador = null;
            if (id.HasValue)
            {
                colaborador = colaboradorRepository.Get(id.Value);
                ViewBag.Empresa = colaborador.Empresa;
            }
            ViewBag.Empresas = empresaRepository.GetAll().OrderBy(e => e.RazaoSocial).ToList();
            return View(colaborador);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Colaborador colaborador)
        {
        //    if (colaboradorRepository.GetByExpression(c => c.ID != colaborador.ID).Count() == 0)
            //    {
                colaborador.SenhasAnteriores = "";
                if (colaborador.ID == 0)
                {
                    colaborador.Codigo = Guid.NewGuid();
                    colaborador.DataCadastro = DateTime.Now;
                }
                try
                {
                    var _DataNascimento = Request.Form["_DataNascimento"];
                    if (!String.IsNullOrEmpty(_DataNascimento))
                    {
                        colaborador.DataNascimento = new DateTime(1900, Convert.ToInt32(_DataNascimento.Split('/')[1]), Convert.ToInt32(_DataNascimento.Split('/')[0]));
                    }
                }
                catch { }
                colaborador.ProblemaEmail = false;

                //verifica o tipo de colaborador ( 1= master , 2 = usuario )
                if (colaborador.TipoID == 2)
                {
                    colaborador.RecebeEmailPedido = false;
                    colaborador.RecebeEmailNFe = false;
                }

                colaboradorRepository.Save(colaborador);
            //}
            //else
            //{
            //    ViewBag.Erro = "O login digitado já existe para outro colaborador que não está no mesmo grupo econômico.";
            //    ViewBag.Empresa = colaborador.Empresa;
            //    ViewBag.Empresas = empresaRepository.GetAll().OrderBy(e => e.RazaoSocial).ToList();
            //    return View(colaborador);
            //}

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

        public ActionResult ErrosEmail(int id)
        {
            var colaborador = colaboradorRepository.Get(id);
            if (colaborador != null)
            {
                return View(colaborador.RetornoEmails.ToList());
            }
            else
                return RedirectToAction("List");
        }
    }
}
