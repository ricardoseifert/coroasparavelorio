﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.IO;
using System.Globalization;
using Domain.Repositories;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    public class BoletoVencidoController : CrudController<Boleto>
    {

        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private PedidoRepository pedidoRepository;
        private AdminModel adminModel;

        public BoletoVencidoController(ObjectContext context)
            : base(context)
        {
            boletoRepository = new PersistentRepository<Boleto>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            pedidoRepository = new PedidoRepository(context);
            adminModel = new AdminModel(context);
        }
         

        [HttpPost]
        public ActionResult Salva(int id, string observacao, string data, string datacontato)
        {
            var dataCEJ = new DateTime();
            try
            {
                dataCEJ = Convert.ToDateTime(data);
            }
            catch
            { }
            var dataContato = new DateTime();
            try
            {
                dataContato = Convert.ToDateTime(datacontato);
            }
            catch
            { }
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                if (dataCEJ != new DateTime())
                    boleto.DataCEJ = dataCEJ;
                else
                    data = "";
                if (dataContato != new DateTime())
                    boleto.DataContato = dataContato;
                else
                    datacontato = "";
                boleto.Observacao = observacao;
                boletoRepository.Save(boleto);

                return Json(new { sucesso = true, data = data, datacontato = datacontato, observacao = boleto.Observacao.Replace("\n", "<br>") });
            }
            else
            {
                return Json(new { sucesso = false, mensagem = "Erro de processamento" });
            }
        }

        [HttpGet]
        public ActionResult Prejuizo(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                boleto.StatusPagamento = Boleto.TodosStatusPagamento.Prejuizo;
                if (boleto.PedidoID.HasValue)
                {
                    boleto.Pedido.StatusPagamento = Pedido.TodosStatusPagamento.Prejuizo;
                }
                else if (boleto.FaturamentoID.HasValue)
                {
                    boleto.Faturamento.StatusPagamento = Pedido.TodosStatusPagamento.Prejuizo;
                }
                
                boletoRepository.Save(boleto);

                return RedirectToAction("List", new { cod = "SaveSucess", msg = "O boleto foi alterado para prejuízo com sucesso!" });
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [AreaAuthorization("Admin", "/Home/Login")]
        [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
        public override ActionResult List(params object[] args)
        {
            var resultado = new List<Boleto>();

            var boletoID = 0;
            Int32.TryParse(Request.QueryString["boletoID"], out boletoID);

            var empresaID = 0;
            Int32.TryParse(Request.QueryString["empresaID"], out empresaID);

            if (boletoID > 0)
            {
                resultado = boletoRepository.GetByExpression(c => c.ID == boletoID).ToList();
            }
            else if (empresaID > 0)
            {
                resultado = boletoRepository.GetByExpression(c => c.Faturamento.EmpresaID == empresaID).ToList();
            }
            else
            {
                var ontem = DateTime.Now.AddDays(-1);
                var data = new DateTime(ontem.Year, ontem.Month, ontem.Day, 23, 59, 59);
                resultado = boletoRepository.GetByExpression(c => (Domain.Core.Funcoes.TruncateTime(c.DataVencimento) <= data && c.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento) || c.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.Vencido || c.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.Protestado).ToList();
            }

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datainicialcej = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialcej"], out datainicialcej);

            var datafinalcej = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalcej"], out datafinalcej);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var nossonumero = Request.QueryString["nossonumero"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);
             
            var statusprocessamento = 0;
            Int32.TryParse(Request.QueryString["statusprocessamento"], out statusprocessamento);

            var origem = 0;
            Int32.TryParse(Request.QueryString["origem"], out origem);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statuspagamentoped = 0;
            Int32.TryParse(Request.QueryString["statuspagamentoped"], out statuspagamentoped);

            var statusretorno = 0;
            Int32.TryParse(Request.QueryString["statusretorno"], out statusretorno);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => Domain.Core.Funcoes.TruncateTime(c.DataVencimento) >= datainicial).ToList();

            if (datafinal != new DateTime())
                resultado = resultado.Where(c => Domain.Core.Funcoes.TruncateTime(c.DataVencimento) <= datafinal.AddDays(1).AddMilliseconds(-1)).ToList();

            if (datainicialcej != new DateTime())
                resultado = resultado.Where(c => (c.Pedido != null) ? Domain.Core.Funcoes.TruncateTime(c.Pedido.DataCriacao) >= datainicialcej : c.Faturamento.PedidoEmpresas.Count(f => Domain.Core.Funcoes.TruncateTime(f.DataCriacao) >= datainicialcej) > 0).ToList();

            if (datafinalcej != new DateTime())
                resultado = resultado.Where(c => (c.Pedido != null) ? Domain.Core.Funcoes.TruncateTime(c.Pedido.DataCriacao) <= datafinalcej.AddDays(1).AddMilliseconds(-1) : c.Faturamento.PedidoEmpresas.Count(f => Domain.Core.Funcoes.TruncateTime(f.DataCriacao) <= datafinalcej.AddDays(1).AddMilliseconds(-1)) > 0).ToList(); 

            if (!String.IsNullOrEmpty(nome))
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Cliente.Nome.ToLower().Contains(nome.Trim().ToLower()) || c.Pedido.Cliente.RazaoSocial.ToLower().Contains(nome.Trim().ToLower()) : c.Faturamento.Empresa.RazaoSocial.ToLower().Contains(nome.Trim().ToLower()) || c.SacadoNome.ToLower().Contains(nome.Trim().ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Cliente.Email.ToLower().Contains(email.Trim().ToLower()) : c.Faturamento.Empresa.EmailContato.ToLower().Contains(email.Trim().ToLower())).ToList();

            if (!String.IsNullOrEmpty(numero))
            {
                var id = 0;
                Int32.TryParse(numero, out id);
                resultado = boletoRepository.GetByExpression(c => (c.Pedido != null) ? c.Pedido.Numero.Contains(numero) : c.Faturamento.PedidoEmpresas.Count(e => e.ID == id) > 0).ToList();
            }
            if (!String.IsNullOrEmpty(nossonumero))
            {
                resultado = boletoRepository.GetByExpression(c => c.NossoNumero == nossonumero).ToList();
            }
            if (clienteID > 0)
                resultado = resultado.Where(c => (c.Pedido != null)?c.Pedido.ClienteID == clienteID:c.Faturamento.EmpresaID == clienteID).ToList();

            if (statusprocessamento > 0)
                resultado = resultado.Where(c => c.StatusProcessamentoID == statusprocessamento).ToList();

            if (statusretorno > 0)
                resultado = resultado.Where(c => c.RetornoID == statusretorno).ToList();

            if (statuspagamento > 0)
                resultado = resultado.Where(c => c.StatusPagamentoID == statuspagamento).ToList();
            
            if (statuspagamentoped > 0)
            {
                resultado = resultado.Where(c => (c.PedidoID.HasValue && c.Pedido.StatusPagamentoID == statuspagamentoped) || (c.FaturamentoID.HasValue && c.Faturamento.StatusPagamentoID == statuspagamentoped)).ToList();
            }

            if (origem == 1)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite != "LAÇOS FLORES").ToList();
            else if (origem == 2)
                resultado = resultado.Where(c => c.PedidoID.HasValue && c.Pedido.OrigemSite == "LAÇOS FLORES").ToList();
            else if (origem == 3)
                resultado = resultado.Where(c => c.FaturamentoID.HasValue).ToList();

            resultado = resultado.OrderBy(c => c.NossoNumero).OrderBy(p => p.DataVencimento.Day).OrderBy(p => p.DataVencimento.Month).OrderBy(p => p.DataVencimento.Year).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 40);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * 40).Take(40).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros. Total " + resultado.Sum(c => c.Valor).ToString("C2");
             
            return View(resultado);
        }

        [HttpGet]
        public ActionResult Protestar(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                if (boleto.RemessaBoletoes.Count > 0)
                {
                    boleto.Comando = Boleto.Comandos.Protestar;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando; 
                    boletoRepository.Save(boleto);

                    return RedirectToAction("List", new { cod = "SaveSucess", msg = "O boleto foi protestado com sucesso!" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Você só pode protestar um boleto que já foi enviado em alguma remessa." });
                }
            }
            else
            {
                return RedirectToAction("List");
            }
        }

        [HttpGet]
        public FileResult CEJ(int? id)
        {
            if (id.HasValue)
            {
                var boleto = boletoRepository.Get(id.Value);
                if (boleto != null)
                {
                    var assinatura = "Obrigado,\nEquipe Coroas Para Velório\n" +
                                       "Atendimento 0800-777-1986\n" +
                                        "www.coroasparavelorio.com.br";

                    var logo = "/content/images/pdf/logo-coroas.png";
                    var culture = new System.Globalization.CultureInfo("pt-BR");
                    var day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                    var empresa = "Coroas para Velório";
                    var telefone = "0800-777-1986";

                    if (boleto.FaturamentoID.HasValue)
                    {
                        assinatura = "Obrigado,\nEquipe Laços Corporativos\n" +
                                           "Atendimento 0800-777-1986\n" +
                                            "www.lacoscorporativos.com.br";

                        logo = "/content/images/pdf/logo-lacos.jpg";  
                        empresa = "Laços Corporativos";
                        telefone = "0800-777-1986";

                    }

                    var texto =
                      "São Paulo, " + DateTime.Now.Date.ToLongDateString().Replace(day + ", ", "") + "\n\n" +
                      "Prezado Sr(a). " + boleto.SacadoNome + " \n\n" +
                      "Pelo presente instrumento particular e na melhor forma de direito, a " + empresa + ", vem formal e respeitosamente à presença de V. Senhoria, expor os seguintes fatos:\n\n" +
                      "Vossa Senhoria encontra-se em débito com o Notificante, pois, verificamos que até o presente momento não quitou o pedido que resultam no valor de " + boleto.Valor.ToString("C2") + " (" + Domain.Core.Funcoes.NumeroPorExtenso(boleto.Valor) + " REAIS).\n\n" +
                      "Diante do exposto, requeremos que V. Senhoria quite os débitos em aberto no prazo de até 5 (cinco) dias úteis através de nossa Central e/ou entre em contato conosco através do seguinte telefone " + telefone + ".\n\n" +
                      "Em não sendo cumprido o prazo acima ou as obrigações faltantes, serão tomadas as medidas judicialmente cabíveis.\n\n" +
                      "Caso o referido débito já tenha sido quitado ao tempo do recebimento desta, favor desconsiderar essa notificação.\n\n" +
                      "Certos de que seremos prontamente atendidos nesse cordial pedido, desde já agradecemos sua compreensão.\n\n" +
                      "Colocamo-nos à disposição para maiores esclarecimentos, Cordialmente,";

                    PdfDocument document = new PdfDocument();

                    //FORMATACAO
                    PdfPage page = document.AddPage();
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XFont font = new XFont("Verdana", 10, XFontStyle.Regular);
                    XTextFormatter tf = new XTextFormatter(gfx);

                    //LOGO
                    XImage xImage = XImage.FromFile(Server.MapPath(logo));
                    gfx.DrawImage(xImage, 40, 10, xImage.PixelWidth, xImage.PixelHeight);

                    //TEXTO
                    XRect rect = new XRect(40, 100, 500, 550);
                    gfx.DrawRectangle(XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Left;
                    tf.DrawString(texto, font, XBrushes.Black, rect, XStringFormats.TopLeft);

                    //ASSINATURA
                    xImage = XImage.FromFile(Server.MapPath(logo));
                    gfx.DrawImage(xImage, 40, 500, xImage.PixelWidth / 2, xImage.PixelHeight / 2);

                    rect = new XRect(150, 500, 550, 232);
                    gfx.DrawRectangle(XBrushes.White, rect);
                    tf.Alignment = XParagraphAlignment.Justify;
                    tf.DrawString(assinatura, font, XBrushes.Black, rect, XStringFormats.TopLeft);


                    byte[] fileContents = null;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        document.Save(stream, true);
                        fileContents = stream.ToArray();
                    }
                    return File(fileContents, "application/pdf");
                }
            }
            return File("","");
        }
         
        [HttpPost]
        public JsonResult EnviaCEJ(int? id)
        {
            if (id.HasValue)
            {
                var boleto = boletoRepository.Get(id.Value);
                if (boleto != null)
                {
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/CEJ.htm")); 
                    var assinatura = "Obrigado,<br>Equipe Coroas Para Velório<br>" +
                                       "Atendimento 0800-777-1986<br>" +
                                        "www.coroasparavelorio.com.br";

                    var logo = "/content/images/pdf/logo-coroas.png";
                    var culture = new System.Globalization.CultureInfo("pt-BR");
                    var day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                    var empresa = "Coroas para Velório";
                    var telefone = "0800-777-1986";
                    var email = "";
                    var nome = "";

                    if (boleto.FaturamentoID.HasValue)
                    {
                        assinatura = "Obrigado,<br>Equipe Laços Corporativos<br>" +
                                           "Atendimento 0800-777-1986<br>" +
                                            "www.lacoscorporativos.com.br";

                        logo = "/content/images/pdf/logo-lacos.jpg";
                        empresa = "Laços Corporativos";
                        telefone = "0800-777-1986";
                        email = boleto.Faturamento.Empresa.EmailContato;
                        nome = boleto.Faturamento.Empresa.NomeCompleto;
                    }
                    else if (boleto.PedidoID.HasValue)
                    {
                        email = boleto.Pedido.EmailSolicitante;
                        nome = boleto.Pedido.NomeSolicitante;
                        if (boleto.Pedido.OrigemSite.ToUpper() == "LAÇOS FLORES")
                        {
                            html = System.IO.File.ReadAllText(Server.MapPath("/content/html/lacosflores/CEJ.htm"));
                            empresa = "Laços Flores";
                        }

                    }

                    var texto =
                      "São Paulo, " + DateTime.Now.Date.ToLongDateString().Replace(day + ", ", "") + "<br><br>" +
                      "Prezado Sr(a). " + boleto.SacadoNome + " <br><br>" +
                      "Pelo presente instrumento particular e na melhor forma de direito, a " + empresa + ", vem formal e respeitosamente à presença de V. Senhoria, expor os seguintes fatos:<br><br>" +
                      "Vossa Senhoria encontra-se em débito com o Notificante, pois, verificamos que até o presente momento não quitou o pedido que resultam no valor de " + boleto.Valor.ToString("C2") + " (" + Domain.Core.Funcoes.NumeroPorExtenso(boleto.Valor) + " REAIS).<br><br>" +
                      "Diante do exposto, requeremos que V. Senhoria quite os débitos em aberto no prazo de até 5 (cinco) dias úteis através de nossa Central e/ou entre em contato conosco através do seguinte telefone " + telefone + ".<br><br>" +
                      "Em não sendo cumprido o prazo acima ou as obrigações faltantes, serão tomadas as medidas judicialmente cabíveis.<br><br>" +
                      "Caso o referido débito já tenha sido quitado ao tempo do recebimento desta, favor desconsiderar essa notificação.<br><br>" +
                      "Certos de que seremos prontamente atendidos nesse cordial pedido, desde já agradecemos sua compreensão.<br><br>" +
                      "Colocamo-nos à disposição para maiores esclarecimentos, Cordialmente,";
                     
                    //ENVIAR EMAIL
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[LOGO]", Domain.Core.Configuracoes.DOMINIO + logo);
                    html = html.Replace("[ASSINATURA]", assinatura);
                    html = html.Replace("[MENSAGEM]", texto);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FINANCEIRO, empresa, email, nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, empresa, "", "[" + empresa.ToUpper() + "] - Comunicação Extra Judicial - " + empresa, html, true);

                    boleto.DataCEJ = DateTime.Now;
                    boletoRepository.Save(boleto);
                    return Json("OK");
                }
            }
            return Json("Não foi possível enviar o e-mail.");
        }
         
        [HttpPost]
        public JsonResult EnviaCobranca(int? id)
        {
            if (id.HasValue)
            {
                var boleto = boletoRepository.Get(id.Value);
                if (boleto != null)
                {
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/CEJ.htm")); 
                    var assinatura = "Obrigado,<br>Equipe Coroas Para Velório<br>" +
                                       "Atendimento 0800-777-1986<br>" +
                                        "www.coroasparavelorio.com.br";

                    var logo = "/content/images/pdf/logo-coroas.png";
                    var culture = new System.Globalization.CultureInfo("pt-BR");
                    var day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);
                    var empresa = "Coroas para Velório"; 
                    var email = "";
                    var nome = "";

                    if (boleto.FaturamentoID.HasValue)
                    {
                        assinatura = "Obrigado,<br>Equipe Laços Corporativos<br>" +
                                           "Atendimento 0800-777-1986<br>" +
                                            "www.lacoscorporativos.com.br";

                        logo = "/content/images/pdf/logo-lacos.jpg";
                        empresa = "Laços Corporativos"; 
                        email = boleto.Faturamento.Empresa.EmailContato;
                        nome = boleto.Faturamento.Empresa.NomeCompleto;
                    }
                    else if (boleto.PedidoID.HasValue)
                    {
                        email = boleto.Pedido.EmailSolicitante;
                        nome = boleto.Pedido.NomeSolicitante;
                        if (boleto.Pedido.OrigemSite.ToUpper() == "LAÇOS FLORES")
                        {
                            html = System.IO.File.ReadAllText(Server.MapPath("/content/html/lacosflores/CEJ.htm"));
                            empresa = "Laços Flores";
                        }
                    }

                    var texto = 
                      "Prezado Sr(a). " + boleto.SacadoNome + " <br><br>" +
                      "Houve algum problema no pagamento do nosso boleto?<br><br>" +
                      "Nessa data seu boleto está sujeito a protesto em cartório.<br><br>" +
                      "Favor entrar em contato conosco com <b>Urgência</b>.";

                    //ENVIAR EMAIL 
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[LOGO]", Domain.Core.Configuracoes.DOMINIO + logo);
                    html = html.Replace("[ASSINATURA]", assinatura);
                    html = html.Replace("[MENSAGEM]", texto);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FINANCEIRO, empresa, email, nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, empresa, "", "[" + empresa.ToUpper() + "] - Aviso de cobrança - " + empresa, html, true);

                    boleto.DataCobranca = DateTime.Now;
                    boletoRepository.Save(boleto);
                    return Json("OK");
                }
            }
            return Json("Não foi possível enviar o e-mail.");
        }

        public JsonResult AlterarVencimentoBoletoPedido(int id, string data)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = pedido.Boleto;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                    if (boleto.RemessaBoletoes.Count > 0)
                        boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;
                    boleto.DataVencimento = datavencimento;
                    boleto.DataEnvioLembreteVencimento = null;
                    boletoRepository.Save(boleto);

                    var Fatura = boleto.Faturamento;
                    Fatura.DataVencimento = datavencimento;

                    faturamentoRepository.Save(Fatura);

                    return Json(new { retorno = "OK", id = boleto.ID, data = data });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "Data inválida" });

            }
            else
                return Json(new { retorno = "ERRO", mensagem = "O pedido não pode ser alterado. Tente novamente mais tarde" }); 
        }

        public JsonResult AlterarVencimentoBoletoFatura(int id, string data)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = fatura.Boleto;
                    boleto.Processado = false;
                    if (boleto.RemessaBoletoes.Count > 0)
                        boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;
                    boleto.DataVencimento = datavencimento;
                    boleto.DataEnvioLembreteVencimento = null;
                    boletoRepository.Save(boleto);

                    var Fatura = boleto.Faturamento;
                    Fatura.DataVencimento = datavencimento;

                    faturamentoRepository.Save(Fatura);

                    return Json(new { retorno = "OK", id = boleto.ID, data = data });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "Data inválida" });

            }
            else
                return Json(new { retorno = "ERRO", mensagem = "O pedido não pode ser alterado. Tente novamente mais tarde" }); 
        }
    }
}
