﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Floricultura|Financeiro", "/Home/AccessDenied")]
    public class FornecedorGenericoController : CrudController<FornecedorGenerico>
    {
        #region Variaveis
        private IPersistentRepository<FornecedorGenerico> FornecedorGenericoRepository;
        private IPersistentRepository<FornecedorGenericoBanco> FornecedorGenericoBancoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<FornecedorGenericoConta> fornecedorContaRepository;
        private IPersistentRepository<FornecedorGenericoTipoConta> FornecedorGenericoTipoContaRepository;
        private IPersistentRepository<FornecedorGenericoConta> FornecedorGenericoContaRepository;

        private AdminModel adminModel;
        #endregion

        public FornecedorGenericoController(ObjectContext context)
            : base(context) {
            adminModel = new AdminModel(context);
            FornecedorGenericoRepository = new PersistentRepository<FornecedorGenerico>(context);
            FornecedorGenericoBancoRepository = new PersistentRepository<FornecedorGenericoBanco>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            fornecedorContaRepository = new PersistentRepository<FornecedorGenericoConta>(context);
            FornecedorGenericoTipoContaRepository = new PersistentRepository<FornecedorGenericoTipoConta>(context);
            FornecedorGenericoContaRepository = new PersistentRepository<FornecedorGenericoConta>(context);


        }
        public ActionResult Novo()
        {
            ViewBag.Cidades = cidadeRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Bancos = FornecedorGenericoBancoRepository.GetAll().OrderBy(r => r.NomeBanco).ToList();
            ViewBag.TiposConta = FornecedorGenericoTipoContaRepository.GetAll().OrderBy(r => r.Sigla).ToList();
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            FornecedorGenerico novoFornecedor = new FornecedorGenerico();
            novoFornecedor.NomeFornecedor = Request.Form["NomeFornecedor"].ToUpper();
            novoFornecedor.TipoID = int.Parse(Request.Form["TipoID"].ToUpper());
            novoFornecedor.Documento = Request.Form["Documento"].ToUpper().Replace("-", "").Replace(".", "").Replace("/", "");
            novoFornecedor.Telefone = Request.Form["Telefone"].ToUpper();
            novoFornecedor.EstadoID = int.Parse(Request.Form["EstadoID"].ToUpper());
            novoFornecedor.CidadeID = int.Parse(Request.Form["CidadeID"].ToUpper());            

            if(!string.IsNullOrEmpty(Request.Form["Logradouro"]))
                novoFornecedor.Logradouro = Request.Form["Logradouro"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Numero"]))
                novoFornecedor.Numero = Request.Form["Numero"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Complemento"]))
                novoFornecedor.Complemento = Request.Form["Complemento"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["CEP"]))
                novoFornecedor.CEP = Request.Form["CEP"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Bairro"]))
                novoFornecedor.Bairro = Request.Form["Bairro"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Obs"]))
                novoFornecedor.Obs = Request.Form["Obs"].ToUpper();


            if (!string.IsNullOrEmpty(Request.Form["Email"]))
            {
                novoFornecedor.Email = Request.Form["Email"].ToLower();
            }
            else
            {
                novoFornecedor.Email = null;
            }
            novoFornecedor.DataCadastro = DateTime.Now;

            ViewBag.Cidades = cidadeRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(r => r.Nome).ToList();
            
            if (FornecedorGenericoRepository.GetByExpression(r => r.Documento.ToLower() == novoFornecedor.Documento.ToLower()).ToList().Count > 0)
            {
                ViewBag.Bancos = FornecedorGenericoBancoRepository.GetAll().OrderBy(r => r.NomeBanco).ToList();
                ViewBag.TiposConta = FornecedorGenericoTipoContaRepository.GetAll().OrderBy(r => r.Sigla).ToList();
                ViewBag.Erro = "Este CPF/CNPJ já está sendo utilizado por outro cliente";
                return View(novoFornecedor);
            }

            FornecedorGenericoRepository.Save(novoFornecedor);

            var ContaFornecedor = new FornecedorGenericoConta();

            if(!string.IsNullOrEmpty(Request.Form["Agencia"]) && !string.IsNullOrEmpty(Request.Form["Conta"]) && !string.IsNullOrEmpty(Request.Form["ContaDig"]) && !string.IsNullOrEmpty(Request.Form["TiposConta"]) && !string.IsNullOrEmpty(Request.Form["FornecedorGenericoBancoID"]))
            {
                ContaFornecedor.FornecedorGenerico = novoFornecedor;
                ContaFornecedor.Agencia = Request.Form["Agencia"];
                ContaFornecedor.AgenciaDigito = Request.Form["AgenciaDig"];
                ContaFornecedor.Conta = Request.Form["Conta"];
                ContaFornecedor.ContaDigito = Request.Form["ContaDig"];
                ContaFornecedor.Favorecido = Request.Form["Favorecido"];
                ContaFornecedor.FornecedorGenericoTipoConta = FornecedorGenericoTipoContaRepository.Get(int.Parse(Request.Form["TiposConta"]));
                ContaFornecedor.FornecedorGenericoBanco = FornecedorGenericoBancoRepository.Get(int.Parse(Request.Form["FornecedorGenericoBancoID"]));

                fornecedorContaRepository.Save(ContaFornecedor);
            }

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Fornecedor " + novoFornecedor.NomeFornecedor.ToUpper() + "foi criado com sucesso!" });
        }
        public override ActionResult List(params object[] args)
        {
            List<FornecedorGenerico> lsFornecedores = new List<FornecedorGenerico>();
            lsFornecedores = FornecedorGenericoRepository.GetAll().OrderBy(r => r.NomeFornecedor).ToList();

            ViewBag.cidadeRepository = cidadeRepository;

            var nome = Request.QueryString["nome"];
            var email = Request.QueryString["email"];
            var documento = Request.QueryString["documento"];
            var tipoid = Request.QueryString["tipoid"];

            if (!String.IsNullOrEmpty(nome))
                lsFornecedores = lsFornecedores.Where(c => c.NomeFornecedor.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                lsFornecedores = lsFornecedores.Where(c => c.Email.ToLower().Contains(email.ToLower())).ToList();

            if (!String.IsNullOrEmpty(documento))
                lsFornecedores = lsFornecedores.Where(c => c.Documento.ToLower().Contains(documento.ToLower())).ToList();

            if (!String.IsNullOrEmpty(tipoid))
                lsFornecedores = lsFornecedores.Where(c => c.TipoID == int.Parse(tipoid)).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lsFornecedores.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lsFornecedores = lsFornecedores.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(lsFornecedores);
        }
        public ActionResult Visualizar(int id)
        {
            var Fornecedor = FornecedorGenericoRepository.Get(id);

            if (Fornecedor != null)
            {
                ViewBag.Cidades = cidadeRepository.GetAll().OrderBy(r => r.Nome).ToList();
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(r => r.Nome).ToList();
                ViewBag.Bancos = FornecedorGenericoBancoRepository.GetAll().OrderBy(r => r.NomeBanco).ToList();
                ViewBag.TiposConta = FornecedorGenericoTipoContaRepository.GetAll().OrderBy(r => r.Sigla).ToList();
                return View(Fornecedor);
            }
            else
            {
                return RedirectToAction("List");
            }             
        }
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            FornecedorGenerico novoFornecedor = FornecedorGenericoRepository.Get(int.Parse(Request.Form["ID"]));
            novoFornecedor.NomeFornecedor = Request.Form["NomeFornecedor"].ToUpper();
            novoFornecedor.TipoID = int.Parse(Request.Form["TipoID"].ToUpper());
            novoFornecedor.Documento = Request.Form["Documento"].ToUpper().Replace("-", "").Replace(".", "").Replace("/", "");
            novoFornecedor.Telefone = Request.Form["Telefone"].ToUpper();
            novoFornecedor.EstadoID = int.Parse(Request.Form["EstadoID"].ToUpper());
            novoFornecedor.CidadeID = int.Parse(Request.Form["CidadeID"].ToUpper());            

            if (!string.IsNullOrEmpty(Request.Form["Logradouro"]))
                novoFornecedor.Logradouro = Request.Form["Logradouro"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Numero"]))
                novoFornecedor.Numero = Request.Form["Numero"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Complemento"]))
                novoFornecedor.Complemento = Request.Form["Complemento"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["CEP"]))
                novoFornecedor.CEP = Request.Form["CEP"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Bairro"]))
                novoFornecedor.Bairro = Request.Form["Bairro"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Obs"]))
                novoFornecedor.Obs = Request.Form["Obs"].ToUpper();

            if (!string.IsNullOrEmpty(Request.Form["Email"]))
            {
                novoFornecedor.Email = Request.Form["Email"].ToLower();
            }
            else
            {
                novoFornecedor.Email = null;
            }
            if (FornecedorGenericoRepository.GetByExpression(r => r.Documento.ToLower() == novoFornecedor.Documento.ToLower() && r.ID != novoFornecedor.ID).ToList().Count > 0)
            {
                ViewBag.Cidades = cidadeRepository.GetAll().OrderBy(r => r.Nome).ToList();
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(r => r.Nome).ToList();
                ViewBag.Bancos = FornecedorGenericoBancoRepository.GetAll().OrderBy(r => r.NomeBanco).ToList();
                ViewBag.TiposConta = FornecedorGenericoTipoContaRepository.GetAll().OrderBy(r => r.Sigla).ToList();

                ViewBag.Erro = "Este CPF/CNPJ já está sendo utilizado por outro cliente";
                return View("Visualizar", novoFornecedor);
            }

            FornecedorGenericoRepository.Save(novoFornecedor);


            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Fornecedor " + novoFornecedor.NomeFornecedor.ToUpper() + "foi criado com sucesso!" });
        }

        public JsonResult AdicionarConta(int FornecedorID, int BancoID, int TipoContaID, string Agencia, string AgenciaDigito, string Conta, string ContaDigito, string Favorecido)
        {
            var ContaFornecedor = new FornecedorGenericoConta();
            ContaFornecedor.FornecedorGenerico = FornecedorGenericoRepository.Get(FornecedorID);

            ContaFornecedor.Agencia = Agencia;
            ContaFornecedor.AgenciaDigito = AgenciaDigito;

            ContaFornecedor.Conta = Conta;
            ContaFornecedor.ContaDigito = ContaDigito;

            ContaFornecedor.Favorecido = Favorecido.ToUpper();

            ContaFornecedor.FornecedorGenericoTipoConta = FornecedorGenericoTipoContaRepository.Get(TipoContaID);
            ContaFornecedor.FornecedorGenericoBanco = FornecedorGenericoBancoRepository.Get(BancoID);

            fornecedorContaRepository.Save(ContaFornecedor);

            return Json("OK");

        }
        public JsonResult AlterarConta(int ContaID, int FornecedorID, int BancoID, int TipoContaID, string Agencia, string AgenciaDigito, string Conta, string ContaDigito, string Favorecido)
        {
            var ContaFornecedor = FornecedorGenericoContaRepository.Get(ContaID);
            ContaFornecedor.FornecedorGenerico = FornecedorGenericoRepository.Get(FornecedorID);

            ContaFornecedor.Agencia = Agencia;
            ContaFornecedor.AgenciaDigito = AgenciaDigito;

            ContaFornecedor.Conta = Conta;
            ContaFornecedor.ContaDigito = ContaDigito;

            ContaFornecedor.Favorecido = Favorecido.ToUpper();

            ContaFornecedor.FornecedorGenericoTipoConta = FornecedorGenericoTipoContaRepository.Get(TipoContaID);
            ContaFornecedor.FornecedorGenericoBanco = FornecedorGenericoBancoRepository.Get(BancoID);

            fornecedorContaRepository.Save(ContaFornecedor);

            return Json("OK");

        }

        public JsonResult ExcluirConta(int ContaID)
        {
            var ContaFornecedor = FornecedorGenericoContaRepository.Get(ContaID);
            FornecedorGenericoContaRepository.Delete(ContaFornecedor);
            return Json("OK");
        }
    }
}