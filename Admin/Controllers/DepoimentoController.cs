﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.IO;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Comunicacao", "/Home/AccessDenied")]
    public class DepoimentoController : CrudController<Depoimento>
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;

        public DepoimentoController(ObjectContext context)
            : base(context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
        }


        public override ActionResult List(params object[] args)
        {
            var resultado = depoimentoRepository.GetAll().OrderByDescending(p => p.Data).ToList();
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }


        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Depoimento depoimento = null;
            if (id.HasValue)
            {
                depoimento = depoimentoRepository.Get(id.Value);
            }
            else
            {
                depoimento = new Depoimento();
            }
            ViewBag.Produtos = produtoRepository.GetAll().OrderBy(p => p.Nome).ToList();
            return View(depoimento);
        }
    }
}
