﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro|Fornecedores|Qualidade|Atendimento|FloriculturaMisto", "/Home/AccessDenied")]

    public class NFeController : CrudController<vwNFe>
    {

        private IPersistentRepository<vwNFe> NFeRepository;

        public NFeController(ObjectContext context)
            : base(context)
        {
            NFeRepository = new PersistentRepository<vwNFe>(context);
        } 

        public override ActionResult List(params object[] args)
        {
            var resultado = new List<vwNFe>();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);
            datafinal = datafinal.AddDays(1).AddMilliseconds(-1);

            resultado = NFeRepository.GetByExpression(r => r.NFeData.HasValue && r.DataCriacao >= datainicial && r.DataCriacao <= datafinal).ToList().OrderByDescending(c => c.DataCriacao).ToList();
           
            var nome = Request.QueryString["nome"];
            var pedido = Request.QueryString["pedido"];
            var email = Request.QueryString["email"]; 
            var origem = Request.QueryString["origem"];
            var enviada = Request.QueryString["enviada"];
         
            if (datainicial != new DateTime())
                resultado = resultado.Where(c => Domain.Core.Funcoes.TruncateTime(c.DataCriacao) >= datainicial).ToList();

            if (datafinal != new DateTime())
                resultado = resultado.Where(c => Domain.Core.Funcoes.TruncateTime(c.DataCriacao) <= datafinal.AddDays(1).AddMilliseconds(-1)).ToList();

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.NomeSolicitante.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.EmailSolicitante.ToLower().Trim().Contains(email.Trim().ToLower())).ToList();

            if (!String.IsNullOrEmpty(pedido))
            { 
                resultado = resultado.Where(c => c.Pedido == pedido || c.Fatura.ToString() == pedido).ToList();
            }

            if (!String.IsNullOrEmpty(origem))
            {
                resultado = resultado.Where(c => c.OrigemSite == origem).ToList();
            }
            if (!String.IsNullOrEmpty(enviada))
            {
                resultado = resultado.Where(c => c.DataEmailNFe.HasValue).ToList();
            }

            var origens = resultado.OrderBy(s => s.OrigemSite).Select(s => s.OrigemSite).Distinct().Where(s => s != null).ToList();
            ViewBag.Origem = origens;

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        public ActionResult Excel(params object[] args)
        {
            var resultado = new List<vwNFe>();
            var data = DateTime.Now.AddDays(-30);

            resultado = NFeRepository.GetAll().OrderByDescending(c => c.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var origem = Request.QueryString["origem"];
            var enviada = Request.QueryString["enviada"];
            var nfe = Request.QueryString["nfe"];

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao <= datafinal.AddDays(1).AddMilliseconds(-1)).ToList();

            if (!String.IsNullOrEmpty(nome))
                resultado = NFeRepository.GetByExpression(c => c.NomeSolicitante.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = NFeRepository.GetByExpression(c => c.EmailSolicitante.ToLower().Trim().Contains(email.Trim().ToLower())).ToList();

            if (!String.IsNullOrEmpty(numero))
            {
                resultado = NFeRepository.GetByExpression(c => c.Pedido == numero || c.Fatura.ToString() == numero).ToList();
            }

            if (!String.IsNullOrEmpty(origem))
            {
                resultado = NFeRepository.GetByExpression(c => c.OrigemSite == origem).ToList();
            }
            if (!String.IsNullOrEmpty(enviada))
            {
                resultado = NFeRepository.GetByExpression(c => c.DataEmailNFe.HasValue).ToList();
            }
            if (!String.IsNullOrEmpty(nfe))
            {
                resultado = NFeRepository.GetByExpression(c => c.NFeData.HasValue).ToList();
            }


            var retorno = "";
            retorno += "<table border='1px'>";
            retorno += "<tr style='color:#fff;background-color:darkblue;'><th>Data</th><th>Nome/Razao Social</th><th>E-mail</th><th>Pedido</th><th>Fatura</th><th>Possui NFe?</th><th>NFe Enviada?</th><th>Data NFe</th><th>Link NFe</th><th>Origem</th></tr>";

            foreach (var item in resultado)
            {
                retorno += "<tr>";
                retorno += "<td>" + item.DataCriacao.ToString("dd/MM/yyyy") + "</td><td>" + item.NomeSolicitante + "</td><td>" + item.EmailSolicitante + "</td><td>" + item.Pedido + "</td><td>" + item.Fatura + "</td><td>" + (item.NFeNumero.HasValue ? "<b>SIM</b>" : "NÃO") + "</td><td>" + (item.NFeData.HasValue ? "<b>SIM</b>" : "NÃO") + "</td><td>" + (item.NFeData.HasValue ? item.NFeData.Value.ToString("dd/MM/yyyy HH:mm") : "") + "</td><td>" + item.NFeLink + "</td><td>" + item.OrigemSite + "</td>";                 
                retorno += "</tr>"; 
            }

            retorno += "</table>";

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=RelatorioNotasFiscais-" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            Response.Write(retorno);
            Response.End();

            return View();
        }
    }
}
