﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.IO;
using System.Collections.Specialized;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class PromocaoController : Controller
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Promocao> promocaoRepository;
        private IPersistentRepository<PromocaoProduto> promocaoProdutoRepository; 

        public PromocaoController(ObjectContext context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            promocaoRepository = new PersistentRepository<Promocao>(context);
            promocaoProdutoRepository = new PersistentRepository<PromocaoProduto>(context);
        }

        public ActionResult List()
        {
            var promocoes = promocaoRepository.GetAll().OrderByDescending(p => p.DataInicio).ToList();
            return View(promocoes);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Promocao promocao = null;
            if (id.HasValue)
            {
                promocao = promocaoRepository.Get(id.Value);
            }
            ViewBag.Produtos = produtoRepository.GetAll().OrderBy(f => f.Nome);
            return View(promocao);
        }


        [HttpPost]
        public ActionResult Edit(Promocao promocao)
        {
            promocaoRepository.Save(promocao);

            foreach (var promocaoproduto in promocao.PromocaoProdutoes.ToList())
                promocaoProdutoRepository.Delete(promocaoproduto);


            if (Request.Form["produtos"] != null)
            {
                foreach (var id in Request.Form["produtos"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        promocao.PromocaoProdutoes.Add(new PromocaoProduto() { ProdutoID = Convert.ToInt32(_id), PromocaoID = promocao.ID });
                    }
                }
            }
            promocaoRepository.Save(promocao);


            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

        public ActionResult Delete(int id)
        {
            var promocao = promocaoRepository.Get(id);

            if (promocao != null)
            {
                promocaoRepository.Delete(promocao);

                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "DeleteFailure" });
            }
        }

    }
}
