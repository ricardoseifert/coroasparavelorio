﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using System.Globalization;
using Domain.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|RH", "/Home/AccessDenied")]
    public class RamaisController : CrudController<Ramai>
    {
        private IPersistentRepository<Administrador> adminRepository;
        private IPersistentRepository<Ramai> ramaisRepository;
        private AdministradorPedidoRepository admPedidoRepository;

        private AdminModel adminModel;

        public RamaisController(ObjectContext context)
            : base(context)
        {
            adminRepository = new PersistentRepository<Administrador>(context);
            ramaisRepository = new PersistentRepository<Ramai>(context);
            admPedidoRepository = new AdministradorPedidoRepository(context);
            adminModel = new AdminModel(context);
        }


        public override ActionResult List(params object[] args)
        {
            var resultado = ramaisRepository.GetAll().OrderBy(p => p.NumeroRamal).ToList();

            if (!string.IsNullOrEmpty(Request.QueryString["NumeroRamal"]))
            {
                int NumeroRamal = int.Parse(Request.QueryString["NumeroRamal"]);

                if (NumeroRamal > 0)
                    resultado = resultado.Where(c => c.NumeroRamal == NumeroRamal).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Ramai ramal = null;
            if (id.HasValue)
            {
                ramal = ramaisRepository.Get(id.Value);
            }            
            return View(ramal);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Ramai ramal)
        {
            if (ramaisRepository.GetByExpression(c => c.ID != ramal.ID && c.NumeroRamal == ramal.NumeroRamal).Count() == 0)
            {
                ramaisRepository.Save(ramal);
            }
            else
            {
                ViewBag.Erro = "O ramal digitado já existe.";               
                return View(ramal);
            }

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }
    }
}
