﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;

namespace Admin.Controllers {
    public class RelatoriosController : Controller {
        private RelatorioRepository relatorioRepository;

        public RelatoriosController(ObjectContext context) {
            relatorioRepository = new RelatorioRepository(context);
        }

        public ActionResult Index(int Id) {
            return View();
        }

        public string GetVendasPjPf(DateTime datainicio, DateTime datafim) {
            var vendasTipo = relatorioRepository.GetRelatorioTipoPessoa(datainicio, datafim.AddSeconds(-86399)).ToList();

            var result = "0";
            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica) != null) {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total > 0) {
                    result += vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total.ToString() + ",";
                }
                else {
                    result += "0,";
                }
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total > 0) {
                    result += vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total.ToString();
                }
                else {
                    result += "0";
                }
            }

            return result;
        }


    }
}
