﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Artem.Google.Net;
using MvcExtensions.Security.Filters;
using Admin.ViewModels;
using ImageResizer;

namespace Admin.Controllers
{
    public class LocalController : CrudController<Local>
    {

        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<FornecedorLocal> fornecedorLocalRepository;
        private AdminModel adminModel;

        public LocalController(ObjectContext context)
            : base(context)
        {
            localRepository = new PersistentRepository<Local>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            fornecedorLocalRepository = new PersistentRepository<FornecedorLocal>(context);
            adminModel = new AdminModel(context);
        }

        [AreaAuthorization("Admin", "/Home/Login")]
        [AllowGroup("Socios|TI|Coordenadores|Comunicacao|Fornecedores|Qualidade|FloriculturaMisto", "/Home/AccessDenied")]
        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;

            #region FILTRO LOCAIS

            List<Local> LstLocais = new List<Local>();

            if (string.IsNullOrEmpty(Request.QueryString["EstadoID"]))
            {
                LstLocais = localRepository.GetByExpression(r => r.EstadoID == 26).ToList().OrderBy(p => p.Tipo).OrderBy(p => p.Titulo).ToList();
            }
            else
            {
                var Localid = int.Parse(Request.QueryString["EstadoID"]);
                LstLocais = localRepository.GetByExpression(r => r.EstadoID == Localid).ToList().OrderBy(p => p.Tipo).OrderBy(p => p.Titulo).ToList();
            }

            if (Request.QueryString["termo"] != null)
            {
                var termoPesquisa = Request.QueryString["termo"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower();
                if (termoPesquisa != null)
                    LstLocais = LstLocais.Where(c => c.Titulo.Trim().ToLower().Contains(termoPesquisa.Trim().ToLower())).OrderBy(c => c.Titulo).ToList();
            }

            if (Request.QueryString["Cidade"] != null)
            {
                LstLocais = LstLocais.Where(r => r.Cidade.Nome.ToLower().Contains(Request.QueryString["Cidade"].ToLower())).ToList();
            }

            #endregion

            #region ESTADOS PARA BUSCA

            var estados = estadoRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };

            List<Estado> lstEstados = new List<Estado>();

            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }

            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));
            ViewBag.Estados = lstEstados;

            #endregion

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = LstLocais.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                LstLocais = LstLocais.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(LstLocais);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Local local = null;
            ViewBag.Admin = adminModel.CurrentAdministrador;
            if (id.HasValue)
            {
                local = localRepository.Get(id.Value);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == local.EstadoID).OrderBy(e => e.Nome);
                try
                {
                    ViewBag.Fornecedores = local.FornecedorLocals.Select(c => c.Fornecedor).OrderBy(e => e.Nome);
                }
                catch
                {
                    ViewBag.Fornecedores = new List<Cidade>();
                }
                ViewBag.TodosFornecedores = fornecedorRepository.GetByExpression(e => e.FornecedorCidades.Count(c => c.CidadeID == local.CidadeID) > 0).OrderBy(e => e.Nome);
            }
            else
            {
                ViewBag.Cidades = new List<Cidade>();
                ViewBag.Fornecedores = new List<Fornecedor>();
                ViewBag.TodosFornecedores = new List<Fornecedor>();
            }
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(local);
        }
        //TODO: Qdo existir o rótulo no redirect, deletar ao inserir ou editar

        [HttpGet]
        public ActionResult Ordem(int id)
        {
            Local local = localRepository.Get(id);
            if (local != null)
            {
                return View(local);
            }
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Ordem(FormCollection form)
        {
            Local local = localRepository.Get(Convert.ToInt32(form["ID"]));

            var TemOrdem = local.FornecedorLocals.Where(r => r.Ordem > 0).Any();

            int Count = 1;
            if (local != null)
            {
                foreach (var fornecedor in local.FornecedorLocals)
                {
                    if (TemOrdem)
                    {
                        fornecedor.Ordem = Convert.ToInt32(form["ordem_" + fornecedor.ID]);
                    }
                    else
                    {
                        fornecedor.Ordem = Count;
                        Count++;
                    }
                    fornecedorLocalRepository.Save(fornecedor);
                }
                return RedirectToAction("Ordem", new { id = local.ID, cod = "SaveSucess" });
            }
            else
                return RedirectToAction("Ordem", new { cod = "SaveFailure" });
        }

        [HttpPost]
        [ValidateInput(false)]
        public override ActionResult Edit(Local local)
        {
            if (local.ID == 0 || local.Foto == null)
                local.Foto = "";


            if (localRepository.GetByExpression(c => c.RotuloUrl.Trim().ToLower() == local.RotuloUrl.Trim().ToLower() && c.ID != local.ID).Count() > 0)
            {
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == local.EstadoID).OrderBy(e => e.Nome);
                ViewBag.Fornecedores = local.FornecedorLocals.Select(c => c.Fornecedor).OrderBy(e => e.Nome);
                ViewBag.TodosFornecedores = fornecedorRepository.GetByExpression(e => e.FornecedorCidades.Count(c => c.CidadeID == local.CidadeID) > 0).OrderBy(e => e.Nome);
                

                ViewBag.Erro = "Esta URL já está sendo utilizada por outro local";

                return View(local);
            }
            else
            {
                ViewBag.Cidades = new List<Cidade>();
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);



                if (adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Atendimento ||
                    adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Fornecedores)
                {
                    string telefone = local.Telefone;
                    local = localRepository.GetByExpression(l => l.ID == local.ID).FirstOrDefault();
                    local.Telefone = telefone;
                }
                localRepository.Save(local);

                var fotoUpload = Request.Files["File"];

                if (fotoUpload != null && fotoUpload.ContentLength > 0)
                {
                    Image foto = Image.FromStream(fotoUpload.InputStream);

                    //validacao tamanho fixo de imagem 555 px
                    bool fotoValida = (foto.Height == 555 && foto.Width == 555);

                    if (fotoValida)
                    {
                        //Verifica a existencia do diretorio de destino
                        string desFolder = Server.MapPath("/content/locais/") + local.ID.ToString();
                        if (!Directory.Exists(desFolder)) Directory.CreateDirectory(desFolder);

                        try
                        {

                            string _imgNome = Domain.Core.Funcoes.GeraNomeArquivoMVC(fotoUpload.FileName);
                        string _imgThumb = Server.MapPath("/content/locais/" + local.ID + "/" + local.Foto);


                        string _imgDiretorioDetino = Server.MapPath("/content/locais/" + local.ID + "/" + _imgNome);
                        string _imgThumbDiretorioDetino = Server.MapPath("/content/locais/" + local.ID + "/thumb/" + local.Foto);

                        var _imgResizeSettings = new ResizeSettings("width=555&height=555&crop=auto;format=jpg;mode=crop;scale=both");

                        var _imgThumbResizeSettings = new ResizeSettings("width=230&height=130&anchor=middlecenter;format=jpg;mode=crop;scale=both");

                        //var _imgIntrucoes = "width=555&height=555&crop=auto;format=jpg;mode=crop;scale=both";

                        ////FOTO
                        fotoUpload.InputStream.Seek(0, SeekOrigin.Begin);
                       
                        ImageJob img = new ImageJob(fotoUpload, _imgDiretorioDetino, _imgResizeSettings, false, false);
                        // ImageJob img = new ImageJob(fotoUpload, _imgDiretorioDetino, new ResizeSettings(), false, true);

                        //Verifica a existencia do arquivo no diretorio de destino
                        bool existeArquivo = System.IO.File.Exists(_imgDiretorioDetino);

                        if (existeArquivo)
                        {
                            System.IO.File.Delete(_imgDiretorioDetino);
                        }

                        img.CreateParentDirectory = true;
                        img.Build();

                        local.Foto = _imgNome;
                        localRepository.Save(local);

                        //THUMB
                        //imgThumb.InputStream.Seek(0, SeekOrigin.Begin);
                        img = new ImageJob(_imgThumb, _imgThumbDiretorioDetino, _imgThumbResizeSettings);

                        bool existeArquivoThumb = System.IO.File.Exists(_imgThumbDiretorioDetino);

                        if (existeArquivoThumb)
                        {
                            System.IO.File.Delete(_imgThumbDiretorioDetino);
                        }

                        img.CreateParentDirectory = true;
                        img.Build();

                        }
                        catch { }

                    }
                    else
                    {
                        ViewBag.Erro = "As dimensões da foto não são válidas !";
                        return RedirectToAction("Edit", new { cod = "SaveFailure", msg = ViewBag.Erro });
                    }
                }
                //else
                //{ 
                //    ViewBag.Erro = "Foto não selecionada ou não alterada!";
                //    return RedirectToAction("Edit", new { cod = "SaveFailure", msg = ViewBag.Erro });
                //}
                foreach (var fornecedorlocal in local.FornecedorLocals.ToList())
                        fornecedorLocalRepository.Delete(fornecedorlocal);

                    if (Request.Form["box2View"] != null)
                    {
                        foreach (var id in Request.Form["box2View"].Split(','))
                        {
                            int _id = 0;
                            Int32.TryParse(id.ToString(), out _id);
                            if (_id > 0)
                            {
                                local.FornecedorLocals.Add(new FornecedorLocal() { FornecedorID = Convert.ToInt32(_id), LocalID = local.ID });
                            }
                        }
                        localRepository.Save(local);
                    }

                    var forcarAtualizacao = Request.Form["forcarAtualizacao"] == "on";
                    if (!String.IsNullOrEmpty(local.CEP))
                    {
                        if (local.Latitude == null || local.Latitude == 0 || local.Longitude == null || local.Longitude == 0 || forcarAtualizacao)
                        {
                            if (local.Estado != null)
                            {
                                var endereco = local.CEP + "," + local.Estado.Sigla + ",Brasil";
                                if (!String.IsNullOrEmpty(local.Logradouro))
                                    endereco = local.Logradouro + " " + local.Numero + "," + endereco;
                                GeoRequest request = new GeoRequest(endereco);
                                GeoResponse response = request.GetResponse();
                                GeoLocation location = response.Results[0].Geometry.Location;
                                local.Latitude = Convert.ToDecimal(location.Latitude);
                                local.Longitude = Convert.ToDecimal(location.Longitude);
                                localRepository.Save(local);
                            }
                        }
                    }

                    return RedirectToAction("List", new { cod = "SaveSucess" });

                
            }
        }
    }
}
