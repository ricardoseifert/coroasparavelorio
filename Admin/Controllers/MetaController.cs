﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Admin.ViewModel;
using Admin.ViewModels;
using Domain.Entities;
using Domain.MetodosExtensao;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;

namespace Admin.Controllers {
    //===============================================================
    public class MetaController: Controller {

        #region Variaveis
        //-----------------------------------------------------------
        private IPersistentRepository<Meta> metaRepository;
        private IPersistentRepository<Metrica> metricaRepository;
        private IPersistentRepository<CriterioBusca> criterioRepository;
        private IPersistentRepository<StatusMeta> statusMetaRepository;
        private IPersistentRepository<Administrador> adminRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private AdminModel adminModel;
        //-----------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------
        public MetaController(ObjectContext context) {
            metaRepository = new PersistentRepository<Meta>(context);
            metricaRepository = new PersistentRepository<Metrica>(context);
            criterioRepository = new PersistentRepository<CriterioBusca>(context);
            statusMetaRepository = new PersistentRepository<StatusMeta>(context);
            adminRepository = new PersistentRepository<Administrador>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            adminModel = new AdminModel(context);
        }
        //-----------------------------------------------------------
        #endregion

        #region Actions
        //-----------------------------------------------------------
        public ActionResult Index(){
            ViewBag.adminModel = adminModel;
            
            return View();
        }
        //-----------------------------------------------------------
        public ActionResult Listar(MetaFiltroViewModel filtro){
            ViewBag.adminModel = adminModel;
            if (adminModel.PerfilId == (int)Administrador.Perfis.LacosCorporativos){
                filtro.IdUsuario = adminModel.CurrentAdministrador.ID;
            }

            Expression<Func<Meta, bool>> exp = m => m.IdArea == 1;
            if (filtro.IdUsuario.HasValue){
                exp = exp.And(m => m.IdUsuario.HasValue && m.IdUsuario.Value == filtro.IdUsuario.Value);
            }

            if (filtro.DataInicio.HasValue) {
                exp = exp.And(m => m.DataInicio.HasValue && m.DataInicio.Value >= filtro.DataInicio.Value);
            }
            if (filtro.DataFim.HasValue) {
                filtro.DataFim = new DateTime(filtro.DataFim.Value.Year, filtro.DataFim.Value.Month, filtro.DataFim.Value.Day).AddDays(1).AddSeconds(-1);
                exp = exp.And(m => m.DataFim.HasValue && m.DataFim.Value <= filtro.DataFim.Value);
            }
            if (!string.IsNullOrWhiteSpace(filtro.Nome)){
                exp = exp.And(m => m.NomeMeta.Contains(filtro.Nome));
            }
            if (filtro.IdStatus.HasValue){
                exp = exp.And(m => m.IdStatusMeta == filtro.IdStatus.Value);
            }

            List<Meta> lstMetas = metaRepository.GetByExpression(exp).ToList();
            
            return View(lstMetas);
        }
        //-----------------------------------------------------------
        public ActionResult Deletar(int id){
            Meta meta = metaRepository.GetByExpression(m=>m.Id == id).FirstOrDefault();
            List<Metrica> lstMetricas = metricaRepository.GetByExpression(m => m.IdMeta == id).ToList();

            foreach (Metrica metrica in lstMetricas){
                metricaRepository.Delete(metrica);
            }

            metaRepository.Delete(meta);
            return RedirectToAction("Index");
        }
        //-----------------------------------------------------------
        public ActionResult Editar(int? id){
            MetaViewModel metaView = new MetaViewModel();
            if (id.HasValue){
                Meta meta = metaRepository.GetByExpression(m => m.Id == id.Value).FirstOrDefault();
                if (meta != null){
                    metaView.Id = meta.Id;

                    metaView.Nome = meta.NomeMeta;
                    metaView.IdCriterioBusca = meta.IdCriterioBusca;
                    metaView.DataInicio = meta.DataInicio;
                    metaView.DataFim = meta.DataFim;
                    metaView.IdUsuario = meta.IdUsuario.HasValue && meta.IdUsuario.Value > 0 ? meta.IdUsuario : null;
                    metaView.IdStatus = meta.IdStatusMeta;
                    foreach (Metrica metrica in meta.Metricas){
                        MetricaViewModel newMetrica = new MetricaViewModel{
                            Id = metrica.Id,
                            Nome = metrica.NomeMetrica,
                            IdMeta = metrica.IdMeta,
                            IdTipoMetrica = metrica.IdTipoMetrica
                        };
                        switch ((Metrica.TodosTiposMetrica)metrica.IdTipoMetrica){
                            case Metrica.TodosTiposMetrica.Numerico:
                                newMetrica.Valor = Convert.ToInt64(metrica.Valor).ToString();
                            break; 
                            case Metrica.TodosTiposMetrica.Porcentagem:
                            case Metrica.TodosTiposMetrica.Valor:
                                newMetrica.Valor = metrica.Valor.ToString();
                            break;
                        }
                       
                        metaView.Metricas.Add(newMetrica);
                    }
                }
            }
            else{
                metaView.Metricas.Add(new MetricaViewModel { Nome = "Faturamento" ,IdTipoMetrica = (int)Metrica.TodosTiposMetrica.Valor, Valor = "0"});
                metaView.Metricas.Add(new MetricaViewModel { Nome = "Ativação", IdTipoMetrica = (int)Metrica.TodosTiposMetrica.Numerico, Valor = "0" });
                metaView.Metricas.Add(new MetricaViewModel { Nome = "Visitas", IdTipoMetrica = (int)Metrica.TodosTiposMetrica.Numerico , Valor = "0" });
                metaView.IdCriterioBusca = (int)CriterioBusca.TodosTipos.LacosCorp;
            }

            PopularViewBags();
            //mais pra frente dependento do usuário logado podemos ter views diferentes, por exemplo comercial e atendimento
            metaView.IdArea = 1;
            return View("EditarComercial", metaView);
        }
        //-----------------------------------------------------------
        [HttpPost]
        public ActionResult Editar(MetaViewModel metaView){

            if (metaView.DataInicio.HasValue && metaView.DataFim.HasValue) {
                if (metaView.DataFim.Value < metaView.DataInicio.Value) {
                    ModelState["DataFim"].Errors.Add("O término deve ser maior que o início.");
                }
                else if(!ValidarColisoesDatas(metaView.DataInicio.Value,metaView.DataFim.Value,metaView.IdUsuario,metaView.IdArea,metaView.Id)) {
                    ModelState["DataFim"].Errors.Add("Já existe uma meta com vigência neste período.");
                }
                else{
                    ModelState["DataFim"].Errors.Clear();
                }
            }
      

            if (ModelState.IsValid){
                Meta meta = new Meta();
                if (metaView.Id > 0){
                    meta = metaRepository.GetByExpression(m => m.Id == metaView.Id).FirstOrDefault();
                }
                meta.IdArea = metaView.IdArea;
                meta.IdCriterioBusca = metaView.IdCriterioBusca;
                meta.IdStatusMeta = metaView.IdStatus;
                meta.IdUsuario = metaView.IdUsuario.HasValue && metaView.IdUsuario.Value > 0 ? metaView.IdUsuario : null;
                meta.IdGrupo =  null;
                meta.DataInicio = metaView.DataInicio;
                meta.DataFim = metaView.DataFim.HasValue ? new DateTime(metaView.DataFim.Value.Year, metaView.DataFim.Value.Month, metaView.DataFim.Value.Day).AddDays(1).AddSeconds(-1) : metaView.DataFim;
                meta.NomeMeta = metaView.Nome;
                metaRepository.Save(meta);
                
                if (metaView.Metricas != null && metaView.Metricas.Any()){
                    foreach (MetricaViewModel metricaView in metaView.Metricas) {
                        Metrica metrica = new Metrica();
                        metrica.Id = metricaView.Id;
                        metrica.IdMeta = meta.Id;
                        metrica.IdTipoMetrica = metricaView.IdTipoMetrica;
                        metrica.NomeMetrica = metricaView.Nome;
                        metrica.Valor = decimal.Parse(metricaView.Valor);
                        metricaRepository.Save(metrica);
                    }
                }
                
                return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
            }
            
            PopularViewBags();
            return View("EditarComercial", metaView);
        }
        //-----------------------------------------------------------
        public ActionResult Filtrar(){
            MetaFiltroViewModel filtro = new MetaFiltroViewModel();
            ViewBag.adminModel = adminModel;
            filtro.IdArea = 1;
            PopularViewBags();
            
            return View(filtro);
        }
        //-----------------------------------------------------------
        #endregion

        #region Metodos
        //-----------------------------------------------------------
        private bool ValidarColisoesDatas(DateTime datInicio, DateTime datFim, int? idUsuario, int idArea, int idMeta){
            return true;
        }
        //-----------------------------------------------------------
        private void PopularViewBags(){
            List<Administrador> lstUsuarios = new List<Administrador>();
            switch ((Administrador.Perfis)adminModel.PerfilId) {
                case Administrador.Perfis.Coordenadores:
                case Administrador.Perfis.TI:
                    lstUsuarios = empresaRepository.GetAll().Where(e=>e.AtendimentoID.HasValue).Select(a => a.Administrador).Distinct().OrderBy(r => r.Nome).ToList();
                break;
                case Administrador.Perfis.LacosCorporativos:
                    lstUsuarios.Add(adminModel.CurrentAdministrador);
                    break;
                default:
                    lstUsuarios = adminRepository.GetByExpression(r => r.Liberado).OrderBy(r => r.Nome).ToList();
                break;
            }
            
            ViewBag.Colaboradores = new SelectList(lstUsuarios,"ID" , "Nome");

            List<CriterioBusca> lstCriterios = criterioRepository.GetAll().OrderBy(c=>c.Nome).ToList();
            ViewBag.Criterios = new SelectList(lstCriterios,"Id","Nome");

            List<StatusMeta> lstStatusMetas = statusMetaRepository.GetAll().OrderBy(s=>s.Nome).ToList();
            ViewBag.Status = new SelectList(lstStatusMetas, "Id", "Nome");

            ViewBag.Perfis = new SelectList(GetEnumSelectList<Administrador.Perfis>(),"Value","Text");
           
        }
        //-----------------------------------------------------------
        public static IEnumerable<SelectListItem> GetEnumSelectList<T>() {
            return (Enum.GetValues(typeof(T)).Cast<T>()
                .Select(enu => new SelectListItem { Text = enu.ToString(), Value = Convert.ToInt32(enu).ToString() })).OrderBy(e=>e.Text).ToList();
        }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}