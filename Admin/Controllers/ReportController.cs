﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Admin.ViewModels;
using Domain.Entities;
using System.Data.Objects;
using Admin.Interfaces;
using Admin.ViewModel;
using DomainExtensions.Repositories;

namespace Admin.Controllers {
    public class ReportController : Controller {
        private IPersistentRepository<Administrador> adminRepository;
        private AdminModel adminModel;

        public ReportController(ObjectContext context) {
            adminModel = new AdminModel(context);
            adminRepository = new PersistentRepository<Administrador>(context);
        }

        public ActionResult ShowReport(int Id) {
            ViewBag.adminModel = adminModel;

            ViewBag.ReportId = Id;
            ViewBag.Permission = "YES";

            return View("~/Views/Report/Index.cshtml");
        }


        public ActionResult Index() {
            ViewBag.adminModel = adminModel;
            return View();
        }

        public ActionResult LocaisRecentes(){
            //ViewBag.adminModel = adminModel;
            List<IReportable> lstLocais = new List<IReportable>();
            LocalRecente local = new LocalRecente();
            #region Local 01
            local.Cidade = "Cidade 1";
            local.Estado = "Estado 1";
            local.Ativo = true;
            local.Local = "Local 1";
            local.DataCriacao = DateTime.Now;
            lstLocais.Add(local);
            #endregion
            #region Local 02
            local = new LocalRecente();
            local.Cidade = "Cidade 2";
            local.Estado = "Estado 2";
            local.Ativo = false;
            local.Local = "Local 2";
            local.DataCriacao = DateTime.Now;
            local.Idade = 10;
            lstLocais.Add(local);
            #endregion
            #region Local 03
            local = new LocalRecente();
            local.Cidade = "Cidade 3";
            local.Estado = "Estado 3";
            local.Ativo = false;
            local.Local = "Local 3";
            local.DataCriacao = DateTime.Now.AddDays(1);
            lstLocais.Add(local);
            #endregion
            return View("ReportGrid", lstLocais);
        }

    }
}
