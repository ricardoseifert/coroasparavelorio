﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.IO;
using System.Globalization;
using Domain.Repositories;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using Domain.Factories;
using PagarMe;
using System.Text;
using System.Configuration;
using System.Data.EntityClient;

namespace Admin.Controllers
{
    public class CobrancaController : CrudController<Pedido>
    {

        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<BoletoPagarMe> boletoPagarMeRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private PedidoRepository pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<LogMandrill> logMandrillRepository;
        private AdminModel adminModel;
        private IPersistentRepository<ListaNegra> blackListRepository;

        public CobrancaController(ObjectContext context): base(context)
        {
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            localRepository = new PersistentRepository<Local>(context);
            pedidoRepository = new PedidoRepository(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            boletoPagarMeRepository = new PersistentRepository<BoletoPagarMe>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            logMandrillRepository = new PersistentRepository<LogMandrill>(context);
            adminModel = new AdminModel(context);
            blackListRepository = new PersistentRepository<ListaNegra>(context);
        }

        public List<IPedido> Listar()
        {
            var resultado = new List<IPedido>();

            var IdPedidoCPV = Request.QueryString["pedidoCPV"];

            var IdPedidoCORP = 0;
            Int32.TryParse(Request.QueryString["pedidoCORP"], out IdPedidoCORP);

            var Origem = 0;
            Int32.TryParse(Request.QueryString["origem"], out Origem);

            var NomeCliente = "";
            NomeCliente = Request.QueryString["NomeCliente"];

            var EmailCliente = "";
            EmailCliente = Request.QueryString["EmailCliente"];

            var NossoNumero = "";
            NossoNumero = Request.QueryString["NossoNumero"];

            var TransID = "";
            TransID = Request.QueryString["TransID"];

            var meiopagamento = "";
            meiopagamento = Request.QueryString["meiopagamento"];

            var administradorID = "";
            administradorID = Request.QueryString["administradorID"];

            var empresaID = 0;
            Int32.TryParse(Request.QueryString["empresaID"], out empresaID);

            var ClienteID = 0;
            Int32.TryParse(Request.QueryString["ClienteID"], out ClienteID);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);
            datafinal = datafinal.AddDays(1).AddMilliseconds(-1);

            if (string.IsNullOrEmpty(Request.QueryString["datafinal"]))
            {
                // AJUSTA DATA FINAL
                datafinal = DateTime.Now.AddDays(-1);
                datafinal = datafinal.AddDays(1).AddMilliseconds(-1);
            }

            if (!string.IsNullOrEmpty(IdPedidoCPV))
            {
                resultado.AddRange(pedidoRepository.GetByExpression(r => r.Numero == IdPedidoCPV).ToList());
            }
            else if(IdPedidoCORP > 0)
            {
                resultado.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.ID == IdPedidoCORP).ToList());
            }
            else if(empresaID > 0)
            {
                var FaturasAberta = faturamentoRepository.GetByExpression(r => r.Empresa.ID == empresaID && r.DataVencimento <= datafinal && r.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.AguardandoPagamento).ToList();
                foreach (var fatura in FaturasAberta)
                {
                    foreach (var pedido in fatura.PedidoEmpresas)
                    {
                        resultado.Add(pedido);
                    }
                }
            }
            else if(ClienteID > 0)
            {
                var PedidoPagarme = pedidoRepository.GetByExpression(r => r.ClienteID == ClienteID && r.StatusCancelamentoID == null && r.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && (r.BoletoPagarMes.Where(p => (p.DataVencimento.Value <= datafinal && p.StatusPagamentoID == (int)BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento)).Any())).ToList();
                resultado.AddRange(PedidoPagarme);
            }
            else
            {
                #region PREENCHE PEDIDOS CPV

                if(Origem == 0 || Origem == 1)
                {
                    // PREENCHE PEDIDOS CPV - BOLETOS PAGARME
                    var PedidoPagarme = pedidoRepository.GetByExpression(r => r.StatusCancelamentoID == null && r.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && (r.BoletoPagarMes.Where(p => (p.DataVencimento.Value <= datafinal && p.StatusPagamentoID == (int)BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento)).Any())).ToList();

                    // PREENCHE PEDIDOS CPV - BOLETOS
                    var Pedido = pedidoRepository.GetByExpression(r => r.StatusCancelamentoID == null && r.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && r.Boletoes.Where(b => b.DataVencimento <= datafinal && b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento).Any()).ToList();

                    // PREENCHE PEDIDOS CPV - OUTROS PAGAMENTOS
                    var PedidoOutros = pedidoRepository.GetByExpression(r => r.StatusCancelamentoID == null && r.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && r.DataCriacao <= datafinal && r.MeioPagamentoID != (int)Domain.Entities.Pedido.MeiosPagamento.BoletoPagarMe && r.MeioPagamentoID != (int)Domain.Entities.Pedido.MeiosPagamento.Boleto).ToList();

                    resultado.AddRange(PedidoPagarme);
                    resultado.AddRange(Pedido);
                    resultado.AddRange(PedidoOutros);
                }

                #endregion

                #region PREENCHE PEDIDOS CORP

                if (Origem == 0 || Origem == 2)
                {
                    // PREENCHE PEDIDOS CORP - BOLETOS
                    var PedidoCorp = pedidoEmpresaRepository.GetByExpression(r => r.StatusCancelamentoID == null && r.Faturamento.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && (r.Faturamento.Boletoes.Where(x => x.DataVencimento <= datafinal && x.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento).Any())).ToList();

                    // PREENCHE PEDIDOS CORP - OUTROS
                    var PedidoCorpOutros = pedidoEmpresaRepository.GetByExpression(r => r.StatusCancelamentoID == null && r.Faturamento.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && r.Faturamento.DataVencimento <= datafinal && r.Faturamento.MeioPagamentoID != (int)Domain.Entities.Pedido.MeiosPagamento.Boleto).ToList();

                    resultado.AddRange(PedidoCorp);
                    resultado.AddRange(PedidoCorpOutros);
                }

                #endregion
            }

            #region PRRENCHER FILTRO DE ATENDENTES

            SelectList LstAtendentes = new SelectList(
                resultado.Where(r => r.AdministradorPedidoID != null).ToList().Select(o => new KeyValuePair<int, string>((int)o.AdministradorPedidoID, o.AdministradorNome)).Distinct().ToList().Where(r => r.Key != 0).ToList().Select(x => new { Value = x.Key, Text = x.Value }),
                "Value",
                "Text"
            );

            ViewBag.LstAtendentes = LstAtendentes;

            #endregion

            #region REMOVER PEDIDOS DE FATURAS. DEIXAR SOMENTE 1 PEDIDO POR FATURA

            List<PedidoEmpresa> PedidosRemover = new List<PedidoEmpresa>();
            var LstPedidosEmpresa = resultado.Where(r => r.TipoPedido == OrigemPedido.Empresa).ToList().OfType<PedidoEmpresa>().ToList();

            foreach (var item in LstPedidosEmpresa)
            {
                // TEM MAIS DE UM PEDIDO NA FATURA?
                if(LstPedidosEmpresa.Where(r => r.FaturamentoID == item.Faturamento.ID).Count() > 1)
                {
                    var PedidoPodeFicar = LstPedidosEmpresa.Where(r => r.FaturamentoID == item.Faturamento.ID).First();
                    PedidosRemover.AddRange(LstPedidosEmpresa.Where(r => r.FaturamentoID == item.Faturamento.ID && r.ID != PedidoPodeFicar.ID).ToList());
                }
            }

            PedidosRemover = PedidosRemover.Distinct().ToList();
            LstPedidosEmpresa.RemoveAll(p => PedidosRemover.Contains(p));
            resultado = resultado.Where(r => r.TipoPedido == OrigemPedido.Cliente).ToList();
            resultado.AddRange(LstPedidosEmpresa);
            
            #endregion

            #region FILTROS

            if (!string.IsNullOrEmpty(NomeCliente))
                resultado = resultado.Where(r => (r.NomeCliente.ToLower().Contains(NomeCliente.ToLower()) || r.RazaoSocial.ToLower().Contains(NomeCliente.ToLower()) || r.NomeEmpresa.ToLower().Contains(NomeCliente.ToLower()))).ToList();

            if (!string.IsNullOrEmpty(EmailCliente))
                resultado = resultado.Where(r => r.EmailCliente.ToLower().Contains(EmailCliente.ToLower())).ToList();

            if (!string.IsNullOrEmpty(meiopagamento))
                resultado = resultado.Where(r => r.MeioPgtID == int.Parse(meiopagamento)).ToList();

            if (!string.IsNullOrEmpty(administradorID))
                resultado = resultado.Where(r => r.AdministradorPedidoID == int.Parse(administradorID)).ToList();

            List<int> IdPedidosFind = new List<int>();

            #region FILTRAR NOSSO NUMERO

            if (!string.IsNullOrEmpty(NossoNumero))
            {
                foreach (var Pedido in resultado)
                {
                    if (Pedido.TipoPedido == OrigemPedido.Empresa)
                    {
                        PedidoEmpresa PedidoCORP = (PedidoEmpresa)Pedido;
                        if (PedidoCORP.Faturamento.FormaPagamento == Domain.Entities.Pedido.FormasPagamento.Boleto)
                        {
                            foreach (var Boleto in PedidoCORP.Faturamento.Boletoes.Where(r => r.NossoNumero == NossoNumero).ToList())
                            {
                                foreach (var PedidoR in Boleto.Faturamento.PedidoEmpresas)
                                {
                                    IdPedidosFind.Add(PedidoR.ID);
                                }
                            }
                        }
                    }
                    else
                    {
                        Pedido PedidoCPV = (Pedido)Pedido;
                        if(PedidoCPV.FormaPagamento == Domain.Entities.Pedido.FormasPagamento.Boleto)
                        {
                            if (PedidoCPV.Boleto.NossoNumero == NossoNumero)
                                IdPedidosFind.Add(PedidoCPV.ID);
                        }
                        
                    }
                }

                
            }

            #endregion

            #region FILTRO TRANSAÇÃO ID

            if (!string.IsNullOrEmpty(TransID))
            {
                foreach (var Pedido in resultado)
                {
                    if (Pedido.TipoPedido == OrigemPedido.Cliente)
                    {
                        Pedido PedidoCPV = (Pedido)Pedido;

                        if (PedidoCPV.FormaPagamento == Domain.Entities.Pedido.FormasPagamento.BoletoPagarMe)
                        {
                            if (PedidoCPV.BoletoPagarMeReturn.TransactionID == TransID)
                                IdPedidosFind.Add(PedidoCPV.ID);
                        }
                    }
                }
            }

            #endregion

            if (IdPedidosFind.Count > 0)
                resultado = resultado.Where(r => IdPedidosFind.Contains(r.ID)).ToList();

            if ((!string.IsNullOrEmpty(NossoNumero) || !string.IsNullOrEmpty(TransID)) && IdPedidosFind.Count == 0)
            {
                resultado.Clear();
            }

            #endregion

            #region FILL PROP

            decimal ValorTotal = 0;

            foreach (var Iten in resultado)
            {

                if (Iten.TipoPedido == OrigemPedido.Cliente)
                {
                    var PedidoCPV = (Pedido)Iten;

                    ValorTotal += PedidoCPV.ValorTotal;

                    if (PedidoCPV.LocalID != null)
                    {
                        var LocalID = (int)PedidoCPV.LocalID;
                        var Local = localRepository.Get(LocalID);

                        Iten.LocalDeEntregaDetalhado += Local.Titulo + " - " + Local.Cidade.Nome + " / " + Local.Estado.Sigla + " ";                        
                    }
                    else
                    {
                        Iten.LocalDeEntregaDetalhado += PedidoCPV.Cidade.NomeCompleto;

                        if (!string.IsNullOrEmpty(PedidoCPV.LocalEntrega))
                        {
                            Iten.LocalDeEntregaDetalhado += " - " + PedidoCPV.LocalEntrega + " ";
                        }
                    }

                    #region PREENCHE QTD BOLETOS VENCIDOS AGUARDANDO PAGAMENTO

                    var Boletos = boletoPagarMeRepository.GetByExpression(b => b.Pedido.Cliente.ID == PedidoCPV.ClienteID && b.DataVencimento <= datafinal && b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento).ToList();

                    Iten.BoletosVencidos = Boletos.Count;

                    #endregion

                    #region LISTA LOGS MANDRILL

                    var Emails = logMandrillRepository.GetByExpression(r => r.PedidoID == PedidoCPV.ID).ToList();

                    if (Emails.Count > 0)
                    {
                        Iten.LstLogsMandrill = Emails;
                    }

                        #endregion
                }
                else
                {
                    var PedidoCORP = (PedidoEmpresa)Iten;

                    ValorTotal += PedidoCORP.Faturamento.ValorTotal;

                    if (PedidoCORP.LocalID != null)
                    {
                        var LocalID = (int)PedidoCORP.LocalID;
                        var Local = localRepository.Get(LocalID);

                        Iten.LocalDeEntregaDetalhado += Local.Titulo + " - " + Local.Cidade.Nome + " / " + Local.Estado.Sigla + " ";
                    }
                    else
                    {
                        Iten.LocalDeEntregaDetalhado += PedidoCORP.Cidade.NomeCompleto;

                        if (!string.IsNullOrEmpty(PedidoCORP.LocalEntrega))
                        {
                            Iten.LocalDeEntregaDetalhado += " - " + PedidoCORP.LocalEntrega + " ";
                        }
                    }

                    #region PREENCHE QTD BOLETOS VENCIDOS AGUARDANDO PAGAMENTO

                    //var Boletos = faturamentoRepository.GetByExpression(r => r.Empresa.ID == PedidoCORP.Empresa.ID && r.Boletoes.Where(b => b.DataVencimento <= datafinal && b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento).Any()).ToList();
                    var FaturasAberta = faturamentoRepository.GetByExpression(r => r.Empresa.ID == PedidoCORP.Empresa.ID && r.DataVencimento <= datafinal && r.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.AguardandoPagamento).ToList();

                    Iten.FaturasVencidas = FaturasAberta.Count;

                    #endregion

                    #region LISTA LOGS MANDRILL

                    var Emails = logMandrillRepository.GetByExpression(r => r.PedidoEmpresaID == PedidoCORP.ID && (r.TipoEmailStr == "PrimeiroBoleto" || r.TipoEmailStr == "NFE")).ToList();

                    if(Emails.Count >0)
                        Iten.LstLogsMandrill.AddRange(Emails);

                    #endregion

                    #region PREENCHE PEDIDOS QUE COMPOE A FATURA

                    foreach (var PedidoCORPr in resultado.Where(r => r.TipoPedido == OrigemPedido.Empresa).ToList())
                    {
                        PedidoCORPr.LstPedidosDaFatura = new List<PedidoEmpresa>();
                        List<PedidoEmpresa> LstAddReturn = new List<PedidoEmpresa>();

                        PedidoEmpresa PedidoREturn = (PedidoEmpresa)PedidoCORPr;

                        foreach (var item in PedidoREturn.Faturamento.PedidoEmpresas)
                        {
                            LstAddReturn.Add(item);
                        }

                        PedidoCORPr.LstPedidosDaFatura.AddRange(LstAddReturn);
                    }

                        #endregion

                    #region LISTA LOGS MANDRILL

                    var EmailsCorp = logMandrillRepository.GetByExpression(r => r.PedidoEmpresaID == PedidoCORP.ID).ToList();

                    if (EmailsCorp.Count > 0)
                    {
                        Iten.LstLogsMandrill = EmailsCorp;
                    }

                    #endregion

                }

            }

            #endregion

            ViewBag.datafinal = datafinal;

            ViewBag.MensagemFooter = resultado.Count + " registros. Valor Total de: " + ValorTotal.ToString("C2");

            return resultado.OrderBy(r => r.DataCobranca == null).ThenBy(r => r.DataCobranca).ToList();
        }
        //----------------------------------------------------------------------
        public void ExportExcel(List<BoletoPagarMe> LstObjects)
        {
            string Csv = string.Empty;

            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms, Encoding.UTF8);

            string TBL = string.Empty;

            TBL += "<table>";
            TBL += "<tr>";
            TBL += "<td>TRANSACAO</td>";
            TBL += "<td>ID</td>";
            TBL += "<td>PEDIDO</td>";
            TBL += "<td>SACADO</td>";
            TBL += "<td>DOCUMENTO</td>";
            TBL += "<td>VALOR</td>";
            TBL += "<td>TELEFONE</td>";
            TBL += "<td>CELULAR</td>";
            TBL += "<td>OBS</td>";
            TBL += "<td>DATA DE VENC</td>";
            TBL += "<td>DATA DE CRIACAO</td>";
            TBL += "<td>STATUS</td>";
            TBL += "</tr>";

            foreach (var iten in LstObjects)
            {
                TBL += "<tr>";
                TBL += "<td>" + iten.TransactionID + "</td>";
                TBL += "<td>" + iten.ID + "</td>";
                TBL += "<td>" + iten.Pedido.Numero + "</td>";
                TBL += "<td>" + iten.SacadoNome + "</td>";
                TBL += "<td>" + iten.SacadoDocumento + "</td>";
                TBL += "<td>" + iten.Valor + "</td>";
                TBL += "<td>" + iten.Pedido.Cliente.TelefoneContato + "</td>";
                TBL += "<td>" + iten.Pedido.Cliente.CelularContato + "</td>";
                TBL += "<td>" + iten.Observacao + "</td>";
                TBL += "<td>" + iten.DataVencimento + "</td>";
                TBL += "<td>" + iten.DataCriacao + "</td>";
                TBL += "<td>" + Domain.Helpers.EnumHelper.GetDescription(iten.StatusPagamento) + "</td>";
                TBL += "</tr>";
            }

            TBL += "</table>";
            tw.Write(TBL);

            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();


            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", "attachment; filename=Export.xls");
            Response.BinaryWrite(bytes);
            Response.End();

        }

        public override ActionResult List(params object[] args)
        {
            var resultado = Listar();

            ////PAGINAÇÃO
            //var paginaAtual = 0;
            //Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            //if (paginaAtual == 0)
            //    paginaAtual = 1;

            //var totalItens = resultado.Count;
            //var totalPaginas = Math.Ceiling((decimal)totalItens / 40);
            //if (totalPaginas > 1)
            //{
            //    resultado = resultado.Skip((paginaAtual - 1) * 40).Take(40).ToList();
            //}
            //ViewBag.PaginaAtual = paginaAtual;
            //ViewBag.TotalItens = totalItens;
            //ViewBag.TotalPaginas = totalPaginas;
            //ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
             
            return View(resultado);
        }

        [HttpPost]
        public ActionResult Serasa(int id, string Tipo)
        {
            if (Tipo == "Empresa")
            {
               var Pedido = pedidoEmpresaRepository.Get(id);

                #region ALTERA STATUS FATURA

                Pedido.Faturamento.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Serasa;
                pedidoEmpresaRepository.Save(Pedido);

                #endregion

                #region INCLUI BLACKLIST

                // VERIFICA CNPJ EMPRESA
                if (blackListRepository.GetByExpression(r => r.Documento.Replace("-", "").Replace(".", "").Replace("/", "") == Pedido.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "")).Any() == false)
                {
                    blackListRepository.Save(new ListaNegra
                    {
                        DataCadastro = DateTime.Now,
                        Documento = Pedido.Empresa.CNPJ,
                        Observacao = "SERASA"
                    });
                }

                #endregion

                if (Pedido.Faturamento.MeioPagamento == Domain.Entities.Pedido.MeiosPagamento.Boleto)
                {
                    var boleto = Pedido.Faturamento.Boleto;

                    #region CANCELA O BOLETO

                    if (boleto != null)
                    {
                        if (boleto.RemessaBoletoes.Count > 0)
                        {
                            boleto.Comando = Boleto.Comandos.Cancelar;
                            boleto.Processado = false;
                            boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Processando;
                            boletoRepository.Save(boleto);
                        }
                        else
                        {
                            return Json(new { sucesso = false, mensagem = "Você só pode protestar um boleto que já foi enviado em alguma remessa. O cliente foi adicionado a Blacklist e a fatura marca como 'Protestado'. " });
                        }
                    }

                    #endregion
                }
            }
            else
            {
                var Pedido = pedidoRepository.Get(id);

                #region ALTERA STATUS FATURA

                Pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Serasa;
                pedidoRepository.Save(Pedido);

                #endregion

                #region INCLUI BLACKLIST

                // VERIFICA CNPJ EMPRESA
                if (blackListRepository.GetByExpression(r => r.Documento.Replace("-", "").Replace(".", "").Replace("/", "") == Pedido.Cliente.Documento.Replace("-", "").Replace(".", "").Replace("/", "")).Any() == false)
                {
                    blackListRepository.Save(new ListaNegra
                    {
                        DataCadastro = DateTime.Now,
                        Documento = Pedido.Cliente.Documento,
                        Observacao = "SERASA"
                    });
                }

                #endregion

                if (Pedido.MeioPagamento == Domain.Entities.Pedido.MeiosPagamento.BoletoPagarMe)
                {
                    #region CANCELA O BOLETO

                    var boleto = Pedido.BoletoPagarMeReturn;

                    if (boleto != null)
                    {
                        boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;
                        boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Cancelado;                        
                        boletoPagarMeRepository.Save(boleto);
                    }

                    #endregion
                }

            }

            return Json(new { sucesso = true });
        }

        [HttpPost]
        public ActionResult Prejuizo(int id, string Tipo)
        {
            if(Tipo == "Empresa")
            {
                #region ALTERAR FATURA

                var Fatura = pedidoEmpresaRepository.Get(id).Faturamento;
                Fatura.StatusPagamento = Pedido.TodosStatusPagamento.Prejuizo;
                Fatura.Status = Faturamento.TodosStatus.Prejuizo;

                faturamentoRepository.Save(Fatura);

                #endregion

                #region ALTERAR BOLETOS

                var boletos = boletoRepository.GetByExpression(r => r.FaturamentoID == Fatura.ID).ToList();

                foreach (var boleto in boletos)
                {
                    boleto.StatusPagamento = Boleto.TodosStatusPagamento.Prejuizo;
                    if (boleto.FaturamentoID.HasValue)
                    {
                        boletoRepository.Save(boleto);
                    }
                }

                #endregion
            }
            else
            {

                #region ALTERAR PEDIDO

                var Pedido = pedidoRepository.Get(id);
                Pedido.StatusPagamento = Pedido.TodosStatusPagamento.Prejuizo;

                pedidoRepository.Save(Pedido);

                #endregion

                #region ALTERAR BOLETOS ITAU

                foreach (var boleto in Pedido.Boletoes)
                {
                    boleto.StatusPagamento = Boleto.TodosStatusPagamento.Prejuizo;
                    if (boleto.FaturamentoID.HasValue)
                    {
                        boletoRepository.Save(boleto);
                    }
                }

                #endregion

                #region ALTERAR BOLETOS PAGARME

                foreach (var boleto in Pedido.BoletoPagarMes)
                {
                    boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Prejuizo;
                    boletoPagarMeRepository.Save(boleto);
                }

                #endregion

            }

            return Json(new { sucesso = true });
        }
        [HttpPost]
        public ActionResult EnviaCobranca(int id, string Tipo)
        {
            if (Tipo == "Empresa")
            {
                var PedidoEmpresa = pedidoEmpresaRepository.Get(id);
                //PedidoEmpresa.EnviarEmailCobranca();
            }
            else
            {
                var Pedido = pedidoRepository.Get(id);
                //Pedido.EnviarEmailCobranca();
            }

            return Json(new { sucesso = true });
        }
        [HttpPost]
        public ActionResult Salva(int id, string Tipo, string texto)
        {
            try
            {
                if (Tipo == "Empresa")
                {
                    var Pedido = pedidoEmpresaRepository.Get(id);
                    Pedido.ObservacaoFinanceira = texto;

                    pedidoEmpresaRepository.Save(Pedido);
                }
                else
                {
                    var Pedido = pedidoRepository.Get(id);
                    Pedido.ObservacaoFinanceira = texto;

                    pedidoRepository.Save(Pedido);
                }

                return Json(new { sucesso = true });
            }
            catch
            {
                return Json(new { sucesso = false, mensagem = "Erro de processamento" });
            }
        }
        [HttpPost]
        public ActionResult AlterarVecimento(int id, string Tipo, string data)
        {
            if (Tipo == "Empresa")
            {
                var PedidoCORP = pedidoEmpresaRepository.Get(id);
                var fatura = faturamentoRepository.Get(PedidoCORP.Faturamento.ID);

                if (fatura != null)
                {
                    var datavencimento = new DateTime();
                    DateTime.TryParse(data, out datavencimento);

                    if (datavencimento != new DateTime())
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.AlterouDataVencimento);

                        var boleto = fatura.Boleto;
                        boleto.Processado = false;

                        if (boleto.RemessaBoletoes.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;

                        boleto.DataVencimento = datavencimento;
                        boleto.DataVencimentoOriginal = datavencimento;

                        boleto.PrimeiroRenegociado = false;
                        boleto.SegundoRenegociado = false;

                        boleto.DataEnvioLembreteVencimento = null;
                        boletoRepository.Save(boleto);

                        fatura.DataVencimento = datavencimento;
                        fatura.DataVencimentoOriginal = datavencimento;

                        fatura.PrimeiroRenegociado = false;
                        fatura.SegundoRenegociado = false;

                        faturamentoRepository.Save(fatura);

                        return Json(new { retorno = "OK", id = boleto.ID, data = data, LinkBoleto = Domain.Core.Configuracoes.DOMINIO + "/boleto/empresa/" + Domain.Core.Criptografia.Encrypt(fatura.ID.ToString()), Tipo = "Empresa" });
                    }
                    else
                        return Json(new { retorno = "ERRO", mensagem = "Data inválida" });

                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "O pedido não pode ser alterado. Tente novamente mais tarde" });
            }
            else
            {
                var Pedido = pedidoRepository.Get(id);

                if(Pedido.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe)
                {
                    var datavencimento = new DateTime();
                    DateTime.TryParse(data, out datavencimento);

                    if (datavencimento != new DateTime())
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, Pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouDataVencimento);

                        var boleto = Pedido.BoletoPagarMeReturn;
                        boleto.DataVencimento = datavencimento;

                        #region ALTERAR BOLETO PAGAR ME

                        BoletoPagarMeFactory.Alterar(boleto, Remessa.Empresa.CPV);

                        #endregion

                        return Json(new { retorno = "OK", id = boleto.ID, data = data, Tipo = "CPV", PedidoID = boleto.PedidoID, LinkBoleto = "http://admin.coroasparavelorio.com.br/PedidosSite/Boleto?id=" + boleto.PedidoID });
                    }
                    else
                        return Json(new { retorno = "ERRO", mensagem = "Data inválida" });
                }
                else
                {
                    return Json(new { retorno = "ERRO", mensagem = "O pedido não pode ser alterado. Tente novamente mais tarde" });
                }
            }
        }
    }
}
