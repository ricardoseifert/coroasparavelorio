﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using System.Text.RegularExpressions;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|FloriculturaMisto", "/Home/AccessDenied")]
    public class QualidadeController : CrudController<Pedido>
    {
        private PedidoRepository pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Reclamacao> reclamacaoRepository;
        private AdminModel adminModel;

        public QualidadeController(ObjectContext context)
            : base(context)
        {
            pedidoRepository = new PedidoRepository(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            reclamacaoRepository = new PersistentRepository<Reclamacao>(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            adminModel = new AdminModel(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PedidosSite()
        {
            var resultado = pedidoRepository.GetAll().OrderByDescending(p => p.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var cupomID = 0;
            Int32.TryParse(Request.QueryString["cupomID"], out cupomID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statusentrega = 0;
            Int32.TryParse(Request.QueryString["statusentrega"], out statusentrega);

            var statuscontatonao = 10;
            Int32.TryParse(Request.QueryString["statuscontatonao"], out statuscontatonao);

            var statuscontato = 10;
            Int32.TryParse(Request.QueryString["statuscontato"], out statuscontato);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataCriacao <= datafinal).ToList();
            }

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Cliente.Nome.ToLower().Contains(nome.ToLower()) || c.Cliente.RazaoSocial.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Cliente.Email.Contains(email)).ToList();

            if (!String.IsNullOrEmpty(numero))
                resultado = resultado.Where(c => c.Numero.Contains(numero)).ToList();

            if (clienteID > 0)
                resultado = resultado.Where(c => c.ClienteID == clienteID).ToList();

            if (cupomID > 0)
                resultado = resultado.Where(c => c.CupomID == cupomID).ToList();

            if (status > 0)
                resultado = resultado.Where(c => c.StatusProcessamentoID == status).ToList();
            else
                resultado = resultado.Where(c => c.StatusProcessamentoID != (int)Domain.Entities.Pedido.TodosStatusProcessamento.Cancelado && c.StatusProcessamentoID != (int)Domain.Entities.Pedido.TodosStatusProcessamento.Removido).ToList();

            if (statuspagamento > 0)
                resultado = resultado.Where(c => c.StatusPagamentoID == statuspagamento).ToList();

            if (statusentrega > 0)
                resultado = resultado.Where(c => c.StatusEntregaID == statusentrega).ToList();

            if (statuscontatonao > 0)
                resultado = resultado.Where(c => c.QualidadeStatusContatoNaoID == statuscontatonao).ToList();

            if (statuscontato > 0)
                resultado = resultado.Where(c => c.QualidadeStatusContatoID == statuscontato).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            return View(resultado);
        }

        public ActionResult PedidosEmpresa()
        {
            var resultado = pedidoEmpresaRepository.GetAll().OrderByDescending(p => p.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var nome = Request.QueryString["nome"]; 
            var email = Request.QueryString["email"];

            var numero = 0;
            Int32.TryParse(Request.QueryString["numero"], out numero);

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var cupomID = 0;
            Int32.TryParse(Request.QueryString["cupomID"], out cupomID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);
             
            var statusentrega = 0;
            Int32.TryParse(Request.QueryString["statusentrega"], out statusentrega);

            var statuscontatonao = 10;
            Int32.TryParse(Request.QueryString["statuscontatonao"], out statuscontatonao);

            var statuscontato = 10;
            Int32.TryParse(Request.QueryString["statuscontato"], out statuscontato);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataCriacao <= datafinal).ToList();
            }

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Empresa.RazaoSocial.ToLower().Contains(nome.ToLower()) || c.Empresa.NomeFantasia.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Empresa.EmailContato.Contains(email)).ToList();

            if (numero > 0)
                resultado = resultado.Where(c => c.ID == numero).ToList();

            if (clienteID > 0)
                resultado = resultado.Where(c => c.EmpresaID == clienteID).ToList();

            if (cupomID > 0)
                resultado = resultado.Where(c => c.CupomID == cupomID).ToList();

            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList();
            else
                resultado = resultado.Where(c => c.StatusID != (int)Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado && c.StatusID != (int)Domain.Entities.Pedido.TodosStatusProcessamento.Removido).ToList();
             
            if (statusentrega > 0)
                resultado = resultado.Where(c => c.StatusEntregaID == statusentrega).ToList();

            if (statuscontatonao > 0)
                resultado = resultado.Where(c => c.QualidadeStatusContatoNaoID == statuscontatonao).ToList();

            if (statuscontato > 0)
                resultado = resultado.Where(c => c.QualidadeStatusContatoID == statuscontato).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            return View(resultado);
        }

        [HttpPost]
        public ActionResult ModalPesquisa(int? pedidoID, int? pedidoEmpresaID)
        {
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            if (pedidoID.HasValue)
            {
                var pedido = pedidoRepository.Get(pedidoID.Value);
                if (pedido != null)
                {
                    ViewBag.Pedido = pedido;
                }
            }
            if (pedidoEmpresaID.HasValue)
            {
                var pedido = pedidoEmpresaRepository.Get(pedidoEmpresaID.Value);
                if (pedido != null)
                {
                    ViewBag.PedidoEmpresa = pedido;
                }
            }
            return View();
        }

        [HttpPost]
        public JsonResult AtualizarPesquisa(int id, int statuscontato, int? statuscontatonao, string visualizado, int errofaixa, int facilvisualizacao, int satisfeito, int expectativa, string observacao, string porque, int ligacao1, string horario1, int atendente1, int ligacao2, string horario2, int atendente2, int ligacao3, string horario3, int atendente3)
        {            
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.QualidadeStatusContatoID = statuscontato;
                if (statuscontatonao.HasValue && statuscontato == 2)
                    pedido.QualidadeStatusContatoNaoID = statuscontatonao.Value;
                else
                    pedido.QualidadeStatusContatoNaoID = (int)Domain.Entities.Pedido.TodosStatusContatoNao.NI;
                if (visualizado == "1")
                    pedido.QualidadeProdutoVisualizado = true;
                else if (visualizado == "0")
                    pedido.QualidadeProdutoVisualizado = false;
                else
                    pedido.QualidadeProdutoVisualizado = null;
                pedido.QualidadeErroFaixa = errofaixa > 0;
                pedido.QualidadeFacilVisualizacao = facilvisualizacao > 0;
                pedido.QualidadeSatisfeito = satisfeito > 0;
                pedido.QualidadeExpectativaAtingida = expectativa > 0;
                pedido.QualidadeObservacoes = observacao;
                pedido.QualidadePorqueEscolheu = porque;

                pedido.Qualidade1Ligacao = ligacao1 > 0;
                pedido.Qualidade1LigacaoHorario = horario1;
                if (pedido.Qualidade1Ligacao)
                    pedido.Qualidade1LigacaoAtendenteID = atendente1;
                pedido.Qualidade2Ligacao = ligacao2 > 0;
                pedido.Qualidade2LigacaoHorario = horario2;
                if (pedido.Qualidade2Ligacao)
                    pedido.Qualidade2LigacaoAtendenteID = atendente2;
                pedido.Qualidade3Ligacao = ligacao3 > 0;
                pedido.Qualidade3LigacaoHorario = horario3;
                if (pedido.Qualidade3Ligacao)
                    pedido.Qualidade3LigacaoAtendenteID = atendente3;

                pedido.QualidadeDataCriacao = DateTime.Now;
                pedido.QualidadeAdministradorID = adminModel.ID;
                pedidoRepository.Save(pedido);
                return Json("OK");
            }
            else
                return Json("Não foi possível gravar a pesquisa, tente novamente.");
        }

        [HttpPost]
        public JsonResult AtualizarPesquisaEmpresa(int id, int statuscontato, int? statuscontatonao, string visualizado, int errofaixa, int facilvisualizacao, int satisfeito, int expectativa, string observacao, string porque, int ligacao1, string horario1, int atendente1, int ligacao2, string horario2, int atendente2, int ligacao3, string horario3, int atendente3)
        {
            var pedido = pedidoEmpresaRepository.Get(id);
            if (pedido != null)
            {
                pedido.QualidadeStatusContatoID = statuscontato;
                if (statuscontatonao.HasValue && statuscontato == 2)
                    pedido.QualidadeStatusContatoNaoID = statuscontatonao.Value;
                else
                    pedido.QualidadeStatusContatoNaoID = (int)Domain.Entities.Pedido.TodosStatusContatoNao.NI;
                if (visualizado == "1")
                    pedido.QualidadeProdutoVisualizado = true;
                else if (visualizado == "0")
                    pedido.QualidadeProdutoVisualizado = false;
                else
                    pedido.QualidadeProdutoVisualizado = null;
                pedido.QualidadeErroFaixa = errofaixa > 0;
                pedido.QualidadeFacilVisualizacao = facilvisualizacao > 0;
                pedido.QualidadeSatisfeito = satisfeito > 0;
                pedido.QualidadeExpectativaAtingida = expectativa > 0;
                pedido.QualidadeObservacoes = observacao;
                pedido.QualidadePorqueEscolheu = porque;

                pedido.Qualidade1Ligacao = ligacao1 > 0;
                pedido.Qualidade1LigacaoHorario = horario1;
                if (pedido.Qualidade1Ligacao)
                    pedido.Qualidade1LigacaoAtendenteID = atendente1;
                pedido.Qualidade2Ligacao = ligacao2 > 0;
                pedido.Qualidade2LigacaoHorario = horario2;
                if (pedido.Qualidade2Ligacao)
                    pedido.Qualidade2LigacaoAtendenteID = atendente2;
                pedido.Qualidade3Ligacao = ligacao3 > 0;
                pedido.Qualidade3LigacaoHorario = horario3;
                if (pedido.Qualidade3Ligacao)
                    pedido.Qualidade3LigacaoAtendenteID = atendente3;

                pedido.QualidadeDataCriacao = DateTime.Now;
                pedido.QualidadeAdministradorID = adminModel.ID;
                pedidoEmpresaRepository.Save(pedido);
                return Json("OK");
            }
            else
                return Json("Não foi possível gravar a pesquisa, tente novamente.");
        }
        
        [HttpPost]
        public JsonResult EnviarNF(int id)
        {            
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.NFeData.HasValue && !String.IsNullOrEmpty(pedido.NFeLink) && pedido.NFeNumero.HasValue && pedido.NFeValor > 0)
                {
                    pedido.EnviarNFe();
                    return Json("NF enviada com sucesso!");
                }
                else
                {
                    return Json("Para enviar o e-mail é necessário preencher todos os dados da NF.");
                }
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        [HttpPost]
        public JsonResult EnviarConfirmacao(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusEntrega == TodosStatusEntrega.Entregue && pedido.DataEntrega.HasValue)
                {
                    pedido.EnviarEmailEntrega();
                    return Json("Confirmação de entrega enviada com sucesso!");
                }
                else
                {
                    return Json("Para enviar o e-mail é que o status de entrega do pedido esteja como 'Entregue'.");
                }
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }
        
        [HttpPost]
        public JsonResult EnviarConfirmacaoEmpresa(int id)
        {
            var pedido = pedidoEmpresaRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusEntrega == TodosStatusEntrega.Entregue && pedido.DataEntrega.HasValue)
                {
                    pedido.EnviarEmailEntrega();
                    return Json("Confirmação de entrega enviada com sucesso!");
                }
                else
                {
                    return Json("Para enviar o e-mail é que o status de entrega do pedido esteja como 'Entregue'.");
                }
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        [HttpPost]
        public JsonResult EnviarElogioCPV(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
                if (!String.IsNullOrEmpty(configuracao.CupomElogio))
                {
                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/cupom2.htm"));
                    var remetente = "Coroas para Velório"; 
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOME]", pedido.Cliente.Nome.ToUpper());  
                    html = html.Replace("[CUPOM]", configuracao.CupomElogio);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, remetente, pedido.Cliente.Email, pedido.Cliente.Nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, remetente, Domain.Core.Configuracoes.EMAIL_COPIA, "[" + remetente.ToUpper() + "] - Seu cupom de desconto!", html, true);
                    return Json("Cupom de elogio enviado com sucesso!");
                }else
                    return Json("Cadastre o código do cupom de desconto (elogio) em configurações gerais.");
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        [HttpPost]
        public JsonResult EnviarElogioLacos(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
                if (!String.IsNullOrEmpty(configuracao.CupomElogio))
                {
                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/lacosflores/cupom2.htm"));
                    var remetente = "Laços Flores"; 
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOME]", pedido.Cliente.Nome.ToUpper());
                    html = html.Replace("[CUPOM]", configuracao.CupomElogio);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, remetente, pedido.Cliente.Email, pedido.Cliente.Nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, remetente, Domain.Core.Configuracoes.EMAIL_COPIA, "[" + remetente.ToUpper() + "] - Seu cupom de desconto!", html, true);
                    return Json("Cupom de elogio enviado com sucesso!");
                }
                else
                    return Json("Cadastre o código do cupom de desconto (elogio) em configurações gerais.");
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        [HttpPost]
        public JsonResult EnviarReclamacao(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
                if (!String.IsNullOrEmpty(configuracao.CupomReclamacao))
                {
                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/cupom2.htm"));
                    var remetente = "Coroas para Velório";
                    if (pedido.OrigemSite.ToUpper() == "LAÇOS FLORES")
                    {
                        html = System.IO.File.ReadAllText(Server.MapPath("/content/html/lacosflores/cupom2.htm"));
                        remetente = "Laços Flores";
                    }

                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOME]", pedido.Cliente.Nome.ToUpper());
                    html = html.Replace("[CUPOM]", configuracao.CupomReclamacao);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, remetente, pedido.Cliente.Email, pedido.Cliente.Nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, remetente, Domain.Core.Configuracoes.EMAIL_COPIA, "[" + remetente.ToUpper() + "] - Seu cupom de desconto!", html, true);
                    return Json("Cupom de reclamação enviado com sucesso!");
                }
                else
                    return Json("Cadastre o código do cupom de desconto (reclamação) em configurações gerais.");
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        [HttpPost]
        public JsonResult EnviarBoleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.EnviarBoleto();
                return Json("Boleto enviado com sucesso!");
            }
            else
                return Json("Não foi possível enviar o e-mail, tente novamente.");
        }

        public ActionResult Reclamacao(){
            var resultado = reclamacaoRepository.GetAll().OrderByDescending(p => p.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var numeroCorp = Request.QueryString["numeroCorp"];
            var email = Request.QueryString["email"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);
             
            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);
             
            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataCriacao <= datafinal).ToList();
            }

            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Pedido.Cliente.Nome.ToLower().Contains(nome.ToLower()) || c.Pedido.Cliente.RazaoSocial.ToLower().Contains(nome.ToLower())).ToList();

            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Pedido.Cliente.Email.Contains(email)).ToList();

            if (!String.IsNullOrEmpty(numero))
                resultado = resultado.Where(c => c.Pedido.Numero.Contains(numero)).ToList();

            if (!String.IsNullOrEmpty(numeroCorp))
                resultado = resultado.Where(c => c.PedidoEmpresaID.ToString() == numeroCorp).ToList();

            if (clienteID > 0)
                resultado = resultado.Where(c => c.Pedido.ClienteID == clienteID).ToList(); 

            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList(); 

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public ActionResult EditReclamacao(int? id)
        {
            Reclamacao reclamacao = new Reclamacao();
            if (id.HasValue) {
                reclamacao = reclamacaoRepository.Get(id.Value);
                if (!reclamacao.PedidoID.HasValue){
                    reclamacao.PedidoID = reclamacao.PedidoEmpresaID;
                }
            }
                
            return View(reclamacao);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditReclamacao(Reclamacao reclamacao, FormCollection tipo){
            //ObjectContext context = ((ObjectContext) reclamacaoRepository);
            if (reclamacao.ID == 0){
                reclamacao.DataCriacao = DateTime.Now;
            }
            if (int.Parse(tipo["SelectorPedido"]) == 1){
                reclamacao.PedidoID = reclamacao.PedidoID.Value;
            }
            else if (int.Parse(tipo["SelectorPedido"]) == 2){
                reclamacao.PedidoEmpresaID = reclamacao.PedidoID.Value;
                reclamacao.PedidoID = null;
            }
            else{
                if (reclamacao.PedidoEmpresaID.HasValue){
                    reclamacao.PedidoID = null;
                }
            }

            reclamacao.DataAlteracao = DateTime.Now;
            //nao encontrei o problema, tem que apontar pra producao e ver o que ele manda salvar
            reclamacaoRepository.Save(reclamacao);
             
            return RedirectToAction("Reclamacao", new { cod = "SaveSucess" });
        }

        public JsonResult GetPedido(string numero)
        {
            Domain.Entities.Pedido PedidoSite = null;
            Domain.Entities.PedidoEmpresa PedidoEmpresa = null;
            Domain.Entities.AdministradorPedido AdminPedido = new AdministradorPedido();

            // verifica se pedido corporativo
            if (Regex.Replace(numero, "[^a-zA-Z]+", "").Length == 0)
            {
                int p = int.Parse(numero.Trim());
                PedidoEmpresa = pedidoEmpresaRepository.GetByExpression(c => c.ID == p).FirstOrDefault();
                if (PedidoEmpresa != null) 
                {
                    AdminPedido = PedidoEmpresa.AdministradorPedidoes.FirstOrDefault(c => c.AcaoID == (int)AdministradorPedido.Acao.ProcessouPedido);
                }
                
            }
            else{
                PedidoSite = pedidoRepository.GetByExpression(c => c.Numero.Trim() == numero.Trim()).FirstOrDefault();
                if(PedidoSite != null)
                {
                    AdminPedido = PedidoSite.AdministradorPedidoes.FirstOrDefault(c => c.AcaoID == (int)AdministradorPedido.Acao.ProcessouPedido);
                }
                
            }


            if (PedidoSite != null)
            {
                return Json(new { retorno = "OK", pedidoID = PedidoSite.ID, datapedido = PedidoSite.DataCriacao.ToString("dd/MM/yyyy HH:mm"), floricultura = PedidoSite.FornecedorID.HasValue ? PedidoSite.Fornecedor.Nome : "", funcionario = AdminPedido != null ? AdminPedido.Administrador.Nome : "", SelectorPedido = "1" });
            }

            if (PedidoEmpresa != null)
            {
                return Json(new { retorno = "OK", pedidoID = PedidoEmpresa.ID, datapedido = PedidoEmpresa.DataCriacao.ToString("dd/MM/yyyy HH:mm"), floricultura = PedidoEmpresa.FornecedorID.HasValue ? PedidoEmpresa.Fornecedor.Nome : "", funcionario = AdminPedido != null ? AdminPedido.Administrador.Nome : "", SelectorPedido = "2" });
            }


            return Json(new { retorno = "ERRO", mensagem = "Pedido inexistente" });
                
        }
    }
}
