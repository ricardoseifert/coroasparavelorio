﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using MvcExtensions.Controllers;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Comunicacao", "/Home/AccessDenied")]
    public class BannerController : CrudController<Banner>
    {
        private IPersistentRepository<Banner> bannerRepository;

        public BannerController(ObjectContext context) : base(context)
        {
            bannerRepository = new PersistentRepository<Banner>(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = bannerRepository.GetAll().OrderBy(p => p.Ordem).ToList();
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpPost]
        public override ActionResult Edit(Banner banner)
        {
            ModelState.Remove("Arquivo");

            if (banner.ID == 0)
                banner.Arquivo = "";

            bannerRepository.Save(banner);

            var foto = Request.Files["File"];
            if (foto.ContentLength > 0)
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/banners/" + banner.ID + "/"));
                if (!dir.Exists)
                {
                    dir.Create();
                }

                //try
                //{

                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                //FOTO
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/banners/" + banner.ID + "/" + nomefoto), new ImageResizer.ResizeSettings(
                                        "width=958&height=140&crop=auto;format=jpg;mode=crop;scale=both"));

                i.CreateParentDirectory = true;
                i.Build();

                banner.Arquivo = nomefoto;
                bannerRepository.Save(banner);

                //}
                //catch { }
            }

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

    }
}
