﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using System.Data.SqlTypes;
using Domain.Factories;
using System.Globalization;
using System.Net;

namespace Admin.Controllers
{
    //==========================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class PedidosSiteController : CrudController<Pedido>
    {

        #region Variaveis
        private IPersistentRepository<Cliente> clienteRepository;
        private PedidoRepository pedidoRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<BoletoPagarMe> boletoPagarMeRepository;
        private PedidoService pedidoService;
        private IPersistentRepository<Promocao> promocaoRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<PedidoNota> pedidoNotaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Local> localRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private FornecedorPushRepository fornecedorPushRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<NPSRating_NEW> npsNewREspository;
        private IPersistentRepository<FornecedorLocal> fornecedorlocalRepository;
        private IPersistentRepository<OrigemSite> origemSiteRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<StatusCancelamento> statusCancelamentoRepository;
        private IPersistentRepository<ListaNegra> blackListRepository;
        private IPersistentRepository<ClienteFloricultura> ClienteFloriculturaRepository;
        private IPersistentRepository<ProdutoFloricultura> produtoFloriculturaRepository;
        private IPersistentRepository<PedidoItemFloricultura> PedidoItemFloriculturaRepository;
        private IPersistentRepository<ProdutoTamanhoPrecoFloricultura> ProdutoTamanhoPrecoFloriculturaRepository;
        private IPersistentRepository<ListaNegra> listaNegraRepository;
        private IPersistentRepository<LeadScore> leadScoreRepository;
        private IPersistentRepository<Pedido_X_Ligacao> PedidoXligacaoRepository;
        private IPersistentRepository<Ligaco> ligacoesRepository;
        private IPersistentRepository<Linha> linhasRepository;
        private IPersistentRepository<Ramai> ramaisRepository;
        private AdminModel adminModel;
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public PedidosSiteController(ObjectContext context)
            : base(context)
        {
            administradorRepository = new PersistentRepository<Administrador>(context);
            promocaoRepository = new PersistentRepository<Promocao>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            pedidoRepository = new PedidoRepository(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            boletoPagarMeRepository = new PersistentRepository<BoletoPagarMe>(context);
            pedidoService = new PedidoService(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            pedidoNotaRepository = new PersistentRepository<PedidoNota>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            localRepository = new PersistentRepository<Local>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            fornecedorPushRepository = new FornecedorPushRepository(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            npsREspository = new PersistentRepository<NPSRating>(context);
            npsNewREspository = new PersistentRepository<NPSRating_NEW>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            origemSiteRepository = new PersistentRepository<OrigemSite>(context);
            fornecedorlocalRepository = new PersistentRepository<FornecedorLocal>(context);
            statusCancelamentoRepository = new PersistentRepository<StatusCancelamento>(context);
            blackListRepository = new PersistentRepository<ListaNegra>(context);
            ClienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
            produtoFloriculturaRepository = new PersistentRepository<ProdutoFloricultura>(context);
            PedidoItemFloriculturaRepository = new PersistentRepository<PedidoItemFloricultura>(context);
            ProdutoTamanhoPrecoFloriculturaRepository = new PersistentRepository<ProdutoTamanhoPrecoFloricultura>(context);
            listaNegraRepository = new PersistentRepository<ListaNegra>(context);
            leadScoreRepository = new PersistentRepository<LeadScore>(context);
            PedidoXligacaoRepository = new PersistentRepository<Pedido_X_Ligacao>(context);
            ligacoesRepository = new PersistentRepository<Ligaco>(context);
            linhasRepository = new PersistentRepository<Linha>(context);
            ramaisRepository = new PersistentRepository<Ramai>(context);
            adminModel = new AdminModel(context);

        }
        //----------------------------------------------------------------------    
        #endregion

        #region HTTPGET
        //----------------------------------------------------------------------
        public override ActionResult List(params object[] args)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["PedidoID"]))
            {
                var IdPedidoCriado = int.Parse(Request.QueryString["PedidoID"]);
                ViewBag.PedidoCriado = pedidoRepository.Get(IdPedidoCriado);
            }

            int[] origensPrincipais = new[] { 21, 3, 6, 10, 23, 27, 34, 24, 7, 14, 12, 25, 38 };
            List<OrigemSite> lstOrigens = new List<OrigemSite>();
            var origens = origemSiteRepository.GetAll().ToList();
            foreach (int idOrigem in origensPrincipais)
            {
                lstOrigens.Add(origens.FirstOrDefault(o => o.ID == idOrigem));
            }
            ViewBag.OrigemSite = new SelectList(lstOrigens, "Titulo", "Titulo");

            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.PromocaoSemana = promocao;

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<Pedido> lstPedidos = new List<Pedido>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            //var origem = 
            // var origemSite = 
            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var documento = Request.QueryString["documento"];

            if (!string.IsNullOrEmpty(documento))
                documento = documento.Replace(".", "").Replace("-", "").Replace("/", "");

            var PossuiFoto = Request.QueryString["PossuiFoto"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var AdministradorID = 0;
            Int32.TryParse(Request.QueryString["administradorid"], out AdministradorID);

            var cupomID = 0;
            Int32.TryParse(Request.QueryString["cupomID"], out cupomID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statusentrega = 0;
            Int32.TryParse(Request.QueryString["statusentrega"], out statusentrega);

            var meiopagamento = 0;
            Int32.TryParse(Request.QueryString["meiopagamento"], out meiopagamento);

            if (!String.IsNullOrEmpty(numero))
            {
                lstPedidos = pedidoRepository.GetByExpression(p => p.Numero.Contains(numero)).ToList();
            }
            else {

                lstPedidos = pedidoRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal).OrderByDescending(p => p.DataCriacao).ToList();

                if (!String.IsNullOrEmpty(nome))
                    lstPedidos = lstPedidos.Where(c => c.Cliente.Nome.ToLower().Contains(nome.ToLower()) || c.Cliente.RazaoSocial.ToLower().Contains(nome.ToLower())).ToList();

                if (!String.IsNullOrEmpty(email))
                    lstPedidos = lstPedidos.Where(c => c.Cliente.Email.Contains(email)).ToList();

                if (!String.IsNullOrEmpty(documento))
                    lstPedidos = lstPedidos.Where(c => c.Cliente.Documento.Contains(documento)).ToList();

                if (clienteID > 0)
                    lstPedidos = lstPedidos.Where(c => c.ClienteID == clienteID).ToList();

                if (cupomID > 0)
                    lstPedidos = lstPedidos.Where(c => c.CupomID == cupomID).ToList();

                if (AdministradorID > 0)
                    lstPedidos = lstPedidos.Where(c => c.AdministradorID == AdministradorID).ToList();

                if (status > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusProcessamentoID == status).ToList();

                if (statuspagamento > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusPagamentoID == statuspagamento).ToList();

                if (statusentrega > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusEntregaID == statusentrega).ToList();

                if (meiopagamento > 0)
                    lstPedidos = lstPedidos.Where(c => c.MeioPagamentoID == meiopagamento).ToList();

                if (!String.IsNullOrEmpty(PossuiFoto))
                {
                    if (PossuiFoto == "SIM")
                        lstPedidos = lstPedidos.Where(p => p.PedidoItems.Where(c => c.FotoEntrega != "").Any()).ToList();

                    if (PossuiFoto == "NAO")
                        lstPedidos = lstPedidos.Where(p => p.PedidoItems.Where(c => c.FotoEntrega == "").Any()).ToList();
                }

            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstPedidos.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lstPedidos = lstPedidos.Skip((paginaAtual - 1) * Configuracoes.ITENS_PAGINA_ADMIN).Take(Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.LstUsuarios = administradorRepository.GetByExpression(r => r.Liberado).OrderBy(r => r.Nome).ToList();

            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";



            return View(lstPedidos);
        }
        //----------------------------------------------------------------------
        public ActionResult Novo()
        {
            Cliente cliente = new Cliente();
            var estados = estadoRepository.GetAll().ToList();
            var cidades = cidadeRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
            List<Estado> lstEstados = new List<Estado>();

            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }

            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

            ViewBag.Estados = new SelectList(lstEstados, "ID", "Sigla");
            ViewBag.Cidades = new SelectList(cidades, "ID", "nome");

            var produtos = produtoRepository.GetByExpression(c => c.Disponivel || c.TipoID == (int)Produto.Tipos.Personalizado).OrderBy(c => c.TipoID).OrderBy(c => c.Nome).ToList();
            ViewBag.Produtos = produtos;

            ViewBag.Configuracoes = configuracaoRepository.GetAll().FirstOrDefault();
            int[] origensPrincipais = new[] { 21, 3, 6, 10, 23, 27, 34, 24, 7, 14, 12, 25, 38, 41 };
            List<OrigemSite> lstOrigens = new List<OrigemSite>();
            var origens = origemSiteRepository.GetAll().ToList();
            foreach (int idOrigem in origensPrincipais)
            {
                lstOrigens.Add(origens.FirstOrDefault(o => o.ID == idOrigem));
            }
            ViewBag.OrigemSite = new SelectList(lstOrigens, "Titulo", "Titulo");
           
            if (!String.IsNullOrEmpty(Request.QueryString["documento"]))
            {
                List<Pedido> LstPedidosHistorico = new List<Pedido>();
                var documento = Request.QueryString["documento"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                foreach(var ClienteResult in clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento).ToList())
                {
                    var estad = ClienteResult.EstadoID;
                    var cid = ClienteResult.CidadeID;
                    ViewBag.ClienteID = ClienteResult.ID;

                    LstPedidosHistorico.AddRange(ClienteResult.Pedidoes);

                    cliente = ClienteResult;
                }

                ViewBag.Pedidos = LstPedidosHistorico.OrderByDescending(c => c.DataCriacao).Take(5);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["documento"]))
            {
                var Doc = Request.QueryString["documento"].Replace(".", "").Replace("/", "").Replace("-", "");
                var LstClientesLeadPesquisar = clienteRepository.GetByExpression(r => r.Documento == Doc).ToList();

                foreach (var Cliente in LstClientesLeadPesquisar)
                {
                    if (leadScoreRepository.GetByExpression(r => r.Pedido.Cliente.ID == Cliente.ID).Any())
                    {
                        ViewBag.JaRepondeuLead = true;                        
                    }
                }
            }

            return View(cliente);
        }
        //----------------------------------------------------------------------

        public ActionResult Pagar(int id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);

            if (pedido.MeioPagamentoID != (int)Pedido.MeiosPagamento.PagarMe || pedido.StatusCancelamentoID != null || pedido.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.Pago)
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Erro!" });
            }

            return View(pedido);
        }

        public JsonResult ProcessarPagamento(int PedidoID, string hash, int Parcelas)
        {
            var pedido = pedidoService.PagarPedidoADM(PedidoID, hash, Parcelas);

            if(pedido == null)
            {
                return Json("Erro. Pedido Null");
            }
            else{
                switch (pedido.Retorno)
                {
                    case "Paid":
                        return Json("Paid");
                    case "Refused":
                        return Json("Recusada");
                    case "Error":
                        return Json("Erro. Pagar Pedido: " + pedido.Retorno + " / " + pedido.Mensagem);
                    default:
                        return Json("Erro. Genérico");
                }
            }
        }

        public ActionResult Visualizar(int id)
        {

            List<FornecedorLocal> lstFornecedorLocal = new List<FornecedorLocal>();
            List<int> IdsToRemove = new List<int>();

            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);

            //var a = pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Estado.ID;

            if (pedido != null)
            {
              
                #region PREENCHE FORNECEDOR RECUSOU ENTREGA

                if (pedido.AdministradorPedidoes.Where(r => r.AcaoID == 22).ToList().Count() > 0)
                {
                    int IdFOrnecedorRecusouEntrega = (int)pedido.AdministradorPedidoes.Where(r => r.AcaoID == 22).First().IdFornecedor;
                    ViewBag.FornecedorRecusouEntrega = fornecedorRepository.Get(IdFOrnecedorRecusouEntrega).Nome;
                }


                #endregion

                var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
                ViewBag.PromocaoSemana = promocao;
                ViewBag.StatusEntregaPedido = pedido.StatusEntrega;

                ViewBag.Produtos = produtoRepository.GetByExpression(c => c.Disponivel).ToList();

                #region VIEW BAG ESTADOS E CIDADES 

                var estados = estadoRepository.GetAll().ToList();
                int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                List<Estado> lstEstados = new List<Estado>();
                foreach (int idEstado in principaisEstados)
                {
                    lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                }
                lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                ViewBag.Estados = lstEstados;
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == pedido.EstadoID).OrderBy(e => e.Nome);

                #endregion

                #region VIEW BAG ESTADOS E CIDADES NF SUBSTITUTA

                if (pedido.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade != null)
                {
                    var IDResult = (int)pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Estado.ID;

                    ViewBag.cidadesNFSubstituta = cidadeRepository.GetByExpression(e => e.EstadoID == IDResult).OrderBy(e => e.Nome);
                }
                    

                #endregion

                var ResultLocais = localRepository.GetByExpression(r => r.CidadeID == pedido.CidadeID).OrderBy(r => r.Titulo).ToList().Select(r => new SelectListItem { Value = r.ID.ToString(), Text = r.Titulo + " - " +  r.Telefone }).ToList();

                ViewBag.Locais = ResultLocais;

                ViewBag.StatusCancelamento = statusCancelamentoRepository.GetByExpression(s => s.Ativo.HasValue && s.Ativo.Value).OrderBy(s => s.Titulo).ToList();
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante"));
                if (dir.Exists)
                    ViewBag.Comprovantes = dir.GetFiles().ToList();

                var blackList = blackListRepository.GetByExpression(b => b.Documento.Replace(".", "").Replace("-", "").Replace("/", "") == pedido.Cliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "")).FirstOrDefault();
                ViewBag.ClienteBlackList = blackList;

                foreach (var Item in pedido.AdministradorPedidoes.Where(r => r.IdFornecedor != null).ToList())
                {
                    var Result = fornecedorlocalRepository.GetByExpression(r => r.FornecedorID == (int)Item.IdFornecedor).FirstOrDefault();

                    if (Result != null)
                    {
                        Item.NomeFornecedor = Result.Fornecedor.Nome;
                    }

                }

                ViewBag.FornecedorAutomatico = pedido.FornecedorAutomatico;

                var LIgacoes = pedido.Pedido_X_Ligacao.Where(r => r.Pedido.ID == pedido.ID).ToList();

                if(LIgacoes.Count > 0)
                {
                    ViewBag.Ligacao = LIgacoes.First().Ligaco;

                    if(LIgacoes.First().Ligaco.IdLinha != null)
                    {
                        int IdDaLinha = (int)LIgacoes.First().Ligaco.IdLinha;
                        ViewBag.Linha = linhasRepository.Get(IdDaLinha);
                    }

                }

                var LstIDCoroasRuben = "464, 465, 466, 467, 468, 470, 471, 472, 473";
                var LstCoroasRubens = LstIDCoroasRuben.Split(',').Select(Int32.Parse).ToList();

                ViewBag.TravaFornecedor = false;
                if (pedido.PedidoItems.Where(r => LstCoroasRubens.Contains(r.ProdutoTamanho.ProdutoID)).Any())
                    ViewBag.TravaFornecedor = true;

                return View(pedido);
            }
            else
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Pedido inexistente!" });
        }
        //----------------------------------------------------------------------
        public ContentResult Boleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.BoletoPagarMes.Count > 0)
                {
                    string url = @"http://api.pdflayer.com/api/convert?access_key=422086415fa2a1a3913b98f08eb38172&document_url=" + pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).First().BoletoURL + "&document_name=" + id + ".pdf";
                    Response.Redirect(url);

                    return Content("");
                }
                else if (pedido.Boleto != null)
                {
                    var html = BoletoService.Gerar(pedido.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, pedido.Numero + "_boleto.pdf");
                    return Content("");
                }
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
        //----------------------------------------------------------------------
        public ContentResult MontaBoleto(int id)
        {
            var boleto = boletoRepository.Get(id);
            if (boleto != null)
            {
                var html = BoletoService.Gerar(boleto);
                Domain.Core.Funcoes.ImprimirPDF(html, boleto.Pedido.Numero + "_boleto.pdf");
                return Content("");
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
        //----------------------------------------------------------------------
        //public void BoletoPDF(int id)
        //{
        //    var pedido = pedidoRepository.Get(id);
        //    if (pedido != null)
        //    {
        //        if (pedido.Boleto != null)
        //        {
        //            var html = BoletoService.Gerar(pedido.Boleto); 
        //            var pdf = Domain.Core.Funcoes.ImprimirPDF(html, id);

        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.AddHeader("Content-Disposition", "inline;filename=" + pdf);
        //            Response.ContentType = "application/pdf";
        //            Response.WriteFile(pdf);
        //            Response.Flush();
        //            Response.Clear();
        //        }
        //    }
        //}
        //----------------------------------------------------------------------
        public JsonResult Estornar(int id, string valor, string data)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var dataestorno = new DateTime();
                DateTime.TryParse(data, out dataestorno);

                decimal valorestorno = 0;
                Decimal.TryParse(valor, out valorestorno);

                if (dataestorno != new DateTime())
                    pedido.DataEstorno = dataestorno;

                if (valorestorno > 0)
                    pedido.ValorEstorno = valorestorno;

                var statuspagamentoid = pedido.StatusPagamentoID;
                pedido.StatusPagamento = Pedido.TodosStatusPagamento.Estornado;
                pedidoRepository.Save(pedido);

                var historico = "Pedido alterado por " + adminModel.Nome + "<br>" + "<b>Status Pagamento: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((Pedido.TodosStatusPagamento)statuspagamentoid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusPagamento) + "</i>"; ;
                var nota2 = new PedidoNota();
                nota2.DataCriacao = DateTime.Now;
                nota2.PedidoID = pedido.ID;
                nota2.Observacoes = historico;
                pedidoNotaRepository.Save(nota2);

                //TODO: Verificar qdo tiver um repasse pago para o fornecedor.

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarProduto(int id, int itemID, int produtoTamanhoID, decimal valor)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando))
                {
                    var produtoanterior = pedido.PedidoItems.Where(c => c.ID == itemID).FirstOrDefault();
                    var nomeproduto = produtoanterior.ProdutoTamanho.Produto.Nome.ToUpper();
                    var valoranterior = produtoanterior.Valor;

                    ////REMOVE PRODUTO ATUAL
                    //pedidoItemRepository.Delete(produtoanterior);

                    //ALTERA PRODUTO
                    var pedidoitem = produtoanterior;
                    pedidoitem.Mensagem = produtoanterior.Mensagem;
                    pedidoitem.Descricao = produtoanterior.Descricao;
                    pedidoitem.Observacoes = produtoanterior.Observacoes;
                    pedidoitem.PedidoID = pedido.ID;
                    pedidoitem.ProdutoTamanhoID = produtoTamanhoID;
                    pedidoitem.Valor = valor;
                    pedidoItemRepository.Save(pedidoitem);
                    pedidoitem.Descricao = pedidoitem.ProdutoTamanho.Produto.Nome;
                    pedidoItemRepository.Save(pedidoitem);


                    //ALTERA PEDIDO
                    decimal valortotal = 0;
                    foreach (var item in pedido.PedidoItems)
                    {
                        valortotal += item.Valor;
                    }
                    pedido.ValorTotal = valortotal;
                    pedidoRepository.Save(pedido);

                    ////REPASSE
                    //if (pedido.FornecedorID.HasValue)
                    //{
                    //    var repasse = new Repasse();
                    //    repasse.FornecedorID = pedido.FornecedorID;
                    //    repasse.PedidoItemID = pedidoitem.ID;
                    //    repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                    //    repasse.AdministradorID = adminModel.ID;
                    //    var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedido.FornecedorID).ToList();
                    //    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                    //    if (fornecedorpreco != null)
                    //    {
                    //        if (fornecedorpreco.Repasse.HasValue)
                    //        {
                    //            if (fornecedorpreco.Repasse.Value > 0)
                    //            {
                    //                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                    //            }
                    //        }
                    //    }
                    //    repasseRepository.Save(repasse);
                    //}

                    //CRIA UMA NOTA
                    var pedidonota = new PedidoNota();
                    pedidonota.PedidoID = pedido.ID;
                    pedidonota.DataCriacao = DateTime.Now;
                    pedidonota.Observacoes = adminModel.Nome + "<br>Produto alterado de " + nomeproduto + " no valor de " + valoranterior.ToString("C2") + " para " + pedidoitem.ProdutoTamanho.Produto.Nome.ToUpper();
                    pedidoNotaRepository.Save(pedidonota);

                    //SE TEM BOLETO
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Processado = false;
                        boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                        if (boleto.RemessaBoletoes.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                        boleto.Valor = pedido.ValorTotal;
                        boletoRepository.Save(boleto);
                    }

                    //SE TEM BOLETO PAGARME
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe && pedido.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe)
                    {
                        // CANCELA BOLETOS PAGARME
                        BoletoPagarMeFactory.CancelarBoletos(pedido);

                        var Result = BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());

                        if (string.IsNullOrEmpty(Result.Erro))
                        {
                            return Json("OK");
                        }
                        else
                        {
                            return Json(Result.Erro);
                        }
                    }

                    return Json("OK");
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AdicionarProduto(int id, int produtoTamanhoID, decimal valor, string frase)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando))
                {

                    //ADICIONA NOVO PRODUTO
                    var pedidoitem = new PedidoItem();
                    pedidoitem.Mensagem = frase.ToUpper();
                    pedidoitem.Descricao = "";
                    pedidoitem.Observacoes = "";
                    pedidoitem.PedidoID = pedido.ID;
                    pedidoitem.ProdutoTamanhoID = produtoTamanhoID;
                    pedidoitem.Valor = valor;
                    pedidoItemRepository.Save(pedidoitem);
                    pedidoitem.Descricao = pedidoitem.ProdutoTamanho.Produto.Nome;
                    pedidoitem.StatusFotoID = 1;
                    pedidoItemRepository.Save(pedidoitem);

                    //ALTERA PEDIDO
                    pedido.ValorTotal = pedido.ValorTotal + pedidoitem.Valor;

                    if (pedidoitem.ProdutoTamanho.Produto.TipoID != 1)
                    {
                        pedido.LocalID = null;
                    }

                    pedidoRepository.Save(pedido);

                    //REPASSE
                    if (pedido.FornecedorID.HasValue)
                    {
                        var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == pedidoitem.ID).FirstOrDefault();
                        if (repasse == null)
                        {
                            repasse = new Repasse();
                            repasse.FornecedorID = pedido.FornecedorID;
                            repasse.PedidoItemID = pedidoitem.ID;
                            repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                            repasse.AdministradorID = adminModel.ID;
                            repasse.DataCriacao = DateTime.Now;
                            repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                            var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedido.FornecedorID).ToList();
                            var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                            if (fornecedorpreco != null)
                            {
                                if (fornecedorpreco.Repasse.HasValue)
                                {
                                    if (fornecedorpreco.Repasse.Value > 0)
                                    {
                                        repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                    }
                                }
                            }
                            repasseRepository.Save(repasse);
                        }
                    }

                    //CRIA UMA NOTA
                    var pedidonota = new PedidoNota();
                    pedidonota.PedidoID = pedido.ID;
                    pedidonota.DataCriacao = DateTime.Now;
                    pedidonota.Observacoes = adminModel.Nome + "<br>Produto " + pedidoitem.ProdutoTamanho.Produto.Nome.ToUpper() + " adicionado no valor de " + pedidoitem.Valor.ToString("C2");
                    pedidoNotaRepository.Save(pedidonota);

                    //SE TEM BOLETO
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Processado = false;
                        boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                        if (boleto.RemessaBoletoes.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                        boleto.Valor = pedido.ValorTotal;
                        boletoRepository.Save(boleto);
                    }

                    //SE TEM BOLETO PAGARME
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe && pedido.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe)
                    {
                        // CANCELA BOLETOS PAGARME
                        BoletoPagarMeFactory.CancelarBoletos(pedido);

                        var Result = BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());

                        if (string.IsNullOrEmpty(Result.Erro))
                        {
                            return Json("OK");
                        }
                        else
                        {
                            return Json(Result.Erro);
                        }
                    }

                    return Json("OK");
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult RemoverProduto(int id, int itemID)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando))
                {
                    var produtoanterior = pedido.PedidoItems.Where(c => c.ID == itemID).FirstOrDefault();
                    if (produtoanterior.RepasseAtual.ID == 0)
                    {
                        var nomeprodutoanterior = produtoanterior.ProdutoTamanho.Produto.Nome.ToUpper();
                        var valorprodutoanterior = produtoanterior.Valor.ToString("C2");
                        //REMOVE PRODUTO ATUAL
                        pedidoItemRepository.Delete(produtoanterior);

                        //ALTERA PEDIDO
                        pedido.ValorTotal = pedido.ValorTotal - produtoanterior.Valor;
                        pedidoRepository.Save(pedido);

                        //CRIA UMA NOTA
                        var pedidonota = new PedidoNota();
                        pedidonota.PedidoID = pedido.ID;
                        pedidonota.DataCriacao = DateTime.Now;
                        pedidonota.Observacoes = adminModel.Nome + "<br>Produto " + nomeprodutoanterior + " no valor de " + valorprodutoanterior + " removido ";
                        pedidoNotaRepository.Save(pedidonota);

                        //SE TEM BOLETO
                        if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                        {
                            var boleto = pedido.Boleto;
                            boleto.Processado = false;
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            if (boleto.RemessaBoletoes.Count > 0)
                                boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                            boleto.Valor = pedido.ValorTotal;
                            boletoRepository.Save(boleto);
                        }

                        if(pedido.PedidoItems.Count > 0)
                        {
                            //SE TEM BOLETO PAGARME
                            if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe && pedido.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe)
                            {
                                // CANCELA BOLETOS PAGARME
                                BoletoPagarMeFactory.CancelarBoletos(pedido);

                                var Result = BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());

                                if (string.IsNullOrEmpty(Result.Erro))
                                {
                                    return Json("OK");
                                }
                                else
                                {
                                    return Json(Result.Erro);
                                }
                            }
                        }

                        return Json("OK");
                    }
                    else {
                        return Json("O produto não pode ser removido porque possui um repasse em aberto. Cancele o repasse e faça a alteração novamente.");
                    }
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarVencimentoBoleto(int id, string data)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouDataVencimento);

                    if(pedido.FormaPagamentoID == (int)Pedido.FormasPagamento.Boleto)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Processado = false;
                        boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                        if (boleto.RemessaBoletoes.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;
                        boleto.DataVencimento = datavencimento;
                        boleto.DataEnvioLembreteVencimento = null;
                        boletoRepository.Save(boleto);
                    }
                    if (pedido.FormaPagamentoID == (int)Pedido.FormasPagamento.BoletoPagarMe)
                    {
                        var BoletoAtual = pedido.BoletoPagarMeReturn;
                        BoletoAtual.DataVencimento = datavencimento;
                        BoletoPagarMeFactory.Alterar(BoletoAtual, Remessa.Empresa.CPV);
                    }

                    return Json("OK");
                }
                else
                    return Json("Data inválida");

            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------        
        public JsonResult FornecedorInfo(int fornecedorID, int PedidoID)
        {
            var fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null)
            {
                string ValorMinimoDeCoroa = "R$ 0,00";
                if (fornecedor.ValorMinimoDeCoroa != null)
                    ValorMinimoDeCoroa = fornecedor.ValorMinimoDeCoroa.Value.ToString("C2");

                var LstRepasses = repasseRepository.GetByExpression(r => r.FornecedorID == fornecedor.ID && r.PedidoItemID != null && r.PedidoItem.Pedido.StatusCancelamentoID == null).OrderByDescending(r => r.PedidoItem.Pedido.DataCriacao).Take(10).ToList().Select(r => new
                {
                    Produto = r.PedidoItem.ProdutoTamanho.Produto.Nome + " - " + r.PedidoItem.ProdutoTamanho.Tamanho.NomeCompleto,
                    Valor = r.ValorRepasse,
                    PedidoID = r.PedidoItem.Pedido.ID, 
                    DataCriacao = r.PedidoItem.Pedido.DataCriacao.ToShortDateString()
                });

                var LstProdutos = pedidoRepository.Get(PedidoID).PedidoItems.Select(r => new
                {
                    Produto = r.ProdutoTamanho.Produto.Nome + " - " + r.ProdutoTamanho.Tamanho.NomeCompleto,
                    RepasseIdeal = r.ValorIdealRepasse
                });

                return Json(new {
                    retorno = "OK",
                    caracteristicas = !string.IsNullOrEmpty(fornecedor.Caracteristicas) ? fornecedor.Caracteristicas : "N/A",
                    AvgRepasse = LstRepasses.Average(r => r.Valor),
                    NrRepassesFornecedor = fornecedor.Repasses.Count,
                    ValorMinimoDeCoroa,
                    LstProdutos,
                    frete = fornecedor.ValorFrete > 0 ? fornecedor.ValorFrete.ToString("C2") : "N/A",
                    horario = !string.IsNullOrEmpty(fornecedor.HorarioFuncionamento) && fornecedor.HorarioFuncionamento != "" ? fornecedor.HorarioFuncionamento : "N/A",
                    problemas = !string.IsNullOrEmpty(fornecedor.ResponsavelProblemas) ? fornecedor.ResponsavelProblemas : "N/A",
                    nome = "" + fornecedor.Nome + "",
                    telefones = "" + fornecedor.TelefonePrimario + "<br>" + fornecedor.TelefoneSecundario + "<br>" + fornecedor.TelefonesAdicionais + "",
                    contato = "" + fornecedor.PessoaContato + "",
                    obs = "" + fornecedor.Observacao + "",
                    entrega24h = "" + (fornecedor.Entrega24h ? "SIM" : "NÃO") + "",
                    LstRepasses
                });
            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult FornecedorInfoPedidos(int fornecedorID)
        {
            List<Repasse> _lstPedidos = new List<Repasse>();
            Repasse _repasse = new Repasse();

            if (fornecedorID != 0)
            {
                var _repasses = repasseRepository.GetByExpression(c => c.FornecedorID == fornecedorID && c.StatusID != (int)Repasse.TodosStatus.Cancelado).Take(5).OrderByDescending(c => c.DataCriacao).ToList();
                if (_repasses != null)
                {
                    //var pedidos = _pedidoFornecedorLocal.OrderBy(c => c.Fornecedor.Nome).OrderBy(c => c.Ordem).Select(c => c.Fornecedor).ToList();
                    foreach (Repasse repasse in _repasses)
                    {
                        if (repasse.PedidoEmpresa != null)
                        {
                            if (repasse.PedidoEmpresa.StatusEntrega == TodosStatusEntrega.PendenteFornecedor)
                            {
                                _lstPedidos.Add(new Repasse
                                {
                                    ID = (int)repasse.PedidoEmpresaID,
                                    AdministradorID = repasse.AdministradorID,
                                    DataCriacao = repasse.DataCriacao,
                                    PedidoItemID = repasse.PedidoItemID,
                                    PedidoItem = repasse.PedidoItem
                                });
                            }
                            else
                            {
                                return Json("Não é possível alterar o processamento de um pedido em operação");
                            }
                        }
                        else
                        {

                            _lstPedidos.Add(new Repasse
                            {

                                AdministradorID = repasse.AdministradorID,
                                DataCriacao = repasse.DataCriacao,
                                PedidoItemID = repasse.PedidoItemID,
                                PedidoItem = repasse.PedidoItem
                            });

                            foreach (var item in _lstPedidos)
                            {
                                item.PedidoItem.TipoFaixaID = (int)PedidoItem.TodosTiposFaixa.PF;
                            }
                        }
                    }
                }

                //if (pedido != null)
                //{
                //    return Json(new { retorno = "OK", numeropedidos = Pedidoes.Count(), pedido = pedido.FirstOrDefault().Numero, datacriacao = pedido.FirstOrDefault().DataCriacao.Date.ToString("dd/MM/yyyy"), produto = pedido.FirstOrDefault().PedidoItems.FirstOrDefault().ProdutoTamanho.Produto.Nome });

                //}
                //else
                //    return Json(new { retorno = "ERRO" });
                if (_lstPedidos != null)
                {
                    return Json(new { retorno = "OK", _lstPedidos }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 503;
                    return Json(new { retorno = "Que feio servidor, ERRO?." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 503;
                return Json(new { retorno = "Que feio servidor, ERRO?." }, JsonRequestBehavior.AllowGet);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult GetValorRepasse(int itemID)
        {
            var pedidoitem = pedidoItemRepository.Get(itemID);
            if (pedidoitem != null)
            {
                if (pedidoitem.Pedido.Fornecedor != null)
                {
                    var fornecedorpreco = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedidoitem.Pedido.FornecedorID).FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                    if (fornecedorpreco != null)
                    {
                        if (fornecedorpreco.Repasse.HasValue)
                        {
                            if (fornecedorpreco.Repasse.Value > 0)
                            {
                                return Json(new { retorno = "OK", valor = fornecedorpreco.Repasse.Value });
                            }
                        }
                    }
                }
                return Json(new { retorno = "OK", valor = 0 });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------

        #endregion

        #region HTTPPOST
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            try
            {

                #region Dados Cadastrais e Complementares do Cliente
                //----------------------------------------------------------------------
                var Documento = form["Documento"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                var Email = string.IsNullOrEmpty(form["Email"]) ? "" : form["Email"].Trim().ToLower();
                var EmailSecundario = string.IsNullOrEmpty(form["EmailSecundario"]) ? "" : form["EmailSecundario"].Trim().ToLower();

                var cliente = clienteRepository.GetByExpression(c => c.Email.Trim().ToLower() == Email).FirstOrDefault();

                if (cliente == null)
                    cliente = clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == Documento).FirstOrDefault();

                if (cliente == null)
                {
                    cliente = new Cliente();
                    cliente.Senha = Funcoes.CriarSenha(4);
                    cliente.DataCadastro = DateTime.Now;
                }

                cliente.Exterior = (form["Exterior"] == "on");
                cliente.TipoID = Convert.ToInt32(form["TipoID"]);
                cliente.TipoIE = Convert.ToInt32(form["TipoIE"]);

                if (Documento.Length > 0)
                    cliente.Documento = Documento;
                else
                    cliente.Documento = "N/I";

                cliente.Nome = form["Nome"].Trim().ToUpper();
                cliente.RazaoSocial = form["RazaoSocial"].Trim().ToUpper();

                if (cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.Isento)
                    cliente.InscricaoEstadual = "ISENTO";
                if (cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.NaoContribuinte)
                    cliente.InscricaoEstadual = "NÃO CONTRIBUINTE";
                if (cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.Contribuinte)
                    cliente.InscricaoEstadual = form["InscricaoEstadual"];

                cliente.PessoaContato = form["PessoaContato"];

                if (Email.Length > 0)
                    cliente.Email = Email;
                else
                    cliente.Email = "N/I";

                cliente.EmailSecundario = EmailSecundario;
                cliente.TelefoneContato = form["TelefoneContato"];
                cliente.CelularContato = form["CelularContato"];
                cliente.CEP = form["CEP"];
                try
                {
                    cliente.EstadoID = Convert.ToInt32(form["EstadoID"]);
                }
                catch { }
                try
                {
                    cliente.CidadeID = Convert.ToInt32(form["CidadeID"]);
                }
                catch { }

                cliente.Logradouro = form["Logradouro"];
                cliente.Numero = form["Numero"];
                cliente.Complemento = form["Complemento"];
                cliente.Bairro = form["Bairro"];

                ///Dados Complementares
                cliente.ComoConheceu = form["ComoConheceu"];

                ///Salva Dados preenchidos do Cliente
                clienteRepository.Save(cliente);

                //----------------------------------------------------------------------
                #endregion

                #region Dados Pedido
                //----------------------------------------------------------------------
                //PRODUTO (1)
                var ProdutoTamanhoID = Convert.ToInt32(form["ProdutoTamanhoID"]);
                var descricaoproduto1 = form["DescricaoProduto1"];
                var produtoTamanho = produtoTamanhoRepository.Get(ProdutoTamanhoID);

                //PRODUTO (2)
                var ProdutoTamanhoID2 = 0;
                Int32.TryParse(form["ProdutoTamanhoID2"], out ProdutoTamanhoID2);
                var descricaoproduto2 = form["DescricaoProduto2"];
                var produtoTamanho2 = produtoTamanhoRepository.Get(ProdutoTamanhoID2);

                //----------------------------------------------------------------------
                #endregion

                #region Detalhes de Entrega
                //----------------------------------------------------------------------

                int? _localEntregaId = null;

                if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                    _localEntregaId = int.Parse(form["LocalID"]);

                if (_localEntregaId == 0)
                    _localEntregaId = null;

                //----------------------------------------------------------------------
                #endregion

                #region Pagamento
                //----------------------------------------------------------------------
                var valortotal = Convert.ToDecimal(form["ValorTotal"]);
                var parcela = Convert.ToInt32(form["Parcela"]);
                int _FormaPagamentoID = 0;
                int _MeioPagamentoID = 0;
                int _StatusPagamentoID = 0;

                if (form["OrigemSite"].ToUpper() == "AMAR ASSIST" && form["OrigemForma"].ToUpper() == "ONLINE" && Convert.ToInt32(form["FormaPagamentoID"]) == 1)
                {
                    parcela = 1;
                    _FormaPagamentoID = 1;
                    _MeioPagamentoID = 16;
                    _StatusPagamentoID = 2;
                }
                else
                {
                    switch (Convert.ToInt32(form["FormaPagamentoID"]))
                    {
                        case (int)Pedido.FormasPagamento.BoletoPagarMe:
                            parcela = 1;
                            break;
                        case (int)Pedido.FormasPagamento.DepositoBancario:
                            parcela = 1;
                            break;
                        case (int)Pedido.FormasPagamento.TransferenciaBancaria:
                            parcela = 1;
                            break;
                        case (int)Pedido.FormasPagamento.DinheiroCheque:
                            parcela = 1;
                            break;
                        case (int)Pedido.FormasPagamento.Tradaq:
                            parcela = 1;
                            break;
                    }

                    _FormaPagamentoID = Convert.ToInt32(form["FormaPagamentoID"]);
                    _MeioPagamentoID = Convert.ToInt32(form["MeioPagamentoID"]);
                    _StatusPagamentoID = Convert.ToInt32(form["StatusPagamentoID"]);
                }

                decimal ValorDoDesconto = 0;

                if (!string.IsNullOrEmpty(form["ValorDesconto"]))
                    ValorDoDesconto = Convert.ToDecimal(form["ValorDesconto"]);
                //----------------------------------------------------------------------
                #endregion

                var _ComplementoLocalEntrega = "";
                if (!string.IsNullOrEmpty(form["ComplementoLocalEntrega"]))
                    _ComplementoLocalEntrega = form["ComplementoLocalEntrega"].ToString().ToUpper();

                #region Pedido - Instancia Pedido
                //----------------------------------------------------------------------
                var pedido = new Pedido()
                {
                    ClienteID = cliente.ID,
                    Codigo = Guid.NewGuid(),
                    Numero = CriarNumero(),
                    TID = "",
                    Retorno = "",
                    Origem = "ADMIN",
                    NomeSolicitante = string.Empty,
                    EmailSolicitante = string.Empty,
                    //NomeSolicitante = form["NomeSolicitante"].ToUpper(),
                    //EmailSolicitante = form["EmailSolicitante"].Trim().ToLower(),
                    Parentesco = "",
                    TelefoneContato = form["TelefoneContato"],

                    //OrigemTel = form["OrigemTel"],
                    OrigemSite = form["OrigemSite"].ToUpper(),
                    OrigemForma = form["OrigemForma"].ToUpper(),
                    Mensagem = "",

                    PessoaHomenageada = form["PessoaHomenageada"],

                    EstadoID = Convert.ToInt32(form["EstadoEntregaID"]),
                    CidadeID = Convert.ToInt32(form["CidadeEntregaID"]),
                    LocalID = _localEntregaId,
                    //LocalEntrega = string.IsNullOrEmpty(form["LocalEntrega"]) ? "LOCAL NÃO CADASTRADO" : form["LocalEntrega"],
                    ComplementoLocalEntrega = _ComplementoLocalEntrega,
                    DataSolicitada = string.IsNullOrEmpty(form["DataEntrega"]) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(form["DataEntrega"]),

                    ValorDesconto = ValorDoDesconto,
                    ValorTotal = valortotal,
                    NFeValor = valortotal,
                    Parcela = parcela,
                    FormaPagamentoID = _FormaPagamentoID,
                    MeioPagamentoID = _MeioPagamentoID,
                    StatusPagamentoID = _StatusPagamentoID,

                    Observacoes = "",

                    QualidadeStatusContatoID = (int)Pedido.TodosStatusContato.NI,
                    QualidadeStatusContatoNaoID = (int)Pedido.TodosStatusContatoNao.NI,

                    StatusProcessamento = Pedido.TodosStatusProcessamento.Criado,

                    DataAtualizacao = DateTime.Now,
                    DataCriacao = DateTime.Now,
                    DataInicioEdicao = Convert.ToDateTime(form["DataInicioEdicao"]),
                    StatusNfID = 1, 
                    AdministradorID = adminModel.ID
                };

                pedidoRepository.Save(pedido);

                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Pagamento
                //----------------------------------------------------------------------
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.Pago)
                {
                    pedido.DataPagamento = Convert.ToDateTime(form["DataPagamento"]);
                    pedidoRepository.Save(pedido);
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - ITENS
                //----------------------------------------------------------------------
                var item = new PedidoItem();
                item.PedidoID = pedido.ID;
                item.ProdutoTamanhoID = ProdutoTamanhoID;
                item.Observacoes = "";
                item.Mensagem = form["Frase"].ToUpper();
                item.Descricao = produtoTamanho.Produto.Nome;
                item.Valor = produtoTamanho.Preco;
                item.StatusFotoID = 1;
                pedidoItemRepository.Save(item);

                if (item.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                {
                    item.Descricao = "Produto personalizado: \n" + descricaoproduto1;
                    pedidoItemRepository.Save(item);
                }

                var item2 = new PedidoItem();
                if (produtoTamanho2 != null)
                {
                    item2.PedidoID = pedido.ID;
                    item2.ProdutoTamanhoID = ProdutoTamanhoID2;
                    item2.Observacoes = "";
                    item2.Mensagem = form["Frase2"].ToUpper();
                    item2.Descricao = produtoTamanho.Produto.Nome;
                    item2.Valor = produtoTamanho2.Preco;
                    item2.StatusFotoID = 1;
                    pedidoItemRepository.Save(item2);

                    if (item2.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                    {
                        item2.Descricao = "Produto personalizado: \n" + descricaoproduto2;
                        pedidoItemRepository.Save(item2);
                    }
                }

                if (item.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
                {
                    pedido.LocalID = null;
                    pedidoRepository.Save(pedido);
                }

                if (item2.ProdutoTamanho != null)
                {
                    if (item2.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
                    {
                        pedido.LocalID = null;
                        pedidoRepository.Save(pedido);
                    }
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Define o Status Inicial de Entrega do Pedido
                //----------------------------------------------------------------------
                //Verifica campos preenchidos para definir status inicial do pedido
                if (string.IsNullOrEmpty(pedido.PessoaHomenageada)
                    //|| string.IsNullOrEmpty(item.Mensagem)
                    || string.IsNullOrEmpty(pedido.EstadoID.ToString())
                    || string.IsNullOrEmpty(pedido.CidadeID.ToString())
                    || string.IsNullOrEmpty(pedido.DataSolicitada.ToString())
                    || pedido.DataSolicitada.ToString("dd/MM/yyyy").Equals("01/01/1753")
                    )
                {
                    pedido.StatusEntrega = TodosStatusEntrega.PendenteAtendimento;
                }
                else
                {
                    pedido.StatusEntrega = TodosStatusEntrega.PendenteFornecedor;

                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Observações
                //----------------------------------------------------------------------
                if (!String.IsNullOrEmpty(form["ObservacaoNota"]))
                {
                    var nota = new PedidoNota();
                    nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                    nota.DataCriacao = DateTime.Now;
                    nota.PedidoID = pedido.ID;
                    pedidoNotaRepository.Save(nota);
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pagamento - Forma Boleto
                //----------------------------------------------------------------------

                if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                {
                    var Boleto = BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());
                }
                //----------------------------------------------------------------------
                #endregion

                #region Envio de E-mail Pedido
                //----------------------------------------------------------------------

                pedido.EnviarPedidoEmailNovo();

                var enviarEmail = form["EnviarEmail"] == "on";

                if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                {
                    pedido.EnviarPedidoEmailBoleto();
                }

                //----------------------------------------------------------------------
                #endregion

                #region YOURVIEWS

                var StrYourView = "Erro ao enviar YourReviews: ";

                try
                {
                    var ResultYurViews = pedido.EnviarYourViews();

                    if (ResultYurViews.HasErrors)
                    {
                        foreach (var Erro in ResultYurViews.ErrorList)
                        {
                            StrYourView += Erro.Error + " / " + Erro.Field + " || ";
                        }
                    }
                    else
                    {
                        StrYourView = "YourViews enviado com sucesso";
                    }

                }
                catch (Exception ex)
                {
                    StrYourView += ex.Message;
                }

                var notaYourViews = new PedidoNota();

                notaYourViews.DataCriacao = DateTime.Now;
                notaYourViews.PedidoID = pedido.ID;

                notaYourViews.Observacoes = StrYourView;
                pedidoNotaRepository.Save(notaYourViews);

                #endregion

                #region LeadScore

                var LeadScoreValues = form["LeadScoreValue"];

                if (!string.IsNullOrEmpty(LeadScoreValues))
                {
                    var Pergunta1 = LeadScoreValues.Split(',')[0];
                    if (Pergunta1 == "sim")
                    {
                        Pergunta1 = "true";
                    }
                    else
                    {
                        Pergunta1 = "false";
                    }
                    var Pergunta2 = LeadScoreValues.Split(',')[1];
                    if (Pergunta2 == "sim")
                    {
                        Pergunta2 = "true";
                    }
                    else
                    {
                        Pergunta2 = "false";
                    }

                    leadScoreRepository.Save(new LeadScore
                    {
                        Pergunta1 = bool.Parse(Pergunta1),
                        Pergunta2 = bool.Parse(Pergunta2),
                        DataCriacao = DateTime.Now,
                        Pedido = pedido
                    });
                }

                #region Pedido - VtCall
                //----------------------------------------------------------------------

                if(pedido.OrigemForma == "TELEFONE")
                {
                    var DadosVtCall = form["DadosLigacao"].ToString().Split(',');

                    Ligaco novaLigacao = new Ligaco();
                    novaLigacao.IdLigacao = DadosVtCall[0].Replace("X", ".").Replace("#", "");

                    DateTime dt;
                    DateTime.TryParseExact(DadosVtCall[1].ToString(), "dd/MM/yyyy - HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);

                    if(dt.Year > 1)
                    {
                        novaLigacao.HoraAtendimento = dt;
                        novaLigacao.NrOrigem = DadosVtCall[2];
                        novaLigacao.NrDestino = DadosVtCall[3];
                        novaLigacao.TipoLigacao = DadosVtCall[4];
                        if (int.Parse(DadosVtCall[5]) > 0)
                            novaLigacao.IdLinha = int.Parse(DadosVtCall[5]);
                        novaLigacao.TempoLigacao = DadosVtCall[6];
                        novaLigacao.LinkAudio = DadosVtCall[7];

                        int Ramal = adminModel.CurrentAdministrador.Ramai.NumeroRamal;
                        novaLigacao.Ramai = ramaisRepository.GetByExpression(r => r.NumeroRamal == Ramal).First();

                        ligacoesRepository.Save(novaLigacao);

                        PedidoXligacaoRepository.Save(new Pedido_X_Ligacao
                        {
                            Pedido = pedido,
                            Ligaco = novaLigacao
                        });
                    }
                    
                }

                //----------------------------------------------------------------------
                #endregion

                #endregion

                if (pedido.MeioPagamentoID == (int)Pedido.MeiosPagamento.PagarMe)
                    Response.Redirect("/PedidosSite/Pagar/" + pedido.ID);

                #region MYMETRICS

                var Result = new Domain.Service.PedidoService().SendMYMEtrics(pedido, "VENDER");
                ViewBag.ResultMyMetrics = Result;

                #endregion

                return RedirectToAction("List", new { datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy"), statusentrega = 0, cod = "SaveSucess", msg = " O pedido " + pedido.Numero + " para o cliente " + cliente.Nome + " foi criado com sucesso!", PedidoID = pedido.ID  });
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy"), statusentrega = 0, cod = "SaveFailure", msg = "Falha na criação do Pedido!" + ex.Message });
            }
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var nota = new PedidoNota();
            var nota2 = new PedidoNota();
            var pedido = pedidoRepository.Get(id);
            var statusentregaid = 0;
            
            //----------------------------------------------------------------------
            if (pedido != null)
            {
                var datarepasseold = pedido.DataRepasse;
                var enviapesquisa = false;

                if (!string.IsNullOrEmpty(form["DataEntrega"]))
                {
                    enviapesquisa = pedido.DataEntrega != Convert.ToDateTime(form["DataEntrega"]) && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";
                }

                #region YOURVIEWS

                if (!string.IsNullOrEmpty(form["StatusEntregaID"]))
                {
                    if (Convert.ToInt32(form["StatusEntregaID"]) == 3 && pedido.StatusEntregaID != 3)
                    {
                        var StrYourView = "Erro ao enviar YourReviews: ";

                        try
                        {
                            var ResultYurViews = pedido.AtualizarYourViewsEntrega();

                            if (ResultYurViews.HasErrors)
                            {
                                foreach (var Erro in ResultYurViews.ErrorList)
                                {
                                    StrYourView += Erro.Error + " / " + Erro.Field + " || ";
                                }
                            }
                            else
                            {
                                StrYourView = "YourViews enviado com sucesso - Pedido marcado como entrege.";
                            }

                        }
                        catch (Exception ex)
                        {
                            StrYourView += ex.Message;
                        }

                        var notaYourViews = new PedidoNota();

                        notaYourViews.DataCriacao = DateTime.Now;
                        notaYourViews.PedidoID = pedido.ID;

                        notaYourViews.Observacoes = StrYourView;
                        pedidoNotaRepository.Save(notaYourViews);
                    }
                }

                #endregion

                #region STATUS DE ENTREGA

                statusentregaid = pedido.StatusEntregaID;

                if (!string.IsNullOrEmpty(form["StatusEntregaID"]))
                {
                    if (Convert.ToInt32(form["StatusEntregaID"]) == 3)
                    {
                        if (pedido.Repasse.Where(r => r.StatusID != (int)Repasse.TodosStatus.Cancelado).Sum(r => r.ValorRepasse) == 0)
                        {
                            return RedirectToAction("List", new { cod = "SaveFailure", msg = "Voce não pode alterar um pedido para entregue, se o repasse for 0." });
                        }
                        else
                        {
                            if (pedido.Repasse.Where(r => r.StatusID != (int)Repasse.TodosStatus.Cancelado).Any())
                            {
                                pedido.StatusEntregaID = Convert.ToInt32(form["StatusEntregaID"]);
                            }
                            else
                            {
                                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Voce não pode alterar um pedido para entregue, se ele nao tiver um repasse." });
                            }
                        }
                    }
                    else
                    {
                        pedido.StatusEntregaID = Convert.ToInt32(form["StatusEntregaID"]);
                    }
                }

                #endregion

                #region Dados do Pedido

                pedido.PessoaHomenageada = form["PessoaHomenageada"];
                pedido.Parentesco = form["Parentesco"];
                pedido.ParentescoRecebidoPor = form["ParentescoRecebidoPor"];
                pedido.RecebidoPor = form["RecebidoPor"];
                pedido.TelefoneContato = form["TelefoneContato"];

                int? _localEntregaId = null;

                if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                    _localEntregaId = int.Parse(form["LocalID"]);

                if (_localEntregaId == 0)
                    _localEntregaId = null;

                if (pedido.LocalID != _localEntregaId)
                {
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
                }

                pedido.LocalID = _localEntregaId;

                if (!string.IsNullOrEmpty(form["DataEntrega"]))
                {
                    pedido.DataEntrega = Convert.ToDateTime(form["DataEntrega"]);
                    enviapesquisa = pedido.DataEntrega != Convert.ToDateTime(form["DataEntrega"]) && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";
                }

                if (!string.IsNullOrEmpty(form["DataSolicitada"]))
                {
                    pedido.DataSolicitada = Convert.ToDateTime(form["DataSolicitada"]);
                }

                pedido.EstadoID = Convert.ToInt32(form["EstadoID"]);

                if (!string.IsNullOrEmpty(form["CidadeID"]))
                {
                    pedido.CidadeID = int.Parse(form["CidadeID"]);
                }

                if (!string.IsNullOrEmpty(form["LocalEntrega"]))
                {
                    pedido.LocalEntrega = form["LocalEntrega"];
                }
                else
                {
                    pedido.LocalEntrega = null;
                }

                if(!string.IsNullOrEmpty(form["ComplementoLocalEntrega"]))
                    pedido.ComplementoLocalEntrega = form["ComplementoLocalEntrega"].ToString().ToUpper();

                pedido.Observacoes = form["Observacoes"];
                pedido.ObservacoesPagamento = form["ObservacoesPagamento"];

                if (!string.IsNullOrEmpty(form["NFeNumero"]))
                {
                    var numeroNF = Convert.ToInt32(form["NFeNumero"]);

                    if (pedido.NFeNumero != numeroNF)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.PreencheuNF);
                    }

                    pedido.NFeNumero = numeroNF;
                }
                else
                {
                    pedido.NFeNumero = null;
                    pedido.DataEmailNFe = null;
                }

                if (!string.IsNullOrEmpty(form["NFeValor"]))
                {
                    pedido.NFeValor = Convert.ToDecimal(form["NFeValor"]);
                }
                else
                {
                    pedido.NFeValor = 0;
                }

                if (!string.IsNullOrEmpty(form["NFeData"]))
                {
                    pedido.NFeData = Convert.ToDateTime(form["NFeData"]);
                }
                else
                {
                    pedido.NFeData = null;
                }

                if (!string.IsNullOrEmpty(form["ObservacaoNFE"]))
                {
                    pedido.ObservacaoNFE = form["ObservacaoNFE"];
                }
                else
                {
                    pedido.ObservacaoNFE = "";
                }

                var enviaNFe = !String.IsNullOrEmpty(pedido.NFeLink) && pedido.NFeLink != form["NFeLink"] && !pedido.DataEmailNFe.HasValue;

                pedido.CaminhoXmlNotaFiscal = form["CaminhoXmlNotaFiscal"].Trim();
                pedido.NFeLink = form["NFeLink"].Trim();

                pedido.DataAtualizacao = DateTime.Now;
                //----------------------------------------------------------------------
                #endregion

                #region NF SUBSTITUTA

                if (!string.IsNullOrEmpty(form["NFSubstituta.Documento"])) {

                    var NFSubstituta_NomeRazao = form["NFSubstituta.NomeRazao"];
                    var NFSubstituta_Documento = form["NFSubstituta.Documento"];
                    var NFSubstituta_IE = form["NFSubstituta.IE"];
                    var NFSubstituta_Logradouro = form["NFSubstituta.Logradouro"];
                    var NFSubstituta_Numero = form["NFSubstituta.Numero"];
                    var NFSubstituta_Complemento = form["NFSubstituta.Complemento"];
                    var NFSubstituta_CEP = form["NFSubstituta.CEP"];
                    var NFSubstituta_TipoIE = form["NFSubstituta.TipoIE"];

                    int NFSubstituta_EstadoID = 0;
                    if (!string.IsNullOrEmpty(form["NFSubstituta_EstadoID"]))
                    {
                        NFSubstituta_EstadoID = int.Parse(form["NFSubstituta_EstadoID"]);
                    }

                    int NFSubstituta_CidadeID = 0;
                    if (!string.IsNullOrEmpty(form["NFSubstituta_CidadeID"]))
                    {
                        NFSubstituta_CidadeID = int.Parse(form["NFSubstituta_CidadeID"]);
                    }


                    var NFSubstituta_Bairro = form["NFSubstituta.Bairro"];

                    if (pedido.NFSubstituta_X_Pedido_PedidoEmpresa.Any())
                    {
                        var NfSubstituta = pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First();
                        NfSubstituta.NFSubstituta.NomeRazao = NFSubstituta_NomeRazao;
                        NfSubstituta.NFSubstituta.Documento = NFSubstituta_Documento;
                        NfSubstituta.NFSubstituta.Logradouro = NFSubstituta_Logradouro;
                        NfSubstituta.NFSubstituta.Numero = NFSubstituta_Numero;
                        NfSubstituta.NFSubstituta.Complemento = NFSubstituta_Complemento;
                        NfSubstituta.NFSubstituta.CEP = NFSubstituta_CEP;
                        NfSubstituta.NFSubstituta.Estado = estadoRepository.Get(NFSubstituta_EstadoID);
                        NfSubstituta.NFSubstituta.Cidade = cidadeRepository.Get(NFSubstituta_CidadeID);
                        NfSubstituta.NFSubstituta.Bairro = NFSubstituta_Bairro;
                        NfSubstituta.NFSubstituta.TipoIE = int.Parse(NFSubstituta_TipoIE);

                        switch (NfSubstituta.NFSubstituta.TipoIE)
                        {
                            case 1:
                                NfSubstituta.NFSubstituta.IE = "ISENTO";
                                break;
                            case 2:
                                NfSubstituta.NFSubstituta.IE = "NÃO CONTRIBUINTE";
                                break;
                            default:
                                NfSubstituta.NFSubstituta.IE = NFSubstituta_IE;
                                break;
                        }
                    }
                    else
                    {
                        var NewNfSubstitura = new NFSubstituta_X_Pedido_PedidoEmpresa();
                        NewNfSubstitura.NFSubstituta = new NFSubstituta();

                        NewNfSubstitura.NFSubstituta.NomeRazao = NFSubstituta_NomeRazao;
                        NewNfSubstitura.NFSubstituta.Documento = NFSubstituta_Documento;
                        NewNfSubstitura.NFSubstituta.Logradouro = NFSubstituta_Logradouro;
                        NewNfSubstitura.NFSubstituta.Numero = NFSubstituta_Numero;
                        NewNfSubstitura.NFSubstituta.Complemento = NFSubstituta_Complemento;
                        NewNfSubstitura.NFSubstituta.CEP = NFSubstituta_CEP;
                        NewNfSubstitura.NFSubstituta.Estado = estadoRepository.Get((int)NFSubstituta_EstadoID);
                        NewNfSubstitura.NFSubstituta.Cidade = cidadeRepository.Get((int)NFSubstituta_CidadeID);
                        NewNfSubstitura.NFSubstituta.Bairro = NFSubstituta_Bairro;
                        NewNfSubstitura.NFSubstituta.TipoIE = int.Parse(NFSubstituta_TipoIE);

                        switch (NewNfSubstitura.NFSubstituta.TipoIE)
                        {
                            case 1:
                                NewNfSubstitura.NFSubstituta.IE = "ISENTO";
                                break;
                            case 2:
                                NewNfSubstitura.NFSubstituta.IE = "NÃO CONTRIBUINTE";
                                break;
                            default:
                                NewNfSubstitura.NFSubstituta.IE = NFSubstituta_IE;
                                break;
                        }


                        pedido.NFSubstituta_X_Pedido_PedidoEmpresa.Add(NewNfSubstitura);

                        var NotaNfSubstituta = new PedidoNota();
                        NotaNfSubstituta.DataCriacao = DateTime.Now;
                        NotaNfSubstituta.PedidoID = pedido.ID;
                        NotaNfSubstituta.Observacoes = "Nova NF Substituta criada. NF antiga: " + pedido.NFeLink;
                        pedidoNotaRepository.Save(NotaNfSubstituta);
                    }
                }

                #endregion

                #region NFE
                //----------------------------------------------------------------------
                if (enviaNFe)
                {
                    pedido.EnviarNFe();
                    pedido.DataEmailNFe = DateTime.Now;
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailNFe);
                }
                //----------------------------------------------------------------------
                #endregion

                #region ADMINISTRADOR ID

                if (pedido.AdministradorID == 1043)
                {
                    pedido.AdministradorID = adminModel.ID;
                }

                pedidoRepository.Save(pedido);

                #endregion

                foreach (var item in pedido.PedidoItems.ToList())
                {
                    var foto = Request.Files["foto_" + item.ID];

                    if (foto != null && foto.ContentLength > 0)
                    {
                        #region UPLOAD FOTO ENTREGA

                        DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero));
                        if (!dir.Exists)
                        {
                            dir.Create();
                        }

                        var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                        //FOTO
                        ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedido.Numero + "/" + item.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=both"));

                        i.CreateParentDirectory = true;
                        i.Build();

                        item.StatusFotoID = 3;

                        #endregion
                    }
                    else
                    {
                        #region STATUS DA FOTO

                        if (!string.IsNullOrEmpty(form["StatusFotoID_" + item.ID]))
                        {
                            // GRAVA HISTORICO DE STATUS DE FOTO
                            if (int.Parse(form["StatusFotoID_" + item.ID]) == 3)
                            {
                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ValidouFoto);
                                item.StatusFotoID = int.Parse(form["StatusFotoID_" + item.ID]);
                            }

                            if (int.Parse(form["StatusFotoID_" + item.ID]) == 1)
                            {
                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ReprovouFoto);
                                item.StatusFotoID = int.Parse(form["StatusFotoID_" + item.ID]);

                                // EXCLUI FOTO DO SERVIDOR
                                var PathFoto = "E:\\WEB\\cdn.coroasparavelorio.com.br\\Pedidos\\" + pedido.Numero + "\\" + item.ID + ".jpg";
                                //var PathFoto = Server.MapPath("/content/pedidos/" + pedido.Numero + "/" + item.ID + ".jpg");
                                System.IO.File.Delete(PathFoto);
                            }
                        }

                        #endregion
                    }

                    item.Mensagem = form["Mensagem_" + item.ID];
                    
                    if (!string.IsNullOrEmpty(form["TipoFaixaID_" + item.ID]))
                    {
                        item.TipoFaixaID = Convert.ToInt32(form["TipoFaixaID_" + item.ID]);
                    }

                    pedidoItemRepository.Save(item);
                }

                #region Comprovante Pagamento

                if (Request.Files.Count > 0)
                {
                    var comprovante = Request.Files["comprovante"];
                    if (comprovante.ContentLength > 0)
                    {
                        DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante"));
                        if (!dir.Exists)
                        {
                            dir.Create();
                        }
                        var extensao = Path.GetExtension(comprovante.FileName);
                        comprovante.SaveAs(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante/comprovante-pagto-" + DateTime.Now.ToString("yyyyMMddHHmm") + extensao));
                    }
                }

                #endregion


                #region Historico Status Entrega
                //----------------------------------------------------------------------
                var historico = "";
                if (pedido.StatusEntregaID != statusentregaid)
                    historico += "<b>Status Entrega: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((TodosStatusEntrega)statusentregaid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusEntrega) + "</i>";
                if (historico.Length > 0)
                {
                    historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;
                    nota2.DataCriacao = DateTime.Now;
                    nota2.PedidoID = pedido.ID;
                    nota2.Observacoes = historico;
                }
                //----------------------------------------------------------------------
                #endregion

                #region Notas
                //----------------------------------------------------------------------
                if (form["ObservacaoNota"].Length > 0)
                {

                    nota.DataCriacao = DateTime.Now;
                    nota.PedidoID = pedido.ID;
                    nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pesquisa
                //----------------------------------------------------------------------
                if (enviapesquisa)
                {
                    if (Configuracoes.ENVIA_TRUSTVOX)
                    {
                        pedido.DataEnvioPesquisa = null;
                    }
                    else
                    {
                        pedido.EnviarEmailPesquisa();
                        pedido.DataEnvioPesquisa = DateTime.Now;
                    }
                }
                //----------------------------------------------------------------------
                #endregion

                #region SALVA DADOS PEDIDO
                //----------------------------------------------------------------------

                if (historico.Length > 0)
                    pedidoNotaRepository.Save(nota2);

                if (form["ObservacaoNota"].Length > 0)
                    pedidoNotaRepository.Save(nota);

                pedidoRepository.Save(pedido);

                //----------------------------------------------------------------------
                #endregion


                return RedirectToAction("Visualizar", new { id = pedido.ID, cod = "SaveSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" });
            }
        }
        //----------------------------------------------------------------------
        public JsonResult CriarRepasse(int itemID, decimal valor)
        {
            var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == itemID).Where(p => p.StatusID != 3).FirstOrDefault();
            if (repasse == null)
            {
                var pedidoitem = pedidoItemRepository.Get(itemID);
                repasse = new Repasse();
                repasse.FornecedorID = pedidoitem.Pedido.FornecedorID;
                repasse.PedidoItemID = pedidoitem.ID;
                repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                repasse.AdministradorID = adminModel.ID;
                repasse.ValorRepasse = valor;
                repasse.DataCriacao = DateTime.Now;
                repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                repasseRepository.Save(repasse);

                administradorPedidoRepository.Registra(adminModel.ID, pedidoitem.PedidoID, null, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                return Json(new { retorno = "OK" });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarRepasse(int id, int itemID, decimal valor)
        {
            var pedidoitem = pedidoItemRepository.Get(itemID);
            var pedido = pedidoitem.Pedido;

            var repasse = repasseRepository.Get(id);
            if (repasse != null)
            {
                if (repasse.Status == Repasse.TodosStatus.AguardandoPagamento)
                {
                    repasse.Observacao = "\nRepasse alterado de " + repasse.ValorRepasse.ToString("C2") + " para " + valor.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                    repasse.ValorRepasse = valor;
                    repasseRepository.Save(repasse);

                    administradorPedidoRepository.Registra(adminModel.ID, pedidoitem.PedidoID, null, null, null, AdministradorPedido.Acao.AlterouRepasse);

                    return Json(new { retorno = "OK" });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "O valor do repasse só pode ser alterado se estiver aguardando pagamento." });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult CancelarLeilaoFornecedor(int pedidoID, int localID)
        {
            var retorno = "";
            try
            {
                var _acaoExistente = administradorPedidoRepository.GetByExpression(c => c.AcaoID == (int)AdministradorPedido.Acao.RecusouEntrega && c.Pedido.ID == pedidoID).Count();

                if (_acaoExistente == 0)
                {
                    var lstFornecedoresLocal = fornecedorlocalRepository.GetByExpression(c => c.LocalID == localID).OrderByDescending(c => c.FornecedorID).ToList();

                    foreach (var fornecedor in lstFornecedoresLocal)
                    {
                        administradorPedidoRepository.RegistraLogFornecedor(adminModel.ID, pedidoID, null, null, null, AdministradorPedido.Acao.RecusouEntrega, fornecedor.Fornecedor.ID);
                    }

                    var pedido = pedidoRepository.Get(pedidoID);
                    if (pedido != null)
                    {
                        pedido.StatusEntrega = TodosStatusEntrega.PendenteFornecedor;
                    }

                    retorno = "OK";
                }
            }
            catch (Exception)
            {
                retorno = "ERRO";
            }

            return Json(retorno);
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarFornecedor(int id, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);

                if (pedido != null)
                {
                    #region NOVO FORNECEDOR
                    //----------------------------------------------------------------------
                    if (!pedido.FornecedorID.HasValue)
                    {
                        //Registra log do usuário

                        pedido.FornecedorID = fornecedorID;

                        #region INSERE PEDIDO PRODUCAO FORNECEDORES
                        //----------------------------------------------------------------------
                        // *** coloca o pedido diretamente em producao no dash do fornecedor
                        pedido.StatusEntregaID = (int)TodosStatusEntrega.EmProducao;
                        pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                        pedido.DataAtualizacao = DateTime.Now;

                        //pedido.FornecedorEmProcessamento = true;
                        pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;

                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                        // administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega);
                        // administradorPedidoRepository.RegistraLogFornecedor(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega, fornecedorID);

                        pedidoRepository.Save(pedido);
                        //----------------------------------------------------------------------
                        #endregion

                        #region DADOS DE NPS NEW
                        //----------------------------------------------------------------------
                        npsNewREspository.Save(new NPSRating_NEW { PedidoID = pedido.ID, AdministradorID = adminModel.ID, DataCriacao = DateTime.Now, Nota = 0, Guid = pedido.Codigo, FornecedorID = fornecedorID, EmailEnviado = false });
                        //----------------------------------------------------------------------
                        #endregion

                        #region FORNECEDOR PREÇOS
                        //----------------------------------------------------------------------
                        var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();

                        if (fornecedorprecos != null)
                        {
                            foreach (var item in pedido.PedidoItems)
                            {
                                #region FORNECEDOR REPASSE
                                //----------------------------------------------------------------------
                                var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == item.ID).FirstOrDefault();

                                if (repasse == null)
                                {
                                    repasse = new Repasse();
                                    repasse.FornecedorID = pedido.FornecedorID;
                                    repasse.PedidoItemID = item.ID;
                                    repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                                    repasse.AdministradorID = adminModel.ID;
                                    repasse.DataCriacao = DateTime.Now;
                                    repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;

                                    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == item.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == item.ProdutoTamanhoID);
                                    if (fornecedorpreco != null)
                                    {
                                        if (fornecedorpreco.Repasse.HasValue)
                                        {
                                            if (fornecedorpreco.Repasse.Value > 0)
                                            {
                                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                                                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                            }
                                        }
                                    }
                                    repasseRepository.Save(repasse);
                                    //----------------------------------------------------------------------
                                   
                                }

                                #endregion
                            }
                        }
                        //----------------------------------------------------------------------
                        #endregion

                        return Json("OK");
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region ALTERAR FORNECEDOR
                    //----------------------------------------------------------------------
                    else
                    {
                        if (pedido.TotalRepassesPagos == 0)
                        {
                            pedido.FornecedorID = fornecedorID;

                            #region INSERE PEDIDO PRODUCAO FORNECEDORES

                            pedido.StatusEntregaID = (int)TodosStatusEntrega.EmProducao;
                            pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                            pedido.DataAtualizacao = DateTime.Now;

                            pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                            administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);

                            pedidoRepository.Save(pedido);

                            #endregion

                            #region DADOS DE NPS NEW
                            //----------------------------------------------------------------------
                            var NPSNEW = npsNewREspository.GetByExpression(p => p.PedidoID == pedido.ID).FirstOrDefault();

                            if (NPSNEW != null)
                            {
                                NPSNEW.AdministradorID = adminModel.ID;
                                NPSNEW.FornecedorID = fornecedorID;
                            }

                            npsNewREspository.Save(NPSNEW);
                            //----------------------------------------------------------------------
                            #endregion

                            var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();

                            foreach (var item in pedido.PedidoItems)
                            {
                                #region CANCELA REPASSE ANTIGO

                                var repasses = repasseRepository.GetByExpression(c => c.PedidoItemID == item.ID).ToList();
                                foreach (var _repasse in repasses)
                                {
                                    _repasse.Status = Repasse.TodosStatus.Cancelado;
                                    _repasse.Observacao = "\nRepasse cancelado por " + adminModel.CurrentAdministrador.Nome;
                                    repasseRepository.Save(_repasse);
                                }

                                #endregion

                                #region CRIA REPASSE

                                var repasse = new Repasse();
                                repasse.FornecedorID = pedido.FornecedorID;
                                repasse.PedidoItemID = item.ID;
                                repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                                repasse.DataCriacao = DateTime.Now;
                                repasse.Observacao = "\nRepasse alterado para " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                                repasse.AdministradorID = adminModel.ID;

                                #region ADD PRECO PADRAO AO REPASSE

                                if (fornecedorprecos != null)
                                {
                                    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == item.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == item.ProdutoTamanhoID);
                                    if (fornecedorpreco != null)
                                    {
                                        if (fornecedorpreco.Repasse.HasValue)
                                        {
                                            if (fornecedorpreco.Repasse.Value > 0)
                                            {
                                                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                            }
                                        }
                                    }
                                }

                                #endregion

                                repasseRepository.Save(repasse);
                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouRepasse);

                                #endregion
                            }

                            return Json("OK");
                        }
                        else
                        {
                            return Json("O fornecedor não pode ser alterado porque possui um repasses pagos. Cancele o repasse e faça a alteração novamente.");
                        }

                    }
                    //----------------------------------------------------------------------
                    #endregion
                }
                else
                    return Json("O pedido não pode ser alterado. Pedido não localizado");
            }
            catch (Exception ex)
            {
                return Json("Falha na alteração do Pedido!" + ex.Message);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarPushFornecedor(int id, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);

                if (pedido != null)
                {
                    #region INSERE STATUS e LOG
                    //----------------------------------------------------------------------
                    pedido.StatusEntregaID = (int)TodosStatusEntrega.PendenteFornecedor;
                    pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                    pedido.DataAtualizacao = DateTime.Now;

                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);

                    pedidoRepository.Save(pedido);
                    //----------------------------------------------------------------------
                    #endregion

                    #region INSERE PUSH
                    //----------------------------------------------------------------------
                    var fornecedorPush = fornecedorPushRepository.RegistraPush(adminModel.ID, pedido.ID, null, null, fornecedorID);

                    if (fornecedorPush != null)
                    {
                        var _fornecedor = fornecedorRepository.Get(fornecedorID);

                        fornecedorPush.NomeFornecedor = _fornecedor.Nome;
                        fornecedorPush.TelefonePrimario = _fornecedor.TelefonePrimario;
                    }

                    return Json(new { retorno = "OK", FornecedorID = fornecedorPush.IdFornecedor, NomeFornecedor = fornecedorPush.NomeFornecedor, TelefonePrimario = fornecedorPush.TelefonePrimario, DataCriacao = fornecedorPush.DataCriacao.Date.ToString("dd/MM/yyyy") });

                    //----------------------------------------------------------------------
                    #endregion
                }
                else
                {
                    return Json("O pedido não foi encontrado. Tente novamente mais tarde!");
                }
            }
            catch (Exception ex)
            {
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde! " + ex.Message);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarPrejuizo(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.StatusPagamentoID = Convert.ToInt32(Domain.Entities.Pedido.TodosStatusPagamento.Prejuizo);
                pedido.DataAtualizacao = DateTime.Now;
                pedidoRepository.Save(pedido);

                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                {
                    if (pedido.Boleto != null)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                        boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                        if (boleto.RemessaBoletoes.Count == 0)
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                            boleto.Processado = true;
                        }
                        else
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            boleto.Processado = false;
                        }
                        boletoRepository.Save(boleto);
                    }
                }


                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouPrejuizo);

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarSerasa(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.StatusPagamentoID = Convert.ToInt32(Domain.Entities.Pedido.TodosStatusPagamento.Serasa);
                pedido.DataAtualizacao = DateTime.Now;
                pedidoRepository.Save(pedido);

                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                {
                    if (pedido.Boleto != null)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                        boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                        if (boleto.RemessaBoletoes.Count == 0)
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                            boleto.Processado = true;
                        }
                        else
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            boleto.Processado = false;
                        }
                        boletoRepository.Save(boleto);
                    }
                }

                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouPrejuizo);

                ListaNegra listaNegra = new ListaNegra();
                listaNegra.Documento = pedido.Cliente.Documento.Replace("_", "").Trim();
                listaNegra.DataCadastro = DateTime.Now;
                listaNegra.Observacao = "SERASA";

                if(!listaNegraRepository.GetByExpression(r => r.Documento == pedido.Cliente.Documento.Replace("_", "").Trim()).Any())
                {
                    listaNegraRepository.Save(listaNegra);
                }

                

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarPagamento(int id, string forma, string meio, string status, string data, string valor)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var statuspagamentoid = pedido.StatusPagamentoID;
                var datapagamento = new DateTime();
                DateTime.TryParse(data, out datapagamento);

                Decimal valorpagamento = 0;
                Decimal.TryParse(valor, out valorpagamento);

                if (datapagamento != new DateTime() && Convert.ToInt32(status) == (int)Pedido.TodosStatusPagamento.Pago)
                {
                    pedido.DataPagamento = datapagamento;
                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Pago;

                    if (valorpagamento > 0)
                        pedido.ValorPago = valorpagamento;
                    else
                        pedido.ValorPago = pedido.ValorTotal;

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouPago);
                }

                pedido.FormaPagamentoID = Convert.ToInt32(forma);
                pedido.MeioPagamentoID = Convert.ToInt32(meio);
                pedido.StatusPagamentoID = Convert.ToInt32(status);
                pedido.DataAtualizacao = DateTime.Now;

                pedidoRepository.Save(pedido);

                // CANCELA BOLETOS PAGARME
                BoletoPagarMeFactory.CancelarBoletos(pedido);

                // REGISTRA LOG DO USUÁRIO
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFormaPagamento);

                #region CRIAR BOLETO PAGAR ME

                if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe && pedido.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe && pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento)
                {
                    if (Convert.ToInt32(forma) == (int)Pedido.FormasPagamento.BoletoPagarMe && (pedido.Cliente.Exterior || pedido.Cliente.Documento.Length == 0))
                    {
                        return Json("O pedido não pode ser alterado para boleto pois o cliente é do exterior ou não tem o CPF/CNPJ informado.");
                    }

                    var Result = BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, valor);

                    if (string.IsNullOrEmpty(Result.Erro))
                    {
                        var historico = "";
                        if (pedido.StatusPagamentoID != statuspagamentoid)
                            historico += "<b>Status Pagamento: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((Pedido.TodosStatusPagamento)statuspagamentoid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusPagamento) + "</i>";
                        if (historico.Length > 0)
                        {
                            historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;
                            var nota2 = new PedidoNota();
                            nota2.DataCriacao = DateTime.Now;
                            nota2.PedidoID = pedido.ID;
                            nota2.Observacoes = historico;
                            pedidoNotaRepository.Save(nota2);
                        }

                        return Json("OK");
                    }
                    else
                    {
                        return Json(Result.Erro);
                    }
                }
                else
                {
                    return Json("OK");
                }

                #endregion
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Cancelar(int id, int statusCancelamentoID, string observacoes)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento != Pedido.TodosStatusProcessamento.Cancelado)
                {
                    if (pedido.TotalRepassesAberto == 0)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.CancelouPedido);

                        pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                        pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                        pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                        pedido.StatusCancelamentoID = statusCancelamentoID;
                        pedido.DataProcessamento = DateTime.Now;
                        pedido.DataAtualizacao = DateTime.Now;
                        pedidoRepository.Save(pedido);

                        pedido.EnviarEmailCancelamento();

                        var boleto = pedido.Boleto;
                        if (boleto != null)
                        {
                            boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                            boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                            if (boleto.RemessaBoletoes.Count == 0)
                            {
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                                boleto.Processado = true;
                            }
                            else
                            {
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                boleto.Processado = false;
                            }
                            boletoRepository.Save(boleto);
                        }

                        // CANCELA BOLETOS PAGARME
                        BoletoPagarMeFactory.CancelarBoletosInclusivePagos(pedido);

                        if (!string.IsNullOrWhiteSpace(observacoes))
                        {
                            var nota = new PedidoNota();
                            nota.DataCriacao = DateTime.Now;
                            nota.PedidoID = pedido.ID;
                            nota.Observacoes =
                                "Cancelado por: "
                                + adminModel.Nome
                                + "<br>" + "Motivo: "
                                + Domain.Helpers.EnumHelper.GetDescription((TodosStatusCancelamento)statusCancelamentoID)
                                + "<br>" + observacoes;
                            pedidoNotaRepository.Save(nota);
                        }

                        var Result = new Domain.Service.PedidoService().SendMYMEtrics(pedido, "CANCELAR");

                        return Json("OK");
                    }
                    else
                    {
                        return Json("Este pedido tem repasses pagos ou aguardando pagamento. Você deve cancelar todos antes de concluir o pedido.");
                    }
                }
                else
                {
                    return Json("Este pedido já foi cancelado.");
                }
            }
            else
                return Json("O pedido não pode ser cancelado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Processar(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado)
                {
                    

                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                    pedido.DataProcessamento = DateTime.Now;
                    pedido.DataAtualizacao = DateTime.Now;

                    if(pedido.AdministradorID == 1043)
                        pedido.AdministradorID = adminModel.ID;

                    pedidoRepository.Save(pedido);
                    return Json("OK");
                }
                else
                {
                    return Json("Este pedido já está sendo processado.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }

        //----------------------------------------------------------------------
        public JsonResult RotateFoto(int idItem, string Direction)
        {
            var PedidoItem = pedidoItemRepository.Get(idItem);

            try
            {
                Domain.Core.Funcoes.RotateImage("/content/pedidos/" + PedidoItem.Pedido.Numero + "/" + idItem + ".jpg", Direction);
                return Json("OK");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult Concluir(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando)
                {
                    //if (pedido.StatusEntrega == TodosStatusEntrega.Entregue || pedido.StatusEntrega == TodosStatusEntrega.EntregaNaoRequerida || pedido.StatusEntrega == TodosStatusEntrega.Devolvido) {
                    if (pedido.StatusEntrega == TodosStatusEntrega.Entregue)
                    {
                        if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.Pago || pedido.StatusPagamento == Pedido.TodosStatusPagamento.Estornado)
                        {
                            if (pedido.PedidoItems.Count == pedido.TotalRepassesAberto)
                            {
                                //Registra log do usuário
                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ConcluiuPedido);

                                pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Concluido;
                                pedido.DataProcessamento = DateTime.Now;
                                pedido.DataAtualizacao = DateTime.Now;
                                pedidoRepository.Save(pedido);

                                //pedido.EnviarEmailPesquisa();
                                return Json("OK");
                            }
                            else
                            {
                                return Json("Este pedido não tem todos os repasses criados. Você deve criá-los antes de concluir o pedido.");
                            }
                        }
                        else
                        {
                            return Json("Este pedido tem que ter o status 'Pago' ou 'Estornado' para ser concluído. Altere o status, salve e tente novamente.");
                        }
                    }
                    else
                    {
                        return Json("O pedido tem que ter os status 'Entregue', 'Entrega não Requerida' ou 'Devolvido' para ser concluído. Altere o status, salve e tente novamente.");
                    }
                }
                else
                {
                    return Json("Este pedido precisa estar em processamento para ser concluído.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPesquisa(int id)
        {
            if (!Domain.Core.Configuracoes.ENVIA_TRUSTVOX)
            {
                var pedido = pedidoRepository.Get(id);
                if (pedido != null)
                {
                    pedido.EnviarEmailPesquisa();
                    return Json("OK");
                }
                else
                    return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
            else
                return Json("A pesquisa não pode ser enviada. Ela é feita automaticamente pelo trustvox");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarBoleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.EnviarBoleto();
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailBoleto);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarBoletoPagarMe(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.EnviarPedidoEmailBoleto();
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailBoleto);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarNFe(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailNFe);
                pedido.EnviarNFe();
                pedido.DataEmailNFe = DateTime.Now;
                pedidoRepository.Save(pedido);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult CancelaNFe(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                NFE ResultConsultar = null;

                try
                {
                    NFEFactory.Cancelar(pedido.NrReferenciaNF, OrigemPedido.Cliente);
                    ResultConsultar = NFEFactory.Consultar(pedido.NrReferenciaNF, NFE.TiposConsulta.cancelamento,OrigemPedido.Cliente, id);

                    if (ResultConsultar != null)
                    {
                        if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.erro_autorizacao)
                        {
                            return Json("Erro no Cancelamento. Msg: " + ResultConsultar.mensagem_sefaz + " - Cod: " + ResultConsultar.status_sefaz);
                        }
                        else if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.cancelado)
                        {
                            pedido.NFeLink = null;
                            pedido.NFeData = null;
                            pedido.NFeNumero = null;
                            pedido.NFeValor = 0;
                            pedido.NrReferenciaNF = null;

                            pedidoRepository.Save(pedido);

                            pedido.EnviarCancelamentoNFe();

                            administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.CancelouNFE);
                            administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailAutomaticoCancelamentoNFe);

                            return Json("OK");

                        }
                        else
                        {
                            return Json("NF não cancelada - Motivo: Erro Não Tratado." + ResultConsultar.statusNF);
                        }
                    }
                    else
                    {
                        return Json("NF não cancelada - Motivo: Erro Não Tratado" + ResultConsultar.statusNF);
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }
            }
            else
                return Json("NF não cancelada - Motivo: Pedido nao encontrato");
        }
        //----------------------------------------------------------------------
        public JsonResult RegistraCliqueImpressaoBoleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ImprimiuBoleto);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailEntrega(int id, string local, string data, string pessoa, string recebido, string parentesco, string complemento)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var dataentrega = new DateTime();
                DateTime.TryParse(data, out dataentrega);
                if (dataentrega != new DateTime())
                {
                    //var enviapesquisa = pedido.DataEntrega != dataentrega && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";

                    pedido.LocalEntrega = local;
                    pedido.DataEntrega = dataentrega;
                    pedido.PessoaHomenageada = pessoa;
                    pedido.RecebidoPor = recebido;
                    pedido.Parentesco = parentesco;
                    pedido.ComplementoLocalEntrega = complemento;
                    pedido.DataEmailEntrega = DateTime.Now;
                    pedido.StatusEntrega = TodosStatusEntrega.Entregue;
                    pedidoRepository.Save(pedido);

                    pedido.EnviarEmailEntregaNovo();

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                    return Json("OK");
                }
                else
                    return Json("Data de entrega inválida!");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailFornecedor(int id, string local, string _pessoaHomenageada, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);
                if (pedido != null)
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailFornecedor);

                    pedido.LocalEntrega = local;
                    if (string.IsNullOrEmpty(pedido.PessoaHomenageada))
                    {
                        pedido.PessoaHomenageada = "NÃO INFORMADO";
                    }
                    else
                        pedido.PessoaHomenageada = pedido.PessoaHomenageada;
                    pedido.FornecedorID = fornecedorID;
                    pedidoRepository.Save(pedido);
                    if (!String.IsNullOrEmpty(pedido.Fornecedor.EmailContato))
                    {
                        if (pedido.EnviarEmailFornecedor())
                            return Json("OK");
                        else
                            return Json("Não foi possível enviar o e-mail no momento. Tente novamente mais tarde.");
                    }
                    else
                    {
                        return Json("O fornecedor não tem um e-mail cadastrado de contato. Atualize e tente novamente.");
                    }
                }
                else
                    return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
            catch (Exception)
            {
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPedido(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ReenviouEmailPedido);

                if (!String.IsNullOrEmpty(pedido.Cliente.Email))
                {
                    pedido.EnviarPedidoEmailNovo();

                    if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                    {
                        pedido.EnviarPedidoEmailBoleto();
                    }

                    return Json("OK");
                }
                else
                {
                    return Json("O cliente não tem um e-mail cadastrado ou válido. Atualize e tente novamente.");
                }

            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public string CriarNumero()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                if (!new Pedido().NumeroExiste(numero.ToString()))
                {
                    return numero.ToString();
                }
            } while (true);
        }
       
        #endregion
    }
    //==========================================================================
}

