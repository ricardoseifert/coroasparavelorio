﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class EmpresaController : CrudController<Empresa>
    {
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<EmpresaProdutoTamanho_X_Colaborador> empresaProdutoTamanho_X_ColaboradorRepository;
        private IPersistentRepository<EmpresaFollowUp> empresaFollowRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<EmpresaFrase> empresaFraseRepository;
        private IPersistentRepository<EmpresaProdutoTamanho> produtosEmpresaRepository;
        private IPersistentRepository<Produto> produtosRepository;
        private IPersistentRepository<ProspeccaoComplemento> prospeccaoComplementoRepository;
        private AdminModel adminModel;

        public EmpresaController(ObjectContext context)
            : base(context)
        {
            empresaRepository = new PersistentRepository<Empresa>(context);
            empresaProdutoTamanho_X_ColaboradorRepository = new PersistentRepository<EmpresaProdutoTamanho_X_Colaborador>(context);
            empresaFollowRepository = new PersistentRepository<EmpresaFollowUp>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            produtosRepository = new PersistentRepository<Produto>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            empresaFraseRepository = new PersistentRepository<EmpresaFrase>(context);
            produtosEmpresaRepository = new PersistentRepository<EmpresaProdutoTamanho>(context);
            prospeccaoComplementoRepository = new PersistentRepository<ProspeccaoComplemento>(context);
            adminModel = new AdminModel(context);
        }


        public override ActionResult List(params object[] args)
        {
            var resultado = empresaRepository.GetAll().OrderBy(p => p.RazaoSocial).ToList();

            int empresaid = 0;
            Int32.TryParse(Request.QueryString["empresaid"], out empresaid);
            if (empresaid > 0)
                resultado = resultado.Where(c => c.ID == empresaid).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            int atendimento = 0;
            Int32.TryParse(Request.QueryString["atendimento"], out atendimento);
            if (atendimento > 0)
                resultado = resultado.Where(c => c.AtendimentoID == atendimento).ToList();

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => d.DataCriacao >= datainicial) > 0).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => d.DataCriacao <= datafinal) > 0).ToList();
            }

            var razaosocial = Request.QueryString["razaosocial"];
            if (!String.IsNullOrEmpty(razaosocial))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(razaosocial.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.NomeFantasia.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(razaosocial.ToLower()))).ToList();

            var cnpj = Request.QueryString["cnpj"];
            if (!String.IsNullOrEmpty(cnpj))
                resultado = resultado.Where(c => Domain.Core.Funcoes.Formata_CNPJ(c.CNPJ).Contains(Domain.Core.Funcoes.Formata_CNPJ(cnpj))).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Colaboradors.Count(n => Domain.Core.Funcoes.AcertaAcentos(n.Nome).Contains(Domain.Core.Funcoes.AcertaAcentos(nome))) > 0).ToList();

            var email = Request.QueryString["email"];
            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Colaboradors.Count(n => n.Email.ToLower().Contains(email.ToLower())) > 0).ToList();

            var grupo = Request.QueryString["grupo"];
            if (!String.IsNullOrEmpty(grupo))
                resultado = resultado.Where(c => !String.IsNullOrEmpty(c.GrupoEconomico) && c.GrupoEconomico.ToLower().Contains(grupo.ToLower())).ToList();

            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            ViewBag.Admin = adminModel.CurrentAdministrador;

            return View(resultado);
        }

        public ActionResult Produtos(int empresaID){
            var resultado = produtosEmpresaRepository.GetByExpression(c => c.EmpresaID == empresaID).OrderByDescending(p => p.Valor).ToList();
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1){
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.IdEmpresa = empresaID;
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Empresa = empresaRepository.Get(empresaID).RazaoSocial;
            return View(resultado);
        }
        
        [HttpPost]
        public ActionResult AplicarAcao(List<int> idsProdutos, string acao,int idEmpresa){
            if (idsProdutos.Any()) {
                List<EmpresaProdutoTamanho> lstProdutos = produtosEmpresaRepository.GetByExpression(p => idsProdutos.Contains(p.ID)).ToList();

                switch (acao) {
                    case "ativar":
                        foreach (var produto in lstProdutos){
                            produto.Disponivel = true;
                            produtosEmpresaRepository.Save(produto);
                        }
                    break;
                    case "desativar":
                        foreach (var produto in lstProdutos) {
                            produto.Disponivel = false;
                            produtosEmpresaRepository.Save(produto);
                        }
                    break;
                    case "excluir":
                    foreach (var produto in lstProdutos) {
                        produtosEmpresaRepository.Delete(produto);
                    }
                    break;
                }
            }
            return Json(new{ok = true,idEmpresa}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            Empresa empresa = null;
            ViewBag.Cidades = new List<Cidade>();
            if (id.HasValue)
            {
                empresa = empresaRepository.Get(id.Value);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == empresa.EstadoID).OrderBy(e => e.Nome);
            }
            else if(!String.IsNullOrEmpty(Request.QueryString["cnpj"]))
            {
                var cnpj = Request.QueryString["cnpj"].Replace("/", "").Replace(".", "").Replace("-", "").Trim();
                empresa = empresaRepository.GetByExpression(c => c.CNPJ.Replace("/", "").Replace(".", "").Replace("-", "").Trim() == cnpj).FirstOrDefault();
                if (empresa != null)
                {
                    ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == empresa.EstadoID).OrderBy(e => e.Nome);
                }
                else
                {
                    empresa = new Empresa();
                    empresa.CNPJ = cnpj;
                } 
            }
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            return View(empresa);
        }

        [HttpPost]
        public JsonResult AtualizarAtendimento(int id, int atendimento)
        {
            if (adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Socios || adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.LacosCorporativos || adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Coordenadores)
            {
                var empresa = empresaRepository.Get(id);
                if (empresa != null)
                {
                    empresa.AtendimentoID = atendimento;
                    empresaRepository.Save(empresa);
                    return Json("OK");
                }
                else
                    return Json("Não foi possível gravar, tente novamente.");
            }
            else
            {
                return Json("Somente usuários sócios, laços corporativos ou coordenadores podem alterar o atendimento.");
            }
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Empresa empresa)
        {
            ModelState.Remove("Logo");
            if (empresa.ID == 0)
            {
                empresa.Codigo = Guid.NewGuid();
                empresa.Logo = " ";
                empresa.DataCadastro = DateTime.Now;
            }
            empresa.CNPJ = empresa.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            empresa.ProblemaEmail = false;

            if(empresa.CondicaoFaturamentoID == 99)
            {
                empresa.FaturamentoInstantaneo = true;
                empresa.CondicaoFaturamentoID = null;
            }
            else
            {
                empresa.FaturamentoInstantaneo = false;
            }

            empresaRepository.Save(empresa);

            var cliente = clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == empresa.CNPJ).FirstOrDefault();
            if (cliente != null)
            {
                cliente.LacosDataMigracao = DateTime.Now;
                clienteRepository.Save(cliente);

                foreach (var complemento in cliente.ProspeccaoComplementoes.ToList())
                {
                    complemento.EmpresaID = empresa.ID;
                    prospeccaoComplementoRepository.Save(complemento);
                }
            }

            var logo = Request.Files["File"];
            if (logo.ContentLength > 0)
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/empresas/" + empresa.ID));
                if (!dir.Exists)
                {
                    dir.Create();
                }

                //try
                //{

                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(logo.FileName);
                //FOTO
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(logo, Server.MapPath("/content/empresas/" + empresa.ID + "/" + nomefoto), new ImageResizer.ResizeSettings(
                                        "width=100&height=60&crop=auto;format=png;mode=pad;scale=canvas"));

                i.CreateParentDirectory = true;
                i.Build();

                empresa.Logo = nomefoto;
                empresaRepository.Save(empresa);

                //}
                //catch { }
            }
            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

        [HttpGet]
        public ActionResult DuplicarProduto(int empresaID)
        {
            var empresa = empresaRepository.Get(empresaID);
            ViewBag.Empresas = empresaRepository.GetAll().OrderBy(p => p.RazaoSocial);
            return View(empresa);
        }

        [ValidateInput(false)]
        public ActionResult DuplicarProduto(int id, int EmpresaID)
        {
            var empresaorigem = empresaRepository.Get(id);
            var empresadestino = empresaRepository.Get(EmpresaID);
            foreach (var produto in empresaorigem.EmpresaProdutoTamanhoes.ToList())
            { 
                    var produtotamanho = empresadestino.EmpresaProdutoTamanhoes.Where(c => c.EmpresaID == empresadestino.ID && c.ProdutoTamanhoID == produto.ProdutoTamanhoID).FirstOrDefault();
                    if (produtotamanho == null)
                        produtotamanho = new EmpresaProdutoTamanho();
                    produtotamanho.ProdutoTamanhoID = produto.ProdutoTamanhoID;
                    produtotamanho.Valor = produto.Valor;
                    produtotamanho.EmpresaID = empresadestino.ID;
                    produtotamanho.Disponivel = produto.Disponivel;
                    produtosEmpresaRepository.Save(produtotamanho); 
            }

            return RedirectToAction("List", new { empresaID = EmpresaID, cod = "SaveSucess", msg = "Produtos copiados com sucesso!" });
        }

        [HttpGet]
        public ActionResult EditProduto(int empresaID, int? id)
        {
            EmpresaProdutoTamanho produto = null;

            if (id.HasValue)
            {
                produto = produtosEmpresaRepository.Get(id.Value);

                #region COLABORADORES SELECIONADOS

                var colaboradoresSeleionados = produto.EmpresaProdutoTamanho_X_Colaborador.Select(u => new SelectListItem
                {
                    Text = u.Colaborador.Nome + " | " + u.Colaborador.Tipo,
                    Value = u.Colaborador.ID.ToString()
                });

                ViewBag.colaboradoresSeleionados = colaboradoresSeleionados;

                #endregion

            }
            else
            {
                ViewBag.colaboradoresSeleionados = new List<SelectListItem>() { };
            }

            ViewBag.Produtos = produtosRepository.GetAll().OrderBy(p => p.Nome);

            #region TODOS OS COLABORADORES

            var colaboradores = empresaRepository.Get(empresaID).Colaboradors.Select(u => new SelectListItem
            {
                Text = u.Nome + " | " + u.Tipo,
                Value = u.ID.ToString()
            });

            ViewBag.Colaboradores = colaboradores;

            #endregion

           


            return View(produto);
        }

        [ValidateInput(false)]
        public ActionResult EditProduto(EmpresaProdutoTamanho produto)
        {
            produtosEmpresaRepository.Save(produto);

            foreach (var item in produto.EmpresaProdutoTamanho_X_Colaborador.ToList())
                empresaProdutoTamanho_X_ColaboradorRepository.Delete(item);

            if (Request.Form["box2View"] != null)
            {
                foreach (var id in Request.Form["box2View"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        empresaProdutoTamanho_X_ColaboradorRepository.Save(new EmpresaProdutoTamanho_X_Colaborador
                        {
                            ColaboradorID = _id, 
                            IDEmpresaProdutoTamanho = produto.ID
                        });
                    }
                }
            }

            return RedirectToAction("Produtos", new { empresaID = produto.EmpresaID, cod = "SaveSucess", msg = "Produto salvo com sucesso!" });
        }

        [ValidateInput(false)]
        public ActionResult DeleteProduto(int empresaID, int id)
        {
            var produto = produtosEmpresaRepository.Get(id);

            try
            {
                empresaProdutoTamanho_X_ColaboradorRepository.Delete(produto.EmpresaProdutoTamanho_X_Colaborador.FirstOrDefault());
            }
            catch{                }
            

            produtosEmpresaRepository.Delete(produto);
            return RedirectToAction("Produtos", new { empresaID = empresaID, cod = "SaveSucess", msg = "Produto removido com sucesso!" });
        }

        public ActionResult Frases(int empresaID)
        {
            var empresa = empresaRepository.Get(empresaID);
            if (empresa != null)
            {
                var resultado = empresa.EmpresaFrases.ToList();
                //PAGINAÇÃO
                var paginaAtual = 0;
                Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
                if (paginaAtual == 0)
                    paginaAtual = 1;

                var totalItens = resultado.Count;
                var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
                if (totalPaginas > 1)
                {
                    resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
                }
                ViewBag.PaginaAtual = paginaAtual;
                ViewBag.TotalItens = totalItens;
                ViewBag.TotalPaginas = totalPaginas;
                ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

                ViewBag.Empresa = empresa;
                return View(resultado.OrderBy(c => c.Frase));
            }
            else
                return RedirectToAction("List");
        }

        public ActionResult TelaGuerra(int empresaID)
        {
            var empresa = empresaRepository.Get(empresaID);
            if (empresa != null)
            {
                var grupo = empresaRepository.GetByExpression(c => c.GrupoEconomico.Length > 0 && c.GrupoEconomico == empresa.GrupoEconomico).OrderBy(c => c.RazaoSocial).ToList();
                if (grupo.Count == 0)
                    grupo.Add(empresa);
                ViewBag.GrupoEconomico = grupo;
                return View(empresa);
            }
            else
                return RedirectToAction("List");
        }
        [HttpPost]
        public JsonResult SalvarNotaPlanoAcao(int id, string NotaPlanoAcao)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                empresa.NotaPlanoAcao = NotaPlanoAcao;
                empresaRepository.Save(empresa);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }

        [HttpPost]
        public JsonResult SalvarNotaUltimoContato(int id, string NotaUltimoContato)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                empresa.NotaUltimoContato = NotaUltimoContato;
                empresaRepository.Save(empresa);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }
        [HttpPost]
        public JsonResult SalvarContrato(int id, int Valor)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                empresa.ContratoFormalizado = Valor == 1;
                empresaRepository.Save(empresa);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }

        [HttpGet]
        public ActionResult EditFrase(int empresaID, int? id)
        {
            EmpresaFrase frase = null;
            if (id.HasValue)
            {
                frase = empresaFraseRepository.Get(id.Value);
            }
            return View(frase);
        }

        [ValidateInput(false)]
        public ActionResult EditFrase(EmpresaFrase empresaFrase)
        {
            empresaFraseRepository.Save(empresaFrase);
            return RedirectToAction("Frases", new { empresaID = empresaFrase.EmpresaID, cod = "SaveSucess", msg = "Frase salva com sucesso!" });
        }

        [ValidateInput(false)]
        public ActionResult DeleteFrase(int empresaID, int id)
        {
            var frase = empresaFraseRepository.Get(id);
            empresaFraseRepository.Delete(frase);
            return RedirectToAction("Frases", new { empresaID = empresaID, cod = "SaveSucess", msg = "Frase salva com sucesso!" });
        }

        [HttpPost]
        public JsonResult AdicionarFollow(int id, string data, string obs)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                var follow = new EmpresaFollowUp();
                follow.Observacao = obs;
                follow.EmpresaID = id;
                follow.AtendimentoID = adminModel.ID;
                follow.Data = Convert.ToDateTime(data);
                follow.DataCriacao = DateTime.Now;
                empresaFollowRepository.Save(follow);

                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }

        public ActionResult ErrosEmail(int id)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                return View(empresa.RetornoEmails.ToList());
            }
            else
                return RedirectToAction("List");
        }


        public ActionResult Export()
        {

            var resultado = empresaRepository.GetAll().OrderBy(p => p.RazaoSocial).ToList(); 

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            int atendimento = 0;
            Int32.TryParse(Request.QueryString["atendimento"], out atendimento);
            if (atendimento > 0)
                resultado = resultado.Where(c => c.AtendimentoID == atendimento).ToList();

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => d.DataCriacao >= datainicial) > 0).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => d.DataCriacao <= datafinal) > 0).ToList();
            }

            var razaosocial = Request.QueryString["razaosocial"];
            if (!String.IsNullOrEmpty(razaosocial))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(razaosocial.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.NomeFantasia.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(razaosocial.ToLower()))).ToList();

            var cnpj = Request.QueryString["cnpj"];
            if (!String.IsNullOrEmpty(cnpj))
                resultado = resultado.Where(c => Domain.Core.Funcoes.Formata_CNPJ(c.CNPJ).Contains(Domain.Core.Funcoes.Formata_CNPJ(cnpj))).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.Colaboradors.Count(n => Domain.Core.Funcoes.AcertaAcentos(n.Nome).Contains(Domain.Core.Funcoes.AcertaAcentos(nome))) > 0).ToList();

            var email = Request.QueryString["email"];
            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Colaboradors.Count(n => n.Email.ToLower().Contains(email.ToLower())) > 0).ToList();

            var grupo = Request.QueryString["grupo"];
            if (!String.IsNullOrEmpty(grupo))
                resultado = resultado.Where(c => !String.IsNullOrEmpty(c.GrupoEconomico) && c.GrupoEconomico.ToLower().Contains(grupo.ToLower())).ToList();


            //TODO: Melhorar isso 
            var _result = "<table>";
            _result += "<tr style='height:43px;font-weight:bold;'><th>Grupo Economico</th><th>Razao Social</th><th>CNPJ</th><th>Data Cadastro</th><th>Faturamento Total</th><th>Faturamento Concluido</th><th>Atendimento</th></tr>";
            foreach (var item in resultado)
            {
                string faturamento = "R$ 0,00";
                
                if(datainicial == new DateTime() && datafinal == new DateTime()){
                    faturamento = item.PedidoEmpresas.Where(c => c.Status != Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado).Sum(c => c.Valor).ToString("C2");
                }else if(datainicial != new DateTime() && datafinal == new DateTime()){
                    faturamento = item.PedidoEmpresas.Where(c => c.Status != Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado && c.DataCriacao >= datainicial).Sum(c => c.Valor).ToString("C2");
                }
                else if (datainicial == new DateTime() && datafinal != new DateTime())
                {
                    faturamento = item.PedidoEmpresas.Where(c => c.Status != Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado && c.DataCriacao <= datafinal).Sum(c => c.Valor).ToString("C2");
                }
                else if (datainicial != new DateTime() && datafinal != new DateTime())
                {
                    faturamento = item.PedidoEmpresas.Where(c => c.Status != Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado && c.DataCriacao >= datainicial && c.DataCriacao <= datafinal).Sum(c => c.Valor).ToString("C2");
                }

                string faturamentoconcluido = "R$ 0,00";

                if (datainicial == new DateTime() && datafinal == new DateTime())
                {
                    faturamentoconcluido = item.PedidoEmpresas.Where(c => c.Status == Domain.Entities.PedidoEmpresa.TodosStatus.PedidoConcluido).Sum(c => c.Valor).ToString("C2");
                }
                else if (datainicial != new DateTime() && datafinal == new DateTime())
                {
                    faturamentoconcluido = item.PedidoEmpresas.Where(c => c.Status == Domain.Entities.PedidoEmpresa.TodosStatus.PedidoConcluido && c.DataCriacao >= datainicial).Sum(c => c.Valor).ToString("C2");
                }
                else if (datainicial == new DateTime() && datafinal != new DateTime())
                {
                    faturamentoconcluido = item.PedidoEmpresas.Where(c => c.Status == Domain.Entities.PedidoEmpresa.TodosStatus.PedidoConcluido && c.DataCriacao <= datafinal).Sum(c => c.Valor).ToString("C2");
                }
                else if (datainicial != new DateTime() && datafinal != new DateTime())
                {
                    faturamentoconcluido = item.PedidoEmpresas.Where(c => c.Status == Domain.Entities.PedidoEmpresa.TodosStatus.PedidoConcluido && c.DataCriacao >= datainicial && c.DataCriacao <= datafinal).Sum(c => c.Valor).ToString("C2");
                }
                _result += "<tr><td>" + (!String.IsNullOrEmpty(item.GrupoEconomico)?Domain.Core.Funcoes.HtmlEncode(item.GrupoEconomico):"") + "</td><td>" + Domain.Core.Funcoes.HtmlEncode(item.RazaoSocial) + "</td><td>" + Domain.Core.Funcoes.Formata_CNPJ(item.CNPJ) + "</td><td>" + item.DataCadastro.ToString("dd/MM/yyyy") + "</td><td>" + faturamento + "</td><td>" + faturamentoconcluido + "</td><td>" + (item.AtendimentoID.HasValue ? item.Administrador.Nome : "S/A") + "</td>";

            }
            _result += "</table> ";
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "iso-8859-1";
            Response.BufferOutput = true;
            Response.AddHeader("content-disposition", "attachment; filename = faturamento." + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            Response.Write(_result);
            Response.End();

            return View();
        }
    }
}

