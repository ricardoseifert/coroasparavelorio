﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using System.Linq.Expressions;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using Domain.Repositories;
using Admin.ViewModels;
using Domain.Core;
using Domain.MetodosExtensao;
using MvcExtensions.Security.Filters;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using YamlDotNet.Serialization;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace Admin.Controllers
{
    //===========================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class PedidosEmpresaController : CrudController<Pedido>
    {

        #region Variaveis
        //----------------------------------------------------------------------
        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<Log> logRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<PedidoEmpresaNota> pedidoNotaRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private FornecedorPushRepository fornecedorPushRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        //private IPersistentRepository<EmpresaProdutoRestrito> empresaProdutoRestritoRepository;
        private IPersistentRepository<EmpresaProdutoTamanho> empresaProdutoTamanhoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Local> localREspository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private IPersistentRepository<FornecedorLocal> fornecedorlocalREspository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<StatusCancelamento> statusCancelamentoRepository;
        private CupomRepository cupomRepository;
        private CupomService cupomService;
        private AdminModel adminModel;
        private PedidoService pedidoService;
        
        private IPersistentRepository<ClienteFloricultura> ClienteFloriculturaRepository;
        private IPersistentRepository<ProdutoFloricultura> produtoFloriculturaRepository;
        private IPersistentRepository<PedidoItemFloricultura> PedidoItemFloriculturaRepository;
        private IPersistentRepository<ProdutoTamanhoPrecoFloricultura> ProdutoTamanhoPrecoFloriculturaRepository;
        private IPersistentRepository<PedidoEmpresaNF> pedidoEmpresaNFRepository;
        private IPersistentRepository<Transportador> transportadorRepository;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public PedidosEmpresaController(ObjectContext context) : base(context)
        {
            logRepository = new PersistentRepository<Log>(context);
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            pedidoNotaRepository = new PersistentRepository<PedidoEmpresaNota>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            //empresaProdutoRestritoRepository = new PersistentRepository<EmpresaProdutoRestrito>(context);
            empresaProdutoTamanhoRepository = new PersistentRepository<EmpresaProdutoTamanho>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            fornecedorPushRepository = new FornecedorPushRepository(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            fornecedorlocalREspository = new PersistentRepository<FornecedorLocal>(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            localREspository = new PersistentRepository<Local>(context);
            statusCancelamentoRepository = new PersistentRepository<StatusCancelamento>(context);
            cupomRepository = new CupomRepository(context);
            cupomService = new CupomService(context);
            adminModel = new AdminModel(context);
            pedidoService = new PedidoService(context);
            ClienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
            produtoFloriculturaRepository = new PersistentRepository<ProdutoFloricultura>(context);
            PedidoItemFloriculturaRepository = new PersistentRepository<PedidoItemFloricultura>(context);
            ProdutoTamanhoPrecoFloriculturaRepository = new PersistentRepository<ProdutoTamanhoPrecoFloricultura>(context);
            pedidoEmpresaNFRepository = new PersistentRepository<PedidoEmpresaNF>(context);
            transportadorRepository = new PersistentRepository<Transportador>(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public override ActionResult List(params object[] args)
        {
            List<ListItem> lstGrupo = new List<ListItem>();
            using (COROASEntities con = new COROASEntities())
            {
                lstGrupo = con.Empresas.Where(b => b.GrupoEconomico != "").GroupBy(e => e.GrupoEconomico).Select(p => new ListItem { Text = p.Key.ToUpper().Substring(0, 40) + "...", Value = p.Key.ToUpper() }).Distinct().OrderBy(p => p.Value).ToList();
            }

            lstGrupo.Insert(0, new ListItem { Text = "", Value = "" });

            ViewBag.lstGrupo = lstGrupo;

            return View(ListaPedidosEmpresa());
        }
        //----------------------------------------------------------------------
        public ActionResult Visualizar(int id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                #region PREENCHE FORNECEDOR RECUSOU ENTREGA

                if (pedido.AdministradorPedidoes.Where(r => r.AcaoID == 22).ToList().Count() > 0)
                {
                    int IdFOrnecedorRecusouEntrega = (int)pedido.AdministradorPedidoes.Where(r => r.AcaoID == 22).First().IdFornecedor;
                    ViewBag.FornecedorRecusouEntrega = fornecedorRepository.Get(IdFOrnecedorRecusouEntrega).Nome;
                }

                #endregion

                foreach (var Item in pedido.AdministradorPedidoes.Where(r => r.IdFornecedor != null).ToList())
                {
                    var Result = fornecedorlocalREspository.GetByExpression(r => r.FornecedorID == (int)Item.IdFornecedor).FirstOrDefault();
                    Item.NomeFornecedor = Result.Fornecedor.Nome;
                }

                var ResultLocais = localREspository.GetByExpression(r => r.CidadeID == pedido.CidadeID).OrderBy(r => r.Titulo).ToList().Select(r => new SelectListItem { Value = r.ID.ToString(), Text = r.Titulo + " - " + r.Tipo + " - " + r.Telefone }).ToList();

                ViewBag.Locais = ResultLocais;

                var estados = estadoRepository.GetAll().ToList();
                int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                List<Estado> lstEstados = new List<Estado>();
                foreach (int idEstado in principaisEstados)
                {
                    lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                }
                lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                ViewBag.StatusEntregaPedido = pedido.StatusEntrega;

                ViewBag.Estados = lstEstados;

                ViewBag.FornecedorAutomatico = pedido.FornecedorAutomatico;

                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == pedido.EstadoID).OrderBy(e => e.Nome);
                ViewBag.StatusCancelamento = statusCancelamentoRepository.GetByExpression(s => s.Ativo.HasValue && s.Ativo.Value).OrderBy(s => s.Titulo).ToList();

                //#region RENDERIZAR FORMULARIO DINAMICO

                //var Fromulario = new Domain.Factories.FormularioFactory();
                //ViewBag.Fromulario = Fromulario.RenderizarFormulario(1, id, "CORP");

                //#endregion

                var ListTransportadores = transportadorRepository.GetAll().OrderBy(r => r.Nome).ToList();
                ViewBag.Transportadores = new SelectList(ListTransportadores, "ID", "Nome");

                return View(pedido);
            }
            else
                return RedirectToAction("List");
        }
        //----------------------------------------------------------------------
        public ActionResult PrintEntrega(int id)
        {
            var pedido = pedidoRepository.Get(id);
            ViewBag.pedido = pedido;
            string LocalExt = "";

            if (pedido.LocalID != null)
            {
                int LocalID = (int)pedido.LocalID;
                var Local = localREspository.Get(LocalID);
                LocalExt = Local.Cidade.NomeCompleto;
            }

            foreach (var Nota in pedido.PedidoEmpresaNotas)
            {
                if(Nota.Observacoes.Contains("Telefone de Contato do Homenageado"))
                {
                    ViewBag.TelHomenageado = Nota.Observacoes.Replace("Telefone de Contato do Homenageado : ", "");
                }
            }

            ViewBag.LocalExt = LocalExt;

            return View();
        }
        [HttpGet]
        public void Exportar()
        {
            ExportExcel(ListaPedidosEmpresa());
        }
        //----------------------------------------------------------------------
        public ActionResult Novo()
        {
            var estados = estadoRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
            List<Estado> lstEstados = new List<Estado>();
            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }
            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

            ViewBag.Estados = new SelectList(lstEstados, "ID", "Sigla");
            ViewBag.Empresas = empresaRepository.GetByExpression(r => r.Ativa).OrderBy(c => c.RazaoSocial).ToList();

            var ListTransportadores = transportadorRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Transportadores = new SelectList(ListTransportadores, "ID", "Nome");

            ViewBag.EmpresaID = 0;
            ViewBag.NovoColaborador = false;
            if (Request.QueryString["cnpj"] != null)
            {
                var cnpj = Request.QueryString["cnpj"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                var empresa = empresaRepository.GetByExpression(c => c.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == cnpj).FirstOrDefault();
                if (empresa != null)
                {

                    ViewBag.EmpresaID = empresa.ID;
                    ViewBag.NovoColaborador = empresa.LiberaCadastroColaborador;
                    ViewBag.PedidoEmpresas = empresa.PedidoEmpresas.OrderByDescending(c => c.DataCriacao).Take(5);
                }
            }
            return View();
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetColaboradores(int id = 0, string selected = "")
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                List<SelectListItem> result = new List<SelectListItem>();
                foreach (var colaborador in empresa.Colaboradors.Where(c => c.Removido == false).OrderBy(o => o.PrimeiroNome).ToList())
                {
                    result.Add(new SelectListItem { Text = colaborador.Nome.ToUpper(), Value = colaborador.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(colaborador.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
                }
                return Json(result);
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult AdicionaColaborador(int empresaID, string nome, string login, string email, string telefone, string departamento)
        {
            if (empresaID > 0 && !string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(telefone) && !string.IsNullOrEmpty(departamento))
            {
                var empresa = empresaRepository.Get(empresaID);
                if (empresa != null)
                {
                    if (Domain.Core.Funcoes.ValidaEmail(email))
                    {
                        var colaborador = colaboradorRepository.GetByExpression(c => c.Login == login).FirstOrDefault();
                        if (colaborador == null)
                        {
                            colaborador = new Colaborador();
                            colaborador.Nome = nome;
                            colaborador.Email = email.Trim().ToLower();
                            colaborador.Login = login.Trim();
                            colaborador.Telefone = telefone;
                            colaborador.Departamento = departamento.ToUpper();
                            colaborador.Senha = Domain.Core.Funcoes.CriarSenha(5);
                            colaborador.Tipo = Colaborador.Tipos.Usuario;
                            colaborador.DataCadastro = DateTime.Now;
                            colaborador.EmpresaID = empresaID;
                            colaborador.SenhasAnteriores = "";
                            colaboradorRepository.Save(colaborador);

                            //ENVIA EMAIL DE AVISO  
                            var corpo = string.Format("O seguinte colaborador foi cadastrado para a empresa:\n\nNOME: {0}\n\nE-MAIL: {1}\n\nTELEFONE: {2}\n\nDEPARTAMENTO: {3}", nome, email, telefone, departamento);

                            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Corporativos", Domain.Core.Configuracoes.EMAIL_ATENDIMENTO_LACOS, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[LAÇOS CORPORATIVOS] - Inclusão de novo colaborador para a empresa " + empresa.RazaoSocial, corpo, false);

                            return Json("OK");
                        }
                        else
                            return Json("O login informado já existe!");
                    }
                    else
                        return Json("E-mail inválido!");
                }
                else
                    return Json("Empresa inválida.");
            }
            else
                return Json("Digite todos os dados do colaborador!");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult VerificaLiberaCadastro(int id)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                if (empresa.LiberaCadastroColaborador)
                    return Json("OK");
                else
                    return Json("");
            }
            else
                return Json("");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult IntegrarMandae(string Pedido)
        {
            var ID = int.Parse(Pedido);
            var PedidoEmp = pedidoRepository.Get(ID);

            #region INTEGRACAO MANDAE

            var Mandae = new Domain.Entities.Integracoes.Mandae();
            Mandae.customerId = "5DE46F8F767B7AE92D0D59537A066225";
            Mandae.scheduling = DateTime.Now;

            #region SENDER

            Mandae.sender = new Domain.Entities.Integracoes.Sender()
            {
                fullName = "LAÇOS CORPORATIVOS COMERCIO DE PRESENTES EIRELI - ME",
                address = new Domain.Entities.Integracoes.Address()
                {
                    postalCode = "01537001",
                    street = "Av. Lins de Vasconcelos",
                    number = "1975",
                    neighborhood = "Vila Mariana",
                    city = "São Paulo",
                    state = "SP",
                    country = "BR"
                }
            };

            #endregion

            #region ITENS

            Mandae.items = new List<Domain.Entities.Integracoes.Item>();

            var LstSKUs = new List<Domain.Entities.Integracoes.Sku>();

            LstSKUs.Add(new Domain.Entities.Integracoes.Sku() {
                description = PedidoEmp.ProdutoTamanho.Produto.Nome,
                freight = PedidoEmp.ValorFrete != null ? (double)PedidoEmp.ValorFrete : 0,
                price = (double)PedidoEmp.Valor,
                quantity = 1, 
                skuId = PedidoEmp.ProdutoTamanho.Produto.ID.ToString()
            });

            #region NFE 

            var ChaveNFe = "";
            Domain.Entities.Integracoes.invoice invoice = new Domain.Entities.Integracoes.invoice();

            if (!string.IsNullOrEmpty(PedidoEmp.NFeLinkRemessa))
            {
                ChaveNFe = PedidoEmp.NFeLinkRemessa.Split('/')[7].Replace(".pdf", "");

                invoice = new Domain.Entities.Integracoes.invoice()
                {
                    id = (int)PedidoEmp.NFeNumeroRemessa,
                    type = "NFe",
                    key = ChaveNFe
                };
            }

            #endregion

            Mandae.items.Add(new Domain.Entities.Integracoes.Item()
            {
                skus = LstSKUs, 
                invoice = invoice,
                 recipient = new Domain.Entities.Integracoes.Recipient() {
                     //email = PedidoEmp.Colaborador.Email, 
                     fullName = PedidoEmp.PessoaHomenageada.ToString().ToUpper(), 
                     phone = PedidoEmp.TelefoneContato,
                     address = new Domain.Entities.Integracoes.Address() {
                          postalCode = PedidoEmp.CepEntrega,
                          street = PedidoEmp.LogradouroEntrega,
                          number = PedidoEmp.NrEntrega.ToString(),
                         addressLine2 = PedidoEmp.ComplementoLocalEntrega.ToUpper(),
                        city = PedidoEmp.Cidade.Nome,
                         neighborhood = PedidoEmp.BairroEntrega,
                         state = PedidoEmp.Estado.Sigla,
                          country = "BR"
                     }
                 },
                shippingService = "Frete-Mandae",
                store = PedidoEmp.ID.ToString(),
                partnerItemId = PedidoEmp.ID.ToString(),
                totalValue = (double)PedidoEmp.Valor,
                totalFreight = PedidoEmp.ValorFrete != null ? (double)PedidoEmp.ValorFrete : 0
            });

            #endregion

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", Domain.Core.Configuracoes.TOKEN_MANDAE);

            var content = new StringContent(JsonConvert.SerializeObject(Mandae), Encoding.UTF8, "application/json");
            var result = httpClient.PostAsync(Domain.Core.Configuracoes.ENDPOINT_MANDAE + "orders/add-parcel", content).Result;
            var contents = result.Content.ReadAsStringAsync().Result;

            dynamic data = JObject.Parse(contents);

            long IdReturn = 0;
            string Erro = "";

            try
            {
                IdReturn = (long)data.id.Value;
            }
            catch { }

            try
            {
                if (data.error != null)
                    Erro = contents;
            }
            catch { }

            #endregion

            if (IdReturn > 0) {

                #region ATUALIZAR PEDIDO

                PedidoEmp.CodRastreamentoMandae = (int)data.items[0].trackingId;
                pedidoRepository.Save(PedidoEmp);

                #endregion

                return Json(new { IdDoObjeto = IdReturn, CodigoDeRastreamento = (int)data.items[0].trackingId });
            }
                

            return Json(Erro);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProdutos(int id = 0, string selected = "", int colaboradorID = 0)
        {
            var empresa = empresaRepository.Get(id);

            if (empresa != null)
            {
                List<Produto> produtos = new List<Produto>();
                var result = "";

                #region LER XML LACOS CORP

                string Path = "";
                XmlDocument doc = new XmlDocument();
                Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\corporativo.lacoscorporativos.com.br\\Web.config";

                if (Domain.Core.Configuracoes.HOMOLOGACAO)
                    Path = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName + "\\Site Lacos\\Web.config";

                doc.Load(Path);

                var AtributeValue = doc.SelectSingleNode("//add[@key='ProdutoRestritoCNPJ']").Attributes[1];
                var AtributeValueGrupoEconomico = doc.SelectSingleNode("//add[@key='ProdutoRestritoGrupoEconomico']").Attributes[1];

                List<int> LstIdsProdOutrasEmpresas = new List<int>();
                List<int> LstIdsProdOutrasMinhaEmpresa = new List<int>();
                foreach (var item in AtributeValue.Value.Split('|'))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (empresa.CNPJ != item.Split(':')[0])
                        {
                            LstIdsProdOutrasEmpresas.AddRange(item.Split(':')[1].Split(',').Select(r => int.Parse(r)));
                        }
                        else
                        {
                            LstIdsProdOutrasMinhaEmpresa.AddRange(item.Split(':')[1].Split(',').Select(r => int.Parse(r)));
                        }
                    }
                }
                foreach (var item in AtributeValueGrupoEconomico.Value.Split('|'))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (empresa.GrupoEconomico != item.Split(':')[0])
                        {
                            LstIdsProdOutrasEmpresas.AddRange(item.Split(':')[1].Split(',').Select(r => int.Parse(r)));
                        }
                        else
                        {
                            LstIdsProdOutrasMinhaEmpresa.AddRange(item.Split(':')[1].Split(',').Select(r => int.Parse(r)));
                        }
                    }
                }

                LstIdsProdOutrasEmpresas = LstIdsProdOutrasEmpresas.Distinct().ToList();

                #endregion

                if (empresa.SoExibeProdutosConveniados)
                    produtos = produtoRepository.GetByExpression(c => (LstIdsProdOutrasMinhaEmpresa.Contains(c.ID) || c.ProdutoTamanhoes.Where(x => x.EmpresaProdutoTamanhoes.Where(e => e.EmpresaID == empresa.ID).Any()).Any()) && c.Disponivel).ToList();
                else
                {
                    produtos = produtoRepository.GetByExpression(c => c.Disponivel && c.DisponivelLacos).ToList();

                    produtos = produtos.Where(r => !LstIdsProdOutrasEmpresas.Contains(r.ID)).ToList();
                }

                #region PREENCHE PRODUTOS PERSONALIZADOS

                var produtoPersonalizados = produtoTamanhoRepository.GetByExpression(c => c.Produto.TipoID == (int)Produto.Tipos.Personalizado).ToList();

                if (produtoPersonalizados.Count > 0)
                {
                    result += "<optgroup label=\"Personalizado\">";

                    foreach (var produtoPersonalizado in produtoPersonalizados)
                    {
                        if (produtoPersonalizado != null)
                        {
                            result += "<option value=\"" + produtoPersonalizado.ID + "\" data-valor=\"" + produtoPersonalizado.Preco + "\">" + produtoPersonalizado.Produto.Nome + "</option>";
                        }
                    }
                    result += "</optgroup>";
                }

                #endregion

                #region PRODUTOS RESTRITO A COLABORADORES

                if (colaboradorID > 0)
                {
                    var colaborador = colaboradorRepository.Get(colaboradorID);
                    List<int> lstToRemove = new List<int>();

                    var produtoEmpresa = empresa.EmpresaProdutoTamanhoes.ToList();
                    foreach (var produto in produtoEmpresa)
                    {
                        // se produto for restrito e nao estiver disponivel para colaborador
                        if (produto.RestritoColaboradores && produto.EmpresaProdutoTamanho_X_Colaborador.Where(r => r.ColaboradorID == colaborador.ID).Count() == 0)
                            lstToRemove.Add(produto.ProdutoTamanho.Produto.ID);
                    }

                    if (lstToRemove.Count > 0)
                        produtos.RemoveAll((x) => lstToRemove.Contains(x.ID));

                }

                #endregion


                foreach (var produto in produtos)
                {
                    var produtosTamanhos = empresaProdutoTamanhoRepository.GetByExpression(p => p.EmpresaID == empresa.ID && p.ProdutoTamanho.ProdutoID == produto.ID && p.Disponivel).ToList();

                    result += "<optgroup label=\"" + produto.Nome + "\">";
                    foreach (var item in produto.ProdutoTamanhoes.Where(c => c.Disponivel).OrderByDescending(c => c.Tamanho.Nome))
                    {
                        var produtotamanho = produtosTamanhos.Where(c => c.ProdutoTamanhoID == item.ID).FirstOrDefault();
                        if (produtotamanho != null)
                            result += "<option " + (item.Produto.Tipo == Produto.Tipos.Kitsbebe ? "data-qtd=\"true\"" : "") + "  rel=\"" + item.Produto.Tipo + "\"  value=\"" + item.ID + "\" data-valor=\"" + produtotamanho.Valor.ToString("N2") + "\">" + produto.Nome + " - " + item.Tamanho.Dimensao + " - " + produtotamanho.Valor.ToString("C2") + "</option>";
                        else if (!empresa.SoExibeProdutosConveniados)
                            result += "<option " + (item.Produto.Tipo == Produto.Tipos.Kitsbebe ? "data-qtd=\"true\"" : "") + " rel=\"" + item.Produto.Tipo + "\" value=\"" + item.ID + "\" data-valor=\"" + item.Preco.ToString("N2") + "\">" + produto.Nome + " - " + item.Tamanho.Dimensao + " - " + item.Preco.ToString("C2") + "</option>";
                    }
                    result += "</optgroup>";
                }
                return Json(result);
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        public JsonResult Cancelar(int id, int statusCancelamentoID, string observacoes)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                if (!pedidoEmp.FaturamentoID.HasValue)
                {
                    if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.NovoPedido || pedidoEmp.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise)
                    {
                        if (pedidoEmp.TotalRepassesAberto == 0)
                        {
                            //Registra log do usuário
                            administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.CancelouPedido);

                            pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoCancelado;
                            pedidoEmp.StatusEntrega = TodosStatusEntrega.Cancelado;
                            pedidoEmp.StatusCancelamentoID = statusCancelamentoID;
                            pedidoEmp.DataProcessamento = DateTime.Now;
                            pedidoEmp.DataAtualizacao = DateTime.Now;
                            //pedido.DataRepasse = null;
                            //pedido.ValorRepasse = 0;
                            //pedido.FornecedorID = null;
                            pedidoRepository.Save(pedidoEmp);
                            pedidoEmp.EnviarEmailCancelamento();

                            //if (pedido.Faturamento.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.Faturamento.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                            //{
                            //    var boleto = pedido.Faturamento.Boleto;
                            //    boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                            //    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                            //    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            //    boleto.Processado = false;
                            //    boletoRepository.Save(boleto);
                            //}
                            if (!string.IsNullOrWhiteSpace(observacoes))
                            {
                                var nota = new PedidoEmpresaNota();
                                nota.DataCriacao = DateTime.Now;
                                nota.PedidoEmpresaID = pedidoEmp.ID;
                                nota.Observacoes =
                                    "Cancelado por: "
                                    + adminModel.Nome
                                    + "<br>" + "Motivo: "
                                    + Domain.Helpers.EnumHelper.GetDescription((TodosStatusCancelamento)statusCancelamentoID)
                                    + "<br>" + observacoes;
                                pedidoNotaRepository.Save(nota);
                            }
                          
                            return Json("OK");
                        }
                        else {
                            return Json("Este pedido tem repasses pagos ou aguardando pagamento. Você deve cancelar todos antes de concluir o pedido.");
                        }
                    }
                    else {
                        return Json("Este pedido já foi processado, não pode ser cancelado.");
                    }
                }
                else {
                    return Json("Este pedido já foi faturado, não pode ser cancelado. Remova do faturamento caso este ainda não tenha sido pago.");
                }
            }
            else
                return Json("O pedido não pode ser cancelado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Processar(int id)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.NovoPedido)
                {
                    
                    pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoEmAnalise;
                    pedidoEmp.DataProcessamento = DateTime.Now;
                    pedidoEmp.DataAtualizacao = DateTime.Now;

                    if (pedidoEmp.AdministradorID == 1043)
                        pedidoEmp.AdministradorID = adminModel.ID;

                    pedidoRepository.Save(pedidoEmp);
                    return Json("OK");
                }
                else {
                    return Json("Este pedido já está sendo processado.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult RotateFoto(int idItem, string Direction)
        {
            try
            {
                Domain.Core.Funcoes.RotateImage("/content/pedidos/" + idItem + "/" + idItem + ".jpg", Direction);
                return Json("OK");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public JsonResult Concluir(int id)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise)
                {
                    //if (pedido.StatusEntrega == TodosStatusEntrega.Entregue || pedido.StatusEntrega == TodosStatusEntrega.EntregaNaoRequerida || pedido.StatusEntrega == TodosStatusEntrega.Devolvido) {
                    if (pedidoEmp.StatusEntrega == TodosStatusEntrega.Entregue)
                    {
                        if (pedidoEmp.FaturamentoID.HasValue)
                        {
                            if (pedidoEmp.RepasseAtual.ID > 0)
                            {
                                //Registra log do usuário
                                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ConcluiuPedido);

                                pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoConcluido;
                                pedidoEmp.DataProcessamento = DateTime.Now;
                                pedidoEmp.DataAtualizacao = DateTime.Now;
                                pedidoRepository.Save(pedidoEmp);
                                return Json("OK");
                            }
                            else {
                                return Json("Este pedido não tem todos os repasses criados. Você deve criá-los antes de concluir o pedido.");
                            }
                        }
                        else {
                            return Json("O pedido precisa estar faturado para ser concluído.");
                        }
                    }
                    else {
                        return Json("O pedido tem que ter os status 'Entregue', 'Entrega não Requerida' ou 'Devolvido' para ser concluído. Altere o status, salve e tente novamente.");
                    }
                }
                else {
                    return Json("Este pedido precisa estar em processamento para ser concluído.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarFornecedor(int id, int fornecedorID)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                //NOVO FORNECEDOR
                if (!pedidoEmp.FornecedorID.HasValue)
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                    pedidoEmp.FornecedorID = fornecedorID;
                    pedidoRepository.Save(pedidoEmp);

                    var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();
                    if (fornecedorprecos != null)
                    {
                        var repasse = new Repasse();
                        repasse.FornecedorID = pedidoEmp.FornecedorID;
                        repasse.PedidoEmpresaID = pedidoEmp.ID;
                        repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                        repasse.AdministradorID = adminModel.ID;
                        repasse.DataCriacao = DateTime.Now;
                        repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;

                        var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoEmp.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoEmp.ProdutoTamanhoID);
                        if (fornecedorpreco != null)
                        {
                            if (fornecedorpreco.Repasse.HasValue)
                            {
                                if (fornecedorpreco.Repasse.Value > 0)
                                {
                                    repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                }
                            }
                        }
                        repasseRepository.Save(repasse);

                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                       
                    }
                    return Json("OK");
                }
                //ALTERA FORNECEDOR
                else {
                    if (pedidoEmp.TotalRepassesPagos == 0)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                        pedidoEmp.FornecedorID = fornecedorID;
                        pedidoRepository.Save(pedidoEmp);

                        var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();
                        if (fornecedorprecos != null)
                        {
                            if (pedidoEmp.RepasseAtual.Status == Repasse.TodosStatus.AguardandoPagamento)
                            {
                                var repasseatual = pedidoEmp.RepasseAtual;
                                repasseatual.Status = Repasse.TodosStatus.Cancelado;
                                repasseatual.Observacao = "\nRepasse cancelado por " + adminModel.CurrentAdministrador.Nome;
                                repasseRepository.Save(repasseatual);
                            }

                            var repasse = new Repasse();
                            repasse.FornecedorID = pedidoEmp.FornecedorID;
                            repasse.PedidoEmpresaID = pedidoEmp.ID;
                            repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                            repasse.DataCriacao = DateTime.Now;
                            repasse.Observacao = "\nRepasse alterado para " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                            repasse.AdministradorID = adminModel.ID;

                            var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoEmp.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoEmp.ProdutoTamanhoID);
                            if (fornecedorpreco != null)
                            {
                                if (fornecedorpreco.Repasse.HasValue)
                                {
                                    if (fornecedorpreco.Repasse.Value > 0)
                                    {
                                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouRepasse);
                                        repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                    }
                                }
                            }

                            repasseRepository.Save(repasse);

                        }
                        return Json("OK");
                    }
                    else {
                        return Json("O fornecedor não pode ser alterado porque possui um repasse pago. Cancele o repasse e faça a alteração novamente.");
                    }

                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarRepasse(int id, int itemID, decimal valor, decimal? valorFrete)
        {
            var pedidoEmp = pedidoRepository.Get(itemID);
            var repasse = repasseRepository.Get(id);
            if (repasse != null)
            {
                if (repasse.Status == Repasse.TodosStatus.AguardandoPagamento)
                {
                    repasse.Observacao = "\nRepasse alterado de " + repasse.ValorRepasse.ToString("C2") + " para " + valor.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                    repasse.ValorRepasse = valor;

                    if (valorFrete == null)
                        valorFrete = 0;

                    repasse.ValorFrete = valorFrete;

                    repasse.ValorTotal = valor + valorFrete;

                    repasseRepository.Save(repasse);

                    administradorPedidoRepository.Registra(adminModel.ID, null, itemID, null, null, AdministradorPedido.Acao.AlterouRepasse);

                    return Json(new { retorno = "OK" });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "O valor do repasse só pode ser alterado se estiver aguardando pagamento." });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult CriarRepasse(int itemID, decimal valor)
        {
            var pedidoEmp = pedidoRepository.Get(itemID);
            var repasse = repasseRepository.GetByExpression(c => c.PedidoEmpresaID == itemID).Where(p => p.StatusID != 3).FirstOrDefault();

            if (repasse == null)
            {
                repasse = new Repasse();
                repasse.FornecedorID = pedidoEmp.FornecedorID;
                repasse.PedidoEmpresaID = pedidoEmp.ID;
                repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                repasse.AdministradorID = adminModel.ID;
                repasse.ValorRepasse = valor;
                repasse.DataCriacao = DateTime.Now;
                repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                repasseRepository.Save(repasse);

                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                return Json(new { retorno = "OK" });

            }
            else
                return Json(new { retorno = "ERRO" });

        }
        //----------------------------------------------------------------------
        public JsonResult CancelarLeilaoFornecedor(int pedidoEmpresaID, int localID)
        {
            var retorno = "";
            try
            {
                var lstFornecedoresLocal = fornecedorlocalREspository.GetByExpression(c => c.LocalID == localID).OrderByDescending(c => c.FornecedorID).ToList();

                foreach (var fornecedor in lstFornecedoresLocal)
                {
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmpresaID, null, null, AdministradorPedido.Acao.RecusouEntrega);
                }

                retorno = "OK";
            }
            catch (Exception)
            {
                retorno = "ERRO";
            }

            return Json(retorno);
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailEntrega(int id, string local, string data, string pessoa, string recebido, string parentesco, string complemento)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                var dataentrega = new DateTime();
                DateTime.TryParse(data, out dataentrega);
                if (dataentrega != new DateTime())
                {
                    pedidoEmp.LocalEntrega = local;
                    pedidoEmp.DataEntrega = dataentrega;
                    pedidoEmp.PessoaHomenageada = pessoa;
                    pedidoEmp.RecebidoPor = recebido;
                    pedidoEmp.Parentesco = parentesco;

                    if(!string.IsNullOrEmpty(complemento))
                        pedidoEmp.ComplementoLocalEntrega = complemento.ToString().ToUpper();

                    pedidoEmp.DataEmailEntrega = DateTime.Now;
                    pedidoEmp.StatusEntrega = TodosStatusEntrega.Entregue;
                    pedidoRepository.Save(pedidoEmp);

                    pedidoEmp.EnviarEmailEntrega();

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                    return Json("OK");
                }
                else
                    return Json("Data de entrega inválida!");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPedido(int id)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ReenviouEmailPedido);

                if (!String.IsNullOrEmpty(pedidoEmp.Colaborador.Email))
                {
                    pedidoEmp.EnviarPedidoEmailSolicitante();
                    return Json("OK");
                }
                else {
                    return Json("O cliente não tem um e-mail cadastrado ou válido. Atualize e tente novamente.");
                }

            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            try
            {
                var id = Convert.ToInt32(form["ID"]);
                var nota = new PedidoEmpresaNota();
                var nota2 = new PedidoEmpresaNota();
                var pedidoEmp = pedidoRepository.Get(id);
                var statusentregaid = 0;

                #region Dados do Pedido
                //----------------------------------------------------------------------
                if (pedidoEmp != null)
                {
                    var datarepasseold = pedidoEmp.DataRepasse;
                    statusentregaid = pedidoEmp.StatusEntregaID;
                    pedidoEmp.StatusEntregaID = Convert.ToInt32(form["StatusEntregaID"]);
                    pedidoEmp.PessoaHomenageada = form["PessoaHomenageada"];
                    pedidoEmp.Parentesco = form["Parentesco"];
                    pedidoEmp.ParentescoRecebidoPor = form["ParentescoRecebidoPor"];
                    pedidoEmp.NomeFuncionario = form["NomeFuncionario"];
                    pedidoEmp.RecebidoPor = form["RecebidoPor"];
                    pedidoEmp.TelefoneContato = form["TelefoneContato"];

                    pedidoEmp.OrdemDeCompra = form["OrdemDeCompra"];
                    pedidoEmp.TextoPadraoNF = form["TextoPadraoNF"];

                    if (!string.IsNullOrEmpty(form["LogradouroEntrega"]))
                        pedidoEmp.LogradouroEntrega = form["LogradouroEntrega"].ToUpper();

                    if (!string.IsNullOrEmpty(form["BairroEntrega"]))
                        pedidoEmp.BairroEntrega = form["BairroEntrega"].ToUpper();

                    if (!string.IsNullOrEmpty(form["CepEntrega"]))
                        pedidoEmp.CepEntrega = form["CepEntrega"].ToUpper();

                    try
                    {
                        if (!string.IsNullOrEmpty(form["NrEntrega"]))
                            pedidoEmp.NrEntrega = int.Parse(form["NrEntrega"]);

                    }
                    catch { }
                    
                    int? _localEntregaId = null;

                    if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                        _localEntregaId = int.Parse(form["LocalID"]);

                    if (_localEntregaId == 0)
                        _localEntregaId = null;

                    if (pedidoEmp.LocalID != _localEntregaId)
                    {
                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
                    }

                    pedidoEmp.LocalID = _localEntregaId;

                    if (!string.IsNullOrEmpty(form["DataEntrega"]))
                    {
                        try { pedidoEmp.DataEntrega = Convert.ToDateTime(form["DataEntrega"]); }
                        catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    }

                    if (!string.IsNullOrEmpty(form["DataSolicitada"]))
                    {
                        try
                        {
                            pedidoEmp.DataSolicitada = Convert.ToDateTime(form["DataSolicitada"]);
                        }
                        catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    }

                    if (!string.IsNullOrEmpty(form["EstadoID"]))
                        pedidoEmp.EstadoID = Convert.ToInt32(form["EstadoID"]);

                    if (!string.IsNullOrEmpty(form["NfeVolumeRemessa"]))
                        pedidoEmp.NfeVolumeRemessa = Convert.ToInt32(form["NfeVolumeRemessa"]);

                    if (!string.IsNullOrEmpty(form["CidadeID"]))
                        pedidoEmp.CidadeID = Convert.ToInt32(form["CidadeID"]);


                    var _ComplementoLocalEntrega = "";
                    if (!string.IsNullOrEmpty(form["ComplementoLocalEntrega"]))
                        _ComplementoLocalEntrega = form["ComplementoLocalEntrega"].ToString().ToUpper();

                    pedidoEmp.ComplementoLocalEntrega = _ComplementoLocalEntrega;
                    pedidoEmp.Observacoes = form["Observacoes"];
                    pedidoEmp.DataAtualizacao = DateTime.Now;
                    pedidoEmp.Mensagem = form["Mensagem"];
                    //----------------------------------------------------------------------
                    #endregion

                    #region Foto Item
                    //----------------------------------------------------------------------
                    if (Request.Files.Count > 0)
                    {
                        var foto = Request.Files["File"];

                        if (foto != null && foto.ContentLength > 0)
                        {
                            #region UPLOAD FOTO ENTREGA

                            DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedidoEmp.ID));
                            if (!dir.Exists)
                            {
                                dir.Create();
                            }

                            var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);

                            ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedidoEmp.ID + "/" + pedidoEmp.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                    "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=canvas"));

                            i.CreateParentDirectory = true;
                            i.Build();

                            pedidoEmp.StatusFotoID = 3;

                            #endregion
                        }
                        else
                        {
                            #region STATUS DA FOTO

                            if (!string.IsNullOrEmpty(form["StatusFotoID_" + pedidoEmp.ID]))
                            {
                                // GRAVA HISTORICO DE STATUS DE FOTO
                                if (int.Parse(form["StatusFotoID_" + pedidoEmp.ID]) == 3)
                                {
                                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ValidouFoto);
                                    pedidoEmp.StatusFotoID = int.Parse(form["StatusFotoID_" + pedidoEmp.ID]);
                                }

                                if (int.Parse(form["StatusFotoID_" + pedidoEmp.ID]) == 1)
                                {
                                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ReprovouFoto);
                                    pedidoEmp.StatusFotoID = int.Parse(form["StatusFotoID_" + pedidoEmp.ID]);

                                    // EXCLUI FOTO DO SERVIDOR
                                    var PathFoto = "E:\\WEB\\cdn.coroasparavelorio.com.br\\Pedidos\\" + pedidoEmp.ID + "\\" + pedidoEmp.ID + ".jpg";
                                    //var PathFoto = Server.MapPath("/content/pedidos/" + pedido.Numero + "/" + item.ID + ".jpg");
                                    System.IO.File.Delete(PathFoto);
                                }
                            }

                            #endregion
                        }

                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region Historico Status Entrega
                    //----------------------------------------------------------------------
                    var historico = "";
                    if (pedidoEmp.StatusEntregaID != statusentregaid)
                        historico += "<b>Status Entrega: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((TodosStatusEntrega)statusentregaid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedidoEmp.StatusEntrega) + "</i>";
                    if (historico.Length > 0)
                    {
                        historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;

                        nota2.DataCriacao = DateTime.Now;
                        nota2.PedidoEmpresaID = pedidoEmp.ID;
                        nota2.Observacoes = historico;
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region Notas
                    //----------------------------------------------------------------------
                    if (form["ObservacaoNota"].Length > 0)
                    {
                        nota.DataCriacao = DateTime.Now;
                        nota.PedidoEmpresaID = pedidoEmp.ID;
                        nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region VALOR NF SUBSTITUTIVO

                    if (!string.IsNullOrEmpty(form["NFeValorRemessaSubstitutivo"]))
                    {
                        if(decimal.Parse(form["NFeValorRemessaSubstitutivo"]) != pedidoEmp.Valor)
                            pedidoEmp.NFeValorRemessaSubstitutivo = decimal.Parse(form["NFeValorRemessaSubstitutivo"]);
                    }

                    #endregion

                    #region ADMINISTRADOR ID

                    if (pedidoEmp.AdministradorID == 1043)
                    {
                        pedidoEmp.AdministradorID = adminModel.ID;
                    }

                    #endregion

                    #region FRETE

                    if (!string.IsNullOrEmpty(form["ValorFrete"]))
                    {
                        var Frete = decimal.Parse(form["ValorFrete"]);

                        if (pedidoEmp.ValorFrete == 0 && Frete > 0)
                        {
                            pedidoEmp.Valor = pedidoEmp.Valor + Frete;
                        }

                        if (pedidoEmp.ValorFrete > 0 && Frete == 0)
                        {
                            pedidoEmp.Valor = pedidoEmp.Valor - (decimal)pedidoEmp.ValorFrete;
                        }

                        pedidoEmp.ValorFrete = Frete;
                    }

                    if (!string.IsNullOrEmpty(form["TransportadorID"]))
                    {
                        var TransportadorID = int.Parse(form["TransportadorID"]);
                        pedidoEmp.TransportadorID = TransportadorID;
                    }
                    else
                    {
                        pedidoEmp.TransportadorID = null;
                    }

                    if (!string.IsNullOrEmpty(form["TipoFreteID"]))
                    {
                        var TipoFreteID = int.Parse(form["TipoFreteID"]);
                        pedidoEmp.TipoFreteID = TipoFreteID;
                    }
                    else
                    {
                        pedidoEmp.TipoFreteID = null;
                    }


                    pedidoRepository.Save(pedidoEmp);

                    #endregion

                    #region SALVA DADOS PEDIDO
                    //----------------------------------------------------------------------
                    try
                    {
                        if (historico.Length > 0)
                            pedidoNotaRepository.Save(nota2);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }

                    try
                    {
                        if (form["ObservacaoNota"].Length > 0)
                            pedidoNotaRepository.Save(nota);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }

                    try
                    {
                        pedidoRepository.Save(pedidoEmp);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    //----------------------------------------------------------------------
                    #endregion

                    return RedirectToAction("Visualizar", new { id = pedidoEmp.ID, cod = "SaveSucess" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message });
            }
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            var EmpresaID = Convert.ToInt32(form["EmpresaID"]);
            var produtotamanhoid = Convert.ToInt32(form["EmpresaProdutoTamanhoID"]);
            var ColaboradorID = Convert.ToInt32(form["ColaboradorID"]);
            var codigo = form["Cupom"];
            var empresa = empresaRepository.Get(EmpresaID);
            var desconto = Convert.ToDecimal(form["ValorDesconto"]);
            var valortotal = Convert.ToDecimal(form["ValorTotal"]);
            var frete = Convert.ToDecimal(form["ValorFrete"]);
            var descricaoproduto = form["DescricaoProduto"];

            var OrdemDeCompra = form["OrdemDeCompra"];
            var TextoPadraoNF = form["TextoPadraoNF"];


            decimal valor = 0;
            decimal valorept = 0;
            decimal valorpt = 0;
            int? empresaprodutotamanhoid = null;
            var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produtotamanhoid && c.EmpresaID == EmpresaID).FirstOrDefault();
            var produtotamanho = produtoTamanhoRepository.Get(produtotamanhoid);

            if (empresaprodutoTamanho != null) {
                valorept = empresaprodutoTamanho.Valor;
                empresaprodutotamanhoid = empresaprodutoTamanho.ID;
            }
            else if (produtotamanho != null)
            {
                valorpt = produtotamanho.Preco;
                produtotamanhoid = produtotamanho.ID;
            }

            if (valorept > 0)
                valor = valorept;
            else
                valor = valorpt;

            var valororiginal = valor;
            var cupom = cupomRepository.GetByCodigo(codigo);
            int? cupomID = null;
            if (cupom != null)
            {
                if (cupomService.PodeUtilizar(empresa, cupom))
                {
                    cupomID = cupom.ID;
                    if (cupom.Tipo == Cupom.Tipos.Valor)
                    {
                        valor = valor - cupom.Valor;
                    }
                    else if (cupom.Tipo == Cupom.Tipos.Porcentagem)
                    {
                        valor = valor * (1 - cupom.Valor / 100);
                    }
                    if (valor < 0)
                        valor = 0;
                }
            }
            else {
                valor = valororiginal - desconto;
            }

            if (valortotal > 0 && valortotal != valor)
            {
                valor = valortotal;
            }

            var dataHora = form["DataEntrega"] + " " + form["hora"];
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada))
            {
                return Content("Data e/ou Hora invalida(s)");
            }

            dynamic horario_opt = null;

            if (form["horario"] != null && form["horario"] != "Outro")
            {
                horario_opt = "Horário de Entrega: " + form["horario"] + "\n\n";
            }

            int? _localEntregaId = null;
            if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                _localEntregaId = int.Parse(form["LocalID"]);

            if (_localEntregaId == 0)
                _localEntregaId = null;

            var _ComplementoLocalEntrega = "";
            if (!string.IsNullOrEmpty(form["ComplementoLocalEntrega"]))
                _ComplementoLocalEntrega = form["ComplementoLocalEntrega"].ToString().ToUpper();

            var _LogradouroEntrega = "";
            if (!string.IsNullOrEmpty(form["LogradouroEntrega"]))
                _LogradouroEntrega = form["LogradouroEntrega"].ToUpper();

            var _BairroEntrega = "";
            if (!string.IsNullOrEmpty(form["BairroEntrega"]))
                _BairroEntrega = form["BairroEntrega"].ToUpper();

            var _CepEntrega = "";
            if (!string.IsNullOrEmpty(form["CepEntrega"]))
                _CepEntrega = form["CepEntrega"].ToUpper();

            int? _NrEntrega = null;
            try
            {
                if (!string.IsNullOrEmpty(form["NrEntrega"]))
                    _NrEntrega = int.Parse(form["NrEntrega"]);
            }
            catch { }

            int _QTD = 1;
            try
            {
                if (!string.IsNullOrEmpty(form["QTD"]))
                    _QTD = int.Parse(form["QTD"]);
            }
            catch {  }

            if (_QTD == 0)
                _QTD = 1;

            var pedidoEmp = new PedidoEmpresa()
            {
                CidadeID = Convert.ToInt32(form["CidadeEntregaID"]),
                EmpresaProdutoTamanhoID = empresaprodutotamanhoid,
                ProdutoTamanhoID = produtotamanhoid,
                ColaboradorID = ColaboradorID,
                EmpresaID = EmpresaID,
                Codigo = Guid.NewGuid(),
                Mensagem = form["Frase"].ToUpper(),
                Departamento = form["Departamento"],
                Origem = "ADMIN",
                OrigemSite = "LAÇOS CORPORATIVOS",
                OrigemForma = form["OrigemForma"].ToUpper(),
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                EstadoID = Convert.ToInt32(form["EstadoEntregaID"]),
                LocalEntrega = form["LocalID"],
                ComplementoLocalEntrega = _ComplementoLocalEntrega,
                Observacoes = horario_opt + "",
                Parentesco = form["Parentesco"],
                NomeFuncionario = form["NomeFuncionario"],
                PessoaHomenageada = form["PessoaHomenageada"],
                Status = PedidoEmpresa.TodosStatus.NovoPedido,
                StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                TelefoneContato = form["TelefoneContato"],
                CupomID = cupomID,
                Valor = valor,
                ValorDesconto = desconto,
                SubTotal = valororiginal,
                QualidadeStatusContatoID = (int)PedidoEmpresa.TodosStatusContato.NI,
                QualidadeStatusContatoNaoID = (int)PedidoEmpresa.TodosStatusContatoNao.NI,
                DescricaoProduto = descricaoproduto,
                LocalID = _localEntregaId,
                TextoPadraoNF = TextoPadraoNF,
                OrdemDeCompra = OrdemDeCompra,
                ValorFrete = frete,
                StatusFotoID = 1,
                AdministradorID = adminModel.ID, 
                LogradouroEntrega = _LogradouroEntrega,
                CepEntrega = _CepEntrega,
                NrEntrega = _NrEntrega, 
                BairroEntrega = _BairroEntrega, 
                QTD = _QTD
            };
            pedidoRepository.Save(pedidoEmp);            

            if (pedidoEmp.LocalID != _localEntregaId)
            {
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
            }

            if (pedidoEmp.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
            {
                pedidoEmp.LocalID = null;
                pedidoRepository.Save(pedidoEmp);
            }

            try
            {
                pedidoEmp.EnviarPedidoEmail();
            }
            catch {

                logRepository.Save(new Log
                {
                    Pedido = pedidoEmp.Numero,
                    Data = DateTime.Now,
                    Var1 = "ERRO: ", 
                    Var2 = "Erro ao enviar o email do pedido."
                });
            }

            if (!String.IsNullOrEmpty(form["ObservacaoNota"]))
            {
                var nota = new PedidoEmpresaNota();
                nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                nota.DataCriacao = DateTime.Now;
                nota.PedidoEmpresaID = pedidoEmp.ID;
                pedidoNotaRepository.Save(nota);
            }

            if (cupomID.HasValue)
            {
                cupomService.Utilizar(cupom.Codigo);

                //CRIA UMA NOTA
                var pedidonota = new PedidoEmpresaNota();
                pedidonota.PedidoEmpresaID = pedidoEmp.ID;
                pedidonota.DataCriacao = DateTime.Now;
                pedidonota.Observacoes = "Empresa utilizou o cupom " + cupom.Codigo + " que garantiu " + (cupom.Tipo == Cupom.Tipos.Valor ? cupom.Valor.ToString("C2") : cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                pedidoNotaRepository.Save(pedidonota);
            }
            

            #region PRODUTOS FATURAMENTO NFE

            if (!string.IsNullOrEmpty(Request.Form["ValoresProdutoFaturamento"]))
            {
                var StrProdutosFaturamento = Request.Form["ValoresProdutoFaturamento"];

                foreach (var Item in StrProdutosFaturamento.Split('|'))
                {
                    var CodProdutoFaturamento = int.Parse(Item.Split(',')[0]);
                    var QTD = int.Parse(Item.Split(',')[1]);
                    var ValorUni = Item.Split(',')[2];

                    PedidoEmpresaNF novoPedidoEmpresaNF = new PedidoEmpresaNF();
                    novoPedidoEmpresaNF.PedidoEmpresaID = pedidoEmp.ID;
                    novoPedidoEmpresaNF.ProdutoFaturamentoID = CodProdutoFaturamento;
                    novoPedidoEmpresaNF.Qtd = QTD;
                    novoPedidoEmpresaNF.ValorUnitario = decimal.Parse(ValorUni.Replace(".", ","));

                    pedidoEmpresaNFRepository.Save(novoPedidoEmpresaNF);
                }
            }

            #endregion

            #region FATURAR PEDIDO 

            string FaturamentoStr = "";

            if (pedidoEmp.Empresa.FaturamentoInstantaneo && pedidoEmp.Empresa.OrdemDeCompra == false)
            {
                var fatura = new Domain.Entities.Faturamento().Faturar(pedidoEmp);

                pedidoEmp.FaturamentoID = fatura.ID;
                pedidoRepository.Save(pedidoEmp);

                if (fatura.StatusNF == Faturamento.TodosStatusNFs.Processando)
                    FaturamentoStr = "&StatusNF=Processando";

                if (fatura.StatusNF == Faturamento.TodosStatusNFs.NaoEmitida)
                    FaturamentoStr = "&StatusNF=NaoEmitida";
            }

            #endregion

            #region TRANSPORTADOR

            if (!string.IsNullOrEmpty(Request.Form["TransportadorID"]))
            {
                var TransportadorID = int.Parse(Request.Form["TransportadorID"]);
                pedidoEmp.TransportadorID = TransportadorID;
                pedidoRepository.Save(pedidoEmp);
            }

            #endregion

            #region TIPO DO FRETE

            if (!string.IsNullOrEmpty(Request.Form["TipoFreteID"]))
            {
                var TipoFreteID = int.Parse(Request.Form["TipoFreteID"]);
                pedidoEmp.TipoFreteID = TipoFreteID;
                pedidoRepository.Save(pedidoEmp);
            }
            else
            {
                pedidoEmp.TipoFreteID = (int)PedidoEmpresa.TodosTiposDeFrete.SemFrete;
                pedidoRepository.Save(pedidoEmp);
            }

            #endregion

            #region TELEFONE DO HOMENAGEADO

            if (!string.IsNullOrEmpty(Request.Form["TelefoneContatoHomenageado"]))
            {
                var nota = new PedidoEmpresaNota();

                nota.DataCriacao = DateTime.Now;
                nota.PedidoEmpresaID = pedidoEmp.ID;
                nota.Observacoes = "Telefone de Contato do Homenageado: " + Request.Form["TelefoneContatoHomenageado"];

                pedidoNotaRepository.Save(nota);
            }

            #endregion

            Response.Redirect("/PedidosEmpresa/List?datainicial=" + DateTime.Now.ToShortDateString() + "&cod=SaveSucess&msg=" + "O pedido " + pedidoEmp.ID + " foi criado com sucesso!" + FaturamentoStr);

            return RedirectToAction("List");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarProduto(int id, int produtoTamanhoID, decimal valorProduto = 0, string descricao = "")
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.Status == PedidoEmpresa.TodosStatus.NovoPedido || pedido.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise)
                {
                    decimal valor = 0;
                    decimal valorept = 0;
                    decimal valorpt = 0;
                    int? empresaprodutotamanhoid = null;
                    var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produtoTamanhoID && c.EmpresaID == pedido.EmpresaID).FirstOrDefault();
                    var produtotamanho = produtoTamanhoRepository.Get(produtoTamanhoID);

                    if (empresaprodutoTamanho != null)
                    {
                        valorept = empresaprodutoTamanho.Valor;
                        empresaprodutotamanhoid = empresaprodutoTamanho.ID;
                    }
                    if (produtotamanho != null)
                    {
                        valorpt = produtotamanho.Preco;
                        produtoTamanhoID = produtotamanho.ID;
                    }
                    if (valorept > 0)
                        valor = valorept;
                    else
                        valor = valorpt;

                    var valororiginal = valor;
                    if (pedido.Cupom != null)
                    {
                        if (pedido.Cupom.Tipo == Cupom.Tipos.Valor)
                        {
                            valor = valor - pedido.Cupom.Valor;
                        }
                        else if (pedido.Cupom.Tipo == Cupom.Tipos.Porcentagem)
                        {
                            valor = valor * (1 - pedido.Cupom.Valor / 100);
                        }
                        if (valor < 0)
                            valor = 0;
                        pedido.ValorDesconto = valororiginal - valor;


                        //CRIA UMA NOTA
                        var pedidonota = new PedidoEmpresaNota();
                        pedidonota.PedidoEmpresaID = pedido.ID;
                        pedidonota.DataCriacao = DateTime.Now;
                        pedidonota.Observacoes = "Empresa utilizou o cupom " + pedido.Cupom.Codigo + " que garantiu " + (pedido.Cupom.Tipo == Cupom.Tipos.Valor ? pedido.Cupom.Valor.ToString("C2") : pedido.Cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                        pedidoNotaRepository.Save(pedidonota);
                    }
                    else {
                        valor = valororiginal - pedido.ValorDesconto;
                    }

                    if (valorProduto > 0 && valorProduto != valor)
                    {
                        valor = valorProduto;
                        pedido.ValorDesconto = 0;
                    }

                    pedido.EmpresaProdutoTamanhoID = empresaprodutotamanhoid;
                    pedido.ProdutoTamanhoID = produtoTamanhoID;
                    pedido.Valor = valor;
                    pedido.DescricaoProduto = descricao;
                    pedido.SubTotal = valororiginal;
                    pedidoRepository.Save(pedido);
                    return Json(new { retorno = "OK" });
                }
                else {
                    return Json(new { retorno = "ERRO", mensagem = "Pedido precisa estar com status de novo para alterar o produto" });
                }
            }
            else {
                return Json(new { retorno = "ERRO", mensagem = "Pedido inválido" });
            }
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarProdutoNF(int PedidoEmpresaID, int ProdutoFaturamentoID, int QTD, string ValorUnitario)
        {
            try
            {
                PedidoEmpresaNF novoPedidoEmpresaNF = new PedidoEmpresaNF();
                novoPedidoEmpresaNF.PedidoEmpresaID = PedidoEmpresaID;
                novoPedidoEmpresaNF.ProdutoFaturamentoID = ProdutoFaturamentoID;
                novoPedidoEmpresaNF.Qtd = QTD;
                novoPedidoEmpresaNF.ValorUnitario = decimal.Parse(ValorUnitario);

                pedidoEmpresaNFRepository.Save(novoPedidoEmpresaNF);

                return Json(new { retorno = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { retorno = "ERRO", mensagem = ex.Message });
                throw;
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFrases(int id)
        {
            var empresa = empresaRepository.Get(id);

            List<SelectListItem> result = new List<SelectListItem>();
            if (empresa != null)
            {
                foreach (EmpresaFrase frase in empresa.EmpresaFrases)
                {
                    result.Add(new SelectListItem { Text = frase.Frase, Value = frase.Frase });
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult ExcluirPedidoEmpresaNF(int id)
        {
            try
            {
                var PedidoEmpresaNF = pedidoEmpresaNFRepository.Get(id);
                pedidoEmpresaNFRepository.Delete(PedidoEmpresaNF);

                return Json("OK");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        [HttpPost]
        public JsonResult ValidaCupom(int empresaID, string codigo)
        {
            var empresa = empresaRepository.Get(empresaID);
            if (empresa == null)
            {
                return Json(new { mensagem = "Empresa inválida", retorno = "ERRO" });
            }
            var cupom = cupomRepository.GetByCodigo(codigo);
            if (cupom == null)
            {
                return Json(new { mensagem = "Cupom inválido", retorno = "ERRO" });
            }
            else if (!cupomService.PodeUtilizar(empresa, cupom))
            {
                return Json(new { mensagem = "Esta empresa já utilizou esse cupom", retorno = "ERRO" });
            }
            else {
                var retorno = "Este cupom garante ";
                if (cupom.Tipo == Cupom.Tipos.Porcentagem)
                    retorno += cupom.Valor + "% de desconto.";
                else
                    retorno += cupom.Valor.ToString("C2") + " de desconto.";
                return Json(new { mensagem = retorno, retorno = "OK", valor = cupom.Valor.ToString("N2").Replace(".", "").Replace(",", "."), tipo = cupom.TipoID });
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailFornecedor(int id, string local, string pessoa, int fornecedorID)
        {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.EnviouEmailFornecedor);

                pedidoEmp.LocalEntrega = local;
                pedidoEmp.PessoaHomenageada = pessoa;
                pedidoEmp.FornecedorID = fornecedorID;
                pedidoRepository.Save(pedidoEmp);
                if (!String.IsNullOrEmpty(pedidoEmp.Fornecedor.EmailContato))
                {
                    if (pedidoEmp.EnviarEmailFornecedor())
                        return Json("OK");
                    else
                        return Json("Não foi possível enviar o e-mail no momento. Tente novamente mais tarde.");
                }
                else {
                    return Json("O fornecedor não tem um e-mail cadastrado de contato. Atualize e tente novamente.");
                }
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult FornecedorInfo(int fornecedorID, int PedidoID)
        {
            var fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null)
            {
                string ValorMinimoDeCoroa = "R$ 0,00";
                if (fornecedor.ValorMinimoDeCoroa != null)
                    ValorMinimoDeCoroa = fornecedor.ValorMinimoDeCoroa.Value.ToString("C2");

                var LstRepasses = repasseRepository.GetByExpression(r => r.FornecedorID == fornecedor.ID && r.PedidoEmpresaID != null && r.PedidoEmpresa.StatusCancelamentoID == null).OrderByDescending(r => r.PedidoEmpresa.DataCriacao).Take(10).ToList().Select(r => new
                {
                    Produto = r.PedidoEmpresa.ProdutoTamanho.Produto.Nome + " - " + r.PedidoEmpresa.ProdutoTamanho.Tamanho.NomeCompleto,
                    Valor = r.ValorRepasse,
                    PedidoID = r.PedidoEmpresa.ID,
                    DataCriacao = r.PedidoEmpresa.DataCriacao.ToShortDateString()
                });

                var Produto = pedidoRepository.Get(PedidoID);
                var LstProdutos = new
                {
                    Produto = Produto.ProdutoTamanho.Produto.Nome + " - " + Produto.ProdutoTamanho.Tamanho.NomeCompleto,
                    RepasseIdeal = Produto.ValorIdealRepasse
                };

                if (LstRepasses.Count() > 0)
                {
                    return Json(new
                    {
                        retorno = "OK",
                        caracteristicas = !string.IsNullOrEmpty(fornecedor.Caracteristicas) ? fornecedor.Caracteristicas : "N/A",
                        AvgRepasse = LstRepasses.Average(r => r.Valor),
                        NrRepassesFornecedor = fornecedor.Repasses.Count,
                        ValorMinimoDeCoroa,
                        LstProdutos,
                        LstRepasses,
                        frete = fornecedor.ValorFrete > 0 ? fornecedor.ValorFrete.ToString("C2") : "N/A",
                        horario = !string.IsNullOrEmpty(fornecedor.HorarioFuncionamento) && fornecedor.HorarioFuncionamento != "" ? fornecedor.HorarioFuncionamento : "N/A",
                        problemas =  !string.IsNullOrEmpty(fornecedor.ResponsavelProblemas) ? fornecedor.ResponsavelProblemas : "N/A",
                        nome = "" + fornecedor.Nome + "",
                        telefones = "" + fornecedor.TelefonePrimario + "<br>" + fornecedor.TelefoneSecundario + "<br>" + fornecedor.TelefonesAdicionais + "",
                        contato = "" + fornecedor.PessoaContato + "",
                        obs = "" + fornecedor.Observacao + "",
                        entrega24h = "" + (fornecedor.Entrega24h ? "SIM" : "NÃO") + ""
                    });
                }
                else
                {
                    return Json(new
                    {
                        retorno = "OK",
                        caracteristicas = fornecedor.Caracteristicas,
                        ValorMinimoDeCoroa,
                        frete = fornecedor.ValorFrete.ToString("C2"),
                        horario = fornecedor.HorarioFuncionamento,
                        problemas = fornecedor.ResponsavelProblemas,
                        nome = "" + fornecedor.Nome + "",
                        telefones = "" + fornecedor.TelefonePrimario + "<br>" + fornecedor.TelefoneSecundario + "<br>" + fornecedor.TelefonesAdicionais + "",
                        contato = "" + fornecedor.PessoaContato + "",
                        obs = "" + fornecedor.Observacao + "",
                        entrega24h = "" + (fornecedor.Entrega24h ? "SIM" : "NÃO") + ""
                    });
                }
                
               
            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult PedidoPorFornecedor(int fornecedorID)
        {
            var pedido = pedidoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).Take(5).OrderByDescending(c => c.DataCriacao);


            if (pedido != null)
            {
                return Json(new { retorno = "OK", pedido });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        #endregion

        #region Metodos
        public string CriarNumero()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);
            } while (true);
        }
        //----------------------------------------------------------------------
        public List<PedidoEmpresa> ListaPedidosEmpresa()
        {
            List<PedidoEmpresa> lstPedidos = new List<PedidoEmpresa>();

            if (!string.IsNullOrWhiteSpace(Request.QueryString["ID"]))
            {
                int id;
                if (int.TryParse(Request.QueryString["ID"], out id))
                {
                    lstPedidos = pedidoRepository.GetByExpression(p => p.ID == id).ToList();
                }
            }
            else {
                Expression<Func<PedidoEmpresa, bool>> exp = p => p.ID > 0;

                #region Data inicial
                if (!string.IsNullOrWhiteSpace(Request.QueryString["datainicial"]))
                {
                    DateTime datainicial = new DateTime();
                    if (DateTime.TryParse(Request.QueryString["datainicial"], out datainicial))
                    {
                        exp = exp.And(p => p.DataCriacao >= datainicial);
                    }
                }
                #endregion

                #region Data final
                if (!string.IsNullOrWhiteSpace(Request.QueryString["datafinal"]))
                {
                    DateTime datafinal = new DateTime();
                    if (DateTime.TryParse(Request.QueryString["datafinal"], out datafinal))
                    {
                        datafinal = new DateTime(datafinal.Year, datafinal.Month, datafinal.Day).AddDays(1).AddSeconds(-1);
                        exp = exp.And(p => p.DataCriacao <= datafinal);
                    }
                }
                #endregion

                #region E-mail
                if (!string.IsNullOrWhiteSpace(Request.QueryString["email"]))
                {
                    string email = Request.QueryString["email"];
                    exp = exp.And(p => p.Colaborador.Email.Contains(email));
                }
                #endregion

                #region Meio Pagamento
                if (!string.IsNullOrWhiteSpace(Request.QueryString["meiopagamento"]))
                {
                    int meiopagamento;
                    if (int.TryParse(Request.QueryString["meiopagamento"], out meiopagamento) && meiopagamento > 0)
                    {
                        exp = exp.And(p => p.FaturamentoID.HasValue && p.Faturamento.MeioPagamentoID == meiopagamento);
                    }
                }
                #endregion

                #region Cliente ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["clienteID"]))
                {
                    int clienteID;
                    if (int.TryParse(Request.QueryString["clienteID"], out clienteID))
                    {
                        exp = exp.And(p => p.EmpresaID == clienteID);
                        Empresa emp = empresaRepository.GetByExpression(e => e.ID == clienteID).FirstOrDefault();
                        if (emp != null)
                        {
                            ViewBag.RazaoSocialEmpresa = emp.RazaoSocial;
                        }
                    }
                }
                #endregion

                #region Cupom ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["cupomID"]))
                {
                    int cupomID;
                    if (int.TryParse(Request.QueryString["cupomID"], out cupomID))
                    {
                        exp = exp.And(p => p.CupomID == cupomID);
                    }
                }
                #endregion

                #region Não Faturado
                if (!string.IsNullOrWhiteSpace(Request.QueryString["naofaturado"]))
                {
                    if (Request.QueryString["naofaturado"] == "on")
                    {
                        exp = exp.And(p => !p.FaturamentoID.HasValue);
                    }
                }
                #endregion

                #region Status
                if (!string.IsNullOrWhiteSpace(Request.QueryString["status"]))
                {
                    int status;
                    if (int.TryParse(Request.QueryString["status"], out status))
                    {
                        exp = exp.And(p => p.StatusID == status);
                    }
                }
                #endregion

                #region GrupoEconomico
                if (!string.IsNullOrWhiteSpace(Request.QueryString["GrupoEconomico"]))
                {
                    string GrupoEconomico = Request.QueryString["GrupoEconomico"];
                    exp = exp.And(p => p.Empresa.GrupoEconomico.ToUpper() == GrupoEconomico);
                }
                #endregion

                #region Status Pagamento
                if (!string.IsNullOrWhiteSpace(Request.QueryString["statuspagamento"]))
                {
                    int statuspagamento;
                    if (int.TryParse(Request.QueryString["statuspagamento"], out statuspagamento))
                    {
                        if(statuspagamento != 7)
                        {
                            exp = exp.And(p => p.FaturamentoID.HasValue && p.Faturamento.StatusPagamentoID == statuspagamento);
                        }
                        else
                        {
                            exp = exp.And(p => p.FaturamentoID == null);
                        }
                    }
                }
                #endregion

                #region Status Entrega
                if (!string.IsNullOrWhiteSpace(Request.QueryString["statusentrega"]))
                {
                    int statusentrega;
                    if (int.TryParse(Request.QueryString["statusentrega"], out statusentrega))
                    {
                        exp = exp.And(p => p.StatusEntregaID == statusentrega);
                    }
                }
                #endregion

                //faz a consulta com a expressão montada acima (exp), com os parametros opcionais
                lstPedidos = pedidoRepository.GetByExpression(exp).ToList();

                #region Razão Social
                var razao = Request.QueryString["razao"];
                if (!string.IsNullOrWhiteSpace(razao))
                {
                    lstPedidos = lstPedidos.Where(c =>
                    Funcoes.AcertaAcentos(c.Empresa.RazaoSocial.ToLower()).Contains(Funcoes.AcertaAcentos(razao.ToLower()))
                    || Funcoes.AcertaAcentos(c.Empresa.NomeFantasia.ToLower()).Contains(Funcoes.AcertaAcentos(razao.ToLower()))).ToList();
                }
                #endregion
            }

            lstPedidos = lstPedidos.OrderByDescending(p => p.DataCriacao).ToList();

            return lstPedidos;
        }
        //----------------------------------------------------------------------
        public void ExportExcel  (List<PedidoEmpresa> LstObjects)
        {
            string Csv = string.Empty;

            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms, Encoding.UTF8);

            string TBL = string.Empty;

            TBL += "<table>";
            TBL += "<tr>";
            TBL += "<td>NR</td>";
            TBL += "<td>PRODUTO</td>";
            TBL += "<td>FATURADO</td>";
            TBL += "<td>ATENDENTE</td>";
            TBL += "<td>DATA CRIAÇÃO</td>";
            TBL += "<td>STATUS</td>";
            TBL += "<td>VALOR</td>";
            TBL += "<td>DATA ENTREGA</td>";
            TBL += "<td>PESSOA HOM</td>";
            TBL += "<td>FAIXA</td>";
            TBL += "<td>FUNCIONÁRIO</td>";
            TBL += "<td>PARENTESCO</td>";
            TBL += "<td>DEPARTAMENTO</td>";
            TBL += "<td>LOCAL ENTR.</td>";
            TBL += "<td>ESTADO</td>";
            TBL += "<td>CIDADE</td>";
            TBL += "<td>OBS</td>";
            TBL += "<td>TEL CONTATO</td>";
            TBL += "<td>NOME DO SOLICITANTE</td>";
            TBL += "<td>CNPJ</td>";
            TBL += "<td>COMPLEMENTO LOCAL DE ENTREGA</td>";

            TBL += "</tr>";

            foreach (var iten in LstObjects)
            {
                string NomeAdm = "";

                if (iten.Administrador != null)
                    NomeAdm = iten.Administrador.Nome;

                string Faturado = string.Empty;

                if (iten.Faturamento == null)
                {
                    Faturado = "NÃO";
                }
                else
                {
                    Faturado = "SIM";
                }

                TBL += "<tr>";
                
                TBL += "<td>" + iten.Numero + "</td>";
                TBL += "<td>" + iten.ProdutoTamanho.Produto.Nome + " " + iten.ProdutoTamanho.Tamanho.Nome + "</td>";
                TBL += "<td>" + Faturado + "</td>";
                TBL += "<td>" + NomeAdm + "</td>";
                TBL += "<td>" + iten.DataCriacao + "</td>";
                TBL += "<td>" + Domain.Helpers.EnumHelper.GetDescription(iten.StatusEntrega) + "</td>";
                TBL += "<td>" + iten.Valor + "</td>";
                TBL += "<td>" + iten.DataEntrega + "</td>";
                TBL += "<td>" + iten.PessoaHomenageada + "</td>";
                TBL += "<td>" + iten.Mensagem + "</td>";
                TBL += "<td>" + iten.NomeFuncionario + "</td>";
                TBL += "<td>" + iten.Parentesco + "</td>";
                TBL += "<td>" + iten.Departamento + "</td>";
                TBL += "<td>" + iten.LocalEntrega.Replace(System.Environment.NewLine, " ") + "</td>";
                TBL += "<td>" + iten.Estado.Nome + "</td>";
                TBL += "<td>" + iten.Cidade.Nome + "</td>";
                TBL += "<td>" + iten.Observacoes.Replace(System.Environment.NewLine, "") + "</td>";
                TBL += "<td>" + iten.TelefoneContato + "</td>";
                TBL += "<td>" + iten.Colaborador.Nome + "</td>";
                TBL += "<td>" + iten.Empresa.CNPJ + "</td>";
                if (!string.IsNullOrWhiteSpace(iten.ComplementoLocalEntrega))
                {
                    TBL += "<td>" + iten.ComplementoLocalEntrega.ToUpper().Trim() + "</td>";
                }
                else {
                    TBL += "<td></td>";
                }
                TBL += "</tr>";
            }

            TBL += "</table>";
            tw.Write(TBL);

            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();


            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";

            Response.AddHeader("content-disposition", "attachment; filename=Export.xls");
            Response.BinaryWrite(bytes);
            Response.End();

        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}
