﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Admin.Controllers
{
    public class ImportaController : Controller
    {
    //    private IPersistentRepository<C_clientes> c_clienteRepository;
    //    private IPersistentRepository<Cliente> clienteRepository;
    //    private IPersistentRepository<Estado> estadoRepository;
        //    private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Repasse> repasseRepository;

        public ImportaController(ObjectContext context)
        {
            //c_clienteRepository = new PersistentRepository<C_clientes>(context);
            //clienteRepository = new PersistentRepository<Cliente>(context);
            //estadoRepository = new PersistentRepository<Estado>(context);
            //cidadeRepository = new PersistentRepository<Cidade>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
        }

        public ContentResult Index()
        {
            Response.Buffer = false;
            var fornecedores = fornecedorRepository.GetAll().ToList();
            foreach (var fornecedor in fornecedores.ToList())
            {
                var outros = fornecedorRepository.GetByExpression(c => c.ID != fornecedor.ID && c.EmailContato == fornecedor.EmailContato).ToList();
                foreach (var _f in outros.ToList())
                {
                    if (_f.Pedidoes.Count == 0 && _f.PedidoEmpresas.Count == 0 && _f.Repasses.Count == 0)
                    {
                        Response.Write("<BR>" + _f.Nome + " - sem pedidos e repasses");
                    }
                    else
                    {
                        Response.Write("<BR>" + _f.Nome + " - Pedidos: <b>" + _f.Pedidoes.Count + "</b> - Pedidos empresa: <b>" + _f.PedidoEmpresas.Count + "</b> - Repasses: <b>" + _f.Repasses.Count + "</b>");
                        //MIGRA PEDIDOS
                        foreach (var i in _f.Pedidoes.ToList())
                        {
                            var pedido = i;
                            pedido.FornecedorID = fornecedor.ID;
                            pedidoRepository.Save(pedido);
                        }

                        //MIGRA PEDIDOS EMPRESA
                        foreach (var i in _f.PedidoEmpresas.ToList())
                        {
                            var pedido = i;
                            pedido.FornecedorID = fornecedor.ID;
                            pedidoEmpresaRepository.Save(pedido);
                        }

                        //MIGRA REPASSES
                        foreach (var i in _f.Repasses.ToList())
                        {
                            var repasse = i;
                            repasse.FornecedorID = fornecedor.ID;
                            repasseRepository.Save(repasse);
                        }
                    }
                   fornecedorRepository.Delete(_f);
                }
            }
            return Content("");
        }

        //public ActionResult Index()
        //{
        //    //Response.Flush();
        //    Response.Buffer = false;
        //    //Response.Charset = encoding.EncodingName;
        //    //Response.ContentType = "application/text";
        //    //Response.ContentEncoding = Encoding.ASCII;

        //    var clientes_old = c_clienteRepository.GetAll().OrderBy(c => c.nome);
        //    var countOK = 0;
        //    var countERRO = 0;

        //    foreach (var clieold in clientes_old)
        //    {
        //        Response.Write(clieold.ID + " - ");

        //        Response.Write(Domain.Core.Funcoes.AcertaTextoUtf8(clieold.nome)); 

        //        if (String.IsNullOrEmpty(clieold.email))
        //        {
        //            countERRO++;
        //            Response.Write(" - NÃO IMPORTADO, NÃO TEM EMAIL");
        //        }
        //        else
        //        {

        //            countOK++;
        //            var tipoid = 1;
        //            var documento = "";
        //            if (clieold.tipo == "2")
        //            {
        //                if(clieold.cpf != null)
        //                documento = clieold.cpf.Replace("-", "").Replace(".", "").Trim();
        //            }
        //            else if (clieold.tipo == "1")
        //            {
        //                if (clieold.cnpj != null)
        //                documento = clieold.cnpj.Replace("-","").Replace(".","").Replace("/","").Trim();
        //                tipoid = 2;
        //            }
        //            var email = clieold.email.ToLower().Trim();
        //            var cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
        //            if (cliente == null)
        //            {
        //                cliente = new Cliente();
        //                cliente.TipoID = tipoid;
        //                if (clieold.nome != null)
        //                    cliente.Nome = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.nome.Trim()).ToUpper();

        //                cliente.RazaoSocial = "";
        //                        cliente.InscricaoEstadual = "ISENTO";
        //                cliente.Documento = documento;
        //                if (cliente.TipoID == 2)
        //                {
        //                    if (clieold.nome != null)
        //                    cliente.RazaoSocial = cliente.Nome;

        //                    if (clieold.inscricao != null)
        //                        cliente.InscricaoEstadual = clieold.inscricao;
        //                }
        //                cliente.Email = email;
        //                cliente.Senha = Domain.Core.Funcoes.CriarSenha(4);

        //                if (clieold.telefone != null)
        //                    cliente.TelefoneContato = clieold.telefone.Trim();
        //                if (clieold.celular != null)
        //                    cliente.CelularContato = clieold.celular.Trim();

        //                if (clieold.pescontato != null)
        //                    cliente.PessoaContato = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.pescontato.Trim()).ToUpper();

        //                if (clieold.endereco != null)
        //                    cliente.Logradouro = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.endereco.ToUpper()).Trim();

        //                if (clieold.numero != null)
        //                    cliente.Numero = clieold.numero.ToUpper().Trim();

        //                if (clieold.complemento != null)
        //                    cliente.Complemento = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.complemento.ToUpper()).Trim();

        //                if (clieold.cep != null)
        //                    cliente.CEP = clieold.cep.ToUpper().Trim();

        //                if (clieold.bairro != null)
        //                    cliente.Bairro = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.bairro).ToUpper().Trim();
                        
        //                if (clieold.como != null)
        //                    cliente.ComoConheceu = Domain.Core.Funcoes.AcertaTextoUtf8(clieold.como).ToUpper().Trim();
        //                cliente.Exterior = false;
        //                cliente.DataCadastro = new DateTime(2013, 05, 01);
        //                var estado = estadoRepository.GetByExpression(c => c.Sigla == clieold.uf.ToUpper().Trim()).FirstOrDefault();
        //                if (estado != null)
        //                    cliente.EstadoID = estado.ID;

        //                var _cidade = Domain.Core.Funcoes.AcertaAcentos(Domain.Core.Funcoes.AcertaTextoUtf8(clieold.cidade)).ToUpper().Trim();
        //                var cidade = cidadeRepository.GetByExpression(c => c.Nome.Trim() == _cidade).FirstOrDefault();
        //                if (cidade != null)
        //                    cliente.CidadeID = cidade.ID;
        //                else
        //                {
        //                    //if (!String.IsNullOrEmpty(clieold.cep))
        //                    //{
        //                    //    var cep = new Domain.Core.WebCEP(clieold.cep.Trim());

        //                    //    Response.Write("  ---- " + cep.Cidade + " ------ ");
        //                    //}
        //                    //else
        //                    //{
        //                        cliente.EmailSecundario = clieold.cidade;
        //                    //}
        //                }

        //                try
        //                {
        //                    clienteRepository.Save(cliente);
        //                }
        //                catch (Exception e)
        //                {
        //                    Response.Write(" - ERRO " + e.ToString());
        //                }
        //                Response.Write(cliente.Nome + " - " + cliente.CidadeID);

        //            }
        //            else
        //            {
        //                if (String.IsNullOrEmpty(cliente.Documento) && !String.IsNullOrEmpty(documento))
        //                {
        //                    cliente.Documento = documento;
        //                    cliente.TipoID = tipoid;
        //                    try
        //                    {
        //                        clienteRepository.Save(cliente);
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        Response.Write(" - ERRO JA EXISTE " + e.ToString());
        //                    }
        //                }
        //                Response.Write(" - JÁ EXISTE");

        //            }


        //        }
        //        Response.Write("<br>");
        //    }
        //    Response.Write("<br>TOTAL OK = " + countOK);
        //    Response.Write("<br>TOTAL ERRO = " + countERRO);
        //    return View();
        //}

    }
}
