﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Floricultura|FloriculturaMisto", "/Home/AccessDenied")]
    public class FunerariaController : CrudController<Funeraria>
    {
        private IPersistentRepository<Funeraria> funerariaRepository;
        private IPersistentRepository<ContatoFunararia> ContatofunerariaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private AdminModel adminModel;


        public FunerariaController(ObjectContext context)
            : base(context)
        {
            adminModel = new AdminModel(context);
            funerariaRepository = new PersistentRepository<Funeraria>(context);
            ContatofunerariaRepository = new PersistentRepository<ContatoFunararia>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            adminModel = new AdminModel(context);
        }


        public ActionResult Caminho(string area, Cliente cliente)
        {
            ViewBag.Area = area;
            ViewBag.Cliente = cliente;
            return View();
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = funerariaRepository.GetAll().OrderBy(p => p.RazaoSocial).ToList();

            int FunerariaID = 0;
            Int32.TryParse(Request.QueryString["FunerariaID"], out FunerariaID);
            if (FunerariaID > 0)
                resultado = resultado.Where(c => c.ID == FunerariaID).ToList();

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            var nome = Request.QueryString["razaosocial"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower()))).ToList();

            var cpf = Request.QueryString["documento"];
            if (!String.IsNullOrEmpty(cpf))
                resultado = resultado.Where(c => Domain.Core.Funcoes.Formata_CNPJ(c.Documento).Contains(Domain.Core.Funcoes.Formata_CNPJ(cpf))).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }

            

            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            return View(resultado);

        }

        [HttpGet]
        public ActionResult Editar(int id)
        {
            Funeraria Funeraria = funerariaRepository.Get(id);

            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == Funeraria.Estado.ID).OrderBy(e => e.Nome);

            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;
            
            return View(Funeraria);
        }

        [HttpGet]
        public ActionResult Novo()
        {
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.Cidades = new List<Cidade>();

            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Editar(Funeraria RequestCliente)
        {
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == RequestCliente.Estado.ID).OrderBy(e => e.Nome);

            if(RequestCliente.Documento != null)
            {
                if (!Domain.Core.Funcoes.Valida_CNPJ(RequestCliente.Documento))
                {
                    ViewBag.Erro = "CNPJ inválido";
                    return View(RequestCliente);
                }
                if (funerariaRepository.GetByExpression(c => c.Documento.Trim().ToLower() == RequestCliente.Documento.Trim().ToLower() && c.ID != RequestCliente.ID).Count() > 0)
                {
                    ViewBag.Erro = "Este CPF/CNPJ já está sendo utilizada por outro cliente";
                    return View(RequestCliente);
                }
                RequestCliente.Documento = RequestCliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            }
           

            Funeraria FunEdit = new Funeraria();
            if (RequestCliente.ID > 0)
            {
                FunEdit = funerariaRepository.Get(RequestCliente.ID);
            }
            else
            {
                FunEdit.DataCadastro = DateTime.Now;
            }

            FunEdit.RazaoSocial = RequestCliente.RazaoSocial.ToUpper();
            FunEdit.EmailContato = RequestCliente.EmailContato;
            FunEdit.Documento = RequestCliente.Documento;
            FunEdit.TelefoneContato = RequestCliente.TelefoneContato;
            FunEdit.CEP = RequestCliente.CEP;

            FunEdit.Estado = estadoRepository.Get(RequestCliente.Estado.ID);
            FunEdit.Cidade = cidadeRepository.Get(RequestCliente.Cidade.ID);

            FunEdit.Logradouro = RequestCliente.Logradouro;
            FunEdit.Numero = RequestCliente.Numero;
            FunEdit.Complemento = RequestCliente.Complemento;
            FunEdit.Bairro = RequestCliente.Bairro;

            FunEdit.DataAtualizacao = DateTime.Now;
            funerariaRepository.Save(FunEdit);

            if(!string.IsNullOrEmpty(Request.Form["redirect"]))
            {
                return RedirectToAction("Novo", "ContatoFuneraria", new { cod = "SaveSucess", funerariaid = FunEdit.ID, redirect = "true" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "SaveSucess", clienteid = RequestCliente.ID });
            }
            
        }

        [HttpPost]
        public JsonResult GetCliente(string documento)
        {
            var cliente = funerariaRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim()).FirstOrDefault();
            if (cliente != null)
                return Json(new { Tipo = "CLIENTE", Nome = cliente.RazaoSocial.ToUpper(), TelefoneContato = cliente.TelefoneContato, EmailContato = cliente.EmailContato, CEP = cliente.CEP, EstadoID = cliente.Estado.ID, CidadeID = cliente.Cidade.ID, Logradouro = cliente.Logradouro, Numero = cliente.Numero, Complemento = cliente.Complemento, Bairro = cliente.Bairro,  RazaoSocial = cliente.RazaoSocial });
            else
                return Json("");
        }

        [HttpGet]
        public ActionResult Deletar(int id)
        {
            var deleteResult = funerariaRepository.Get(id);
            try
            {
                funerariaRepository.Delete(deleteResult);
                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { cod = "DeleteFailure", msg = "Não foi possível excluir o registro. É possível que haja algum pedido relacionado." });
            }

        }

        [HttpPost]
        public JsonResult GetFunerarias(string Nome, string CNPJ, string Email)
        {
            var clientes = funerariaRepository.GetAll().OrderBy(r => r.RazaoSocial).ToList();

            if(!string.IsNullOrEmpty(Nome))
                clientes = clientes.Where(c => c.RazaoSocial.ToLower().Contains(Nome.ToLower())).ToList();

            if (!string.IsNullOrEmpty(CNPJ))
                clientes = clientes.Where(c => c.Documento.ToLower().Contains(CNPJ.ToLower())).ToList();

            if (!string.IsNullOrEmpty(Email))
                clientes = clientes.Where(c => c.EmailContato.ToLower().Contains(Email.ToLower())).ToList();
            
            if(clientes.Count > 0)
            {
                return Json(clientes.Select(i => new { i.EmailContato, i.RazaoSocial, i.ID, i.Documento, i.DocumentoFormatado }).ToList());
            }
            else
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult GetContatosFuneraria(int? IdFuneraria)
        {
            if(IdFuneraria == null)
            {
                return Json("");
            }
            else
            {
                var contatos = funerariaRepository.Get((int)IdFuneraria).ContatoFunararias.OrderBy(r => r.Nome).ToList();

                return Json(contatos.Select(i => new { i.Nome, i.ID }));
            }
           
        }
    }
}
