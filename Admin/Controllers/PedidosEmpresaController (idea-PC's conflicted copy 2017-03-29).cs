﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using System.Linq.Expressions;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using Domain.Repositories;
using Admin.ViewModels;
using Domain.Core;
using Domain.MetodosExtensao;
using MvcExtensions.Security.Filters;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

namespace Admin.Controllers {
    //===========================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro", "/Home/AccessDenied")]
    public class PedidosEmpresaController : CrudController<Pedido> {

        #region Variaveis
        //----------------------------------------------------------------------
        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<PedidoEmpresaNota> pedidoNotaRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private FornecedorPushRepository fornecedorPushRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        private IPersistentRepository<EmpresaProdutoRestrito> empresaProdutoRestritoRepository;
        private IPersistentRepository<EmpresaProdutoTamanho> empresaProdutoTamanhoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Local> localREspository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private IPersistentRepository<FornecedorLocal> fornecedorlocalREspository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<StatusCancelamento> statusCancelamentoRepository;
        private CupomRepository cupomRepository;
        private CupomService cupomService;
        private AdminModel adminModel;
        private PedidoService pedidoService;
        private IPersistentRepository<PedidoFloricultura> pedidoFloriculturaRepository;
        private IPersistentRepository<ClienteFloricultura> ClienteFloriculturaRepository;
        private IPersistentRepository<PedidoFloriculturaOrigem> PedidoFloriculturaOrigemRepository;
        private IPersistentRepository<ProdutoFloricultura> produtoFloriculturaRepository;
        private IPersistentRepository<PedidoItemFloricultura> PedidoItemFloriculturaRepository;
        private IPersistentRepository<ProdutoTamanhoPrecoFloricultura> ProdutoTamanhoPrecoFloriculturaRepository;
        private IPersistentRepository<PedidoFloriculturaNota> pedidoFloriculturaNotaRepository;        
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public PedidosEmpresaController(ObjectContext context): base(context) {
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            pedidoNotaRepository = new PersistentRepository<PedidoEmpresaNota>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            empresaProdutoRestritoRepository = new PersistentRepository<EmpresaProdutoRestrito>(context);
            empresaProdutoTamanhoRepository = new PersistentRepository<EmpresaProdutoTamanho>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            fornecedorPushRepository = new FornecedorPushRepository(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            fornecedorlocalREspository = new PersistentRepository<FornecedorLocal>(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            localREspository = new PersistentRepository<Local>(context);
            statusCancelamentoRepository = new PersistentRepository<StatusCancelamento>(context);
            cupomRepository = new CupomRepository(context);
            cupomService = new CupomService(context);
            adminModel = new AdminModel(context);
            pedidoService = new PedidoService(context);
            pedidoFloriculturaRepository = new PersistentRepository<PedidoFloricultura>(context);
            ClienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
            PedidoFloriculturaOrigemRepository = new PersistentRepository<PedidoFloriculturaOrigem>(context);
            produtoFloriculturaRepository = new PersistentRepository<ProdutoFloricultura>(context);
            PedidoItemFloriculturaRepository = new PersistentRepository<PedidoItemFloricultura>(context);
            ProdutoTamanhoPrecoFloriculturaRepository = new PersistentRepository<ProdutoTamanhoPrecoFloricultura>(context);
            pedidoFloriculturaNotaRepository = new PersistentRepository<PedidoFloriculturaNota>(context);
            pedidoFloriculturaNotaRepository = new PersistentRepository<PedidoFloriculturaNota>(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public override ActionResult List(params object[] args){
            List<ListItem> lstGrupo = new List<ListItem>();
            using (COROASEntities con = new COROASEntities()) {
                lstGrupo = con.Empresas.Where(b => b.GrupoEconomico != "").GroupBy(e =>e.GrupoEconomico).Select(p => new ListItem { Text = p.Key.ToUpper().Substring(0, 40) + "...", Value = p.Key.ToUpper()}).Distinct().OrderBy(p => p.Value).ToList();
            }

            lstGrupo.Insert(0, new ListItem { Text = "", Value = "" });

            ViewBag.lstGrupo = lstGrupo;
            
            return View(ListaPedidosEmpresa());
        }
        //----------------------------------------------------------------------
        public ActionResult Visualizar(int id) {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {

                var fornecedorPush = fornecedorPushRepository.GetByExpression(c => c.PedidoID == pedido.ID).OrderByDescending(c => c.DataCriacao).ToList();

                ViewBag.ListaPedidosPush = fornecedorPush;

                if (fornecedorPush != null && fornecedorPush.Count() > 0)
                {
                    var _fornecedor = fornecedorRepository.Get((int)fornecedorPush.FirstOrDefault().IdFornecedor);

                    fornecedorPush.FirstOrDefault().NomeFornecedor = _fornecedor.Nome;
                    fornecedorPush.FirstOrDefault().TelefonePrimario = _fornecedor.TelefonePrimario;

                    ViewBag.FornecedorPush = fornecedorPush.OrderByDescending(c => c.DataCriacao).FirstOrDefault();
                }

                foreach (var Item in pedido.AdministradorPedido.Where(r => r.IdFornecedor != null).ToList())
                {
                    var Result = fornecedorlocalREspository.GetByExpression(r => r.FornecedorID == (int)Item.IdFornecedor).FirstOrDefault();
                    Item.NomeFornecedor = Result.Fornecedor.Nome;
                }

                var ResultLeilao = pedidoService.PedidoEmLeilao(id, OrigemPedido.Empresa);
                ViewBag.TravaFornecedores = ResultLeilao;

                var ResultLocais = localREspository.GetByExpression(r => r.CidadeID == pedido.CidadeID).OrderBy(r => r.Titulo).ToList().Select(r => new SelectListItem { Value = r.ID.ToString(), Text = r.Titulo + " - " + r.Tipo + " - " + r.Telefone }).ToList();
                //ResultLocais.Insert(0, new SelectListItem { Text = "LOCAL NÃO CADASTRADO", Value = "NULL" });

                ViewBag.Locais = ResultLocais;

                var estados = estadoRepository.GetAll().ToList();
                int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                List<Estado> lstEstados = new List<Estado>();
                foreach (int idEstado in principaisEstados)
                {
                    lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                }
                lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                ViewBag.StatusEntregaPedido = pedido.StatusEntrega;

                ViewBag.Estados = lstEstados;
                
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == pedido.EstadoID).OrderBy(e => e.Nome);
                ViewBag.StatusCancelamento = statusCancelamentoRepository.GetByExpression(s => s.Ativo.HasValue && s.Ativo.Value).OrderBy(s => s.Titulo).ToList();

                return View(pedido);
            }
            else
                return RedirectToAction("List");
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public void Exportar()
        {            
            ExportExcel(ListaPedidosEmpresa());
        }
        //----------------------------------------------------------------------
        public ActionResult Novo() {
            var estados = estadoRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
            List<Estado> lstEstados = new List<Estado>();
            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }
            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

            ViewBag.Estados = new SelectList(lstEstados, "ID", "Sigla");
            ViewBag.Empresas = empresaRepository.GetAll().OrderBy(c => c.RazaoSocial).ToList();
            //ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.EmpresaID = 0;
            ViewBag.NovoColaborador = false;
            if (Request.QueryString["cnpj"] != null) {
                var cnpj = Request.QueryString["cnpj"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                var empresa = empresaRepository.GetByExpression(c => c.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == cnpj).FirstOrDefault();
                if (empresa != null) {

                    ViewBag.EmpresaID = empresa.ID;
                    ViewBag.NovoColaborador = empresa.LiberaCadastroColaborador;
                    ViewBag.PedidoEmpresas = empresa.PedidoEmpresas.OrderByDescending(c=>c.DataCriacao).Take(5);
                }
            }
            return View();
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetColaboradores(int id = 0, string selected = "") {
            var empresa = empresaRepository.Get(id);
            if (empresa != null) {
                List<SelectListItem> result = new List<SelectListItem>();
                foreach (var colaborador in empresa.Colaboradores.Where(c => c.Removido == false).ToList()) {
                    result.Add(new SelectListItem { Text = colaborador.Nome.ToUpper(), Value = colaborador.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(colaborador.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
                }
                return Json(result);
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult AdicionaColaborador(int empresaID, string nome, string login, string email, string telefone, string departamento) {
            if (empresaID > 0 && !string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(telefone) && !string.IsNullOrEmpty(departamento)) {
                var empresa = empresaRepository.Get(empresaID);
                if (empresa != null) {
                    if (Domain.Core.Funcoes.ValidaEmail(email)) {
                        var colaborador = colaboradorRepository.GetByExpression(c => c.Login == login).FirstOrDefault();
                        if (colaborador == null) {
                            colaborador = new Colaborador();
                            colaborador.Nome = nome;
                            colaborador.Email = email.Trim().ToLower();
                            colaborador.Login = login.Trim();
                            colaborador.Telefone = telefone;
                            colaborador.Departamento = departamento.ToUpper();
                            colaborador.Senha = Domain.Core.Funcoes.CriarSenha(5);
                            colaborador.Tipo = Colaborador.Tipos.Usuario;
                            colaborador.DataCadastro = DateTime.Now;
                            colaborador.EmpresaID = empresaID;
                            colaborador.SenhasAnteriores = "";
                            colaboradorRepository.Save(colaborador);

                            //ENVIA EMAIL DE AVISO  
                            var corpo = string.Format("O seguinte colaborador foi cadastrado para a empresa:\n\nNOME: {0}\n\nE-MAIL: {1}\n\nTELEFONE: {2}\n\nDEPARTAMENTO: {3}", nome, email, telefone, departamento);

                            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Corporativos", Domain.Core.Configuracoes.EMAIL_ATENDIMENTO_LACOS, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[LAÇOS CORPORATIVOS] - Inclusão de novo colaborador para a empresa " + empresa.RazaoSocial, corpo, false);

                            return Json("OK");
                        }
                        else
                            return Json("O login informado já existe!");
                    }
                    else
                        return Json("E-mail inválido!");
                }
                else
                    return Json("Empresa inválida.");
            }
            else
                return Json("Digite todos os dados do colaborador!");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult VerificaLiberaCadastro(int id) {
            var empresa = empresaRepository.Get(id);
            if (empresa != null) {
                if (empresa.LiberaCadastroColaborador)
                    return Json("OK");
                else
                    return Json("");
            }
            else
                return Json("");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProdutos(int id = 0, string selected = "") {
            var empresa = empresaRepository.Get(id);
            if (empresa != null) {
                List<Produto> produtos = new List<Produto>();
                var produtoPersonalizado = produtoTamanhoRepository.GetByExpression(c => c.Produto.TipoID == (int)Produto.Tipos.Personalizado).FirstOrDefault();
                if (empresa.SoExibeProdutosConveniados)
                    produtos = empresaProdutoTamanhoRepository.GetByExpression(c => c.EmpresaID == empresa.ID && c.ProdutoTamanho.Disponivel).Select(c => c.ProdutoTamanho.Produto).ToList();
                else
                {
                    produtos = produtoRepository.GetByExpression(c => c.Disponivel).ToList();
                    List<Produto> lstProdutosOutrasEmpresas = empresaProdutoRestritoRepository.GetByExpression(b => true && !(b.GrupoEconomico ?? "").Equals(empresa.GrupoEconomico) && !(b.CNPJ ?? "").Equals(empresa.CNPJ)).Select(d => d.Produto).ToList();
                    produtos = produtos.Except<Produto>(lstProdutosOutrasEmpresas).ToList();
                }
               
                var result = "";
                if (produtoPersonalizado != null) {
                    result += "<optgroup label=\"Personalizado\">";
                    result += "<option value=\"" + produtoPersonalizado.ID + "\" data-valor=\"0,01\">Produto Personalizado</option>";
                    result += "</optgroup>";
                }

                foreach (var produto in produtos) {
                    var produtosTamanhos = empresaProdutoTamanhoRepository.GetByExpression(p => p.EmpresaID == empresa.ID && p.ProdutoTamanho.ProdutoID == produto.ID && p.Disponivel).ToList();

                    result += "<optgroup label=\"" + produto.Nome + "\">";
                    foreach (var item in produto.ProdutoTamanhos.Where(c => c.Disponivel).OrderByDescending(c => c.Tamanho.Nome)) {
                        var produtotamanho = produtosTamanhos.Where(c => c.ProdutoTamanhoID == item.ID).FirstOrDefault();
                        if (produtotamanho != null)
                            result += "<option value=\"" + item.ID + "\" data-valor=\"" + produtotamanho.Valor.ToString("N2") + "\">" + produto.Nome + " - " + item.Tamanho.Dimensao + " - " + produtotamanho.Valor.ToString("C2") + "</option>";
                        else if (!empresa.SoExibeProdutosConveniados)
                            result += "<option value=\"" + item.ID + "\" data-valor=\"" + item.Preco.ToString("N2") + "\">" + produto.Nome + " - " + item.Tamanho.Dimensao + " - " + item.Preco.ToString("C2") + "</option>";
                    }
                    result += "</optgroup>";
                }
                return Json(result);
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        public JsonResult Cancelar(int id, int statusCancelamentoID, string observacoes) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                if (!pedidoEmp.FaturamentoID.HasValue) {
                    if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.NovoPedido || pedidoEmp.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise) {
                        if (pedidoEmp.TotalRepassesAberto == 0) {
                            //Registra log do usuário
                            administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.CancelouPedido);

                            pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoCancelado;
                            pedidoEmp.StatusEntrega = TodosStatusEntrega.Cancelado;
                            pedidoEmp.StatusCancelamentoID = statusCancelamentoID;
                            pedidoEmp.DataProcessamento = DateTime.Now;
                            pedidoEmp.DataAtualizacao = DateTime.Now;
                            //pedido.DataRepasse = null;
                            //pedido.ValorRepasse = 0;
                            //pedido.FornecedorID = null;
                            pedidoRepository.Save(pedidoEmp);
                            pedidoEmp.EnviarEmailCancelamento();

                            //if (pedido.Faturamento.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.Faturamento.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                            //{
                            //    var boleto = pedido.Faturamento.Boleto;
                            //    boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                            //    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                            //    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            //    boleto.Processado = false;
                            //    boletoRepository.Save(boleto);
                            //}
                            if (!string.IsNullOrWhiteSpace(observacoes)) {
                                var nota = new PedidoEmpresaNota();
                                nota.DataCriacao = DateTime.Now;
                                nota.PedidoEmpresaID = pedidoEmp.ID;
                                nota.Observacoes =
                                    "Cancelado por: "
                                    + adminModel.Nome
                                    + "<br>" + "Motivo: "
                                    + Domain.Helpers.EnumHelper.GetDescription((TodosStatusCancelamento)statusCancelamentoID)
                                    + "<br>" + observacoes;
                                pedidoNotaRepository.Save(nota);
                            }

                            var NovoPedidoFloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCorporativo == pedidoEmp.ID && r.StatusCancelamentoID == null).FirstOrDefault();

                            if (NovoPedidoFloricultura != null)
                            {
                                NovoPedidoFloricultura.StatusCancelamentoID = (int)PedidoFloricultura.TodosStatusCancelamento.Removido;
                                NovoPedidoFloricultura.StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Cancelado;
                                NovoPedidoFloricultura.StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.Cancelado;
                                NovoPedidoFloricultura.StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Cancelado;

                                pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                {
                                    PedidoFloricultura = NovoPedidoFloricultura,
                                    DataCriacao = DateTime.Now,
                                    Observacoes = "Pedido cancelado automaticamente por conta ddo cancelamento do peido no sistema LACOS CORP."
                                });
                            }

                            return Json("OK");
                        }
                        else {
                            return Json("Este pedido tem repasses pagos ou aguardando pagamento. Você deve cancelar todos antes de concluir o pedido.");
                        }
                    }
                    else {
                        return Json("Este pedido já foi processado, não pode ser cancelado.");
                    }
                }
                else {
                    return Json("Este pedido já foi faturado, não pode ser cancelado. Remova do faturamento caso este ainda não tenha sido pago.");
                }
            }
            else
                return Json("O pedido não pode ser cancelado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Processar(int id) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.NovoPedido) {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ProcessouPedido);

                    pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoEmAnalise;
                    pedidoEmp.DataProcessamento = DateTime.Now;
                    pedidoEmp.DataAtualizacao = DateTime.Now;
                    pedidoRepository.Save(pedidoEmp);
                    return Json("OK");
                }
                else {
                    return Json("Este pedido já está sendo processado.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Concluir(int id) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                if (pedidoEmp.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise) {
                    //if (pedido.StatusEntrega == TodosStatusEntrega.Entregue || pedido.StatusEntrega == TodosStatusEntrega.EntregaNaoRequerida || pedido.StatusEntrega == TodosStatusEntrega.Devolvido) {
                    if (pedidoEmp.StatusEntrega == TodosStatusEntrega.Entregue)
                    {
                        if (pedidoEmp.FaturamentoID.HasValue) {
                            if (pedidoEmp.RepasseAtual.ID > 0) {
                                //Registra log do usuário
                                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ConcluiuPedido);

                                pedidoEmp.Status = PedidoEmpresa.TodosStatus.PedidoConcluido;
                                pedidoEmp.DataProcessamento = DateTime.Now;
                                pedidoEmp.DataAtualizacao = DateTime.Now;
                                pedidoRepository.Save(pedidoEmp);
                                return Json("OK");
                            }
                            else {
                                return Json("Este pedido não tem todos os repasses criados. Você deve criá-los antes de concluir o pedido.");
                            }
                        }
                        else {
                            return Json("O pedido precisa estar faturado para ser concluído.");
                        }
                    }
                    else {
                        return Json("O pedido tem que ter os status 'Entregue', 'Entrega não Requerida' ou 'Devolvido' para ser concluído. Altere o status, salve e tente novamente.");
                    }
                }
                else {
                    return Json("Este pedido precisa estar em processamento para ser concluído.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarFornecedor(int id, int fornecedorID) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                //NOVO FORNECEDOR
                if (!pedidoEmp.FornecedorID.HasValue) {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                    pedidoEmp.FornecedorID = fornecedorID;
                    pedidoRepository.Save(pedidoEmp);

                    var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();
                    if (fornecedorprecos != null) {
                        var repasse = new Repasse();
                        repasse.FornecedorID = pedidoEmp.FornecedorID;
                        repasse.PedidoEmpresaID = pedidoEmp.ID;
                        repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                        repasse.AdministradorID = adminModel.ID;
                        repasse.DataCriacao = DateTime.Now;
                        repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;

                        var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoEmp.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoEmp.ProdutoTamanhoID);
                        if (fornecedorpreco != null) {
                            if (fornecedorpreco.Repasse.HasValue) {
                                if (fornecedorpreco.Repasse.Value > 0) {
                                    repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                }
                            }
                        }
                        repasseRepository.Save(repasse);

                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.PreencheuRepasse);

                        #region INSERE PEDIDO LINS

                        // ADICIONAR O PEDIDO AUTOMATICAMENTE CASO O FORNECEDOR SEJA A --- FLORICULTURA FLOR DA LINS, ID 388
                        if (fornecedorID == 388)
                        {
                            var NovoPedidoFloricultura = new PedidoFloricultura
                            {
                                PedidoFloriculturaOrigem = PedidoFloriculturaOrigemRepository.Get(2),
                                Codigo = Guid.NewGuid(),
                                Numero = CriarNumero(),
                                ClienteFloricultura = ClienteFloriculturaRepository.Get(31),
                                StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Concluido,
                                StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.AguardandoPagamento,
                                MeioPagamentoID = (int)PedidoFloricultura.MeiosPagamento.Faturamento,
                                FormaPagamentoID = (int)PedidoFloricultura.FormasPagamento.Indefinido,
                                StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Pendente,
                                HorarioEntrega = pedidoEmp.DataEntrega,
                                CobrarNoLocal = false,
                                ValorTotal = 0,
                                ValorDesconto = 0,
                                CidadeID = 8570,
                                EstadoID = 26,
                                DataCriacao = DateTime.Now,
                                Obs = "Pedido automático inserido pelo sistema do LACOS CORP. NR = " + pedidoEmp.Numero,
                                IdPedidoCorporativo = pedidoEmp.ID
                            };

                            pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                            pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                            {
                                PedidoFloricultura = NovoPedidoFloricultura,
                                DataCriacao = DateTime.Now,
                                Observacoes = "Pedido Inserido automaticamente por conta da seleção da Flor Da Lins como Fornecedor."
                            });

                            var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == pedidoEmp.ProdutoTamanho.Produto.ID).FirstOrDefault();

                            if (ProdutoFloricultura != null)
                            {
                                var IdTamanhoProdFloricultura = 0;

                                switch (pedidoEmp.ProdutoTamanho.Tamanho.ID)
                                {
                                    // PEQUENO
                                    case 1:
                                    case 4:
                                    case 8:
                                    case 11:
                                    case 14:
                                    case 17:
                                    case 20:
                                        IdTamanhoProdFloricultura = 2;
                                        break;
                                    // MEDIO
                                    case 2:
                                    case 5:
                                    case 21:
                                    case 9:
                                    case 12:
                                    case 15:
                                    case 18:
                                        IdTamanhoProdFloricultura = 3;
                                        break;
                                    // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                    case 3:
                                    case 6:
                                    case 10:
                                    case 13:
                                    case 16:
                                    case 19:
                                    case 22:
                                        IdTamanhoProdFloricultura = 4;
                                        break;
                                    default:
                                        IdTamanhoProdFloricultura = 5;
                                        break;
                                }

                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                {
                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                    Descricao = ProdutoFloricultura.Nome,
                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                    Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem
                                });
                            }
                            else {
                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                {
                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                    Descricao = pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper(),
                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                    Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem,
                                    Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto Corporativo: " + pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper()
                                });
                            }

                            // ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                            var SomaItenPedidoFloricultura = NovoPedidoFloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                            NovoPedidoFloricultura.ValorTotal = SomaItenPedidoFloricultura;

                            // ADICIONA DADOS DE ENTREGA
                            NovoPedidoFloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                            NovoPedidoFloricultura.EstadoID = pedidoEmp.Estado.ID;
                            NovoPedidoFloricultura.CidadeID = pedidoEmp.Cidade.ID;
                            NovoPedidoFloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                            if (pedidoEmp.LocalID != null)
                            {
                                NovoPedidoFloricultura.LocalID = pedidoEmp.LocalID;
                            }

                            pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                            NovoPedidoFloricultura.EnviarPedidoEmail();
                        }

                        #endregion
                    }
                    return Json("OK");
                }
                //ALTERA FORNECEDOR
                else {
                    if (pedidoEmp.TotalRepassesPagos == 0) {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                        pedidoEmp.FornecedorID = fornecedorID;
                        pedidoRepository.Save(pedidoEmp);

                        var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();
                        if (fornecedorprecos != null) {
                            if (pedidoEmp.RepasseAtual.Status == Repasse.TodosStatus.AguardandoPagamento) {
                                var repasseatual = pedidoEmp.RepasseAtual;
                                repasseatual.Status = Repasse.TodosStatus.Cancelado;
                                repasseatual.Observacao = "\nRepasse cancelado por " + adminModel.CurrentAdministrador.Nome;
                                repasseRepository.Save(repasseatual);
                            }

                            var repasse = new Repasse();
                            repasse.FornecedorID = pedidoEmp.FornecedorID;
                            repasse.PedidoEmpresaID = pedidoEmp.ID;
                            repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                            repasse.DataCriacao = DateTime.Now;
                            repasse.Observacao = "\nRepasse alterado para " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                            repasse.AdministradorID = adminModel.ID;

                            var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoEmp.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoEmp.ProdutoTamanhoID);
                            if (fornecedorpreco != null) {
                                if (fornecedorpreco.Repasse.HasValue) {
                                    if (fornecedorpreco.Repasse.Value > 0) {
                                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouRepasse);
                                        repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                    }
                                }
                            }

                            #region ALTERA PEDIDO LINS

                            var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCorporativo == pedidoEmp.ID && r.StatusCancelamentoID == null).FirstOrDefault();
                            if (NovoPedidoloricultura != null)
                            {
                                NovoPedidoloricultura.StatusCancelamentoID = (int)PedidoFloricultura.TodosStatusCancelamento.Removido;
                                NovoPedidoloricultura.StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Cancelado;
                                NovoPedidoloricultura.StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.Cancelado;
                                NovoPedidoloricultura.StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Cancelado;

                                pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                                pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                {
                                    PedidoFloricultura = NovoPedidoloricultura,
                                    DataCriacao = DateTime.Now,
                                    Observacoes = "Pedido cancelado automaticamente por conta da remoção da Flor Da Lins como Fornecedor."
                                });
                            }

                            // ADICIONAR O PEDIDO AUTOMATICAMENTE CASO O FORNECEDOR SEJA A --- FLORICULTURA FLOR DA LINS, ID 388
                            if (fornecedorID == 388)
                            {
                                var NovoPedidoFloricultura = new PedidoFloricultura
                                {
                                    PedidoFloriculturaOrigem = PedidoFloriculturaOrigemRepository.Get(2),
                                    Codigo = Guid.NewGuid(),
                                    Numero = CriarNumero(),
                                    ClienteFloricultura = ClienteFloriculturaRepository.Get(31),
                                    StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Concluido,
                                    StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.AguardandoPagamento,
                                    MeioPagamentoID = (int)PedidoFloricultura.MeiosPagamento.Faturamento,
                                    FormaPagamentoID = (int)PedidoFloricultura.FormasPagamento.Indefinido,
                                    StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Pendente,
                                    ValorTotal = 0,
                                    HorarioEntrega = pedidoEmp.DataEntrega,
                                    CobrarNoLocal = false,
                                    ValorDesconto = 0,
                                    CidadeID = 8570,
                                    EstadoID = 26,
                                    DataCriacao = DateTime.Now,
                                    Obs = "Pedido automático inserido pelo sistema do LACOS CORP. NR = " + pedidoEmp.Numero,
                                    IdPedidoCorporativo = pedidoEmp.ID
                                };

                                pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                {
                                    PedidoFloricultura = NovoPedidoFloricultura,
                                    DataCriacao = DateTime.Now,
                                    Observacoes = "Pedido Inserido automaticamente por conta da seleção da Flor Da Lins como Fornecedor."
                                });

                                var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == pedidoEmp.ProdutoTamanho.Produto.ID).FirstOrDefault();

                                if (ProdutoFloricultura != null)
                                {
                                    var IdTamanhoProdFloricultura = 0;

                                    switch (pedidoEmp.ProdutoTamanho.Tamanho.ID)
                                    {
                                        // PEQUENO
                                        case 1:
                                        case 4:
                                        case 8:
                                        case 11:
                                        case 14:
                                        case 17:
                                        case 20:
                                            IdTamanhoProdFloricultura = 2;
                                            break;
                                        // MEDIO
                                        case 2:
                                        case 5:
                                        case 21:
                                        case 9:
                                        case 12:
                                        case 15:
                                        case 18:
                                            IdTamanhoProdFloricultura = 3;
                                            break;
                                        // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                        case 3:
                                        case 6:
                                        case 10:
                                        case 13:
                                        case 16:
                                        case 19:
                                        case 22:
                                            IdTamanhoProdFloricultura = 4;
                                            break;
                                        default:
                                            IdTamanhoProdFloricultura = 5;
                                            break;
                                    }

                                    var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                                    var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                    PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                    {
                                        PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                        Descricao = ProdutoFloricultura.Nome,
                                        ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                        Valor = (decimal)ValorTblRepasse.Repasse,
                                        Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem
                                    });
                                }
                                else
                                {
                                    var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                                    var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                    PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                    {
                                        PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                        Descricao = pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper(),
                                        ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                        Valor = (decimal)ValorTblRepasse.Repasse,
                                        Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem,
                                        Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto Corporativo: " + pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper()
                                    });
                                }

                                // ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                                var SomaItenPedidoFloricultura = NovoPedidoFloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                                NovoPedidoFloricultura.ValorTotal = SomaItenPedidoFloricultura;

                                // ADICIONA DADOS DE ENTREGA
                                NovoPedidoFloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                                NovoPedidoFloricultura.EstadoID = pedidoEmp.Estado.ID;
                                NovoPedidoFloricultura.CidadeID = pedidoEmp.Cidade.ID;
                                NovoPedidoFloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                                if (pedidoEmp.LocalID != null)
                                {
                                    NovoPedidoFloricultura.LocalID = pedidoEmp.LocalID;
                                }

                                pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                NovoPedidoFloricultura.EnviarPedidoEmail();
                            }

                            #endregion

                            repasseRepository.Save(repasse);

                        }
                        return Json("OK");
                    }
                    else {
                        return Json("O fornecedor não pode ser alterado porque possui um repasse pago. Cancele o repasse e faça a alteração novamente.");
                    }

                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarRepasse(int id, int itemID, decimal valor) {
            var pedidoEmp = pedidoRepository.Get(itemID);
            var repasse = repasseRepository.Get(id);
            if (repasse != null) {
                if (repasse.Status == Repasse.TodosStatus.AguardandoPagamento) {
                    repasse.Observacao = "\nRepasse alterado de " + repasse.ValorRepasse.ToString("C2") + " para " + valor.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                    repasse.ValorRepasse = valor;
                    repasseRepository.Save(repasse);

                    administradorPedidoRepository.Registra(adminModel.ID, null, itemID, null, null, AdministradorPedido.Acao.AlterouRepasse);

                    #region ATUALIZA PEDIDO DA LINS

                    var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCorporativo == pedidoEmp.ID && r.StatusCancelamentoID == null).FirstOrDefault();

                    if (NovoPedidoloricultura != null)
                    {
                        NovoPedidoloricultura.HorarioEntrega = pedidoEmp.DataSolicitada;
                        NovoPedidoloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedidoEmp.EstadoID;
                        NovoPedidoloricultura.CidadeID = pedidoEmp.CidadeID;
                        NovoPedidoloricultura.LocalID = pedidoEmp.LocalID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                        #region DELETA PRODUTOS ANTERIORES

                        var ListaItens = NovoPedidoloricultura.PedidoItemFloricultura.ToList();
                        foreach (var Item in ListaItens)
                        {
                            PedidoItemFloriculturaRepository.Delete(Item);
                        }
                        #endregion

                        var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == pedidoEmp.ProdutoTamanho.Produto.ID).FirstOrDefault();

                        if (ProdutoFloricultura != null)
                        {
                            var IdTamanhoProdFloricultura = 0;

                            switch (pedidoEmp.ProdutoTamanho.Tamanho.ID)
                            {
                                // PEQUENO
                                case 1:
                                case 4:
                                case 8:
                                case 11:
                                case 14:
                                case 17:
                                case 20:
                                    IdTamanhoProdFloricultura = 2;
                                    break;
                                // MEDIO
                                case 2:
                                case 5:
                                case 21:
                                case 9:
                                case 12:
                                case 15:
                                case 18:
                                    IdTamanhoProdFloricultura = 3;
                                    break;
                                // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                case 3:
                                case 6:
                                case 10:
                                case 13:
                                case 16:
                                case 19:
                                case 22:
                                    IdTamanhoProdFloricultura = 4;
                                    break;
                                default:
                                    IdTamanhoProdFloricultura = 5;
                                    break;
                            }

                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = ProdutoFloricultura.Nome,
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = (decimal)ValorTblRepasse.Repasse,
                                Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem
                            });
                        }
                        else {
                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            decimal ValorRepasse = 0;
                            if (ValorTblRepasse != null)
                            {
                                ValorRepasse = (decimal)ValorTblRepasse.Repasse;
                            }
                            else
                            {
                                if (pedidoEmp.RepasseAtual != null && pedidoEmp.RepasseAtual.ValorRepasse > 0)
                                {
                                    ValorRepasse = pedidoEmp.RepasseAtual.ValorRepasse;
                                }
                            }

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper(),
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = ValorRepasse,
                                Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem,
                                Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto Corporativo: " + pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + pedidoEmp.DescricaoProduto.ToUpper()
                            });
                        }

                        // ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                        var SomaItenPedidoFloricultura = NovoPedidoloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                        NovoPedidoloricultura.ValorTotal = SomaItenPedidoFloricultura;

                        // ADICIONA DADOS DE ENTREGA
                        NovoPedidoloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedidoEmp.Estado.ID;
                        NovoPedidoloricultura.CidadeID = pedidoEmp.Cidade.ID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                        if (pedidoEmp.LocalID != null)
                        {
                            NovoPedidoloricultura.LocalID = pedidoEmp.LocalID;
                        }

                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);
                    }

                    #endregion


                    return Json(new { retorno = "OK" });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "O valor do repasse só pode ser alterado se estiver aguardando pagamento." });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult CancelarLeilaoFornecedor(int pedidoEmpresaID, int localID)
        {
            var retorno = "";
            try
            {
                var lstFornecedoresLocal = fornecedorlocalREspository.GetByExpression(c => c.LocalID == localID).OrderByDescending(c => c.FornecedorID).ToList();

                foreach (var fornecedor in lstFornecedoresLocal)
                {
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmpresaID, null, null, AdministradorPedido.Acao.RecusouEntrega);
                }

                retorno = "OK";
            }
            catch (Exception)
            {
                retorno = "ERRO";
            }

            return Json(retorno);
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailEntrega(int id, string local, string data, string pessoa, string recebido, string parentesco, string complemento) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                var dataentrega = new DateTime();
                DateTime.TryParse(data, out dataentrega);
                if (dataentrega != new DateTime()) {
                   

                    pedidoEmp.LocalEntrega = local;
                    pedidoEmp.DataEntrega = dataentrega;
                    pedidoEmp.PessoaHomenageada = pessoa;
                    pedidoEmp.RecebidoPor = recebido;
                    pedidoEmp.Parentesco = parentesco;
                    pedidoEmp.ComplementoLocalEntrega = complemento;
                    pedidoEmp.DataEmailEntrega = DateTime.Now;
                    pedidoEmp.StatusEntrega = TodosStatusEntrega.Entregue;
                    pedidoRepository.Save(pedidoEmp);

                    pedidoEmp.EnviarEmailEntrega();

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                    //por hora apenas SMS para pedidos do site
                    //if (!pedido.Colaborador.Exterior) {
                    //    if (!string.IsNullOrEmpty(pedido.Cliente.CelularContatoSMS)) {
                    //        var retorno = "";

                    //        if (pedido.OrigemSite.ToUpper() == "LAÇOS FLORES")
                    //            Domain.Core.SMS.EnviaSMS("LACOS FLORES", pedido.Cliente.CelularContatoSMS, Domain.Core.Configuracoes.MSG_ZENVIA_SMS);
                    //        else
                    //            Domain.Core.SMS.EnviaSMS("COROAS PARA VELORIO", pedido.Cliente.CelularContatoSMS, Domain.Core.Configuracoes.MSG_ZENVIA_SMS);
                    //        return Json(retorno + "\nE-mail enviado com sucesso!");
                    //    }
                    //}
                    return Json("OK");
                }
                else
                    return Json("Data de entrega inválida!");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPedido(int id) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ReenviouEmailPedido);

                if (!String.IsNullOrEmpty(pedidoEmp.Colaborador.Email)) {
                    pedidoEmp.EnviarPedidoEmailSolicitante();
                    return Json("OK");
                }
                else {
                    return Json("O cliente não tem um e-mail cadastrado ou válido. Atualize e tente novamente.");
                }

            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            try
            {
                var id = Convert.ToInt32(form["ID"]);
                var nota = new PedidoEmpresaNota();
                var nota2 = new PedidoEmpresaNota();
                var pedidoEmp = pedidoRepository.Get(id);
                var statusentregaid = 0;

                #region Dados do Pedido
                //----------------------------------------------------------------------
                if (pedidoEmp != null)
                {
                    var datarepasseold = pedidoEmp.DataRepasse;
                    statusentregaid = pedidoEmp.StatusEntregaID;
                    pedidoEmp.StatusEntregaID = Convert.ToInt32(form["StatusEntregaID"]);
                    pedidoEmp.PessoaHomenageada = form["PessoaHomenageada"];
                    pedidoEmp.Parentesco = form["Parentesco"];
                    pedidoEmp.ParentescoRecebidoPor = form["ParentescoRecebidoPor"];
                    pedidoEmp.NomeFuncionario = form["NomeFuncionario"];
                    pedidoEmp.RecebidoPor = form["RecebidoPor"];
                    pedidoEmp.TelefoneContato = form["TelefoneContato"];

                    int? _localEntregaId = null;

                    if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                        _localEntregaId = int.Parse(form["LocalID"]);

                    if (_localEntregaId == 0)
                        _localEntregaId = null;

                    if (pedidoEmp.LocalID != _localEntregaId)
                    {
                        administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
                    }

                    pedidoEmp.LocalID = _localEntregaId;

                    if (!string.IsNullOrEmpty(form["DataEntrega"]))
                    {
                        try {pedidoEmp.DataEntrega = Convert.ToDateTime(form["DataEntrega"]); }
                        catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    }                    

                    if (!string.IsNullOrEmpty(form["DataSolicitada"]))
                    {
                        try
                        {
                            pedidoEmp.DataSolicitada = Convert.ToDateTime(form["DataSolicitada"]);
                        }
                        catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    }

                    if (!string.IsNullOrEmpty(form["EstadoID"]))
                        pedidoEmp.EstadoID = Convert.ToInt32(form["EstadoID"]);

                    if (!string.IsNullOrEmpty(form["CidadeID"]))
                        pedidoEmp.CidadeID = Convert.ToInt32(form["CidadeID"]);

                    pedidoEmp.ComplementoLocalEntrega = form["ComplementoLocalEntrega"];
                    pedidoEmp.Observacoes = form["Observacoes"];
                    pedidoEmp.DataAtualizacao = DateTime.Now;
                    pedidoEmp.Mensagem = form["Mensagem"];
                    //----------------------------------------------------------------------
                    #endregion

                    #region Foto Item
                    //----------------------------------------------------------------------
                    if (Request.Files.Count > 0)
                    {
                        var foto = Request.Files["File"];
                        if (foto != null)
                        {
                            if (foto.ContentLength > 0)
                            {
                                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedidoEmp.ID));
                                if (!dir.Exists)
                                {
                                    dir.Create();
                                }

                                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);

                                ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedidoEmp.ID + "/" + pedidoEmp.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                        "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=canvas"));

                                i.CreateParentDirectory = true;
                                i.Build();
                            }
                        }

                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region Historico Status Entrega
                    //----------------------------------------------------------------------
                    var historico = "";
                    if (pedidoEmp.StatusEntregaID != statusentregaid)
                        historico += "<b>Status Entrega: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((TodosStatusEntrega)statusentregaid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedidoEmp.StatusEntrega) + "</i>";
                    if (historico.Length > 0)
                    {
                        historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;
                        
                        nota2.DataCriacao = DateTime.Now;
                        nota2.PedidoEmpresaID = pedidoEmp.ID;
                        nota2.Observacoes = historico;
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region Notas
                    //----------------------------------------------------------------------
                    if (form["ObservacaoNota"].Length > 0)
                    {
                        nota.DataCriacao = DateTime.Now;
                        nota.PedidoEmpresaID = pedidoEmp.ID;
                        nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region SALVA DADOS PEDIDO
                    //----------------------------------------------------------------------
                    try
                    {
                        if (historico.Length > 0)
                            pedidoNotaRepository.Save(nota2);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }

                    try
                    {
                        if (form["ObservacaoNota"].Length > 0)
                            pedidoNotaRepository.Save(nota);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }

                    try
                    {
                        pedidoRepository.Save(pedidoEmp);
                    }
                    catch (Exception ex) { return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message }); }
                    //----------------------------------------------------------------------
                    #endregion

                    # region ATUALIZA PEDIDO DA LINS
                    //----------------------------------------------------------------------

                    var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCorporativo == pedidoEmp.ID && r.StatusCancelamentoID == null).FirstOrDefault();

                    if (NovoPedidoloricultura != null)
                    {
                        NovoPedidoloricultura.HorarioEntrega = pedidoEmp.DataSolicitada;
                        NovoPedidoloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedidoEmp.EstadoID;
                        NovoPedidoloricultura.CidadeID = pedidoEmp.CidadeID;
                        NovoPedidoloricultura.LocalID = pedidoEmp.LocalID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                        #region DELETA PRODUTOS ANTERIORES

                        var ListaItens = NovoPedidoloricultura.PedidoItemFloricultura.ToList();
                        foreach (var Item in ListaItens)
                        {
                            PedidoItemFloriculturaRepository.Delete(Item);
                        }
                        #endregion

                        var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == pedidoEmp.ProdutoTamanho.Produto.ID).FirstOrDefault();

                        if (ProdutoFloricultura != null)
                        {
                            var IdTamanhoProdFloricultura = 0;

                            switch (pedidoEmp.ProdutoTamanho.Tamanho.ID)
                            {
                                // PEQUENO
                                case 1:
                                case 4:
                                case 8:
                                case 11:
                                case 14:
                                case 17:
                                case 20:
                                    IdTamanhoProdFloricultura = 2;
                                    break;
                                // MEDIO
                                case 2:
                                case 5:
                                case 21:
                                case 9:
                                case 12:
                                case 15:
                                case 18:
                                    IdTamanhoProdFloricultura = 3;
                                    break;
                                // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                case 3:
                                case 6:
                                case 10:
                                case 13:
                                case 16:
                                case 19:
                                case 22:
                                    IdTamanhoProdFloricultura = 4;
                                    break;
                                default:
                                    IdTamanhoProdFloricultura = 5;
                                    break;
                            }

                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = ProdutoFloricultura.Nome,
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = (decimal)ValorTblRepasse.Repasse,
                                Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem
                            });
                        }
                        else
                        {
                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == pedidoEmp.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == pedidoEmp.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            decimal ValorRepasse = 0;
                            if (ValorTblRepasse != null)
                            {
                                ValorRepasse = (decimal)ValorTblRepasse.Repasse;
                            }
                            else
                            {
                                if (pedidoEmp.RepasseAtual != null && pedidoEmp.RepasseAtual.ValorRepasse > 0)
                                {
                                    ValorRepasse = pedidoEmp.RepasseAtual.ValorRepasse;
                                }
                            }

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper(),
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = ValorRepasse,
                                Mensagem = "NR = " + pedidoEmp.Numero + " / " + pedidoEmp.Mensagem,
                                Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto Corporativo: " + pedidoEmp.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + pedidoEmp.DescricaoProduto.ToUpper()
                            });
                        }
                        
                        #region ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                        //----------------------------------------------------------------------

                        var SomaItenPedidoFloricultura = NovoPedidoloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                        NovoPedidoloricultura.ValorTotal = SomaItenPedidoFloricultura;
                        //----------------------------------------------------------------------
                        #endregion

                        #region ADICIONA DADOS DE ENTREGA
                        //----------------------------------------------------------------------

                        NovoPedidoloricultura.PessoaHomenageada = pedidoEmp.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedidoEmp.Estado.ID;
                        NovoPedidoloricultura.CidadeID = pedidoEmp.Cidade.ID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedidoEmp.ComplementoLocalEntrega;

                        if (pedidoEmp.LocalID != null)
                        {
                            NovoPedidoloricultura.LocalID = pedidoEmp.LocalID;
                        }

                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);
                        //----------------------------------------------------------------------
                        #endregion
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    return RedirectToAction("Visualizar", new { id = pedidoEmp.ID, cod = "SaveSucess" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" + ex.Message });
            }
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form) {
            var EmpresaID = Convert.ToInt32(form["EmpresaID"]);
            var produtotamanhoid = Convert.ToInt32(form["EmpresaProdutoTamanhoID"]);
            var ColaboradorID = Convert.ToInt32(form["ColaboradorID"]);
            var codigo = form["Cupom"];
            var empresa = empresaRepository.Get(EmpresaID);
            var desconto = Convert.ToDecimal(form["ValorDesconto"]);
            var valortotal = Convert.ToDecimal(form["ValorTotal"]);
            var descricaoproduto = form["DescricaoProduto"];

           

            decimal valor = 0;
            decimal valorept = 0;
            decimal valorpt = 0;
            int? empresaprodutotamanhoid = null;
            var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produtotamanhoid && c.EmpresaID == EmpresaID).FirstOrDefault();
            var produtotamanho = produtoTamanhoRepository.Get(produtotamanhoid);

            if (empresaprodutoTamanho != null) {
                valorept = empresaprodutoTamanho.Valor;
                empresaprodutotamanhoid = empresaprodutoTamanho.ID;
            }
            if (produtotamanho != null) {
                valorpt = produtotamanho.Preco;
                produtotamanhoid = produtotamanho.ID;
            }
            if (valorept > 0)
                valor = valorept;
            else
                valor = valorpt;

            var valororiginal = valor;
            var cupom = cupomRepository.GetByCodigo(codigo);
            int? cupomID = null;
            if (cupom != null) {
                if (cupomService.PodeUtilizar(empresa, cupom)) {
                    cupomID = cupom.ID;
                    if (cupom.Tipo == Cupom.Tipos.Valor) {
                        valor = valor - cupom.Valor;
                    }
                    else if (cupom.Tipo == Cupom.Tipos.Porcentagem) {
                        valor = valor * (1 - cupom.Valor / 100);
                    }
                    if (valor < 0)
                        valor = 0;
                }
            }
            else {
                valor = valororiginal - desconto;
            }

            if (valortotal > 0 && valortotal != valor) {
                valor = valortotal;
            }

            var dataHora = form["DataEntrega"] + " " + form["hora"];
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada)) {
                return Content("Data e/ou Hora invalida(s)");
            }

            dynamic horario_opt = null;

            if (form["horario"] != null && form["horario"] != "Outro") {
                horario_opt = "Horário de Entrega: " + form["horario"] + "\n\n";
            }

            int? _localEntregaId = null;
            if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                _localEntregaId = int.Parse(form["LocalID"]);

            if (_localEntregaId == 0)
                _localEntregaId = null;


            var pedidoEmp = new PedidoEmpresa() {
                CidadeID = Convert.ToInt32(form["CidadeEntregaID"]),
                EmpresaProdutoTamanhoID = empresaprodutotamanhoid,
                ProdutoTamanhoID = produtotamanhoid,
                ColaboradorID = ColaboradorID,
                EmpresaID = EmpresaID,
                Codigo = Guid.NewGuid(),
                Mensagem = form["Frase"].ToUpper(),
                Departamento = form["Departamento"],
                Origem = "ADMIN",
                OrigemSite = "LAÇOS CORPORATIVOS",
                OrigemForma = form["OrigemForma"].ToUpper(),
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                EstadoID = Convert.ToInt32(form["EstadoEntregaID"]),
                LocalEntrega = form["LocalID"],
                ComplementoLocalEntrega = form["ComplementoLocalEntrega"],
                Observacoes = horario_opt + "",
                Parentesco = form["Parentesco"],
                NomeFuncionario = form["NomeFuncionario"],
                PessoaHomenageada = form["PessoaHomenageada"],
                Status = PedidoEmpresa.TodosStatus.NovoPedido,
                StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                TelefoneContato = form["TelefoneContato"],
                CupomID = cupomID,
                Valor = valor,
                ValorDesconto = desconto,
                SubTotal = valororiginal,
                QualidadeStatusContatoID = (int)PedidoEmpresa.TodosStatusContato.NI,
                QualidadeStatusContatoNaoID = (int)PedidoEmpresa.TodosStatusContatoNao.NI,
                DescricaoProduto = descricaoproduto,
                LocalID = _localEntregaId
            };
            pedidoRepository.Save(pedidoEmp);

            if (pedidoEmp.LocalID != _localEntregaId)
            {
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
            }

            if (pedidoEmp.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
            {
                pedidoEmp.LocalID = null;
                pedidoRepository.Save(pedidoEmp);
            }


            //pedido.Valor = pedido.EmpresaProdutoTamanho.Valor;
            //pedidoRepository.Save(pedido);
            pedidoEmp.EnviarPedidoEmail();

            if (!String.IsNullOrEmpty(form["ObservacaoNota"])) {
                var nota = new PedidoEmpresaNota();
                nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                nota.DataCriacao = DateTime.Now;
                nota.PedidoEmpresaID = pedidoEmp.ID;
                pedidoNotaRepository.Save(nota);
            }

            if (cupomID.HasValue) {
                cupomService.Utilizar(cupom.Codigo);

                //CRIA UMA NOTA
                var pedidonota = new PedidoEmpresaNota();
                pedidonota.PedidoEmpresaID = pedidoEmp.ID;
                pedidonota.DataCriacao = DateTime.Now;
                pedidonota.Observacoes = "Empresa utilizou o cupom " + cupom.Codigo + " que garantiu " + (cupom.Tipo == Cupom.Tipos.Valor ? cupom.Valor.ToString("C2") : cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                pedidoNotaRepository.Save(pedidonota);
            }

            //Registra log do usuário
            administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.ProcessouPedido);

            if (pedidoEmp.Empresa.FaturamentoInstantaneo) {
                var fatura = new Faturamento();
                fatura.EmpresaID = EmpresaID;
                fatura.Titulo = "automática referente ao pedido #" + pedidoEmp.ID;
                fatura.Observacoes = "";
                fatura.Status = Faturamento.TodosStatus.AguardandoPagamento;
                fatura.StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento;
                fatura.FormaPagamento = Pedido.FormasPagamento.Boleto;
                fatura.MeioPagamento = Pedido.MeiosPagamento.Boleto;
                fatura.ValorTotal = pedidoEmp.Valor;
                fatura.ValorDesconto = pedidoEmp.ValorDesconto;
                fatura.ValorEstorno = 0;
                fatura.ValorPago = 0;
                fatura.NFeValor = pedidoEmp.Valor;
                fatura.DataCriacao = DateTime.Now;
                fatura.DataAtualizacao = DateTime.Now;
                fatura.DataVencimento = Funcoes.DataVencimentoUtil(pedidoEmp.Empresa.DiasParaVencimentoBoleto);
                faturamentoRepository.Save(fatura);

                pedidoEmp.FaturamentoID = fatura.ID;
                pedidoRepository.Save(pedidoEmp);

                if (fatura.ValorTotal > 0) {
                    if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto) {
                        var random = new Random();
                        var nossoNumero = "";
                        do {
                            nossoNumero = random.Next(99999999).ToString("00000000");
                        }
                        while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                        // 24/02/17 - Rodrigo - devido a problemas no cnab 
                        //Mudanca da forma de gerar , devido aos id de pedido terem passdo de 5 caracteres 
                        //Informação ITAU - "No. do Documento" informado pelo Beneficiário é composto por: 10 dígitos
                        //var numeroDocumento = "2" + DateTime.Now.Year + pedido.ID.ToString("00000");

                       var numeroDocumento = "20" + pedidoEmp.ID.ToString("00000");

                        var boleto = new Boleto();
                        boleto.FaturamentoID = fatura.ID;
                        boleto.TipoID = (int)Boleto.TodosTipos.PJ;
                        boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                        boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                        boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                        boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                        boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                        boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                        boleto.DataCriacao = DateTime.Now;
                        boleto.DataVencimento = fatura.DataVencimento;
                        boleto.DataEnvioLembreteVencimento = null;
                        boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC;
                        boleto.NossoNumero = nossoNumero;
                        boleto.NumeroDocumento = numeroDocumento;
                        boleto.SacadoDocumento = fatura.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "").Trim();
                        boleto.SacadoNome = fatura.Empresa.RazaoSocial;
                        if (String.IsNullOrEmpty(fatura.Empresa.Bairro))
                            boleto.SacadoBairro = "";
                        else
                            boleto.SacadoBairro = fatura.Empresa.Bairro;

                        if (String.IsNullOrEmpty(fatura.Empresa.CEP))
                            boleto.SacadoCEP = "";
                        else
                            boleto.SacadoCEP = fatura.Empresa.CEP;

                        if (fatura.Empresa.Cidade == null)
                            boleto.SacadoCidade = "";
                        else
                            boleto.SacadoCidade = fatura.Empresa.Cidade.Nome;

                        if (String.IsNullOrEmpty(fatura.Empresa.Logradouro))
                            boleto.SacadoEndereco = "";
                        else
                            boleto.SacadoEndereco = fatura.Empresa.Logradouro + " " + fatura.Empresa.Numero + " " + fatura.Empresa.Complemento;

                        if (fatura.Empresa.Estado == null)
                            boleto.SacadoUF = "";
                        else
                            boleto.SacadoUF = fatura.Empresa.Estado.Sigla;

                        boleto.Comando = Boleto.Comandos.CriarRemessa;
                        boleto.Processado = false;
                        boleto.StatusPagamento = Boleto.TodosStatusPagamento.AguardandoPagamento;
                        boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Criado;
                        boleto.Valor = fatura.ValorTotal;

                        boletoRepository.Save(boleto);

                        fatura.EnviarBoleto();
                        var msg = Server.UrlEncode("O pedido " + pedidoEmp.ID + " foi criado com sucesso!");
                        Response.Redirect("/PedidosEmpresa/List?datainicial=" + DateTime.Now.ToShortDateString() + "&cod=SaveSucess&msg=" + msg);
                    }
                }
            }

            Response.Redirect("/PedidosEmpresa/List?datainicial=" + DateTime.Now.ToShortDateString() + "&cod=SaveSucess&msg=" + "O pedido " + pedidoEmp.ID + " foi criado com sucesso!");

            return RedirectToAction("List");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarProduto(int id, int produtoTamanhoID, decimal valorProduto = 0, string descricao = "") {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                if (pedido.Status == PedidoEmpresa.TodosStatus.NovoPedido || pedido.Status == PedidoEmpresa.TodosStatus.PedidoEmAnalise) {
                    decimal valor = 0;
                    decimal valorept = 0;
                    decimal valorpt = 0;
                    int? empresaprodutotamanhoid = null;
                    var empresaprodutoTamanho = empresaProdutoTamanhoRepository.GetByExpression(c => c.ProdutoTamanhoID == produtoTamanhoID && c.EmpresaID == pedido.EmpresaID).FirstOrDefault();
                    var produtotamanho = produtoTamanhoRepository.Get(produtoTamanhoID);

                    if (empresaprodutoTamanho != null) {
                        valorept = empresaprodutoTamanho.Valor;
                        empresaprodutotamanhoid = empresaprodutoTamanho.ID;
                    }
                    if (produtotamanho != null) {
                        valorpt = produtotamanho.Preco;
                        produtoTamanhoID = produtotamanho.ID;
                    }
                    if (valorept > 0)
                        valor = valorept;
                    else
                        valor = valorpt;

                    var valororiginal = valor;
                    if (pedido.Cupom != null) {
                        if (pedido.Cupom.Tipo == Cupom.Tipos.Valor) {
                            valor = valor - pedido.Cupom.Valor;
                        }
                        else if (pedido.Cupom.Tipo == Cupom.Tipos.Porcentagem) {
                            valor = valor * (1 - pedido.Cupom.Valor / 100);
                        }
                        if (valor < 0)
                            valor = 0;
                        pedido.ValorDesconto = valororiginal - valor;


                        //CRIA UMA NOTA
                        var pedidonota = new PedidoEmpresaNota();
                        pedidonota.PedidoEmpresaID = pedido.ID;
                        pedidonota.DataCriacao = DateTime.Now;
                        pedidonota.Observacoes = "Empresa utilizou o cupom " + pedido.Cupom.Codigo + " que garantiu " + (pedido.Cupom.Tipo == Cupom.Tipos.Valor ? pedido.Cupom.Valor.ToString("C2") : pedido.Cupom.Valor + "%") + " de desconto sobre o valor original de " + valororiginal.ToString("C2");
                        pedidoNotaRepository.Save(pedidonota);
                    }
                    else {
                        valor = valororiginal - pedido.ValorDesconto;
                    }

                    if (valorProduto > 0 && valorProduto != valor) {
                        valor = valorProduto;
                        pedido.ValorDesconto = 0;
                    }

                    pedido.EmpresaProdutoTamanhoID = empresaprodutotamanhoid;
                    pedido.ProdutoTamanhoID = produtoTamanhoID;
                    pedido.Valor = valor;
                    pedido.DescricaoProduto = descricao;
                    pedido.SubTotal = valororiginal;
                    pedidoRepository.Save(pedido);
                    return Json(new { retorno = "OK" });
                }
                else {
                    return Json(new { retorno = "ERRO", mensagem = "Pedido precisa estar com status de novo para alterar o produto" });
                }
            }
            else {
                return Json(new { retorno = "ERRO", mensagem = "Pedido inválido" });
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFrases(int id) {
            var empresa = empresaRepository.Get(id);

            List<SelectListItem> result = new List<SelectListItem>();
            if (empresa != null) {
                foreach (EmpresaFrase frase in empresa.EmpresaFrase) {
                    result.Add(new SelectListItem { Text = frase.Frase, Value = frase.Frase });
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult ValidaCupom(int empresaID, string codigo) {
            var empresa = empresaRepository.Get(empresaID);
            if (empresa == null) {
                return Json(new { mensagem = "Empresa inválida", retorno = "ERRO" });
            }
            var cupom = cupomRepository.GetByCodigo(codigo);
            if (cupom == null) {
                return Json(new { mensagem = "Cupom inválido", retorno = "ERRO" });
            }
            else if (!cupomService.PodeUtilizar(empresa, cupom)) {
                return Json(new { mensagem = "Esta empresa já utilizou esse cupom", retorno = "ERRO" });
            }
            else {
                var retorno = "Este cupom garante ";
                if (cupom.Tipo == Cupom.Tipos.Porcentagem)
                    retorno += cupom.Valor + "% de desconto.";
                else
                    retorno += cupom.Valor.ToString("C2") + " de desconto.";
                return Json(new { mensagem = retorno, retorno = "OK", valor = cupom.Valor.ToString("N2").Replace(".", "").Replace(",", "."), tipo = cupom.TipoID });
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailFornecedor(int id, string local, string pessoa, int fornecedorID) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, pedidoEmp.ID, null, null, AdministradorPedido.Acao.EnviouEmailFornecedor);

                pedidoEmp.LocalEntrega = local;
                pedidoEmp.PessoaHomenageada = pessoa;
                pedidoEmp.FornecedorID = fornecedorID;
                pedidoRepository.Save(pedidoEmp);
                if (!String.IsNullOrEmpty(pedidoEmp.Fornecedor.EmailContato)) {
                    if (pedidoEmp.EnviarEmailFornecedor())
                        return Json("OK");
                    else
                        return Json("Não foi possível enviar o e-mail no momento. Tente novamente mais tarde.");
                }
                else {
                    return Json("O fornecedor não tem um e-mail cadastrado de contato. Atualize e tente novamente.");
                }
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult FornecedorInfo(int fornecedorID) {
            var fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null) {
                return Json(new { retorno = "OK", caracteristicas = fornecedor.Caracteristicas, frete = fornecedor.ValorFrete.ToString("C2"), horario = fornecedor.HorarioFuncionamento, problemas = fornecedor.ResponsavelProblemas, nome = "" + fornecedor.Nome + "", telefones = "" + fornecedor.TelefonePrimario + "<br>" + fornecedor.TelefoneSecundario + "<br>" + fornecedor.TelefonesAdicionais + "", contato = "" + fornecedor.PessoaContato + "", obs = "" + fornecedor.Observacao + "", entrega24h = "" + (fornecedor.Entrega24h ? "SIM" : "NÃO") + "" });
            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult PedidoPorFornecedor(int fornecedorID)
        {
            var pedido = pedidoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).Take(5).OrderByDescending(c => c.DataCriacao);


            if (pedido != null)
            {
                return Json(new { retorno = "OK", pedido });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        #endregion

        #region Metodos
        public string CriarNumero()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                if (!new PedidoFloricultura().NumeroExiste(numero.ToString()))
                {
                    return numero.ToString();
                }
            } while (true);
        }
        //----------------------------------------------------------------------
        public List<PedidoEmpresa> ListaPedidosEmpresa()
        {
            List<PedidoEmpresa> lstPedidos = new List<PedidoEmpresa>();

            if (!string.IsNullOrWhiteSpace(Request.QueryString["ID"]))
            {
                int id;
                if (int.TryParse(Request.QueryString["ID"], out id))
                {
                    lstPedidos = pedidoRepository.GetByExpression(p => p.ID == id).ToList();
                }
            }
            else {
                Expression<Func<PedidoEmpresa, bool>> exp = p => p.ID > 0;

                #region Data inicial
                if (!string.IsNullOrWhiteSpace(Request.QueryString["datainicial"]))
                {
                    DateTime datainicial = new DateTime();
                    if (DateTime.TryParse(Request.QueryString["datainicial"], out datainicial))
                    {
                        exp = exp.And(p => p.DataCriacao >= datainicial);
                    }
                }
                #endregion

                #region Data final
                if (!string.IsNullOrWhiteSpace(Request.QueryString["datafinal"]))
                {
                    DateTime datafinal = new DateTime();
                    if (DateTime.TryParse(Request.QueryString["datafinal"], out datafinal))
                    {
                        datafinal = new DateTime(datafinal.Year, datafinal.Month, datafinal.Day).AddDays(1).AddSeconds(-1);
                        exp = exp.And(p => p.DataCriacao <= datafinal);
                    }
                }
                #endregion

                #region E-mail
                if (!string.IsNullOrWhiteSpace(Request.QueryString["email"]))
                {
                    string email = Request.QueryString["email"];
                    exp = exp.And(p => p.Colaborador.Email.Contains(email));
                }
                #endregion

                #region Meio Pagamento
                if (!string.IsNullOrWhiteSpace(Request.QueryString["meiopagamento"]))
                {
                    int meiopagamento;
                    if (int.TryParse(Request.QueryString["meiopagamento"], out meiopagamento) && meiopagamento > 0)
                    {
                        exp = exp.And(p => p.FaturamentoID.HasValue && p.Faturamento.MeioPagamentoID == meiopagamento);
                    }
                }
                #endregion

                #region Cliente ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["clienteID"]))
                {
                    int clienteID;
                    if (int.TryParse(Request.QueryString["clienteID"], out clienteID))
                    {
                        exp = exp.And(p => p.EmpresaID == clienteID);
                        Empresa emp = empresaRepository.GetByExpression(e => e.ID == clienteID).FirstOrDefault();
                        if (emp != null)
                        {
                            ViewBag.RazaoSocialEmpresa = emp.RazaoSocial;
                        }
                    }
                }
                #endregion

                #region Cupom ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["cupomID"]))
                {
                    int cupomID;
                    if (int.TryParse(Request.QueryString["cupomID"], out cupomID))
                    {
                        exp = exp.And(p => p.CupomID == cupomID);
                    }
                }
                #endregion

                #region Não Faturado
                if (!string.IsNullOrWhiteSpace(Request.QueryString["naofaturado"]))
                {
                    bool naofaturado;
                    if (bool.TryParse(Request.QueryString["naofaturado"], out naofaturado) && naofaturado)
                    {
                        exp = exp.And(p => !p.FaturamentoID.HasValue);
                    }
                }
                #endregion

                #region Status
                if (!string.IsNullOrWhiteSpace(Request.QueryString["status"]))
                {
                    int status;
                    if (int.TryParse(Request.QueryString["status"], out status))
                    {
                        exp = exp.And(p => p.StatusID == status);
                    }
                }
                #endregion

                #region GrupoEconomico
                if (!string.IsNullOrWhiteSpace(Request.QueryString["GrupoEconomico"]))
                {
                    string GrupoEconomico = Request.QueryString["GrupoEconomico"];
                    exp = exp.And(p => p.Empresa.GrupoEconomico.ToUpper() == GrupoEconomico);
                }
                #endregion

                #region Status Pagamento
                if (!string.IsNullOrWhiteSpace(Request.QueryString["statuspagamento"]))
                {
                    int statuspagamento;
                    if (int.TryParse(Request.QueryString["statuspagamento"], out statuspagamento))
                    {
                        exp = exp.And(p => p.FaturamentoID.HasValue && p.Faturamento.StatusPagamentoID == statuspagamento);
                    }
                }
                #endregion

                #region Status Entrega
                if (!string.IsNullOrWhiteSpace(Request.QueryString["statusentrega"]))
                {
                    int statusentrega;
                    if (int.TryParse(Request.QueryString["statusentrega"], out statusentrega))
                    {
                        exp = exp.And(p => p.StatusEntregaID == statusentrega);
                    }
                }
                #endregion

                //faz a consulta com a expressão montada acima (exp), com os parametros opcionais
                lstPedidos = pedidoRepository.GetByExpression(exp).ToList();

                #region Razão Social
                var razao = Request.QueryString["razao"];
                if (!string.IsNullOrWhiteSpace(razao))
                {
                    lstPedidos = lstPedidos.Where(c =>
                    Funcoes.AcertaAcentos(c.Empresa.RazaoSocial.ToLower()).Contains(Funcoes.AcertaAcentos(razao.ToLower()))
                    || Funcoes.AcertaAcentos(c.Empresa.NomeFantasia.ToLower()).Contains(Funcoes.AcertaAcentos(razao.ToLower()))).ToList();
                }
                #endregion

            }


            lstPedidos = lstPedidos.OrderByDescending(p => p.DataCriacao).ToList();

            return lstPedidos;
        }
        //----------------------------------------------------------------------
        public void ExportExcel(List<PedidoEmpresa> LstObjects)
        {
            string Csv = string.Empty;

            MemoryStream ms = new MemoryStream();
            TextWriter tw = new StreamWriter(ms, Encoding.UTF8);

            string TBL = string.Empty;

            TBL += "<table>";
            TBL += "<tr>";
            TBL += "<td>PRODUTO</td>";
            TBL += "<td>FATURADO</td>";
            TBL += "<td>ATENDENTE</td>";
            TBL += "<td>DATA CRIAÇÃO</td>";
            TBL += "<td>STATUS</td>";
            TBL += "<td>VALOR</td>";
            TBL += "<td>DATA ENTREGA</td>";
            TBL += "<td>PESSOA HOM</td>";
            TBL += "<td>FAIXA</td>";
            TBL += "<td>FUNCIONÁRIO</td>";
            TBL += "<td>PARENTESCO</td>";
            TBL += "<td>DEPARTAMENTO</td>";
            TBL += "<td>LOCAL ENTR.</td>";
            TBL += "<td>ESTADO</td>";
            TBL += "<td>CIDADE</td>";
            TBL += "<td>OBS</td>";
            TBL += "<td>TEL CONTATO</td>";
            TBL += "</tr>";

            foreach (var iten in LstObjects)
            {
                string NomeAdm = "";

                if(iten.AdministradorPedido != null)
                {
                    if(iten.AdministradorPedido.Count > 0)
                        NomeAdm = iten.AdministradorPedido.ToList()[0].Administrador.Nome;
                }

                string Faturado = string.Empty;

                if(iten.Faturamento == null)
                {
                    Faturado = "NÃO";
                }
                else
                {
                    Faturado = "SIM";
                }

                TBL += "<tr>";
                TBL += "<td>" + iten.ProdutoTamanho.Produto.Nome + " " + iten.ProdutoTamanho.Tamanho.Nome + "</td>";
                TBL += "<td>" + Faturado + "</td>";
                TBL += "<td>" + NomeAdm + "</td>";
                TBL += "<td>" + iten.DataCriacao + "</td>";
                TBL += "<td>" + Domain.Helpers.EnumHelper.GetDescription(iten.StatusEntrega) + "</td>";
                TBL += "<td>" + iten.Valor + "</td>";
                TBL += "<td>" + iten.DataEntrega + "</td>";
                TBL += "<td>" + iten.PessoaHomenageada + "</td>";
                TBL += "<td>" + iten.Mensagem + "</td>";
                TBL += "<td>" + iten.NomeFuncionario + "</td>";
                TBL += "<td>" + iten.Parentesco + "</td>";
                TBL += "<td>" + iten.Departamento + "</td>";
                TBL += "<td>" + iten.LocalEntrega.Replace(System.Environment.NewLine, " ") + "</td>";
                TBL += "<td>" + iten.Estado.Nome + "</td>";
                TBL += "<td>" + iten.Cidade.Nome + "</td>";
                TBL += "<td>" + iten.Observacoes.Replace(System.Environment.NewLine, "") + "</td>";
                TBL += "<td>" + iten.TelefoneContato + "</td>";                
                TBL += "</tr>";
            }

            TBL += "</table>";
            tw.Write(TBL);

            tw.Flush();
            byte[] bytes = ms.ToArray();
            ms.Close();
            

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            
            Response.AddHeader("content-disposition", "attachment; filename=Export.xls");
            Response.BinaryWrite(bytes);
            Response.End();

        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}
