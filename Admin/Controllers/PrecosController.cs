﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.IO;
using System.Collections.Specialized;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class PrecosController : Controller
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;

        public PrecosController(ObjectContext context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
        }

        public ActionResult List(int tipo)
        {
            var produtos = produtoRepository.GetByExpression(c => c.TipoID == tipo).OrderBy(p => p.Nome).ToList();
            ViewBag.Tamanhos = tamanhoRepository.GetAll().Count();
            return View(produtos);
        }

        [HttpGet]
        public ActionResult Edit(int produtoID, int tipo, int? id)
        {
            Produto produto = produtoRepository.Get(produtoID);
            if(produto == null)
                return RedirectToAction("List");

            ProdutoTamanho produtoTamanho = null;
            if (id.HasValue)
            {
                produtoTamanho = produtoTamanhoRepository.Get(id.Value);
                //ViewBag.Tamanhos = new List<Tamanho>();
                //ViewBag.Tamanhos.Add(produtoTamanho.Tamanho);
                ViewBag.Tamanhos = tamanhoRepository.GetAll().OrderBy(f => f.ID);
            }
            else
            {
                //ViewBag.Tamanhos = tamanhoRepository.GetByExpression(c => c.ProdutoTamanhoes.Count(p => p.ProdutoID == produto.ID) == 0).OrderBy(f => f.ID);
                ViewBag.Tamanhos = tamanhoRepository.GetAll().OrderBy(f => f.ID);
            }
            ViewBag.Produto = produto;
            return View(produtoTamanho);
        }


        [HttpPost]
        public ActionResult Edit(ProdutoTamanho produtoTamanho)
        {
            produtoTamanhoRepository.Save(produtoTamanho);

            return RedirectToAction("List", new { tipo = produtoTamanho.Produto.TipoID, cod = "SaveSucess" });
        }

        public ActionResult Delete(int id)
        {
            var produtoTamanho = produtoTamanhoRepository.Get(id);

            var ResultfornecedorPreco = fornecedorPrecoRepository.GetByExpression(p => p.ProdutoTamanhoID == id).ToList();

            foreach (var item in ResultfornecedorPreco)
            {
                fornecedorPrecoRepository.Delete(item);
            }


            if(produtoTamanho != null)
            {
                var tipo = produtoTamanho.Produto.TipoID;
                produtoTamanhoRepository.Delete(produtoTamanho);

                return RedirectToAction("List", new { tipo = tipo, cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "DeleteFailure" });
            }
        }

    }
}
