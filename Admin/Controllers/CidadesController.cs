﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores", "/Home/AccessDenied")]
    public class CidadesController : CrudController<Cidade>
    {

        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;

        public CidadesController(ObjectContext context)
            : base(context)
        {
            cidadeRepository = new PersistentRepository<Cidade>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = cidadeRepository.GetAll().OrderBy(p => p.Nome).ToList();
            if (Request.QueryString["termo"] != null)
            {
                var termo = Request.QueryString["termo"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower();
                if (termo != null)
                    resultado = resultado.Where(c => c.Nome.Trim().ToLower().Contains(termo)).OrderBy(c => c.Nome).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Cidade cidade = null;
            if (id.HasValue)
            {
                cidade = cidadeRepository.Get(id.Value);
            } 
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(cidade);
        }

    }
}
