﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Service;
using MvcExtensions.Controllers;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using System.IO;
using System.Globalization;
using Domain.Repositories;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using Domain.Factories;
using PagarMe;
using System.Text;
using Domain.Core;
using System.Net;
using System.Collections.Specialized;

namespace Admin.Controllers
{
    public class NFEaEmitirController : CrudController<Pedido>
    {
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private PedidoRepository pedidoRepository;        
        private AdminModel adminModel;
        private RelatorioRepository relatorioRepository;

        public NFEaEmitirController(ObjectContext context) : base(context)
        {            
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            pedidoRepository = new PedidoRepository(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            adminModel = new AdminModel(context);
            relatorioRepository = new RelatorioRepository(context);
        }
        public List<IPedido> ListaIPedidos()
        {
            var resultado = new List<IPedido>();
            string NumeroPedido = Request.QueryString["numero"];

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);
            datafinal = datafinal.AddDays(1).AddMilliseconds(-1);

            if (!string.IsNullOrEmpty(NumeroPedido))
            {
                resultado.AddRange(pedidoRepository.GetByExpression(r => r.Numero == NumeroPedido && r.StatusCancelamentoID == null).ToList());

                int NrCorp;
                if(int.TryParse(NumeroPedido, out NrCorp))
                {
                    resultado.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.ID == NrCorp && r.StatusCancelamentoID == null).ToList());
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["datainicial"]) && !string.IsNullOrEmpty(Request.QueryString["datafinal"]))
                {
                    resultado.AddRange(pedidoRepository.GetByExpression(r => r.DataCriacao >= datainicial && r.DataCriacao <= datafinal && (r.NFeData == null) && r.StatusCancelamentoID == null).ToList());
                    resultado.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.DataCriacao >= datainicial && r.DataCriacao <= datafinal && (r.Faturamento.NFeData == null && r.Faturamento != null) && r.StatusCancelamentoID == null).ToList());
                }
                else
                {
                    resultado.AddRange(pedidoRepository.GetByExpression(r => r.DataCriacao >= DateTime.Today && r.DataCriacao.Day == DateTime.Today.Day && r.DataCriacao.Month == DateTime.Today.Month && r.DataCriacao.Year == DateTime.Today.Year && (r.NFeData == null) && r.StatusCancelamentoID == null).ToList());
                    resultado.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.DataCriacao >= DateTime.Today && r.DataCriacao.Day == DateTime.Today.Day && r.DataCriacao.Month == DateTime.Today.Month && r.DataCriacao.Year == DateTime.Today.Year && (r.Faturamento.NFeData == null && r.Faturamento != null) && r.StatusCancelamentoID == null).ToList());
                }
            }

            var nome = Request.QueryString["nome"];

            if (!String.IsNullOrEmpty(nome))
            {
                var ListPedidosCPV = resultado.Where(r => r.TipoPedido == OrigemPedido.Cliente).ToList().ConvertAll(x => (Pedido)x).Where(c => c.Cliente.Nome.ToLower().Contains(nome.ToLower())).ToList();
                var ListPedidosCORP = resultado.Where(r => r.TipoPedido == OrigemPedido.Empresa).ToList().ConvertAll(x => (PedidoEmpresa)x).Where(c => c.Empresa.NomeCompleto.ToLower().Contains(nome.ToLower())).ToList();

                resultado.Clear();

                resultado.AddRange(ListPedidosCPV);
                resultado.AddRange(ListPedidosCORP);
            }

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamentoped"], out statuspagamento);

            if (statuspagamento > 0)
            {
                var ListPedidosCPV = resultado.Where(r => r.TipoPedido == OrigemPedido.Cliente).ToList().ConvertAll(x => (Pedido)x).Where(c => c.StatusPagamentoID == statuspagamento).ToList();
                var ListPedidosCORP = resultado.Where(r => r.TipoPedido == OrigemPedido.Empresa).ToList().ConvertAll(x => (PedidoEmpresa)x).Where(c => c.Faturamento.StatusPagamentoID == statuspagamento).ToList();

                resultado.Clear();

                resultado.AddRange(ListPedidosCPV);
                resultado.AddRange(ListPedidosCORP);
            }

            var tipopedido = 0;
            Int32.TryParse(Request.QueryString["tipopedido"], out tipopedido);

            if (tipopedido == 2)
            {
                resultado = resultado.Where(r => r.TipoPedido == OrigemPedido.Empresa).ToList();
            }
            if (tipopedido == 1)
            {
                resultado = resultado.Where(r => r.TipoPedido == OrigemPedido.Cliente).ToList();
            }

            resultado = resultado.OrderByDescending(c => c.ID).OrderBy(p => p.DataCriacao).ToList();

            return resultado;
        }
        public ActionResult ListResult()
        {
            ViewBag.pedidoRepository = pedidoRepository;
            return View();
        }

        public ActionResult ListProcessar()
        {
            if(Domain.Core.Configuracoes.HOMOLOGACAO == false)
            {
                var LstNFAutomaticaEmitirCPV = relatorioRepository.GetRelatorioNFEmitirCPV();
                ViewBag.LstNFAutomaticaEmitirCPV = LstNFAutomaticaEmitirCPV;

                foreach (var item in LstNFAutomaticaEmitirCPV)
                {
                    var Result = Atualizar(item.PedidoID + "|CPV");
                    if(Result.Data.ToString().Contains("OK"))
                    {
                        administradorPedidoRepository.Registra(adminModel.ID, item.PedidoID, null, null, null, AdministradorPedido.Acao.NFEnviadaParaFocus);
                    }
                }
            }

            List<IPedido> LstPedidos = new List<IPedido>();

            // PEDIDOS CPV
            LstPedidos.AddRange(pedidoRepository.GetByExpression(r => r.StatusNfID == (int)Pedido.TodosStatusNFs.Processando).ToList());

            // PEDIDOS CORP
            LstPedidos.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.Faturamento.StatusNfID == (int)Faturamento.TodosStatusNFs.Processando).ToList());

            // PEDIDOS CORP - REMESSA
            LstPedidos.AddRange(pedidoEmpresaRepository.GetByExpression(r => r.StatusNfIDRemessa == (int)PedidoEmpresa.TodosStatusNFsRemessa.Processando).ToList());

            ViewBag.pedidoRepository = pedidoRepository;

            return View(LstPedidos);
        }

        public JsonResult Consultar(int PedidoID, string Tipo, string Modo)
        {
            RetornoResult novoRetorno = null;
            NFE ResultConsultar = null;

            Pedido PedidoCPV = null;
            PedidoEmpresa PedidoCORP = null;

            try
            {
                if (Tipo == "CPV")
                {
                    PedidoCPV = pedidoRepository.Get(PedidoID);

                    #region REQUISITAR A CONSULTA

                    ResultConsultar = NFEFactory.Consultar(PedidoCPV.NrReferenciaNF, NFE.TiposConsulta.emissao, OrigemPedido.Cliente, PedidoID);

                    #endregion

                    #region VERIFICA RETORNO DA CONSULTA

                    if (ResultConsultar != null)
                    {
                        if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.erro_autorizacao)
                        {
                            PedidoCPV.StatusNF = Pedido.TodosStatusNFs.NaoEmitida;
                            pedidoRepository.Save(PedidoCPV);

                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Cliente, LinkNF = "", Msg = ResultConsultar.mensagem_sefaz + " - Cod: " + ResultConsultar.status_sefaz };
                        }
                        else if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.autorizado)
                        {
                            PedidoCPV.NFeData = DateTime.Now;
                            PedidoCPV.NFeNumero = int.Parse(ResultConsultar.numeroNFe);
                            PedidoCPV.NFeValor = PedidoCPV.ValorTotal;

                            PedidoCPV.StatusNF = Pedido.TodosStatusNFs.Emitida;

                            PedidoCPV.CaminhoXmlNotaFiscal = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoXML;
                            PedidoCPV.NFeLink = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoDanfe;

                            if (PedidoCPV.NFeLink == null && PedidoCPV.CaminhoXmlNotaFiscal != null)
                                PedidoCPV.NFeLink = PedidoCPV.CaminhoXmlNotaFiscal.Replace(".xml", ".pdf");

                            PedidoCPV.NrReferenciaNF = ResultConsultar.NrReferenciaNF;

                            pedidoRepository.Save(PedidoCPV);

                            #region NFE SUBSTITUTA

                            if (PedidoCPV.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && PedidoCPV.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
                            {
                                PedidoCPV.EnviarEmailNfSubstituta("cobranca@lacosflores.com.br", "Cobranca");
                            }
                            else {
                                PedidoCPV.EnviarNFe();
                                PedidoCPV.DataEmailNFe = DateTime.Now;
                            }

                            #endregion

                            administradorPedidoRepository.Registra(adminModel.ID, PedidoCPV.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailAutomaticoNFe);
                            administradorPedidoRepository.Registra(adminModel.ID, PedidoCPV.ID, null, null, null, AdministradorPedido.Acao.PreencheuNF);

                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Cliente, LinkNF = "https://api.focusnfe.com.br" + ResultConsultar.caminhoDanfe, Msg = "OK" };
                        }
                        else
                        {
                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Cliente, LinkNF = "", Msg = "NF não emitida - Motivo: Erro Não Tratado. " + ResultConsultar.statusNF};
                        }
                    }

                    #endregion

                }
                else
                {
                    PedidoCORP = pedidoEmpresaRepository.Get(PedidoID);

                    #region REQUISITAR A CONSULTA

                    if (Modo == "Remessa")
                    {
                        ResultConsultar = NFEFactory.Consultar(PedidoCORP.NrReferenciaNFRemessa, NFE.TiposConsulta.emissao, OrigemPedido.Empresa, PedidoID);
                    }
                    else
                    {
                        ResultConsultar = NFEFactory.Consultar(PedidoCORP.Faturamento.NrReferenciaNF, NFE.TiposConsulta.emissao, OrigemPedido.Empresa, PedidoID);
                    }

                    #endregion
                   
                    #region VERIFICA RETORNO DA CONSULTA

                    if (ResultConsultar != null)
                    {
                        if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.erro_autorizacao)
                        {
                            if (Modo == "Remessa")
                            {
                                PedidoCORP.StatusNFRemessa = PedidoEmpresa.TodosStatusNFsRemessa.NaoEmitida;
                            }
                            else
                            {
                                PedidoCORP.Faturamento.StatusNF = Faturamento.TodosStatusNFs.NaoEmitida;
                            }

                            pedidoEmpresaRepository.Save(PedidoCORP);

                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Empresa, LinkNF = "", Msg = ResultConsultar.mensagem_sefaz + " - Cod: " + ResultConsultar.status_sefaz };
                        }
                        else if (ResultConsultar.statusNF == NFE.TodosStatusProcessamento.autorizado)
                        {
                            if (Modo == "Remessa")
                            {
                                PedidoCORP.NFeDataRemessa = DateTime.Now;
                                PedidoCORP.NFeNumeroRemessa = int.Parse(ResultConsultar.numeroNFe);
                                PedidoCORP.NFeValorRemessa = PedidoCORP.Valor;

                                PedidoCORP.StatusNFRemessa = PedidoEmpresa.TodosStatusNFsRemessa.Emitida;

                                PedidoCORP.CaminhoXmlNotaFiscalRemessa = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoXML;
                                PedidoCORP.NFeLinkRemessa = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoDanfe;

                                if (PedidoCORP.NFeLinkRemessa == null && PedidoCORP.CaminhoXmlNotaFiscalRemessa != null)
                                    PedidoCORP.NFeLinkRemessa = PedidoCORP.CaminhoXmlNotaFiscalRemessa.Replace(".xml", ".pdf");

                                PedidoCORP.NrReferenciaNFRemessa = ResultConsultar.NrReferenciaNF;
                            }
                            else
                            {
                                PedidoCORP.Faturamento.NFeData = DateTime.Now;
                                PedidoCORP.Faturamento.NFeNumero = int.Parse(ResultConsultar.numeroNFe);
                                PedidoCORP.Faturamento.NFeValor = PedidoCORP.Faturamento.ValorTotal;

                                PedidoCORP.Faturamento.StatusNF = Faturamento.TodosStatusNFs.Emitida;

                                PedidoCORP.Faturamento.CaminhoXmlNotaFiscal = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoXML;
                                PedidoCORP.Faturamento.NFeLink = Configuracoes.Focus_LinkNFE + ResultConsultar.caminhoDanfe;

                                if (PedidoCORP.Faturamento.NFeLink == null && PedidoCORP.Faturamento.CaminhoXmlNotaFiscal != null)
                                    PedidoCORP.Faturamento.NFeLink = PedidoCORP.Faturamento.CaminhoXmlNotaFiscal.Replace(".xml", ".pdf");

                                PedidoCORP.Faturamento.NrReferenciaNF = ResultConsultar.NrReferenciaNF;

                                #region NFE SUBSTITUTA

                                if (PedidoCORP.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && PedidoCORP.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
                                {
                                    PedidoCORP.Faturamento.EnviarEmailNfSubstituta("cobranca@coroasparavelorio.com.br", "Cobranca");
                                }
                                
                                #endregion
                            }

                            pedidoEmpresaRepository.Save(PedidoCORP);

                            if (!string.IsNullOrEmpty(PedidoCORP.Faturamento.NFeLink))
                            {
                                administradorPedidoRepository.Registra(adminModel.ID, null, PedidoCORP.ID, null, null, AdministradorPedido.Acao.PreencheuNF);
                                PedidoCORP.Faturamento.EnviarNFe();
                                PedidoCORP.Faturamento.DataEmailNFe = DateTime.Now;
                                administradorPedidoRepository.Registra(adminModel.ID, null, PedidoCORP.ID, null, null, AdministradorPedido.Acao.EnviouEmailNFe);
                            }
                           
                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Empresa, LinkNF = "https://api.focusnfe.com.br" + ResultConsultar.caminhoDanfe, Msg = "OK" };
                        }
                        else
                        {
                            novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Empresa, LinkNF = "", Msg = "NF não emitida - Motivo: Erro Não Tratado. " + ResultConsultar.statusNF };
                            }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                novoRetorno = new RetornoResult { IdPedido = PedidoID.ToString(), TipoPedido = OrigemPedido.Cliente, LinkNF = "", Msg = ex.Message };
            }

            return Json(novoRetorno);
        }

        public JsonResult Atualizar(string Pedidos)
        {
            foreach (var ID in Pedidos.Split(','))
            {
               
                if (ID.Split('|')[1] == "CPV")
                {
                    #region NF CPV

                    var PedidoCPVID = int.Parse(ID.Split('|')[0]);

                    var PedidoCPV = pedidoRepository.Get(PedidoCPVID);

                    #region ATUALIZA NF AUTOMATICA

                    if((PedidoCPV.Cliente.NFAutomatica == false || PedidoCPV.Cliente.NFAutomatica == null) && PedidoCPV.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica)
                    {
                        PedidoCPV.Cliente.NFAutomatica = true;
                        pedidoRepository.Save(PedidoCPV);
                    }

                    #endregion

                    #region ATUALIZA NF SUBSTITUTIVA

                    if(PedidoCPV.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && PedidoCPV.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
                    {
                        PedidoCPV.StatusNfID = 1;
                        pedidoRepository.Save(PedidoCPV);
                    }

                    #endregion

                    var ResultEmissao = EnviarNF(PedidoCPVID, "CPV");

                    if (!string.IsNullOrEmpty(ResultEmissao.NrReferenciaNF))
                    {
                        PedidoCPV.NrReferenciaNF = ResultEmissao.NrReferenciaNF;
                        PedidoCPV.StatusNF = Pedido.TodosStatusNFs.Processando;

                        pedidoRepository.Save(PedidoCPV);

                        return Json("OK");
                    }
                    else
                    {
                        PedidoCPV.StatusNF = Pedido.TodosStatusNFs.NaoEmitida;
                        pedidoRepository.Save(PedidoCPV);

                        return Json(ResultEmissao.mensagem_sefaz);
                    }

                    #endregion
                }
                else if (ID.Split('|')[1].Split(':')[0] == "CORPREMESSA")
                {
                    #region NF CORP REMESSA

                    var PedidoID = int.Parse(ID.Split('|')[0]);
                    var PedidoCorp = pedidoEmpresaRepository.Get(PedidoID);

                    var ResultEmissao = EnviarNF(PedidoID, "CORPREMESSA", ID.Split('|')[1].Split(':')[1]);

                    if (!string.IsNullOrEmpty(ResultEmissao.NrReferenciaNF))
                    {
                        PedidoCorp.NrReferenciaNFRemessa = ResultEmissao.NrReferenciaNF;
                        PedidoCorp.StatusNFRemessa = PedidoEmpresa.TodosStatusNFsRemessa.Processando;

                        pedidoEmpresaRepository.Save(PedidoCorp);

                        return Json("OK");
                    }
                    else
                    {
                        PedidoCorp.StatusNFRemessa = PedidoEmpresa.TodosStatusNFsRemessa.NaoEmitida;
                        pedidoEmpresaRepository.Save(PedidoCorp);

                        return Json(ResultEmissao.mensagem_sefaz);
                    }

                    #endregion
                }
                else
                {
                    #region NF CORP

                    var IdFat = int.Parse(ID.Split('|')[0]);

                    var FatCorporativo = faturamentoRepository.Get(IdFat);

                    #region ATUALIZA NF SUBSTITUTIVA

                    if (FatCorporativo.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && FatCorporativo.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
                    {
                        FatCorporativo.StatusNfID = 1;
                        faturamentoRepository.Save(FatCorporativo);
                    }

                    #endregion

                    var ResultEmissao = EnviarNF(IdFat, "CORP");

                    if (!string.IsNullOrEmpty(ResultEmissao.NrReferenciaNF))
                    {
                        FatCorporativo.NrReferenciaNF = ResultEmissao.NrReferenciaNF;
                        FatCorporativo.StatusNF = Faturamento.TodosStatusNFs.Processando;

                        faturamentoRepository.Save(FatCorporativo);

                        return Json("OK");
                    }
                    else
                    {
                        FatCorporativo.StatusNF = Faturamento.TodosStatusNFs.NaoEmitida;
                        faturamentoRepository.Save(FatCorporativo);

                        return Json(ResultEmissao.mensagem_sefaz);
                    }

                    #endregion
                }
            }
            return Json("ERRO");
        }

        public NFE EnviarNF(int IdPedido, string TipoPedido, string NrCFOP = null)
        {
            var PedidoID = "";
            List<RetornoResult> novoRetorno = new List<RetornoResult>();

            if (TipoPedido == "CPV")
            {
                var NF = new NFE();
                NF.PedidoCPV = IdPedido;
                PedidoID = NF.PedidoCPV.ToString();

                var PedidoCPV = pedidoRepository.Get(IdPedido);
                if (PedidoCPV.StatusNF == Pedido.TodosStatusNFs.NaoEmitida)
                {
                    try
                    {
                        return NFEFactory.Emitir(NF, OrigemPedido.Cliente);
                    }
                    catch (Exception ex)
                    {
                        return new NFE { mensagem_sefaz = ex.Message };
                    }
                }
                else
                {
                    return new NFE { mensagem_sefaz = "Nota fiscal já emitida ou agurdando retorno" };
                }
            }
            else if (TipoPedido == "CORPREMESSA")
            {
                var NF = new NFE();
                NF.PedidoCORP = IdPedido;

                if (!string.IsNullOrEmpty(NrCFOP))
                    NF.CFOP = NrCFOP;

                var PedidoCorp = pedidoEmpresaRepository.Get(IdPedido);

                if (PedidoCorp.StatusNfIDRemessa == null || PedidoCorp.StatusNFRemessa == PedidoEmpresa.TodosStatusNFsRemessa.NaoEmitida)
                {
                    try
                    {
                        return NFEFactory.Emitir(NF, OrigemPedido.Empresa);
                    }
                    catch (Exception ex)
                    {
                        return new NFE { mensagem_sefaz = ex.Message };
                    }
                }
                else
                {
                    return new NFE { mensagem_sefaz = "Nota fiscal já emitida ou agurdando retorno" };
                }
            }
            else
            {
                var NF = new NFE();
                NF.IdFat = IdPedido;

                var Faturamento = faturamentoRepository.Get(IdPedido);

                if (Faturamento.StatusNfID == null || Faturamento.StatusNF == Faturamento.TodosStatusNFs.NaoEmitida)
                {
                    try
                    {
                        return NFEFactory.Emitir(NF, OrigemPedido.Empresa);
                    }
                    catch (Exception ex)
                    {
                        return new NFE { mensagem_sefaz = ex.Message };
                    }
                }
                else
                {
                    return new NFE { mensagem_sefaz = "Nota fiscal já emitida ou agurdando retorno" };
                }
            }
        }

        public override ActionResult 
            List(params object[] args)
        {
            var resultado = ListaIPedidos();

            ////PAGINAÇÃO
            //var paginaAtual = 0;
            //Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            //if (paginaAtual == 0)
            //    paginaAtual = 1;

            //var totalItens = resultado.Count;
            //var totalPaginas = Math.Ceiling((decimal)totalItens / 40);
            //if (totalPaginas > 1)
            //{
            //    resultado = resultado.Skip((paginaAtual - 1) * 40).Take(40).ToList();
            //}
            //ViewBag.PaginaAtual = paginaAtual;
            //ViewBag.TotalItens = totalItens;
            //ViewBag.TotalPaginas = totalPaginas;
            //ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
             
            return View(resultado);
        }

    }
}
