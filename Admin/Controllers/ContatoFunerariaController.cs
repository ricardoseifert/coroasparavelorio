﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Floricultura|FloriculturaMisto", "/Home/AccessDenied")]
    public class ContatoFunerariaController : CrudController<ContatoFunararia>
    {
        private IPersistentRepository<ContatoFunararia> contatofunerariaRepository;
        private IPersistentRepository<Funeraria> funerariaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private AdminModel adminModel;


        public ContatoFunerariaController(ObjectContext context)
            : base(context)
        {
            adminModel = new AdminModel(context);
            contatofunerariaRepository = new PersistentRepository<ContatoFunararia>(context);
            funerariaRepository = new PersistentRepository<Funeraria>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = contatofunerariaRepository.GetAll().OrderBy(p => p.Nome).ToList();

            int clienteid = 0;
            Int32.TryParse(Request.QueryString["clienteid"], out clienteid);
            if (clienteid > 0)
                resultado = resultado.Where(c => c.ID == clienteid).ToList();

            int FunerariaID = 0;
            Int32.TryParse(Request.QueryString["FunerariaID"], out FunerariaID);
            if (FunerariaID > 0)
                resultado = resultado.Where(c => c.Funeraria.ID == FunerariaID).ToList();

            var funeraria = Request.QueryString["funeraria"];
            if (!String.IsNullOrEmpty(funeraria))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.Funeraria.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(funeraria.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.Funeraria.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(funeraria.ToLower()))).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.Nome.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.Nome.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower()))).ToList();

            var email = Request.QueryString["email"];
            if (!String.IsNullOrEmpty(email))
                resultado = resultado.Where(c => c.Email.Contains(email)).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }

            

            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            return View(resultado);
        }

        [HttpGet]
        public ActionResult Editar(int id)
        {
            ContatoFunararia Funeraria = contatofunerariaRepository.Get(id);

            ViewBag.Funerarias = funerariaRepository.GetAll().OrderBy(r => r.RazaoSocial).ToList();
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            return View(Funeraria);
        }

        [HttpGet]
        public ActionResult Novo()
        {
            ViewBag.Funerarias = funerariaRepository.GetAll().OrderBy(r => r.RazaoSocial).ToList();
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Editar(ContatoFunararia RequestCliente)
        {
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            ContatoFunararia FunEdit = new ContatoFunararia();
            if (RequestCliente.ID > 0)
            {
                FunEdit = contatofunerariaRepository.Get(RequestCliente.ID);
            }

            FunEdit.Nome = RequestCliente.Nome.ToUpper();
            FunEdit.Email = RequestCliente.Email;
            FunEdit.Telefone = RequestCliente.Telefone;
            FunEdit.Celular = RequestCliente.Celular;
            FunEdit.Funeraria = funerariaRepository.Get(RequestCliente.Funeraria.ID);

            contatofunerariaRepository.Save(FunEdit);

            if (!string.IsNullOrEmpty(Request.Form["redirect"]))
            {
                return RedirectToAction("Novo", "PedidosFloricultura", new { cod = "SaveSucess", contatoid = FunEdit.ID, redirect = "true", tipocadastro = "funeraria" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "SaveSucess", clienteid = FunEdit.ID });
            }
        }

        [HttpGet]
        public ActionResult Deletar(int id)
        {
            var deleteResult = contatofunerariaRepository.Get(id);
            try
            {
                contatofunerariaRepository.Delete(deleteResult);
                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { cod = "DeleteFailure", msg = "Não foi possível excluir o registro. É possível que haja algum pedido relacionado." });
            }
            
        }

        [HttpPost]
        public JsonResult GetNomeContato(int? IdContato)
        {            
            return Json(contatofunerariaRepository.Get((int)IdContato).Nome);
        }
    }
}
