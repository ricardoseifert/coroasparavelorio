﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using Domain.Factories;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;
using Admin.ViewModels;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
    public class RemessaController : Controller
    {
        public int AdminId
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Usuario"]))
                {
                    return int.Parse(Request.QueryString["Usuario"]);
                }
                return adminModel.PerfilId == 3 ? adminModel.CurrentAdministrador.ID : 0;
            }
        }

        private IPersistentRepository<Remessa> remessaRepository;
        private AdminModel adminModel;
        public RemessaController(ObjectContext context)
        {
            adminModel = new AdminModel(context);
            remessaRepository = new PersistentRepository<Remessa>(context);
        }

        public ActionResult Index()
        {
            ViewBag.adminModel = adminModel;

            return View();
        }

        public ActionResult List(string Tipo)
        {
            var BaseDate = DateTime.Now.AddDays(-120);
            int CodCedente = 0;
            ViewBag.Tipo = Tipo;

            if (Tipo == "CPV")
            {
                CodCedente = Domain.Core.Configuracoes.BOLETO_CODIGO_CEDENTE;
            }
            if (Tipo == "LC")
            {
                CodCedente = Domain.Core.Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
            }

            var resultado = remessaRepository.GetByExpression(r => r.Data >= BaseDate && r.RemessaBoletoes.Where(p => r.RemessaBoletoes.Where(x => x.Boleto.CedenteCodigo == CodCedente).Any()).Any()).ToList().OrderByDescending(p => p.Data).ToList();            

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }
        
        public JsonResult Novo(string Tipo)
        {
            int Total = 0;

            if(Tipo.ToString() == "1")
            {
                Total = BoletoFactory.CriarRemessa(Remessa.Empresa.CPV);
            }

            if (Tipo.ToString() == "2")
            {
                Total = BoletoFactory.CriarRemessa(Remessa.Empresa.LC);
            }

            if (Total > 0)
                return Json(Total);
            else
                return Json(Total);
        }
        
        public ActionResult Download(int id)
        {
            var remessa = remessaRepository.Get(id);
            if (remessa != null)
                return File("/content/remessas/" + remessa.Arquivo, "utf-8", remessa.Arquivo);
            else
                return RedirectToAction("List");
        }
    }
}
