﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores", "/Home/AccessDenied")]
    public class RelatorioController : Controller
    {
        private IPersistentRepository<Pesquisa> pesquisaRepository;

        public RelatorioController(ObjectContext context)
        {
            pesquisaRepository = new PersistentRepository<Pesquisa>(context);
        }

        public ActionResult Pesquisa()
        {
            var resultado = pesquisaRepository.GetAll().OrderByDescending(d => d.DataCriacao).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        public ActionResult PesquisaExcel()
        {
            var pesquisa = pesquisaRepository.GetAll().OrderByDescending(d => d.DataCriacao).ToList();
            var resultado = "<table class='gridtable'><tr><th width='150'>Nome</th><th width='150'>E-mail</th><th width='150'>Telefone</th><th width='150'>Pedido</th><th width='150'>Cliente</th><th width='150'>Prazo</th><th width='150'>Qualidade</th><th width='150'>Atendimento</th><th width='150'>Recomendaria?</th><th width='150'>Como Conheceu</th><th width='150'>Satisfeito</th><th width='150'>Porque CPV</th><th width='150'>Sugestões</th><th width='150'>Data</th>";

            foreach (var item in pesquisa)
            {
                resultado += "<tr><td>" + item.Nome + "</td><td>" + item.Email + "</td><td>" + item.Telefone + "</td><td>" + (item.Pedido != null ? item.Pedido.Numero : "") + "</td><td>" + (item.Cliente != null ? item.Cliente.Nome + "<br>" + item.Cliente.RazaoSocial : "") + "</td><td>" + item.PrazoEntrega + "</td><td>" + item.QualidadeProduto + "</td><td>" + item.Atendimento + "</td><td>" + (item.Recomendacao.HasValue ? item.Recomendacao.Value ? "SIM" : "NÃO" : "NAO RESPONDEU") + "</td><td>" + item.ComoConheceu + "</td><td>" + (item.Satisfeito.HasValue ? item.Satisfeito.Value ? "SIM" : "NÃO/n" + item.SatisfeitoNao : "NAO RESPONDEU") + "</td><td>" + Domain.Core.Funcoes.AcertaAcentos(item.Sugestoes) + "</td><td>" + item.PorqueCPV + "</td><td>" + item.DataCriacao.Value.ToString("dd/MM/yyyy HH:mm") + "</td>";
            }

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "iso-8859-1";
            Response.BufferOutput = true;
            Response.AddHeader("content-disposition", "attachment; filename = relatorio-pesquisa.xls");
            Response.Write(resultado);
            Response.End();
            return View();
        }
    }
}
