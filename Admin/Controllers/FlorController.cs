﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class FlorController : CrudController<Flor>
    {
        private IPersistentRepository<Flor> florRepository;

        public FlorController(ObjectContext context) : base(context)
        {
            florRepository = new PersistentRepository<Flor>(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = florRepository.GetAll().OrderBy(p => p.Nome).ToList();
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Flor flor)
        {
            ModelState.Remove("Foto");
            if (flor.ID == 0)
                flor.Foto = "";
            flor.RotuloUrl = Domain.Core.Funcoes.GeraRotuloMVC(flor.Nome);
            florRepository.Save(flor);

            var foto = Request.Files["File"];
            if (foto.ContentLength > 0)
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/flores/" + flor.ID));
                if (!dir.Exists)
                {
                    dir.Create();
                }

                //try
                //{

                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                //FOTO
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/flores/" + flor.ID + "/" + nomefoto), new ImageResizer.ResizeSettings(
                                        "width=150&height=150&crop=auto;format=jpg;mode=pad;scale=both"));

                i.CreateParentDirectory = true;
                i.Build();

                flor.Foto = nomefoto;
                florRepository.Save(flor);

                //THUMB
                i = new ImageResizer.ImageJob(Server.MapPath("/content/flores/" + flor.ID + "/" + flor.Foto), Server.MapPath("/content/flores/" + flor.ID + "/thumb/" + flor.Foto), new ImageResizer.ResizeSettings(
                                        "width=70&height=70&crop=auto;format=jpg;mode=crop;scale=both"));

                i.CreateParentDirectory = true;
                i.Build();

                //}
                //catch { }
            }
            return RedirectToAction("List", new { cod = "SaveSucess" });
        }

    }
}
