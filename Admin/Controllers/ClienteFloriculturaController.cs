﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Floricultura|FloriculturaMisto", "/Home/AccessDenied")]
    public class ClienteFloriculturaController : CrudController<ClienteFloricultura>
    {
        private IPersistentRepository<ClienteFloricultura> clienteFloriculturaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private AdminModel adminModel;


        public ClienteFloriculturaController(ObjectContext context)
            : base(context)
        {
            adminModel = new AdminModel(context);
            clienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            adminModel = new AdminModel(context);
        }


        public ActionResult Caminho(string area, Cliente cliente)
        {
            ViewBag.Area = area;
            ViewBag.Cliente = cliente;
            return View();
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = clienteFloriculturaRepository.GetAll().OrderBy(p => p.Nome).ToList();

            int clienteid = 0;
            Int32.TryParse(Request.QueryString["clienteid"], out clienteid);
            if (clienteid > 0)
                resultado = resultado.Where(c => c.ID == clienteid).ToList();

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => Domain.Core.Funcoes.AcertaAcentos(c.RazaoSocial.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower())) || Domain.Core.Funcoes.AcertaAcentos(c.Nome.ToLower()).Contains(Domain.Core.Funcoes.AcertaAcentos(nome.ToLower()))).ToList();

            var cpf = Request.QueryString["cpf"];
            if (!String.IsNullOrEmpty(cpf))
                resultado = resultado.Where(c => Domain.Core.Funcoes.Formata_CNPJ(c.Documento).Contains(Domain.Core.Funcoes.Formata_CNPJ(cpf))).ToList();

            int pedidos = 0;
            Int32.TryParse(Request.QueryString["pedidos"], out pedidos);
            if (pedidos > 0)
                resultado = resultado.Where(c => c.PedidoFloriculturas.Count >= pedidos).ToList();
             
             
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            ClienteFloricultura cliente = null;
            if (id.HasValue)
            {
                cliente = clienteFloriculturaRepository.Get(id.Value);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.Estado.ID).OrderBy(e => e.Nome);
            }
            else
            {
                ViewBag.Cidades = new List<Cidade>();
            }

            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(cliente);
        }

        [ValidateInput(false)] 
        public override ActionResult Edit(ClienteFloricultura RequestCliente)
        {
            ModelState.Remove("RazaoSocial"); 
            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();
            ViewBag.Administrador = adminModel.CurrentAdministrador;

            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == RequestCliente.Estado.ID).OrderBy(e => e.Nome);

            ClienteFloricultura novoCliente = new ClienteFloricultura();
            var Documento = RequestCliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim();

            if (RequestCliente.ID > 0)
            {
                novoCliente = clienteFloriculturaRepository.Get(RequestCliente.ID);

                novoCliente.DataAtualizacao = DateTime.Now;

                if (clienteFloriculturaRepository.GetByExpression(c => c.Documento.Trim().ToLower() == Documento && c.ID != novoCliente.ID).Count() > 0)
                {
                    ViewBag.Erro = "Este CPF/CNPJ já está sendo utilizada por outro cliente";
                    return View(RequestCliente);
                }
            }
            else
            {
                novoCliente.DataCadastro = DateTime.Now;

                if (clienteFloriculturaRepository.GetByExpression(c => c.Documento.Trim().ToLower() == Documento).Count() > 0)
                {
                    ViewBag.Erro = "Este CPF/CNPJ já está sendo utilizada por outro cliente";
                    return View(RequestCliente);
                }
            }

            if (RequestCliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica && !Domain.Core.Funcoes.Valida_CPF(RequestCliente.Documento))
            {
                ViewBag.Erro = "CPF inválido";
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == RequestCliente.Estado.ID).OrderBy(e => e.Nome);
                return View(RequestCliente);
            }
            else if (RequestCliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica && !Domain.Core.Funcoes.Valida_CNPJ(RequestCliente.Documento))
            {
                ViewBag.Erro = "CNPJ inválido";
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == RequestCliente.Cidade.ID).OrderBy(e => e.Nome);
                return View(RequestCliente);
            }
            else
            {
                if (RequestCliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica)
                    RequestCliente.RazaoSocial = "";

                if (RequestCliente.RazaoSocial == null)
                    RequestCliente.RazaoSocial = "";

                novoCliente.Cidade = cidadeRepository.Get(RequestCliente.Cidade.ID);
                novoCliente.Estado = estadoRepository.Get(RequestCliente.Estado.ID);

                novoCliente.Documento = RequestCliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                novoCliente.Nome = RequestCliente.Nome.ToUpper();
                novoCliente.RazaoSocial = RequestCliente.RazaoSocial.ToUpper();
                novoCliente.Email = RequestCliente.Email;
                novoCliente.TipoID = RequestCliente.TipoID;
                novoCliente.TelefoneContato = RequestCliente.TelefoneContato;
                novoCliente.CelularContato = RequestCliente.CelularContato;
                novoCliente.CEP = RequestCliente.CEP;
                novoCliente.Logradouro = RequestCliente.Logradouro;
                novoCliente.Numero = RequestCliente.Numero;
                novoCliente.Complemento = RequestCliente.Complemento;
                novoCliente.Bairro = RequestCliente.Bairro;

                clienteFloriculturaRepository.Save(novoCliente);

                if (!string.IsNullOrEmpty(Request.Form["redirect"]))
                {
                    return RedirectToAction("Novo", "PedidosFloricultura", new { cod = "SaveSucess", clienteid = novoCliente.ID, redirect = "true", tipocadastro = "cliente" });
                }
                else
                {
                    return RedirectToAction("List", new { cod = "SaveSucess", clienteid = novoCliente.ID });
                }

                
            }
        }

        [HttpPost]
        public JsonResult GetClientes(string Nome, string CNPJ, string Email)
        {
            var clientes = clienteFloriculturaRepository.GetAll().OrderBy(r => r.RazaoSocial).ToList();

            if (!string.IsNullOrEmpty(Nome))
                clientes = clientes.Where(c => c.RazaoSocial.ToLower().Contains(Nome.ToLower()) || c.NomeCompleto.ToLower().Contains(Nome.ToLower())).ToList();

            if (!string.IsNullOrEmpty(CNPJ))
                clientes = clientes.Where(c => c.Documento.ToLower().Contains(CNPJ.ToLower())).ToList();

            if (!string.IsNullOrEmpty(Email))
                clientes = clientes.Where(c => c.Email.ToLower().Contains(Email.ToLower())).ToList();

            if (clientes.Count > 0)
            {
                var Balcao = clientes.Where(r => r.ID == 17);

                if(Balcao != null)
                {
                    clientes.Remove(Balcao.FirstOrDefault());
                }

                clientes = clientes.OrderBy(r => r.NomeCompleto).ToList();

                return Json(clientes.Select(i => new { i.Email, i.NomeCompleto, i.ID, i.Documento, i.DocumentoFormatado }).ToList());
            }
            else
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult GetCliente(string documento)
        {
            var cliente = clienteFloriculturaRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim()).FirstOrDefault();
            if (cliente != null)
                return Json(new { Tipo = "CLIENTE", Nome = cliente.Nome.ToUpper(), Email = cliente.Email.ToLower(), TelefoneContato = cliente.TelefoneContato, CelularContato = cliente.CelularContato, CEP = cliente.CEP, EstadoID = cliente.Estado.ID, CidadeID = cliente.Cidade.ID, Logradouro = cliente.Logradouro, Numero = cliente.Numero, Complemento = cliente.Complemento, Bairro = cliente.Bairro,  RazaoSocial = cliente.RazaoSocial });
            else
                return Json("");
        }

        [HttpPost]
        public JsonResult GetNomeCliente(int? IdCliente)
        {
            return Json(clienteFloriculturaRepository.Get((int)IdCliente).Nome);
        }
    }
}
