﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.IO;
using System.Collections.Specialized;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    //=================================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos|Financeiro", "/Home/AccessDenied")]
    public class ProdutoController : Controller
    {
        #region Variáveis
        //-----------------------------------------------------------------------------
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Flor> florRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Coloracao> coloracaoRepository;
        private IPersistentRepository<ProdutoFlor> produtoFlorRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<ProdutoRelacionamento> produtoRelacionamentoRepository;
        private IPersistentRepository<ProdutoColoracao> produtoColoracaoRepository;
        private IPersistentRepository<ProdutoSemelhante> produtoSemelhanteRepository;
        private IPersistentRepository<OrigemSite> origemSiteRepository;
        private IPersistentRepository<ProdutoOrigemSite> produtoOrigemSiteRepository;
        private IPersistentRepository<Produto_X_ProdutoFaturamento> produto_X_ProdutoFaturamento;
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public ProdutoController(ObjectContext context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            florRepository = new PersistentRepository<Flor>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            coloracaoRepository = new PersistentRepository<Coloracao>(context);
            produtoFlorRepository = new PersistentRepository<ProdutoFlor>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            produtoRelacionamentoRepository = new PersistentRepository<ProdutoRelacionamento>(context);
            produtoColoracaoRepository = new PersistentRepository<ProdutoColoracao>(context);
            produtoSemelhanteRepository = new PersistentRepository<ProdutoSemelhante>(context);
            origemSiteRepository = new PersistentRepository<OrigemSite>(context);
            produtoOrigemSiteRepository = new PersistentRepository<ProdutoOrigemSite>(context);
            produto_X_ProdutoFaturamento = new PersistentRepository<Produto_X_ProdutoFaturamento>(context);
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region GET
        //-----------------------------------------------------------------------------
        [HttpGet]
        public ActionResult List(int? tipo) //Solicitação de listagem por tipo de produtos ticket812
        {
            List<Produto> produtos = null;            
            string _titulo = null;
            string _origem = null;

            if (tipo != 0 && tipo!= null)
            {
                 produtos = produtoRepository.GetByExpression(c => c.TipoID == tipo).OrderBy(p => p.Nome).ToList();

                if (!String.IsNullOrEmpty(Request.QueryString["titulo"]))
                {
                    _titulo = Request.QueryString["titulo"];
                    if (!String.IsNullOrEmpty(_titulo))
                        produtos = produtos.Where(c => c.Nome.ToLower().Contains(_titulo.ToLower())).ToList();
                }

                if (!String.IsNullOrEmpty(Request.QueryString["origem"]))
                {
                    _origem = Request.QueryString["origem"];
                    if (!String.IsNullOrEmpty(_origem))
                        produtos = produtos.Where(c => c.ProdutoOrigemSites.Count(e => e.OrigemSite.Titulo.ToLower().Contains(_origem.ToLower())) > 0).ToList();
                }
            }
            else
            {
                produtos = produtoRepository.GetAll().OrderBy(p => p.Nome).ToList();

                if (!String.IsNullOrEmpty(Request.QueryString["titulo"]))
                {
                     _titulo = Request.QueryString["titulo"];
                    if (!String.IsNullOrEmpty(_titulo))
                        produtos = produtos.Where(c => c.Nome.ToLower().Contains(_titulo.ToLower())).ToList();
                }

                if (!String.IsNullOrEmpty(Request.QueryString["origem"]))
                {
                    _origem = Request.QueryString["origem"];
                    if (!String.IsNullOrEmpty(_origem))
                        produtos = produtos.Where(c => c.ProdutoOrigemSites.Count(e => e.OrigemSite.Titulo.ToLower().Contains(_origem.ToLower())) > 0).ToList();
                }
            }
            
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = produtos.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                produtos = produtos.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.OrigemSite = origemSiteRepository.GetAll(); 
            return View(produtos);
        }
        //-----------------------------------------------------------------------------
        //public ActionResult List()
        //{
        //    var resultado = produtoRepository.GetAll().OrderBy(p => p.Nome).ToList();

        //    var titulo = Request.QueryString["titulo"];
        //    if (!String.IsNullOrEmpty(titulo))
        //        resultado = resultado.Where(c => c.Nome.ToLower().Contains(titulo.ToLower())).ToList();

        //    var origem = Request.QueryString["origem"];
        //    if (!String.IsNullOrEmpty(origem))
        //        resultado = resultado.Where(c => c.ProdutoOrigemSite.Count(e => e.OrigemSite.Titulo.ToLower().Contains(origem.ToLower())) > 0).ToList();

        //    //PAGINAÇÃO
        //    var paginaAtual = 0;
        //    Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
        //    if (paginaAtual == 0)
        //        paginaAtual = 1;

        //    var totalItens = resultado.Count;
        //    var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
        //    if (totalPaginas > 1)
        //    {
        //        resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
        //    }
        //    ViewBag.PaginaAtual = paginaAtual;
        //    ViewBag.TotalItens = totalItens;
        //    ViewBag.TotalPaginas = totalPaginas;
        //    ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

        //    ViewBag.OrigemSite = origemSiteRepository.GetAll();
        //    return View(resultado);
        //}
        //-----------------------------------------------------------------------------
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Produto produto = null;
            if (id.HasValue)
            {
                produto = produtoRepository.Get(id.Value);
                try
                {
                    var origens = produto.ProdutoOrigemSites.Select(c => c.OrigemSite).OrderBy(e => e.Titulo).ToList();
                    var todasorigens = new List<OrigemSite>();
                    foreach (var origem in origemSiteRepository.GetAll().OrderBy(e => e.Titulo).ToList())
                    {
                        if (origens.Count(c => c.ID == origem.ID) == 0)
                            todasorigens.Add(origem);
                    }
                    ViewBag.Origens = origens;
                    ViewBag.TodasOrigens = todasorigens;
                }
                catch
                {
                    ViewBag.Origens = new List<OrigemSite>();
                    ViewBag.TodasOrigens = origemSiteRepository.GetAll().OrderBy(e => e.Titulo).ToList();
                }
            }
            else
            {
                ViewBag.Origens = new List<OrigemSite>();
                ViewBag.TodasOrigens = new List<OrigemSite>();
            }
            ViewBag.Coloracoes = coloracaoRepository.GetAll().OrderBy(f => f.Nome);
            ViewBag.Flores = florRepository.GetAll().OrderBy(f => f.Nome);
            ViewBag.Tamanhos = tamanhoRepository.GetAll().OrderBy(f => f.Nome);
            ViewBag.Relacionamentos = relacionamentoRepository.GetAll();

            #region PRODUTO X PRODUTO FATURAMENTO

            if (produto != null) {
                var LstProdutos = produto_X_ProdutoFaturamento.GetByExpression(R => R.ProdutoID == produto.ID).ToList();
                string TblProdutoXFaturamento = "";
                foreach (var item in LstProdutos)
                {
                    TblProdutoXFaturamento += "<tr><td rel='" + item.ProdutoFaturamentoID + "'>" + item.ProdutoFaturamento.Codigo + "</td><td>" + item.ProdutoFaturamento.NomeProdutoFaturamento + "</td><td>" + item.ProdutoFaturamento.NCM.CodigoNCM + "</td><td>" + item.ValorUnitario.ToString("C2") + "</td><td>" + item.ValorTotal.ToString("C2") + "</td><td>" + item.ValorDesconto.ToString("C2") + "</td><td>" + item.Qtd + "</td><td><input type='button' class='mws-button red big removerProdutoFaturamento' value='Remover'></td></tr>";
                }

                ViewBag.TblProdutoXFaturamento = TblProdutoXFaturamento;
            }

            #endregion

            decimal PrecoProduto = 0;

            if(produto != null)
            {
                if (produto.Tipo == Produto.Tipos.Kitsbebe && produto.ProdutoTamanhoes.Count == 1)
                {
                    PrecoProduto = produto.ProdutoTamanhoes.First().Preco;
                }
            }

            ViewBag.ValorProduto = PrecoProduto;

            return View(produto);
        }
        //-----------------------------------------------------------------------------
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Produto produto = null;
            if (id > 0)
            {
                produto = produtoRepository.Get(id);
                produtoRepository.Delete(produto);
                //Deletar fotos
                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "DeleteFailure" });
            }
        }
        ////-----------------------------------------------------------------------------
        //[HttpGet]
        //public ActionResult Semelhantes(int produtoid)
        //{
        //    Produto produto = produtoRepository.Get(produtoid);
        //    ViewBag.Produto = produto;
        //    return View(produto.sem.ToList());
        //}
        //-----------------------------------------------------------------------------
        [HttpGet]
        public ActionResult EditSemelhante(int produtoID)
        {
            Produto produto = produtoRepository.Get(produtoID);
            ViewBag.Produtos = produtoRepository.GetByExpression(c => c.ID != produtoID).ToList();
            ViewBag.Produto = produto;
            return View();
        }
        //-----------------------------------------------------------------------------
        [HttpGet]
        public ActionResult DeleteSemelhante(int id)
        {
            ProdutoSemelhante produto = null;
            if (id > 0)
            {
                produto = produtoSemelhanteRepository.Get(id);
                var produtoID = produto.ProdutoID;
                produtoSemelhanteRepository.Delete(produto);

                return RedirectToAction("Semelhantes", new { produtoid = produtoID, cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List");
            }
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region POST
        //-----------------------------------------------------------------------------
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Produto produto)
        {
            produto.RotuloUrl = Domain.Core.Funcoes.GeraRotuloMVC(produto.Nome).ToLower().Trim();
            if (produto.ID == 0)
                produto.Foto = " ";

            produtoRepository.Save(produto);

            foreach (var origemsite in produto.ProdutoOrigemSites.ToList())
                produtoOrigemSiteRepository.Delete(origemsite);

            if (Request.Form["box2View"] != null)
            {
                foreach (var id in Request.Form["box2View"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        produto.ProdutoOrigemSites.Add(new ProdutoOrigemSite() { OrigemSiteID = Convert.ToInt32(_id), ProdutoID = produto.ID });
                    }
                }
                produtoRepository.Save(produto);
            }

            var foto = Request.Files["File"];
            if (foto.ContentLength > 0)
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/produtos/" + produto.ID + "/thumb"));
                if (!dir.Exists)
                {
                    dir.Create();
                }

                dir = new DirectoryInfo(Server.MapPath("/content/produtos/" + produto.ID + "/big"));
                if (!dir.Exists)
                {
                    dir.Create();
                }

                //try
                //{

                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);

                //FOTO
                ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/produtos/" + produto.ID + "/big/" + nomefoto), new ImageResizer.ResizeSettings(
                                        "width=600&height=667&crop=auto;format=jpg;mode=pad;scale=both"));

                i.CreateParentDirectory = true;
                i.Build();

                produto.Foto = nomefoto;
                produtoRepository.Save(produto);

                //NORMAL
                i = new ImageResizer.ImageJob(Server.MapPath("/content/produtos/" + produto.ID + "/big/" + produto.Foto), Server.MapPath("/content/produtos/" + produto.ID + "/" + produto.Foto), new ImageResizer.ResizeSettings(
                                        "width=324&height=360&crop=auto;format=jpg;mode=pad;scale=canvas"));

                i.CreateParentDirectory = true;
                i.Build();


                //THUMB
                i = new ImageResizer.ImageJob(Server.MapPath("/content/produtos/" + produto.ID + "/big/" + produto.Foto), Server.MapPath("/content/produtos/" + produto.ID + "/thumb/" + produto.Foto), new ImageResizer.ResizeSettings(
                                        "width=200&height=222&crop=auto;format=jpg;mode=pad;scale=canvas"));

                i.CreateParentDirectory = true;
                i.Build();
                //}
                //catch { }
            }

            foreach (var produtoflor in produto.ProdutoFlors.ToList())
                produtoFlorRepository.Delete(produtoflor);


            if (Request.Form["flores"] != null)
            {
                foreach (var id in Request.Form["flores"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        produto.ProdutoFlors.Add(new ProdutoFlor() { FlorID = Convert.ToInt32(_id), ProdutoID = produto.ID });
                    }
                }
            }

            foreach (var produtocoloracao in produto.ProdutoColoracaos.ToList())
                produtoColoracaoRepository.Delete(produtocoloracao);


            if (Request.Form["coloracoes"] != null)
            {
                foreach (var id in Request.Form["coloracoes"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        produto.ProdutoColoracaos.Add(new ProdutoColoracao() { ColoracaoID = Convert.ToInt32(_id), ProdutoID = produto.ID });
                    }
                }
            }

            foreach (var produtorelacionamento in produto.ProdutoRelacionamentoes.ToList())
                produtoRelacionamentoRepository.Delete(produtorelacionamento);


            if (Request.Form["relacionamentos"] != null)
            {
                foreach (var id in Request.Form["relacionamentos"].Split(','))
                {
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        produto.ProdutoRelacionamentoes.Add(new ProdutoRelacionamento() { RelacionamentoID = Convert.ToInt32(_id), ProdutoID = produto.ID });
                    }
                }
            }
            produtoRepository.Save(produto);

            #region PRODUTO X FATURAMENTO DELETE

            var LstProdutos = produto_X_ProdutoFaturamento.GetByExpression(R => R.ProdutoID == produto.ID).ToList();
            foreach (var item in LstProdutos)
            {
                produto_X_ProdutoFaturamento.Delete(item);
            }

            produtoRepository.Save(produto);

            #endregion

            #region PRODUTOS X FATURAMENTO

            if (!string.IsNullOrEmpty(Request.Form["ValoresProdutoFaturamento"]))
            {
                var StrProdutosFaturamento = Request.Form["ValoresProdutoFaturamento"];

                foreach (var Item in StrProdutosFaturamento.Split('|'))
                {
                    var CodProdutoFaturamento = int.Parse(Item.Split(',')[0]);
                    var QTD = int.Parse(Item.Split(',')[1]);
                    var ValorUni = Item.Split(',')[2].Replace(".", ",");
                    var ValorDesconto = Item.Split(',')[3].Replace(".", ",");

                    Produto_X_ProdutoFaturamento NovoProduto_X_ProdutoFaturamento = new Produto_X_ProdutoFaturamento();
                    NovoProduto_X_ProdutoFaturamento.ProdutoID = produto.ID;
                    NovoProduto_X_ProdutoFaturamento.ProdutoFaturamentoID = CodProdutoFaturamento;

                    NovoProduto_X_ProdutoFaturamento.Qtd = QTD;
                    NovoProduto_X_ProdutoFaturamento.ValorUnitario = decimal.Parse(ValorUni);
                    NovoProduto_X_ProdutoFaturamento.ValorDesconto = decimal.Parse(ValorDesconto);
                    NovoProduto_X_ProdutoFaturamento.ValorTotal = (decimal.Parse(ValorUni) - decimal.Parse(ValorDesconto)) * QTD;

                    produto_X_ProdutoFaturamento.Save(NovoProduto_X_ProdutoFaturamento);
                }
            }

            #endregion

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }
        //-----------------------------------------------------------------------------
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditSemelhante(ProdutoSemelhante produto)
        {
            produtoSemelhanteRepository.Save(produto);

            return RedirectToAction("Semelhantes", new { produtoid = produto.ProdutoID, cod = "SaveSucess" });
        }
        //-----------------------------------------------------------------------------
        #endregion
    }
    //=================================================================================
}
