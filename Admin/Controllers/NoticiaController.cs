﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.IO;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Comunicacao", "/Home/AccessDenied")]
    public class NoticiaController : Controller
    {
        private IPersistentRepository<Noticia> noticiaRepository;

        public NoticiaController(ObjectContext context)
        {
            noticiaRepository = new PersistentRepository<Noticia>(context);
        }

        public ActionResult List()
        {
            var resultado = noticiaRepository.GetAll().OrderByDescending(p => p.Data).ToList();
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Noticia noticia = null;
            if (id.HasValue)
            {
                noticia = noticiaRepository.Get(id.Value);
            }
            return View(noticia);
        }

        [HttpPost]
        [ValidateInput(false)]        
        public ActionResult Edit(Noticia noticia)
        {
            noticia.RotuloUrl = Domain.Core.Funcoes.GeraRotuloMVC(noticia.Titulo).ToLower().Trim();
            if (noticia.ID == 0)
                noticia.Foto = "";
            try
            {
                noticiaRepository.Save(noticia);

                var foto = Request.Files["File"];
                if (foto.ContentLength > 0)
                {
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/noticias/" + noticia.ID + "/thumb"));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }

                    //try
                    //{

                    var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                    //FOTO
                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/noticias/" + noticia.ID + "/" + nomefoto), new ImageResizer.ResizeSettings(
                                            "width=500&height=500&crop=auto;format=jpg;mode=pad;scale=both"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    noticia.Foto = nomefoto;
                    noticiaRepository.Save(noticia);

                    //THUMB
                    i = new ImageResizer.ImageJob(Server.MapPath("/content/noticias/" + noticia.ID + "/" + noticia.Foto), Server.MapPath("/content/noticias/" + noticia.ID + "/thumb/" + noticia.Foto), new ImageResizer.ResizeSettings(
                                            "width=400&height=400&crop=auto;format=jpg;mode=pad;scale=both"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    //}
                    //catch { }
                }
            }
            catch { return RedirectToAction("List", new { cod = "SaveFailure" }); }

            return RedirectToAction("Edit", new { id= noticia.ID, cod = "SaveSucess" });
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Noticia noticia = noticiaRepository.Get(id);
            if (noticia != null)
            {
                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/noticias/"+noticia.ID));
                if(dir.Exists)
                    dir.Delete(true);
                noticiaRepository.Delete(noticia);
            }

            return RedirectToAction("List", new { cod = "DeleteSucess" });
        }
    }
}
