﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;
using System.Data.SqlTypes;

namespace Admin.Controllers {
    //==========================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro", "/Home/AccessDenied")]
    public class PedidosSiteController : CrudController<Pedido>
    {

        #region Variaveis
        private IPersistentRepository<Cliente> clienteRepository;
        private PedidoRepository pedidoRepository;        
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private PedidoService pedidoService;
        private IPersistentRepository<Promocao> promocaoRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<PedidoNota> pedidoNotaRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Local> localRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private FornecedorPushRepository fornecedorPushRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<FornecedorLocal> fornecedorlocalRepository;
        private IPersistentRepository<OrigemSite> origemSiteRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<StatusCancelamento> statusCancelamentoRepository;
        private IPersistentRepository<ListaNegra> blackListRepository;
        private IPersistentRepository<PedidoFloricultura> pedidoFloriculturaRepository;
        private IPersistentRepository<PedidoFloriculturaNota> pedidoFloriculturaNotaRepository;
        private IPersistentRepository<ClienteFloricultura> ClienteFloriculturaRepository;
        private IPersistentRepository<PedidoFloriculturaOrigem> PedidoFloriculturaOrigemRepository;
        private IPersistentRepository<ProdutoFloricultura> produtoFloriculturaRepository;
        private IPersistentRepository<PedidoItemFloricultura> PedidoItemFloriculturaRepository;
        private IPersistentRepository<ProdutoTamanhoPrecoFloricultura> ProdutoTamanhoPrecoFloriculturaRepository;
        private AdminModel adminModel;
        #endregion
        
        #region Construtor
        //----------------------------------------------------------------------
        public PedidosSiteController(ObjectContext context)
            : base(context) {
            administradorRepository = new PersistentRepository<Administrador>(context);
            promocaoRepository = new PersistentRepository<Promocao>(context);            
            clienteRepository = new PersistentRepository<Cliente>(context);
            pedidoRepository = new PedidoRepository(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            pedidoService = new PedidoService(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            pedidoNotaRepository = new PersistentRepository<PedidoNota>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            localRepository = new PersistentRepository<Local>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            fornecedorPushRepository = new FornecedorPushRepository(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            npsREspository = new PersistentRepository<NPSRating>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            origemSiteRepository = new PersistentRepository<OrigemSite>(context);
            fornecedorlocalRepository = new PersistentRepository<FornecedorLocal>(context);
            statusCancelamentoRepository = new PersistentRepository<StatusCancelamento>(context);
            blackListRepository = new PersistentRepository<ListaNegra>(context);
            pedidoFloriculturaRepository = new PersistentRepository<PedidoFloricultura>(context);
            ClienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
            PedidoFloriculturaOrigemRepository = new PersistentRepository<PedidoFloriculturaOrigem>(context);
            produtoFloriculturaRepository = new PersistentRepository<ProdutoFloricultura>(context);
            PedidoItemFloriculturaRepository = new PersistentRepository<PedidoItemFloricultura>(context);
            ProdutoTamanhoPrecoFloriculturaRepository = new PersistentRepository<ProdutoTamanhoPrecoFloricultura>(context);
            pedidoFloriculturaNotaRepository = new PersistentRepository<PedidoFloriculturaNota>(context);
            adminModel = new AdminModel(context);
        
        }
        //----------------------------------------------------------------------    
        #endregion

        #region HTTPGET
        //----------------------------------------------------------------------
        public override ActionResult List(params object[] args) {

            int[] origensPrincipais = new[] { 21, 3, 6, 10, 23, 27, 34, 24, 7, 14, 12, 25, 38 };
            List<OrigemSite> lstOrigens = new List<OrigemSite>();
            var origens = origemSiteRepository.GetAll().ToList();
            foreach (int idOrigem in origensPrincipais)
            {
                lstOrigens.Add(origens.FirstOrDefault(o => o.ID == idOrigem));
            }
            ViewBag.OrigemSite = new SelectList(lstOrigens, "Titulo", "Titulo");


            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.PromocaoSemana = promocao;

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<Pedido> lstPedidos = new List<Pedido>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            lstPedidos = pedidoRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal).OrderByDescending(p => p.DataCriacao).ToList();

            //var origem = 
            // var origemSite = 
            var nome = Request.QueryString["nome"];
            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var PossuiFoto = Request.QueryString["PossuiFoto"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var cupomID = 0;
            Int32.TryParse(Request.QueryString["cupomID"], out cupomID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var statusentrega = 0;
            Int32.TryParse(Request.QueryString["statusentrega"], out statusentrega);

            var meiopagamento = 0;
            Int32.TryParse(Request.QueryString["meiopagamento"], out meiopagamento);

            if (!String.IsNullOrEmpty(numero))
                lstPedidos = lstPedidos.Where(c => c.Numero.Contains(numero.ToUpper())).ToList();
            else {
                if (!String.IsNullOrEmpty(nome))
                    lstPedidos = lstPedidos.Where(c => c.Cliente.Nome.ToLower().Contains(nome.ToLower()) || c.Cliente.RazaoSocial.ToLower().Contains(nome.ToLower())).ToList();

                if (!String.IsNullOrEmpty(email))
                    lstPedidos = lstPedidos.Where(c => c.Cliente.Email.Contains(email)).ToList();

                if (clienteID > 0)
                    lstPedidos = lstPedidos.Where(c => c.ClienteID == clienteID).ToList();

                if (cupomID > 0)
                    lstPedidos = lstPedidos.Where(c => c.CupomID == cupomID).ToList();

                if (status > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusProcessamentoID == status).ToList();

                if (statuspagamento > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusPagamentoID == statuspagamento).ToList();

                if (statusentrega > 0)
                    lstPedidos = lstPedidos.Where(c => c.StatusEntregaID == statusentrega).ToList();

                if (meiopagamento > 0)
                    lstPedidos = lstPedidos.Where(c => c.MeioPagamentoID == meiopagamento).ToList();

                if (!String.IsNullOrEmpty(PossuiFoto)) {
                    if (PossuiFoto == "SIM")
                        lstPedidos = lstPedidos.Where(p => p.PedidoItens.Where(c => c.FotoEntrega != "").Any()).ToList();

                    if (PossuiFoto == "NAO")
                        lstPedidos = lstPedidos.Where(p => p.PedidoItens.Where(c => c.FotoEntrega == "").Any()).ToList();
                }

            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstPedidos.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1) {
                lstPedidos = lstPedidos.Skip((paginaAtual - 1) * Configuracoes.ITENS_PAGINA_ADMIN).Take(Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(lstPedidos);
        }
        //----------------------------------------------------------------------
        public ActionResult Novo() {
            Cliente cliente = new Cliente();
            var estados = estadoRepository.GetAll().ToList();
            var cidades = cidadeRepository.GetAll().ToList();
            int[] principaisEstados = new[]{26, 11, 19, 18, 7, 23, 9, 16};
            List<Estado> lstEstados = new List<Estado>();
            foreach (int idEstado in principaisEstados) {
                lstEstados.Add(estados.FirstOrDefault(e=>e.ID == idEstado));
            }
            lstEstados.AddRange(estados.Where(e=> !principaisEstados.Contains(e.ID)));

            ViewBag.Estados = new SelectList(lstEstados, "ID", "Sigla");
            ViewBag.Cidades = new SelectList(cidades, "ID", "nome"); 

            var produtos = produtoRepository.GetByExpression(c => c.Disponivel || c.TipoID == (int)Produto.Tipos.Personalizado).OrderBy(c => c.TipoID).OrderBy(c => c.Nome).ToList();
            ViewBag.Produtos = produtos;

            ViewBag.Configuracoes = configuracaoRepository.GetAll().FirstOrDefault();
            int[] origensPrincipais = new[]{21, 3, 6, 10, 23, 27, 34, 24, 7, 14, 12, 25, 38 };
            List<OrigemSite> lstOrigens = new List<OrigemSite>();
            var origens = origemSiteRepository.GetAll().ToList();
            foreach (int idOrigem in origensPrincipais){
                lstOrigens.Add(origens.FirstOrDefault(o=>o.ID == idOrigem));
            }
            ViewBag.OrigemSite = new SelectList(lstOrigens, "Titulo", "Titulo");

            if (!String.IsNullOrEmpty(Request.QueryString["documento"]))
            {
                var documento = Request.QueryString["documento"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();
                cliente = clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == documento).FirstOrDefault();
                if (cliente != null)
                {
                    var estad = cliente.EstadoID;
                    var cid = cliente.CidadeID;


                    ViewBag.ClienteID = cliente.ID;
                    ViewBag.Pedidos = cliente.Pedidos.OrderByDescending(c => c.DataCriacao).Take(5);
                }
            }
            return View(cliente);
        }
        //----------------------------------------------------------------------

        public ActionResult Pagar(int id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);

            if(pedido.MeioPagamentoID != (int)Pedido.MeiosPagamento.PagarMe || pedido.StatusCancelamentoID != null || pedido.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.Pago)
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Erro!" });
            }

            return View(pedido);
        }

        public JsonResult ProcessarPagamento(int PedidoID, string hash)
        {
            var pedido = pedidoService.PagarPedidoADM(PedidoID, hash);

            if (pedido != null && pedido.Retorno == "Paid")
            {
                return Json("Paid");
            }
            else
            {
                return Json("Não foi possível criar seu pedido. Verifique os dados preenchidos.");
            }
        }

        public ActionResult Visualizar(int id)
        {
            List<Administrador> lstUserPush = new List<Administrador>();
            List<AdministradorPedido> lstAcaoPush = new List<AdministradorPedido>();
            List<FornecedorPush> lstFornPush = new List<FornecedorPush>();
            List<FornecedorLocal> lstFornecedorLocal = new List<FornecedorLocal>();
            List<int> IdsToRemove = new List<int>();

            ViewBag.Admin = adminModel.CurrentAdministrador;
            var pedido = pedidoRepository.Get(id);

            if (pedido != null)
            {
                var ResultLeilao = pedidoService.PedidoEmLeilao(id, OrigemPedido.Cliente);
                ViewBag.TravaFornecedores = ResultLeilao;

                if (ResultLeilao)
                {
                    
                    var ListaFornecedores = pedido.Local.FornecedorLocal.OrderBy(r => r.Ordem).ToList();

                    if (ListaFornecedores.Count > 0)
                    {
                        foreach (var forn in ListaFornecedores)
                        {
                            var DentroDoHorarioDeEntrega = pedidoService.DentroHorarioEntregaLeilao(pedido.ID, forn.Fornecedor.ID, OrigemPedido.Cliente);
                            var MeuIntervalo = pedidoService.RetornaIntervaloDeEntrega(pedido.ID, forn.Fornecedor.ID, OrigemPedido.Cliente);
                            var FornecedorRecusouaEntrega = pedidoService.FornecedoresRecusaramEntrega(pedido.ID, OrigemPedido.Cliente).Where(r => r.Key == forn.Fornecedor.ID).First().Value;
                            var FornecedoresAcimaDescartados = pedidoService.FornecedoresAcimaDescartados(forn.Fornecedor.ID, pedido.ID, OrigemPedido.Cliente);

                            List<int> IdsFornecedoresAbaixoComPedido = new List<int>();

                            var IntervaloPorFornecedor = pedidoService.IntervaloPorFornecedor(pedido.ID, forn.Fornecedor.ID, OrigemPedido.Cliente);

                            foreach (var Iten in IntervaloPorFornecedor)
                            {
                                bool EstaComPedido = bool.Parse(Iten.Value.Split('|')[0]);
                                int Fornecedor = int.Parse(Iten.Value.Split('|')[1]);
                                int Ordem = int.Parse(Iten.Value.Split('|')[2]);

                                if (EstaComPedido == true && Ordem > pedido.Local.FornecedorLocal.Where(r => r.FornecedorID == forn.Fornecedor.ID).FirstOrDefault().Ordem)
                                {
                                    IdsFornecedoresAbaixoComPedido.Add(Fornecedor);
                                }
                            }

                            if (!FornecedoresAcimaDescartados)
                            {
                                ViewBag.FornecedorAtual = forn.Fornecedor;
                            }

                            if (pedidoService.LeilaoExpirado(pedido.ID, OrigemPedido.Cliente) || FornecedorRecusouaEntrega == true)
                            {
                                // NAO MOSTAR
                                IdsToRemove.Add(pedido.ID);
                            }
                            else
                            {
                                // SE ESTIVER DENTOR DO HORARIO DE ENTREGA OU SE OS FORNECEDORES ACIMA FORAM DESCARTADO
                                if ((DentroDoHorarioDeEntrega || FornecedoresAcimaDescartados) && IdsFornecedoresAbaixoComPedido.Count == 0)
                                {
                                    // MOSTRAR
                                }
                                else
                                {
                                    // NAO MOSTRAR
                                    IdsToRemove.Add(pedido.ID);
                                }
                            }

                            pedido.RepasseId = pedido.PedidoItens.FirstOrDefault().RepasseAtual.ID;

                        }

                        // REMOVES OS PEDIDOS DO CPV QUE JÁ PASSARAM DO PRAZO
                        foreach (var Iten in IdsToRemove)
                        {
                            var REsult = pedido.ID == Iten;
                            //pedido.Remove(REsult);
                        }
                    }
                }

                var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
                ViewBag.PromocaoSemana = promocao;

                    //TrustvoxService.AtualizaPedido(pedido.ID);
                    //TESTE>pedido.EnviarLembreteVencimentoBoleto(); pedido.EnviarEmailPagamento();

                    //var fornecedorPush = fornecedorPushRepository.GetByExpression(c=>c.PedidoID == pedido.ID).OrderByDescending( c=>c.DataCriacao).FirstOrDefault();

                    var pedidosEmPush = fornecedorPushRepository.GetByExpression(c => c.PedidoID == pedido.ID).OrderByDescending(c => c.DataCriacao).ToList();

                    if (pedidosEmPush != null && pedidosEmPush.Count() > 0)
                    {
                        foreach (var push in pedidosEmPush)
                        {
                            var _fornecedor = fornecedorRepository.Get((int)push.IdFornecedor);

                            push.NomeFornecedor = _fornecedor.Nome;
                            push.TelefonePrimario = _fornecedor.TelefonePrimario;

                            var fornPush = pedidosEmPush.OrderByDescending(c => c.DataCriacao).FirstOrDefault();

                            var userPush = administradorRepository.GetByExpression(a => a.ID == push.AdministradorID && a.Liberado).FirstOrDefault();

                            var acaoPush = administradorPedidoRepository.GetByExpression
                           (
                               r => r.Pedido.ID == id
                               && (r.AcaoID == (int)AdministradorPedido.Acao.RecusouEntrega
                               || r.AcaoID == (int)AdministradorPedido.Acao.AceitouEntrega)
                           )
                           .OrderBy(c => c.DataCriacao).FirstOrDefault();

                            lstFornPush.Add(fornPush);
                            lstUserPush.Add(userPush);
                            lstAcaoPush.Add(acaoPush);
                        }
                    }

                    ViewBag.StatusEntregaPedido = pedido.StatusEntrega;

                    ViewBag.QtdPedidosPush = lstFornPush.Count();
                    ViewBag.FornecedorPush = lstFornPush;
                    ViewBag.ListaPedidosPush = pedidosEmPush;
                    ViewBag.UsuarioPushPedido = lstUserPush;
                    ViewBag.AcoesPushPedido = lstAcaoPush;

                    ViewBag.Produtos = produtoRepository.GetByExpression(c => c.Disponivel).ToList();
                    var estados = estadoRepository.GetAll().ToList();
                    int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                    List<Estado> lstEstados = new List<Estado>();
                    foreach (int idEstado in principaisEstados)
                    {
                        lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                    }
                    lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                    ViewBag.Estados = lstEstados;
                    ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == pedido.EstadoID).OrderBy(e => e.Nome);

                    //var ResultLocais = localREspository.GetByExpression(r => r.CidadeID == pedido.CidadeID).OrderBy(r => r.Titulo).ToList().Select(r => new SelectListItem { Value = r.ID.ToString(), Text = r.Titulo + " - " + r.Tipo + " - " + r.Telefone }).ToList();
                    var ResultLocais = localRepository.GetByExpression(r => r.CidadeID == pedido.CidadeID).OrderBy(r => r.Titulo).ToList().Select(r => new SelectListItem { Value = r.ID.ToString(), Text = r.Titulo }).ToList();

                    //ResultLocais.Insert(0, new SelectListItem { Text = "LOCAL NÃO CADASTRADO", Value = "NULL" });

                    ViewBag.Locais = ResultLocais;

                    ViewBag.StatusCancelamento = statusCancelamentoRepository.GetByExpression(s => s.Ativo.HasValue && s.Ativo.Value).OrderBy(s => s.Titulo).ToList();
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante"));
                    if (dir.Exists)
                        ViewBag.Comprovantes = dir.GetFiles().ToList();

                    var blackList = blackListRepository.GetByExpression(b => b.Documento.Replace(".", "").Replace("-", "").Replace("/", "") == pedido.Cliente.Documento.Replace(".", "").Replace("-", "").Replace("/", "")).FirstOrDefault();
                    ViewBag.ClienteBlackList = blackList;

                    foreach (var Item in pedido.AdministradorPedido.Where(r => r.IdFornecedor != null).ToList())
                    {
                        var Result = fornecedorlocalRepository.GetByExpression(r => r.FornecedorID == (int)Item.IdFornecedor).FirstOrDefault();

                        if (Result != null)
                        {
                            Item.NomeFornecedor = Result.Fornecedor.Nome;
                        }

                    }
                    return View(pedido);
                }
                else
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Pedido inexistente!" });
            }
        //----------------------------------------------------------------------
        public ContentResult Boleto(int id) {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                if (pedido.Boleto != null) {
                    var html = BoletoService.Gerar(pedido.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, pedido.Numero + "_boleto.pdf");
                    return Content("");
                }
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
        //----------------------------------------------------------------------
        public ContentResult MontaBoleto(int id) {
            var boleto = boletoRepository.Get(id);
            if (boleto != null) {
                var html = BoletoService.Gerar(boleto);
                Domain.Core.Funcoes.ImprimirPDF(html, boleto.Pedido.Numero + "_boleto.pdf");
                return Content("");
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
        //----------------------------------------------------------------------
        //public void BoletoPDF(int id)
        //{
        //    var pedido = pedidoRepository.Get(id);
        //    if (pedido != null)
        //    {
        //        if (pedido.Boleto != null)
        //        {
        //            var html = BoletoService.Gerar(pedido.Boleto); 
        //            var pdf = Domain.Core.Funcoes.ImprimirPDF(html, id);

        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.AddHeader("Content-Disposition", "inline;filename=" + pdf);
        //            Response.ContentType = "application/pdf";
        //            Response.WriteFile(pdf);
        //            Response.Flush();
        //            Response.Clear();
        //        }
        //    }
        //}
        //----------------------------------------------------------------------
        public JsonResult Estornar(int id, string valor, string data) {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                var dataestorno = new DateTime();
                DateTime.TryParse(data, out dataestorno);

                decimal valorestorno = 0;
                Decimal.TryParse(valor, out valorestorno);

                if (dataestorno != new DateTime())
                    pedido.DataEstorno = dataestorno;

                if (valorestorno > 0)
                    pedido.ValorEstorno = valorestorno;

                var statuspagamentoid = pedido.StatusPagamentoID;
                pedido.StatusPagamento = Pedido.TodosStatusPagamento.Estornado;
                pedidoRepository.Save(pedido);

                var historico = "Pedido alterado por " + adminModel.Nome + "<br>" + "<b>Status Pagamento: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((Pedido.TodosStatusPagamento)statuspagamentoid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusPagamento) + "</i>"; ;
                var nota2 = new PedidoNota();
                nota2.DataCriacao = DateTime.Now;
                nota2.PedidoID = pedido.ID;
                nota2.Observacoes = historico;
                pedidoNotaRepository.Save(nota2);

                //TODO: Verificar qdo tiver um repasse pago para o fornecedor.

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarProduto(int id, int itemID, int produtoTamanhoID, decimal valor) {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando)) {
                    var produtoanterior = pedido.PedidoItens.Where(c => c.ID == itemID).FirstOrDefault();
                    var nomeproduto = produtoanterior.ProdutoTamanho.Produto.Nome.ToUpper();
                    var valoranterior = produtoanterior.Valor;

                    ////REMOVE PRODUTO ATUAL
                    //pedidoItemRepository.Delete(produtoanterior);

                    //ALTERA PRODUTO
                    var pedidoitem = produtoanterior;
                    pedidoitem.Mensagem = produtoanterior.Mensagem;
                    pedidoitem.Descricao = produtoanterior.Descricao;
                    pedidoitem.Observacoes = produtoanterior.Observacoes;
                    pedidoitem.PedidoID = pedido.ID;
                    pedidoitem.ProdutoTamanhoID = produtoTamanhoID;
                    pedidoitem.Valor = valor;
                    pedidoItemRepository.Save(pedidoitem);
                    pedidoitem.Descricao = pedidoitem.ProdutoTamanho.Produto.Nome;
                    pedidoItemRepository.Save(pedidoitem);


                    //ALTERA PEDIDO
                    decimal valortotal = 0;
                    foreach (var item in pedido.PedidoItens) {
                        valortotal += item.Valor;
                    }
                    pedido.ValorTotal = valortotal;
                    pedidoRepository.Save(pedido);

                    ////REPASSE
                    //if (pedido.FornecedorID.HasValue)
                    //{
                    //    var repasse = new Repasse();
                    //    repasse.FornecedorID = pedido.FornecedorID;
                    //    repasse.PedidoItemID = pedidoitem.ID;
                    //    repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                    //    repasse.AdministradorID = adminModel.ID;
                    //    var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedido.FornecedorID).ToList();
                    //    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                    //    if (fornecedorpreco != null)
                    //    {
                    //        if (fornecedorpreco.Repasse.HasValue)
                    //        {
                    //            if (fornecedorpreco.Repasse.Value > 0)
                    //            {
                    //                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                    //            }
                    //        }
                    //    }
                    //    repasseRepository.Save(repasse);
                    //}

                    //CRIA UMA NOTA
                    var pedidonota = new PedidoNota();
                    pedidonota.PedidoID = pedido.ID;
                    pedidonota.DataCriacao = DateTime.Now;
                    pedidonota.Observacoes = adminModel.Nome + "<br>Produto alterado de " + nomeproduto + " no valor de " + valoranterior.ToString("C2") + " para " + pedidoitem.ProdutoTamanho.Produto.Nome.ToUpper();
                    pedidoNotaRepository.Save(pedidonota);

                    //SE TEM BOLETO
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto) {
                        var boleto = pedido.Boleto;
                        boleto.Processado = false;
                        boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                        if (boleto.RemessasBoleto.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                        boleto.Valor = pedido.ValorTotal;
                        boletoRepository.Save(boleto);
                    }

                    return Json("OK");
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AdicionarProduto(int id, int produtoTamanhoID, decimal valor, string frase) {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando)) {

                    //ADICIONA NOVO PRODUTO
                    var pedidoitem = new PedidoItem();
                    pedidoitem.Mensagem = frase.ToUpper();
                    pedidoitem.Descricao = "";
                    pedidoitem.Observacoes = "";
                    pedidoitem.PedidoID = pedido.ID;
                    pedidoitem.ProdutoTamanhoID = produtoTamanhoID;
                    pedidoitem.Valor = valor;
                    pedidoItemRepository.Save(pedidoitem);
                    pedidoitem.Descricao = pedidoitem.ProdutoTamanho.Produto.Nome;
                    pedidoItemRepository.Save(pedidoitem);

                    //ALTERA PEDIDO
                    pedido.ValorTotal = pedido.ValorTotal + pedidoitem.Valor;

                    if(pedidoitem.ProdutoTamanho.Produto.TipoID != 1)
                    {
                        pedido.LocalID = null;
                    }

                    pedidoRepository.Save(pedido);

                    //REPASSE
                    if (pedido.FornecedorID.HasValue) {
                        var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == pedidoitem.ID).FirstOrDefault();
                        if (repasse == null) {
                            repasse = new Repasse();
                            repasse.FornecedorID = pedido.FornecedorID;
                            repasse.PedidoItemID = pedidoitem.ID;
                            repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                            repasse.AdministradorID = adminModel.ID;
                            repasse.DataCriacao = DateTime.Now;
                            repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                            var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedido.FornecedorID).ToList();
                            var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                            if (fornecedorpreco != null) {
                                if (fornecedorpreco.Repasse.HasValue) {
                                    if (fornecedorpreco.Repasse.Value > 0) {
                                        repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                    }
                                }
                            }
                            repasseRepository.Save(repasse);
                        }
                    }

                    //CRIA UMA NOTA
                    var pedidonota = new PedidoNota();
                    pedidonota.PedidoID = pedido.ID;
                    pedidonota.DataCriacao = DateTime.Now;
                    pedidonota.Observacoes = adminModel.Nome + "<br>Produto " + pedidoitem.ProdutoTamanho.Produto.Nome.ToUpper() + " adicionado no valor de " + pedidoitem.Valor.ToString("C2");
                    pedidoNotaRepository.Save(pedidonota);

                    //SE TEM BOLETO
                    if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto) {
                        var boleto = pedido.Boleto;
                        boleto.Processado = false;
                        boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                        if (boleto.RemessasBoleto.Count > 0)
                            boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                        boleto.Valor = pedido.ValorTotal;
                        boletoRepository.Save(boleto);
                    }

                    return Json("OK");
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult RemoverProduto(int id, int itemID) {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null) {
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento && (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado || pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando)) {
                    var produtoanterior = pedido.PedidoItens.Where(c => c.ID == itemID).FirstOrDefault();
                    if (produtoanterior.RepasseAtual.ID == 0) {
                        var nomeprodutoanterior = produtoanterior.ProdutoTamanho.Produto.Nome.ToUpper();
                        var valorprodutoanterior = produtoanterior.Valor.ToString("C2");
                        //REMOVE PRODUTO ATUAL
                        pedidoItemRepository.Delete(produtoanterior);

                        //ALTERA PEDIDO
                        pedido.ValorTotal = pedido.ValorTotal - produtoanterior.Valor;
                        pedidoRepository.Save(pedido);

                        //CRIA UMA NOTA
                        var pedidonota = new PedidoNota();
                        pedidonota.PedidoID = pedido.ID;
                        pedidonota.DataCriacao = DateTime.Now;
                        pedidonota.Observacoes = adminModel.Nome + "<br>Produto " + nomeprodutoanterior + " no valor de " + valorprodutoanterior + " removido ";
                        pedidoNotaRepository.Save(pedidonota);

                        //SE TEM BOLETO
                        if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto) {
                            var boleto = pedido.Boleto;
                            boleto.Processado = false;
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            if (boleto.RemessasBoleto.Count > 0)
                                boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarDados;
                            boleto.Valor = pedido.ValorTotal;
                            boletoRepository.Save(boleto);
                        }

                        return Json("OK");
                    }
                    else {
                        return Json("O produto não pode ser removido porque possui um repasse em aberto. Cancele o repasse e faça a alteração novamente.");
                    }
                }
                else {
                    return Json("O pedido precisa estar com o status AGUARDANDO PAGAMENTO e ser um NOVO PEDIDO ou estar em PROCESSAMENTO para ter seu produto alterado.");
                }
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarVencimentoBoleto(int id, string data) {
            var pedidoEmp = pedidoRepository.Get(id);
            if (pedidoEmp != null) {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime()) {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedidoEmp.ID, null, null, null, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = pedidoEmp.Boleto;
                    boleto.Processado = false;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                    if (boleto.RemessasBoleto.Count > 0)
                        boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;
                    boleto.DataVencimento = datavencimento;
                    boleto.DataEnvioLembreteVencimento = null;
                    boletoRepository.Save(boleto);

                    return Json("OK");
                }
                else
                    return Json("Data inválida");

            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult FornecedorInfo(int fornecedorID) {
            var fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null)
            {
                return Json(new { retorno = "OK", caracteristicas = fornecedor.Caracteristicas, frete = fornecedor.ValorFrete.ToString("C2"), horario = fornecedor.HorarioFuncionamento, problemas = fornecedor.ResponsavelProblemas, nome = "" + fornecedor.Nome + "", telefones = "" + fornecedor.TelefonePrimario + "<br>" + fornecedor.TelefoneSecundario + "<br>" + fornecedor.TelefonesAdicionais + "", contato = "" + fornecedor.PessoaContato + "", obs = "" + fornecedor.Observacao + "", entrega24h = "" + (fornecedor.Entrega24h ? "SIM" : "NÃO") + "" });
            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult FornecedorInfoPedidos(int fornecedorID)
        {
            List<Repasse> _lstPedidos = new List<Repasse>();
            Repasse _repasse = new Repasse();

            if (fornecedorID != 0)
            {
                var _repasses = repasseRepository.GetByExpression(c => c.FornecedorID == fornecedorID && c.StatusID != (int)Repasse.TodosStatus.Cancelado).Take(5).OrderByDescending(c => c.DataCriacao).ToList();
                if (_repasses != null)
                {
                    //var pedidos = _pedidoFornecedorLocal.OrderBy(c => c.Fornecedor.Nome).OrderBy(c => c.Ordem).Select(c => c.Fornecedor).ToList();
                    foreach (Repasse repasse in _repasses)
                    {
                        if (repasse.PedidoEmpresa != null)
                        {
                            if (repasse.PedidoEmpresa.StatusEntrega == TodosStatusEntrega.PendenteFornecedor)
                            {
                                _lstPedidos.Add(new Repasse
                                {
                                    ID = (int)repasse.PedidoEmpresaID,
                                    AdministradorID = repasse.AdministradorID,
                                    DataCriacao = repasse.DataCriacao,
                                    PedidoItemID = repasse.PedidoItemID,
                                    PedidoItem = repasse.PedidoItem
                                });
                            }
                            else
                            {
                                return Json("Não é possível alterar o processamento de um pedido em operação");
                            }
                        }
                        else
                        {

                            _lstPedidos.Add(new Repasse
                            {

                                AdministradorID = repasse.AdministradorID,
                                DataCriacao = repasse.DataCriacao,
                                PedidoItemID = repasse.PedidoItemID,
                                PedidoItem = repasse.PedidoItem
                            });

                            foreach (var item in _lstPedidos)
                            {
                                item.PedidoItem.TipoFaixaID = (int)PedidoItem.TodosTiposFaixa.PF;
                            }
                        }
                    }
                }
                
                //if (pedido != null)
                //{
                //    return Json(new { retorno = "OK", numeropedidos = pedido.Count(), pedido = pedido.FirstOrDefault().Numero, datacriacao = pedido.FirstOrDefault().DataCriacao.Date.ToString("dd/MM/yyyy"), produto = pedido.FirstOrDefault().PedidoItens.FirstOrDefault().ProdutoTamanho.Produto.Nome });

                //}
                //else
                //    return Json(new { retorno = "ERRO" });
                if (_lstPedidos != null)
                {
                    return Json(new { retorno = "OK", _lstPedidos }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = 503;
                    return Json(new { retorno = "Que feio servidor, ERRO?." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 503;
                return Json(new { retorno = "Que feio servidor, ERRO?." }, JsonRequestBehavior.AllowGet);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult GetValorRepasse(int itemID) {
            var pedidoitem = pedidoItemRepository.Get(itemID);
            if (pedidoitem != null) {
                if (pedidoitem.Pedido.Fornecedor != null) {
                    var fornecedorpreco = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == pedidoitem.Pedido.FornecedorID).FirstOrDefault(c => c.ProdutoID == pedidoitem.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == pedidoitem.ProdutoTamanhoID);
                    if (fornecedorpreco != null) {
                        if (fornecedorpreco.Repasse.HasValue) {
                            if (fornecedorpreco.Repasse.Value > 0) {
                                return Json(new { retorno = "OK", valor = fornecedorpreco.Repasse.Value });
                            }
                        }
                    }
                }
                return Json(new { retorno = "OK", valor = 0 });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
       
        #endregion

        #region HTTPPOST
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            try
            {

                #region Dados Cadastrais e Complementares do Cliente
                //----------------------------------------------------------------------
                var Documento = form["Documento"].Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                var Email = string.IsNullOrEmpty(form["Email"]) ? "" : form["Email"].Trim().ToLower();
                var EmailSecundario = string.IsNullOrEmpty(form["EmailSecundario"]) ? "" : form["EmailSecundario"].Trim().ToLower();

                var cliente = clienteRepository.GetByExpression(c => c.Email.Trim().ToLower() == Email).FirstOrDefault();

                if (cliente == null)
                    cliente = clienteRepository.GetByExpression(c => c.Documento.Replace(".", "").Replace("-", "").Replace("/", "").Trim() == Documento).FirstOrDefault();

                if (cliente == null)
                {
                    cliente = new Cliente();
                    cliente.Senha = Funcoes.CriarSenha(4);
                    cliente.DataCadastro = DateTime.Now;
                }

                cliente.Exterior = (form["Exterior"] == "on");
                cliente.TipoID = Convert.ToInt32(form["TipoID"]);

                if (Documento.Length > 0)
                    cliente.Documento = Documento;
                else
                    cliente.Documento = "N/I";

                cliente.Nome = form["Nome"].Trim().ToUpper();
                cliente.RazaoSocial = form["RazaoSocial"].Trim().ToUpper();
                cliente.InscricaoEstadual = form["InscricaoEstadual"];

                cliente.PessoaContato = form["PessoaContato"];

                if (Email.Length > 0)
                    cliente.Email = Email;
                else
                    cliente.Email = "N/I";

                cliente.EmailSecundario = EmailSecundario;
                cliente.TelefoneContato = form["TelefoneContato"];
                cliente.CelularContato = form["CelularContato"];
                cliente.CEP = form["CEP"];
                try
                {
                    cliente.EstadoID = Convert.ToInt32(form["EstadoID"]);
                }
                catch { }
                try
                {
                    cliente.CidadeID = Convert.ToInt32(form["CidadeID"]);
                }
                catch { }

                cliente.Logradouro = form["Logradouro"];
                cliente.Numero = form["Numero"];
                cliente.Complemento = form["Complemento"];
                cliente.Bairro = form["Bairro"];

                ///Dados Complementares
                cliente.ComoConheceu = form["ComoConheceu"];

                ///Salva Dados preenchidos do Cliente
                clienteRepository.Save(cliente);

                //----------------------------------------------------------------------
                #endregion

                #region Dados Pedido
                //----------------------------------------------------------------------
                //PRODUTO (1)
                var ProdutoTamanhoID = Convert.ToInt32(form["ProdutoTamanhoID"]);
                var descricaoproduto1 = form["DescricaoProduto1"];
                var produtoTamanho = produtoTamanhoRepository.Get(ProdutoTamanhoID);

                //PRODUTO (2)
                var ProdutoTamanhoID2 = 0;
                Int32.TryParse(form["ProdutoTamanhoID2"], out ProdutoTamanhoID2);
                var descricaoproduto2 = form["DescricaoProduto2"];
                var produtoTamanho2 = produtoTamanhoRepository.Get(ProdutoTamanhoID2);

                //----------------------------------------------------------------------
                #endregion

                #region Detalhes de Entrega
                //----------------------------------------------------------------------

                int? _localEntregaId = null;

                if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                    _localEntregaId = int.Parse(form["LocalID"]);

                if (_localEntregaId == 0)
                    _localEntregaId = null;

                //----------------------------------------------------------------------
                #endregion

                #region Pagamento
                //----------------------------------------------------------------------
                var valortotal = Convert.ToDecimal(form["ValorTotal"]);
                var parcela = Convert.ToInt32(form["Parcela"]);

                decimal ValorDoDesconto = 0;

                if (!string.IsNullOrEmpty(form["ValorDesconto"]))
                    ValorDoDesconto = Convert.ToDecimal(form["ValorDesconto"]);
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Instancia Pedido
                //----------------------------------------------------------------------
                var pedido = new Pedido()
                {

                    ClienteID = cliente.ID,
                    Codigo = Guid.NewGuid(),
                    Numero = CriarNumero(),
                    TID = "",
                    Retorno = "",
                    Origem = "ADMIN",
                    NomeSolicitante = string.Empty,
                    EmailSolicitante = string.Empty,
                    //NomeSolicitante = form["NomeSolicitante"].ToUpper(),
                    //EmailSolicitante = form["EmailSolicitante"].Trim().ToLower(),
                    Parentesco = "",
                    TelefoneContato = form["TelefoneContato"],

                    //OrigemTel = form["OrigemTel"],
                    OrigemSite = form["OrigemSite"].ToUpper(),
                    OrigemForma = form["OrigemForma"].ToUpper(),
                    Mensagem = "",

                    PessoaHomenageada = form["PessoaHomenageada"],

                    EstadoID = Convert.ToInt32(form["EstadoEntregaID"]),
                    CidadeID = Convert.ToInt32(form["CidadeEntregaID"]),
                    LocalID = _localEntregaId,
                    //LocalEntrega = string.IsNullOrEmpty(form["LocalEntrega"]) ? "LOCAL NÃO CADASTRADO" : form["LocalEntrega"],
                    ComplementoLocalEntrega = form["ComplementoLocalEntrega"],
                    DataSolicitada = string.IsNullOrEmpty(form["DataEntrega"]) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(form["DataEntrega"]),

                    ValorDesconto = ValorDoDesconto,
                    ValorTotal = valortotal,
                    NFeValor = valortotal,
                    Parcela = parcela,
                    FormaPagamentoID = Convert.ToInt32(form["FormaPagamentoID"]),
                    MeioPagamentoID = Convert.ToInt32(form["MeioPagamentoID"]),
                    StatusPagamentoID = Convert.ToInt32(form["StatusPagamentoID"]),

                    Observacoes = "",

                    QualidadeStatusContatoID = (int)Pedido.TodosStatusContato.NI,
                    QualidadeStatusContatoNaoID = (int)Pedido.TodosStatusContatoNao.NI,


                    StatusProcessamento = Pedido.TodosStatusProcessamento.Criado,

                    DataAtualizacao = DateTime.Now,
                    DataCriacao = DateTime.Now,
                };
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - ADWORDS
                //----------------------------------------------------------------------
                // ADICIONA TELEFONE DO ADWORDS NO PEDIDO
                if (form["Adwords"] != null)
                {
                    if (form["Adwords"] == "on")
                    {
                        pedido.OrigemTel = "40033138";
                    }
                }

                pedidoRepository.Save(pedido);
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Pagamento
                //----------------------------------------------------------------------
                if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.Pago)
                {
                    pedido.DataPagamento = Convert.ToDateTime(form["DataPagamento"]);
                    pedidoRepository.Save(pedido);
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - ITENS
                //----------------------------------------------------------------------
                var item = new PedidoItem();
                item.PedidoID = pedido.ID;
                item.ProdutoTamanhoID = ProdutoTamanhoID;
                item.Observacoes = "";
                item.Mensagem = form["Frase"].ToUpper();
                item.Descricao = produtoTamanho.Produto.Nome;
                item.Valor = produtoTamanho.Preco;
                pedidoItemRepository.Save(item);

                if (item.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                {
                    item.Descricao = "Produto personalizado: \n" + descricaoproduto1;
                    pedidoItemRepository.Save(item);
                }

                var item2 = new PedidoItem();
                if (produtoTamanho2 != null)
                {
                    item2.PedidoID = pedido.ID;
                    item2.ProdutoTamanhoID = ProdutoTamanhoID2;
                    item2.Observacoes = "";
                    item2.Mensagem = form["Frase2"].ToUpper();
                    item2.Descricao = produtoTamanho.Produto.Nome;
                    item2.Valor = produtoTamanho2.Preco;
                    pedidoItemRepository.Save(item2);

                    if (item2.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                    {
                        item2.Descricao = "Produto personalizado: \n" + descricaoproduto2;
                        pedidoItemRepository.Save(item2);
                    }
                }

                if (item.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
                {
                    pedido.LocalID = null;
                    pedidoRepository.Save(pedido);
                }

                if (item2.ProdutoTamanho != null)
                {
                    if (item2.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor)
                    {
                        pedido.LocalID = null;
                        pedidoRepository.Save(pedido);
                    }
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Define o Status Inicial de Entrega do Pedido
                //----------------------------------------------------------------------
                //Verifica campos preenchidos para definir status inicial do pedido
                if (string.IsNullOrEmpty(pedido.PessoaHomenageada)
                    //|| string.IsNullOrEmpty(item.Mensagem)
                    || string.IsNullOrEmpty(pedido.EstadoID.ToString())
                    || string.IsNullOrEmpty(pedido.CidadeID.ToString())
                    || string.IsNullOrEmpty(pedido.DataSolicitada.ToString())
                    || pedido.DataSolicitada.ToString("dd/MM/yyyy").Equals("01/01/1753")
                    )
                {
                    pedido.StatusEntrega = TodosStatusEntrega.PendenteAtendimento;
                }
                else
                {
                    pedido.StatusEntrega = TodosStatusEntrega.PendenteFornecedor;

                    //if (_localEntregaId == null)
                    //{
                    //    pedido.StatusEntrega = TodosStatusEntrega.PendenteFornecedor;
                    //}
                    //else
                    //{
                    //    pedido.StatusEntrega = 0;
                    //}
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pedido - Observações
                //----------------------------------------------------------------------
                if (!String.IsNullOrEmpty(form["ObservacaoNota"]))
                {
                    var nota = new PedidoNota();
                    nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                    nota.DataCriacao = DateTime.Now;
                    nota.PedidoID = pedido.ID;
                    pedidoNotaRepository.Save(nota);
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pagamento - Forma Boleto
                //----------------------------------------------------------------------
                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto)
                {
                    var random = new Random();
                    var nossoNumero = "";
                    do
                    {
                        nossoNumero = random.Next(99999999).ToString("00000000");
                    }
                    while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                    // 24/02/17 - Rodrigo - devido a problemas no cnab 
                    //Mudanca da forma de gerar , devido aos id de pedido terem passdo de 5 caracteres 
                    //Informação ITAU - "No. do Documento" informado pelo Beneficiário é composto por: 10 dígitos
                    //var numeroDocumento = "2" + DateTime.Now.Year + pedido.ID.ToString("00000");

                    var numeroDocumento = "2000" + pedido.ID.ToString("00000");

                    var tipoID = Domain.Entities.Boleto.TodosTipos.PJ;
                    tipoID = (cliente.Tipo == Cliente.Tipos.PessoaFisica) ? Domain.Entities.Boleto.TodosTipos.PF : Domain.Entities.Boleto.TodosTipos.PJ;

                    var boleto = new Boleto();
                    boleto.PedidoID = pedido.ID;
                    boleto.TipoID = (int)tipoID;
                    boleto.Carteira = Configuracoes.BOLETO_CARTEIRA;
                    boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA;
                    boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ;
                    boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE;
                    boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE;
                    boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL;
                    boleto.DataCriacao = DateTime.Now;
                    boleto.DataVencimento = Domain.Core.Funcoes.DataVencimentoUtil();
                    boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO;
                    boleto.NossoNumero = nossoNumero;
                    boleto.NumeroDocumento = numeroDocumento;
                    boleto.SacadoDocumento = cliente.Documento;
                    if (tipoID == Domain.Entities.Boleto.TodosTipos.PJ)
                        boleto.SacadoNome = cliente.RazaoSocial;
                    else
                        boleto.SacadoNome = cliente.Nome;
                    if (String.IsNullOrEmpty(cliente.Bairro))
                        boleto.SacadoBairro = "";
                    else
                        boleto.SacadoBairro = cliente.Bairro;

                    if (String.IsNullOrEmpty(cliente.CEP))
                        boleto.SacadoCEP = "";
                    else
                        boleto.SacadoCEP = cliente.CEP;

                    if (cliente.Cidade == null)
                        boleto.SacadoCidade = "";
                    else
                        boleto.SacadoCidade = cliente.Cidade.Nome;

                    if (String.IsNullOrEmpty(cliente.Logradouro))
                        boleto.SacadoEndereco = "";
                    else
                        boleto.SacadoEndereco = cliente.Logradouro + " " + cliente.Numero + " " + cliente.Complemento;

                    if (cliente.Estado == null)
                        boleto.SacadoUF = "";
                    else
                        boleto.SacadoUF = cliente.Estado.Sigla;

                    boleto.Comando = Domain.Entities.Boleto.Comandos.CriarRemessa;
                    boleto.Processado = false;
                    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.AguardandoPagamento;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Criado;
                    boleto.Valor = pedido.ValorTotal;

                    boletoRepository.Save(boleto);
                }

                //----------------------------------------------------------------------
                #endregion

                #region Envio de E-mail Pedido
                //----------------------------------------------------------------------
                var enviarEmailSolicitante = false;//form["EnviarEmailSolicitante"] == "on";
                var enviarEmail = form["EnviarEmail"] == "on";

                pedido.EnviarPedidoEmail(enviarEmail, enviarEmailSolicitante);
                //----------------------------------------------------------------------
                #endregion

                #region Envio de E-mail Marketing
                //----------------------------------------------------------------------
                AvisoService avisoService = new AvisoService();
                if (!string.IsNullOrEmpty(cliente.Email) && cliente.Email != "N/I")
                {
                    cliente.ReceberEmail = 1;
                    cliente.DataAniversario = "";
                    avisoService.addEmailMkt(cliente, "ADMIN COROAS PARA VELÓRIO", form["OrigemSite"].ToUpper());
                }

                //----------------------------------------------------------------------
                #endregion

                #region Log Pedido
                //----------------------------------------------------------------------
                //Registra log do Pedido por Usuário (tbl = [Sistema].[AdministradorPedido]) e ( Ação - tbl [Sistema].[AdministradorPedidoAcao])
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ProcessouPedido);
                //----------------------------------------------------------------------
                #endregion

                if(pedido.MeioPagamentoID == (int)Pedido.MeiosPagamento.PagarMe)
                {
                    Response.Redirect("/PedidosSite/Pagar/" + pedido.ID);
                }

                return RedirectToAction("List", new { datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy"), statusentrega = 0, cod = "SaveSucess", msg = " O pedido " + pedido.Numero + " para o cliente " + cliente.Nome + " foi criado com sucesso!" });

            }
            catch (Exception ex)
            {
                return RedirectToAction("List", new { datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy"), statusentrega = 0, cod = "SaveFailure", msg = "Falha na criação do Pedido!" + ex.Message });
            }
        }
        //----------------------------------------------------------------------
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var nota = new PedidoNota();
            var nota2 = new PedidoNota();
            var pedido = pedidoRepository.Get(id);
            var statusentregaid = 0;

            #region Dados do Pedido
            //----------------------------------------------------------------------
            if (pedido != null)
            {
                var datarepasseold = pedido.DataRepasse;
                var enviapesquisa = false;

                if (!string.IsNullOrEmpty(form["DataEntrega"]))
                {
                    enviapesquisa = pedido.DataEntrega != Convert.ToDateTime(form["DataEntrega"]) && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";
                }


                statusentregaid = pedido.StatusEntregaID;

                if (!string.IsNullOrEmpty(form["StatusEntregaID"]))
                {
                    pedido.StatusEntregaID = Convert.ToInt32(form["StatusEntregaID"]);
                }
                
                pedido.PessoaHomenageada = form["PessoaHomenageada"];
                pedido.Parentesco = form["Parentesco"];
                pedido.ParentescoRecebidoPor = form["ParentescoRecebidoPor"];
                pedido.RecebidoPor = form["RecebidoPor"];
                pedido.TelefoneContato = form["TelefoneContato"];

                int? _localEntregaId = null;

                if (!string.IsNullOrEmpty(form["LocalID"]) && form["LocalID"] != "NULL" && form["LocalID"] != "-- selecione --")
                    _localEntregaId = int.Parse(form["LocalID"]);

                if (_localEntregaId == 0)
                    _localEntregaId = null;

                if (pedido.LocalID != _localEntregaId)
                {
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouLocalIDEntrega);
                }

                pedido.LocalID = _localEntregaId;

                if (!string.IsNullOrEmpty(form["DataEntrega"]))
                {
                    pedido.DataEntrega = Convert.ToDateTime(form["DataEntrega"]);
                    enviapesquisa = pedido.DataEntrega != Convert.ToDateTime(form["DataEntrega"]) && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";
                }

                if (!string.IsNullOrEmpty(form["DataSolicitada"]))
                {
                    pedido.DataSolicitada = Convert.ToDateTime(form["DataSolicitada"]);
                }

                pedido.EstadoID = Convert.ToInt32(form["EstadoID"]);

                if (!string.IsNullOrEmpty(form["CidadeID"]))
                {
                    pedido.CidadeID = int.Parse(form["CidadeID"]);
                }

                //if (!string.IsNullOrEmpty(form["LocalEntrega"]))
                //{
                //    pedido.LocalEntrega = form["LocalEntrega"];
                //}
                //else
                //{
                //    pedido.LocalEntrega = "LOCAL NÃO CADASTRADO";
                //}

                pedido.ComplementoLocalEntrega = form["ComplementoLocalEntrega"];
                pedido.Observacoes = form["Observacoes"];
                pedido.ObservacoesPagamento = form["ObservacoesPagamento"];

                if (!string.IsNullOrEmpty(form["NFeNumero"]))
                {
                    var numeroNF = Convert.ToInt32(form["NFeNumero"]);

                    if (pedido.NFeNumero != numeroNF)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.PreencheuNF);
                    }

                    pedido.NFeNumero = numeroNF;
                }

                if (!string.IsNullOrEmpty(form["NFeValor"]))
                {
                    pedido.NFeValor = Convert.ToDecimal(form["NFeValor"]);
                }

                if (!string.IsNullOrEmpty(form["NFeData"]))
                {
                    pedido.NFeData = Convert.ToDateTime(form["NFeData"]);
                }

                var enviaNFe = !String.IsNullOrEmpty(pedido.NFeLink) && pedido.NFeLink != form["NFeLink"] && !pedido.DataEmailNFe.HasValue;

                pedido.NFeLink = form["NFeLink"].Trim();
                pedido.DataAtualizacao = DateTime.Now;
                //----------------------------------------------------------------------
                #endregion

                #region NFE
                //----------------------------------------------------------------------
                if (enviaNFe)
                {
                    pedido.EnviarNFe();
                    pedido.DataEmailNFe = DateTime.Now;
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailNFe);
                }
                //----------------------------------------------------------------------
                #endregion

                #region Foto Item
                //----------------------------------------------------------------------
                bool ValidouFoto = false;

                foreach (var item in pedido.PedidoItens.ToList())
                {
                    item.Mensagem = form["Mensagem_" + item.ID];
                    if (form["FotoValidada_" + item.ID] != null && form["FotoValidada_" + item.ID].ToString() == "on")
                    {
                        item.FotoValidada = true;
                        ValidouFoto = true;
                    }
                    else
                    {
                        item.FotoValidada = false;
                    }

                    if(!string.IsNullOrEmpty(form["TipoFaixaID_" + item.ID]))
                    {
                        item.TipoFaixaID = Convert.ToInt32(form["TipoFaixaID_" + item.ID]);
                    }

                    pedidoItemRepository.Save(item);
                }

                if (ValidouFoto)
                {
                    if (!string.IsNullOrEmpty(form["InserirLogValidaFoto"]))
                    {
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ValidouFoto);
                    }
                }

                if (Request.Files.Count > 0)
                {
                    foreach (var item in pedido.PedidoItens)
                    {
                        var foto = Request.Files["foto_" + item.ID];
                        if (foto != null)
                        {
                            if (foto.ContentLength > 0)
                            {
                                DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero));
                                if (!dir.Exists)
                                {
                                    dir.Create();
                                }

                                var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                                //FOTO
                                ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedido.Numero + "/" + item.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                        "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=both"));

                                i.CreateParentDirectory = true;
                                i.Build();
                            }
                        }

                        pedidoItemRepository.Save(item);
                    }

                    #region Comprovante Pagamento
                    //----------------------------------------------------------------------

                    var comprovante = Request.Files["comprovante"];
                    if (comprovante.ContentLength > 0)
                    {
                        DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante"));
                        if (!dir.Exists)
                        {
                            dir.Create();
                        }
                        var extensao = Path.GetExtension(comprovante.FileName);
                        comprovante.SaveAs(Server.MapPath("/content/pedidos/" + pedido.Numero + "/comprovante/comprovante-pagto-" + DateTime.Now.ToString("yyyyMMddHHmm") + extensao));
                    }
                    //----------------------------------------------------------------------
                    #endregion
                }
                //----------------------------------------------------------------------
                #endregion

                #region Historico Status Entrega
                //----------------------------------------------------------------------
                var historico = "";
                if (pedido.StatusEntregaID != statusentregaid)
                    historico += "<b>Status Entrega: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((TodosStatusEntrega)statusentregaid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusEntrega) + "</i>";
                if (historico.Length > 0)
                {
                    historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;
                    nota2.DataCriacao = DateTime.Now;
                    nota2.PedidoID = pedido.ID;
                    nota2.Observacoes = historico;
                }
                //----------------------------------------------------------------------
                #endregion

                #region Notas
                //----------------------------------------------------------------------
                if (form["ObservacaoNota"].Length > 0)
                {

                    nota.DataCriacao = DateTime.Now;
                    nota.PedidoID = pedido.ID;
                    nota.Observacoes = adminModel.Nome + "<br>" + form["ObservacaoNota"];
                }
                //----------------------------------------------------------------------
                #endregion

                #region Pesquisa
                //----------------------------------------------------------------------
                if (enviapesquisa)
                {
                    if (Configuracoes.ENVIA_TRUSTVOX)
                    {
                        pedido.DataEnvioPesquisa = null;
                    }
                    else
                    {
                        pedido.EnviarEmailPesquisa();
                        pedido.DataEnvioPesquisa = DateTime.Now;
                    }
                }
                //----------------------------------------------------------------------
                #endregion

                #region SALVA DADOS PEDIDO
                //----------------------------------------------------------------------

                if (historico.Length > 0)
                    pedidoNotaRepository.Save(nota2);

                if (form["ObservacaoNota"].Length > 0)
                    pedidoNotaRepository.Save(nota);

                pedidoRepository.Save(pedido);

                //----------------------------------------------------------------------
                #endregion

                #region ATUALIZA PEDIDO DA LINS
                //----------------------------------------------------------------------
                var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCPV == pedido.ID && r.StatusCancelamentoID == null).FirstOrDefault();
                if (NovoPedidoloricultura != null)
                {
                    NovoPedidoloricultura.HorarioEntrega = pedido.DataSolicitada;
                    NovoPedidoloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                    NovoPedidoloricultura.EstadoID = pedido.EstadoID;
                    NovoPedidoloricultura.CidadeID = pedido.CidadeID;
                    NovoPedidoloricultura.LocalID = pedido.LocalID;
                    NovoPedidoloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                    #region DELETA PRODUTOS ANTERIORES

                    var ListaItens = NovoPedidoloricultura.PedidoItemFloricultura.ToList();
                    foreach (var Item in ListaItens)
                    {
                        PedidoItemFloriculturaRepository.Delete(Item);
                    }
                    #endregion

                    foreach (var Coroa in pedido.PedidoItens)
                    {
                        var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == Coroa.ProdutoTamanho.Produto.ID).FirstOrDefault();

                        if (ProdutoFloricultura != null)
                        {
                            var IdTamanhoProdFloricultura = 0;
                            switch (Coroa.ProdutoTamanho.Tamanho.ID)
                            {
                                // PEQUENO
                                case 1:
                                case 4:
                                case 8:
                                case 11:
                                case 14:
                                case 17:
                                case 20:
                                    IdTamanhoProdFloricultura = 2;
                                    break;
                                // MEDIO
                                case 2:
                                case 5:
                                case 21:
                                case 9:
                                case 12:
                                case 15:
                                case 18:
                                    IdTamanhoProdFloricultura = 3;
                                    break;
                                // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                case 3:
                                case 6:
                                case 10:
                                case 13:
                                case 16:
                                case 19:
                                case 22:
                                    IdTamanhoProdFloricultura = 4;
                                    break;
                                default:
                                    IdTamanhoProdFloricultura = 5;
                                    break;
                            }

                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = ProdutoFloricultura.Nome,
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = (decimal)ValorTblRepasse.Repasse,
                                Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem
                            });
                        }
                        else
                        {
                            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                            decimal ValorRepasse = 0;
                            if (ValorTblRepasse != null)
                            {
                                ValorRepasse = (decimal)ValorTblRepasse.Repasse;
                            }
                            else
                            {
                                if (Coroa.RepasseAtual != null && Coroa.RepasseAtual.ValorRepasse > 0)
                                {
                                    ValorRepasse = Coroa.RepasseAtual.ValorRepasse;
                                }
                            }

                            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                            {
                                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                Descricao = Coroa.ProdutoTamanho.Produto.Nome.ToUpper(),
                                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                Valor = (decimal)ValorTblRepasse.Repasse,
                                Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem,
                                Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto CPV: " + Coroa.ProdutoTamanho.Produto.Nome.ToUpper()
                            });
                        }
                    }

                    // ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                    var SomaItenPedidoFloricultura = NovoPedidoloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                    NovoPedidoloricultura.ValorTotal = SomaItenPedidoFloricultura;

                    // ADICIONA DADOS DE ENTREGA
                    NovoPedidoloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                    NovoPedidoloricultura.EstadoID = pedido.Estado.ID;
                    NovoPedidoloricultura.CidadeID = pedido.Cidade.ID;
                    NovoPedidoloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                    if (pedido.LocalID != null)
                    {
                        NovoPedidoloricultura.LocalID = pedido.LocalID;
                    }
                    pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                }
                //----------------------------------------------------------------------
                #endregion

                return RedirectToAction("Visualizar", new { id = pedido.ID, cod = "SaveSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Falha na alteração do Pedido!" });
            }
        }
        //----------------------------------------------------------------------
        public JsonResult CriarRepasse(int itemID, decimal valor)
        {
            var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == itemID).Where(p => p.StatusID != 3).FirstOrDefault();
            if (repasse == null)
            {
                var pedidoitem = pedidoItemRepository.Get(itemID);
                repasse = new Repasse();
                repasse.FornecedorID = pedidoitem.Pedido.FornecedorID;
                repasse.PedidoItemID = pedidoitem.ID;
                repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                repasse.AdministradorID = adminModel.ID;
                repasse.ValorRepasse = valor;
                repasse.DataCriacao = DateTime.Now;
                repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                repasseRepository.Save(repasse);

                administradorPedidoRepository.Registra(adminModel.ID, pedidoitem.PedidoID, null, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                return Json(new { retorno = "OK" });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarRepasse(int id, int itemID, decimal valor)
        {
            var pedidoitem = pedidoItemRepository.Get(itemID);
            var pedido = pedidoitem.Pedido;

            var repasse = repasseRepository.Get(id);
            if (repasse != null)
            {
                if (repasse.Status == Repasse.TodosStatus.AguardandoPagamento)
                {
                    repasse.Observacao = "\nRepasse alterado de " + repasse.ValorRepasse.ToString("C2") + " para " + valor.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                    repasse.ValorRepasse = valor;
                    repasseRepository.Save(repasse);

                    administradorPedidoRepository.Registra(adminModel.ID, pedidoitem.PedidoID, null, null, null, AdministradorPedido.Acao.AlterouRepasse);


                    #region ATUALIZA PEDIDO DA LINS

                    var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCPV == pedido.ID && r.StatusCancelamentoID == null).FirstOrDefault();
                    if (NovoPedidoloricultura != null)
                    {
                        NovoPedidoloricultura.HorarioEntrega = pedido.DataSolicitada;
                        NovoPedidoloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedido.EstadoID;
                        NovoPedidoloricultura.CidadeID = pedido.CidadeID;
                        NovoPedidoloricultura.LocalID = pedido.LocalID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                        #region DELETA PRODUTOS ANTERIORES

                        var ListaItens = NovoPedidoloricultura.PedidoItemFloricultura.ToList();
                        foreach (var Item in ListaItens)
                        {
                            PedidoItemFloriculturaRepository.Delete(Item);
                        }
                        #endregion

                        foreach (var Coroa in pedido.PedidoItens)
                        {
                            var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == Coroa.ProdutoTamanho.Produto.ID).FirstOrDefault();

                            if (ProdutoFloricultura != null)
                            {
                                var IdTamanhoProdFloricultura = 0;
                                switch (Coroa.ProdutoTamanho.Tamanho.ID)
                                {
                                    // PEQUENO
                                    case 1:
                                    case 4:
                                    case 8:
                                    case 11:
                                    case 14:
                                    case 17:
                                    case 20:
                                        IdTamanhoProdFloricultura = 2;
                                        break;
                                    // MEDIO
                                    case 2:
                                    case 5:
                                    case 21:
                                    case 9:
                                    case 12:
                                    case 15:
                                    case 18:
                                        IdTamanhoProdFloricultura = 3;
                                        break;
                                    // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                    case 3:
                                    case 6:
                                    case 10:
                                    case 13:
                                    case 16:
                                    case 19:
                                    case 22:
                                        IdTamanhoProdFloricultura = 4;
                                        break;
                                    default:
                                        IdTamanhoProdFloricultura = 5;
                                        break;
                                }

                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                {
                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                    Descricao = ProdutoFloricultura.Nome,
                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem
                                });
                            }
                            else
                            {
                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                {
                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoloricultura.ID),
                                    Descricao = Coroa.ProdutoTamanho.Produto.Nome.ToUpper(),
                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem,
                                    Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto CPV: " + Coroa.ProdutoTamanho.Produto.Nome.ToUpper()
                                });
                            }
                        }

                        // ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                        var SomaItenPedidoFloricultura = NovoPedidoloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                        NovoPedidoloricultura.ValorTotal = SomaItenPedidoFloricultura;

                        // ADICIONA DADOS DE ENTREGA
                        NovoPedidoloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                        NovoPedidoloricultura.EstadoID = pedido.Estado.ID;
                        NovoPedidoloricultura.CidadeID = pedido.Cidade.ID;
                        NovoPedidoloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                        if (pedido.LocalID != null)
                        {
                            NovoPedidoloricultura.LocalID = pedido.LocalID;
                        }
                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                    }

                    #endregion

                    return Json(new { retorno = "OK" });
                }
                else
                    return Json(new { retorno = "ERRO", mensagem = "O valor do repasse só pode ser alterado se estiver aguardando pagamento." });

            }
            else
                return Json(new { retorno = "ERRO" });
        }
        //----------------------------------------------------------------------
        public JsonResult CancelarLeilaoFornecedor(int pedidoID, int localID)
        {
            var retorno = "";
            try
            {
                var _acaoExistente = administradorPedidoRepository.GetByExpression(c => c.AcaoID == (int)AdministradorPedido.Acao.RecusouEntrega && c.Pedido.ID == pedidoID).Count();

                if (_acaoExistente == 0)
                {
                    var lstFornecedoresLocal = fornecedorlocalRepository.GetByExpression(c => c.LocalID == localID).OrderByDescending(c => c.FornecedorID).ToList();

                    foreach (var fornecedor in lstFornecedoresLocal)
                    {
                        administradorPedidoRepository.RegistraLogFornecedor(adminModel.ID, pedidoID, null, null, null, AdministradorPedido.Acao.RecusouEntrega, fornecedor.Fornecedor.ID);
                    }

                    var pedido = pedidoRepository.Get(pedidoID);
                    if (pedido != null)
                    {
                        pedido.StatusEntrega = TodosStatusEntrega.PendenteFornecedor;
                    }

                    retorno = "OK";
                }
            }
            catch (Exception)
            {
                retorno = "ERRO";
            }

            return Json(retorno);
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarFornecedor(int id, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);

                if (pedido != null)
                {
                    #region NOVO FORNECEDOR
                    //----------------------------------------------------------------------
                    if (!pedido.FornecedorID.HasValue)
                    {
                        //Registra log do usuário

                        pedido.FornecedorID = fornecedorID;

                        #region INSERE PEDIDO PRODUCAO FORNECEDORES
                        //----------------------------------------------------------------------
                        // *** coloca o pedido diretamente em producao no dash do fornecedor
                        pedido.StatusEntregaID = (int)TodosStatusEntrega.EmProducao;
                        pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                        pedido.DataAtualizacao = DateTime.Now;

                        pedido.FornecedorEmProcessamento = true;
                        pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;

                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                        // administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega);
                        // administradorPedidoRepository.RegistraLogFornecedor(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega, fornecedorID);

                        pedidoRepository.Save(pedido);
                        //----------------------------------------------------------------------
                        #endregion

                        #region DADOS DE NPS
                        //----------------------------------------------------------------------
                        npsREspository.Save(new NPSRating { PedidoID = pedido.ID, AdministradorID = adminModel.ID, DataCriacao = DateTime.Now, Nota = 0, Guid = Guid.NewGuid(), FornecedorID = fornecedorID, EmailEnviado = false });
                        //----------------------------------------------------------------------
                        #endregion

                        #region FORNECEDOR PREÇOS
                        //----------------------------------------------------------------------
                        var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();

                        if (fornecedorprecos != null)
                        {
                            foreach (var item in pedido.PedidoItens)
                            {
                                #region FORNECEDOR REPASSE
                                //----------------------------------------------------------------------
                                var repasse = repasseRepository.GetByExpression(c => c.PedidoItemID == item.ID).FirstOrDefault();

                                if (repasse == null)
                                {
                                    repasse = new Repasse();
                                    repasse.FornecedorID = pedido.FornecedorID;
                                    repasse.PedidoItemID = item.ID;
                                    repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                                    repasse.AdministradorID = adminModel.ID;
                                    repasse.DataCriacao = DateTime.Now;
                                    repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;

                                    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == item.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == item.ProdutoTamanhoID);
                                    if (fornecedorpreco != null)
                                    {
                                        if (fornecedorpreco.Repasse.HasValue)
                                        {
                                            if (fornecedorpreco.Repasse.Value > 0)
                                            {
                                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                                                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                            }
                                        }
                                    }
                                    repasseRepository.Save(repasse);
                                    //----------------------------------------------------------------------
                                    #endregion

                                    #region INSERE PEDIDO LINS
                                    //----------------------------------------------------------------------
                                    // ADICIONAR O PEDIDO AUTOMATICAMENTE CASO O FORNECEDOR SEJA A --- FLORICULTURA FLOR DA LINS, ID 388
                                    if (fornecedorID == 388)
                                    {
                                        var NovoPedidoFloricultura = new PedidoFloricultura
                                        {
                                            PedidoFloriculturaOrigem = PedidoFloriculturaOrigemRepository.Get(2),
                                            Codigo = Guid.NewGuid(),
                                            Numero = CriarNumeroFloricultura(),
                                            ClienteFloricultura = ClienteFloriculturaRepository.Get(31),
                                            StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Concluido,
                                            StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.AguardandoPagamento,
                                            MeioPagamentoID = (int)PedidoFloricultura.MeiosPagamento.Faturamento,
                                            FormaPagamentoID = (int)PedidoFloricultura.FormasPagamento.Indefinido,
                                            StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Pendente,
                                            ValorTotal = 0,
                                            HorarioEntrega = pedido.DataEntrega,
                                            CobrarNoLocal = false,
                                            ValorDesconto = 0,
                                            CidadeID = 8570,
                                            EstadoID = 26,
                                            DataCriacao = DateTime.Now,
                                            Obs = "Pedido automático inserido pelo sistema do CPV. NR = " + pedido.Numero,
                                            IdPedidoCPV = pedido.ID
                                        };

                                        pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                        pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                        {
                                            PedidoFloricultura = NovoPedidoFloricultura,
                                            DataCriacao = DateTime.Now,
                                            Observacoes = "Pedido Inserido automaticamente por conta da seleção da Flor Da Lins como Fornecedor."
                                        });

                                        foreach (var Coroa in pedido.PedidoItens)
                                        {
                                            var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == Coroa.ProdutoTamanho.Produto.ID).FirstOrDefault();

                                            if (ProdutoFloricultura != null)
                                            {
                                                var IdTamanhoProdFloricultura = 0;
                                                switch (Coroa.ProdutoTamanho.Tamanho.ID)
                                                {
                                                    // PEQUENO
                                                    case 1:
                                                    case 4:
                                                    case 8:
                                                    case 11:
                                                    case 14:
                                                    case 17:
                                                    case 20:
                                                        IdTamanhoProdFloricultura = 2;
                                                        break;
                                                    // MEDIO
                                                    case 2:
                                                    case 5:
                                                    case 21:
                                                    case 9:
                                                    case 12:
                                                    case 15:
                                                    case 18:
                                                        IdTamanhoProdFloricultura = 3;
                                                        break;
                                                    // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                                    case 3:
                                                    case 6:
                                                    case 10:
                                                    case 13:
                                                    case 16:
                                                    case 19:
                                                    case 22:
                                                        IdTamanhoProdFloricultura = 4;
                                                        break;
                                                    default:
                                                        IdTamanhoProdFloricultura = 5;
                                                        break;
                                                }

                                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                                {
                                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                                    Descricao = ProdutoFloricultura.Nome,
                                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem
                                                });
                                            }
                                            else
                                            {
                                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                                decimal ValorRepasse = 0;
                                                if (ValorTblRepasse != null)
                                                {
                                                    ValorRepasse = (decimal)ValorTblRepasse.Repasse;
                                                }


                                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                                {
                                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                                    Descricao = item.ProdutoTamanho.Produto.Nome.ToUpper(),
                                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                                    Valor = ValorRepasse,
                                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem,
                                                    Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto CPV: " + item.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + item.ProdutoTamanho.Produto.Descricao.ToUpper()
                                                });
                                            }
                                        }

                                        #region ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                                        //----------------------------------------------------------------------
                                        var SomaItenPedidoFloricultura = NovoPedidoFloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                                        NovoPedidoFloricultura.ValorTotal = SomaItenPedidoFloricultura;
                                        //----------------------------------------------------------------------
                                        #endregion

                                        #region  ADICIONA DADOS DE ENTREGA
                                        //----------------------------------------------------------------------
                                        NovoPedidoFloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                                        NovoPedidoFloricultura.EstadoID = pedido.Estado.ID;
                                        NovoPedidoFloricultura.CidadeID = pedido.Cidade.ID;
                                        NovoPedidoFloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                                        if (pedido.LocalID != null)
                                        {
                                            NovoPedidoFloricultura.LocalID = pedido.LocalID;
                                        }
                                        pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                        NovoPedidoFloricultura.EnviarPedidoEmail();
                                        //----------------------------------------------------------------------
                                        #endregion

                                        //----------------------------------------------------------------------
                                        #endregion

                                    }
                                }
                            }
                        }
                        //----------------------------------------------------------------------
                        #endregion

                        return Json("OK");
                    }
                    //----------------------------------------------------------------------
                    #endregion

                    #region ALTERAR FORNECEDOR
                    //----------------------------------------------------------------------
                    else
                    {
                        if (pedido.TotalRepassesPagos == 0)
                        {
                            //Registra log do usuário
                            pedido.FornecedorID = fornecedorID;

                            #region INSERE PEDIDO PRODUCAO FORNECEDORES
                            //----------------------------------------------------------------------
                            // *** coloca o pedido diretamente em producao no dash do fornecedor
                            pedido.StatusEntregaID = (int)TodosStatusEntrega.EmProducao;
                            pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                            pedido.DataAtualizacao = DateTime.Now;

                            pedido.FornecedorEmProcessamento = true;
                            pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;

                            administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);
                            administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega);
                            administradorPedidoRepository.RegistraLogFornecedor(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AceitouEntrega, fornecedorID);

                            pedidoRepository.Save(pedido);
                            //----------------------------------------------------------------------
                            #endregion

                            #region DADOS DE NPS
                            //----------------------------------------------------------------------
                            var NPS = npsREspository.GetByExpression(p => p.PedidoID == pedido.ID).FirstOrDefault();

                            if (NPS != null)
                            {
                                NPS.AdministradorID = adminModel.ID;
                                NPS.FornecedorID = fornecedorID;
                            }

                            npsREspository.Save(NPS);
                            //----------------------------------------------------------------------
                            #endregion

                            #region Fornecedor Preços
                            //----------------------------------------------------------------------
                            var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();

                            if (fornecedorprecos != null)
                            {
                                foreach (var item in pedido.PedidoItens)
                                {
                                    #region REPASSE
                                    //----------------------------------------------------------------------
                                    var repasses = repasseRepository.GetByExpression(c => c.PedidoItemID == item.ID).ToList();
                                    foreach (var _repasse in repasses)
                                    {
                                        _repasse.Status = Repasse.TodosStatus.Cancelado;
                                        _repasse.Observacao = "\nRepasse cancelado por " + adminModel.CurrentAdministrador.Nome;
                                        repasseRepository.Save(_repasse);
                                    }
                                    var repasse = new Repasse();
                                    repasse.FornecedorID = pedido.FornecedorID;
                                    repasse.PedidoItemID = item.ID;
                                    repasse.Status = Repasse.TodosStatus.AguardandoPagamento;
                                    repasse.DataCriacao = DateTime.Now;
                                    repasse.Observacao = "\nRepasse alterado para " + repasse.ValorRepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;
                                    repasse.AdministradorID = adminModel.ID;

                                    var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == item.ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == item.ProdutoTamanhoID);
                                    if (fornecedorpreco != null)
                                    {
                                        if (fornecedorpreco.Repasse.HasValue)
                                        {
                                            if (fornecedorpreco.Repasse.Value > 0)
                                            {
                                                repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                            }
                                        }
                                    }

                                    //----------------------------------------------------------------------
                                    #endregion

                                    #region INSERE PEDIDO LINS
                                    //----------------------------------------------------------------------
                                    var NovoPedidoloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCPV == pedido.ID && r.StatusCancelamentoID == null).FirstOrDefault();
                                    if (NovoPedidoloricultura != null)
                                    {
                                        NovoPedidoloricultura.StatusCancelamentoID = (int)PedidoFloricultura.TodosStatusCancelamento.Removido;
                                        NovoPedidoloricultura.StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Cancelado;
                                        NovoPedidoloricultura.StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.Cancelado;
                                        NovoPedidoloricultura.StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Cancelado;
                                        pedidoFloriculturaRepository.Save(NovoPedidoloricultura);

                                        pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                        {
                                            PedidoFloricultura = NovoPedidoloricultura,
                                            DataCriacao = DateTime.Now,
                                            Observacoes = "Pedido cancelado automaticamente por conta da remoção da Flor Da Lins como Fornecedor."
                                        });
                                    }

                                    // ADICIONAR O PEDIDO AUTOMATICAMENTE CASO O FORNECEDOR SEJA A --- FLORICULTURA FLOR DA LINS, ID 388
                                    if (fornecedorID == 388)
                                    {
                                        var NovoPedidoFloricultura = new PedidoFloricultura
                                        {
                                            PedidoFloriculturaOrigem = PedidoFloriculturaOrigemRepository.Get(2),
                                            Codigo = Guid.NewGuid(),
                                            Numero = CriarNumeroFloricultura(),
                                            ClienteFloricultura = ClienteFloriculturaRepository.Get(31),
                                            StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Concluido,
                                            StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.AguardandoPagamento,
                                            MeioPagamentoID = (int)PedidoFloricultura.MeiosPagamento.Faturamento,
                                            FormaPagamentoID = (int)PedidoFloricultura.FormasPagamento.Indefinido,
                                            StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Pendente,
                                            ValorTotal = 0,
                                            ValorDesconto = 0,
                                            HorarioEntrega = pedido.DataEntrega,
                                            CobrarNoLocal = false,
                                            CidadeID = 8570,
                                            EstadoID = 26,
                                            DataCriacao = DateTime.Now,
                                            Obs = "Pedido automático inserido pelo sistema do CPV. NR = " + pedido.Numero,
                                            IdPedidoCPV = pedido.ID
                                        };

                                        pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                        pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                                        {
                                            PedidoFloricultura = NovoPedidoFloricultura,
                                            DataCriacao = DateTime.Now,
                                            Observacoes = "Pedido Inserido automaticamente por conta da seleção da Flor Da Lins como Fornecedor."
                                        });

                                        foreach (var Coroa in pedido.PedidoItens)
                                        {
                                            var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == Coroa.ProdutoTamanho.Produto.ID).FirstOrDefault();

                                            if (ProdutoFloricultura != null)
                                            {
                                                var IdTamanhoProdFloricultura = 0;

                                                switch (Coroa.ProdutoTamanho.Tamanho.ID)
                                                {
                                                    // PEQUENO
                                                    case 1:
                                                    case 4:
                                                    case 8:
                                                    case 11:
                                                    case 14:
                                                    case 17:
                                                    case 20:
                                                        IdTamanhoProdFloricultura = 2;
                                                        break;
                                                    // MEDIO
                                                    case 2:
                                                    case 5:
                                                    case 21:
                                                    case 9:
                                                    case 12:
                                                    case 15:
                                                    case 18:
                                                        IdTamanhoProdFloricultura = 3;
                                                        break;
                                                    // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                                                    case 3:
                                                    case 6:
                                                    case 10:
                                                    case 13:
                                                    case 16:
                                                    case 19:
                                                    case 22:
                                                        IdTamanhoProdFloricultura = 4;
                                                        break;
                                                    default:
                                                        IdTamanhoProdFloricultura = 5;
                                                        break;
                                                }

                                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                                {
                                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                                    Descricao = ProdutoFloricultura.Nome,
                                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem
                                                });
                                            }
                                            else
                                            {
                                                var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                                                var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                                                PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                                                {
                                                    PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                                                    Descricao = item.ProdutoTamanho.Produto.Nome.ToUpper(),
                                                    ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                                                    Valor = (decimal)ValorTblRepasse.Repasse,
                                                    Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem,
                                                    Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto CPV: " + item.ProdutoTamanho.Produto.Nome.ToUpper()
                                                });
                                            }
                                        }
                                        //----------------------------------------------------------------------
                                        #endregion

                                        #region ADICIONAR OS VALORES DOS ITENS AO PEDIDO
                                        //----------------------------------------------------------------------
                                        var SomaItenPedidoFloricultura = NovoPedidoFloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                                        NovoPedidoFloricultura.ValorTotal = SomaItenPedidoFloricultura;
                                        //----------------------------------------------------------------------
                                        #endregion

                                        #region ADICIONA DADOS DE ENTREGA
                                        //----------------------------------------------------------------------
                                        NovoPedidoFloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                                        NovoPedidoFloricultura.EstadoID = pedido.Estado.ID;
                                        NovoPedidoFloricultura.CidadeID = pedido.Cidade.ID;
                                        NovoPedidoFloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                                        if (pedido.LocalID != null)
                                        {
                                            NovoPedidoFloricultura.LocalID = pedido.LocalID;
                                        }

                                        pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                                        NovoPedidoFloricultura.EnviarPedidoEmail();
                                        //----------------------------------------------------------------------
                                        #endregion
                                    }


                                    repasseRepository.Save(repasse);

                                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouRepasse);
                                }
                            }
                            //----------------------------------------------------------------------
                            #endregion

                            return Json("OK");
                        }
                        else
                        {
                            return Json("O fornecedor não pode ser alterado porque possui um repasses pagos. Cancele o repasse e faça a alteração novamente.");
                        }

                    }
                    //----------------------------------------------------------------------
                    #endregion
                }
                else
                    return Json("O pedido não pode ser alterado. Pedido não localizado");
            }
            catch (Exception ex)
            {
                return Json("Falha na alteração do Pedido!" + ex.Message );
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarPushFornecedor(int id, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);

                if (pedido != null)
                {
                    #region INSERE STATUS e LOG
                    //----------------------------------------------------------------------
                    pedido.StatusEntregaID = (int)TodosStatusEntrega.PendenteFornecedor;
                    pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                    pedido.DataAtualizacao = DateTime.Now;

                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFloricultura);

                    pedidoRepository.Save(pedido);
                    //----------------------------------------------------------------------
                    #endregion

                    #region INSERE PUSH
                    //----------------------------------------------------------------------
                    var fornecedorPush = fornecedorPushRepository.RegistraPush(adminModel.ID, pedido.ID, null, null, fornecedorID);

                    if (fornecedorPush != null)
                    {
                        var _fornecedor = fornecedorRepository.Get(fornecedorID);

                        fornecedorPush.NomeFornecedor = _fornecedor.Nome;
                        fornecedorPush.TelefonePrimario = _fornecedor.TelefonePrimario;
                    }

                    return Json(new { retorno = "OK", FornecedorID = fornecedorPush.IdFornecedor, NomeFornecedor = fornecedorPush.NomeFornecedor, TelefonePrimario = fornecedorPush.TelefonePrimario, DataCriacao = fornecedorPush.DataCriacao.Date.ToString("dd/MM/yyyy") });

                    //----------------------------------------------------------------------
                    #endregion
                }
                else
                {
                    return Json("O pedido não foi encontrado. Tente novamente mais tarde!");
                }
            }
            catch (Exception ex)
            {
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde! " + ex.Message);
            }
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarPrejuizo(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.StatusPagamentoID = Convert.ToInt32(Domain.Entities.Pedido.TodosStatusPagamento.Prejuizo);
                pedido.DataAtualizacao = DateTime.Now;
                pedidoRepository.Save(pedido);

                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                {
                    if (pedido.Boleto != null)
                    {
                        var boleto = pedido.Boleto;
                        boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                        boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                        if (boleto.RemessasBoleto.Count == 0)
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                            boleto.Processado = true;
                        }
                        else
                        {
                            boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                            boleto.Processado = false;
                        }
                        boletoRepository.Save(boleto);
                    }
                }


                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouPrejuizo);

                return Json("OK");
            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult AlterarPagamento(int id, string forma, string meio, string status, string data, string valor)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var statuspagamentoid = pedido.StatusPagamentoID;
                var datapagamento = new DateTime();
                DateTime.TryParse(data, out datapagamento);

                Decimal valorpagamento = 0;
                Decimal.TryParse(valor, out valorpagamento);

                if (datapagamento != new DateTime() && Convert.ToInt32(status) == (int)Pedido.TodosStatusPagamento.Pago)
                {
                    pedido.DataPagamento = datapagamento;
                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Pago;

                    if (valorpagamento > 0)
                        pedido.ValorPago = valorpagamento;
                    else
                        pedido.ValorPago = pedido.ValorTotal;

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouPago);
                }
                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                {
                    if (Convert.ToInt32(forma) != pedido.FormaPagamentoID && Convert.ToInt32(meio) != pedido.MeioPagamentoID)
                    {
                        var boleto = pedido.Boleto;
                        if (boleto != null)
                        {
                            if (boleto.RemessasBoleto.Count > 0)
                            {
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                boleto.Processado = false;
                            }
                            else
                            {
                                boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.Processado = true;
                            }
                            boletoRepository.Save(boleto);
                        }

                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.AlterouFormaPagamento);
                    }
                }
                if (Convert.ToInt32(forma) == (int)Pedido.FormasPagamento.Boleto && (pedido.Cliente.Exterior || pedido.Cliente.Documento.Length == 0))
                {
                    return Json("O pedido não pode ser alterado para boleto pois o cliente é do exterior ou não tem o CPF/CNPJ informado.");
                }

                pedido.FormaPagamentoID = Convert.ToInt32(forma);
                pedido.MeioPagamentoID = Convert.ToInt32(meio);
                pedido.StatusPagamentoID = Convert.ToInt32(status);
                pedido.DataAtualizacao = DateTime.Now;
                pedidoRepository.Save(pedido);


                if (pedido.FormaPagamento == Pedido.FormasPagamento.Boleto && pedido.Boleto == null)
                {
                    var random = new Random();
                    var nossoNumero = "";
                    do
                    {
                        nossoNumero = random.Next(99999999).ToString("00000000");
                    }
                    while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                    //var numeroDocumento = "2" + DateTime.Now.Year + pedido.ID.ToString("00000");
                    var numeroDocumento = "2000" + pedido.ID.ToString("00000");

                    var tipoID = Domain.Entities.Boleto.TodosTipos.PJ;
                    tipoID = (pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica) ? Domain.Entities.Boleto.TodosTipos.PF : Domain.Entities.Boleto.TodosTipos.PJ;

                    var boleto = new Boleto();
                    boleto.PedidoID = pedido.ID;
                    boleto.TipoID = (int)tipoID;
                    boleto.Carteira = Configuracoes.BOLETO_CARTEIRA;
                    boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA;
                    boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ;
                    boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE;
                    boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE;
                    boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL;
                    boleto.DataCriacao = DateTime.Now;
                    boleto.DataVencimento = Domain.Core.Funcoes.DataVencimentoUtil();
                    boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO;
                    boleto.NossoNumero = nossoNumero;
                    boleto.NumeroDocumento = numeroDocumento;
                    boleto.SacadoDocumento = pedido.Cliente.Documento;
                    boleto.SacadoNome = pedido.Cliente.Nome;
                    if (String.IsNullOrEmpty(pedido.Cliente.Bairro))
                        boleto.SacadoBairro = "";
                    else
                        boleto.SacadoBairro = pedido.Cliente.Bairro;

                    if (String.IsNullOrEmpty(pedido.Cliente.CEP))
                        boleto.SacadoCEP = "";
                    else
                        boleto.SacadoCEP = pedido.Cliente.CEP;

                    if (pedido.Cliente.Cidade == null)
                        boleto.SacadoCidade = "";
                    else
                        boleto.SacadoCidade = pedido.Cliente.Cidade.Nome;

                    if (String.IsNullOrEmpty(pedido.Cliente.Logradouro))
                        boleto.SacadoEndereco = "";
                    else
                        boleto.SacadoEndereco = pedido.Cliente.Logradouro + " " + pedido.Cliente.Numero + " " + pedido.Cliente.Complemento;

                    if (pedido.Cliente.Estado == null)
                        boleto.SacadoUF = "";
                    else
                        boleto.SacadoUF = pedido.Cliente.Estado.Sigla;

                    boleto.Comando = Domain.Entities.Boleto.Comandos.CriarRemessa;
                    boleto.Processado = false;
                    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.AguardandoPagamento;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Criado;
                    boleto.Valor = pedido.ValorTotal;

                    boletoRepository.Save(boleto);
                }

                var historico = "";
                if (pedido.StatusPagamentoID != statuspagamentoid)
                    historico += "<b>Status Pagamento: </b> de <i>" + Domain.Helpers.EnumHelper.GetDescription((Pedido.TodosStatusPagamento)statuspagamentoid) + "</i> para <i>" + Domain.Helpers.EnumHelper.GetDescription(pedido.StatusPagamento) + "</i>";
                if (historico.Length > 0)
                {
                    historico = "Pedido alterado por " + adminModel.Nome + "<br>" + historico;
                    var nota2 = new PedidoNota();
                    nota2.DataCriacao = DateTime.Now;
                    nota2.PedidoID = pedido.ID;
                    nota2.Observacoes = historico;
                    pedidoNotaRepository.Save(nota2);
                }

                return Json("OK");


            }
            else
                return Json("O pedido não pode ser alterado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Cancelar(int id, int statusCancelamentoID, string observacoes)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento != Pedido.TodosStatusProcessamento.Cancelado)
                {
                    if (pedido.TotalRepassesAberto == 0)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.CancelouPedido);

                        pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                        pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                        pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                        pedido.StatusCancelamentoID = statusCancelamentoID;
                        pedido.DataProcessamento = DateTime.Now;
                        pedido.DataAtualizacao = DateTime.Now;
                        pedidoRepository.Save(pedido);

                        pedido.EnviarEmailCancelamento();

                        var boleto = pedido.Boleto;
                        if (boleto != null)
                        {
                            boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                            boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                            if (boleto.RemessasBoleto.Count == 0)
                            {
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                                boleto.Processado = true;
                            }
                            else
                            {
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                boleto.Processado = false;
                            }
                            boletoRepository.Save(boleto);
                        }
                        if (!string.IsNullOrWhiteSpace(observacoes))
                        {
                            var nota = new PedidoNota();
                            nota.DataCriacao = DateTime.Now;
                            nota.PedidoID = pedido.ID;
                            nota.Observacoes =
                                "Cancelado por: "
                                + adminModel.Nome
                                + "<br>" + "Motivo: "
                                + Domain.Helpers.EnumHelper.GetDescription((TodosStatusCancelamento)statusCancelamentoID)
                                + "<br>" + observacoes;
                            pedidoNotaRepository.Save(nota);
                        }

                        var NovoPedidoFloricultura = pedidoFloriculturaRepository.GetByExpression(r => r.IdPedidoCPV == pedido.ID && r.StatusCancelamentoID == null).FirstOrDefault();

                        if (NovoPedidoFloricultura != null)
                        {
                            NovoPedidoFloricultura.StatusCancelamentoID = (int)PedidoFloricultura.TodosStatusCancelamento.Removido;
                            NovoPedidoFloricultura.StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Cancelado;
                            NovoPedidoFloricultura.StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.Cancelado;
                            NovoPedidoFloricultura.StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Cancelado;

                            pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                            pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                            {
                                PedidoFloricultura = NovoPedidoFloricultura,
                                DataCriacao = DateTime.Now,
                                Observacoes = "Pedido cancelado automaticamente por conta ddo cancelamento do peido no sistema CPV."
                            });
                        }

                        return Json("OK");
                    }
                    else
                    {
                        return Json("Este pedido tem repasses pagos ou aguardando pagamento. Você deve cancelar todos antes de concluir o pedido.");
                    }
                }
                else
                {
                    return Json("Este pedido já foi cancelado.");
                }
            }
            else
                return Json("O pedido não pode ser cancelado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Processar(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado)
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ProcessouPedido);

                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                    pedido.DataProcessamento = DateTime.Now;
                    pedido.DataAtualizacao = DateTime.Now;
                    pedidoRepository.Save(pedido);
                    return Json("OK");
                }
                else
                {
                    return Json("Este pedido já está sendo processado.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult Concluir(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                if (pedido.StatusProcessamento == Pedido.TodosStatusProcessamento.Processando)
                {
                    //if (pedido.StatusEntrega == TodosStatusEntrega.Entregue || pedido.StatusEntrega == TodosStatusEntrega.EntregaNaoRequerida || pedido.StatusEntrega == TodosStatusEntrega.Devolvido) {
                    if (pedido.StatusEntrega == TodosStatusEntrega.Entregue)
                    {
                        if (pedido.StatusPagamento == Pedido.TodosStatusPagamento.Pago || pedido.StatusPagamento == Pedido.TodosStatusPagamento.Estornado)
                        {
                            if (pedido.PedidoItens.Count == pedido.TotalRepassesAberto)
                            {
                                //Registra log do usuário
                                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ConcluiuPedido);

                                pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Concluido;
                                pedido.DataProcessamento = DateTime.Now;
                                pedido.DataAtualizacao = DateTime.Now;
                                pedidoRepository.Save(pedido);

                                //pedido.EnviarEmailPesquisa();
                                return Json("OK");
                            }
                            else
                            {
                                return Json("Este pedido não tem todos os repasses criados. Você deve criá-los antes de concluir o pedido.");
                            }
                        }
                        else
                        {
                            return Json("Este pedido tem que ter o status 'Pago' ou 'Estornado' para ser concluído. Altere o status, salve e tente novamente.");
                        }
                    }
                    else
                    {
                        return Json("O pedido tem que ter os status 'Entregue', 'Entrega não Requerida' ou 'Devolvido' para ser concluído. Altere o status, salve e tente novamente.");
                    }
                }
                else
                {
                    return Json("Este pedido precisa estar em processamento para ser concluído.");
                }
            }
            else
                return Json("O pedido não pode ser processado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPesquisa(int id)
        {
            if (!Domain.Core.Configuracoes.ENVIA_TRUSTVOX)
            {
                var pedido = pedidoRepository.Get(id);
                if (pedido != null)
                {
                    pedido.EnviarEmailPesquisa();
                    return Json("OK");
                }
                else
                    return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
            else
                return Json("A pesquisa não pode ser enviada. Ela é feita automaticamente pelo trustvox");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarBoleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                pedido.EnviarBoleto();
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailBoleto);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviarNFe(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailNFe);
                pedido.EnviarNFe();
                pedido.DataEmailNFe = DateTime.Now;
                pedidoRepository.Save(pedido);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult RegistraCliqueImpressaoBoleto(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ImprimiuBoleto);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailEntrega(int id, string local, string data, string pessoa, string recebido, string parentesco, string complemento)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var dataentrega = new DateTime();
                DateTime.TryParse(data, out dataentrega);
                if (dataentrega != new DateTime())
                {
                    var enviapesquisa = pedido.DataEntrega != dataentrega && pedido.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO";



                    pedido.LocalEntrega = local;
                    pedido.DataEntrega = dataentrega;
                    pedido.PessoaHomenageada = pessoa;
                    pedido.RecebidoPor = recebido;
                    pedido.Parentesco = parentesco;
                    pedido.ComplementoLocalEntrega = complemento;
                    pedido.DataEmailEntrega = DateTime.Now;
                    pedido.StatusEntrega = TodosStatusEntrega.Entregue;
                    pedidoRepository.Save(pedido);
                    pedido.EnviarEmailEntrega();

                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                    var NPS = npsREspository.GetByExpression(p => p.PedidoID == pedido.ID).FirstOrDefault();

                    if (NPS != null)
                    {

                        NPS.DataUltimoEnvioEmail = DateTime.Now;
                        NPS.EmailEnviado = true;
                        npsREspository.Save(NPS);
                    }

                    if (enviapesquisa)
                    {
                        if (Configuracoes.ENVIA_TRUSTVOX)
                        {
                            pedido.DataEnvioPesquisa = null;
                            pedidoRepository.Save(pedido);
                        }
                        else
                        {
                            pedido.EnviarEmailPesquisa();
                            pedido.DataEnvioPesquisa = DateTime.Now;
                            pedidoRepository.Save(pedido);
                        }
                    }
                    try
                    {
                        if (!pedido.Cliente.Exterior)
                        {
                            if (!string.IsNullOrEmpty(pedido.Cliente.CelularContatoSMS))
                            {
                                var retorno = "";

                                if (pedido.OrigemSite.ToUpper() == "LAÇOS FLORES")
                                    SMS.EnviaSMS("LACOS FLORES", pedido.Cliente.CelularContatoSMS, Configuracoes.MSG_ZENVIA_SMS);
                                else
                                    SMS.EnviaSMS("COROAS PARA VELORIO", pedido.Cliente.CelularContatoSMS, Configuracoes.MSG_ZENVIA_SMS);
                                return Json(retorno + "\nE-mail enviado com sucesso!");
                            }
                        }
                    }
                    catch
                    {

                    }

                    return Json("OK");
                }
                else
                    return Json("Data de entrega inválida!");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailFornecedor(int id, string local, string _pessoaHomenageada, int fornecedorID)
        {
            try
            {
                var pedido = pedidoRepository.Get(id);
                if (pedido != null)
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailFornecedor);

                    pedido.LocalEntrega = local;
                    if (string.IsNullOrEmpty(pedido.PessoaHomenageada))
                    {
                        pedido.PessoaHomenageada = "NÃO INFORMADO";
                    }
                    else
                        pedido.PessoaHomenageada = pedido.PessoaHomenageada;
                    pedido.FornecedorID = fornecedorID;
                    pedidoRepository.Save(pedido);
                    if (!String.IsNullOrEmpty(pedido.Fornecedor.EmailContato))
                    {
                        if (pedido.EnviarEmailFornecedor())
                            return Json("OK");
                        else
                            return Json("Não foi possível enviar o e-mail no momento. Tente novamente mais tarde.");
                    }
                    else
                    {
                        return Json("O fornecedor não tem um e-mail cadastrado de contato. Atualize e tente novamente.");
                    }
                }
                else
                    return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
            catch (Exception)
            {
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
            }
        }
        //----------------------------------------------------------------------
        public JsonResult EnviaEmailPedido(int id)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.ReenviouEmailPedido);

                if (!String.IsNullOrEmpty(pedido.Cliente.Email))
                {
                    pedido.EnviarPedidoEmail();
                    return Json("OK");
                }
                else
                {
                    return Json("O cliente não tem um e-mail cadastrado ou válido. Atualize e tente novamente.");
                }

            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
        //----------------------------------------------------------------------
        public string CriarNumero() {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                if (!new Pedido().NumeroExiste(numero.ToString())) {
                    return numero.ToString();
                }
            } while (true);
        }
        //----------------------------------------------------------------------
        public string CriarNumeroFloricultura()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                if (!new PedidoFloricultura().NumeroExiste(numero.ToString()))
                {
                    return numero.ToString();
                }
            } while (true);
        }
        //----------------------------------------------------------------------
        #endregion
    }
    //==========================================================================
}

