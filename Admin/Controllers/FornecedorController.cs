﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using Domain.Repositories;
using Artem.Google.Net;
using MvcExtensions.Security.Filters;
using System.Linq.Expressions;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Financeiro|Atendimento|FloriculturaMisto", "/Home/AccessDenied")]
    public class FornecedorController : CrudController<Fornecedor>
    {
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<NPSRating> npsRepository;
        private IPersistentRepository<NPSRating_NEW> npsNewRepository;
        private IPersistentRepository<FornecedorCidade> fornecedorCidadeRepository;
        private IPersistentRepository<FornecedorCaracteristica> fornecedorCaracteristicaRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Administrador> adminRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<AdministradorCarteira> AdministradorCarteiraRepository;
        //private IPersistentRepository<Banco> bancoRepository;

        private AdminModel adminModel;

        public FornecedorController(ObjectContext context) : base(context)
        {
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            npsRepository = new PersistentRepository<NPSRating>(context);
            npsNewRepository = new PersistentRepository<NPSRating_NEW>(context);
            fornecedorCidadeRepository = new PersistentRepository<FornecedorCidade>(context);
            fornecedorCaracteristicaRepository = new PersistentRepository<FornecedorCaracteristica>(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            produtoRepository = new PersistentRepository<Produto>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            npsREspository = new PersistentRepository<NPSRating>(context);
            adminRepository = new PersistentRepository<Administrador>(context);
            AdministradorCarteiraRepository = new PersistentRepository<AdministradorCarteira>(context);
            //bancoRepository = new PersistentRepository<Banco>(context);

            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = fornecedorRepository.GetAll().OrderBy(p => p.Nome).ToList();

            if (Request.QueryString["termo"] != null)
            {
                var termo = Server.UrlDecode(Request.QueryString["termo"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower()).Replace("%40", "@");
                if (!String.IsNullOrEmpty(termo))                    
                    resultado = resultado.Where(c => c.Nome.Trim().ToLower().Contains(termo)).OrderBy(c => c.Nome).ToList();
            }
            if (Request.QueryString["email"] != null)
            {
                var email = Request.QueryString["email"].ToLower();
                if (!String.IsNullOrEmpty(email))                    
                    resultado = resultado.Where(c => c.EmailContato != null && c.EmailContato.Trim().ToLower().Contains(email)).OrderBy(c => c.Nome).ToList();
            }
            if (Request.QueryString["favorecido"] != null)
            {
                var favorecido = Server.UrlDecode(Request.QueryString["favorecido"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower()).Replace("%40", "@");
                if (!String.IsNullOrEmpty(favorecido))                    
                    resultado = fornecedorRepository.GetByExpression(c => c.FavorecidoPrimario.Trim().ToLower().Contains(favorecido) || c.FavorecidoSecundario.Trim().ToLower().Contains(favorecido)).OrderBy(c => c.Nome).ToList();
            }
            if (Request.QueryString["cidade"] != null)
            {
                var cidade = Server.UrlDecode(Request.QueryString["cidade"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower()).Replace("%40", "@");
                if (!String.IsNullOrEmpty(cidade))                    
                    resultado = resultado.Where(c => c.FornecedorCidades.Count(d => d.Cidade.Nome.Trim().ToLower().Contains(cidade)) > 0).OrderBy(c => c.Nome).ToList();
            }
            if (Request.QueryString["banco"] != null)
            {
                var banco = Server.UrlDecode(Request.QueryString["banco"].Trim().ToLower());
                if (!String.IsNullOrEmpty(Request.QueryString["banco"]))                    
                    resultado = resultado.Where(c => (!String.IsNullOrEmpty(c.BancoPrimario) && c.BancoPrimario.Trim().ToLower().Contains(banco)) || (!String.IsNullOrEmpty(c.BancoSecundario) && c.BancoSecundario.Trim().ToLower().Contains(banco))).OrderBy(c => c.Nome).ToList();
            }

            var fornecedorID = 0;
            Int32.TryParse(Request.QueryString["fornecedorID"], out fornecedorID);

            bool valorrepasse = false;
            Boolean.TryParse(Request.QueryString["valorrepasse"], out valorrepasse);

            bool datarepasse = false;
            Boolean.TryParse(Request.QueryString["datarepasse"], out datarepasse);

            bool repasseaberto = false;
            Boolean.TryParse(Request.QueryString["repasseaberto"], out repasseaberto);

            var fluxo = 0;
            Int32.TryParse(Request.QueryString["fluxo"], out fluxo);

            //TODO: ARRUMAR A BUSCA

            if (valorrepasse)
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => d.ValorRepasse == 0) > 1 || c.Pedidoes.Count(d => d.ValorRepasse == 0) > 1).ToList();

            if (datarepasse)
                resultado = resultado.Where(c => c.PedidoEmpresas.Count(d => !d.DataRepasse.HasValue) > 1 || c.Pedidoes.Count(d => !d.DataRepasse.HasValue) > 1).ToList();

            if (repasseaberto)
               resultado = resultado.Where(c => c.Repasses.Where(p => p.StatusID == (int)Domain.Entities.Repasse.TodosStatus.AguardandoPagamento).Any()).ToList();

            if (fornecedorID > 0)
                resultado = resultado.Where(c => c.ID == fornecedorID).ToList();

            if (fluxo > 0)
                resultado = resultado.Where(c => c.FluxoPagamentoID == fluxo).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }

            // ViewBag.Bancos = lstBancos;
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        public ActionResult NPSGeralNew(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;

            List<Administrador> lstAdmin = new List<Administrador>();
            foreach (var Item in adminRepository.GetAll().OrderBy(n => n.Nome).ToList())
            {
                lstAdmin.Add(new Administrador
                {
                    ID = Item.ID,
                    Nome = Item.Nome
                });
            }

            ViewBag.LstAdmins = lstAdmin;

            DateTime DataInicial = DateTime.Now.AddDays(-30);
            DateTime DataFinal = DateTime.Now;

            if ((string.IsNullOrEmpty(Request.QueryString["datainicial"]) && string.IsNullOrEmpty(Request.QueryString["datafinal"])) && Request.QueryString.Count > 0)
            {
                DataInicial = DateTime.Parse("01/01/1900");
                DataFinal = DateTime.Now;
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["datainicial"]))
                {
                    try
                    {
                        DataInicial = DateTime.Parse(Request.QueryString["datainicial"]);
                    }
                    catch (Exception) { }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["datafinal"]))
                {
                    try
                    {
                        DataFinal = DateTime.Parse(Request.QueryString["datafinal"]);
                        DataFinal = DataFinal.AddMinutes(1439);
                    }
                    catch (Exception) { }
                }
            }

            List<FornecedorNPS> ListaGeralFornecedorNPS = new List<FornecedorNPS>();

            var ResultSearch = npsNewRepository.GetByExpression(p => p.Pedido.DataCriacao >= DataInicial && p.Pedido.DataCriacao <= DataFinal && p.Nota > 0 && p.Pedido.StatusCancelamentoID == null).ToList();

            TimeSpan ts = DataFinal - DataInicial;
            if (ts.Days == 0)
            {
                ViewBag.SpanSays = 1;
            }
            else
            {
                ViewBag.SpanSays = ts.Days;
            }

            foreach (var Item in ResultSearch)
            {
                var Fornecedor = fornecedorRepository.Get((int)Item.FornecedorID);

                string Resposta = string.Empty;
                foreach (var NPS in Item.NPSRating_X_NPSPergunta_NEW)
                {
                    Resposta += NPS.NPSPergunta_NEW.Pergunta + "<br />";
                }

                string Cidade = string.Empty;
                if (Request.QueryString.Count > 0)
                {
                    foreach (var cidade in Fornecedor.FornecedorCidades.OrderBy(c => c.Cidade.Nome).OrderBy(c => c.Cidade.Estado.Sigla))
                    {
                        Cidade += " " + cidade.Cidade.NomeCompleto;
                        Cidade += "<br>";
                    }
                }
                else
                {
                    Cidade = "";
                }

                ListaGeralFornecedorNPS.Add(new FornecedorNPS {
                    IdFornecedor = Fornecedor.ID,
                    Floricultura = Fornecedor.Nome,
                    Nota = Item.Nota,
                    Cidade = Cidade,
                    ContaPrimario = Fornecedor.ContaPrimario,
                    EmailContato = Fornecedor.EmailContato,
                    Estado = Fornecedor.Estado.Sigla,
                    PessoaContato = Fornecedor.PessoaContato,
                    Telefone = Fornecedor.TelefonePrimario,
                    IdAdmin = Item.Pedido.AdministradorID,
                    Obs = Item.Obs,
                    DataEnvioAvaliacao = (DateTime)Item.DataEnvioAvaliacao,
                    NrPedido = Item.Pedido.Numero,
                    IdPedido = Item.PedidoID,
                    Respostas = Resposta,
                    NomeProcessou = Item.Pedido.AdministradorNome,
                    DataPedido = Item.Pedido.DataCriacao
                });

            }

            if (!string.IsNullOrEmpty(Request.QueryString["termo"]))
            {
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.Floricultura.ToLower().Contains(Request.QueryString["termo"].ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["email"]))
            {
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.EmailContato != null && p.EmailContato.ToLower().Contains(Request.QueryString["email"].ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["cidade"]))
            {
                var cidade = Server.UrlDecode(Request.QueryString["cidade"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower()).Replace("%40", "@");
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.Cidade.ToLower().Contains(cidade.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["IdAdministrador"]))
            {
                int IdAdmin = int.Parse(Request.QueryString["IdAdministrador"].ToString());
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.IdAdmin == IdAdmin).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["Nota"]))
            {
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.Nota == int.Parse(Request.QueryString["Nota"].ToString())).ToList();
            }

            ViewBag.ListaGeralFornecedorNPSCalculada = ListaGeralFornecedorNPS.OrderBy(p => p.DataEnvioAvaliacao).Reverse().ToList();

            return View();
        }

        public ActionResult NPSNew(params object[] args)
        {
            DateTime DataInicial = DateTime.Now.AddDays(-30);
            DateTime DataFinal = DateTime.Now;
            System.Collections.Generic.IEnumerable<Admin.ViewModels.FornecedorNPS> resultTop30NPS = new List<Admin.ViewModels.FornecedorNPS>();
            System.Collections.Generic.IEnumerable<Admin.ViewModels.FornecedorNPS> resultPiores30NPS = new List<Admin.ViewModels.FornecedorNPS>();
            List<Administrador> lstAdmin = new List<Administrador>();
            List<FornecedorNPS> ListaGeralFornecedorNPSCalculada = new List<FornecedorNPS>();
            decimal PercentPromotores = 0;
            decimal PercentDetratores = 0;
            decimal NPSGeral = 0;

            ViewBag.Busca = false;
            ViewBag.Admin = adminModel.CurrentAdministrador;

            #region PREENCHE LISTA FILTRO DE COLABORADORES

            foreach (var Item in adminRepository.GetAll().OrderBy(n => n.Nome).ToList())
            {
                lstAdmin.Add(new Administrador
                {
                    ID = Item.ID,
                    Nome = Item.Nome
                });
            }

            ViewBag.LstAdmins = lstAdmin;

            #endregion

            #region PREENCHE FILTRO DE DATA

            if ((string.IsNullOrEmpty(Request.QueryString["datainicial"]) && string.IsNullOrEmpty(Request.QueryString["datafinal"])) && Request.QueryString.Count > 0)
            {
                DataInicial = DateTime.Parse("01/01/1900");
                DataFinal = DateTime.Now;
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["datainicial"]))
                {
                    try
                    {
                        DataInicial = DateTime.Parse(Request.QueryString["datainicial"]);
                    }
                    catch (Exception) { }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["datafinal"]))
                {
                    try
                    {
                        DataFinal = DateTime.Parse(Request.QueryString["datafinal"]);
                        DataFinal = DataFinal.AddMinutes(1439);
                    }
                    catch (Exception) { }
                }
            }


            #endregion

            #region POPULA LISTA GERAL NPS

            List<FornecedorNPS> ListaGeralFornecedorNPS = new List<FornecedorNPS>();

            var ResultSearch = npsNewRepository.GetByExpression(p => p.Pedido.DataCriacao >= DataInicial && p.Pedido.DataCriacao <= DataFinal && p.Nota > 0 && p.Pedido.StatusCancelamentoID == null).ToList();

            TimeSpan ts = DataFinal - DataInicial;
            if (ts.Days == 0)
            {
                ViewBag.SpanSays = 1;
            }
            else
            {
                ViewBag.SpanSays = ts.Days;
            }

            foreach (var Item in ResultSearch)
            {
                var Fornecedor = Item.Fornecedor;

                string Cidade = string.Empty;
                if (Request.QueryString.Count > 0)
                {
                    foreach (var cidade in Fornecedor.FornecedorCidades.OrderBy(c => c.Cidade.Nome).OrderBy(c => c.Cidade.Estado.Sigla))
                    {
                        Cidade += " " + cidade.Cidade.NomeCompleto;
                        Cidade += "<br>";
                    }
                }
                else
                {
                    Cidade = "";
                }

                ListaGeralFornecedorNPS.Add(new FornecedorNPS
                {
                    IdFornecedor = Fornecedor.ID,
                    Floricultura = Fornecedor.Nome,
                    Nota = Item.Nota,
                    Cidade = Cidade,
                    ContaPrimario = Fornecedor.ContaPrimario,
                    EmailContato = Fornecedor.EmailContato,
                    Estado = Fornecedor.Estado.Sigla,
                    PessoaContato = Fornecedor.PessoaContato,
                    Telefone = Fornecedor.TelefonePrimario,
                    IdAdmin = Item.Pedido.AdministradorID,
                    Obs = Item.Obs,
                    DataEnvioAvaliacao = (DateTime)Item.DataEnvioAvaliacao,
                    NrPedido = Item.Pedido.Numero,
                    IdPedido = Item.PedidoID   ,                 
                    NomeProcessou = Item.Pedido.AdministradorNome,
                    DataPedido = Item.Pedido.DataCriacao
                });
            }

            #endregion

            #region FILTRO

            if (!string.IsNullOrEmpty(Request.QueryString["termo"]))
            {
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.Floricultura.ToLower().Contains(Request.QueryString["termo"].ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["email"]))
            {
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.EmailContato.ToLower().Contains(Request.QueryString["email"].ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["cidade"]))
            {
                var cidade = Server.UrlDecode(Request.QueryString["cidade"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower()).Replace("%40", "@");
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.Cidade.ToLower().Contains(cidade.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["IdAdministrador"]))
            {
                int IdAdmin = int.Parse(Request.QueryString["IdAdministrador"].ToString());
                ListaGeralFornecedorNPS = ListaGeralFornecedorNPS.Where(p => p.IdAdmin == IdAdmin).ToList();
            }

            #endregion

            #region PREENCHE NPS GERAL

            PercentPromotores = (decimal)((decimal)ListaGeralFornecedorNPS.Where(r => r.Nota == 9 || r.Nota == 10).Count() / (decimal)ListaGeralFornecedorNPS.Count) * 100;
            PercentDetratores = (decimal)((decimal)ListaGeralFornecedorNPS.Where(r => r.Nota >= 0 && r.Nota <= 6).Count() / (decimal)ListaGeralFornecedorNPS.Count) * 100;
            NPSGeral = PercentPromotores - PercentDetratores;

            if (NPSGeral < 0)
                NPSGeral = 0;

            ViewBag.NPSGeral = NPSGeral;

            #endregion

            #region PREENCHE NPS POR FORNECEDOR

            foreach (var FornecedorID in ListaGeralFornecedorNPS.Select(x => x.IdFornecedor).Distinct())
            {
                var Fornecedor = ListaGeralFornecedorNPS.Where(p => p.IdFornecedor == FornecedorID).FirstOrDefault();

                var LstFornecedorFiltrada = ListaGeralFornecedorNPS.Where(r => r.IdFornecedor == FornecedorID).ToList();

                var PercentPromotoresFornecedor = (decimal)((decimal)LstFornecedorFiltrada.Where(r => r.Nota == 9 || r.Nota == 10).ToList().Count() / (decimal)LstFornecedorFiltrada.Count) * 100;
                var PercentDetratoresFornecedor = (decimal)((decimal)LstFornecedorFiltrada.Where(r => r.Nota >= 0 && r.Nota <= 6).ToList().Count() / (decimal)LstFornecedorFiltrada.Count) * 100;
                var NPSGeralFornecedor = PercentPromotoresFornecedor - PercentDetratoresFornecedor;

                if (NPSGeralFornecedor < 0)
                    NPSGeralFornecedor = 0;

               ListaGeralFornecedorNPSCalculada.Add(new FornecedorNPS { Nome = Fornecedor.Floricultura, Cidade = Fornecedor.Cidade, Telefone = Fornecedor.Telefone, PessoaContato = Fornecedor.PessoaContato, Estado = Fornecedor.Estado, EmailContato = Fornecedor.EmailContato, ContaPrimario = Fornecedor.ContaPrimario, Floricultura = Fornecedor.Floricultura, IdFornecedor = Fornecedor.IdFornecedor, Nota = NPSGeralFornecedor, IdAdmin = Fornecedor.IdAdmin, QtdNota = LstFornecedorFiltrada.Count });
            }

            ViewBag.ListaGeralFornecedorNPSCalculada = ListaGeralFornecedorNPSCalculada.OrderBy(p => p.Nota).Reverse().ToList();

            ViewBag.Busca = false;

            #endregion

            #region POPULA LISTA PIOR E MELHOR

            resultTop30NPS = ListaGeralFornecedorNPSCalculada.OrderBy(p => p.Nota).Reverse().Take(30);
            resultPiores30NPS = ListaGeralFornecedorNPSCalculada.OrderBy(p => p.Nota).Take(30);

            ViewBag.resultTop30NPS = resultTop30NPS;
            ViewBag.resultPiores30NPS = resultPiores30NPS;

            #endregion

            return View();
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            Fornecedor fornecedor = null;
            if (id.HasValue)
            {
                fornecedor = fornecedorRepository.Get(id.Value);
                try
                {
                    ViewBag.Cidades = fornecedor.FornecedorCidades.Select(c => c.Cidade).OrderBy(e => e.Nome);
                }
                catch
                {
                    ViewBag.Cidades = new List<Cidade>();
                }
                ViewBag.TodasCidades = cidadeRepository.GetByExpression(e => e.EstadoID == fornecedor.EstadoID).OrderBy(e => e.Nome);
            }
            else
            {
                ViewBag.Cidades = new List<Cidade>();
                ViewBag.TodasCidades = new List<Cidade>();
            }
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(fornecedor);
        }

        [HttpGet]
        public ActionResult Repasse(int id)
        {
            Fornecedor fornecedor = fornecedorRepository.Get(id);
            if (fornecedor != null)
            {
                var pedidos = new PedidoModel(fornecedor).pedidos;
                var datainicial = new DateTime();
                DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

                var datafinal = new DateTime();
                DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

                if (datainicial != new DateTime())
                    pedidos = pedidos.Where(c => c.DataRepasse >= datainicial).ToList();

                if (datafinal != new DateTime())
                {
                    datafinal = datafinal.AddDays(1).AddSeconds(-1);
                    pedidos = pedidos.Where(c => c.DataRepasse <= datafinal).ToList();
                }

                bool valorrepasse = false;
                Boolean.TryParse(Request.QueryString["valorrepasse"], out valorrepasse);

                bool datarepasse = false;
                Boolean.TryParse(Request.QueryString["datarepasse"], out datarepasse);

                if (valorrepasse)
                    pedidos = pedidos.Where(c => c.ValorRepasse == 0).ToList();

                if (datarepasse)
                    pedidos = pedidos.Where(c => !c.DataRepasse.HasValue).ToList();


                ViewBag.Fornecedor = fornecedor;
                return View(pedidos);
            }
            else
                return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Pedidos()
        {
            int _intNumPedCpv = 0;
            int _intNumPedLc = 0;
            var fornecedores = fornecedorRepository.GetByExpression
                (
                    c => c.PedidoEmpresas.Count
                        (e => e.StatusEntregaID == (int)TodosStatusEntrega.EmProducao
                        && (e.StatusID == (int)PedidoEmpresa.TodosStatus.PedidoEmAnalise || e.StatusID == (int)PedidoEmpresa.TodosStatus.NovoPedido))  > 0 
                        || c.Pedidoes.Count
                            (p => p.StatusEntregaID == (int)TodosStatusEntrega.EmProducao 
                            && p.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando) > 0
                )
                .OrderBy(c => c.Nome)
                .ToList();

            foreach (var forn in fornecedores)
            {
                if (forn.Pedidoes != null)
                {
                    _intNumPedCpv = forn.Pedidoes.Count();

                    _intNumPedCpv += _intNumPedCpv;
                }

                if (forn.PedidoEmpresas != null)
                {
                    _intNumPedLc = forn.PedidoEmpresas.Count();

                    _intNumPedLc += _intNumPedLc;
                }
            }

            ViewBag.TotalPedidosCpv = _intNumPedCpv;
            ViewBag.TotalPedidosLc = _intNumPedLc;

            return View(fornecedores);
        }

        [HttpGet]
        public ActionResult PedidosSite(int fornecedorID)
        {
            Fornecedor fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null)
            {
                var pedidos = fornecedor.Pedidoes.Where(p => p.StatusEntregaID == (int)TodosStatusEntrega.EmProducao && p.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando).OrderBy(c => c.DataSolicitada).ToList();

                ViewBag.Fornecedor = fornecedor;
                return View(pedidos);
            }
            else
                return RedirectToAction("List");
        }

        public JsonResult SalvaPedidoSite(int id, string observacao, string recebido, string parentesco, string data, int status)
        {
            var pedido = pedidoRepository.Get(id);
            if (pedido != null)
            {
                var enviatrustvox = false;
                if (status == (int)TodosStatusEntrega.Entregue)
                {
                    var dataentrega = new DateTime();
                    DateTime.TryParse(data, out dataentrega);
                    if (dataentrega != new DateTime())
                    {
                        pedido.DataEntrega = dataentrega;
                        pedido.RecebidoPor = recebido;
                        pedido.ObsEntrega = observacao;
                        pedido.ParentescoRecebidoPor = parentesco;
                        pedido.DataEmailEntrega = DateTime.Now;
                        pedido.EnviarEmailEntrega();
                       

                        var NPS = npsREspository.GetByExpression(p => p.PedidoID == pedido.ID).FirstOrDefault();

                        if (NPS != null)
                        {
                            NPS.EmailEnviado = true;
                            NPS.DataUltimoEnvioEmail = DateTime.Now;
                            npsREspository.Save(NPS);
                        }

                        enviatrustvox = pedido.DataEntrega != dataentrega;
                    }
                }

                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, pedido.ID, null, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                pedido.StatusEntregaID = status;
                pedido.ParentescoRecebidoPor = parentesco;
                pedido.RecebidoPor = recebido;
                pedido.ObsEntrega = observacao;
                pedidoRepository.Save(pedido);

                if (enviatrustvox)
                    Domain.Core.TrustVox.TrustVox.EnviaTrustVox(pedido);

                return Json(new { sucesso = true, mensagem = "Pedido alterado com sucesso!" });
            }
            else
            {
                return Json(new { sucesso = false, mensagem = "Problema ao alterar o pedido." });
            }

        }
        [HttpGet]
        public ActionResult PedidosEmpresa(int fornecedorID)
        {
            Fornecedor fornecedor = fornecedorRepository.Get(fornecedorID);
            if (fornecedor != null)
            {
                var pedidos = fornecedor.PedidoEmpresas.Where(e => e.StatusEntregaID == (int)TodosStatusEntrega.EmProducao && (e.StatusID == (int)PedidoEmpresa.TodosStatus.PedidoEmAnalise || e.StatusID == (int)PedidoEmpresa.TodosStatus.NovoPedido)).OrderBy(c => c.DataSolicitada).ToList();

                ViewBag.Fornecedor = fornecedor;
                return View(pedidos);
            }
            else
                return RedirectToAction("List");
        }
        public JsonResult SalvaPedidoEmpresa(int id, string observacao, string recebido, string parentesco, string data, int status)
        {
            var pedido = pedidoEmpresaRepository.Get(id);
            if (pedido != null)
            {
                if (status == (int)TodosStatusEntrega.Entregue)
                {
                    var dataentrega = new DateTime();
                    DateTime.TryParse(data, out dataentrega);
                    if (dataentrega != new DateTime())
                    {
                        pedido.DataEntrega = dataentrega;
                        pedido.EnviarEmailEntrega();
                    }
                }

                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, pedido.ID, null, null, AdministradorPedido.Acao.EnviouEmailEntrega);

                pedido.StatusEntregaID = status;
                pedido.Parentesco = parentesco;
                pedido.ParentescoRecebidoPor = recebido;
                pedido.ObsEntrega = observacao;
                pedido.DataEmailEntrega = DateTime.Now;
                pedidoEmpresaRepository.Save(pedido);


                return Json(new { sucesso = true, mensagem = "Pedido alterado com sucesso!" });
            }
            else
            {
                return Json(new { sucesso = false, mensagem = "Problema ao alterar o pedido." });
            }

        }
        [HttpGet]
        public ActionResult Repasses()
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;

            var resultado = repasseRepository.GetByExpression(c => c.PedidoEmpresaID.HasValue || c.PedidoItemID.HasValue).OrderBy(c => c.Fornecedor.Nome).OrderByDescending(p => p.Fornecedor.Repasses.Count(c => c.StatusID == (int)Domain.Entities.Repasse.TodosStatus.AguardandoPagamento)).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datainicialped = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialped"], out datainicialped);

            var datafinalped = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalped"], out datafinalped);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var numero = Request.QueryString["numero"];
            var email = Request.QueryString["email"];
            var fornecedor = Request.QueryString["fornecedor"];

            var fornecedorid = 0;
            Int32.TryParse(Request.QueryString["fornecedorid"], out fornecedorid);


            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataRepasse >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataRepasse <= datafinal).ToList();
            }

            if (datainicialped != new DateTime())
                resultado = resultado.Where(c => (c.PedidoEmpresaID.HasValue && c.PedidoEmpresa.DataCriacao >= datainicialped) || (c.PedidoItemID.HasValue && c.PedidoItem.Pedido.DataCriacao >= datainicialped)).ToList();

            if (datafinalped != new DateTime())
            {
                datafinal = datafinalped.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => (c.PedidoEmpresaID.HasValue && c.PedidoEmpresa.DataCriacao <= datafinalped) || (c.PedidoItemID.HasValue && c.PedidoItem.Pedido.DataCriacao <= datafinalped)).ToList();
            }

            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList();

            if (!String.IsNullOrEmpty(email))
            {

                int pedidoempresaid = 0;
                Int32.TryParse(numero, out pedidoempresaid);
                if (pedidoempresaid > 0)
                    resultado = resultado.Where(c => c.PedidoEmpresaID == pedidoempresaid).ToList();
                else
                {
                    var pedidoitem = pedidoItemRepository.GetByExpression(c => c.Pedido.Cliente.Email == email).Select(c => c.ID).ToList();
                    resultado = resultado.Where(c => pedidoitem.Count(s => s == c.PedidoItemID) > 0).ToList();
                }
            }

            if (!String.IsNullOrEmpty(numero))
            {
                int pedidoempresaid = 0;
                Int32.TryParse(numero, out pedidoempresaid);
                if (pedidoempresaid > 0)
                    resultado = resultado.Where(c => c.PedidoEmpresaID == pedidoempresaid).ToList();
                else
                {
                    var pedidoitem = pedidoItemRepository.GetByExpression(c => c.Pedido.Numero == numero).Select(c => c.ID).ToList();
                    resultado = resultado.Where(c => pedidoitem.Count(s => s == c.PedidoItemID) > 0).ToList();
                }
            }

            if (!String.IsNullOrEmpty(fornecedor))
                resultado = resultado.Where(c => c.FornecedorID.HasValue && c.Fornecedor.Nome.Trim().ToUpper().Contains(fornecedor.Trim().ToUpper())).ToList();

            if (fornecedorid > 0)
                resultado = resultado.Where(c => c.FornecedorID.HasValue && c.FornecedorID == fornecedorid).ToList();

            bool valorrepasse = false;
            Boolean.TryParse(Request.QueryString["valorrepasse"], out valorrepasse);

            bool datarepasse = false;
            Boolean.TryParse(Request.QueryString["datarepasse"], out datarepasse);

            if (valorrepasse)
                resultado = resultado.Where(c => c.ValorRepasse == 0).ToList();

            if (datarepasse)
                resultado = resultado.Where(c => !c.DataRepasse.HasValue).ToList();


            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public ActionResult Datas(int id)
        {
            List<Repasse> resultado = new List<Domain.Entities.Repasse>();

            var datainicial = DateTime.Now.AddDays(-5);
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            resultado = repasseRepository.GetByExpression(c => (c.PedidoEmpresaID.HasValue || c.PedidoItemID.HasValue) && c.DataCriacao >= datainicial && c.FornecedorID == id && c.StatusID != (int)Domain.Entities.Repasse.TodosStatus.Cancelado).OrderByDescending(p => p.DataCriacao).ToList();

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datainicialpedido = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialpedido"], out datainicialpedido);

            var datafinalpedido = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalpedido"], out datafinalpedido);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataRepasse >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataRepasse <= datafinal).ToList();
            }

            if (datainicialpedido != new DateTime())
                resultado = resultado.Where(c => (c.PedidoItemID.HasValue && c.PedidoItem.Pedido.DataCriacao >= datainicialpedido) || (c.PedidoEmpresaID.HasValue && c.PedidoEmpresa.DataCriacao >= datainicialpedido)).ToList();

            if (datafinalpedido != new DateTime())
            {
                datafinalpedido = datafinalpedido.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => (c.PedidoItemID.HasValue && c.PedidoItem.Pedido.DataCriacao <= datafinalpedido) || (c.PedidoEmpresaID.HasValue && c.PedidoEmpresa.DataCriacao <= datafinalpedido)).ToList();
            }

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);
            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList();

            var TipoPedido = 0;
            Int32.TryParse(Request.QueryString["TipoPedido"], out TipoPedido);
            if (TipoPedido > 0)
            {
                if(TipoPedido == 1)
                    resultado = resultado.Where(c => c.PedidoItemID != null).ToList();

                if (TipoPedido == 2)
                    resultado = resultado.Where(c => c.PedidoEmpresaID != null).ToList();
            }
                

            ViewBag.Total = resultado.Sum(c => c.ValorRepasse).ToString("C2");
            Fornecedor fornecedor = fornecedorRepository.Get(id);
            ViewBag.Fornecedor = fornecedor;
            ViewBag.Admin = adminModel.CurrentAdministrador;
            return View(resultado);
        }

        [ValidateInput(false)]
        public override ActionResult Edit(Fornecedor fornecedor)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            if (fornecedorRepository.GetByExpression(c => c.ID != fornecedor.ID && c.EmailContato.Trim().ToLower() == fornecedor.EmailContato.Trim().ToLower()).Count() == 0)
            {
                if (!string.IsNullOrEmpty(Request.Form["ValorMinimoDeCoroa"]))
                {
                    fornecedor.ValorMinimoDeCoroa = decimal.Parse(Request.Form["ValorMinimoDeCoroa"]);
                }
                else
                {
                    fornecedor.ValorMinimoDeCoroa = null;
                }

                if (fornecedor.BancoPrimario != null)
                    fornecedor.BancoPrimario = fornecedor.BancoPrimario.ToUpper();
                if (fornecedor.BancoSecundario != null)
                    fornecedor.BancoSecundario = fornecedor.BancoSecundario.ToUpper();
                if (fornecedor.TipoContaPrimario != null)
                    fornecedor.TipoContaPrimario = fornecedor.TipoContaPrimario.ToUpper();
                if (fornecedor.TipoContaSecundario != null)
                    fornecedor.TipoContaSecundario = fornecedor.TipoContaSecundario.ToUpper();

                if (fornecedor.OperacaoPrimario != null)
                    fornecedor.OperacaoPrimario = fornecedor.OperacaoPrimario.ToUpper();
                if (fornecedor.OperacaoSecundario != null)
                    fornecedor.OperacaoSecundario = fornecedor.OperacaoSecundario.ToUpper();
                if (fornecedor.DigitoAgenciaPrimario != null)
                    fornecedor.DigitoAgenciaPrimario = fornecedor.DigitoAgenciaPrimario.ToUpper();
                if (fornecedor.DigitoAgenciaSecundario != null)
                    fornecedor.DigitoAgenciaSecundario = fornecedor.DigitoAgenciaSecundario.ToUpper();

                fornecedorRepository.Save(fornecedor);


                foreach (var fornecedorcidade in fornecedor.FornecedorCidades.ToList())
                    fornecedorCidadeRepository.Delete(fornecedorcidade);

                if (Request.Form["box2View"] != null)
                {
                    foreach (var id in Request.Form["box2View"].Split(','))
                    {
                        int _id = 0;
                        Int32.TryParse(id.ToString(), out _id);
                        if (_id > 0)
                        {
                            fornecedor.FornecedorCidades.Add(new FornecedorCidade() { CidadeID = Convert.ToInt32(_id), FornecedorID = fornecedor.ID });
                        }
                    }
                    fornecedorRepository.Save(fornecedor);
                }


                foreach (var caracteristica in fornecedor.FornecedorCaracteristicas.ToList())
                    fornecedorCaracteristicaRepository.Delete(caracteristica);

                if (Request.Form["caracteristicas"] != null)
                {
                    foreach (var id in Request.Form["caracteristicas"].Split(','))
                    {
                        int _id = 0;
                        Int32.TryParse(id.ToString(), out _id);
                        if (_id > 0)
                        {
                            fornecedor.FornecedorCaracteristicas.Add(new FornecedorCaracteristica() { CaracteristicaID = Convert.ToInt32(_id), FornecedorID = fornecedor.ID });
                        }
                    }
                    fornecedorRepository.Save(fornecedor);
                }

                var forcarAtualizacao = Request.Form["forcarAtualizacao"] == "on";
                if (!String.IsNullOrEmpty(fornecedor.CEP))
                {
                    if (fornecedor.Latitude == null || fornecedor.Latitude == 0 || fornecedor.Longitude == null || fornecedor.Longitude == 0 || forcarAtualizacao)
                    {
                        if (fornecedor.Estado != null)
                        {
                            GeoRequest request = new GeoRequest(fornecedor.CEP + "," + fornecedor.Estado.Sigla + ",Brasil");
                            GeoResponse response = request.GetResponse();
                            GeoLocation location = response.Results[0].Geometry.Location;
                            fornecedor.Latitude = Convert.ToDecimal(location.Latitude);
                            fornecedor.Longitude = Convert.ToDecimal(location.Longitude);
                            fornecedorRepository.Save(fornecedor);
                        }
                    }
                }

                

                return RedirectToAction("List", new { cod = "SaveSucess" });
            }
            else
            {
                ViewBag.Erro = "O e-mail digitado já existe para outro fornecedor.";
                try
                {
                    ViewBag.Cidades = fornecedor.FornecedorCidades.Select(c => c.Cidade).OrderBy(e => e.Nome);
                }
                catch
                {
                    ViewBag.Cidades = new List<Cidade>();
                }
                ViewBag.TodasCidades = cidadeRepository.GetByExpression(e => e.EstadoID == fornecedor.EstadoID).OrderBy(e => e.Nome);
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                return View(fornecedor);
            }
        }


        public JsonResult AlterarDataRepasse(string data, string pedidos)
        {
            var repassedata = new DateTime();
            try
            {
                repassedata = Convert.ToDateTime(data);
            }
            catch
            {
                return Json(new { sucesso = false, mensagem = "Data de repasse inválida." });
            }

            var mensagem = "OK";
            var sucesso = true;
            var i = 0;
            foreach (var item in pedidos.Split(','))
            {
                var id = 0;
                Int32.TryParse(item, out id);
                if (id > 0)
                {
                    var repasse = repasseRepository.Get(id);
                    if (repasse != null)
                    {
                        if (repassedata != new DateTime())
                            repasse.DataRepasse = repassedata;
                        repasseRepository.Save(repasse);
                        i++;
                    }
                }
            }
            if (i > 0)
                mensagem = "Data alterada com sucesso em " + i + " repasses.";
            else
                mensagem = "Não haviam repasses selecionados para alteração.";
            return Json(new { sucesso = sucesso, mensagem = mensagem });
        }


        public JsonResult AlterarStatusRepasse(int status, string pedidos)
        {
            var mensagem = "OK";
            var sucesso = true;
            var i = 0;
            foreach (var item in pedidos.Split(','))
            {
                var id = 0;
                Int32.TryParse(item, out id);
                if (id > 0)
                {
                    var repasse = repasseRepository.Get(id);
                    if (repasse != null)
                    {
                        repasse.StatusID = status;
                        repasseRepository.Save(repasse);
                        i++;
                    }
                }
            }
            if (i > 0)
                mensagem = "Status alterado com sucesso em " + i + " repasses.";
            else
                mensagem = "Não haviam repasses selecionados para alteração.";
            return Json(new { sucesso = sucesso, mensagem = mensagem });
        }

        public JsonResult SalvaFornecedor(int itemID, string valor, string data, int status)
        {
            var repassedata = new DateTime();
            try
            {
                repassedata = Convert.ToDateTime(data);
            }
            catch
            {
            }

            decimal valorrepasse = 0;
            try
            {
                valorrepasse = Convert.ToDecimal(valor);
            }
            catch
            {
            }

            if (itemID > 0)
            {
                var repasse = repasseRepository.Get(itemID);

                if (valorrepasse > 0)
                {
                    if (repasse.PedidoItem == null)
                    {
                        if (repasse.ValorRepasse == 0)
                            administradorPedidoRepository.Registra(adminModel.ID, null, repasse.PedidoEmpresaID, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                        else
                            administradorPedidoRepository.Registra(adminModel.ID, null, repasse.PedidoEmpresaID, null, null, AdministradorPedido.Acao.AlterouRepasse);
                    }
                    else
                    {
                        if (repasse.ValorRepasse == 0)
                            administradorPedidoRepository.Registra(adminModel.ID, repasse.PedidoItem.PedidoID, null, null, null, AdministradorPedido.Acao.PreencheuRepasse);
                        else
                            administradorPedidoRepository.Registra(adminModel.ID, repasse.PedidoItem.PedidoID, null, null, null, AdministradorPedido.Acao.AlterouRepasse);
                    }

                    repasse.ValorRepasse = valorrepasse;
                    repasse.Observacao += "\nRepasse alterado de " + repasse.ValorRepasse.ToString("C2") + " para " + valorrepasse.ToString("C2") + " por " + adminModel.CurrentAdministrador.Nome;

                }
                if (repassedata != new DateTime())
                {
                    repasse.DataRepasse = repassedata;
                    repasse.Observacao += "\nData de repasse alterado de " + (repasse.DataRepasse.HasValue ? repasse.DataRepasse.Value.ToString("dd/MM/yyyy HH:mm") : " (sem data)") + " para " + repassedata.ToString("dd/MM/yyyy HH:mm") + " por " + adminModel.CurrentAdministrador.Nome;
                }
                var mensagem = "";
                if ((repasse.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || repasse.Status == Domain.Entities.Repasse.TodosStatus.Pago) && status == (int)Domain.Entities.Repasse.TodosStatus.Cancelado)
                    mensagem = "Cancelar um repasse força ao usuário à acessar o pedido e criar um novo repasse ou o pedido não poderá ser concluído.";

                repasse.StatusID = status;
                repasseRepository.Save(repasse);

                return Json(new { sucesso = true, mensagem = mensagem });
            }
            else
            {
                return Json(new { sucesso = false, mensagem = "Problema ao alterar o pedido." });
            }

        }


        [HttpGet]
        public ActionResult TabelaRepasse(int id)
        {
            Fornecedor fornecedor = fornecedorRepository.Get(id);
            if (fornecedor != null)
            {
                ViewBag.Fornecedor = fornecedor;
                var produtos = produtoRepository.GetByExpression(c => c.Disponivel).OrderBy(c => c.Nome).ToList();
                ViewBag.Precos = fornecedor.FornecedorPrecoes.ToList();
                return View(produtos);
            }
            else
                return RedirectToAction("List");
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult TabelaRepasse(FormCollection form)
        {
            Fornecedor fornecedor = fornecedorRepository.Get(Convert.ToInt32(form["FornecedorID"]));
            if (fornecedor != null)
            {
                var produtos = produtoRepository.GetByExpression(c => c.Disponivel).OrderBy(c => c.Nome).ToList();
                foreach (var item in produtos)
                {
                    foreach (var tamanho in item.ProdutoTamanhoes)
                    {
                        decimal valor = 0;
                        Decimal.TryParse(Request.Form[tamanho.ID + "-preco"], out valor);

                        var fornecedorpreco = fornecedorPrecoRepository.GetByExpression(c => c.ProdutoTamanhoID == tamanho.ID && c.ProdutoID == item.ID && c.FornecedorID == fornecedor.ID).FirstOrDefault();
                        if (fornecedorpreco == null)
                            fornecedorpreco = new FornecedorPreco();
                        fornecedorpreco.FornecedorID = fornecedor.ID;
                        fornecedorpreco.ProdutoID = item.ID;
                        fornecedorpreco.ProdutoTamanhoID = tamanho.ID;
                        fornecedorpreco.Repasse = valor;
                        fornecedorPrecoRepository.Save(fornecedorpreco);
                    }
                }
                return RedirectToAction("TabelaRepasse", new { cod = "SaveSucess", msg = "Valores atualizados com sucesso!", id = fornecedor.ID });
            }
            else
                return RedirectToAction("List");
        }


    }
}
