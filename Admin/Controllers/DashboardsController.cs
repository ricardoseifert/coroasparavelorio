﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using Admin.ViewModels;
using System.Data.Objects;
using System.Diagnostics;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading;
using Admin.Interfaces;
using Admin.ViewModel;
using DomainExtensions.Repositories;
using Domain.Repositories;
using Domain.Entities.Generics;
using Domain.MetodosExtensao;
using Domain.Service;

namespace Admin.Controllers
{
    //=================================================================================
    public class DashboardsController : Controller
    {

        #region Variaveis
        //-----------------------------------------------------------------------------
        private IPersistentRepository<Administrador> adminRepository;
        private IPersistentRepository<NPSRating> npsRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpRepository;
        private PedidoDashboardRepository pedidoDashboardRepository;
        private IPersistentRepository<AdministradorCarteira> administradorCarteiraRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Meta> metaRepository;
        private IPersistentRepository<Metrica> metricaRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<Agendum> agendaRepository;
        private PedidoService pedidoService;
        private RelatorioRepository relatorioRepository;
        private IPersistentRepository<ClienteFloricultura> ClienteFloriculturaRepository;
        private IPersistentRepository<EstoqueLote> loteFloriculturaRepository;
        private IPersistentRepository<PedidoItemFloricultura> pedidoItemFloriculturaRepository;
        Administrador admin;

        private AdminModel adminModel;

        //-----------------------------------------------------------------------------
        //Threads
        //-----------------------------------------------------------------------------
        public static DashBoard DashResult;
        public static ObjectContext CtxPublic;
        public static int idPerfil;
        public static QtdVendasPeriodo VendasPeriodo;
        static decimal dcmNps;
        static int intNpsPercent;
        private static List<FornecedorNPS> lstFornecedorNps;
        private static decimal dcmRepasseMedio;
        private static string strStp1, strStp2, strStp3;

        //-----------------------------------------------------------------------------
        #endregion

        #region Propriedades
        //-----------------------------------------------------------------------------
        public DateTime DataInicial
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DataInicial"]))
                {
                    return DateTime.Parse(Request.QueryString["DataInicial"], new CultureInfo("pt-BR"));
                }
                return DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            }
        }
        //-----------------------------------------------------------------------------
        public int UserId
        {
            get
            {
                if (adminModel.PerfilId == (int)Administrador.Perfis.Fornecedores || adminModel.PerfilId == (int)Administrador.Perfis.Qualidade)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Usuario"]))
                    {
                        return int.Parse(Request.QueryString["Usuario"]);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["userID"]))
                    {
                        return int.Parse(Request.QueryString["userID"]);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["Usuario"]))
                        {
                            return int.Parse(Request.QueryString["Usuario"]);
                        }
                        else
                        {
                            return adminModel.CurrentAdministrador.ID;
                        }
                    }
                }

                return adminModel.PerfilId == (int)Administrador.Perfis.Fornecedores ? adminModel.CurrentAdministrador.ID : 0;
            }
        }
        //-----------------------------------------------------------------------------
        public int SistemaId
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Sistema"]))
                {
                    return int.Parse(Request.QueryString["Sistema"]);
                }
                else
                {
                    return 0;
                }
            }
        }
        //-----------------------------------------------------------------------------
        public int Produtos
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Produtos"]))
                {
                    return int.Parse(Request.QueryString["Produtos"]);
                }
                else
                {
                    return 0;
                }
            }
        }
        //-----------------------------------------------------------------------------
        public DateTime DataFinal
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DataFinal"]))
                {
                    return DateTime.Parse(Request.QueryString["DataFinal"], new CultureInfo("pt-BR")).AddSeconds(86399);
                }
                return DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy")).AddSeconds(86399);
            }
        }
        //-----------------------------------------------------------------------------
        public bool TodosStatusCancelamento
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["TodosStatusCancelamento"]))
                {

                    if (Request.QueryString["TodosStatusCancelamento"] == "on")
                    {
                        return true;
                    }
                    try
                    {
                        return bool.Parse(Request.QueryString["TodosStatusCancelamento"]);
                    }
                    catch
                    {
                        return false;
                    }

                }
                return false;
            }
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public DashboardsController(ObjectContext context)
        {
            CtxPublic = context;
            adminModel = new AdminModel(context);
            adminRepository = new PersistentRepository<Administrador>(context);
            npsRepository = new PersistentRepository<NPSRating>(context);
            //fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pedidoEmpRepository = new PersistentRepository<PedidoEmpresa>(context);
            administradorCarteiraRepository = new PersistentRepository<AdministradorCarteira>(context);
            relatorioRepository = new RelatorioRepository(context);
            //repasseRepository = new PersistentRepository<Repasse>(context);
            pedidoService = new PedidoService(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            pedidoDashboardRepository = new PedidoDashboardRepository(context);
            metaRepository = new PersistentRepository<Meta>(context);
            metricaRepository = new PersistentRepository<Metrica>(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            agendaRepository = new PersistentRepository<Agendum>(context);
            pedidoItemFloriculturaRepository = new PersistentRepository<PedidoItemFloricultura>(context);
            
            loteFloriculturaRepository = new PersistentRepository<EstoqueLote>(context);
            ClienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(context);
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Actions
        //-----------------------------------------------------------------------------
        public ActionResult Index()
        {
            string urlParametros = "";
            urlParametros += "Usuario=" + UserId + "&Sistema=" + SistemaId + "&Produtos=" + Produtos;

            if (!string.IsNullOrEmpty(urlParametros))
            {
                ViewBag.UrlParametros = "'/Dashboards/ListaPedidos?" + urlParametros + "'";
            }
            else
            {
                ViewBag.UrlParametros = "'/Dashboards/ListaPedidos'";
            }

            if (adminModel.CurrentAdministrador != null)
            {
                admin = adminRepository.Get(adminModel.CurrentAdministrador.ID);

                idPerfil = admin.PerfilID;

                DashResult = DashService.CalcularDashPorProcessados(DataInicial, DataFinal, UserId, SistemaId, Produtos, !TodosStatusCancelamento);

                var lstUsuarios = CtxPublic.CreateObjectSet<Administrador>().Where(r => (r.PerfilID == (int)Administrador.Perfis.Fornecedores
                || r.PerfilID == (int)Administrador.Perfis.Qualidade
                || r.PerfilID == (int)Administrador.Perfis.Atendimento
                || r.PerfilID == (int)Administrador.Perfis.LacosCorporativos
                ) && r.Liberado).OrderBy(r => r.Nome).ToList();

                ViewBag.LstUsuarios = lstUsuarios;

                if (DashResult.QtdPedidosComAceiteManual > 0 && DashResult.QtdPedidosComAceiteFornecedor > 0)
                {
                    ViewBag.QtdPedidosComAceiteFornecedor = DashResult.QtdPedidosComAceiteFornecedor;
                    ViewBag.QtdPedidosComAceiteManual = DashResult.QtdPedidosComAceiteManual;
                    ViewBag.PercentualAceiteManual = DashResult.QtdPedidosComAceiteManual / DashResult.QtdPedidosComAceiteFornecedor;
                }
                else
                {
                    ViewBag.PercentualAceiteManual = 0;
                    ViewBag.QtdPedidosComAceiteManual = 0;
                }

                ViewBag.ListaPedidosComAceiteManual = DashResult.ListaPedidosComAceiteManual;

                ViewBag.QtdPedidosCancelados = DashResult.QtdPedidosCancelados;

                ViewBag.QtdPedidos = DashResult.QtdPedidos;
                ViewBag.QtdPedidosBruto = DashResult.QtdPedidosBruto;
                ViewBag.NotaNPS = (int)DashResult.NotaNPS;
                ViewBag.QtdPedidosNPS = (int)DashResult.QtdPedidosNPS;

                if (DashResult.QtdPedidosCancelados > 0 && DashResult.QtdPedidosBruto > 0)
                {
                    ViewBag.PercentualPedidosCancelados = DashResult.QtdPedidosCancelados / DashResult.QtdPedidosBruto;
                }
                else
                {
                    ViewBag.PercentualPedidosCancelados = 0;
                }

                if (DashResult.QtdMailConf > 0 && DashResult.QtdPedidos > 0)
                {
                    ViewBag.QtdMailConf = DashResult.QtdMailConf;
                    ViewBag.PercentualMailConf = DashResult.QtdMailConf / DashResult.QtdPedidos;
                }

                ViewBag.adminModel = adminModel;
                ViewBag.DataInicial = DataInicial;
                ViewBag.DataFinal = DataFinal;
                ViewBag.TotalItensPedido = DashResult.QtdPedidos;

                ViewBag.ListaPedidosCanceladosCPV = DashResult.ListaPedidosCanceladosCPV;
                ViewBag.ListaPedidosCanceladosCORP = DashResult.ListaPedidosCanceladosCORP;

                ViewBag.QtdVendasPeriodo = VendasPeriodo;
                ViewBag.QtdPedidosAutomatizados = DashResult.QtdPedidosAutomatizados;
                ViewBag.ListaGeralFornecedorNPSCalculada = lstFornecedorNps;
                ViewBag.NPSPercent = intNpsPercent;
                ViewBag.NPS = dcmNps;
                ViewBag.TodosStatusCancelamento = TodosStatusCancelamento;
                ViewBag.RepasseMedio = dcmRepasseMedio;
                ViewBag.TotalFaturamento = DashResult.totalFaturamento;
                ViewBag.ValorRepassado = DashResult.totalRepasse;
                ViewBag.TicketMedio = DashResult.TicketMedio;
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

            return View("DashFornecedores");            
        }
        //-----------------------------------------------------------------------------
        [HttpPost]
        public string SaiuParaEntrega(int idPedido, OrigemPedido origem)
        {
            if (idPedido != 0)
            {
                try
                {
                    switch (origem)
                    {
                        case OrigemPedido.Cliente:
                            Pedido pedCliente = pedidoRepository.Get(idPedido);

                            pedCliente.FornecedorEmProcessamento = true;
                            pedCliente.DataProcessamentoFornecedor = DateTime.Now;
                            pedCliente.StatusEntregaID = (int)TodosStatusEntrega.EntregaEmAndamento;
                            pedidoRepository.Save(pedCliente);
                            administradorPedidoRepository.Registra(adminModel.ID, idPedido, null, null, null, AdministradorPedido.Acao.AceiteManual);
                            return "OK";

                        case OrigemPedido.Empresa:
                            PedidoEmpresa pedEmpresa = pedidoEmpRepository.Get(idPedido);

                            pedEmpresa.FornecedorEmProcessamento = true;
                            pedEmpresa.DataProcessamentoFornecedor = DateTime.Now;
                            pedEmpresa.StatusEntregaID = (int)TodosStatusEntrega.EntregaEmAndamento;
                            pedidoEmpRepository.Save(pedEmpresa);
                            administradorPedidoRepository.Registra(adminModel.ID, null, idPedido, null, null, AdministradorPedido.Acao.AceiteManual);
                            return "OK";

                        default:
                            return "OK";
                    }
                }
                catch (Exception)
                {
                    return "FALSE";
                }
            }
            else
            {
                return "FALSE";
            }
        }
        //-----------------------------------------------------------------------------
        [HttpPost]
        public string ListaFornecedoresOnlineOffline()
        {
            var Result = relatorioRepository.GetRelatorioFornecedoresOnlineOffline().Where(r => r.Status == "ONLINE").ToList();
            string LstReturn = "<ul>";

            foreach (var item in Result)
            {
                LstReturn += "<li><div>" + System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.Nome.ToLower()) + "</div></li>";
            }

            return LstReturn + "</ul>";
        }
        public ActionResult ListaPedidos()
        {
            List<IPedido> _lstPedidos;

            //int _intTempoLeilao = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TempoLeiaoPorFloricultura"]);

            // RETORNA A LISTA DE PEDIDOS POR USUARIO 
            // ORDENA POR HORARIO DE ENTREGA
            _lstPedidos = pedidoDashboardRepository.ListarPedidosProcessados(UserId, SistemaId, Produtos)
              .Where(r => !r.Cancelado() && r.StatusEntrega != TodosStatusEntrega.Entregue)
              .OrderBy(r => r.DataSolicitada)
              .ToList();

            if(!adminModel.CurrentAdministrador.Comercial)
                _lstPedidos = _lstPedidos.Where(r => r.FornecedorID != 4893).ToList();

            ViewBag.LstPedidosPendenteFornecedor = _lstPedidos.Where(p => !p.FornecedorID.HasValue).OrderBy(p => p.DataSolicitada).ToList();

            ViewBag.LstPedidosEmProducao = _lstPedidos.Where(p => p.FornecedorEmProcessamento == false && p.FornecedorID.HasValue).ToList().OrderBy(p => p.DataSolicitada).ToList();

            ViewBag.EntregaEmAndamento = _lstPedidos.Where(p => p.StatusEntrega != TodosStatusEntrega.Entregue && p.FornecedorEmProcessamento && p.FornecedorID.HasValue).OrderBy(p => p.DataSolicitada).ToList();


            return View("~/Views/Dashboards/RelatorioPedidos.cshtml", _lstPedidos);
        }
        //-----------------------------------------------------------------------------
        public void CalculaNps()
        {
            Stopwatch stp = new Stopwatch();


            int intUser = UserId;

            Stopwatch stpT = new Stopwatch();
            stpT.Start();
            var lstNps = DashService.NpsRatingNew(UserId, DataInicial, DataFinal);
            stpT.Stop();
            strStp1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpT.Elapsed.Minutes, stpT.Elapsed.Seconds, stpT.Elapsed.Milliseconds);

            stp.Start();
            List<FornecedorNPS> lstGeralFornecedores = new List<FornecedorNPS>();

            foreach (var item in lstNps)
            {
                if (item.FornecedorID != null)
                {
                    lstGeralFornecedores.Add(
                        new FornecedorNPS
                        {
                            IdFornecedor = (int)item.FornecedorID,
                            IdAdmin = item.AdministradorID,
                            Nota = item.Nota
                        });
                }
            }

            List<FornecedorNPS> lstGeralFornecedorNpsCalculada = new List<FornecedorNPS>();

            foreach (var fornecedorId in lstGeralFornecedores.Select(x => x.IdFornecedor).Distinct())
            {
                var fornecedor = lstGeralFornecedores.FirstOrDefault(p => p.IdFornecedor == fornecedorId);
                if (fornecedor != null)
                {
                    lstGeralFornecedorNpsCalculada.Add(
                        new FornecedorNPS
                        {
                            IdFornecedor = fornecedor.IdFornecedor,
                            Nota = lstGeralFornecedores.Where(p => p.IdFornecedor == fornecedorId).Average(x => x.Nota),
                            IdAdmin = fornecedor.IdAdmin,
                            QtdNota = lstGeralFornecedores.Count(p => p.IdFornecedor == fornecedorId)
                        });
                }
            }

            lstFornecedorNps = lstGeralFornecedorNpsCalculada.OrderByDescending(p => p.Nota).ToList();

            decimal somaMult = 0;
            decimal somaQtd = 0;

            foreach (var item in lstFornecedorNps)
            {
                somaMult += item.Nota * item.QtdNota;
                somaQtd += item.QtdNota;
            }

            if (somaMult > 0 && somaQtd > 0)
            {
                decimal npsResult = somaMult / somaQtd;
                intNpsPercent = (int)(npsResult * 100) / 5;
                dcmNps = npsResult;
                stp.Stop();
                strStp2 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);

                return;
            }
            dcmNps = 0;
            stp.Stop();
            strStp2 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);
        }
        //-----------------------------------------------------------------------------
        public void CalculaPercentualRepasse()
        {

            Stopwatch stp = new Stopwatch();
            stp.Start();
            //DashResult = DashService.CalcularDashPorCarteiraNew2(DataInicial, DataFinal, UserId, SistemaId, !TodosStatusCancelamento);
            //DashResult = DashService.CalcularDashPorCarteiraNew(DataInicial, DataFinal, AdminId, !TodosStatusCancelamento);
            //DashResult = DashService.CalcularDashPorCarteira(DataInicial, DataFinal, AdminId, !TodosStatusCancelamento);

            if (DashResult.totalRepasse != 0 && DashResult.TotalItensPedido != 0 && DashResult.TicketMedio != 0)
            {
                dcmRepasseMedio = (DashResult.totalRepasse / DashResult.TotalItensPedido) / DashResult.TicketMedio;
                stp.Stop();
                strStp3 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);

                return;
            }
            dcmRepasseMedio = 0;
            stp.Stop();
            strStp3 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);

        }
        //----------------------------------------------------------------------------- 
        private List<Tuple<string, string>> BuscarCarteira()
        {
            List<Tuple<string, string>> lstRet = new List<Tuple<string, string>>();

            List<Estado> lstEstados;
            List<Cidade> lstCidades;
            decimal dcmMetaRepasse, dcmMetaNPS, dcmMetaIndiceCanc;

            string strCarteiras;
            DashService.BuscarCarteiraAdm(UserId == 0 ? (int?)null : UserId, out lstCidades, out lstEstados, out strCarteiras, out dcmMetaRepasse, out dcmMetaNPS, out dcmMetaIndiceCanc);
            lstRet.AddRange(lstCidades.Select(e => new Tuple<string, string>(e.Estado.Sigla, e.Nome)));
            lstRet.AddRange(lstEstados.Select(e => new Tuple<string, string>(e.Sigla, string.Empty)));
            ViewBag.NomesCarteiras = strCarteiras;

            ViewBag.MetaNPS = dcmMetaNPS;
            ViewBag.MetaRepasse = dcmMetaRepasse;
            ViewBag.MetaIndiceCanc = dcmMetaIndiceCanc;

            return lstRet;
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Socios

        public ActionResult Financeiro()
        {
            ViewBag.adminModel = adminModel;

            ViewBag.ReportVencidos = relatorioRepository.GetRelatoriospRelatorioDashFinanceiroVencidos();
            ViewBag.ReportFull = relatorioRepository.GetRelatoriospRelatorioDashFinanceiro();

            return View("Financeiro");
        }

        //public ActionResult LoteFloricultura()
        //{
        //    if (adminModel == null)
        //    {
        //        //RedirectToAction("Login", "Home");
        //    }
        //    ViewBag.adminModel = adminModel;

        //    ViewBag.DataInicial = DataInicial;
        //    ViewBag.DataFinal = DataFinal;

        //    var lstLotes = loteFloriculturaRepository.GetByExpression(r => r.DataCadastro >= DataInicial && r.DataCadastro <= DataFinal);

        //    decimal ValorTotal = 0;
        //    decimal ValorTotalDasFaturas = 0;
        //    decimal ValorTotalDasFaturasPagas = 0;
        //    decimal ValorTotalDasFaturasAbertas = 0;

        //    foreach (var Lote in lstLotes)
        //    {
        //        ValorTotal += Lote.ValorTotal;
        //        ValorTotalDasFaturas += Lote.ValorTotalDasFaturas;
        //        ValorTotalDasFaturasPagas += Lote.ValorTotalDasFaturasPagas;
        //        ValorTotalDasFaturasAbertas += Lote.ValorTotalDasFaturasAbertas;
        //    }

        //    ViewBag.ValorTotal = ValorTotal;
        //    ViewBag.ValorTotalDasFaturas = ValorTotalDasFaturas;
        //    ViewBag.ValorTotalDasFaturasPagas = ValorTotalDasFaturasPagas;
        //    ViewBag.ValorTotalDasFaturasAbertas = ValorTotalDasFaturasAbertas;

        //    ViewBag.ValorAFaturar = ValorTotal - ValorTotalDasFaturas;

        //    ViewBag.QtdLotes = lstLotes.Count();
        //    ViewBag.QtdLotesSemFatura = lstLotes.Where(r => r.EstoqueFaturaPagamento_x_Lote.Count == 0).Count();
        //    ViewBag.QtdLotesFaturados = lstLotes.Where(r => r.EstoqueFaturaPagamento_x_Lote.Count > 0).Count();

        //    //ViewBag.RelatorioProdutosVendidos = DashService.GetRelatorioProdutosFloricultura(DataInicial, DataFinal).ToList();
        //    //ViewBag.RelatorioFunerarias = DashService.GetRelatorioFunerarias(DataInicial, DataFinal).ToList();
        //    //ViewBag.RelatorioMeioPagamentoFloricultura = DashService.GetMeioPagamentoFloricultura(DataInicial, DataFinal).ToList();



        //    //var RelatorioVendasCPVFloricultura = DashService.GetRelatorioVendasCPVFloricultura(DataInicial, DataFinal).ToList();
        //    //ViewBag.RelatorioVendasCPVFloricultura = RelatorioVendasCPVFloricultura;

        //    //List<KeyValuePair<int, string>> LstProdutosPedidoCPV = new List<KeyValuePair<int, string>>();
        //    //foreach (var Item in RelatorioVendasCPVFloricultura)
        //    //{
        //    //    foreach (var ItemFlor in pedidoFloriculturaRepository.Get(Item.ID).PedidoItemFloricultura.ToList())
        //    //    {
        //    //        LstProdutosPedidoCPV.Add(new KeyValuePair<int, string>(Item.ID, ItemFlor.ProdutoTamanhoPrecoFloricultura.ProdutoFloricultura.Nome.ToUpper()));
        //    //    }
        //    //}

        //    //ViewBag.LstProdutosPedidoCPV = LstProdutosPedidoCPV;



        //    //ViewBag.ValorFaturado = (decimal)ValorItensPedidos;
        //    //ViewBag.VendasPJ = VendasPJ;
        //    //ViewBag.VendasPF = VendasPF;
        //    //ViewBag.VendasFuneraria = VendasFuneraria;
        //    //ViewBag.VendasBalcao = VendasBalcao;
        //    //ViewBag.VendasCPV = VendasCPV;

        //    //ViewBag.TotalVendas = VendasPJ + VendasPF + VendasFuneraria + VendasBalcao + VendasCPV;

        //    return View("LoteFloricultura");
        //}

        #endregion

        #region Comercial
        //-----------------------------------------------------------------------------
        public ActionResult Comercial(int? id)
        {
            if (adminModel == null)
            {
                //RedirectToAction("Login", "Home");
            }
            ViewBag.adminModel = adminModel;
            FiltroDashComercialViewModel filtro = new FiltroDashComercialViewModel();

            filtro.DataInicioFaturamento = DateTime.Today.AddMonths(-3);
            filtro.DataFimFaturamento = DateTime.Today;

            if (id.HasValue)
            {
                Meta meta = metaRepository.GetByExpression(m => m.Id == id.Value).FirstOrDefault();
                if (meta != null)
                {
                    if (meta.DataInicio.HasValue)
                    {
                        filtro.DataInicioFaturamento = meta.DataInicio.Value;
                    }
                    if (meta.DataFim.HasValue)
                    {
                        filtro.DataFimFaturamento = meta.DataFim.Value;
                    }

                    filtro.IdUsuario = meta.IdUsuario;
                    filtro.IdMeta = id;
                }
            }

            filtro.DataFimAtivacao = filtro.DataInicioFaturamento.AddDays(-1);
            //filtro.DataInicioAtivacao = filtro.DataInicioFaturamento.AddMonths(-3);

            filtro.DataInicioVisitas = filtro.DataInicioFaturamento;
            filtro.DataFimVisitas = filtro.DataFimFaturamento;

            List<Administrador> lstUsuarios = empresaRepository.GetByExpression(e => e.Administrador != null).Select(a => a.Administrador).Distinct().OrderBy(a => a.Nome).ToList();
            ViewBag.Colaboradores = new SelectList(lstUsuarios, "ID", "Nome");

            return View(filtro);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetFaturamento(FiltroDashComercialViewModel filtro)
        {
            //faz a busca do faturamento de acordo com o filtro e retorna uma view

            Expression<Func<PedidoEmpresa, bool>> exp =
                e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }

            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();

            decimal dcmAtual = 0;
            if (lstPedidos.Any())
            {
                dcmAtual = lstPedidos.Sum(p => p.Valor);
            }

            Metrica metaMetrica = metricaRepository.GetByExpression(m => m.IdMeta == filtro.IdMeta.Value && m.NomeMetrica.Equals("Faturamento")).FirstOrDefault();

            MetricaGenerica metrica = new MetricaGenerica();
            if (metaMetrica != null)
            {
                metrica.Titulo = metaMetrica.NomeMetrica;
                metrica.Meta = metaMetrica.Valor;
                metrica.Atual = dcmAtual;
                metrica.Id = (int)metaMetrica.Id;
            }
            else
            {
                metrica.Meta = 0;
                metrica.Titulo = "Faturamento";
                metrica.Id = 1;
            }

            metrica.TipoDado = MetricaGenerica.TipoDadoMetrica.Moeda;

            return View("~/Views/Shared/MetricaGraficaBarra.cshtml", metrica);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetAtivacao(FiltroDashComercialViewModel filtro)
        {
            //faz a busca do faturamento de acordo com o filtro e retorna uma view

            Expression<Func<Empresa, bool>> exp = e => e.ID > 0;
            Expression<Func<DateTime, bool>> expDataEntre = d => d != new DateTime();
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.AtendimentoID == filtro.IdUsuario);
            }

            if (filtro.DataInicioAtivacao.HasValue)
            {
                expDataEntre = expDataEntre.And(d => d >= filtro.DataInicioAtivacao.Value);
            }
            if (filtro.DataFimAtivacao.HasValue)
            {
                filtro.DataFimAtivacao = new DateTime(filtro.DataFimAtivacao.Value.Year, filtro.DataFimAtivacao.Value.Month, filtro.DataFimAtivacao.Value.Day);
                filtro.DataFimAtivacao = filtro.DataFimAtivacao.Value.AddDays(1).AddSeconds(-1);
                expDataEntre = expDataEntre.And(d => d <= filtro.DataFimAtivacao.Value);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.PedidoEmpresas.Any(p => p.DataCriacao >= filtro.DataInicioFaturamento));
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.PedidoEmpresas.Any(p => p.DataCriacao <= filtro.DataFimFaturamento));
            }

            Func<DateTime, bool> datas = expDataEntre.Compile();
            var empresas = empresaRepository.GetByExpression(exp).Select(e => new { Grupo = e.GrupoEconomico.Trim().TrimStart().TrimEnd(), Data = e.DataCadastro }).Distinct().ToList();

            decimal dcmAtual = empresas.GroupBy(g => g.Grupo).Select(g => new { Grupo = g.Key, Data = g.Min(i => i.Data) }).Count(d => datas(d.Data));

            Metrica metaMetrica = metricaRepository.GetByExpression(m => m.IdMeta == filtro.IdMeta.Value && m.NomeMetrica.Equals("Ativação")).FirstOrDefault();

            MetricaGenerica metrica = new MetricaGenerica();
            if (metaMetrica != null)
            {
                metrica.Titulo = metaMetrica.NomeMetrica;
                metrica.Meta = metaMetrica.Valor;
                metrica.Atual = dcmAtual;
                metrica.Id = (int)metaMetrica.Id;
            }
            else
            {
                metrica.Meta = 0;
                metrica.Titulo = "Ativação";
                metrica.Id = 2;
            }
            metrica.TipoDado = MetricaGenerica.TipoDadoMetrica.Numero;

            return View("~/Views/Shared/MetricaGraficaBarra.cshtml", metrica);
        }
        //-----------------------------------------------------------------------------
        //public ActionResult GetVisitas(FiltroDashComercialViewModel filtro)
        //{
        //    //faz a busca do faturamento de acordo com o filtro e retorna uma view
        //    if (filtro.DataFimVisitas.HasValue)
        //    {
        //        filtro.DataFimVisitas = filtro.DataFimVisitas.Value.AddDays(1).AddSeconds(-1);
        //    }
        //    Expression<Func<Agend, bool>> exp =
        //        a => a.EmpresaID.HasValue;

        //    if (filtro.IdUsuario.HasValue)
        //    {
        //        exp = exp.And(a => a.Empresa != null && a.Empresa.AtendimentoID == filtro.IdUsuario);
        //    }
        //    if (filtro.DataInicioVisitas.HasValue)
        //    {
        //        exp = exp.And(a => a.Data >= filtro.DataInicioVisitas.Value);
        //    }
        //    if (filtro.DataFimVisitas.HasValue)
        //    {
        //        filtro.DataFimVisitas = new DateTime(filtro.DataFimVisitas.Value.Year, filtro.DataFimVisitas.Value.Month, filtro.DataFimVisitas.Value.Day);
        //        filtro.DataFimVisitas = filtro.DataFimVisitas.Value.AddDays(1).AddSeconds(-1);
        //        exp = exp.And(a => a.Data <= filtro.DataFimVisitas.Value);
        //    }

        //    List<Agend> lstReunioes = agendaRepository.GetByExpression(exp).ToList();

        //    decimal dcmAtual = lstReunioes.Count;

        //    Metrica metaMetrica = metricaRepository.GetByExpression(m => m.IdMeta == filtro.IdMeta.Value && m.NomeMetrica.Equals("Visitas")).FirstOrDefault();

        //    MetricaGenerica metrica = new MetricaGenerica();
        //    if (metaMetrica != null)
        //    {
        //        metrica.Titulo = metaMetrica.NomeMetrica;
        //        metrica.Meta = metaMetrica.Valor;
        //        metrica.Atual = dcmAtual;
        //        metrica.Id = (int)metaMetrica.Id;
        //    }
        //    else
        //    {
        //        metrica.Meta = 0;
        //        metrica.Titulo = "Visitas";
        //        metrica.Id = 3;
        //    }
        //    metrica.TipoDado = MetricaGenerica.TipoDadoMetrica.Numero;

        //    return View("~/Views/Shared/MetricaGraficaBarra.cshtml", metrica);
        //}
        //-----------------------------------------------------------------------------
        public ActionResult GetTicketMedio(FiltroDashComercialViewModel filtro)
        {
            //faz a busca do faturamento de acordo com o filtro e retorna uma view
            Expression<Func<PedidoEmpresa, bool>> exp =
                e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }

            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();

            decimal dcmAtual = 0;
            if (lstPedidos.Any())
            {
                dcmAtual = lstPedidos.Sum(p => p.Valor) / lstPedidos.Count;
            }

            return Content(dcmAtual.ToString("N2"));
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetMargemMedia(FiltroDashComercialViewModel filtro)
        {
            //faz a busca do faturamento de acordo com o filtro e retorna uma view
            Expression<Func<PedidoEmpresa, bool>> exp =
               e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }

            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();

            decimal dcmMargem = 0;
            if (lstPedidos.Any())
            {
                var dcmReceitaLiquida = lstPedidos.Sum(p => p.Valor);
                var dcmTotalRepasse = lstPedidos.Select(p => p.Repasses.Where(r => r.Status != Repasse.TodosStatus.Cancelado).Sum(v => v.ValorRepasse)).Sum(v => v);
                if (dcmTotalRepasse > 0)
                {
                    dcmMargem = ((dcmReceitaLiquida - dcmTotalRepasse) / dcmReceitaLiquida) * 100;
                }
                else
                {
                    dcmMargem = 100;
                }
            }
            return Content(dcmMargem.ToString("N1") + "%");
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetEmpresaFaturamento(FiltroDashComercialViewModel filtro)
        {
            Expression<Func<PedidoEmpresa, bool>> exp =
                e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }
            List<IReportable> lstRanking = new List<IReportable>();
            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();

            if (lstPedidos.Any())
            {
                var lstGeral = lstPedidos.Select(p => new { Grupo = p.Empresa.GrupoEconomico, p.Valor }).ToList();
                lstRanking = lstGeral
                .GroupBy(p => p.Grupo)
                .Select(e => new RankingValorViewModel
                {
                    Nome = e.Key.Length > 20 ? e.Key.Substring(0, 20) + "..." : e.Key,
                    Valor = e.Sum(i => i.Valor),
                    Url = Url.Action("List", "Empresa") + string.Format("?datacadastro=&datainicial=&datafinal=&razaosocial=&cnpj=&nome=&email=&grupo={0}&atendimento=", e.Key)
                }).ToList<IReportable>();
            }

            lstRanking = lstRanking.OrderByDescending(r => ((RankingValorViewModel)r).Valor).Take(5).ToList();
            ViewBag.ReportGridExibeTitilos = false;
            return View("ReportGrid", lstRanking);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetEmpresaPedidos(FiltroDashComercialViewModel filtro)
        {
            Expression<Func<PedidoEmpresa, bool>> exp =
                 e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;
            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }
            List<IReportable> lstRanking = new List<IReportable>();
            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();
            if (lstPedidos.Any())
            {
                var lstGeral = lstPedidos.Select(p => new { Grupo = p.Empresa.GrupoEconomico.Trim().TrimStart().TrimEnd(), p.Valor }).ToList();
                lstRanking = lstGeral
                .GroupBy(p => p.Grupo)
                .Select(e => new RankingQuantidadeViewModel
                {
                    Nome = e.Key.Length > 20 ? e.Key.Substring(0, 20) + "..." : e.Key,
                    Quantidade = e.Count(),
                    Url = Url.Action("List", "Empresa") + string.Format("?datacadastro=&datainicial=&datafinal=&razaosocial=&cnpj=&nome=&email=&grupo={0}&atendimento=", e.Key)
                }).ToList<IReportable>();
            }
            lstRanking = lstRanking.OrderByDescending(r => ((RankingQuantidadeViewModel)r).Quantidade).Take(5).ToList();
            ViewBag.ReportGridExibeTitilos = false;
            return View("ReportGrid", lstRanking);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetProdutoFaturamento(FiltroDashComercialViewModel filtro)
        {
            Expression<Func<PedidoEmpresa, bool>> exp =
                 e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;

            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }
            List<IReportable> lstRanking = new List<IReportable>();
            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();
            if (lstPedidos.Any())
            {
                var lstGeral = lstPedidos.Select(p => new { Produto = p.ProdutoTamanho.Produto.Nome + " | " + p.ProdutoTamanho.Tamanho.Nome, p.Valor }).ToList();
                lstRanking = lstGeral
                .GroupBy(p => p.Produto)
                .Select(e => new RankingValorViewModel
                {
                    Nome = e.Key,
                    Valor = e.Sum(i => i.Valor)
                }).ToList<IReportable>();
            }

            lstRanking = lstRanking.OrderByDescending(r => ((RankingValorViewModel)r).Valor).Take(5).ToList();
            ViewBag.ReportGridExibeTitilos = false;
            return View("ReportGrid", lstRanking);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetProdutoQuantidade(FiltroDashComercialViewModel filtro)
        {
            Expression<Func<PedidoEmpresa, bool>> exp =
                 e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;

            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioFaturamento > new DateTime())
            {
                exp = exp.And(e => e.DataCriacao >= filtro.DataInicioFaturamento);
            }
            if (filtro.DataFimFaturamento > new DateTime())
            {
                filtro.DataFimFaturamento = new DateTime(filtro.DataFimFaturamento.Year, filtro.DataFimFaturamento.Month, filtro.DataFimFaturamento.Day);
                filtro.DataFimFaturamento = filtro.DataFimFaturamento.AddDays(1).AddMilliseconds(-1);
                exp = exp.And(e => e.DataCriacao <= filtro.DataFimFaturamento);
            }
            List<IReportable> lstRanking = new List<IReportable>();
            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();
            if (lstPedidos.Any())
            {
                var lstGeral = lstPedidos.Select(p => new { Produto = p.ProdutoTamanho.Produto.Nome + " | " + p.ProdutoTamanho.Tamanho.Nome, p.Valor }).ToList();
                lstRanking = lstGeral
                .GroupBy(p => p.Produto)
                .Select(e => new RankingQuantidadeViewModel
                {
                    Nome = e.Key,
                    Quantidade = e.Count()
                }).ToList<IReportable>();
            }
            lstRanking = lstRanking.OrderByDescending(r => ((RankingQuantidadeViewModel)r).Quantidade).Take(5).ToList();
            ViewBag.ReportGridExibeTitilos = false;
            return View("ReportGrid", lstRanking);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetEmpresasInativas(FiltroDashComercialViewModel filtro)
        {
            Expression<Func<PedidoEmpresa, bool>> exp =
                   e => !e.StatusCancelamentoID.HasValue && e.Empresa.GrupoEconomico != null;

            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(e => e.Empresa.AtendimentoID == filtro.IdUsuario);
            }

            List<PedidoEmpresa> lstPedidos = pedidoEmpRepository.GetByExpression(exp).ToList();
            //List<EmpresasInativasViewModel> lstEmpresas = new List<EmpresasInativasViewModel>();
            List<IReportable> lstEmpresas = new List<IReportable>();
            if (lstPedidos.Any())
            {
                var lstGeral = lstPedidos.GroupBy(g => g.Empresa.GrupoEconomico.Trim().TrimStart().TrimEnd()).ToList();
                var agrupado = lstGeral.Select(g => new
                {
                    Grupo = g.Key,
                    Data = g.Max(i => i.DataCriacao),
                    Id = g.Max(i => i.ID),
                    Valor = g.Where(i => i.DataCriacao >= DateTime.Now.AddYears(-1)).Sum(i => i.Valor)
                }).Where(i => i.Data <= DateTime.Now.AddMonths(-3)).ToList();
                lstEmpresas = agrupado.Select(a => new EmpresasInativasViewModel
                {
                    GrupoEconomico = a.Grupo,
                    DataUltimoPedido = a.Data,
                    IdUltimoPedido = a.Id,
                    Faturamento = a.Valor,
                    UrlGrupo = Url.Action("List", "Empresa") + string.Format("?datacadastro=&datainicial=&datafinal=&razaosocial=&cnpj=&nome=&email=&grupo={0}&atendimento=", a.Grupo),
                    UrlUltimoPedido = Url.Action("Visualizar", "PedidosEmpresa") + string.Format("/{0}", a.Id)
                }).ToList<IReportable>();

                lstEmpresas = lstEmpresas.OrderByDescending(e => ((EmpresasInativasViewModel)e).Faturamento).Take(10).ToList();
            }
            return View("ReportGrid", lstEmpresas);
            //return View(lstEmpresas"",lstEmpresas);
        }
        //-----------------------------------------------------------------------------
        public ActionResult GetVisitas(FiltroDashComercialViewModel filtro)
        {
            //faz a busca do faturamento de acordo com o filtro e retorna uma view
            if (filtro.DataFimVisitas.HasValue)
            {
                filtro.DataFimVisitas = filtro.DataFimVisitas.Value.AddDays(1).AddSeconds(-1);
            }
            Expression<Func<Agendum, bool>> exp =
                a => a.EmpresaID.HasValue;

            if (filtro.IdUsuario.HasValue)
            {
                exp = exp.And(a => a.Empresa != null && a.Empresa.AtendimentoID == filtro.IdUsuario);
            }
            if (filtro.DataInicioVisitas.HasValue)
            {
                exp = exp.And(a => a.Data >= filtro.DataInicioVisitas.Value);
            }
            if (filtro.DataFimVisitas.HasValue)
            {
                filtro.DataFimVisitas = new DateTime(filtro.DataFimVisitas.Value.Year, filtro.DataFimVisitas.Value.Month, filtro.DataFimVisitas.Value.Day);
                filtro.DataFimVisitas = filtro.DataFimVisitas.Value.AddDays(1).AddSeconds(-1);
                exp = exp.And(a => a.Data <= filtro.DataFimVisitas.Value);
            }

            List<Agendum> lstReunioes = agendaRepository.GetByExpression(exp).ToList();

            decimal dcmAtual = lstReunioes.Count;

            Metrica metaMetrica = metricaRepository.GetByExpression(m => m.IdMeta == filtro.IdMeta.Value && m.NomeMetrica.Equals("Visitas")).FirstOrDefault();

            MetricaGenerica metrica = new MetricaGenerica();
            if (metaMetrica != null)
            {
                metrica.Titulo = metaMetrica.NomeMetrica;
                metrica.Meta = metaMetrica.Valor;
                metrica.Atual = dcmAtual;
                metrica.Id = (int)metaMetrica.Id;
            }
            else
            {
                metrica.Meta = 0;
                metrica.Titulo = "Visitas";
                metrica.Id = 3;
            }
            metrica.TipoDado = MetricaGenerica.TipoDadoMetrica.Numero;

            return View("~/Views/Shared/MetricaGraficaBarra.cshtml", metrica);
        }
        #endregion

    }
    //=================================================================================
}
