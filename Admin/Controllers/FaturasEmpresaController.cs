﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.Linq.Expressions;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using Domain.Core;
using Domain.Repositories;
using Admin.ViewModels;
using Domain.MetodosExtensao;
using MvcExtensions.Security.Filters;
using MvcExtensions.Security.Models;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Qualidade|Comunicacao|LacosCorporativos|Fornecedores|Atendimento|Financeiro|FloriculturaMisto", "/Home/AccessDenied")]
    public class FaturasEmpresaController : CrudController<Pedido>
    {
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private AdminModel adminModel;

        public FaturasEmpresaController(ObjectContext context)
            : base(context)
        {
            empresaRepository = new PersistentRepository<Empresa>(context);
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
            boletoRepository = new PersistentRepository<Boleto>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = faturamentoRepository.GetAll().OrderByDescending(p => p.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var cliente = Request.QueryString["cliente"];

            var clienteID = 0;
            Int32.TryParse(Request.QueryString["clienteID"], out clienteID);

            var ID =0;
            Int32.TryParse(Request.QueryString["ID"], out ID);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            var statuspagamento = 0;
            Int32.TryParse(Request.QueryString["statuspagamento"], out statuspagamento);

            var meiopagamento = 0;
            Int32.TryParse(Request.QueryString["meiopagamento"], out meiopagamento);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataCriacao >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataCriacao <= datafinal).ToList();
            }

            if (!String.IsNullOrEmpty(cliente))
                resultado = resultado.Where(c => c.Empresa.RazaoSocial.Contains(cliente)).ToList();

            if (ID > 0)
                resultado = resultado.Where(c => c.ID == ID).ToList();

            if (statuspagamento > 0)
                resultado = resultado.Where(c => c.StatusPagamentoID == statuspagamento).ToList();

            if (meiopagamento > 0)
                resultado = resultado.Where(c => c.MeioPagamentoID == meiopagamento).ToList();

            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList();

            if (clienteID > 0)
                resultado = resultado.Where(c => c.EmpresaID == clienteID).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Empresas = empresaRepository.GetAll().OrderBy(c => c.RazaoSocial).ToList();
            return View(resultado);
        }

        public ActionResult Novo()
        {
            var empresas = empresaRepository.GetByExpression(c => c.PedidoEmpresas.Count(e => !e.FaturamentoID.HasValue && e.StatusID != (int)Domain.Entities.PedidoEmpresa.TodosStatus.PedidoReprovado && e.StatusID != (int)Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado) > 0).OrderBy(c => c.RazaoSocial).ToList();
            return View(empresas);
        }

        public ActionResult Dia(){
            DateTime data;
            
            if (!DateTime.TryParse(Request.QueryString["data"], out data)) {
                data = new DateTime();
            }

            Expression<Func<Empresa, bool>> exp = 
                c => c.PedidoEmpresas.Any(e => !e.FaturamentoID.HasValue 
                    && e.StatusID != (int)PedidoEmpresa.TodosStatus.PedidoReprovado 
                    && e.StatusID != (int)PedidoEmpresa.TodosStatus.PedidoCancelado
                    && !e.StatusCancelamentoID.HasValue
                    && e.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                );
            
            Func<Empresa, bool> exp2 = c =>
                c.CondicaoFaturamentoID == (int)Empresa.CondicoesFaturamento.Imediato 
                ||
                c.FaturamentoInstantaneo == true
                || 
                    (
                        c.FaturamentoInstantaneo == true
                            && 
                        c.OrdemDeCompra == true
                    )
                ||                 
                    (
                        c.CondicaoFaturamentoID == (int)Empresa.CondicoesFaturamento.Mensal 
                            && 
                        c.FaturamentoDiaMes.HasValue && c.FaturamentoDiaMes == data.Day
                    ) 
                || 
                    (
                        c.FaturamentoPontual != null 
                            && 
                        c.FaturamentoPontual.Split(',').Contains(data.Day.ToString()) 
                    );

            Func<Empresa, bool> exp3 = c =>
               c.CondicaoFaturamentoID == (int)Empresa.CondicoesFaturamento.Imediato
               ||
               c.FaturamentoInstantaneo == true
               ||
                   (
                       c.FaturamentoInstantaneo == true
                           &&
                       c.OrdemDeCompra == true
                   )
               ||
                   (
                       c.CondicaoFaturamentoID == (int)Empresa.CondicoesFaturamento.Mensal
                   )
               ||
                   (
                       c.FaturamentoPontual != null
                   );


            var resultado = empresaRepository.GetByExpression(exp).OrderBy(c => c.RazaoSocial).ToList();
            resultado = resultado.Where(exp2).ToList();

            decimal SumComOC = 0;
            decimal SumSemOC = 0;
            decimal SumFatMensal = 0;
            decimal SumFatImediato = 0;
            decimal SumFatInstantaneo = 0;

            foreach (var Empresa in resultado)
            {
                var Pedidos = Empresa.PedidoEmpresas.Where(x => !x.FaturamentoID.HasValue && x.Status != Domain.Entities.PedidoEmpresa.TodosStatus.PedidoCancelado).ToList();

                SumComOC += Pedidos.Where(r => r.Empresa.OrdemDeCompra == true).ToList().Sum(r => r.Valor);
                SumSemOC += Pedidos.Where(r => r.Empresa.OrdemDeCompra == false).ToList().Sum(r => r.Valor);
                SumFatMensal += Pedidos.Where(r => r.Empresa.CondicaoFaturamento == Empresa.CondicoesFaturamento.Mensal).ToList().Sum(r => r.Valor);
                SumFatImediato += Pedidos.Where(r => r.Empresa.CondicaoFaturamento == Empresa.CondicoesFaturamento.Imediato).ToList().Sum(r => r.Valor);
                SumFatInstantaneo += Pedidos.Where(r => r.Empresa.FaturamentoInstantaneo).ToList().Sum(r => r.Valor);
            }

            ViewBag.SumComOC = SumComOC.ToString("C2");
            ViewBag.SumSemOC = SumSemOC.ToString("C2");

            ViewBag.SumFatMensal = SumFatMensal.ToString("C2");
            ViewBag.SumFatImediato = SumFatImediato.ToString("C2");
            ViewBag.SumFatInstantaneo = SumFatInstantaneo.ToString("C2");

            return View(resultado);
        }

        public ActionResult Faturar(int id)
        {
            var empresa = empresaRepository.Get(id);
            if (empresa != null)
            {
                ViewBag.DataVencimento = DateTime.Now.AddDays(empresa.DiasParaVencimentoBoleto).ToString("dd/MM/yyyy");
                ViewBag.Empresa = empresa;
                Expression<Func<PedidoEmpresa, bool>> exp = p => p.EmpresaID == id && !p.FaturamentoID.HasValue;
                exp = exp.And(p => p.StatusID != (int)PedidoEmpresa.TodosStatus.PedidoCancelado && p.StatusID != (int)PedidoEmpresa.TodosStatus.PedidoReprovado);
                exp = exp.And(p => p.StatusEntregaID != (int) TodosStatusEntrega.Cancelado);
                exp = exp.And(p => !p.StatusCancelamentoID.HasValue);
                var pedidos = pedidoRepository.GetByExpression(exp).ToList();
                return View(pedidos);
            }
            else
                return RedirectToAction("Novo");
        }

        public ContentResult Boleto(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var html = BoletoService.Gerar(fatura.Boleto);
                Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                return Content("");
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }

        public JsonResult Cancelar(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.Status == Faturamento.TodosStatus.AguardandoPagamento)
                {
                    if (fatura.StatusPagamento != Pedido.TodosStatusPagamento.Pago)
                    {
                        //Registra log do usuário
                        administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.CancelouFatura);

                        fatura.Status = Faturamento.TodosStatus.Cancelada;
                        fatura.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                        fatura.DataProcessamento = DateTime.Now;
                        fatura.DataAtualizacao = DateTime.Now;
                        if(fatura.PedidoEmpresas.Count > 0)
                            fatura.Observacoes += "-----------------------------------\nFatura cancelada com os seguintes pedidos retirados:\n\n";
                        faturamentoRepository.Save(fatura);

                        foreach (var pedido in fatura.PedidoEmpresas.ToList())
                        {
                            fatura.Observacoes += "ID: " + pedido.ID + " - PRODUTO: " + pedido.ProdutoTamanho.Produto.Nome + "\n";
                            pedido.FaturamentoID = null;
                            pedidoRepository.Save(pedido);
                        }
                        faturamentoRepository.Save(fatura);

                        if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto && fatura.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                        {
                            try
                            {
                                var boleto = fatura.Boleto;
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                boleto.Processado = false;
                                boletoRepository.Save(boleto);
                            }
                            catch { }
                        }
                        return Json("OK");
                    }
                    else
                        return Json("A fatura não pode ser cancelada porque já foi paga.");
                }
                else
                    return Json("A fatura não pode ser cancelada porque deve ser nova ou aguardando pagamento.");
            }
            else
                return Json("A fatura não pode ser cancelada. Tente novamente mais tarde");
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Faturar(FormCollection form)
        {
            var EmpresaID = Convert.ToInt32(form["EmpresaID"]);

            var fatura = new Faturamento();
            fatura.EmpresaID = EmpresaID;
            fatura.Titulo = form["Titulo"];
            fatura.Observacoes = form["Observacoes"];
            fatura.EmailsFinanceiro = form["EmailsFinanceiro"];
            fatura.Status = Faturamento.TodosStatus.AguardandoPagamento;
            fatura.StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento;
            fatura.FormaPagamentoID = Convert.ToInt32(form["FormaPagamentoID"]);
            fatura.MeioPagamentoID = Convert.ToInt32(form["MeioPagamentoID"]);
            fatura.ValorTotal = 0;
            fatura.ValorDesconto = Convert.ToDecimal(form["ValorDesconto"]);
            fatura.ValorEstorno = 0;
            fatura.ValorPago = 0;
            fatura.NFeValor = 0;
            fatura.DataCriacao = DateTime.Now;
            fatura.DataAtualizacao = DateTime.Now;
            fatura.DataVencimento = Convert.ToDateTime(form["DataVencimento"]);
            fatura.DataVencimentoOriginal = fatura.DataVencimento;
            faturamentoRepository.Save(fatura);

            decimal total = 0;

            if (Request.Form["pedidos"] != null)
            {
                var i = 0;
                foreach (var id in Request.Form["pedidos"].Split(','))
                {
                    i++;
                    int _id = 0;
                    Int32.TryParse(id.ToString(), out _id);
                    if (_id > 0)
                    {
                        var pedido = pedidoRepository.Get(_id);
                        pedido.FaturamentoID = fatura.ID;
                        total += pedido.Valor;
                    }
                }
                if (i == 0)
                {
                    var pedido = pedidoRepository.Get(Convert.ToInt32(Request.Form["pedidos"]));
                    pedido.FaturamentoID = fatura.ID;
                    total += pedido.Valor;
                }
            }

            fatura.ValorTotal = total - fatura.ValorDesconto;
            fatura.NFeValor = fatura.ValorTotal;
            faturamentoRepository.Save(fatura);

            if (fatura.ValorTotal > 0)
            {
                if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto)
                {
                    var random = new Random();
                    var nossoNumero = "";
                    do
                    {
                        nossoNumero = random.Next(99999999).ToString("00000000");
                    }
                    while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                    var numeroDocumento = "2" + DateTime.Now.Year + fatura.ID.ToString("00000");
                    
                    var boleto = new Boleto();
                    
                    boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                    boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                    boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                    boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                    boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                    boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                    boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC;

                    boleto.FaturamentoID = fatura.ID;
                    boleto.TipoID = (int)Domain.Entities.Boleto.TodosTipos.PJ;
                    boleto.DataCriacao = DateTime.Now;
                    boleto.DataVencimento = fatura.DataVencimento;
                    boleto.DataEnvioLembreteVencimento = null;
                    boleto.NossoNumero = nossoNumero;
                    boleto.NumeroDocumento = numeroDocumento;
                    boleto.SacadoDocumento = fatura.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "").Trim();
                    boleto.SacadoNome = fatura.Empresa.RazaoSocial;
                    if (String.IsNullOrEmpty(fatura.Empresa.Bairro))
                        boleto.SacadoBairro = "";
                    else
                        boleto.SacadoBairro = fatura.Empresa.Bairro;

                    if (String.IsNullOrEmpty(fatura.Empresa.CEP))
                        boleto.SacadoCEP = "";
                    else
                        boleto.SacadoCEP = fatura.Empresa.CEP;

                    if (fatura.Empresa.Cidade == null)
                        boleto.SacadoCidade = "";
                    else
                        boleto.SacadoCidade = fatura.Empresa.Cidade.Nome;

                    if (String.IsNullOrEmpty(fatura.Empresa.Logradouro))
                        boleto.SacadoEndereco = "";
                    else
                        boleto.SacadoEndereco = fatura.Empresa.Logradouro + " " + fatura.Empresa.Numero + " " + fatura.Empresa.Complemento;

                    if (fatura.Empresa.Estado == null)
                        boleto.SacadoUF = "";
                    else
                        boleto.SacadoUF = fatura.Empresa.Estado.Sigla;

                    boleto.Comando = Domain.Entities.Boleto.Comandos.CriarRemessa;
                    boleto.Processado = false;
                    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.AguardandoPagamento;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Criado;
                    boleto.Valor = fatura.ValorTotal;
                    boleto.DataVencimentoOriginal = boleto.DataVencimento;

                    boletoRepository.Save(boleto);

                    new Domain.Entities.Faturamento().EnviarBoleto(fatura.ID);
                }
            }

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "A fatura " + fatura.ID + " para o cliente " + fatura.Empresa.RazaoSocial + " foi criado com sucesso!" });
        }
         
        public ActionResult Visualizar(int id)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var usuario = LoggedUser.GetFromJSON(HttpContext.User.Identity.Name);

            if(usuario.AccessGroups.First() == "Socios" || usuario.AccessGroups.First() == "Financeiro")
            {
                ViewBag.LiberaObsFinan = true;
            }
            else
            {
                ViewBag.LiberaObsFinan = false;
            }

            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                ViewBag.Empresas = empresaRepository.GetAll().OrderBy(c => c.RazaoSocial).ToList();

                #region VIEW BAG ESTADOS E CIDADES 

                var estados = estadoRepository.GetAll().ToList();
                int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                List<Estado> lstEstados = new List<Estado>();
                foreach (int idEstado in principaisEstados)
                {
                    lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                }
                lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                ViewBag.Estados = lstEstados;

                #endregion

                #region VIEW BAG ESTADOS E CIDADES NF SUBSTITUTA

                if (fatura.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && fatura.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade != null)
                {
                    var IDResult = (int)fatura.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Estado.ID;

                    ViewBag.cidadesNFSubstituta = cidadeRepository.GetByExpression(e => e.EstadoID == IDResult).OrderBy(e => e.Nome);
                }


                #endregion

                return View(fatura);
            }
            else
                return RedirectToAction("List");
        }

        public JsonResult RegistraCliqueImpressaoBoleto(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.ImprimiuBoleto);
                return Json("OK");
            }
            else
                return Json("ERRO");
        }

        public JsonResult AlterarPagamento(int id, string forma, string meio, string status, string data)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var datapagamento = new DateTime();
                DateTime.TryParse(data, out datapagamento);

                if (datapagamento != new DateTime())
                {
                    fatura.DataPagamento = datapagamento;
                    fatura.ValorPago = fatura.ValorTotal;
                    fatura.Status = Faturamento.TodosStatus.Paga;
                    fatura.StatusPagamento = Pedido.TodosStatusPagamento.Pago;
                }
                if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto && fatura.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                {
                    if (Convert.ToInt32(forma) != fatura.FormaPagamentoID && Convert.ToInt32(meio) != fatura.MeioPagamentoID)
                    {
                        var boleto = fatura.Boleto;
                        if (boleto != null)
                        {
                            if (boleto.RemessaBoletoes.Count > 0)
                            {
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processando;
                                boleto.Processado = false;
                            }
                            else
                            {
                                boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.Cancelado;
                                boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Processado;
                                boleto.Comando = Domain.Entities.Boleto.Comandos.Cancelar;
                                boleto.Processado = true;
                            }
                            boletoRepository.Save(boleto);
                        }
                    }
                }

                fatura.FormaPagamentoID = Convert.ToInt32(forma);
                fatura.MeioPagamentoID = Convert.ToInt32(meio);
                fatura.StatusPagamentoID = Convert.ToInt32(status);

                if (Convert.ToInt32(status) == 1)
                    fatura.StatusID = 1;

                fatura.DataAtualizacao = DateTime.Now;
                faturamentoRepository.Save(fatura);

                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.AlterouFormaPagamento);

                if (fatura.FormaPagamento == Pedido.FormasPagamento.Boleto && fatura.Boleto == null)
                {
                    var random = new Random();
                    var nossoNumero = "";
                    do
                    {
                        nossoNumero = random.Next(99999999).ToString("00000000");
                    }
                    while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                    var numeroDocumento = "2" + DateTime.Now.Year + fatura.ID.ToString("00000");

                    var tipoID = Domain.Entities.Boleto.TodosTipos.PJ; 

                    var boleto = new Boleto();
                    boleto.FaturamentoID = fatura.ID;
                    boleto.TipoID = (int)tipoID;
                    boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                    boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                    boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                    boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                    boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                    boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                    boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC;
                    boleto.DataCriacao = DateTime.Now;
                    boleto.DataVencimento = new Domain.Core.Funcoes().DataVencimentoUtil(fatura.Empresa.DiasParaVencimentoBoleto);
                    boleto.NossoNumero = nossoNumero;
                    boleto.NumeroDocumento = numeroDocumento;
                    boleto.SacadoDocumento = fatura.Empresa.CNPJ;
                    boleto.SacadoNome = fatura.Empresa.RazaoSocial;
                    if (String.IsNullOrEmpty(fatura.Empresa.Bairro))
                        boleto.SacadoBairro = "";
                    else
                        boleto.SacadoBairro = fatura.Empresa.Bairro;

                    if (String.IsNullOrEmpty(fatura.Empresa.CEP))
                        boleto.SacadoCEP = "";
                    else
                        boleto.SacadoCEP = fatura.Empresa.CEP;

                    if (fatura.Empresa.Cidade == null)
                        boleto.SacadoCidade = "";
                    else
                        boleto.SacadoCidade = fatura.Empresa.Cidade.Nome;

                    if (String.IsNullOrEmpty(fatura.Empresa.Logradouro))
                        boleto.SacadoEndereco = "";
                    else
                        boleto.SacadoEndereco = fatura.Empresa.Logradouro + " " + fatura.Empresa.Numero + " " + fatura.Empresa.Complemento;

                    if (fatura.Empresa.Estado == null)
                        boleto.SacadoUF = "";
                    else
                        boleto.SacadoUF = fatura.Empresa.Estado.Sigla;

                    boleto.Comando = Domain.Entities.Boleto.Comandos.CriarRemessa;
                    boleto.Processado = false;
                    boleto.StatusPagamento = Domain.Entities.Boleto.TodosStatusPagamento.AguardandoPagamento;
                    boleto.StatusProcessamento = Domain.Entities.Boleto.TodosStatusProcessamento.Criado;
                    boleto.Valor = fatura.ValorTotal;
                    boleto.DataVencimentoOriginal = boleto.DataVencimento;

                    boletoRepository.Save(boleto);

                    var Fatura = boleto.Faturamento;
                    Fatura.DataVencimento = boleto.DataVencimento;

                    faturamentoRepository.Save(Fatura);
                }
                return Json("OK");

            }
            else
                return Json("A fatura não pode ser alterada. Tente novamente mais tarde");
        }

        public JsonResult AlterarVencimentoBoleto(int id, string data)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = fatura.Boleto;
                    boleto.Processado = false;
                    if (boleto.RemessaBoletoes.Count > 0)
                        boleto.Comando = Domain.Entities.Boleto.Comandos.ModificarVencimento;

                    boleto.DataVencimento = datavencimento;
                    boleto.DataVencimentoOriginal = datavencimento;

                    boleto.PrimeiroRenegociado = false;
                    boleto.SegundoRenegociado = false;

                    boleto.DataEnvioLembreteVencimento = null;
                    boletoRepository.Save(boleto);

                    var Fatura = boleto.Faturamento;

                    Fatura.DataVencimento = datavencimento;
                    Fatura.DataVencimentoOriginal = datavencimento;

                    fatura.PrimeiroRenegociado = false;
                    fatura.SegundoRenegociado = false;

                    faturamentoRepository.Save(Fatura);
                    return Json("OK");
                }
                else
                    return Json("Data inválida");
            }
            else
                return Json("A fatura não pode ser alterada. Tente novamente mais tarde");
        }

        public JsonResult EnviarBoleto(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                fatura.EnviarBoleto(id);

                //Registra log do usuário
                administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.EnviouEmailBoleto);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }

        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var fatura = faturamentoRepository.Get(id);

            if (fatura != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(form["NFeNumero"]))
                    {
                        var NFeNumero = Convert.ToInt32(form["NFeNumero"]);
                        if (fatura.NFeNumero != NFeNumero)
                        {
                            //Registra log do usuário
                            administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.PreencheuNF);
                        }
                        fatura.NFeNumero = NFeNumero;
                    }
                    else
                    {
                        fatura.NFeNumero = null;
                    }
                }
                catch { }
                try {

                    if (!string.IsNullOrEmpty(form["NFeValor"]))
                    {
                        fatura.NFeValor = Convert.ToDecimal(form["NFeValor"]);
                    }
                    else
                    {
                        fatura.NFeValor = 0;
                    }
                }
                catch { }
                try {

                    if (!string.IsNullOrEmpty(form["NFeData"]))
                    {
                        fatura.NFeData = Convert.ToDateTime(form["NFeData"]);
                    }
                    else
                    {
                        fatura.NFeData = null;
                    }
                }
                catch { }

                try
                {

                    if (!string.IsNullOrEmpty(form["CaminhoXmlNotaFiscal"]))
                    {
                        fatura.CaminhoXmlNotaFiscal = form["CaminhoXmlNotaFiscal"];
                    }
                    else
                    {
                        fatura.CaminhoXmlNotaFiscal = null;
                    }
                }
                catch { }

                if (!string.IsNullOrEmpty(form["DataVencimento"]))
                {
                    try
                    {
                        if (fatura.DataVencimento != Convert.ToDateTime(form["DataVencimento"]) && fatura.FormaPagamentoID != (int)Pedido.FormasPagamento.Boleto) {
                            fatura.DataVencimentoOriginal = Convert.ToDateTime(form["DataVencimento"]);
                            fatura.PrimeiroRenegociado = false;
                            fatura.SegundoRenegociado = false;
                        }

                        fatura.DataVencimento = Convert.ToDateTime(form["DataVencimento"]);

                    }
                    catch { }
                }

                var enviaNFe = !String.IsNullOrEmpty(fatura.NFeLink) && fatura.NFeLink != form["NFeLink"] && !fatura.DataEmailNFe.HasValue;                 
                 
                fatura.NFeLink = form["NFeLink"].Trim();
                fatura.DataAtualizacao = DateTime.Now;
                fatura.Observacoes = form["Observacoes"];

                if (enviaNFe)
                {
                    if (!string.IsNullOrEmpty(fatura.NFeLink))
                    {
                        administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.EnviouEmailNFe);
                        fatura.EnviarNFe();
                        fatura.DataEmailNFe = DateTime.Now;
                        faturamentoRepository.Save(fatura);
                    }
                }

                #region NF SUBSTITUTA

                if (!string.IsNullOrEmpty(form["NFSubstituta.Documento"]))
                {
                    var NFSubstituta_NomeRazao = form["NFSubstituta.NomeRazao"];
                    var NFSubstituta_Documento = form["NFSubstituta.Documento"];
                    var NFSubstituta_IE = form["NFSubstituta.IE"];
                    var NFSubstituta_Logradouro = form["NFSubstituta.Logradouro"];
                    var NFSubstituta_Numero = form["NFSubstituta.Numero"];
                    var NFSubstituta_Complemento = form["NFSubstituta.Complemento"];
                    var NFSubstituta_CEP = form["NFSubstituta.CEP"];
                    var NFSubstituta_TipoIE = form["NFSubstituta.TipoIE"];

                    int NFSubstituta_EstadoID = 0;
                    if (!string.IsNullOrEmpty(form["NFSubstituta_EstadoID"]))
                    {
                        NFSubstituta_EstadoID = int.Parse(form["NFSubstituta_EstadoID"]);
                    }

                    int NFSubstituta_CidadeID = 0;
                    if (!string.IsNullOrEmpty(form["NFSubstituta_CidadeID"]))
                    {
                        NFSubstituta_CidadeID = int.Parse(form["NFSubstituta_CidadeID"]);
                    }

                    var NFSubstituta_Bairro = form["NFSubstituta.Bairro"];

                    if (fatura.NFSubstituta_X_Pedido_PedidoEmpresa.Any())
                    {
                        var NfSubstituta = fatura.NFSubstituta_X_Pedido_PedidoEmpresa.First();
                        NfSubstituta.NFSubstituta.NomeRazao = NFSubstituta_NomeRazao;
                        NfSubstituta.NFSubstituta.Documento = NFSubstituta_Documento;
                        NfSubstituta.NFSubstituta.Logradouro = NFSubstituta_Logradouro;
                        NfSubstituta.NFSubstituta.Numero = NFSubstituta_Numero;
                        NfSubstituta.NFSubstituta.Complemento = NFSubstituta_Complemento;
                        NfSubstituta.NFSubstituta.CEP = NFSubstituta_CEP;
                        NfSubstituta.NFSubstituta.Estado = estadoRepository.Get(NFSubstituta_EstadoID);
                        NfSubstituta.NFSubstituta.Cidade = cidadeRepository.Get(NFSubstituta_CidadeID);
                        NfSubstituta.NFSubstituta.Bairro = NFSubstituta_Bairro;

                        NfSubstituta.NFSubstituta.TipoIE = int.Parse(NFSubstituta_TipoIE);

                        switch (NfSubstituta.NFSubstituta.TipoIE)
                        {
                            case 1:
                                NfSubstituta.NFSubstituta.IE = "ISENTO";
                                break;
                            case 2:
                                NfSubstituta.NFSubstituta.IE = "NÃO CONTRIBUINTE";
                                break;
                            default:
                                NfSubstituta.NFSubstituta.IE = NFSubstituta_IE;
                                break;
                        }

                    }
                    else
                    {
                        var NewNfSubstitura = new NFSubstituta_X_Pedido_PedidoEmpresa();
                        NewNfSubstitura.NFSubstituta = new NFSubstituta();

                        NewNfSubstitura.NFSubstituta.NomeRazao = NFSubstituta_NomeRazao;
                        NewNfSubstitura.NFSubstituta.Documento = NFSubstituta_Documento;
                        NewNfSubstitura.NFSubstituta.Logradouro = NFSubstituta_Logradouro;
                        NewNfSubstitura.NFSubstituta.Numero = NFSubstituta_Numero;
                        NewNfSubstitura.NFSubstituta.Complemento = NFSubstituta_Complemento;
                        NewNfSubstitura.NFSubstituta.CEP = NFSubstituta_CEP;
                        NewNfSubstitura.NFSubstituta.Estado = estadoRepository.Get(NFSubstituta_EstadoID);
                        NewNfSubstitura.NFSubstituta.Cidade = cidadeRepository.Get(NFSubstituta_CidadeID);
                        NewNfSubstitura.NFSubstituta.Bairro = NFSubstituta_Bairro;
                        fatura.NFSubstituta_X_Pedido_PedidoEmpresa.Add(NewNfSubstitura);

                        NewNfSubstitura.NFSubstituta.TipoIE = int.Parse(NFSubstituta_TipoIE);

                        switch (NewNfSubstitura.NFSubstituta.TipoIE)
                        {
                            case 1:
                                NewNfSubstitura.NFSubstituta.IE = "ISENTO";
                                break;
                            case 2:
                                NewNfSubstitura.NFSubstituta.IE = "NÃO CONTRIBUINTE";
                                break;
                            default:
                                NewNfSubstitura.NFSubstituta.IE = NFSubstituta_IE;
                                break;
                        }

                        fatura.Observacoes += " Nova NF Substituta criada. NF antiga: " + fatura.NFeLink;
                    }

                   
                }

                #endregion

                faturamentoRepository.Save(fatura);

                return RedirectToAction("Visualizar", new { id = fatura.ID, cod = "SaveSucess" });
            }
            else
                return RedirectToAction("List", new { cod = "SaveFailure", msg = "Fatura inexistente!" });
        }

        public JsonResult EnviarNFe(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null && !string.IsNullOrEmpty(fatura.NFeLink))
            {
                administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.EnviouEmailNFe);
                fatura.EnviarNFe();
                fatura.DataEmailNFe = DateTime.Now;
                faturamentoRepository.Save(fatura);
                return Json("OK");
            }
            else
                return Json("O e-mail não pode ser enviado. Tente novamente mais tarde");
        }
    }
}