﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using System.Web.Security;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;

namespace Admin.Controllers
{
    public class LoginController : Controller
    {

        private IPersistentRepository<Administrador> administradorRepository;
        //private IPersistentRepository<Meta> metaRepository;
        public LoginController(ObjectContext context)
        {
            administradorRepository = new PersistentRepository<Administrador>(context);
            //metaRepository = new PersistentRepository<Meta>(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            var email = form["login_email"];
            var senha = form["login_senha"];

            var administrador = administradorRepository.GetByExpression(a => a.Email == email && a.Senha == senha && a.Liberado).FirstOrDefault();
            if (administrador != null)
            {
                var user = new LoggedUser();
                user.ID = administrador.ID;
                user.Name = administrador.Nome;
                user.Username = administrador.Email;
                user.ExpiresIn = DateTime.Now.AddHours(6);
                user.AcessTime = DateTime.Now;
                user.AditionalInfo = new Dictionary<string, string>();
                user.AuthenticatedAreaName = "Admin";
                user.AccessGroups.Add(administrador.Perfil.ToString());
                FormsAuthentication.SetAuthCookie(user.ToJSON(), true);

                if (form["ReturnUrl"] != null)
                {
                    if (form["ReturnUrl"] != "/Home/AccessDenied")
                    {
                        return Redirect(form["ReturnUrl"] + "&userID=" + user.ID + "&name=" + user.Name);
                    }
                }

                if(administrador.Perfil == Administrador.Perfis.Fornecedores)
                {
                    return Redirect("/Dashboards?userID=" + user.ID + "&name=" + user.Name);
                }
                else
                {
                    return Redirect("/?userID=" + user.ID + "&name=" + user.Name);
                }

               
            }

            ViewBag.Mensagem = "Login e/ou Senha inválido(s)";
            return View("Index");
        }


    }
}
