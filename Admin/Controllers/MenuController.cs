﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{

    [AreaAuthorization("Administrador", "/admin/login")]
    public class MenuController : Controller
    {

        public ActionResult Superior()
        {
            var usuario = LoggedUser.GetFromJSON(HttpContext.User.Identity.Name);
            return View(usuario);
        }

        public ActionResult Lateral()
        {
            var usuario = LoggedUser.GetFromJSON(HttpContext.User.Identity.Name);
            return View(usuario);
        }

    }
}
