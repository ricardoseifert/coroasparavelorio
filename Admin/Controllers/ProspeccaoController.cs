﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Domain.Repositories;
using Admin.ViewModels;
using System.Net.Mail;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class ProspeccaoController : CrudController<Cliente>
    {
        private ClienteRepository clienteRepository;
        private IPersistentRepository<Empresa> empresaRepository;
        private IPersistentRepository<EmpresaFollowUp> empresaFollowRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private IPersistentRepository<ProspeccaoNota> notaRepository;
        private IPersistentRepository<ProspeccaoContato> contatoRepository;
        private IPersistentRepository<ProspeccaoAgenda> agendaRepository;
        private IPersistentRepository<vwEmpresasNaoLaco> empresasRepository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        //private IPersistentRepository<Agend> _agendaRepository;
        //private IPersistentRepository<AgendaItem> agendaItemRepository;
        private IPersistentRepository<ProspeccaoComplemento> prospeccaoComplementoRepository;
        private AdminModel adminModel;

        public ProspeccaoController(ObjectContext context)
            : base(context)
        {
            clienteRepository = new ClienteRepository(context);
            empresaRepository = new PersistentRepository<Empresa>(context);
            empresaFollowRepository = new PersistentRepository<EmpresaFollowUp>(context);
            administradorRepository = new PersistentRepository<Administrador>(context);
            notaRepository = new PersistentRepository<ProspeccaoNota>(context);
            contatoRepository = new PersistentRepository<ProspeccaoContato>(context);
            agendaRepository = new PersistentRepository<ProspeccaoAgenda>(context);
            empresasRepository = new PersistentRepository<vwEmpresasNaoLaco>(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            //_agendaRepository = new PersistentRepository<Agend>(context);
            //agendaItemRepository = new PersistentRepository<AgendaItem>(context);
            prospeccaoComplementoRepository = new PersistentRepository<ProspeccaoComplemento>(context);
            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args)
        {
            var resultado = empresasRepository.GetAll().ToList(); 

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.RazaoSocial.ToLower().Contains(nome.ToLower()) || c.Nome.ToLower().Contains(nome.ToLower())).ToList();

            var cpf = Request.QueryString["cpf"];
            if (!String.IsNullOrEmpty(cpf))
                resultado = resultado.Where(c => c.Documento.ToLower().Contains(cpf.ToLower())).ToList();

            int atendimento = 0;
            Int32.TryParse(Request.QueryString["atendimento"], out atendimento);
            if (atendimento > 0)
                resultado = resultado.Where(c => c.LacosAtendimentoID == atendimento).ToList();

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);
            if (status > 0)
                resultado = resultado.Where(c => c.LacosStatusProspecaoID == status).ToList(); 

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Atendimento = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(c => c.Nome).ToList();

            ViewBag.Admin = adminModel.CurrentAdministrador;

            //TODO: Melhorar isso
            var clientes = new List<Cliente>();
            foreach (var item in resultado)
                clientes.Add(clienteRepository.Get(item.ID));
            return View(clientes);
        }

        public ActionResult Agenda()
        {
            var data = DateTime.Now.AddMonths(-1);
            var agenda = agendaRepository.GetByExpression(c => c.Data >= data);
            var follow = empresaFollowRepository.GetByExpression(c => c.Data >= data);
            var clientes = clienteRepository.GetByExpression(c => c.LacosDataProximoContato.HasValue).ToList().Where(c => c.LacosDataProximoContato.Value.DayOfYear >= DateTime.Now.DayOfYear);
            //var reunioes = _agendaRepository.GetByExpression(c => c.Data >= data);
            //var tarefas = agendaItemRepository.GetByExpression(c => c.Deadline >= data);
            ViewBag.Clientes = clientes.ToList();
            ViewBag.Follow = follow.ToList();
            //ViewBag.Reunioes = reunioes.ToList();
            //ViewBag.Tarefas = tarefas.ToList();
            return View(agenda.ToList());
        }

        public ActionResult Aniversariantes()
        {
            var colaboradores = colaboradorRepository.GetByExpression(d => d.DataNascimento.HasValue).ToList().Where(d => (d.DataNascimento.Value.DayOfYear - DateTime.Now.DayOfYear) <= 10 && (d.DataNascimento.Value.DayOfYear - DateTime.Now.DayOfYear) >= 0);
            return View(colaboradores.ToList());
        }

        [HttpPost]
        public JsonResult AtualizarAtendimento(int id, int atendimento)
        {
            if (adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Socios || adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.LacosCorporativos || adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Coordenadores)
            {
                var cliente = clienteRepository.Get(id);
                if (cliente != null)
                {
                    cliente.LacosAtendimentoID = atendimento;
                    clienteRepository.Save(cliente);
                    return Json("OK");
                }
                else
                    return Json("Não foi possível gravar, tente novamente.");
            }
            else
            {
                return Json("Somente usuários sócios, laços corporativos ou coordenadores podem alterar o atendimento.");
            }
        }
        [HttpPost]
        public JsonResult AdicionarNota(int id, string observacaonota)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                ProspeccaoNota nota = new ProspeccaoNota();
                nota.Observacoes = observacaonota;
                nota.ClienteID = id;
                nota.DataCriacao = DateTime.Now;
                notaRepository.Save(nota);

                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }
        [HttpPost]
        public JsonResult AdicionarReuniao(int id, string data, string obs)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                var agenda = new ProspeccaoAgenda();
                agenda.Observacao = obs;
                agenda.ClienteID = id;
                agenda.AtendimentoID = adminModel.ID;
                agenda.Data = Convert.ToDateTime(data);
                agenda.DataCriacao = DateTime.Now;
                agendaRepository.Save(agenda);

                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }
        [HttpPost]
        public JsonResult AdicionarContato(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                ProspeccaoContato contato = new ProspeccaoContato();
                contato.ClienteID = id;
                contato.Nome = form["NomeContato"];
                contato.Email = form["EmailContato"];
                contato.TelefoneContato = form["TelefonesContato"];
                contato.Departamento = form["DepartamentoContato"];
                contato.Produto = form["ProdutoContato"];
                contato.Observacoes = form["ObsContato"];
                contato.DataCriacao = DateTime.Now;
                contatoRepository.Save(contato);
                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }
        [HttpPost]
        public JsonResult ReuniaoRealizada(int id, int realizada)
        {
            var agenda = agendaRepository.Get(id);
            if (agenda != null)
            {
                agenda.Realizada = realizada == 1;
                agendaRepository.Save(agenda);

                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }
        [HttpPost]
        public JsonResult Atualizar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                try
                {
                    cliente.LacosDataContato = Convert.ToDateTime(form["LacosDataContato"]);
                }
                catch { }
                cliente.LacosEmailEnviado = form["LacosEmailEnviado"] == "1";
                try
                {
                    cliente.LacosDataEmailEnviado = Convert.ToDateTime(form["LacosDataEmailEnviado"]);
                }
                catch { }
                cliente.LacosNomeEmailEnviado = form["LacosNomeEmailEnviado"];
                cliente.LacosEnderecoEmailEnviado = form["LacosEnderecoEmailEnviado"];
                cliente.LacosTextoEmailEnviado = form["LacosTextoEmailEnviado"];
                cliente.LacosStatusProspecaoID = Convert.ToInt32(form["status"]);
                try
                {
                    cliente.LacosDataProximoContato = Convert.ToDateTime(form["LacosDataProximoContato"]);
                }
                catch { }
                cliente.LacosInteresse = form["LacosInteresse"] == "1";
                cliente.LacosInteresseMotivo = form["LacosInteresseMotivo"];
                cliente.LacosReuniaoAgendada = form["LacosReuniaoAgendada"] == "1";
                cliente.LacosReuniaoAgendadaMotivo = form["LacosReuniaoAgendadaMotivo"];
                cliente.LacosTesteRealizado = form["LacosTesteRealizado"] == "1";
                cliente.NotaPlanoAcao = form["NotaPlanoAcao"];
                clienteRepository.Save(cliente);
                return Json("OK");
            }
            else
                return Json("Não foi possível gravar, tente novamente.");
        }

        //[HttpGet]
        //public ActionResult EditReuniao(int? id)
        //{
        //    var agenda = new Agend();
        //    if (id.HasValue)
        //    {
        //        agenda = _agendaRepository.Get(id.Value);
        //    }
        //    ViewBag.Comerciais = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(e => e.Nome);
        //    ViewBag.Empresas = empresaRepository.GetAll().OrderBy(p => p.RazaoSocial);
        //    return View(agenda);
        //}

        //[ValidateInput(false)]
        //public ActionResult EditReuniao(Agend agenda)
        //{
        //    agenda.TipoID = (int)Domain.Entities.Agend.Tipos.Reuniao;
        //    _agendaRepository.Save(agenda);
        //    return RedirectToAction("agenda", "prospeccao", new { cod = "SaveSucess" });
        //}

        //[HttpGet]
        //public ActionResult EditTarefa(int? id)
        //{
        //    var agenda = new AgendaItem();
        //    if (id.HasValue)
        //    {
        //        agenda = agendaItemRepository.Get(id.Value);
        //    }
        //    ViewBag.Comerciais = administradorRepository.GetByExpression(c => c.Comercial && c.Liberado).OrderBy(e => e.Nome);
        //    ViewBag.Administrador = adminModel.CurrentAdministrador;
        //    return View(agenda);
        //}

        //[ValidateInput(false)]
        //public ActionResult EditTarefa(AgendaItem agenda)
        //{
        //    var novo = agenda.ID == 0;
        //    if (novo)
        //    {
        //        agenda.DataCriacao = DateTime.Now;
        //        agenda.SolicitanteID = adminModel.CurrentAdministrador.ID; 
        //    }
        //    agendaItemRepository.Save(agenda);
        //    var prioridade = MailPriority.Normal;
        //    if (agenda.Prioridade == "ALTA")
        //        prioridade = MailPriority.High;
        //    else if (agenda.Prioridade == "BAIXA")
        //        prioridade = MailPriority.Low;
        //    if (novo)
        //    {
        //        var corpo = string.Format("Uma nova tarefa foi criada para você. Clique no link {0} para visualizá-la.", "http://admin.coroasparavelorio.com.br/prospeccao/edittarefa?id=" + agenda.ID);
        //        Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Grupo Laços Flores", agenda.Administrador.Email, agenda.Administrador.Nome, agenda.Administrador1.Email, agenda.Administrador1.Nome, null, "[NOVA TAREFA] - " + agenda.Titulo, corpo, false, prioridade);
        //    }
        //    else
        //    {
        //        var corpo = string.Format("Sua tarefa foi alterada. Clique no link {0} para visualizá-la.", "http://admin.coroasparavelorio.com.br/prospeccao/edittarefa?id=" + agenda.ID);
        //        Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Grupo Laços Flores", agenda.Administrador.Email, agenda.Administrador.Nome, agenda.Administrador1.Email, agenda.Administrador1.Nome, null, "[ATUALIZAÇÃO DE TAREFA] - " + agenda.Titulo, corpo, false, prioridade);
        //    }                   

        //    return RedirectToAction("agenda", "prospeccao", new { cod = "SaveSucess" });
        //}

        //public ActionResult DelTarefa(int id)
        //{
        //    var agenda = _agendaRepository.Get(id);
        //    _agendaRepository.Delete(agenda);
        //    return RedirectToAction("agenda", "prospeccao");
        //}

        //public ActionResult DelReuniao(int id)
        //{
        //    var agenda = _agendaRepository.Get(id);
        //    _agendaRepository.Delete(agenda);
        //    return RedirectToAction("agenda", "prospeccao");
        //}

        public ActionResult Prospeccao(int id)
        {
            var cliente = clienteRepository.Get(id);
            if (cliente != null)
            {
                return View(cliente);
            }
            else
                return RedirectToAction("List");
        }

        public ActionResult Complemento()
        {
            var clienteid = 0;
            var empresaid = 0;
            Int32.TryParse(Request.QueryString["clienteid"], out clienteid);
            Int32.TryParse(Request.QueryString["empresaid"], out empresaid);

            if (clienteid > 0)
            {
                var cliente = clienteRepository.Get(clienteid);
                if (cliente != null)
                {
                    ViewBag.Cliente = cliente;
                }
                else
                    return RedirectToAction("List");

                var prospeccao = prospeccaoComplementoRepository.GetByExpression(c => c.ClienteID == clienteid).FirstOrDefault();
                if (prospeccao == null)
                {
                    prospeccao = new ProspeccaoComplemento();
                    prospeccao.ClienteID = clienteid;
                    prospeccao.DataCriacao = DateTime.Now;
                }
                return View(prospeccao);
            }

            if (empresaid > 0)
            {
                var empresa = empresaRepository.Get(empresaid);
                if (empresa != null)
                {
                    ViewBag.Empresa = empresa;
                }
                else
                    return RedirectToAction("List");

                var prospeccao = prospeccaoComplementoRepository.GetByExpression(c => c.EmpresaID == empresaid).FirstOrDefault();
                if (prospeccao == null)
                {
                    prospeccao = new ProspeccaoComplemento();
                    prospeccao.EmpresaID = empresaid;
                    prospeccao.DataCriacao = DateTime.Now;
                }
                return View(prospeccao);
            }
            return RedirectToAction("List");
        }

        [ValidateInput(false)]
        public ActionResult SalvarComplemento(ProspeccaoComplemento prospeccao)
        {
            prospeccaoComplementoRepository.Save(prospeccao);
            if (!prospeccao.EmpresaID.HasValue)
                return RedirectToAction("Complemento", new { cod = "SaveSucess", clienteid = prospeccao.ClienteID });
            else
                return RedirectToAction("Complemento", new { cod = "SaveSucess", empresaid = prospeccao.EmpresaID });
        }

        public ActionResult Export()
        {
            var resultado = empresasRepository.GetAll().ToList();

            var datacadastro = new DateTime();
            DateTime.TryParse(Request.QueryString["datacadastro"], out datacadastro);
            if (datacadastro != new DateTime())
                resultado = resultado.Where(c => c.DataCadastro >= datacadastro && c.DataCadastro <= datacadastro.AddDays(1).AddMinutes(-1)).ToList();

            var nome = Request.QueryString["nome"];
            if (!String.IsNullOrEmpty(nome))
                resultado = resultado.Where(c => c.RazaoSocial.ToLower().Contains(nome.ToLower()) || c.Nome.ToLower().Contains(nome.ToLower())).ToList();

            var cpf = Request.QueryString["cpf"];
            if (!String.IsNullOrEmpty(cpf))
                resultado = resultado.Where(c => c.Documento.ToLower().Contains(cpf.ToLower())).ToList();

            int atendimento = 0;
            Int32.TryParse(Request.QueryString["atendimento"], out atendimento);
            if (atendimento > 0)
                resultado = resultado.Where(c => c.LacosAtendimentoID == atendimento).ToList();

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);
            if (status > 0)
                resultado = resultado.Where(c => c.LacosStatusProspecaoID == status).ToList();


            //TODO: Melhorar isso
            var clientes = new List<Cliente>();
            var _result = "<table>";
            _result += "<tr style='height:43px;font-weight:bold;'><th>Contato</th><th>Razao Social</th><th>CNPJ/CPF</th><th>Cidade</th><th>Telefone</th><th>Atendimento</th></tr>";
            foreach (var item in resultado)
            {
                var cliente = clienteRepository.Get(item.ID);
                
                _result += "<tr><td>" + Domain.Core.Funcoes.HtmlEncode(cliente.Nome) + "</td><td>" + Domain.Core.Funcoes.HtmlEncode(cliente.RazaoSocial) + "</td><td>" + cliente.Documento + "</td><td>" + (cliente.CidadeID.HasValue ? cliente.Cidade.NomeCompleto : "") + "</td><td>" + cliente.TelefoneContato + "<br>" + cliente.CelularContato + "</td><td>" + (cliente.LacosAtendimentoID.HasValue ? cliente.Administrador.Nome : "") + "</td>";
                
            }
            _result += "</table> ";
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "iso-8859-1";
            Response.BufferOutput = true;
            Response.AddHeader("content-disposition", "attachment; filename = prospeccao." + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            Response.Write(_result);
            Response.End();

            return View();
        }
    }
}
