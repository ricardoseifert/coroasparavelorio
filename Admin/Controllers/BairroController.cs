﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Artem.Google.Net;
using MvcExtensions.Security.Filters;
using Admin.ViewModels;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Comunicacao|Fornecedores|Qualidade|FloriculturaMisto", "/Home/AccessDenied")]
    public class BairroController : CrudController<Bairro>
    {

        private IPersistentRepository<Bairro> bairroRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private AdminModel adminModel;

        public BairroController(ObjectContext context)
            : base(context)
        {
            bairroRepository = new PersistentRepository<Bairro>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            adminModel = new AdminModel(context);
        }

        public override ActionResult List(params object[] args)
        {
            ViewBag.Admin = adminModel.CurrentAdministrador;
            var resultado = bairroRepository.GetAll().OrderBy(p => p.Nome).ToList();
            if (Request.QueryString["termo"] != null)
            {
                var termo = Request.QueryString["termo"].Replace(".", "").Replace("-", "").Replace("/", "").Trim().ToLower();
                if (termo != null)
                    resultado = resultado.Where(c => c.Nome.Trim().ToLower().Contains(termo.Trim().ToLower()) || c.Estado.Nome.Trim().ToLower().Contains(termo.Trim().ToLower())).OrderBy(c => c.Nome).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [HttpGet]
        public override ActionResult Edit(int? id)
        {
            Bairro bairro = null;
            if (id.HasValue)
            {
                bairro = bairroRepository.Get(id.Value);
            } 
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
            return View(bairro);
        }
        //TODO: Qdo existir o rótulo no redirect, deletar ao inserir ou editar

        [HttpGet]
        public ActionResult Ordem(int id)
        {
            Bairro bairro = bairroRepository.Get(id);
            if (bairro != null)
            {
                return View(bairro);
            }
            return RedirectToAction("List");
        }
         
        [HttpPost]
        [ValidateInput(false)]
        public override ActionResult Edit(Bairro bairro)
        {
            if (bairro.ID == 0 || bairro.Foto == null)
                bairro.Foto = "";
            if (bairroRepository.GetByExpression(c => c.RotuloUrl.Trim().ToLower() == bairro.RotuloUrl.Trim().ToLower() && c.ID != bairro.ID).Count() > 0)
            { 
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                ViewBag.Erro = "Esta URL já está sendo utilizada por outro bairro";
                return View(bairro);
            }
            else
            {
                bairroRepository.Save(bairro);

                var foto = Request.Files["File"];
                if (foto.ContentLength > 0)
                {
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/bairros/" + bairro.ID));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }

                    //try
                    //{

                    var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(foto.FileName);
                    //FOTO
                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/bairros/" + bairro.ID + "/" + nomefoto), new ImageResizer.ResizeSettings(
                                            "width=400&height=400&crop=auto;format=jpg;mode=crop;scale=both"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    bairro.Foto = nomefoto;
                    bairroRepository.Save(bairro);

                    //THUMB
                    i = new ImageResizer.ImageJob(Server.MapPath("/content/bairros/" + bairro.ID + "/" + bairro.Foto), Server.MapPath("/content/bairros/" + bairro.ID + "/thumb/" + bairro.Foto), new ImageResizer.ResizeSettings(
                                            "width=230&height=130&anchor=middlecenter;format=jpg;mode=crop;scale=both"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    //}
                    //catch { }
                }
                 
                var forcarAtualizacao = Request.Form["forcarAtualizacao"] == "on";
                if (!String.IsNullOrEmpty(bairro.Nome))
                {
                    if (bairro.Latitude == null || bairro.Latitude == 0 || bairro.Longitude == null || bairro.Longitude == 0 || forcarAtualizacao)
                    {
                        if (bairro.Estado != null)
                        {
                            var endereco = bairro.Nome + "," + bairro.Estado.Sigla + ",Brasil"; 
                            GeoRequest request = new GeoRequest(endereco);
                            GeoResponse response = request.GetResponse();
                            GeoLocation location = response.Results[0].Geometry.Location;
                            bairro.Latitude = Convert.ToDecimal(location.Latitude);
                            bairro.Longitude = Convert.ToDecimal(location.Longitude);
                            bairroRepository.Save(bairro);
                        }
                    }
                }

                return RedirectToAction("List", new { cod = "SaveSucess" });
            }
        }
    }
}
