﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.Linq.Expressions;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Admin.ViewModels;
using Domain.MetodosExtensao;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers {
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
    public class LogController : CrudController<Log>
    {
        private IPersistentRepository<Log> logRepository;


        public LogController(ObjectContext context)
            : base(context) {
            logRepository = new PersistentRepository<Log>(context);
        }

        public void Index(){

            var Date = DateTime.Now.AddDays(-5);
            var LogResult = logRepository.GetByExpression(r => r.Data > Date).OrderByDescending(d => d.Data);

            foreach (var Log in LogResult)
            {
                Response.Write("<p>");
                Response.Write(Log.Data + " - " + Log.Pedido + " - " + Log.Envio + " - " + Log.Retorno+ " - " + Log.Var1 + " - " + Log.Var2 + "<br />");
                Response.Write("</p>");
                Response.Write("<hr />");
            }
        }
    }
}
