﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos", "/Home/AccessDenied")]
    public class DescontosController : Controller
    {
        private IPersistentRepository<Desconto> descontoRepository;

        public DescontosController(ObjectContext context)
        {
            descontoRepository = new PersistentRepository<Desconto>(context);
        }

        [HttpGet]
        public ActionResult List()
        {
            var desconto = descontoRepository.GetAll().FirstOrDefault();
            if (desconto != null)
            {
                return RedirectToAction("Edit", new { id = desconto.ID });
            }
            else
            {
                return RedirectToAction("Edit");
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Desconto desconto = null;
            if (id.HasValue)
            {
                desconto = descontoRepository.Get(id.Value);
            }
            else
            {
                desconto = new Desconto();
            }
            return View(desconto);
        }

        public ActionResult Edit(Desconto desconto)
        {
            descontoRepository.Save(desconto);
            return RedirectToAction("Edit", new { cod = "SaveSucess" });
        }
    }
}
