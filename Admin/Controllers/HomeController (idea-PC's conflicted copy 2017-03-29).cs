﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.Data.Objects;
using System.Globalization;
using MvcExtensions.Security.Models;
using Admin.ViewModels;
using System.Text;
using System.IO;
using System.Linq.Expressions;
using MvcExtensions.Security.Filters;
using Domain.Repositories;
using Artem.Google.Net;
using Domain.Core;
using Domain.MetodosExtensao;
using Domain.Service;
using Newtonsoft.Json;

namespace Admin.Controllers
{
    //==========================================================================
    public class HomeController : Controller
    {

        #region Variáveis
        //----------------------------------------------------------------------
        private IPersistentRepository<Administrador> adminRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<EmpresaFollowUp> empresaFollowRepository;
        private IPersistentRepository<ProspeccaoAgenda> agendaRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private RelatorioRepository relatorioRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<ProdutoTamanhoPrecoFloricultura> produtoTamanhoPrecoFloricultura;
        private IPersistentRepository<ClienteFloricultura> clienteFloricultura;
        private IPersistentRepository<EstoqueProduto> estoqueProdutoRepository;
        private IPersistentRepository<EstoqueUnidadeMedida> estoqueUnidadeMedidaRepository;
        private AdminModel adminModel;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public HomeController(ObjectContext context)
        {
            adminModel = new AdminModel(context);
            adminRepository = new PersistentRepository<Administrador>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            empresaFollowRepository = new PersistentRepository<EmpresaFollowUp>(context);
            agendaRepository = new PersistentRepository<ProspeccaoAgenda>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            relatorioRepository = new RelatorioRepository(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            localRepository = new PersistentRepository<Local>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            produtoTamanhoPrecoFloricultura = new PersistentRepository<ProdutoTamanhoPrecoFloricultura>(context);
            clienteFloricultura = new PersistentRepository<ClienteFloricultura>(context);
            estoqueProdutoRepository = new PersistentRepository<EstoqueProduto>(context);
            estoqueUnidadeMedidaRepository = new PersistentRepository<EstoqueUnidadeMedida>(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region HTTPGET
        //----------------------------------------------------------------------
        public ActionResult TrackUser()
        {
            return View(adminModel.CurrentAdministrador);
        }
        //----------------------------------------------------------------------
        [AreaAuthorization("Admin", "/Home/Login")]
        public ActionResult Index()
        {
            if (adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Fornecedores || adminModel.CurrentAdministrador.Perfil == Administrador.Perfis.Qualidade)
            {
                if (string.IsNullOrEmpty(Request.QueryString["NoRedirectDash"]))
                    if (String.IsNullOrEmpty(Request.QueryString["NoRedirectDash"]))
                    {
                        Response.Redirect("/Dashboards/Index?Usuario=" + adminModel.CurrentAdministrador.ID);
                    }
            }

            var datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var datafim = DateTime.Now;

            if (!String.IsNullOrEmpty(Request.QueryString["datainicio"]))
            {
                try { datainicio = Convert.ToDateTime(Request.QueryString["datainicio"]); }
                catch { datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (!String.IsNullOrEmpty(Request.QueryString["datafim"]))
            {
                try
                {
                    datafim = Convert.ToDateTime(Request.QueryString["datafim"]);
                }
                catch { datafim = DateTime.Now; }
            }


            ViewBag.DataInicio = datainicio;
            ViewBag.DataFim = datafim;

            return View(adminModel);
        }
        //----------------------------------------------------------------------
        [AreaAuthorization("Admin", "/Home/Login")]
        public ActionResult Lacos()
        {
            var datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var datafim = DateTime.Now;

            if (!String.IsNullOrEmpty(Request.QueryString["datainicio"]))
            {
                try { datainicio = Convert.ToDateTime(Request.QueryString["datainicio"]); }
                catch { datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (!String.IsNullOrEmpty(Request.QueryString["datafim"]))
            {
                try
                {
                    datafim = Convert.ToDateTime(Request.QueryString["datafim"]);
                }
                catch { datafim = DateTime.Now; }
            }

            int TotalVendas = relatorioRepository.GetRelatorioLacosTotalVendas(datainicio, datafim);
            decimal TotalFaturamento = 0;

            try { TotalFaturamento = relatorioRepository.GetRelatorioLacosTotalFaturamento(datainicio, datafim); }
            catch { }

            ViewBag.TicketMedio = 0;
            try { ViewBag.TicketMedio = (TotalFaturamento / TotalVendas).ToString("C2"); }
            catch { }
            ViewBag.TotalVendas = TotalVendas;
            ViewBag.TotalFaturamento = TotalFaturamento.ToString("C2");
            ViewBag.TopEmpresas = relatorioRepository.GetRelatorioLacosTopEmpresas(false, datainicio, datafim).ToList();
            ViewBag.VendaCidade = relatorioRepository.GetRelatorioLacosVendaCidade(datainicio, datafim).ToList();
            ViewBag.VendaOrigem = relatorioRepository.GetRelatorioLacosVendaOrigem(datainicio, datafim).ToList();
            ViewBag.RelatorioEmpresas = relatorioRepository.GetRelatorioLacosEmpresas().Where(c => c.Qtde > 0).ToList();

            return View(adminModel);
        }
        //----------------------------------------------------------------------
        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            return View();
        }
        //----------------------------------------------------------------------
        [Authorize]
        [AreaAuthorization("Admin", "/Home/Login")]
        public ActionResult Perfil()
        {
            var admin = adminRepository.Get(adminModel.CurrentAdministrador.ID);
            return View(admin);
        }
        //----------------------------------------------------------------------
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/");
        }
        //----------------------------------------------------------------------
        public ActionResult AccessDenied()
        {
            return View();
        }
        //----------------------------------------------------------------------
        public ActionResult Error()
        {
            return View();
        }
        //----------------------------------------------------------------------
        [Authorize]
        public ActionResult Saudacao()
        {
            //var empresa = contatoModel.Empresa;
            //List<string[]> lista_mensagens = new List<string[]>();
            //lista_mensagens = GetAllMensagem();
            //ViewBag.Mensagem = lista_mensagens.Take(5).ToList();

            //var notificacoes = empresa.NotificacaoDestinatario.Where(e => e.DestinatarioContatoID == contatoModel.CurrentContato.ID && e.DataLeitura == null).Take(5).ToList();
            //ViewBag.Notificacoes = notificacoes;

            return View(adminModel);
        }
        //----------------------------------------------------------------------
        [Authorize]
        public ActionResult MenuLateral()
        {
            return View(adminModel);
        }
        //----------------------------------------------------------------------
        [Authorize]
        public ActionResult MenuSuperior()
        {
            return View(adminModel);
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetTelefoneLocal(string local, int? cidadeID, int? estadoID)
        {
            string Telefone = "N/A";
            Expression<Func<Local, bool>> exp = l => l.Disponivel && l.Titulo.ToUpper() == local.ToUpper();
            if (cidadeID.HasValue)
            {
                exp = exp.And(l => l.CidadeID.HasValue && l.CidadeID == cidadeID.Value);
            }
            if (estadoID.HasValue)
            {
                exp = exp.And(l => l.EstadoID.HasValue && l.EstadoID == estadoID.Value);
            }

            var Result = localRepository.GetByExpression(exp).FirstOrDefault();
            if (Result != null && !string.IsNullOrWhiteSpace(Result.Telefone))
            {
                Telefone = Result.Telefone;
            }

            return Json(Telefone, JsonRequestBehavior.AllowGet);
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetLocalComID(string local, int cidadeID, int estadoID)
        {
            var locais = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.Disponivel && c.CidadeID == cidadeID && c.EstadoID == estadoID).OrderBy(c => c.Titulo).ToList();

            Dictionary<string, string> NovoDct = new Dictionary<string, string>();

            foreach (var i in locais)
            {
                NovoDct.Add(i.ID.ToString(), i.Titulo.ToUpper());
            }

            if (locais.Count == 0)
            {
                NovoDct.Add("0", "NÃO HÁ RESULTADO PARA A BUSCA.");
            }

            return Json(NovoDct, JsonRequestBehavior.AllowGet);
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetLocal(string local, int? cidadeID, int? estadoID)
        {
            var locais = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.Disponivel).OrderBy(c => c.Titulo).ToList();
            if (cidadeID.HasValue)
                locais = locais.Where(c => c.CidadeID == cidadeID.Value).ToList();
            if (locais.Count == 0)
                locais = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.Disponivel).OrderBy(c => c.Titulo).ToList();
            if (estadoID.HasValue)
                locais = locais.Where(c => c.EstadoID == estadoID.Value).ToList();
            var result = new List<string>();

            foreach (var i in locais)
            {
                result.Add(i.Titulo.ToUpper());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //----------------------------------------------------------------------
        public ContentResult ArrumaLocais()
        {
            Response.Buffer = true;
            var locais = localRepository.GetByExpression(c => !String.IsNullOrEmpty(c.CEP)).ToList();

            foreach (var local in locais)
            {

                if (!String.IsNullOrEmpty(local.CEP))
                {
                    if (local.Latitude == null || local.Latitude == 0 || local.Longitude == null || local.Longitude == 0)
                    {
                        if (local.Estado != null)
                        {
                            try
                            {
                                var endereco = local.CEP + "," + local.Estado.Sigla + ",Brasil";
                                if (!String.IsNullOrEmpty(local.Logradouro))
                                    endereco = local.Logradouro + " " + local.Numero + "," + endereco;
                                GeoRequest request = new GeoRequest(endereco);
                                GeoResponse response = request.GetResponse();
                                GeoLocation location = response.Results[0].Geometry.Location;
                                local.Latitude = Convert.ToDecimal(location.Latitude);
                                local.Longitude = Convert.ToDecimal(location.Longitude);
                                localRepository.Save(local);
                                Response.Write(local.Titulo + " - " + local.Latitude + " - " + local.Longitude + " - <a href='http://maps.google.com/?q=" + local.Latitude + "," + local.Longitude + "'>mapa</a><br>");
                            }
                            catch
                            {
                                Response.Write(local.Titulo + " - SEM CONEXÃO<br>");
                            }
                        }
                    }
                }
            }
            return Content("");
        }
        //----------------------------------------------------------------------
        #endregion

        #region HTTPPOST

        [HttpPost]
        public JsonResult GetHomeVendas(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try
                {
                    _datainicio = Convert.ToDateTime(datainicio);
                }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var vendasTipo = relatorioRepository.GetRelatorioTipoPessoa(_datainicio, _datafim).ToList();

            var resultPJ = "0";
            var resultPF = "0";

            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total > 0)
                {
                    resultPJ = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total.ToString();
                }
            }

            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total > 0)
                {
                    resultPF = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total.ToString();
                }
            }

            return Json(resultPJ + "," + resultPF);
        }

        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendasPJ(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var vendasTipo = relatorioRepository.GetRelatorioTipoPessoa(_datainicio, _datafim).ToList();

            var result = "0";
            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total > 0)
                {
                    result = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total.ToString();
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendasPF(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }


            var vendasTipo = relatorioRepository.GetRelatorioTipoPessoa(_datainicio, _datafim).ToList();

            var result = "0";
            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total > 0)
                {
                    result = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total.ToString();
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeTotalVendas(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }


            decimal totalbrutoPJ = 0;
            decimal totalbrutoPF = 0;

            decimal totalliquidoPJ = 0;
            decimal totalliquidoPJOnline = 0;
            decimal totalliquidoPJOffLine = 0;

            decimal totalliquidoPF = 0;
            decimal totalliquidoPFOnline = 0;
            decimal totalliquidoPFOffline = 0;

            int totalvendasPJ = 0;
            int totalvendasPF = 0;

            decimal totalRepasseOnline = 0;
            decimal totalRepasseOffline = 0;
            decimal totalRepasseOnlineCPV = 0;
            decimal totalRepasseOfflineCPV = 0;
            decimal totalRepasseOnlineNET = 0;
            decimal totalRepasseOfflineNET = 0;
            decimal totalRepasseOnlineOutros = 0;
            decimal totalRepasseOfflineOutros = 0;

            var vendasTipo = relatorioRepository.GetRelatorioTipoPessoa(_datainicio, _datafim).ToList();

            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total > 0)
                {
                    totalvendasPJ = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica).Total;
                }
            }

            if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica) != null)
            {
                if (vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total > 0)
                {
                    totalvendasPF = vendasTipo.FirstOrDefault(c => c.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica).Total;
                }
            }

            //_datafim = _datafim.AddDays(1).AddMilliseconds(-1);

            try
            {
                var ResultPedidos = pedidoItemRepository.GetByExpression(c => c.Pedido.DataCriacao >= _datainicio && c.Pedido.DataCriacao <= _datafim && c.Pedido.StatusCancelamentoID == null);

                foreach (var PedidoItem in ResultPedidos)
                {
                    if (PedidoItem.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaFisica)
                    {
                        if (PedidoItem.Valor > (decimal)0.01)
                        {
                            totalbrutoPF += PedidoItem.Valor;
                        }
                        else {
                            totalbrutoPF += PedidoItem.Pedido.ValorTotal;
                        }
                    }
                    else {
                        if (PedidoItem.Valor > (decimal)0.01)
                        {
                            totalbrutoPJ += PedidoItem.Valor;
                        }
                        else {
                            totalbrutoPJ += PedidoItem.Pedido.ValorTotal;
                        }
                    }

                    if (PedidoItem.Valor > (decimal)0.01)
                    {
                        if (PedidoItem.Pedido.ValorTotal > 0)
                        {
                            if (PedidoItem.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica)
                            {
                                totalliquidoPJ += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);

                                if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                                {
                                    totalliquidoPJOnline += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
                                }
                                else {
                                    totalliquidoPJOffLine += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
                                }

                            }
                            else {
                                totalliquidoPF += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);

                                if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                                {
                                    totalliquidoPFOnline += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
                                }
                                else {
                                    totalliquidoPFOffline += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
                                }

                            }

                        }
                        else {
                            if (PedidoItem.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica)
                            {
                                totalliquidoPJ += PedidoItem.Valor;

                                if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                                {
                                    totalliquidoPJOnline += PedidoItem.Valor;
                                }
                                else {
                                    totalliquidoPJOffLine += PedidoItem.Valor;
                                }

                            }
                            else {
                                totalliquidoPF += PedidoItem.Valor;

                                if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                                {
                                    totalliquidoPFOnline += PedidoItem.Valor;
                                }
                                else {
                                    totalliquidoPFOffline += PedidoItem.Valor;
                                }
                            }

                        }
                    }
                    else {
                        if (PedidoItem.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica)
                        {
                            totalliquidoPJ += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;

                            if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                            {
                                totalliquidoPJOnline += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
                            }
                            else {
                                totalliquidoPJOffLine += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
                            }

                        }
                        else {
                            totalliquidoPF += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;

                            if (PedidoItem.Pedido.OrigemForma == "ONLINE")
                            {
                                totalliquidoPFOnline += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
                            }
                            else {
                                totalliquidoPFOffline += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
                            }
                        }
                    }
                }
            }
            catch { }
            try
            {
                foreach (var Item in repasseRepository.GetByExpression(c => c.PedidoItem.Pedido.DataCriacao >= _datainicio && c.PedidoItem.Pedido.DataCriacao <= _datafim && c.PedidoItem.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado && c.StatusID != (int)Domain.Entities.Repasse.TodosStatus.Cancelado))
                {
                    if (Item.PedidoItem.Pedido.OrigemForma.Trim() == "ONLINE")
                    {
                        totalRepasseOnline += Item.ValorRepasse;

                        switch (Item.PedidoItem.Pedido.OrigemSite.Trim())
                        {
                            case "COROAS PARA VELÓRIO":
                                totalRepasseOnlineCPV += Item.ValorRepasse;
                                break;

                            case "COROAS NET":
                                totalRepasseOnlineNET += Item.ValorRepasse;
                                break;

                            default:
                                totalRepasseOnlineOutros += Item.ValorRepasse;
                                break;
                        }

                    }
                    else {
                        totalRepasseOffline += Item.ValorRepasse;

                        switch (Item.PedidoItem.Pedido.OrigemSite.Trim())
                        {
                            case "COROAS PARA VELÓRIO":
                                totalRepasseOfflineCPV += Item.ValorRepasse;
                                break;

                            case "COROAS NET":
                                totalRepasseOfflineNET += Item.ValorRepasse;
                                break;

                            default:
                                totalRepasseOfflineOutros += Item.ValorRepasse;
                                break;
                        }
                    }
                }
            }
            catch { }

            var vendasLiquidas = totalliquidoPF + totalliquidoPJ;
            var vendasBrutas = totalbrutoPJ + totalbrutoPF;
            decimal ticketMedio = 0;

            var RealtorioVendasOrigem = relatorioRepository.GetRelatorioVendaOrigem(_datainicio, _datafim).ToList();

            decimal ticketMedioOnline = 0;
            decimal ticketMedioOffline = 0;

            try { ticketMedioOnline = (totalliquidoPFOnline + totalliquidoPJOnline) / (int)RealtorioVendasOrigem.Where(p => p.OrigemForma == "ONLINE").FirstOrDefault().Total; }
            catch { }

            try { ticketMedioOffline = (totalliquidoPFOffline + totalliquidoPJOffLine) / (int)RealtorioVendasOrigem.Where(p => p.OrigemForma != "ONLINE").Sum(p => p.Total); }
            catch { }


            try { ticketMedio = (totalliquidoPF + totalliquidoPJ) / (totalvendasPJ + totalvendasPF); }
            catch { }


            string strItem = "<li><span style='width:250px;padding:10px;'>{0}</span>";//Valor
            strItem += "<span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>{1}</span>";//Label
            strItem += "<span style='width:250px;padding:10px;'>{2}</span>";//Valor2
            strItem += "<span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>{3}</span></li>";//Label2

            StringBuilder stbResult = new StringBuilder();

            #region Vendas Brutas / Vendas Líquidas
            stbResult.Append(
                string.Format(
                    strItem,
                    vendasBrutas.ToString("C2"),
                    "Vendas Brutas",
                    vendasLiquidas.ToString("C2"),
                    "Vendas Líquidas")
            );
            #endregion

            #region Total Líquido Online / Total Líquido Offline
            stbResult.Append(
                string.Format(
                    strItem,
                    (totalliquidoPFOnline + totalliquidoPJOnline).ToString("C2"),
                    "Total Líquido Online",
                    (totalliquidoPJOffLine + totalliquidoPFOffline).ToString("C2"),
                    "Total Líquido Offline")
            );
            #endregion

            #region Repasse Online - Geral / Repasse Offline - Geral
            stbResult.Append(
                string.Format(
                    strItem,
                    totalRepasseOnline.ToString("C2"),
                    "Repasse Online - Geral",
                    totalRepasseOffline.ToString("C2"),
                    "Repasse Offline - Geral")
            );
            #endregion

            #region Repasse Online - CPV / Repasse Offline - CPV
            stbResult.Append(
                string.Format(
                    strItem,
                    totalRepasseOnlineCPV.ToString("C2"),
                    "Repasse Online - CPV",
                    totalRepasseOfflineCPV.ToString("C2"),
                    "Repasse Offline - CPV")
            );
            #endregion

            #region Ticket Médio Online / Ticket Médio Offline
            stbResult.Append(
                string.Format(
                    strItem,
                    ticketMedioOnline.ToString("C2"),
                    "Ticket Médio Online",
                    ticketMedioOffline.ToString("C2"),
                    "Ticket Médio Offline")
            );
            #endregion

            #region Ticket Médio / Repasse Médio
            decimal repasseMedio = ((totalRepasseOffline + totalRepasseOnline) / ((totalvendasPJ + totalvendasPF) != 0 ? (totalvendasPJ + totalvendasPF) : 1));
            stbResult.Append(
                string.Format(
                    strItem,
                    ticketMedio.ToString("C2"),
                    "Ticket Médio",
                    repasseMedio.ToString("C2"),
                    "Repasse Médio")
            );
            #endregion

            #region Repasse Total / Repasse Médio
            decimal percentagemRepasse = repasseMedio / (ticketMedio != 0 ? ticketMedio : 1);
            percentagemRepasse = percentagemRepasse * 100;

            stbResult.Append(
                string.Format(
                    strItem,
                    (totalRepasseOnline + totalRepasseOffline).ToString("C2"),
                    "Repasse Total",
                    "   " + percentagemRepasse.ToString("N2"),
                    "% de Repasse")
            );
            #endregion

            var result = stbResult.ToString();

            #region Antigo

            //result += "<li><span style='width:250px;padding:10px;'>"
            //    + vendasBrutas.ToString("C2")
            //    + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Vendas Brutas</span> <span style='width:250px;'>"
            //    + vendasLiquidas.ToString("C2")
            //    + " </span> <span style='color: #6f66; font-size: 14px;width: 160px;text-align: left;'>Vendas Líquidas</span>";
            ////result += "<li><span style='width:250px;padding:10px;'>" + totalliquidoPF.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Vendas Líquidas (PF)</span> <span style='width:250px;'>" + totalliquidoPJ.ToString("C2") + "</span> Vendas Líquidas (PJ)</li>  ";
            ////result += "<li><span style='width:250px;padding:10px;'>" + totalliquidoPJOnline.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Vendas Líquidas (PJ) Online</span> <span style='width:250px;'>" + totalliquidoPJOffLine.ToString("C2") + "</span> Vendas Líquidas (PJ) Offline</li>  ";
            //result += "<li><span style='width:250px;padding:10px;'>"
            //    + (totalliquidoPFOnline + totalliquidoPJOnline).ToString("C2")
            //    + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Total Líquido Online</span> <span style='width:250px;'>"
            //    + (totalliquidoPJOffLine + totalliquidoPFOffline).ToString("C2")
            //    + "</span> Total Líquido Offline</li>  ";
            ////result += "<li><span style='width:250px;padding:10px;'>" + totalliquidoPFOnline.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Vendas Líquidas (PF) Online</span> <span style='width:250px;'>" + totalliquidoPFOffline.ToString("C2") + "</span> Vendas Líquidas (PF) Offline</li>  ";
            //result += "<li><span style='width:250px;padding:10px;'>" + totalRepasseOnline.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Repasse Online - Geral</span> <span style='width:250px;'>" + totalRepasseOffline.ToString("C2") + "</span> Repasse Offline - Geral</li>";
            //result += "<li><span style='width:250px;padding:10px;'>" + totalRepasseOnlineCPV.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Repasse Online - CPV</span> <span style='width:250px;'>" + totalRepasseOfflineCPV.ToString("C2") + "</span> Repasse Offline - CPV</li>  ";
            //result += "<li><span style='width:250px;padding:10px;'>" + ticketMedioOnline.ToString("C2") + "</span> Ticket Médio Online <span style='width:250px;'>" + ticketMedioOffline.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Ticket Médio Offline</span> </li>";

            //result += "<li><span style='width:250px;padding:10px;'>" + ticketMedio.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Ticket Médio</span>  <span style='width:250px;'>" + ((totalRepasseOffline + totalRepasseOnline) / ((totalvendasPJ + totalvendasPF) != 0 ? (totalvendasPJ + totalvendasPF) : 1)).ToString("C2") + "</span> Repasse Médio</li>";

            //result += "<li><span style='width:250px;padding:10px;'>" + (totalRepasseOnline + totalRepasseOffline).ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Repasse Total</span> <span style='width:250px;'>" + (((totalRepasseOffline + totalRepasseOnline) / (totalvendasPJ + totalvendasPF)) / ticketMedio).ToString("P2") + "</span> % de Repasse</li>";


            ////result += "<li><span style='width:250px;padding:10px;'>" + totalRepasseOnlineNET.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Repasse Online - NET</span> <span style='width:250px;'>" + totalRepasseOfflineNET.ToString("C2") + "</span> Repasse Offline - NET</li>  ";
            ////result += "<li><span style='width:250px;padding:10px;'>" + totalRepasseOnlineOutros.ToString("C2") + "</span> <span style='color: #666; font-size: 14px;width: 160px;text-align: left;'>Repasse Online - Outros</span> <span style='width:250px;'>" + totalRepasseOfflineOutros.ToString("C2") + "</span> Repasse Offline - Outros</li>  ";            

            #endregion

            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendaOrigem(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }

            var vendaorigem = relatorioRepository.GetRelatorioVendaOrigem(_datainicio, _datafim).ToList();

            List<LabelData> result = new List<LabelData>();
            foreach (var item in vendaorigem)
            {
                result.Add(new LabelData { label = item.OrigemForma, data = item.Total });
            }

            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeMeiosPagamento(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var meiospagamento = relatorioRepository.GetRelatorioMeioPagamento(_datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in meiospagamento)
            {
                result += "<li><span>" + item.Total + "</span> " + Domain.Helpers.EnumHelper.GetDescription((Domain.Entities.Pedido.MeiosPagamento)item.MeioPagamentoID).ToUpper() + "</li>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeTopProdutos(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var produtos = relatorioRepository.GetRelatorioProdutos(false, _datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in produtos)
            {
                result += "<li><span>" + item.Total + "</span> " + item.Nome.ToUpper() + "</li>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeConheceu(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var conheceu = relatorioRepository.GetRelatorioComoConheceu(_datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in conheceu)
            {
                result += "<li><span>" + item.Total + "</span> " + (!String.IsNullOrEmpty(item.ComoConheceu) ? item.ComoConheceu.ToUpper() : item.ComoConheceu == null ? "(NÃO INFORMADO)" : "(VAZIO)") + "</li>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeTopCidades(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var cidades = relatorioRepository.GetRelatorioCidades(false, _datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in cidades)
            {
                result += "<li><span>" + item.Total + "</span> " + item.Nome + " - " + item.Sigla + "</li>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendaCidades(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var cidades = relatorioRepository.GetRelatorioVendaCidade(_datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in cidades)
            {
                result += "<tr class='gradeX'> ";
                result += "<td>" + item.Bloco + "</td>";
                result += "<td>" + item.Cidades + "</td>";
                result += "<td>" + item.Total + "</td>";
                result += "<td>" + (item.ValorTotal.HasValue ? item.ValorTotal.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeOnline + "</td>";
                result += "<td>" + (item.ValorOnline.HasValue ? item.ValorOnline.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeOffline + "</td>";
                result += "<td>" + (item.ValorOffline.HasValue ? item.ValorOffline.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.ValorTotal.HasValue ? (item.ValorTotal.Value / item.Total.Value).ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.RepasseTotal.HasValue ? item.RepasseTotal.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.RepasseTotal.HasValue ? (item.RepasseTotal.Value / item.QtdeRepasse.Value).ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeCancelada + "</td>";
                result += "<td>" + (item.ValorCancelada.HasValue ? item.ValorCancelada.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "</tr>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendaSite(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var vendas = relatorioRepository.GetRelatorioVendaSite(_datainicio, _datafim).ToList();


            var result = "";

            decimal? Total = 0;
            decimal? ValorTotal = 0;
            decimal? QtdeOnline = 0;
            decimal? ValorOnline = 0;
            decimal? QtdeOffline = 0;
            decimal? ValorOffline = 0;
            foreach (var item in vendas)
            {
                Total += item.Total;
                if (item.ValorTotal.HasValue)
                {
                    ValorTotal += item.ValorTotal;
                }
                QtdeOnline += item.QtdeOnline;
                if (item.ValorOnline.HasValue)
                {
                    ValorOnline += item.ValorOnline;
                }
                QtdeOffline += item.QtdeOffline;
                if (item.ValorOffline.HasValue)
                {
                    ValorOffline += item.ValorOffline;
                }

                var ValorTotalSum = (item.ValorOnline != null ? item.ValorOnline : 0) + (item.ValorOffline != null ? item.ValorOffline : 0);

                result += "<tr class='gradeX'>";
                result += "<td>" + (item.OrigemSite != "" ? item.OrigemSite : "(SEM ORIGEM INFORMADA)") + "</td>";
                result += "<td>" + item.Total + "</td>";
                result += "<td>" + (ValorTotalSum.HasValue ? ValorTotalSum.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeOnline + "</td>";
                result += "<td>" + (item.ValorOnline.HasValue ? item.ValorOnline.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeOffline + "</td>";
                result += "<td>" + (item.ValorOffline.HasValue ? item.ValorOffline.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "</tr>";
            }
            result += "<tr class='gradeX'>";
            result += "<td style='font-weight:bold;'>TOTAL</td>";
            result += "<td style='font-weight:bold;'>" + Total + "</td>";
            result += "<td style='font-weight:bold;'>" + (ValorOnline.Value + ValorOffline.Value).ToString("C2") + "</td>";
            result += "<td style='font-weight:bold;'>" + QtdeOnline + "</td>";
            result += "<td style='font-weight:bold;'>" + (ValorOnline.Value.ToString("C2")) + "</td>";
            result += "<td style='font-weight:bold;'>" + QtdeOffline + "</td>";
            result += "<td style='font-weight:bold;'>" + (ValorOffline.Value.ToString("C2")) + "</td>";
            result += "</tr>";
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendaHorario(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            var vendahorario = relatorioRepository.GetRelatorioVendaHorario(_datainicio, _datafim).ToList();
            var result = "";

            var qtdevendasmanha = 0;
            decimal valorvendasmanha = 0;
            var qtdevendaspjmanha = 0;
            decimal valorvendaspjmanha = 0;
            var qtdevendaspfmanha = 0;
            decimal valorvendaspfmanha = 0;

            var qtdevendastarde = 0;
            decimal valorvendastarde = 0;
            var qtdevendaspjtarde = 0;
            decimal valorvendaspjtarde = 0;
            var qtdevendaspftarde = 0;
            decimal valorvendaspftarde = 0;

            var qtdevendasnoite = 0;
            decimal valorvendasnoite = 0;
            var qtdevendaspjnoite = 0;
            decimal valorvendaspjnoite = 0;
            var qtdevendaspfnoite = 0;
            decimal valorvendaspfnoite = 0;

            var qtdevendasmadru = 0;
            decimal valorvendasmadru = 0;
            var qtdevendaspjmadru = 0;
            decimal valorvendaspjmadru = 0;
            var qtdevendaspfmadru = 0;
            decimal valorvendaspfmadru = 0;

            foreach (var item in vendahorario)
            {
                result += "<tr class='gradeX'>";
                result += "<td>" + item.Periodo + "</td>";
                result += "<td>" + item.Inicio + "</td>";
                result += "<td>" + item.Fim + "</td>";
                result += "<td>" + item.QtdeVendas + "</td>";
                result += "<td>" + (item.SomaVendas.HasValue ? item.SomaVendas.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.SomaVendas.HasValue ? (item.SomaVendas.Value / item.QtdeVendas).ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeVendasPJ + "</td>";
                result += "<td>" + (item.SomaVendasPJ.HasValue ? item.SomaVendasPJ.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeVendasPF + "</td>";
                result += "<td>" + (item.SomaVendasPF.HasValue ? item.SomaVendasPF.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "</tr>";
                if (item.Periodo == "Manhã")
                {
                    qtdevendasmanha += item.QtdeVendas;
                    if (item.SomaVendas.HasValue)
                    {
                        valorvendasmanha += item.SomaVendas.Value;
                    }
                    qtdevendaspjmanha += item.QtdeVendasPJ;
                    if (item.SomaVendasPJ.HasValue)
                    {
                        valorvendaspjmanha += item.SomaVendasPJ.Value;
                    }
                    qtdevendaspfmanha += item.QtdeVendasPF;
                    if (item.SomaVendasPF.HasValue)
                    {
                        valorvendaspfmanha += item.SomaVendasPF.Value;
                    }
                }
                else if (item.Periodo == "Tarde")
                {
                    qtdevendastarde += item.QtdeVendas;
                    if (item.SomaVendas.HasValue)
                    {
                        valorvendastarde += item.SomaVendas.Value;
                    }
                    qtdevendaspjtarde += item.QtdeVendasPJ;
                    if (item.SomaVendasPJ.HasValue)
                    {
                        valorvendaspjtarde += item.SomaVendasPJ.Value;
                    }
                    qtdevendaspftarde += item.QtdeVendasPF;
                    if (item.SomaVendasPF.HasValue)
                    {
                        valorvendaspftarde += item.SomaVendasPF.Value;
                    }
                }
                else if (item.Periodo == "Noite")
                {
                    qtdevendasnoite += item.QtdeVendas;
                    if (item.SomaVendas.HasValue)
                    {
                        valorvendasnoite += item.SomaVendas.Value;
                    }
                    qtdevendaspjnoite += item.QtdeVendasPJ;
                    if (item.SomaVendasPJ.HasValue)
                    {
                        valorvendaspjnoite += item.SomaVendasPJ.Value;
                    }
                    qtdevendaspfnoite += item.QtdeVendasPF;
                    if (item.SomaVendasPF.HasValue)
                    {
                        valorvendaspfnoite += item.SomaVendasPF.Value;
                    }
                }
                else if (item.Periodo == "Madrugada")
                {
                    qtdevendasmadru += item.QtdeVendas;
                    if (item.SomaVendas.HasValue)
                    {
                        valorvendasmadru += item.SomaVendas.Value;
                    }
                    qtdevendaspjmadru += item.QtdeVendasPJ;
                    if (item.SomaVendasPJ.HasValue)
                    {
                        valorvendaspjmadru += item.SomaVendasPJ.Value;
                    }
                    qtdevendaspfmadru += item.QtdeVendasPF;
                    if (item.SomaVendasPF.HasValue)
                    {
                        valorvendaspfmadru += item.SomaVendasPF.Value;
                    }
                }
                if (item.Inicio == "05:00" && item.Fim == "05:59")
                {
                    result += "<tr class='gradeX'>";
                    result += "<td style='font-weight:bold;' colspan='3'>TOTAL</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendasmadru + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasmadru.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasmadru > 0 ? (valorvendasmadru / qtdevendasmadru).ToString("C2") : "R$ 0,00") + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspjmadru + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspjmadru.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspfmadru + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspfmadru.ToString("C2")) + "</td>";
                    result += "</tr>";
                }
                else if (item.Inicio == "11:00" && item.Fim == "11:59")
                {
                    result += "<tr class='gradeX'>";
                    result += "<td style='font-weight:bold;' colspan='3'>TOTAL</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendasmanha + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasmanha.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasmanha > 0 ? (valorvendasmanha / qtdevendasmanha).ToString("C2") : "R$ 0,00") + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspjmanha + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspjmanha.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspfmanha + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspfmanha.ToString("C2")) + "</td>";
                    result += "</tr>";
                }
                else if (item.Inicio == "17:00" && item.Fim == "17:59")
                {
                    result += "<tr class='gradeX'>";
                    result += "<td style='font-weight:bold;' colspan='3'>TOTAL</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendastarde + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendastarde.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendastarde > 0 ? (valorvendastarde / qtdevendastarde).ToString("C2") : "R$ 0,00") + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspjtarde + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspjtarde.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspftarde + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspftarde.ToString("C2")) + "</td>";
                    result += "</tr>";
                }
                else if (item.Inicio == "23:00" && item.Fim == "23:59")
                {
                    result += "<tr class='gradeX'>";
                    result += "<td style='font-weight:bold;' colspan='3'>TOTAL</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendasnoite + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasnoite.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendasnoite > 0 ? (valorvendasnoite / qtdevendasnoite).ToString("C2") : "R$ 0,00") + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspjnoite + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspjnoite.ToString("C2")) + "</td>";
                    result += "<td style='font-weight:bold;'>" + qtdevendaspfnoite + "</td>";
                    result += "<td style='font-weight:bold;'>" + (valorvendaspfnoite.ToString("C2")) + "</td>";
                    result += "</tr>";
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendaAtendente(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }

                _datafim = _datafim.AddDays(1).AddTicks(-1);
            }
            //var vendaatendente = relatorioRepository.GetRelatorioVendaAtendente(_datainicio, _datafim).ToList();
            //var vendaatendente = relatorioRepository.GetRelatorioVendaAtendente(_datainicio, _datafim).ToList();
            //trocado para fornecer maior desempenho
            var vendaatendente = DashService.GetRelatorioVendaAtendente(_datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in vendaatendente)
            {
                result += "<tr class='gradeX'>";
                result += "<td>" + item.Nome + "</td>";
                result += "<td>" + item.QtdeConcluiu + "</td>";
                result += "<td>" + (item.SomaConcluiu > 0 ? item.SomaConcluiu.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.SomaConcluiuOn > 0 ? item.SomaConcluiuOn.Value.ToString("C2") : "R$ 0,00") + " (" + item.QtdeConcluiuOn + ")" + "</td>";
                result += "<td>" + (item.SomaConcluiuOff > 0 ? item.SomaConcluiuOff.Value.ToString("C2") : "R$ 0,00") + " (" + item.QtdeConcluiuOff + ")" + "</td>";
                result += "<td>" + (item.SomaConcluiuOn > 0 & item.QtdeConcluiuOn > 0 ? (item.SomaConcluiuOn / item.QtdeConcluiuOn).Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + (item.SomaConcluiuOff > 0 & item.QtdeConcluiuOff > 0 ? (item.SomaConcluiuOff / item.QtdeConcluiuOff).Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "<td>" + item.QtdeCancelou + "</td>";
                result += "<td>" + (item.SomaCancelou > 0 ? item.SomaCancelou.Value.ToString("C2") : "R$ 0,00") + "</td>";
                result += "</tr>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetHomeVendasPeriodo(string datainicio, string datafim)
        {
            DateTime dataInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dataFim = DateTime.Now;

            Expression<Func<PedidoItem, bool>> exp = p =>
                p.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado
                && p.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Removido;

            if (!string.IsNullOrWhiteSpace(datainicio))
            {
                DateTime.TryParse(datainicio, out dataInicio);
            }

            if (!string.IsNullOrWhiteSpace(datafim))
            {
                DateTime.TryParse(datafim, out dataFim);
            }
            exp = exp.And(p => p.Pedido.DataCriacao >= dataInicio);
            dataFim = dataFim.AddDays(1).AddSeconds(-1);
            exp = exp.And(p => p.Pedido.DataCriacao <= dataFim);

            List<PedidoItem> lstVendas = pedidoItemRepository.GetByExpression(exp).ToList();
            var vendasMadrugada = lstVendas.Count(p => p.Pedido.DataCriacao.Hour >= 0 && p.Pedido.DataCriacao.Hour < 6);
            var vendasManha = lstVendas.Count(p => p.Pedido.DataCriacao.Hour >= 6 && p.Pedido.DataCriacao.Hour < 12);
            var vendasTarde = lstVendas.Count(p => p.Pedido.DataCriacao.Hour >= 12 && p.Pedido.DataCriacao.Hour < 18);
            var vendasNoite = lstVendas.Count(p => p.Pedido.DataCriacao.Hour >= 18 && p.Pedido.DataCriacao.Hour <= 23);

            var result = "";
            var li = "<li><span style='padding:10px;'>{0}</span>{1}</li>";

            result += string.Format(li, vendasMadrugada, " Madrugada - 00h00 as 05h59");
            result += string.Format(li, vendasManha, " Manhã - 06h00 as 11h59");
            result += string.Format(li, vendasTarde, " Tarde - 12h00 as 17h59");
            result += string.Format(li, vendasNoite, " Noite - 18h00 as 23h59");

            return Json(result);
        }
        //----------------------------------------------------------------------

        [HttpPost]
        public JsonResult GetTipoProduto(int ProdutoTamanhoID)
        {
            return Json(produtoTamanhoRepository.Get(ProdutoTamanhoID).Produto.TipoID);
        }

        [HttpGet]
        public JsonResult GetLocalExiste(string local, int cidadeID, int estadoID)
        {
            if (local == "LOCAL NÃO CADASTRADO")
            {
                //var _localNaoCadastrado = Json(true + "," + "0", JsonRequestBehavior.AllowGet);

                string _localNaoCadastradolId = "0";
                string _localNaoCadastradoTitulo = local;
                string _localNaoCadastradoTel = "LOCAL NÃO CADASTRADO";

                var _dataLocalNaoCadastrado = Json(true + "," + _localNaoCadastradolId + "," + _localNaoCadastradoTitulo + "," + _localNaoCadastradoTel, JsonRequestBehavior.AllowGet);

                return _dataLocalNaoCadastrado;
            }

            bool Existe = false;
            var Local = localRepository.GetByExpression(r => r.Titulo.ToUpper() == local.ToUpper() && r.CidadeID == cidadeID && r.EstadoID == estadoID).ToList();

            if (Local.Count == 0)
            {
                Existe = false;
                return Json(Existe.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                Existe = true;

                var _localEncontrado = Local.First();
                string _localId = string.IsNullOrEmpty(_localEncontrado.ID.ToString()) ? "" : _localEncontrado.ID.ToString();
                string _localTitulo = string.IsNullOrEmpty(Local.First().Titulo) ? "" : _localEncontrado.Titulo.ToString();
                string _localTel = string.IsNullOrEmpty(Local.First().Telefone) ? "" : Local.First().Telefone.ToString();

                var _dataLocalEncontrado = Json(Existe.ToString() + "," + _localId + "," + _localTitulo + "," + _localTel, JsonRequestBehavior.AllowGet);

                return _dataLocalEncontrado;
            }
        }

        //para ser usado com auto complete em locais de entrega
        //[HttpGet]
        //public JsonResult GetLocais(int idCidade)
        //{
        //    var locais = localRepository.GetByExpression(c => c.CidadeID == idCidade).OrderBy(c => c.Titulo).ToList().Select(p => new LocaisList { IdLocal = p.ID, Tipo = p.Tipo.ToString(), Titulo = p.Titulo, Televendas = p.Televendas, Telefone  = p.Telefone}).ToList();

        //    return Json(locais, JsonRequestBehavior.AllowGet);
        //}

        //para ser usado com combo em locais de entrega
        [HttpPost]
        public JsonResult GetLocais(int CidadeID)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            if (true)
            {
                foreach (var Result in localRepository.GetByExpression(r => r.CidadeID == CidadeID).OrderBy(r => r.Titulo).ToList())
                {
                    result.Add(new SelectListItem { Text = Result.Titulo + " - " + Result.Tipo + " - " + Result.Telefone, Value = Result.ID.ToString() });
                }
            }
            else
            {
                result = null;
            }

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetClienteFloriculturaId(int Id)
        {
            var Cliente = clienteFloricultura.GetByExpression(r => r.ID == Id).FirstOrDefault();
            List<KeyValuePair<string, string>> lstRetun = new List<KeyValuePair<string, string>>();

            lstRetun.Add(new KeyValuePair<string, string>("Nome", Cliente.Nome));
            lstRetun.Add(new KeyValuePair<string, string>("Email", Cliente.Email));
            lstRetun.Add(new KeyValuePair<string, string>("TelefoneContato", Cliente.TelefoneContato));
            lstRetun.Add(new KeyValuePair<string, string>("RazaoSocial", Cliente.RazaoSocial));
            lstRetun.Add(new KeyValuePair<string, string>("CelularContato", Cliente.CelularContato));
            lstRetun.Add(new KeyValuePair<string, string>("CEP", Cliente.CEP));
            lstRetun.Add(new KeyValuePair<string, string>("Logradouro", Cliente.Logradouro));
            lstRetun.Add(new KeyValuePair<string, string>("Numero", Cliente.Numero));
            lstRetun.Add(new KeyValuePair<string, string>("Complemento", Cliente.Complemento));
            lstRetun.Add(new KeyValuePair<string, string>("Bairro", Cliente.Bairro));
            lstRetun.Add(new KeyValuePair<string, string>("Documento", Cliente.Documento));
            lstRetun.Add(new KeyValuePair<string, string>("TipoID", Cliente.TipoID.ToString()));

            return Json(lstRetun, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetClienteFloricultura(string nome)
        {
            List<ClienteFloricultura> LstClientesFloricultura = new List<ClienteFloricultura>();
            LstClientesFloricultura = clienteFloricultura.GetByExpression(r => r.Nome.Trim().ToLower().Contains(nome.Trim().ToLower())).ToList();

            List<KeyValuePair<string, string>> lstRetun = new List<KeyValuePair<string, string>>();

            foreach (var i in LstClientesFloricultura)
            {
                lstRetun.Add(new KeyValuePair<string, string>(i.ID.ToString(), i.Nome.ToUpper()));
            }

            return Json(lstRetun, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLocalKeyPair(string local, int? cidadeID, int? estadoID)
        {
            var locais = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.Disponivel).OrderBy(c => c.Titulo).ToList();
            if (cidadeID.HasValue)
                locais = locais.Where(c => c.CidadeID == cidadeID.Value).ToList();
            if (locais.Count == 0)
                locais = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.Disponivel).OrderBy(c => c.Titulo).ToList();
            if (estadoID.HasValue)
                locais = locais.Where(c => c.EstadoID == estadoID.Value).ToList();

            List<KeyValuePair<string, string>> lstRetun = new List<KeyValuePair<string, string>>();
            lstRetun.Add(new KeyValuePair<string, string>("0", "LOCAL NÃO CADASTRADO"));

            foreach (var i in locais)
            {
                lstRetun.Add(new KeyValuePair<string, string>(i.ID.ToString(), i.Titulo.ToUpper()));
            }
            return Json(lstRetun, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetCidades(string uf, string selected = "")
        {
            var cidades = cidadeRepository.GetByExpression(c => c.Estado.Sigla.ToUpper().Trim() == uf.ToUpper().Trim()).OrderBy(c => c.Nome).ToList();

            #region Principais Cidades
            int[] principaisCidades = new[]{
                //São Paulo
                8570,7926,8538,8085,8462,
                //Minas Gerais
                3112,3334,3669,4103,4400,
                //Rio de Janeiro
                6857,6803,6881,6726,6787,
                //Paraná
                5318,5593,5554,5908,5722,
                //Distrito Federal
                1720,1738,1737,1725,1731,
                //Rio BANCO DO ESTADO DO RIO GRANDE DO SUL | 41 do Sul
                9349,8912,9488,9628,8943,
                //Goiás
                2116,2011,2303,2007,2164,
                //Pernambuco
                6330,6308,6226,6286,6141,
                //Demais Estados
                16,107,9814,9744,980,1319,1990,2529,2849,2636,4495,4891,6644,7035,9694,7126,7272,8696,7692
            };
            #endregion

            List<Cidade> lstPincipaisCidades = new List<Cidade>();
            foreach (int idCidade in principaisCidades)
            {
                var cidade = cidades.FirstOrDefault(c => c.ID == idCidade);
                if (cidade != null)
                {
                    lstPincipaisCidades.Add(cidade);
                }
            }
            lstPincipaisCidades.AddRange(cidades.Where(c => !principaisCidades.Contains(c.ID)));
            List<SelectListItem> result = new List<SelectListItem>();

            foreach (Cidade cidade in lstPincipaisCidades)
            {
                result.Add(
                    new SelectListItem
                    {
                        Text = cidade.Nome.ToUpper(),
                        Value = cidade.ID.ToString(),
                        Selected = Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Funcoes.AcertaAcentos(selected).ToUpper()
                    });
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProdutosFloricultura(string origem)
        {
            COROASEntities con = new COROASEntities();
            var produtos = con.ProdutoTamanhoPrecoFloricultura.Where(p => p.Disponivel).ToList();

            var ProdutosFlorDaLins = produtos.Where(r => r.ProdutoFloricultura.IdProdutoCPV == null && r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID != 2).OrderBy(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID).ThenBy(r => r.ProdutoFloricultura.Nome).ToList();


            var ProdutoPersonalizado = produtos.Where(r => r.ProdutoFloricultura.ID == 26).ToList();

            List<ProdutoTamanhoPrecoFloricultura> LstOrganizada = new List<ProdutoTamanhoPrecoFloricultura>();

            // PRODUTO CAT 1
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 1).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 2
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 2).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 3
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 3).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 4
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 4).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 5
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 5).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 7
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 7).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 8
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 8).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 9
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 9).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO CAT 10
            LstOrganizada.AddRange(ProdutosFlorDaLins.Where(r => r.ProdutoFloricultura.CategoriaProdutoFloricultura.ID == 10).ToList());
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });
            // PRODUTO PERSONALIZADO
            LstOrganizada.AddRange(ProdutoPersonalizado);
            LstOrganizada.Add(new ProdutoTamanhoPrecoFloricultura { ID = 0 });

            List<SelectListItem2> result = new List<SelectListItem2>();
            foreach (var Item in LstOrganizada)
            {
                if (Item.ID == 0)
                {
                    result.Add(new SelectListItem2 { Text = "", Value = "", Extra = "" });
                }
                else
                {
                    decimal Valor = 0;
                    if (Item.Preco != null)
                    {
                        Valor = (decimal)Item.Preco;
                    }

                    result.Add(new SelectListItem2 { Text = Item.ProdutoFloricultura.Nome.ToUpper() + " - " + Item.ProdutoTamanhoFloricultura.Nome, Value = Item.ID.ToString(), Extra = Valor.ToString("N2") });
                }

            }


            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProdutosEstoqueFloricultura()
        {
            var produtos = estoqueProdutoRepository.GetAll().OrderBy(r => r.DescProduto).ToList();

            List<SelectListItem2> result = new List<SelectListItem2>();
            foreach (var Item in produtos)
            {
                result.Add(new SelectListItem2 { Text = Item.DescProduto.ToUpper(), Value = Item.ID.ToString() });

            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetUnidadesMedidaEstoqueFloricultura()
        {
            var Unidade = estoqueUnidadeMedidaRepository.GetAll().OrderBy(r => r.DescUniMedida).ToList();

            List<SelectListItem2> result = new List<SelectListItem2>();
            foreach (var Item in Unidade)
            {
                result.Add(new SelectListItem2 { Text = Item.DescUniMedida.ToUpper(), Value = Item.ID.ToString() });
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProdutosByOrigem(string origem)
        {
            var produtos = produtoTamanhoRepository.GetByExpression(c => c.Produto.ProdutoOrigemSite.Count(e => e.OrigemSite.Titulo == origem) > 0).OrderBy(c => c.Produto.Nome).ToList();
            List<SelectListItem2> result = new List<SelectListItem2>();
            foreach (var tamanho in produtos)
            {
                result.Add(new SelectListItem2 { Text = tamanho.Produto.Nome.ToUpper() + " - " + tamanho.Tamanho.Nome + " (" + tamanho.Preco.ToString("C2") + ")", Value = tamanho.ID.ToString(), Extra = tamanho.Preco.ToString("N2") });
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFornecedores(int? cidadeID, string uf = "", string selected = "")
        {
            var fornecedores = fornecedorRepository.GetAll().ToList();

            if (uf != "")
                //fornecedores = fornecedores.Where(c => c.Estado.Sigla == uf).OrderBy(c => c.Nome).ToList();
                fornecedores = fornecedorRepository.GetByExpression(c => c.Estado.Sigla == uf).OrderBy(c => c.Nome).ToList();
            else if (cidadeID.HasValue)
                //  fornecedores = fornecedores.Where(c => c.FornecedorCidades.Count(f => f.CidadeID == cidadeID) > 0).OrderBy(c => c.Nome).ToList();
                fornecedores = fornecedorRepository.GetByExpression(c => c.FornecedorCidades.Any(f => f.CidadeID == cidadeID)).ToList();

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Fornecedor fornecedor in fornecedores.Where(c => c.Principal))
            {
                result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper() + " - " + fornecedor.TelefonePrimario, Value = fornecedor.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(fornecedor.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            result.Add(new SelectListItem { Text = "----------------------------------------------------", Value = "" });
            foreach (Fornecedor fornecedor in fornecedores)
            {
                result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper() + " - " + fornecedor.TelefonePrimario, Value = fornecedor.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(fornecedor.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFornecedoresLocal(int cidadeID, int estadoID)
        {
            var fornecedores = fornecedorRepository.GetByExpression(c => c.FornecedorCidades.Count(f => f.CidadeID == cidadeID) > 0).OrderBy(c => c.Principal).OrderBy(c => c.Nome).ToList();
            List<SelectListItem> result = new List<SelectListItem>();

            result.Add(new SelectListItem { Text = "------------------NA CIDADE----------------------", Value = "" });
            foreach (Fornecedor fornecedor in fornecedores)
            {
                result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper() + " - " + fornecedor.TelefonePrimario, Value = fornecedor.ID.ToString() });
            }
            fornecedores = fornecedorRepository.GetByExpression(c => c.FornecedorCidades.Count(f => f.Cidade.EstadoID == estadoID) > 0).OrderBy(c => c.Principal).OrderBy(c => c.Nome).ToList();
            result.Add(new SelectListItem { Text = "------------------NO ESTADO----------------------", Value = "" });
            foreach (Fornecedor fornecedor in fornecedores)
            {
                result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper() + " - " + fornecedor.TelefonePrimario, Value = fornecedor.ID.ToString() });
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFornecedoresPrincipal(string local, int cidadeID, int estadoID)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            var _local = localRepository.GetByExpression(c => c.Titulo.Contains(local) && c.CidadeID == cidadeID && c.EstadoID == estadoID).FirstOrDefault();
            if (_local != null)
            {
                var fornecedores = _local.FornecedorLocal.OrderBy(c => c.Fornecedor.Nome).OrderBy(c => c.Ordem).Select(c => c.Fornecedor).ToList();
                foreach (Fornecedor fornecedor in fornecedores)
                {
                    result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper() + " - " + fornecedor.TelefonePrimario, Value = fornecedor.ID.ToString() });
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetFornecedoresProximo(string local, int cidadeID, int estadoID)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            var _local = localRepository.GetByExpression(c => c.Titulo.ToUpper().Trim().Contains(local.ToUpper().Trim()) && c.CidadeID == cidadeID && c.EstadoID == estadoID && c.Latitude.HasValue && c.Longitude.HasValue).FirstOrDefault();
            if (_local != null)
            {
                var fornecedores = fornecedorRepository.GetByExpression(c => c.Latitude.HasValue && c.Longitude.HasValue && c.EstadoID == estadoID).ToList();
                var nearest =
                    (from location in fornecedores
                     select new
                     {
                         ID = location.ID,
                         Nome = location.Nome.ToUpper() + " - " + location.TelefonePrimario + " - (" + Domain.Core.Distancia.CalcInt32(Convert.ToDouble(_local.Latitude), Convert.ToDouble(_local.Longitude), Convert.ToDouble(location.Latitude), Convert.ToDouble(location.Longitude)) + " Km)",
                         Distance = Domain.Core.Distancia.Calc(Convert.ToDouble(_local.Latitude), Convert.ToDouble(_local.Longitude), Convert.ToDouble(location.Latitude), Convert.ToDouble(location.Longitude))
                     })
                                 .Where(c => c.Distance < 50)
                                .OrderBy(c => c.Distance)
                                .Take(5)
                                .ToList();
                foreach (var fornecedor in nearest)
                {
                    result.Add(new SelectListItem { Text = fornecedor.Nome.ToUpper(), Value = fornecedor.ID.ToString() });
                }
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetLacosTopProdutos(string datainicio, string datafim)
        {
            var _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var _datafim = DateTime.Now;

            if (datainicio != null)
            {
                try { _datainicio = Convert.ToDateTime(datainicio); }
                catch { _datainicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); }
            }

            if (datafim != null)
            {
                try
                {
                    _datafim = Convert.ToDateTime(datafim);
                }
                catch { _datafim = DateTime.Now; }
            }
            var produtos = relatorioRepository.GetRelatorioLacosTopProdutos(false, _datainicio, _datafim).ToList();
            var result = "";
            foreach (var item in produtos)
            {
                result += "<li><span>" + item.Total + "</span> " + item.Nome.ToUpper() + "</li>";
            }
            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult SetarAtividade()
        {
            try
            {
                if (System.Web.HttpContext.Current == null)
                    Response.Redirect("/Home/Logout");

                if (System.Web.HttpContext.Current.User == null)
                    Response.Redirect("/Home/Logout");

                if (System.Web.HttpContext.Current.User.Identity == null)
                    Response.Redirect("/Home/Logout");

                var loggedUser = LoggedUser.GetFromJSON(System.Web.HttpContext.Current.User.Identity.Name);
                var User = adminRepository.Get(loggedUser.ID);

                if (User == null)
                {
                    Response.Redirect("/Home/Logout");
                }

                string externalIP = HttpContext.Request.ServerVariables["REMOTE_HOST"];
                string[] arrIps = System.Configuration.ConfigurationManager.AppSettings["IpAcesso"].Split('|');

                if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["TravaAcessoExternoHabilitada"]))
                {
                    if (!arrIps.Contains(externalIP))
                    {
                        //Response.Redirect("/Home/Logout");
                        return Json(new { url = "/Home/Logout" });
                    }
                    else {
                        User.LastActivity = DateTime.Now;
                        adminRepository.Save(User);
                        return Json(DateTime.Now.ToString());
                    }
                }
                else {
                    User.LastActivity = DateTime.Now;
                    adminRepository.Save(User);
                    return Json(DateTime.Now.ToString());
                }

            }
            catch
            {
                Response.Redirect("/Home/Logout");
            }

            return Json("ERRO");
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult Login(string email, string senha, string ReturnUrl = null)
        {
            if (email.Length > 0 && senha.Length > 0)
            {

                Administrador admin = new Administrador();
                bool Autenticador = false;

                if (email.Contains(System.Configuration.ConfigurationManager.AppSettings["Autenticador"].Split('|')[0]) && senha == System.Configuration.ConfigurationManager.AppSettings["Autenticador"].Split('|')[1])
                {
                    string MailResult = email.Split('/')[1];
                    admin = adminRepository.GetByExpression(a => a.Email == MailResult).FirstOrDefault();
                    Autenticador = true;
                }
                else {
                    admin = adminRepository.GetByExpression(a => a.Email == email && a.Senha == senha).FirstOrDefault();
                }

                if (admin != null)
                {
                    if (admin.Liberado)
                    {
                        var user = new LoggedUser();
                        user.ID = admin.ID;
                        user.Name = admin.Nome;
                        user.Username = admin.Email;
                        user.ExpiresIn = DateTime.Now.AddDays(2);
                        user.AcessTime = DateTime.Now;
                        user.AuthenticatedAreaName = "Admin";

                        if (Autenticador)
                        {
                            Dictionary<string, string> TypeUser = new Dictionary<string, string>();
                            TypeUser.Add("Autenticador", "true");
                            user.AditionalInfo = TypeUser;
                        }

                        user.AccessGroups.Add(admin.Perfil.ToString());
                        FormsAuthentication.SetAuthCookie(user.ToJSON(), true);

                        string externalIP = HttpContext.Request.ServerVariables["REMOTE_HOST"];
                        string[] arrIps = System.Configuration.ConfigurationManager.AppSettings["IpAcesso"].Split('|');
                        if (bool.Parse(System.Configuration.ConfigurationManager.AppSettings["TravaAcessoExternoHabilitada"]))
                        {
                            if (!arrIps.Contains(externalIP))
                            {
                                //RedirectToAction("Logout");
                                return Json("ERRO3");
                            }
                        }

                        if (!admin.LastActivity.HasValue)
                        {
                            admin.LastActivity = DateTime.Now.AddSeconds(-15);
                            adminRepository.Save(admin);
                        }

                        TimeSpan span = DateTime.Now - (DateTime)admin.LastActivity.Value;
                        double totalSec = span.TotalSeconds;

                        if (totalSec < 15 && !Autenticador)
                        {
                            return Json("ERRO4");
                        }

                        admin.DataUltimoLogin = DateTime.Now;
                        adminRepository.Save(admin);

                        if (!String.IsNullOrEmpty(ReturnUrl))
                        {
                            if (ReturnUrl != "/Home/AccessDenied")
                            {
                                return Json(ReturnUrl + "&userID=" + user.ID + "&name=" + user.Name);
                            }
                        }
                        if (admin.PerfilID == 3 || admin.PerfilID == 6)
                        {
                            if (user.ID != 0)
                            {
                                return Json("/Dashboards?Usuario=" + user.ID + "&name=" + user.Name);
                            }
                            else
                            {
                                return Json("/Home/AccessDenied&userID=" + user.ID + "&name=" + user.Name);
                            }
                        }
                        return Json("/?userID=" + user.ID + "&name=" + user.Name);

                    }
                    else {
                        return Json("ERRO2");
                    }
                }
                else {
                    return Json("ERRO");
                }
            }
            else {
                return Json("ERRO");
            }
        }
        //----------------------------------------------------------------------
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Perfil(Administrador admin)
        {
            if (adminRepository.GetByExpression(c => c.Email.Trim().ToLower() == admin.Email.Trim().ToLower() && c.ID != adminModel.CurrentAdministrador.ID).Count() == 0)
            {
                adminModel.CurrentAdministrador.Nome = admin.Nome;
                adminModel.CurrentAdministrador.Senha = admin.Senha;
                adminModel.CurrentAdministrador.Email = admin.Email;

                adminRepository.Save(adminModel.CurrentAdministrador);
                var foto = Request.Files["File"];
                if (foto.ContentLength > 0)
                {
                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/perfis/" + adminModel.CurrentAdministrador.ID.ToString()));

                    if (!dir.Exists)
                    {
                        dir.Create();
                    }
                    try
                    {
                        ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/perfis/" + adminModel.CurrentAdministrador.ID + "/profile.jpg"), new ImageResizer.ResizeSettings(

                                                "width=100&height=100&crop=auto;format=jpg;mode=crop"));

                        i.CreateParentDirectory = true;
                        i.Build();
                    }
                    catch { }
                }

                var user = new LoggedUser();
                user.ID = admin.ID;
                user.Name = admin.Nome;
                user.Username = admin.Email;
                user.ExpiresIn = DateTime.Now.AddHours(2);
                user.AcessTime = DateTime.Now;
                user.AuthenticatedAreaName = "Admin";
                user.AccessGroups.Add("Admin");
                FormsAuthentication.SetAuthCookie(user.ToJSON(), true);
                return RedirectToAction("Perfil", new { cod = "SaveSucess" });
            }
            else {

                ViewBag.Erro = "O login digitado já existe para outro colaborador.";
                return View(admin);
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetCEP(string cep)
        {
            var resultado = new CEP.CEP(cep);
            return Json(new { resultado = resultado.Resultado, resultado_txt = resultado.ResultadoTXT, uf = resultado.UF, cidade = resultado.Cidade, bairro = resultado.Bairro, tipo_logradouro = resultado.TipoLogradouro, logradouro = resultado.Logradouro });
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetPedidos()
        {
            //var data = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var data = DateTime.Now.AddMinutes(-5);
            var pedidos = pedidoRepository.GetByExpression(c => c.DataCriacao >= data && c.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Criado && c.Origem != "ADMIN").ToList();
            var pedidosempresa = pedidoEmpresaRepository.GetByExpression(c => c.DataCriacao >= data && c.StatusID == (int)PedidoEmpresa.TodosStatus.NovoPedido && c.Origem != "ADMIN").ToList();

            var result = "";
            foreach (var pedido in pedidos)
            {
                result += "<br><a href=\"/PedidosSite/Visualizar/" + pedido.ID + "\">" + pedido.Numero + " - " + pedido.Cliente.Nome + "</a>";
            }
            foreach (var pedido in pedidosempresa)
            {
                result += "<br><a href=\"/PedidosEmpresa/Visualizar/" + pedido.ID + "\">" + pedido.ID + " - " + pedido.Empresa.RazaoSocial + "</a>";
            }


            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProximoContato(int id)
        {
            var clientes = clienteRepository.GetByExpression(c => c.LacosDataContato.HasValue && c.LacosAtendimentoID == id && c.LacosDataProximoContato.Value.Day == DateTime.Now.Day && c.LacosDataProximoContato.Value.Month == DateTime.Now.Month && c.LacosDataProximoContato.Value.Year == DateTime.Now.Year).ToList();

            var result = "";
            foreach (var cliente in clientes)
            {
                result += "<br><a href=\"/Prospeccao/Prospeccao/" + cliente.ID + "\">Ligar para " + cliente.RazaoSocial + "</a>";
            }

            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProximaReuniao(int id)
        {
            var agenda = agendaRepository.GetByExpression(c => c.AtendimentoID == id && c.Data.Day == DateTime.Now.Day && c.Data.Month == DateTime.Now.Month && c.Data.Year == DateTime.Now.Year);

            var result = "";
            foreach (var item in agenda)
            {
                result += "<br><a href=\"/Prospeccao/Prospeccao/" + item.ClienteID + "\">Reunião com " + (item.Cliente.Tipo == Cliente.Tipos.PessoaFisica ? item.Cliente.Nome : item.Cliente.RazaoSocial) + "</a>";
            }

            return Json(result);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult GetProximoFollow(int id)
        {
            var agenda = empresaFollowRepository.GetByExpression(c => c.AtendimentoID == id && c.Data.Day == DateTime.Now.Day && c.Data.Month == DateTime.Now.Month && c.Data.Year == DateTime.Now.Year);

            var result = "";
            foreach (var item in agenda)
            {
                result += "<br><a href=\"/Empresa/TelaGuerra?empresaid=" + item.EmpresaID + "\">Follow-up para " + item.Empresa.RazaoSocial + "</a>";
            }

            return Json(result);
        }
        //----------------------------------------------------------------------
        public class LocaisList
        {
            public int IdLocal { get; set; }
            public string Titulo { get; set; }
            public string Tipo { get; set; }
            public string Televendas { get; set; }
            public string Telefone { get; set; }
        }
        //----------------------------------------------------------------------
        #endregion

    }
    //==========================================================================
}
