﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcExtensions.Controllers;
using Domain.Entities;
using System.Data.Objects;
using System.IO;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;
using System.Text;
using Domain.Repositories;
using Domain.Core;
using Admin.ViewModels;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|Financeiro", "/Home/AccessDenied")]
    public class PagamentoGenericoController : CrudController<Lote>
    {
        #region Variaveis
        private IPersistentRepository<PagamentoGenerico> pagamentoGenericoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<PagamentoGenerico> pagamentogenericoRepository;
        private IPersistentRepository<ItemLote> itemloteRepository;
        private IPersistentRepository<Fornecedor> fornecedorRepository;
        private IPersistentRepository<Repasse_X_ItemLote> repasse_X_ItemLoteRepository;
        private IPersistentRepository<FornecedorGenerico> fornecedorGenericoRepository;
        private IPersistentRepository<StatusPagamentoFornecedorGenerico> statusPagamentoFornecedorGenericoRepository;

        private IPersistentRepository<PagamentoGenerico_X_ItemLote> pagamentoGenerico_X_ItemLoteRepository;

        private AdminModel adminModel;
        #endregion

        public PagamentoGenericoController(ObjectContext context)
            : base(context) {
            adminModel = new AdminModel(context);
            pagamentoGenericoRepository = new PersistentRepository<PagamentoGenerico>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            pagamentogenericoRepository = new PersistentRepository<PagamentoGenerico>(context);
            fornecedorRepository = new PersistentRepository<Fornecedor>(context);
            itemloteRepository = new PersistentRepository<ItemLote>(context);
            repasse_X_ItemLoteRepository = new PersistentRepository<Repasse_X_ItemLote>(context);
            fornecedorGenericoRepository = new PersistentRepository<FornecedorGenerico>(context);
            pagamentoGenerico_X_ItemLoteRepository = new PersistentRepository<PagamentoGenerico_X_ItemLote>(context);
            statusPagamentoFornecedorGenericoRepository = new PersistentRepository<StatusPagamentoFornecedorGenerico>(context);
        }
        public ActionResult Novo()
        {
            var Fornecedores = fornecedorGenericoRepository.GetAll().OrderBy(r => r.NomeFornecedor).ToList();
            ViewBag.FornecedorGenerico = new SelectList(Fornecedores, "ID", "NomeFornecedor");
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Novo(FormCollection form)
        {
            var pagamentogenerico = new PagamentoGenerico {
                Valor = decimal.Parse(form["Valor"]),
                DataCriacao = DateTime.Now,
                DataDePagamento = DateTime.Parse(form["DataDePagamento"]),
                FornecedorGenerico = fornecedorGenericoRepository.Get(int.Parse(form["fornecedorgenerico"])),
                StatusPagamentoFornecedorGenerico = statusPagamentoFornecedorGenericoRepository.GetByExpression(r => r.ID == (int)PagamentoGenerico.TodosStatusPagamento.AguardandoPagamento).First()
            };
            pagamentoGenericoRepository.Save(pagamentogenerico);

            return RedirectToAction("List", new { cod = "SaveSucess", msg = "O Pagamento Genérico " + pagamentogenerico.ID + " foi criado com sucesso!", datainicial = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year, datafinal = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year });
        }
        public ActionResult ListRepasses(params object[] args)
        {
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var datainicialPedido = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialPedido"], out datainicialPedido);

            var datafinalPedido = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalPedido"], out datafinalPedido);

            List<IRepasse> lstIRepasse = new List<IRepasse>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);
            datafinalPedido = datafinalPedido.AddDays(1).AddSeconds(-1);

            var LstRepasses = repasseRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal && (p.StatusID != (int)Repasse.TodosStatus.Cancelado && p.StatusID != (int)Repasse.TodosStatus.Pago)).ToList();

            if (!string.IsNullOrEmpty(Request.QueryString["datainicialPedido"]) && !string.IsNullOrEmpty(Request.QueryString["datafinalPedido"]))
            {
                LstRepasses = LstRepasses.Where(r => (r.PedidoCPV != null && r.PedidoCPV.DataCriacao >= datainicialPedido && r.PedidoCPV.DataCriacao <= datafinalPedido) || r.PedidoEmpresa != null && r.PedidoEmpresa.DataCriacao >= datainicialPedido && r.PedidoEmpresa.DataCriacao <= datafinalPedido).ToList();
            }

            var LstPagamentosGenericos = pagamentogenericoRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal && (p.StatusPagamentoFornecedorGenerico.ID != (int)PagamentoGenerico.TodosStatusPagamento.Cancelado && p.StatusPagamentoFornecedorGenerico.ID != (int)PagamentoGenerico.TodosStatusPagamento.Pago)).ToList();

            ViewBag.LstRepasses = LstRepasses;
            ViewBag.LstPagamentosGenericos = LstPagamentosGenericos;

            lstIRepasse.AddRange(LstRepasses);
            lstIRepasse.AddRange(LstPagamentosGenericos);

            lstIRepasse = lstIRepasse.OrderBy(r => r.NomeFornecedor).ToList();

            var fornecedor = Request.QueryString["nome"];
            var OrigemFornecedor = Request.QueryString["OrigemFornecedor"];
            int fluxo = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["fluxo"]))
            {
                fluxo = int.Parse(Request.QueryString["fluxo"]);
            }

            if (!String.IsNullOrEmpty(fornecedor))
                lstIRepasse = lstIRepasse.Where(r => r.NomeFornecedor.ToLower().Contains(fornecedor.ToLower())).ToList();

            if (!String.IsNullOrEmpty(OrigemFornecedor))
            {
                if(OrigemFornecedor == Domain.Entities.OrigemFornecedor.Floricultura.ToString())
                {
                    lstIRepasse = lstIRepasse.Where(r => r.TipoRepasse == Domain.Entities.OrigemFornecedor.Floricultura).ToList();
                }
                else
                {
                    lstIRepasse = lstIRepasse.Where(r => r.TipoRepasse == Domain.Entities.OrigemFornecedor.Generico).ToList();
                }
            }

            if (fluxo > 0)
                lstIRepasse = lstIRepasse.Where(r => r.IDFluxoDePagamento == fluxo).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstIRepasse.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lstIRepasse = lstIRepasse.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.lstIRepasse = lstIRepasse;

            return View();
        }

        //[HttpPost]
        //public JsonResult GetLotes()
        //{
        //    List<SelectListItem> result = new List<SelectListItem>();
        //    foreach (var Result in loteRepository.GetAll().ToList())
        //    {
        //        result.Add(new SelectListItem { Text = Result.Descricao, Value = Result.ID.ToString() });
        //    }

        //    return Json(result);
        //}

        //public JsonResult AdicionarAoLote(string IdPagamentos, int IdLote, string Origem)
        //{
        //    var Lote = loteRepository.Get(IdLote);
        //    ItemLote novoItemLote = new ItemLote();
        //    var PrimeiroIDRepasse = int.Parse(IdPagamentos.Split(',')[0]);

        //    if (Origem == Domain.Entities.OrigemFornecedor.Floricultura.ToString())
        //    {
        //        var Fornecedor = repasseRepository.GetByExpression(r => r.ID == PrimeiroIDRepasse).FirstOrDefault().Fornecedor;

        //        #region ADICIONAR ITEM LOTE

        //        // VERIFICAR SE JÁ NÃO HÁ O MESMO FORNECEDOR ADICIONADO PARA ESSE ITEM DE LOTE
        //        if (!itemloteRepository.GetByExpression(r => r.Fornecedor.ID == Fornecedor.ID && r.Lote.ID == Lote.ID).Any())
        //        {
        //            novoItemLote.Lote = Lote;
        //            novoItemLote.Fornecedor = Fornecedor;
        //            itemloteRepository.Save(novoItemLote);
        //        }
        //        else
        //        {
        //            novoItemLote = itemloteRepository.GetByExpression(r => r.Fornecedor.ID == Fornecedor.ID && r.Lote.ID == Lote.ID).First();
        //        }

        //        #endregion

        //        #region ADICIONAR REPASSE X ITEM LOTE

        //        foreach (var Ids in IdPagamentos.Split(','))
        //        {
        //            var IDRepasse = int.Parse(Ids);
        //            Repasse Repasse = repasseRepository.GetByExpression(r => r.ID == IDRepasse).FirstOrDefault();

        //            Repasse_X_ItemLote novoRepasseItemLote = new Repasse_X_ItemLote();
        //            novoRepasseItemLote.Repasse = Repasse;
        //            novoRepasseItemLote.ItemLote = novoItemLote;

        //            repasse_X_ItemLoteRepository.Save(novoRepasseItemLote);
        //        }

        //        #endregion
        //    }
        //    else
        //    {
        //        var FornecedorGenerico = pagamentogenericoRepository.GetByExpression(r => r.ID == PrimeiroIDRepasse).FirstOrDefault().FornecedorGenerico;

        //        #region ADICIONAR ITEM LOTE

        //        // VERIFICAR SE JÁ NÃO HÁ O MESMO FORNECEDOR ADICIONADO PARA ESSE ITEM DE LOTE
        //        if (!itemloteRepository.GetByExpression(r => r.Fornecedor.ID == FornecedorGenerico.ID && r.Lote.ID == Lote.ID).Any())
        //        {
        //            novoItemLote.Lote = Lote;
        //            novoItemLote.FornecedorGenerico = FornecedorGenerico;
        //            itemloteRepository.Save(novoItemLote);
        //        }
        //        else
        //        {
        //            novoItemLote = itemloteRepository.GetByExpression(r => r.Fornecedor.ID == FornecedorGenerico.ID && r.Lote.ID == Lote.ID).First();
        //        }

        //        #endregion

        //        #region ADICIONAR PAGAMENTO GENERICO X ITEM LOTE

        //        foreach (var Ids in IdPagamentos.Split(','))
        //        {
        //            var IDRepasse = int.Parse(Ids);
        //            PagamentoGenerico PagamentoGenerico = pagamentogenericoRepository.GetByExpression(r => r.ID == IDRepasse).FirstOrDefault();

        //            PagamentoGenerico_X_ItemLote novoPagamentoGenerico_X_ItemLote = new PagamentoGenerico_X_ItemLote();
        //            novoPagamentoGenerico_X_ItemLote.PagamentoGenerico = PagamentoGenerico;
        //            novoPagamentoGenerico_X_ItemLote.ItemLote = novoItemLote;

        //            pagamentoGenerico_X_ItemLoteRepository.Save(novoPagamentoGenerico_X_ItemLote);
        //        }

        //        #endregion
        //    }



        //    return Json("OK");
        //}

        public ActionResult Visualizar(int id)
        {
            var Pagamento = pagamentoGenericoRepository.Get(id);

            ViewBag.StatusPagamento = statusPagamentoFornecedorGenericoRepository.GetAll();

            return View(Pagamento);
        }

        //public JsonResult RemoverPagamentoRepasse(int ID, string Tipo, int LoteID)
        //{
        //    var Lote = loteRepository.Get(LoteID);

        //    if (Tipo == "PagamentoGenerico")
        //    {
        //        var ItemLote = pagamentoGenerico_X_ItemLoteRepository.GetByExpression(r => r.PagamentoGenerico.ID == ID && r.ItemLote.Lote.ID == LoteID).First();
        //        pagamentoGenerico_X_ItemLoteRepository.Delete(ItemLote);
        //    }
        //    else
        //    {
        //        var ItemLote = repasse_X_ItemLoteRepository.GetByExpression(r => r.Repasse.ID == ID && r.ItemLote.Lote.ID == LoteID).First();
        //        repasse_X_ItemLoteRepository.Delete(ItemLote);
        //    }

        //    return Json("OK");
        //}

        public ActionResult Editar(FormCollection form)
        {
            var id = Convert.ToInt32(form["ID"]);
            var pagamento = pagamentoGenericoRepository.Get(id);

            if (form["Valor"] != "")
            {
                pagamento.Valor = decimal.Parse(form["Valor"]);
            }
            if (form["StatusPagamentoFornecedorGenerico.ID"] != "")
            {
                var StatusPagamento = statusPagamentoFornecedorGenericoRepository.Get(int.Parse(form["StatusPagamentoFornecedorGenerico.ID"]));
                pagamento.StatusPagamentoFornecedorGenerico = StatusPagamento;
            }

            if(pagamento.Status == PagamentoGenerico.TodosStatusPagamento.Cancelado)
            {
                pagamentoGenerico_X_ItemLoteRepository.Delete(pagamentoGenerico_X_ItemLoteRepository.GetByExpression(r => r.PagamentoGenerico.ID == pagamento.ID).First());
            }

            pagamentoGenericoRepository.Save(pagamento);

            return RedirectToAction("Visualizar", new { id = pagamento.ID, cod = "SaveSucess" });
        }

        public override ActionResult List(params object[] args)
        {
            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<PagamentoGenerico> lstPagamentoGenerico = new List<PagamentoGenerico>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            lstPagamentoGenerico = pagamentoGenericoRepository.GetByExpression(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal).OrderByDescending(p => p.DataCriacao).ToList();

            var nome = Request.QueryString["nome"];
            var statuspagamento = Request.QueryString["statuspagamento"];

            int id = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["numero"]))
            {
                id = int.Parse(Request.QueryString["numero"]);
            }

            if (id > 0)
                lstPagamentoGenerico = lstPagamentoGenerico.Where(c => c.ID == id).ToList();
            else
            {
                if (!String.IsNullOrEmpty(nome))
                    lstPagamentoGenerico = lstPagamentoGenerico.Where(c => c.FornecedorGenerico.NomeFornecedor.ToLower().Contains(nome.ToLower())).ToList();

                if (!String.IsNullOrEmpty(statuspagamento))
                    lstPagamentoGenerico = lstPagamentoGenerico.Where(c => c.StatusPagamentoFornecedorGenerico.ID == int.Parse(statuspagamento)).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = lstPagamentoGenerico.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                lstPagamentoGenerico = lstPagamentoGenerico.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(lstPagamentoGenerico);
        }
    }
}