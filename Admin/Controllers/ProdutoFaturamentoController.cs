﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.IO;
using System.Collections.Specialized;
using MvcExtensions.Security.Filters;

namespace Admin.Controllers
{
    //=================================================================================
    [AreaAuthorization("Admin", "/Home/Login")]
    [AllowGroup("Socios|TI|Coordenadores|LacosCorporativos|Financeiro", "/Home/AccessDenied")]
    public class ProdutoFaturamentoController : Controller
    {
        #region Variáveis
        //-----------------------------------------------------------------------------
        private IPersistentRepository<ProdutoFaturamento> produtoFaturamentoRepository;
        private IPersistentRepository<NCM> ncmRepository;
        private IPersistentRepository<ProdutoFaturamentoCategoria> produtoFaturamentoCategoriaRepository;
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public ProdutoFaturamentoController(ObjectContext context)
        {
            produtoFaturamentoRepository = new PersistentRepository<ProdutoFaturamento>(context);
            ncmRepository = new PersistentRepository<NCM>(context);
            produtoFaturamentoCategoriaRepository = new PersistentRepository<ProdutoFaturamentoCategoria>(context);
        }
        //-----------------------------------------------------------------------------
        #endregion
        [HttpGet]
        public ActionResult List(int? tipo)
        {
            List<ProdutoFaturamento> produtos = null;
            string _titulo = null;

            produtos = produtoFaturamentoRepository.GetAll().OrderBy(p => p.NomeProdutoFaturamento).ToList();

            if (!String.IsNullOrEmpty(Request.QueryString["titulo"]))
            {
                _titulo = Request.QueryString["titulo"];
                if (!String.IsNullOrEmpty(_titulo))
                    produtos = produtos.Where(c => c.NomeProdutoFaturamento.ToLower().Contains(_titulo.ToLower()) || c.Codigo.ToLower().Contains(_titulo.ToLower())).ToList();
            }

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = produtos.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN);
            if (totalPaginas > 1)
            {
                produtos = produtos.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).Take(Domain.Core.Configuracoes.ITENS_PAGINA_ADMIN).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";
            
            return View(produtos);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ProdutoFaturamento produto = null;
            if (id.HasValue)
            {
                produto = produtoFaturamentoRepository.Get(id.Value);
            }
            else
            {
                ViewBag.Origens = new List<OrigemSite>();
                ViewBag.TodasOrigens = new List<OrigemSite>();
            }
            ViewBag.NCM = ncmRepository.GetAll().OrderBy(f => f.CodigoNCM);
            ViewBag.Categoria = produtoFaturamentoCategoriaRepository.GetAll().OrderBy(f => f.NomeCategoria);
            return View(produto);
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            ProdutoFaturamento produto = null;
            if (id > 0)
            {
                produto = produtoFaturamentoRepository.Get(id);
                produtoFaturamentoRepository.Delete(produto);
                //Deletar fotos
                return RedirectToAction("List", new { cod = "DeleteSucess" });
            }
            else
            {
                return RedirectToAction("List", new { cod = "DeleteFailure" });
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ProdutoFaturamento produto)
        {
            string NCMCod = produto.IDNCM.ToString();
            if (ncmRepository.GetByExpression(r => r.CodigoNCM == NCMCod).Count() > 0)
            {
                var NCM = ncmRepository.GetByExpression(r => r.CodigoNCM == NCMCod).First();
                produto.NCM = NCM;
            }
            else
            {
                NCM NovoNcm = new NCM();
                NovoNcm.CodigoNCM = produto.IDNCM.ToString();
                ncmRepository.Save(NovoNcm);
                produto.NCM = NovoNcm;
            }

            produtoFaturamentoRepository.Save(produto);

            return RedirectToAction("List", new { cod = "SaveSucess" });
        }
    }
    //=================================================================================
}
