﻿using System;

namespace Admin.Helpers{
    //===============================================================
    [AttributeUsage(AttributeTargets.Class)]
    public class ReportAttribute : Attribute{

        #region Propriedades

        //-----------------------------------------------------------
        public bool Paginacao { get; set; }
        //-----------------------------------------------------------
        public int QuantidadePorPagina { get; set; }
        //-----------------------------------------------------------

        #endregion

        #region Construtor

        //-----------------------------------------------------------

        //-----------------------------------------------------------

        #endregion

        #region Metodos

        //-----------------------------------------------------------

        //-----------------------------------------------------------

        #endregion

    }

    //===============================================================
    [AttributeUsage(AttributeTargets.Property)]
    public class ReportColumnAttribute : Attribute{

        #region Propriedades

        //-----------------------------------------------------------
        public ReportHelper.TipoColuna TipoColuna { get; set; }
        //-----------------------------------------------------------
        public bool Ordenar { get; set; }
        //-----------------------------------------------------------
        public ReportHelper.TipoOrdenacao TipoOrdenacao { get; set; }
        //-----------------------------------------------------------
        public int Index { get; set; }
        //-----------------------------------------------------------
        public string Nome { get; set; }
        //-----------------------------------------------------------
        public int CasasDecimais { get; set; }
        //-----------------------------------------------------------

        #endregion

    }

    //===============================================================
    [AttributeUsage(AttributeTargets.Property)]
    public class ReportLinkAttribute : Attribute{

        #region Propriedades
        //-----------------------------------------------------------
        public string PropriedadeUrl { get; set; }
        //-----------------------------------------------------------
        public bool NovaGuia { get; set; }
        //-----------------------------------------------------------
        public string PropriedadeTituloAlt { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}
