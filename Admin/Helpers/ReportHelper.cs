﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Admin.Interfaces;
using Admin.ViewModel;

namespace Admin.Helpers {
    //===============================================================
    public static class ReportHelper {

        #region Metodos
        //-----------------------------------------------------------
        public static List<ReportGridColuna> GetColunas(IReportable report){
            List<ReportGridColuna> lstColunas = new List<ReportGridColuna>();
            if (report != null){
                Type tipo = report.GetType();
                List<PropertyInfo> lstProps = tipo.GetProperties().ToList();
                List<Tuple<PropertyInfo, ReportColumnAttribute,bool>> lstAuxColunas = new List<Tuple<PropertyInfo, ReportColumnAttribute,bool>>();
                foreach (PropertyInfo property in lstProps){
                    List<ReportColumnAttribute> lstReportColumnAttributes = property.GetCustomAttributes(false).Where(a=>a.GetType() == typeof(ReportColumnAttribute)).Cast<ReportColumnAttribute>().ToList();
                    List<ReportLinkAttribute> lstReportLinkAttributes = property.GetCustomAttributes(false).Where(a=>a.GetType() == typeof(ReportLinkAttribute)).Cast<ReportLinkAttribute>().ToList();
                    bool bolColunaComLink = lstReportLinkAttributes.Any();
                    if (lstReportColumnAttributes.Any()) {
                        lstAuxColunas.Add(new Tuple<PropertyInfo, ReportColumnAttribute,bool>(property, lstReportColumnAttributes.FirstOrDefault(), bolColunaComLink));
                    }

                }
                lstAuxColunas = lstAuxColunas.OrderBy(t => t.Item2.Index).ToList();
                lstColunas.AddRange(lstAuxColunas.Select(t=> new ReportGridColuna {Titulo = t.Item2.Nome + "|" + t.Item1.Name, PossuiLink = t.Item3}));
                
            }
            return lstColunas;
        }
        //-----------------------------------------------------------
        public static string GetValor(IReportable report, string coluna){
            string valor = string.Empty;
            if (report != null) {
                PropertyInfo prop = report.GetType().GetProperty(coluna);

                if (prop != null){
                    Type tipoProp = prop.PropertyType;
                    bool bolNullable = tipoProp.IsGenericType && tipoProp.GetGenericTypeDefinition() == typeof(Nullable<>);
                    object objValor = prop.GetValue(report, null);
                    List<ReportColumnAttribute> atributos = prop.GetCustomAttributes(false).Where(a => a.GetType() == typeof(ReportColumnAttribute)).Cast<ReportColumnAttribute>().ToList();
                    if (atributos.Any()){
                        var atributo = atributos.FirstOrDefault();
                        switch (atributo.TipoColuna){
                            case TipoColuna.Texto:
                                valor = (string) objValor;
                                break;
                            case TipoColuna.Booleano:
                                valor = Convert.ToBoolean(objValor) ? "Sim" : "Não";
                                break;
                            case TipoColuna.Data:
                                valor = Convert.ToDateTime(objValor).ToString("dd/MM/yyyy");
                                break;
                            case TipoColuna.DataFull:
                                valor = Convert.ToDateTime(objValor).ToString("dd/MM/yyyy hh:mm");
                                break;
                            case TipoColuna.Numero:
                                valor = Convert.ToInt64(objValor).ToString();
                                break;
                            case TipoColuna.Decimal:
                                valor = Convert.ToInt64(objValor).ToString("N"+atributo.CasasDecimais);
                                break;
                            case TipoColuna.Moeda:
                                valor = Convert.ToInt64(objValor).ToString("C" + atributo.CasasDecimais);
                                break;
                            case TipoColuna.Porcentagem:
                                valor = Convert.ToInt64(objValor).ToString("N" + atributo.CasasDecimais) + "%";
                                break;
                            default:
                                valor = Convert.ToString(objValor);
                                break;

                        }
                    }
                    else{
                        valor = Convert.ToString(objValor);
                    }
                }
               
            }
            return valor;
        }
        //-----------------------------------------------------------
        public static ReportLinkColuna GetLink(IReportable report, string coluna){
            ReportLinkColuna link = new ReportLinkColuna();
            link.Url = "#";
            if (report != null){
                PropertyInfo prop = report.GetType().GetProperty(coluna);
                if (prop != null){
                    Type tipoProp = prop.PropertyType;
                    
                    List<ReportLinkAttribute> lstLinks = prop.GetCustomAttributes(false).Where(a => a.GetType() == typeof(ReportLinkAttribute)).Cast<ReportLinkAttribute>().ToList();
                    if (lstLinks.Any()){
                        ReportLinkAttribute attribute = lstLinks.FirstOrDefault();
                        if (attribute != null && !string.IsNullOrWhiteSpace(attribute.PropriedadeUrl)){
                            link = new ReportLinkColuna();
                            link.Url = GetValor(report, attribute.PropriedadeUrl);
                            link.NovaGuia = attribute.NovaGuia;
                            if (!string.IsNullOrWhiteSpace(attribute.PropriedadeTituloAlt)){
                                link.TituloAlt = GetValor(report, attribute.PropriedadeTituloAlt);
                            }
                        }
                    }
                }
            }

            return link;
        }
        //-----------------------------------------------------------
        #endregion

        #region Enums
        //-----------------------------------------------------------
        public enum TipoColuna{
            Texto,
            Booleano,
            Data,
            DataFull,
            Numero,
            Decimal,
            Moeda,
            Porcentagem
        }
        //-----------------------------------------------------------
        public enum TipoOrdenacao{
            Ascendente,
            Descendente
        }
        //-----------------------------------------------------------
        #endregion
        
    }
    //===============================================================
}