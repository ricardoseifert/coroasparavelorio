﻿using System;
using Admin.Helpers;
using Admin.Interfaces;

namespace Admin.ViewModel {
    //=================================================================================
    public class EmpresasInativasViewModel : IReportable {

        #region Propriedades
        //-----------------------------------------------------------------------------
        [ReportColumn(Index = 0, Nome = "Grupo Econômico", TipoColuna = ReportHelper.TipoColuna.Texto)]
        [ReportLink(PropriedadeUrl = nameof(UrlGrupo), NovaGuia = true)]
        public string GrupoEconomico { get; set; }
        //-----------------------------------------------------------------------------
        [ReportColumn(Index = 1, Nome = "Data do Último Pedido", TipoColuna = ReportHelper.TipoColuna.Data)]
        [ReportLink(PropriedadeUrl = nameof(UrlUltimoPedido), NovaGuia = true)]
        public DateTime DataUltimoPedido { get; set; }
        //-----------------------------------------------------------------------------
        public long IdUltimoPedido { get; set; }
        //-----------------------------------------------------------------------------
        [ReportColumn(Index = 2, Nome = "Faturamento nos últimos 12 meses", TipoColuna = ReportHelper.TipoColuna.Moeda)]
        public decimal Faturamento { get; set; }
        //-----------------------------------------------------------------------------
        public string UrlGrupo { get; set; }
        //-----------------------------------------------------------------------------
        public string UrlUltimoPedido { get; set; }
        //-----------------------------------------------------------------------------
        #endregion

    }
    //=================================================================================
}