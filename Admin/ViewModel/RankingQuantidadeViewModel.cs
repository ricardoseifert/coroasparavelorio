﻿using Admin.Helpers;
using Admin.Interfaces;

namespace Admin.ViewModel {
    //===============================================================
    public class RankingQuantidadeViewModel: IReportable {

        #region Propriedades
        //-----------------------------------------------------------
        [ReportColumn(Index = 0, Nome = "Nome", TipoColuna = ReportHelper.TipoColuna.Texto)]
        [ReportLink(PropriedadeUrl = nameof(Url), NovaGuia = true)]
        public string Nome { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 1, Nome = "Quantidade", TipoColuna = ReportHelper.TipoColuna.Numero)]
        public int Quantidade { get; set; }
        //-----------------------------------------------------------
        public string Url { get; set; }
        //-----------------------------------------------------------
        public string TituloAltUrl { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}