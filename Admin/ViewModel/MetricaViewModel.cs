﻿using System.ComponentModel.DataAnnotations;

namespace Admin.ViewModel {
    //===============================================================
    public class MetricaViewModel {

        #region Propriedades
        //-----------------------------------------------------------
        public long Id { get; set; }
        //-----------------------------------------------------------
        public int IdMeta { get; set; }
        //-----------------------------------------------------------
        public string Nome { get; set; }
        //-----------------------------------------------------------
        [Required(ErrorMessage = "Campo {0] é obrigatório.")]
        [Display(Name = "Valor")]
        public string Valor { get; set; }
        //-----------------------------------------------------------
        public int IdTipoMetrica { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}