﻿using Admin.Helpers;
using Admin.Interfaces;

namespace Admin.ViewModel {
    //===============================================================
    public class RankingValorViewModel:IReportable {

        #region Propriedades
        //-----------------------------------------------------------
        [ReportColumn(Index = 0, Nome = "Nome", TipoColuna = ReportHelper.TipoColuna.Texto)]
        [ReportLink(PropriedadeUrl = nameof(Url), NovaGuia = true)]
        public string Nome { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 1, Nome = "Valor", TipoColuna = ReportHelper.TipoColuna.Moeda)]
        public decimal Valor { get; set; }
        //-----------------------------------------------------------
        public string Url { get; set; }
        //-----------------------------------------------------------
        public string TituloAltUrl { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}