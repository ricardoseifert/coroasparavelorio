﻿using System;
using Admin.Helpers;
using Admin.Interfaces;

namespace Admin.ViewModel {
    //===============================================================
    [Report(Paginacao = true, QuantidadePorPagina = 15)]
    public class LocalRecente: IReportable {

        #region Propriedades
        //-----------------------------------------------------------
        [ReportColumn(Index = 0, Nome = "Local", TipoColuna = ReportHelper.TipoColuna.Texto)]
        public string Local { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 1, Nome = "Cidade", TipoColuna = ReportHelper.TipoColuna.Texto)]
        public string Cidade { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 2, Nome = "Estado", TipoColuna = ReportHelper.TipoColuna.Texto)]
        public string Estado { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 3, Nome = "Data Criação", TipoColuna = ReportHelper.TipoColuna.DataFull)]
        public DateTime DataCriacao { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = -4, Nome = "Ativo", TipoColuna = ReportHelper.TipoColuna.Booleano)]
        public bool Ativo { get; set; }
        //-----------------------------------------------------------
        [ReportColumn(Index = 0, Nome = "Idade", TipoColuna = ReportHelper.TipoColuna.Numero)]
        public int? Idade { get; set; }
        //-----------------------------------------------------------
        public int teste { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}