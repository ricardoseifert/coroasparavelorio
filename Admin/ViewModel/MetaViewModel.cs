﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Admin.ViewModel {
    //===============================================================
    public class MetaViewModel {

        #region Construtor
        //-----------------------------------------------------------
        public MetaViewModel(){
            Metricas = new List<MetricaViewModel>();
        }
        //-----------------------------------------------------------
        #endregion

        #region Propriedades
        //-----------------------------------------------------------
        public int Id { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Area")]
        [Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public int IdArea { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public string Nome { get; set; }
        //-----------------------------------------------------------
        public int IdCriterioBusca { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Início")]
        [Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public DateTime? DataInicio { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Término")]
        [Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public DateTime? DataFim { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Colaborador")]
        //[Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public int? IdUsuario { get; set; }
        //-----------------------------------------------------------
        [Display(Name = "Status")]
        [Required(ErrorMessage = "Campo {0} é obrigatório.")]
        public int IdStatus { get; set; }
        //-----------------------------------------------------------
        public List<MetricaViewModel> Metricas { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}