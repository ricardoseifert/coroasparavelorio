﻿using System;

namespace Admin.ViewModel {
    //===============================================================
    public class FiltroDashComercialViewModel {

        #region Propriedades
        //-----------------------------------------------------------
        public DateTime DataInicioFaturamento { get; set; }
        //-----------------------------------------------------------
        public DateTime DataFimFaturamento { get; set; }
        //-----------------------------------------------------------
        public DateTime? DataInicioAtivacao { get; set; }
        //-----------------------------------------------------------
        public DateTime? DataFimAtivacao { get; set; }
        //-----------------------------------------------------------
        public DateTime? DataInicioVisitas { get; set; }
        //-----------------------------------------------------------
        public DateTime? DataFimVisitas { get; set; }
        //-----------------------------------------------------------
        public int? IdUsuario { get; set; }
        //-----------------------------------------------------------
        public int? IdMeta { get; set; }
        //-----------------------------------------------------------
        public bool CarregarMais { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}