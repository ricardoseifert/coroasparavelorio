﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Admin.Interfaces {
    //===============================================================
    public interface IPesquisa {

        #region Propriedades
        //-----------------------------------------------------------
        
        int IndexAtual { get; set; }
        //-----------------------------------------------------------
        
        int UltimoIndex { get; set; }
        //-----------------------------------------------------------
        #endregion

    }
    //===============================================================
}
