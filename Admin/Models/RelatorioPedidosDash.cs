﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.IO;

namespace Admin.ViewModels
{
    public class RelatorioPedidosDash
    {
        public string NumeroPedido { get; set; }        
    }

}