﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.IO;

namespace Admin.ViewModels
{
    public class PedidoModel
    {
        public List<PedidosModel> pedidos = new List<PedidosModel>();

        public PedidoModel(Fornecedor fornecedor)
        {
            foreach (var pedido in fornecedor.Pedidoes)
            {
                foreach (var item in pedido.PedidoItems)
                {
                    pedidos.Add(new PedidosModel() { Tipo = PedidosModel.TodosTipos.Cliente, ValorRepasseTotal = pedido.ValorRepasse, ItemPedidoID = item.ID, PedidoID = pedido.ID, Numero = pedido.Numero, Falecido = pedido.PessoaHomenageada, LocalEntrega = pedido.LocalEntrega, ValorRepasse = item.Repasse.HasValue ? item.Repasse.Value : 0, DataRepasse = pedido.DataRepasse, Valor = item.Valor, ValorTotal = pedido.ValorTotal, Produto = item.ProdutoTamanho.Produto.Nome, Tamanho = item.ProdutoTamanho.Tamanho.Nome });
                }
            }
            foreach (var pedido in fornecedor.PedidoEmpresas)
            {
                pedidos.Add(new PedidosModel() { Tipo = PedidosModel.TodosTipos.Empresa, ValorRepasseTotal= pedido.ValorRepasse, ItemPedidoID = pedido.ID, PedidoID = pedido.ID, Numero = pedido.ID.ToString(), Falecido = pedido.PessoaHomenageada, LocalEntrega = pedido.LocalEntrega, ValorRepasse = pedido.ValorRepasse, DataRepasse = pedido.DataRepasse, Valor = pedido.Valor, ValorTotal = pedido.Valor, Produto = pedido.ProdutoTamanho.Produto.Nome, Tamanho = pedido.ProdutoTamanho.Tamanho.Nome });
            }
        } 
        
    }

    public class PedidosModel
    {
        public enum TodosTipos
        {
            Cliente = 1,
            Empresa = 2
        }

        public TodosTipos Tipo
        {
            get { return (TodosTipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public int TipoID { get; set; }
        public int ItemPedidoID { get; set; }
        public int PedidoID { get; set; }
        public string Numero { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorTotal { get; set; }
        public string Produto { get; set; }
        public string Tamanho { get; set; }
        public string Falecido { get; set; }
        public string LocalEntrega { get; set; }
        public decimal ValorRepasse { get; set; }
        public decimal ValorRepasseTotal { get; set; }
        public DateTime? DataRepasse { get; set; }
    }

}