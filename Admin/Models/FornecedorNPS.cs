﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.IO;

namespace Admin.ViewModels
{
    public class FornecedorNPS
    {
        public decimal Nota { get; set; }
        public string Nome { get; set; }
        public string ContaPrimario { get; set; }
        public string Estado { get; set; }
        public string PessoaContato { get; set; }
        public string EmailContato { get; set; }
        public string Telefone { get; set; }
        public string Floricultura { get; set; }
        public string Cidade { get; set; }
        public int IdFornecedor { get; set; }
        public int? IdAdmin { get; set; }
        public int QtdNota { get; set; }
        public DateTime DataEnvioAvaliacao { get; set; }
        public DateTime DataPedido { get; set; }
        public string Obs { get; set; }
        public string NrPedido { get; set; }
        public int IdPedido { get; set; }
        public string Respostas { get; set; }
        public string NomeProcessou { get; set; }


        public FornecedorNPS(string Cidade, string Floricultura, string Nome,  string PessoaContato,  string EmailContato,  string Telefone,  string Estado,  int Nota )
        {
            this.Cidade = Cidade;
            this.Floricultura = Floricultura;
            this.PessoaContato = PessoaContato;
            this.EmailContato = EmailContato;
            this.Telefone = Telefone;
            this.Estado = Estado;
            this.Nota = Nota;
        }

        public FornecedorNPS()
        {

        }

    }

}