﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.IO;

namespace Admin.ViewModels
{
    public class AdminModel
    {
        private IPersistentRepository<Administrador> adminRepository;

        public int ID { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }
        public string PerfilNome { get; set; }
        public int PerfilId { get; set; }
        public Administrador CurrentAdministrador { get; set; }

        public AdminModel(ObjectContext context)
        {
            adminRepository = new PersistentRepository<Administrador>(context);

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var loggeduser = LoggedUser.GetFromJSON(HttpContext.Current.User.Identity.Name);
                int intIdUser = loggeduser.ID;
                var admin = adminRepository.Get(intIdUser);
                if (admin != null)
                {
                    ID = admin.ID;
                    IsAuthenticated = true;
                    Nome = admin.Nome;
                    Foto = admin.Foto;
                    CurrentAdministrador = admin;
                    PerfilId = admin.PerfilID;
                    PerfilNome = "N/A";

                    switch (admin.PerfilID)
                    {
                        case 9:
                            PerfilNome = "Coordenadores";
                            break;
                        case 3:
                            PerfilNome = "Fornecedores";
                            break;
                    }
                    
                }
            }
        }

    }

}