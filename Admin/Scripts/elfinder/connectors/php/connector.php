<?php

error_reporting(E_ALL); // Set E_ALL for debuging

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set('Europe/Moscow');
}

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinder.class.php';

/**
 * Simple example how to use logger with elFinder
 **/
class elFinderLogger implements elFinderILogger {
	
	public function log($cmd, $ok, $context, $err='', $errorData = array()) {
		if (false != ($fp = fopen('./log.txt', 'a'))) {
			if ($ok) {
				$str = "cmd: $cmd; OK; context: ".str_replace("\n", '', var_export($context, true))."; \n";
			} else {
				$str = "cmd: $cmd; FAILED; context: ".str_replace("\n", '', var_export($context, true))."; error: $err; errorData: ".str_replace("\n", '', var_export($errorData, true))."\n";
			}
			fwrite($fp, $str);
			fclose($fp);
		}
	}
	
}

$opts = array(
	'root'            => '../../../../content/files/',                       // path to root directory
	'URL'             => 'http://www.coroasparavelorio.com.br/content/files/', // root directory URL
	'rootAlias'       => 'Home',       // display this instead of root directory name
	'defaults'        => array(        // default permisions
                          'read'   => true,
                          'write'  => true,
                          'rm'     => true
	                        ),
	 'tmbDir'       => '.tmb',       // directory name for image thumbnails. Set to "" to avoid thumbnails generation
   'uploadAllow'   => array('images/*')
	
);

$fm = new elFinder($opts); 
$fm->run();

?>