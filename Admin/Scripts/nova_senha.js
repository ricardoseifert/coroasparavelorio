﻿$(document).ready(function () {
  

    $("#form_senha").validate({
        meta: "validate",
        invalidHandler: function (form, validator) {
            $("#erro").show();
            $("#erro").focus();
            validator.focusInvalid();
        },
        submitHandler: function (form) {
            $("#erro").hide();
            $("#salvar").hide();
            $("#processando").show();
                $.post("/Home/SetarNovaSenha", { senha_atual: $("#senha").val(), nova_senha: $("#nova_senha").val() }, function (data) {
                    if (data.retorno == "OK") {
                        alert('Sua senha foi alterada com sucesso!');
                    } else {
                        alert(data.mensagem);
                    }
                    $("#salvar").show();
                    $("#processando").hide();
                });
        },
        rules: {
            senha: "required",
            nova_senha: {
                required: true,
                equalTo: "#confirma_senha"
            },
            confirma_senha: "required"
        },
        messages: {
            senha: "digite sua senha atual",
            nova_senha: {
                required: "digite sua nova senha",
                equalTo: "as senhas digitadas não conferem"
            },
            confirma_senha: "digite a confirmação da senha"
        }
    });
});
