﻿$(function () {

    var options = {
        lines: { show: true },
        points: { show: true },
        xaxis: {
            mode: "time",
            minTickSize: [1, "day"]
        }
    };
    var data = [];
    var datasets = [];
    var placeholder = $("#mws-line-chart");
    var choiceContainer = $("#choices");
    var dataurl = "/Home/GetVisualizacao";

    // then fetch the data with jQuery
    function onDataReceived(retorno) {
        datasets = retorno;
        var i = 0;
        $.each(datasets, function (key, val) {
            val.color = i;
            ++i;
        });

        // insert checkboxes 
        $.each(datasets, function (key, val) {
            choiceContainer.append('<input type="checkbox" name="' + key +
                               '" checked="checked" id="id' + key + '">' +
                               '<label for="id' + key + '">'
                                + val.label + '</label>&nbsp;');
        });
        choiceContainer.find("input").click(plotAccordingToChoices);

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key])
                data.push(datasets[key]);
        });

        if (data.length > 0)
            $.plot(placeholder, data, options);
        $("#processando").hide();
    }


    function plotAccordingToChoices() {
        var data = [];

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key])
                data.push(datasets[key]);
        });

        if (data.length > 0)
            $.plot(placeholder, data, options);
    }

    function getData(inicio, fim) {
        $.ajax({
            url: dataurl,
            method: 'GET',
            dataType: 'json',
            success: onDataReceived
        });
    }
    $("#processando").show();
    //setTimeout(getData, 1000);
});