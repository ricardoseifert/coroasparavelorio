function youtubeIDextract(n) {
    return n.replace(/^[^v]+v.(.{11}).*/, "$1")
}

function processURL(n) {
    var t, r, i;
    if (n.indexOf("youtube.com") > -1) return t = "http://www.youtube.com/embed/" + n.replace(/^[^v]+v.(.{11}).*/, "$1"), ["youtube", t];
    if (n.indexOf("youtu.be") > -1) return t = "http://www.youtube.com/embed/" + n.split("/")[1], ["youtube", t];
    if (n.indexOf("vimeo.com") > -1) {
        if (r = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/, i = n.match(r), i) return ["vimeo", "http://player.vimeo.com/video/" + i[2]];
        alert("not a vimeo url")
    } else return alert("URL não reconhecida"), ["error", ""]
}

function verificaNumero(n) {
    if (n.which != 8 && n.which != 0 && (n.which < 48 || n.which > 57)) return !1
}

function formatDinheiro(n) {
    var t = n.toFixed(2).split(".");
    return t[0].split("").reverse().reduce(function (n, t, i) {
        return t + (i && !(i % 3) ? "." : "") + n
    }, "") + "," + t[1]
}
$(document).ready(function () {
    var n = {
        cssClass: "el-rte",
        height: 300,
        toolbar: "normal",
        cssfiles: ["/content/elrte/css/elrte-inner.css"],
        fmAllow: !0,
        fmOpen: function (n) {
            $('<div id="myelfinder"><\/div>').elfinder({
                url: "/scripts/elfinder/connectors/php/connector.php",
                lang: "en",
                height: 300,
                dialog: {
                    width: 640,
                    modal: !0,
                    title: "Selecione a Imagem"
                },
                closeOnEditorCallback: !0,
                editorCallback: n
            })
        }
    };
    $("#Descricao").is("textarea") && $("#Descricao").elrte(n), $("#Telefone").mask({
        mask: "(##) ########?#",
        allowPartials: !0,
        placeholder: " "
    }), $("#CelularContato").mask({
        mask: "(##) ########?#",
        allowPartials: !0,
        placeholder: " "
    }), $("#TelefonePrimario").mask({
        mask: "(##) ########?#",
        allowPartials: !0,
        placeholder: " "
    }), $("#TelefoneSecundario").mask({
        mask: "(##) ########?#",
        allowPartials: !0,
        placeholder: " "
    }), $("#TelefoneContato").mask({
        mask: "(##) ########?#",
        allowPartials: !0,
        placeholder: " "
    }), $("#CPF").mask({
        mask: "###.###.###-##",
        allowPartials: !1
    }), $("#CEP").mask({
        mask: "#####-###",
        allowPartials: !1
    }), $("#CNPJ").mask({
        mask: "##.###.###/####-##",
        allowPartials: !1
        }),
        $("#PrecoInicio").maskMoney(),
        $("#PrecoFim").maskMoney(),
        $("#Preco").maskMoney(),
        $("#Valor").maskMoney(),
        $(".repasse").maskMoney(),
        $("#ValorRepasse").maskMoney({
        allowZero: true
        }),

        $("#ValorDesconto").maskMoney(), $("#ValorTotal").maskMoney(), $("#ValorEstorno").maskMoney(), $("#PrecoAntigo").maskMoney(), $("#NovoValor").maskMoney(), $(".lvalor").maskMoney(), $(".ldata").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $(".ldatah").mask({
        mask: "##/##/#### ##:##",
        allowPartials: !1
    }), $("#Data").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#DataValidade").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#DataVencimento").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#DataEstorno").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#VencimentoBoleto").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#DataRepasse").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#DataEntrega").mask({
        mask: "##/##/#### ##:##",
        allowPartials: !1
    }), $("#DataPagamentoForma").mask({
        mask: "##/##/#### ##:##",
        allowPartials: !1
    }), $("#NFeData").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#datainicial").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#datafinal").mask({
        mask: "##/##/####",
        allowPartials: !1
    }), $("#PaisID").change(function () {
        $("#PaisID option:selected").text() != "Brasil" ? ($("#bloco_estrangeiro").show(), $("#bloco_brasil").hide()) : ($("#bloco_estrangeiro").hide(), $("#bloco_brasil").show())
    }),

    $("#EstadoID").change(function () {

        var n = $("#Estado_ID option:selected").text();

       
        if (n == "") {
            n = $("#EstadoID option:selected").text();
        }

        $("#CidadeID").length > 0 ? ($("#CidadeID").hide(), $("#load_cidade").show(), $("#CidadeID").get(0).options.length = 0, $("#CidadeID").get(0).options[0] = new Option("-- selecione --", ""), $.post("/Home/GetCidades/", {
            uf: n
        }, function (n) {
            if (n != undefined && n != "")
                for (var t = 0; t < n.length; t++) $("#CidadeID").get(0).options[t + 1] = new Option(n[t].Text, n[t].Value);
            $("#load_cidade").hide(), $("#CidadeID").show()
        })) : ($("#box1View").hide(), $("#load_cidade").show(), $.post("/Home/GetCidades/", {
            uf: n
        }, function (n) {
            if (n != undefined && n != "")
                for (var t = 0; t < n.length; t++) $("#box1View").get(0).options[t + 1] = new Option(n[t].Text, n[t].Value);
            $("#load_cidade").hide(), $("#box1View").show()
        }))
    }),


    $("#EstadoEntregaID").change(function () {
        var n = $("#EstadoEntregaID option:selected").text();
        $("#CidadeEntregaID").length > 0 && ($("#CidadeEntregaID").hide(), $("#load_cidade_entrega").show(), $("#CidadeEntregaID").get(0).options.length = 0, $("#CidadeEntregaID").get(0).options[0] = new Option("-- selecione --", ""), $.post("/Home/GetCidades/", {
            uf: n
        }, function (n) {
            if (n != undefined && n != "")
                for (var t = 0; t < n.length; t++) $("#CidadeEntregaID").get(0).options[t + 1] = new Option(n[t].Text, n[t].Value);
            $("#load_cidade_entrega").hide(), $("#CidadeEntregaID").show()
        }))
    })
});