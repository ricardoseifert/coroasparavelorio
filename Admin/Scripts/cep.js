function getEnderecoCadastro(n) {
    $.trim(n) != "" ? $.post(url + n, function (n) 
    {
        var i, t;
        n.resultado == "1" ? ($("#Logradouro").val(unescape(n.tipo_logradouro + " " + n.logradouro)),
            $("#Bairro").val(unescape(n.bairro)),
            $('#EstadoID option[text="' + unescape(n.uf) + '"]').attr(
            {
                selected: "selected"
            }),
                $("#CidadeID").show(),
                $("#CidadeID").get(0).options.length = 0,
                $("#CidadeID").get(0).options[0] = new Option("-- selecione --", ""),
                i = unescape(n.uf),
                $("#EstadoID option:contains(" + i + ")").attr("selected", "selected"),
                t = unescape(n.cidade).toUpperCase().latinize(),
                $.post("/Home/GetCidades",
                {
                    uf: i,
                    selected: t
                }, function (n){
                if (n != undefined && n != "")
                    for (var i = 0; i < n.length; i++) $("#CidadeID").get(0).options[i + 1] = new Option(n[i].Text, n[i].Value, n[i].Selected);
                $("#CidadeID").find("option:contains('" + t + "')").each(function () {
                    $(this).text() == t && $(this).attr("selected", "selected")
                }), $("#load_cidade").hide()
        }), $("#load_cep").hide(), $("#Numero").focus()) : $("#load_cep").hide()
    }) : $("#load_cep").hide()
}
$(document).ready(function () {
    $("#CEP").blur(function () {
        $("#load_cep").show(), getEnderecoCadastro($("#CEP").val())
    })
});
var url = "/Home/GetCEP?cep=";