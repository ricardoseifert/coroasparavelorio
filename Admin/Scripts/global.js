﻿var contador = 0;
var tempo;
var timer_is_on = 0;


$(document).ready(function () {
    //StartPushNotification();
});


function CronometrarChamada() {
    //Chamar msgs
    VerificarNovaMensagem();
    VerificarNovaNotificacao();
    contador = contador + 1;
    tempo = setTimeout("CronometrarChamada()", 15000);
}

function StartPushNotification() {
    if (!timer_is_on) {
        timer_is_on = 1;
        CronometrarChamada();
    }
}

function StopPushNotification() {
    clearTimeout(t);
    timer_is_on = 0;
}

function VerificarNovaMensagem() {
    $.post('/Home/GetMensagem', function (data) {
        var conteudo_li = "";
        if (data != "") {
            $(".mws-i-24 i-message mws-dropdown-trigger").append(data.length);
            $.each(data, function (index, objevento) {
                if ($("#novaMensagem-" + objevento[0] + "-" + objevento[5]).length <= 0) {
                    var classe = "unread";
                    if (objevento[3] != "") { classe = "read"; }
                    conteudo_li += "<li id='novaMensagem-" + objevento[0] + "-" + objevento[5] + "' class=" + classe + ">"
                    conteudo_li += "<a href='#' onclick='abrirMensagem(\"" + objevento[0] + "\", " + objevento[5] + ", " + objevento[6] + ");'>";
                    conteudo_li += "<span class='sender'>" + objevento[1] + "(" + objevento[0] + ")</span>";
                    conteudo_li += "<span class='message'>";
                    if (objevento[4].length > 20) {
                        conteudo_li += objevento[4].substring(0, 20);
                    } else {
                        conteudo_li += objevento[4];
                    }
                    conteudo_li += "</span><span class='time'>";
                    conteudo_li += objevento[2];
                    conteudo_li += "</span>";
                    conteudo_li += "</li>";
                }
            });


            if (data.length > 0) {
                $("#mensagem_nova").addClass("mws-dropdown-notif");
                $("#mensagem_nova").html(data.length);
            }

        }
        else {
            //alert("Erro ao gravar valores. Detalhes: " + data.mensagem);
            return false;
        }
    });



}


function VerificarNovaNotificacao() {
    $.post('/Home/GetNotificacao', function (data) {
        var conteudo_li = "";
        if (data != "") {
            $(".mws-i-24 i-message mws-dropdown-trigger").append(data.length);
            $.each(data, function (index, objevento) {
                if ($("#novaNotificacao-" + objevento[0] + "-" + objevento[4]).length <= 0) {
                    var classe = "unread";
                    if (objevento[3] != "") { classe = "read"; }
                    conteudo_li += "<li id='novaNotificacao-" + objevento[0] + "-" + objevento[4] + "' class=" + classe + ">"
                    conteudo_li += "<a href='#' onclick='abrirNotificacao(" + objevento[4] + ");'>";
                    conteudo_li += "<span class='sender'>" + objevento[1] + "(" + objevento[0] + ")</span>";
                    conteudo_li += "<span class='message'>";
                    if (objevento[2].length > 20) {
                        conteudo_li += objevento[2].substring(0, 20);
                    } else {
                        conteudo_li += objevento[2];
                    }
                    conteudo_li += "</span><span class='time'>";
                    conteudo_li += objevento[1];
                    conteudo_li += "</span>";
                    conteudo_li += "</li>";
                }
            });


            if (data.length > 0) {
                $("#notificacao_nova").addClass("mws-dropdown-notif");
                $("#notificacao_nova").html(data.length);
            } else {
                $("#notificacao_nova").removeClass("mws-dropdown-notif");
            }

        }
        else {
            $("#notificacao_nova").removeClass("mws-dropdown-notif");
            //alert("Erro ao gravar valores. Detalhes: " + data.mensagem);
            return false;
        }
    });



}

function abrirMensagem(pagina, id) {
    window.location = '/' + pagina + '/Resposta/' + id;
}

function abrirNotificacao(id) {
    window.location = '/Notificacao?id=' + id;
}