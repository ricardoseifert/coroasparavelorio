﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="col-md-12 center">
    <h3>
        Vendas por Sites
    </h3>
    <hr />
</div>
<script runat="server">
    void Page_Load(object sender, EventArgs e)
    {
        Domain.Entities.COROASEntities teste = new Domain.Entities.COROASEntities();

        var result = teste.RelatorioVendaSite(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day), DateTime.Now);

        ReportViewer1.ShowFindControls = false;

        if (!Page.IsPostBack)
        {
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/VendasPorSite.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource novoDt = new ReportDataSource("RelatorioVendasSite", result);
            ReportViewer1.LocalReport.DataSources.Add(novoDt);
            ReportViewer1.LocalReport.Refresh();
        }
    }
        
</script>
<form id="Form1" style="width: 909px; position: relative; margin: auto; /* text-align: center;
*/" runat="server">
<asp:scriptmanager id="ScriptManager1" runat="server">
        </asp:scriptmanager>
<rsweb:ReportViewer ID="ReportViewer1" PageCountMode="Actual" runat="server" Font-Names="Verdana"
    Font-Size="8pt" InteractiveDeviceInfos="(Collection)" AsyncRendering="false"
    SizeToReportContent="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
    Width="" Height="100px">
    <LocalReport ReportPath="Reports\VendasPorSite.rdlc">
    </LocalReport>
</rsweb:ReportViewer>
</form>
