﻿$(document).ready(function () {
    if ($(this).scrollTop() > 50) {
        $("#BarraCinza").hide();
        $("#TelsTop").show();
    }
    else {
        $("#BarraCinza").show();
        $("#TelsTop").hide();
    }
});

var savedobj = false;
function mostra(target) {
    obj = (document.all) ? document.all[target] : document.getElementById(target);    
    savedobj = obj;
    obj.style.display = 'inline';
}

function esconde(target) {
    obj = (document.all) ? document.all[target] : document.getElementById(target);
    savedobj = obj;    
    obj.style.display = 'none';
}

/////////////////////////////////////////////////////////////////////////////

function mostra_esconde(x) {
    if (document.all(x).style.display == "inline") {
        esconde(x);
    } else {
        mostra(x);
    }
}

/////////////////////////////////////////////////////////////////////////////

function mostra_esconde2(x, opcao) {

    if (opcao == "2") {
        var class1 = "texto2"
        var class2 = "texto_visivel2"
    } else if (opcao == "3") {
        var class1 = "redes_sociais"
        var class2 = "redes_sociais_visivel"
    } else if (opcao == "4") {
        var class1 = "meios_pagamento"
        var class2 = "meios_pagamento_visivel"
    } else if (opcao == "5") {
        var class1 = "seguranca"
        var class2 = "seguranca_visivel"
    } else {
        var class1 = "texto"
        var class2 = "texto_visivel"
    }

    if (document.all(x).style.display == "inline" || document.getElementById("input_" + x).value == "esconde") {
        document.getElementById("input_" + x).value = "mostra";
        document.getElementById(x).className = class1;
    } else {
        document.getElementById(x).className = class2;
        document.getElementById("input_" + x).value = "esconde";
    }

}

/////////////////////////////////////////////////////////////////////////////

function abre(url, id) {
    if (document.getElementById) {
        var x = (window.ActiveXObject) ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();
    }
    if (x) {
        x.onreadystatechange = function () {
            if (x.readyState == 4 && x.status == 200) {
                el = document.getElementById(id);
                el.innerHTML = x.responseText;
            }
        }
        x.open("GET", url, true);
        x.send(null);
    }
}

/////////////////////////////////////////////////////////////////////////////

function esconde_validacao(x, y) {

    esconde('aviso_' + x);

    if (y == "1") {
        document.getElementById(x).className = "FormCampoInput";
    } else if (y == "2") {
        document.getElementById(x).className = "FormCampoTextarea";
    } else if (y == "3") {
        document.getElementById(x).className = "CorFundo";
    } else if (y == "4") {
        document.getElementById(x).className = "FormCampoSelect";
    } else if (y == "5") {
        document.getElementById(x).className = "FormValidacaoTexto2";
    } else if (y == "6") {
        document.getElementById(x).className = "FormCampo2e3";
    }

    document.getElementById(x).focus();

}

/////////////////////////////////////////////////////////////////////////////

function M_E_Sugestao(id, opcao) {

    if (opcao == "1") {
        document.getElementById(id).className = "Selecionado";
        document.getElementById(id + "1").className = "Selecionado1";
        document.getElementById(id + "2").className = "Selecionado2";
        document.getElementById(id + "Valor").className = "Valores";
    } else if (opcao == "2") {
        document.getElementById(id).className = "NaoSelecionado";
        document.getElementById(id + "1").className = "NaoSelecionado1";
        document.getElementById(id + "2").className = "NaoSelecionado2";
        document.getElementById(id + "Valor").className = "Valores3";
    }

}

/////////////////////////////////////////////////////////////////////////////

function M_E_Pag_Produto(id, opcao) {

    if (opcao == "1") {
        mostra(id);
        document.getElementById(id + "T").className = "FloresContemFotosSelecionado";
    } else if (opcao == "2") {
        esconde(id);
        document.getElementById(id + "T").className = "FloresContemFotosNaoSelecionado";
    }

}

////////////////////////////////////////////////////////////////////////////

function formatar(src, mask) {
    var i = src.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i)
    if (texto.substring(0, 1) != saida) {
        src.value += texto.substring(0, 1);
    }
}

/////////////////////////////////////////////////////////////////////////////

$(".ComprarAction").click(function () {
    var Url = $(this).attr("href");
    var Tamanho = $("#TamanhoProduto").val();
    if (Tamanho != undefined) {
        $(this).attr("href", Url + "?tamanho=" + Tamanho + "#comprar");
    }
});

/////////////////////////////////////////////////////////////////////////////

$(".escolha_tamanho_div .MudaTamanho").click(function () {
    try {
        $("#TamanhoProduto").val($(this).attr("class").split(" ")[1]);
    }
    catch (err) {
    }
});

/////////////////////////////////////////////////////////////////////////////

$("#VejaMais").click(function () {
    var Scrolled = false;
    $("#ListagemProdutos .div_centraliza .none:lt(4)").fadeIn('fast', function () {
        $(this).removeClass("none");
        if ($("#ListagemProdutos .div_centraliza .none:lt(4)").length == 0) {
            $("#VejaMais").fadeOut();
        }
        if (Scrolled == false) {
            $('html, body').animate({
                scrollTop: $("#VejaMais").offset().top - 490
            }, 800);
            Scrolled = true;
        }
    });

});

/////////////////////////////////////////////////////////////////////////////

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("#BarraCinza").hide();
        $("#TelsTop").show();
    }
    else {
        $("#BarraCinza").show();
        $("#TelsTop").hide();
    }
});