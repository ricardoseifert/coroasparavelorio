﻿$(document).ready(function () {

    $("#cadastro-fisica-cep").blur(function () {
        $("#loading-fisica-cep").show();
        getEnderecoCadastro($("#cadastro-fisica-cep").val(), 'fisica');
    });

    $("#cadastro-juridica-cep").blur(function () {
        $("#loading-juridica-cep").show();
        getEnderecoCadastro($("#cadastro-juridica-cep").val(), 'juridica');
    });

    $("#CEP").blur(function () {
        $("#load_cep").show();
        getEnderecoCadastroCEP($("#CEP").val());
    });

});

//var url = "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=";
var url = "/Home/GetCEP?cep=";

function getEnderecoCadastro(cep, campo) {
    if ($.trim(cep) != "" && $.trim(cep) != "_____-___") {
        $.post(url + cep, function (resultadoCEP) {
            if (resultadoCEP.resultado == "1") { 
                $("#cadastro-" + campo + "-logradouro").val(unescape(resultadoCEP.tipo_logradouro + ' ' + resultadoCEP.logradouro));
                $("#cadastro-" + campo + "-bairro").val(unescape(resultadoCEP.bairro));
                $('#cadastro-' + campo + '-estado option[text="' + unescape(resultadoCEP.uf) + '"]').attr({ selected: "selected" });

                $("#loading-" + campo + "-cidade").show();

                $("#cadastro-" + campo + "-cidade").get(0).options.length = 0;
                $("#cadastro-" + campo + "-cidade").get(0).options[0] = new Option("-- selecione --", "");

                var uf = unescape(resultadoCEP.uf);
                $("#cadastro-" + campo + "-estado option:contains(" + uf + ")").attr('selected', 'selected');

                var cidade = unescape(resultadoCEP.cidade).toUpperCase().latinize();

                $.post('/Home/GetCidades', { uf: uf, selected: cidade }, function (data) {
                    if (data != undefined && data != "") {
                        for (var i = 0; i < data.length; i++) {
                            $("#cadastro-" + campo + "-cidade").get(0).options[i + 1] = new Option(data[i].Text, data[i].Value, data[i].Selected);
                        }
                    }

                    $("#cadastro-" + campo + "-cidade").find("option:contains('" + cidade + "')").each(function () {
                        if ($(this).text() == cidade) {
                            $(this).attr("selected", "selected");
                        }
                    });
                    $("#loading-" + campo + "-cidade").hide();
                });
                $("#loading-" + campo + "-cep").hide();
                $("#cadastro-" + campo + "-numero").focus();
            } else {
                $("#loading-" + campo + "-cep").hide();
            }
        });

    } else {
        $("#loading-" + campo + "-cep").hide();
    }
}

function getEnderecoCadastroCEP(cep) {
    if ($.trim(cep) != "" && $.trim(cep) != "_____-___") {
        $.post(url + cep, function (resultadoCEP) {
            if (resultadoCEP.resultado == "1") {
                $("#Logradouro").val(unescape(resultadoCEP.tipo_logradouro + ' ' + resultadoCEP.logradouro));
                $("#Bairro").val(unescape(resultadoCEP.bairro));
                $('#EstadoID option[text="' + unescape(resultadoCEP.uf) + '"]').attr({ selected: "selected" });

                $("#CidadeID").show();

                $("#CidadeID").get(0).options.length = 0;
                $("#CidadeID").get(0).options[0] = new Option("-- selecione --", "");

                var uf = unescape(resultadoCEP.uf);
                $("#EstadoID option:contains(" + uf + ")").attr('selected', 'selected');

                var cidade = unescape(resultadoCEP.cidade).toUpperCase().latinize();

                $.post('/Home/GetCidades', { uf: uf, selected: cidade }, function (data) {
                    if (data != undefined && data != "") {
                        for (var i = 0; i < data.length; i++) {
                            $("#CidadeID").get(0).options[i + 1] = new Option(data[i].Text, data[i].Value, data[i].Selected);
                        }
                    }

                    $("#CidadeID").find("option:contains('" + cidade + "')").each(function () {
                        if ($(this).text() == cidade) {
                            $(this).attr("selected", "selected");
                        }
                    });
                    $("#load_cidade").hide();
                });
                $("#load_cep").hide();
                $("#Numero").focus();
            } else {
                $("#load_cep").hide();
            }
        });

    } else {
        $("#load_cep").hide();
    }
}