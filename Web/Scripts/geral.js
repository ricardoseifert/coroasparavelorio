﻿$(document).ready(function () {

    // ADICIONADO - RICARDO
    if ($(".fancy").html() != undefined) {
        $(".fancy").fancybox();
    }

    if ($('#BannersSup div.banner').length >= 2) {
        setInterval('BannerSup.Proximo()', 5000);
    }
    if ($('#BannersInf div.banner').length >= 2) {
        setInterval('BannerInf.Proximo()', 5000);
    }

    //BuscaLocais.Posicionar();


    $('.produto').each(function () {
        //        $(this).find('.mais-detalhes').click(function () {
        //            var id = $(this).attr('data-id');
        //            $(this).hide();
        //            $('#Produto_' + id + ' .box-detalhes').show();
        //        });
        $(this).find('.botao-comprar').click(function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var tamanho = $('#Produto_' + id + ' div.coroa.ativo').attr('id');
            if (tamanho != undefined) {
                url += '?tamanho=' + tamanho + '#comprar';
            }
            window.location = url;
        });
    });

    $('div.planos .coroa').each(function () {
        $(this).click(function () {
            var id = $(this).attr('id');
            var coroa = $(this).attr('data-rel');
            $('#Produto_' + coroa + ' div.planos .coroa').removeClass('ativo');
            $('#Produto_' + coroa + ' div.planos .coroa').addClass('desativado');
            $('#Produto_' + coroa + ' div.planos #' + id).addClass('ativo');
            $('#Produto_' + coroa + ' div.planos #' + id).removeClass('desativado');
        });
    });

    var totalclique = 0;
    $('div.coroa').each(function () {
        $(this).click(function () {
            totalclique = totalclique + 1;
            if (totalclique < 1) {
                var id = $(this).attr('id');
                $('.leve-mais').fadeOut('fast');
                $('#Leve' + id).fadeIn('slow');
            } else {
                $('.leve-mais').fadeOut('fast');
            }
        });
    });

    $('.coroas-lista #comprar').click(function () {
        alert('Remover geral linha 40');
        //location.href = $(this).attr("data-url") + "?tipo=" + $(this).attr("data-tipo");
    });

    // Accordion Dúvidas Frequentes

    var Cont1 = false;
    $('.accordion li.title1').click(function () {
        if (Cont1 == false) {
            $('.accordion li.cont1').slideDown();
            Cont1 = true;
        }
        else {
            $('.accordion li.cont1').slideUp();
            Cont1 = false;
        }
    });

    var Cont2 = false;
    $('.accordion li.title2').click(function () {
        if (Cont2 == false) {
            $('.accordion li.cont2').slideDown();
            Cont2 = true;
        }
        else {
            $('.accordion li.cont2').slideUp();
            Cont2 = false;
        }
    });

    var Cont3 = false;
    $('.accordion li.title3').click(function () {
        if (Cont3 == false) {
            $('.accordion li.cont3').slideDown();
            Cont3 = true;
        }
        else {
            $('.accordion li.cont3').slideUp();
            Cont3 = false;
        }
    });

    var Cont4 = false;
    $('.accordion li.title4').click(function () {
        if (Cont4 == false) {
            $('.accordion li.cont4').slideDown();
            Cont4 = true;
        }
        else {
            $('.accordion li.cont4').slideUp();
            Cont4 = false;
        }
    });

    var Cont5 = false;
    $('.accordion li.title5').click(function () {
        if (Cont5 == false) {
            $('.accordion li.cont5').slideDown();
            Cont5 = true;
        }
        else {
            $('.accordion li.cont5').slideUp();
            Cont5 = false;
        }
    });

    var Cont6 = false;
    $('.accordion li.title6').click(function () {
        if (Cont6 == false) {
            $('.accordion li.cont6').slideDown();
            Cont6 = true;
        }
        else {
            $('.accordion li.cont6').slideUp();
            Cont6 = false;
        }
    });

    // Cartão Condolências Virtuais

    $('#VisualizarCartao').click(function () {
        if ($("#tipo:checked").val().length > 0) {
            $('#visualizaPara').html($('#nomePara').val());
            $('#visualizaMensagem').html($('#mensagem').val().replace(/\n/g, "<br />"));
            $('#visualizaDe').html($('#nomeDe').val());

            if ($('#tipo:checked').val() == 1) {
                $(".cor").attr("style", "background: #0074bc;");
                $(".rosa-condolencia").attr("src", "../../Content/html/condolencias/flor-azul.jpg");
            } else if ($('#tipo:checked').val() == 2) {
                $(".cor").attr("style", "background: #0a736e;");
                $(".rosa-condolencia").attr("src", "../../Content/html/condolencias/flor-verde.jpg");
            } else if ($('#tipo:checked').val() == 3) {
                $(".cor").attr("style", "background: #c05398;");
                $(".rosa-condolencia").attr("src", "../../Content/html/condolencias/flor-rosa.jpg");
            }

            $('.escurece').show();
        } else {
            alert('Selecione o cartão.');
        }
        return false;
    });
    $('.close').click(function () {
        $('.escurece').hide();
        $('.box').hide();
        return false;
    });

    $('#usuario-menu-entrar').click(function () {
        var email = $('#usuario-menu-email').val();
        var senha = $('#usuario-menu-senha').val();
        Cliente.Entrar(email, senha,
            function () {
                window.location = window.location;
            },
            function (data) {
                alert(data);
            }
        );
    });

    $('#autenticacao-entrar').click(function () {
        var email = $('#autenticacao-email').val();
        var senha = $('#autenticacao-senha').val();
        var returnUrl = $('#returnUrl').val();
        Cliente.Entrar(email, senha,
        function () {
            window.location = returnUrl;
        },
        function (data) {
            alert(data);
        }
    );
    });

    $('#usuario-menu-sair').click(function () {
        Cliente.Sair(function () {
            window.location = window.location;
        });
    });

    $('#ResumoCarrinho').click(function () {
        Carrinho.Abrir();
    });


    // MODIFICACAO - RICARDO
    if ($('#primeira_telefone').html() != undefined) {
        $('#primeira_telefone').mask('(99) 99999999?9');
    }

    // MODIFICACAO - RICARDO
    if ($('#primeira_form').html() != undefined) {
        $('#primeira_form').validate({
            meta: "validate",
            invalidHandler: function (n, t) {
                var i = t.numberOfInvalids(), r; if (i) { if (r = i == 1 ? "Por favor, corrija o seguinte erro:\n" : "Por favor, corrija os " + i + " erros abaixo:\n", i = "", t.errorList.length > 0) for (x = 0; x < t.errorList.length; x++) i += "\n● " + t.errorList[x].message; alert(r + i) } t.focusInvalid()
            },
            submitHandler: function (form) {
                $.post('Home/PrimeiraCompra', { nome: $("#primeira_nome").val(), telefone: $("#primeira_telefone").val(), email: $("#primeira_email").val() }, function (data) {
                    if (data.status == 'OK') {
                        alert(data.mensagem);
                        FechaPrimeiraCompra();
                    } else {
                        alert(data.mensagem);
                    }
                });
            },
            rules: {
                'primeira_nome': 'required',
                'primeira_email': {
                    required: true,
                    email: true
                },
                'primeira_telefone': 'required'
            },
            messages: {
                'primeira_nome': 'Preencha seu Nome Completo',
                'primeira_email': {
                    required: "Digite seu Email",
                    email: "Digite um e-mail válido"
                },
                'primeira_telefone': 'Preencha seu Telefone'
            }
        });
    }
});

function AbrePrimeiraCompra() {
    $(".bg").show();
    $(".popup_primeira_compra").show('slow');
}

function FechaPrimeiraCompra() {
    $(".bg").hide();
    $(".popup_primeira_compra").hide('slow');
    $.post('Home/FechaPrimeiraCompra');
}

// Mostra item da Lista de Pedidos
function mostraPedido(id) {
    $('#pedido-' + id).show();
    $('.escurece').show();
    return false;
}

//Box Mais Detalhes - Produtos

$('.mais-detalhes').click(function () {
    idbox = "box-detalhes-" + $(this).attr('data-id');
    idpreco = "preco-" + $(this).attr('data-id');
    $(this).hide();
    $("." + idbox).slideDown('slow');
    $("." + idpreco).hide();
});

// Flores
function mostraFlor(id) {
    $('.txt').css({ 'z-index': '1' });
    $('.caixa-txt .' + id).css({ 'z-index': '5' });
}

var BannerInf = new _BannerInf();
var BannerSup = new _BannerSup();
//var BuscaLocais = new _BuscaLocais();
var Pagina = new _Pagina();
var Pedido = new _Pedido();
var Shopline = new _Shopline();
var Carrinho = new _Carrinho();
var Cliente = new _Cliente();
var Local = new _Local();

function _BannerInf() {
    this.Proximo = function () {
        var total = $('#BannersInf div.banner').length;
        var atual = $('#BannersInf div.banner').index($('#BannersInf div.banner.selecionado'));
        var proximo = atual + 1;
        if (proximo >= total) {
            proximo = 0;
        }
        $('#BannersInf div.banner').removeClass('selecionado');
        $('#BannersInf div.navegacao a').removeClass('selecionado');
        $($('#BannersInf div.banner')[proximo]).addClass('selecionado');
        $($('#BannersInf div.navegacao a')[proximo]).addClass('selecionado');
    };

    this.Abrir = function (id) {
        $('#BannersInf div.banner').removeClass('selecionado');
        $('#BannersInf div.navegacao a').removeClass('selecionado');
        $('#Banner_' + id).addClass('selecionado');
        $('#Navegacao_' + id).addClass('selecionado');
    };
}

function _BannerSup() {
    this.Proximo = function () {
        var total = $('#BannersSup div.banner').length;
        var atual = $('#BannersSup div.banner').index($('#BannersSup div.banner.selecionado'));
        var proximo = atual + 1;
        if (proximo >= total) {
            proximo = 0;
        }
        $('#BannersSup div.banner').removeClass('selecionado');
        $('#BannersSup div.navegacao a').removeClass('selecionado');
        $($('#BannersSup div.banner')[proximo]).addClass('selecionado');
        $($('#BannersSup div.navegacao a')[proximo]).addClass('selecionado');
    };

    this.Abrir = function (id) {
        $('#BannersSup div.banner').removeClass('selecionado');
        $('#BannersSup div.navegacao a').removeClass('selecionado');
        $('#Banner_' + id).addClass('selecionado');
        $('#Navegacao_' + id).addClass('selecionado');
    };
}

//function _BuscaLocais() {

//    this.URL = '';

//    this.Posicionar = function () {
//        if ($('#local-termo').offset() != undefined) {
//            var top = $('#local-termo').offset().top + 28;
//            var left = $('#local-termo').offset().left;
//            $('#ResultadoLocais').offset({ top: top, left: left });

//            $('#local-termo').blur(function () {
//                setTimeout('$("#ResultadoLocais").hide();', 1000);
//            });

//            $('#local-termo').focus(function () {
//                BuscaLocais.Buscar();
//            });

//            $('#local-termo').keyup(function () {
//                BuscaLocais.Buscar();
//            });
//        }
//};

//    this.Buscar = function () {
//        var termo = $('#local-termo').val();
//        if (termo.length > 2) {
//            $.post(BuscaLocais.URL, { termo: termo }, function (data) {
//                BuscaLocais.Carregar(data);
//            });
//        }
//    };

//    this.Carregar = function (resultado) {
//        if (resultado == 'NENHUM') {
//            $('#ResultadoLocais').hide();
//        } else {
//            var html = '';
//            for (var i in resultado) {
//                var item = resultado[i];
//                html += '<div class="tipo">' + item.Tipo + '</div>'
//                for (var l in item.Locais) {
//                    var local = item.Locais[l];
//                    html += '<a href="' + local.Url + '">' + local.Nome + '</a>';
//                }
//            }
//            $('#ResultadoLocais').show();
//            $('#ResultadoLocais').html(html);
//        }
//    };
//}

function _Pagina() {
    this.Ordenar = function (criterio) {
        var locationCurrent = document.URL;
        var locationArray = locationCurrent.split('?');
        var baseUrl = locationArray[0];
        var newUrl = baseUrl + '?';
        var parameters = locationArray.length > 1 ? locationArray[1].split('&') : [];
        var adicionado = false;
        for (var p in parameters) {
            var parameter = parameters[p].split('=');
            var key = parameter[0];
            var value = parameter[1];
            newUrl += (p > 0 ? '&' : '') + key + '=';
            if (key == 'ordenacao') {
                newUrl += criterio;
                adicionado = true;
            } else {
                newUrl += value;
            }
        }
        if (!adicionado) {
            newUrl += (parameters.length > 0 ? '&' : '') + 'ordenacao=' + criterio;
        }
        window.location = newUrl + '#comprar';
    };
}

function _Pedido() {
    this.Criar = function (cidadeID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, parentesco, pessoaHomenageada, telefoneContato, success, error) {
        $.ajax({ 
            url: '/Pedido/Criar',
            type: "POST",
            dataType: 'json',
            data: { cidadeID: cidadeID, dataSolicitada: dataSolicitada, estadoID: estadoID, formaPagamento: formaPagamento, localEntrega: localEntrega, meioPagamento: meioPagamento, observacoes: observacoes, parentesco: parentesco, pessoaHomenageada: pessoaHomenageada, telefoneContato: telefoneContato },
            success: function (data) {
                if (data.Codigo == undefined) {
                    error(data);
                } else {
                    success(data);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                error("Ocorreu um erro ao processar seu pedido. Entre em contato pelo telefone 0800-777-1986 para verificar o status.\n\nErro: " + xhr.status + "\nDescrição: " + thrownError);
            }
        });
    };

    this.Pagar = function (bandeira, nome, numero, validade, codigo, parcela, cidadeID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, parentesco, pessoaHomenageada, telefoneContato, success, error) {
        $.ajax({
            url: '/Pedido/Pagar',
            type: "POST",
            dataType: 'json',
            data: { bandeira: bandeira, nome: nome, numero: numero, validade: validade, codigo: codigo, parcela: parcela, cidadeID: cidadeID, dataSolicitada: dataSolicitada, estadoID: estadoID, formaPagamento: formaPagamento, localEntrega: localEntrega, meioPagamento: meioPagamento, observacoes: observacoes, parentesco: parentesco, pessoaHomenageada: pessoaHomenageada, telefoneContato: telefoneContato },
            success: function (data) {
                if (data.Codigo == undefined) {
                    error(data);
                } else {
                    success(data);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                error("Ocorreu um erro ao processar seu pedido. Entre em contato pelo telefone 0800-777-1986 para verificar o status.\n\nErro: " + xhr.status + "\nDescrição: " + thrownError);
            }
        });
    };
}

function _Shopline() {
    this.GerarDC = function (codigoPedido, success, error) {
        $.post('/Shopline/GerarDC', { codigoPedido: codigoPedido }, function (data) {
            if (data == 'ERRO') {
                error();
            } else {
                success(data);
            }
        });
    };
}

function _Carrinho() {

    this.Adicionar = function (descricao, mensagem, observacoes, tamanhoID, success, error) {
        $.post('/Carrinho/Adicionar', { produtoTamanhoID: tamanhoID, descricao: descricao, mensagem: mensagem, observacoes: observacoes }, function (data) {
            if (data == 'OK') {
                success();
                Carrinho.Resumo();
            } else {
                error();
            }
        });
    };

    this.Atualizar = function (itemID, descricao, mensagem, observacoes, callback) {
        $.post('/Carrinho/Atualizar', { itemID: itemID, descricao: descricao, mensagem: mensagem, observacoes: observacoes }, function (data) {
            callback();
            Carrinho.Resumo();
        });
    };

    this.AtualizarMensagem = function (itemID, mensagem) {
        $.post('/Carrinho/AtualizarMensagem', { itemID: itemID, mensagem: mensagem }, {});
    };

    this.Remover = function (itemID, callback) {
        $.post('/Carrinho/Remover', { itemID: itemID }, function (data) {
            callback();
            Carrinho.Resumo();
        });
    };

    this.Resumo = function () {
        $.post('/Carrinho/Resumo', {}, function (data) {
            $('#ResumoCarrinho').replaceWith(data);
            $('#ResumoCarrinho').click(function () {
                Carrinho.Abrir();
            });
        });
    };

    this.Abrir = function () {
        window.location = '/Carrinho';
    };

    this.AplicarCupom = function () {
        $.post('/Cupom/Aplicar', { codigo: $('#cupom').val() }, function (data) {
            if (data == 'OK') {
                Carrinho.Abrir();
            } else {
                alert(data);
            }
        });
    };

    this.RemoverCupom = function () {
        $.post('/Cupom/Remover', {}, function (data) {
            Carrinho.Abrir();
        });
    };

    this.Abrir = function () {
        window.location = '/Carrinho';
    };
}

function _Cliente() {
    this.Entrar = function (email, senha, success, error) {
        if (email.length > 0 && senha.length > 0) {
            $.post('/Cliente/Entrar', { email: email, senha: senha }, function (data) {
                if (data == 'OK') {
                    success();
                } else {
                    error(data);
                }
            });
        } else {
            alert('Digite o e-mail e senha.');
        }
    };

    this.Sair = function (callback) {
        $.post('/Cliente/Sair', {}, function (data) {
            callback();
        });
    };
}

function _Local() {
    this.Cidades = function (estadoID, callback) {
        $.post('/Local/Cidades', { estadoID: estadoID }, function (data) {
            callback(data);
        });
    };
} 