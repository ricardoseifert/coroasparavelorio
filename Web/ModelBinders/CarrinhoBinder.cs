﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;

namespace Web.ModelBinders
{
    public class CarrinhoBinder : IModelBinder
    {
        private const string SESSION_KEY = "_carrinho";
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
                throw new InvalidOperationException("Cannot update instances");

            var carrinho = (Carrinho)controllerContext.HttpContext.Session[SESSION_KEY];

            if (carrinho == null)
            {
                carrinho = new Carrinho();
                controllerContext.HttpContext.Session[SESSION_KEY] = carrinho;
                controllerContext.HttpContext.Session.Timeout = 360;
            }

            return carrinho;
        }
    }
}