﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Routing;

namespace Web.Helpers
{
    public class EnumHelper
    {

        public static string DropDownListFor(Type type, object current, object htmlAttributes, bool ShowIndefinido = true)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<select");
            var attributes = new RouteValueDictionary(htmlAttributes);
            foreach (var key in attributes.Keys)
            {
                builder.Append(" " + key + "=\"" + attributes[key].ToString() + "\"");
            }
            builder.Append(">");

            string[] names = Enum.GetNames(type);
            Array values = Enum.GetValues(type);
            for (int i = 0; i < names.Length && i < values.Length; i++)
            {
                var value = (int)values.GetValue(i);
                var name = names.GetValue(i).ToString();
                var description = Domain.Helpers.EnumHelper.GetDescriptionByName(type, name);

                if (description == "Indefinido" && !ShowIndefinido)
                    continue;

                builder.Append("<option value=\"");
                builder.Append(value);
                builder.Append("\"");
                if (current.ToString() == name)
                {
                    builder.Append(" selected>");
                }
                else
                {
                    builder.Append(">");
                }
                builder.Append(description);
                builder.Append("</option>");
            }

            builder.AppendLine("</select>");

            return builder.ToString();
        }

        public static Dictionary<string, int> List(Type type)
        {
            string[] names = Enum.GetNames(type);
            Array values = Enum.GetValues(type);
            Dictionary<string, int> list = new Dictionary<string, int>();
            for (int i = 0; i < names.Length && i < values.Length; i++)
            {
                var value = (int)values.GetValue(i);
                var name = names.GetValue(i).ToString();
                var description = Domain.Helpers.EnumHelper.GetDescriptionByName(type, name);
                list.Add(description, value);
            }
            return list;
        }

    }
}