﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Areas.ItauBBA.Controllers
{
    [RequireHttps]
    public class HomeController : BaseController
    {

        public HomeController(ObjectContext context) : base(context, Menu.Indefinido)
        {
        }

        public ActionResult Index()
        { 
            return View();
        }

    }
}
