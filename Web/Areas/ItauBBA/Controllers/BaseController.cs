﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Filters;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Models;
using System.Web.Security;

namespace Web.Areas.ItauBBA.Controllers
{

    [AreaAuthorization("Empresas", "/itaubba/login")]
    public class BaseController : Controller
    {

        public enum Menu
        {
            Indefinido = 0,
            NovoPedido = 1,
            Pedidos = 2,
            Faturas = 3,
            Acessos = 4,
            Suporte = 5
        }

        private IPersistentRepository<Colaborador> colaboradorRepository;

        #if !DEBUG
        //[RequireHttps]//apply to all actions in controller
        #endif
        public BaseController(ObjectContext context, Menu menu)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            var colaborador = new Colaborador();

            var loggedUser = LoggedUser.GetFromJSON(System.Web.HttpContext.Current.User.Identity.Name);
            if (loggedUser != null)
            {
                var codigo = Guid.Empty;
                if (Guid.TryParse(loggedUser.Username, out codigo))
                {
                    colaborador = colaboradorRepository.GetByExpression(c => c.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
                    if (colaborador == null)
                    {
                        colaborador = colaboradorRepository.GetByExpression(c => c.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
                        if (colaborador == null)
                        {
                            colaborador = new Colaborador();
                        }
                    }
                }
            }

            ViewBag.Colaborador = colaborador;
            ViewBag.Menu = menu;
        }

    }
}
