﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Web.Security;
using Domain.Service;

namespace Web.Areas.ItauBBA.Controllers
{
    [RequireHttps]
    public class LoginController : Controller
    {

        private IPersistentRepository<Colaborador> colaboradorRepository;

        public LoginController(ObjectContext context)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Esqueci_Minha_Senha()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Enviar_Senha(string email)
        {
            var colaborador = colaboradorRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (colaborador != null)
            {
                var mensagem = "Olá " + colaborador.Nome + ",\n\nEstes são seus dados para acesso:";
                mensagem += "\nLogin: " + colaborador.Login;
                mensagem += "\nSenha: " + colaborador.Senha;

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "COROAS PARA VELORIO", email, email, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "Coroas para Velório", null, "[COROAS PARA VELÓRIO] - Acesso à área de empresas", mensagem, false))
                {
                    ViewBag.Mensagem = "Seus dados de acesso foram enviados para seu email.";
                }
                else
                {
                    ViewBag.Mensagem = "Não foi possível enviar seus dados de acesso. Tente novamente mais tarde.";
                }
            }
            else
            {
                ViewBag.Mensagem = "Nenhum acesso cadastrado com esse endereço de email.";
            }
            return View("Index");
        }

        [HttpPost]
        public ActionResult Entrar(string login, string senha)
        {
            var colaborador = colaboradorRepository.GetByExpression(c => c.Login == login && c.Senha == senha && !c.Removido && c.Empresa.Ativa).FirstOrDefault();
            if (colaborador != null)
            {
                var user = new LoggedUser();
                user.ID = colaborador.ID;
                user.Name = colaborador.Nome;
                user.Username = colaborador.Codigo.ToString();
                user.ExpiresIn = DateTime.Now.AddHours(2);
                user.AcessTime = DateTime.Now;
                user.AditionalInfo = new Dictionary<string, string>();
                user.AuthenticatedAreaName = "Empresas";
                user.AccessGroups.Add("Empresas" + colaborador.Tipo.ToString());

                colaborador.DataUltimoLogin = DateTime.Now;
                colaboradorRepository.Save(colaborador);

                FormsAuthentication.SetAuthCookie(user.ToJSON(), true);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Mensagem = "Login e/ou Senha inválido(s)";
                return View("Index");
            }
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

    }
}
