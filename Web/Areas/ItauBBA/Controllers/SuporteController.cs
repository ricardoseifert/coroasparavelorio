﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Service;

namespace Web.Areas.ItauBBA.Controllers
{
    [RequireHttps]
    public class SuporteController : BaseController
    {

        public SuporteController(ObjectContext context) : base(context, BaseController.Menu.Suporte)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ContentResult Enviar(string assunto, string telefone, string mensagem)
        {
            var colaborador = (Domain.Entities.Colaborador)ViewBag.Colaborador;

            var body = "Empresa: " + colaborador.Empresa.NomeFantasia;
            body += "\nColaborador: " + colaborador.Nome;
            body += "\nEmail: " + colaborador.Email;
            body += "\nAssunto: " + assunto;
            body += "\nTelefone: " + telefone;
            body += "\nMensagem: " + mensagem;

            if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", colaborador.Email, colaborador.Nome, null, "[COROAS PARA VELÓRIO] - Acesso a Empresas", body, false))
            {
                return Content("OK");
            }
            else
            {
                return Content("ERRO");
            }
        }

    }
}
