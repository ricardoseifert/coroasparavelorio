﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Filters;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Service;

namespace Web.Areas.ItauBBA.Controllers
{

    [RequireHttps]
    [AllowGroup("EmpresasMaster", "/empresas")]
    public class FaturasController : BaseController
    {

        private IPersistentRepository<Faturamento> faturamentoRepository;

        public FaturasController(ObjectContext context) : base(context, BaseController.Menu.Faturas)
        {
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
        }

        public ActionResult Index()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var faturas = faturamentoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID).OrderByDescending(p => p.DataVencimento).ToList();
            return View(faturas);
        }

        public ActionResult Detalhes(int id)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.EmpresaID == colaborador.EmpresaID && colaborador.Tipo == Colaborador.Tipos.Master)
                {
                    return View(fatura);
                }
            }
            return View();
        }

        public ContentResult Boleto(int id)
        {
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                var html = BoletoService.Gerar(fatura.Boleto);
                Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                return Content("");
            }
            return Content("Não foi possível gerar o boleto. Tente novamente mais tarde.");
        }
    }
}
