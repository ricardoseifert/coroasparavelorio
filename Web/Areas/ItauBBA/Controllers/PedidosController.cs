﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;

namespace Web.Areas.ItauBBA.Controllers
{
    [RequireHttps]
    public class PedidosController : BaseController
    {

        private IPersistentRepository<PedidoEmpresa> pedidoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Frase> fraseRepository;
        private IPersistentRepository<EmpresaProdutoTamanho> produtosTamanhosRepository;

        public PedidosController(ObjectContext context) : base(context, BaseController.Menu.Pedidos)
        {
            pedidoRepository = new PersistentRepository<PedidoEmpresa>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            fraseRepository = new PersistentRepository<Frase>(context);
            produtosTamanhosRepository = new PersistentRepository<EmpresaProdutoTamanho>(context);
        }

        public ActionResult Index()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var pedidos = pedidoRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID).OrderByDescending(p => p.DataCriacao).ToList();
            if (colaborador.Tipo != Colaborador.Tipos.Master)
            {
                pedidos = pedidos.Where(p => p.ColaboradorID == colaborador.ID).ToList();
            }
            return View(pedidos);
        }

        public ActionResult Novo()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;

            ViewBag.Menu = BaseController.Menu.NovoPedido;
            ViewBag.Relacionamentos = relacionamentoRepository.GetAll().OrderBy(r => r.Nome).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome).ToList();
            ViewBag.Frases = fraseRepository.GetAll().ToList();
            ViewBag.ProdutosTamanhos = produtosTamanhosRepository.GetByExpression(p => p.EmpresaID == colaborador.EmpresaID && p.Disponivel).ToList();
            ViewBag.Empresa = colaborador.Empresa;
            return View();
        }

        public ContentResult Criar(int produto, string pessoaHomenageada, string mensagem, string nomeFuncionario, string parentesco, string departamento, string local, int? estado, int? cidade, string data, string hora, string observacoes, string telefoneContato)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;

            var dataHora = data + " " + hora;
            var dataSolicitada = DateTime.MinValue;
            if (!DateTime.TryParse(dataHora, out dataSolicitada))
            {
                return Content("Data e/ou Hora invalida(s)");
            }

            var produtoTamanho = produtosTamanhosRepository.Get(produto);
            if (produtoTamanho == null)
            {
                return Content("Produto inválido");
            }

            if (produtoTamanho.EmpresaID != colaborador.EmpresaID)
            {
                return Content("Você não pode adquirir esse produto");
            }

            var cidadeid = 8570; //SP
            var estadoid = 26; //SP

            if (cidade.HasValue && estado.HasValue)
            {
                cidadeid = cidade.Value;
                estadoid = estado.Value;
            }
            else if (colaborador.Empresa.CidadeID.HasValue && colaborador.Empresa.EstadoID.HasValue)
            {
                cidadeid = colaborador.Empresa.CidadeID.Value;
                estadoid = colaborador.Empresa.EstadoID.Value;
            }
            
            var pedido = new PedidoEmpresa()
            {
                CidadeID = cidadeid,
                Codigo = Guid.NewGuid(),
                ColaboradorID = colaborador.ID,
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                Departamento = departamento,
                EmpresaID = colaborador.EmpresaID,
                EmpresaProdutoTamanhoID = produto,
                ProdutoTamanhoID = produtoTamanho.ProdutoTamanhoID,
                EstadoID = estadoid,
                Origem = "SITE",
                OrigemForma = "ONLINE",
                OrigemSite = "COROAS PARA VELÓRIOS - EMPRESA",
                LocalEntrega = local,
                Mensagem = mensagem,
                NomeFuncionario = nomeFuncionario,
                Observacoes = observacoes,
                Parentesco = parentesco,
                PessoaHomenageada = pessoaHomenageada,
                Status = PedidoEmpresa.TodosStatus.NovoPedido,
                StatusEntrega = PedidoEmpresa.TodosStatusEntrega.Pendente,
                TelefoneContato = telefoneContato,
                Valor = produtoTamanho.Valor
            };
            pedidoRepository.Save(pedido);
            pedido.EnviarPedidoEmail();

            return Content("OK");
        }

        public ActionResult Detalhes(Guid codigo)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var pedido = pedidoRepository.GetByExpression(p => p.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
            if (pedido != null)
            {
                if (pedido.EmpresaID == colaborador.EmpresaID && (colaborador.Tipo == Colaborador.Tipos.Master || pedido.ColaboradorID == colaborador.ID))
                {
                    return View(pedido);
                }
            }
            return View();
        }

        public ContentResult Solicitar(Guid codigo)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var pedido = pedidoRepository.GetByExpression(p => p.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
            if (pedido != null)
            {
                if (pedido.EmpresaID == colaborador.EmpresaID && (colaborador.Tipo == Colaborador.Tipos.Master || pedido.ColaboradorID == colaborador.ID))
                {
                    var body = "O colaborador " + colaborador.Nome + " da empresa " + colaborador.Empresa.NomeFantasia + " solicitou o faturamento do pedido ID #" + pedido.ID + " criado em " + pedido.DataCriacao.ToString();

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", colaborador.Email, colaborador.Nome, null, "[COROAS PARA VELÓRIO] - Solicitação de Faturamento", body, false);
                    
                    return Content("OK");
                }
            }
            return Content("ERRO");
        }

        public struct Cidade
        {
            public int ID;
            public string Nome;
        }

        public JsonResult Cidades(int estadoID)
        {
            var cidades = new List<Cidade>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null)
            {
                foreach(var _cidade in estado.Cidades.OrderBy(c => c.Nome))
                cidades.Add(new Cidade()
                {
                    ID = _cidade.ID,
                    Nome = _cidade.Nome
                });
            }
            return Json(cidades);
        }

    }
}
