﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Filters;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;

namespace Web.Areas.ItauBBA.Controllers
{

    [RequireHttps]
    public class AcessosController : BaseController
    {

        private IPersistentRepository<Colaborador> colaboradorRepository;

        public AcessosController(ObjectContext context) : base(context, BaseController.Menu.Acessos)
        {
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ActionResult Index()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var colaboradores = colaboradorRepository.GetByExpression(c => c.EmpresaID == colaborador.EmpresaID && !c.Removido).OrderBy(c => c.Nome).ToList();
            return View(colaboradores);
        }

        public ActionResult Meus_Dados()
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            return View(colaborador);
        }

        public ActionResult Alterar_Senha()
        {
            return View();
        }

        public ContentResult Salvar_Senha(string senhaAtual, string senhaNova)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            if (colaborador.Senha == senhaAtual)
            {
                colaborador.Senha = senhaNova;
                colaboradorRepository.Save(colaborador);
                return Content("OK");
            }
            return Content("A senha atual não confere.");
        }

        public ContentResult Enviar_Modificacao(string mensagem)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var body = "O colaborador " + colaborador.Nome + " da empresa " + colaborador.Empresa.NomeFantasia + " solicitou a modificação de dados cadastrais.\n\n" + mensagem;
            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", colaborador.Email, colaborador.Nome, null, "[COROAS PARA VELÓRIO] - Solicitação de Modificação de Dados", body, false);

            return Content("OK");
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ContentResult Excluir(int id)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var excluir = colaboradorRepository.Get(id);
            if (excluir == null)
            {
                return Content("Acesso inválido");
            }
            else if (excluir.EmpresaID != colaborador.EmpresaID)
            {
                return Content("Você pode excluir apenas acesso da sua Empresa");
            }
            else if (excluir.ID == colaborador.ID)
            {
                return Content("Você não pode excluir você mesmo");
            }
            else if (excluir.Tipo == Colaborador.Tipos.Master && excluir.Empresa.Colaboradores.Count(c => c.TipoID == (int)Colaborador.Tipos.Master) <= 1)
            {
                return Content("É necessário ao menos um acesso com o perfil Master");
            }
            excluir.Removido = true;
            colaboradorRepository.Save(excluir);
            return Content("OK");
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ActionResult Modificar(int id)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;
            var modificar = colaboradorRepository.Get(id);
            if (modificar == null)
            {
                return RedirectToAction("Index");
            }
            else if (modificar.EmpresaID != colaborador.EmpresaID)
            {
                return RedirectToAction("Index");
            }
            return View("Editar", modificar);
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        public ActionResult Novo()
        {
            return View("Editar", null);
        }

        [AllowGroup("EmpresasMaster", "/empresas")]
        [HttpPost]
        public ActionResult Salvar(FormCollection form)
        {
            var colaborador = (Colaborador)ViewBag.Colaborador;

            var codigo = Guid.Empty;
            
            Colaborador salvar = null;

            if (Guid.TryParse(form["acesso-codigo"], out codigo))
            {
                salvar = colaboradorRepository.GetByExpression(c => c.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
                if (salvar != null)
                {
                    if (salvar.EmpresaID != colaborador.EmpresaID)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            if (salvar == null)
            {
                salvar = new Colaborador()
                {
                    Codigo = Guid.NewGuid(),
                    DataCadastro = DateTime.Now,
                    EmpresaID = colaborador.EmpresaID
                };
            }
            salvar.Departamento = form["acesso-departamento"];
            salvar.Email = form["acesso-email"];
            salvar.Login = form["acesso-login"];
            salvar.Nome = form["acesso-nome"];
            salvar.Senha = form["acesso-senha"];
            salvar.Telefone = form["acesso-telefone"];
            salvar.TipoID = int.Parse(form["acesso-tipo"]);

            salvar.RecebeEmailPedido = (form["email-pedido"] == "on");
            salvar.RecebeEmailNFe = (form["email-nf"] == "on");

            colaboradorRepository.Save(salvar);

            return RedirectToAction("Index");
        }

    }
}
