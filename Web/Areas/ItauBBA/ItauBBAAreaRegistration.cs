﻿using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using System.Collections.Generic;

namespace Web.Areas.ItauBBA
{
    public class ItauBBAAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ItauBBA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute
            (
                "Empresas_Login_Esqueci_Minha_Senha",
                "itaubba/login/esqueci-minha-senha",
                new { controller = "Login", action = "Esqueci_Minha_Senha", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );

            context.MapRoute
            (
                "Empresas_Login_Enviar_Senha",
                "itaubba/login/enviar-senha",
                new { controller = "Login", action = "Enviar_Senha", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );

            context.MapRoute
            (
                "Empresas_Acessos_Alterar_Senha",
                "itaubba/acessos/alterar-senha",
                new { controller = "Acessos", action = "Alterar_Senha", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );

            context.MapRoute
            (
                "Empresas_Acessos_Salvar_Senha",
                "itaubba/acessos/salvar-senha",
                new { controller = "Acessos", action = "Salvar_Senha", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );

            context.MapRoute
            (
                "Empresas_Acessos_Meus_Dados",
                "itaubba/acessos/meus-dados",
                new { controller = "Acessos", action = "Meus_Dados", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );

            context.MapRoute
            (
                "Empresas_Default",
                "itaubba/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "Web.Areas.ItauBBA.Controllers" }
            );
        }

    }
}
