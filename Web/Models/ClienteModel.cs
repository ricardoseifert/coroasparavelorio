﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Repositories;
using Domain.Entities;
using System.Data.Objects;
using MvcExtensions.Security.Models;

namespace Web.Models
{
    public class ClienteModel
    {
        private ClienteRepository clienteRepository;

        public bool Autenticado { get; set; }
        public Cliente CurrentCliente { get; set; }

        public int ID
        {
            get { return CurrentCliente != null ? CurrentCliente.ID : 0; }
        }

        public string Nome
        {
            get { return CurrentCliente != null ? CurrentCliente.Nome : ""; }
        }

        public ClienteModel(ObjectContext context)
        {
            clienteRepository = new ClienteRepository(context);
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var loggeduser = LoggedUser.GetFromJSON(HttpContext.Current.User.Identity.Name);
                var cliente = clienteRepository.Get(loggeduser.ID);
                if (cliente != null)
                {
                    Autenticado = true;
                    CurrentCliente = cliente;
                }
            }
            else
                Autenticado = false;
        }

    }
}