﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Web.Models;
using Web.Handler;

namespace Web.Controllers
{
    public class LocalController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<Redirect> redirectRepository;

        public struct Resultado
        {
            public string Tipo;
            public List<Local> Locais;
        }

        public struct Local
        {
            public string Nome;
            public string Url;
        }

        public LocalController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            produtoRepository = new ProdutoRepository(context);
            redirectRepository = new PersistentRepository<Redirect>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index(string rotuloUrl)
        {
            var local = localRepository.GetByRotuloUrl(rotuloUrl);
            if (local != null)
            {
                ViewBag.Titulo = local.Titulo + " - Velório, Floricultura, Coroa de Flores";
                ViewBag.Title = local.Titulo + " - Velório, Floricultura, Coroa de Flores";
                ViewBag.Local = local.Cidade.NomeCompleto;
                ViewBag.Descricao = local.DescricaoRodape;
                ViewBag.Description = local.TagDescription;
                ViewBag.Keywords = local.TagKeywords;
                ViewBag.Televendas = local.Televendas;

                ViewBag.Produtos = produtoRepository.Publicados();

                Session["local"] = local;

                ViewBag.LocaisCidade = localRepository.GetByExpression(c => c.CidadeID == local.CidadeID && c.Disponivel && c.ID != local.ID).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();

                return View(local);
            }
            else
            {
                var pagina = "/coroas-de-flores/" + rotuloUrl;
                var redirect = redirectRepository.GetByExpression(c => c.Origem.Contains(pagina)).FirstOrDefault();
                if (redirect == null)
                {

                    Response.StatusCode = 404;
                    Response.TrySkipIisCustomErrors = true;

                    var destaques = produtoRepository.DestaquesHome();
                    ViewBag.Destaques = destaques;
                    ViewBag.Pagina = pagina;

                    return RedirectToAction("Oops", "Error", new { url = pagina});
                }
                else
                {
                    return RedirectPermanent(redirect.Destino);
                }
            }
        }

        [HttpGet]
        public ActionResult Buscar(string termo)
        {
            var resultado = localRepository.Buscar(termo).OrderBy(c => c.Titulo).OrderBy(c=> c.TipoID).ToList();
            if (Request.QueryString["cidadeID"] != null)
            {
                var cidadeID = 0;
                Int32.TryParse(Request.QueryString["cidadeID"], out cidadeID);
                if (cidadeID > 0)
                {
                    resultado = resultado.Where(c => c.CidadeID == cidadeID).ToList();
                }
            }
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_SITE);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_SITE).Take(Domain.Core.Configuracoes.ITENS_PAGINA_SITE).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Destaques = produtoRepository.DestaquesHome(4);
            return View(resultado);
        }

        //[HttpPost]
        //public JsonResult BuscarRapido(string termo)
        //{
        //    var locais = localRepository.Buscar(termo);
        //    if (locais.Any())
        //    {
        //        var tipos = locais.GroupBy(l => l.Tipo);
        //        var resultados = new List<Resultado>();
        //        foreach (var tipo in tipos)
        //        {
        //            var resultado = new Resultado();
        //            resultado.Tipo = Domain.Helpers.EnumHelper.GetDescription(tipo.Key);
        //            resultado.Locais = new List<Local>();
        //            foreach (var local in tipo)
        //            {
        //                resultado.Locais.Add(new Local() { Nome = local.Titulo, Url = Url.RouteUrl("Local", new { rotuloUrl = local.RotuloUrl }) });
        //            }
        //            resultados.Add(resultado);
        //        }
        //        return Json(resultados);
        //    }
        //    else
        //    {
        //        return Json("NENHUM");
        //    }
        //}

        public JsonResult Cidades(int estadoID)
        {
            var cidades = new List<CidadeModel>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null)
            {
                foreach (var cidade in estado.Cidades)
                {
                    cidades.Add(new CidadeModel(cidade));
                }
            }
            return Json(cidades);
        }

    }
}
