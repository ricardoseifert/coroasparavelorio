﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Web.Models;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class Comprar_em_um_CliqueController : BaseController
    {
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;
        private ClienteModel clienteModel;

        public Comprar_em_um_CliqueController(ObjectContext context)
            : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            clienteModel = new ClienteModel(context);
        }

        public ActionResult Index()
        {
            if (clienteModel.Autenticado)
            {
                ViewBag.Estados = estadoRepository.GetAll();
                ViewBag.Produtos = produtoRepository.GetByExpression(p => p.Disponivel).ToList();
                return View(clienteModel.CurrentCliente);
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/comprar-em-um-clique?produtoID=" + Request.QueryString["produtoID"] + "&tamanhoID=" + Request.QueryString["tamanhoID"] });
            }
        }

    }
}
