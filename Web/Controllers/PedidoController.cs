﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Web.Models;
using System.Data.Objects;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Repositories;

namespace Web.Controllers
{
    public class PedidoController : BaseController
    {

        private ClienteModel clienteModel;
        private PedidoService pedidoService;
        private IPersistentRepository<Estado> estadoRepository;
        private PedidoRepository pedidoRepository;

        public PedidoController(ObjectContext context) : base(context)
        {
            clienteModel = new ClienteModel(context);
            pedidoService = new PedidoService(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            pedidoRepository = new PedidoRepository(context);
        }

        
        public ActionResult Index(Carrinho carrinho)
        {
            if (clienteModel.Autenticado)
            {
                ViewBag.Homolog = (Domain.Core.Configuracoes.HOMOLOGACAO && clienteModel.CurrentCliente.Email == Domain.Core.Configuracoes.EMAIL_HOMOLOGACAO);
                //Tirar qdo homologar.

                if (clienteModel.CurrentCliente.DataAtualizacao.HasValue)
                {
                    ViewBag.Cliente = clienteModel.CurrentCliente;
                    ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                    if (carrinho.Vazio)
                    {
                        return RedirectToAction("index", "home");
                    }
                    return View(carrinho);
                }
                else
                {
                    return RedirectToAction("minha-conta", "cliente", new { returnUrl = "/pedido" , atualizar = "sim"});
                }
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/pedido" });
            }
        }

        
        public JsonResult Criar(Carrinho carrinho, int cidadeID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada)
        {
            try
            {
                var pedido = pedidoService.CriarPedido(carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, "", pessoaHomenageada, clienteModel.CurrentCliente.TelefoneContato, "SITE", "COROAS PARA VELÓRIO", "ONLINE");
                if (pedido != null)
                {
                    var model = new PedidoModel(pedido);
                    pedido.EnviarPedidoEmail();
                    carrinho.Limpar();
                    return Json(model);
                }
                else
                {
                    return Json("Não foi possível criar seu pedido. Verifique os dados preenchidos.");
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        
        public JsonResult Pagar(Carrinho carrinho, int bandeira, string nome, string numero, string validade, string codigo, int parcela, int cidadeID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada)
        {
            try
            {
                var data = validade.Split('/');
                var mes = 0;
                var ano = 0;
                if (data.Length < 2)
                {
                    return Json("Validade do cartão inválida");
                }
                else if (!int.TryParse(data[0], out mes) || !int.TryParse(data[1], out ano))
                {
                    return Json("Validade do cartão inválida");
                }
                numero = numero.Replace(".", "").Trim();

                var pedido = pedidoService.PagarPedido(bandeira, nome, numero, mes, ano, codigo, parcela, carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, "", pessoaHomenageada, "");
                if (pedido != null)
                {
                    var model = new PedidoModel(pedido);
                    pedido.EnviarPedidoEmail(false, false);
                    return Json(model);
                }
                else
                {
                    return Json("Não foi possível criar seu pedido. Verifique os dados preenchidos.");
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        
        public ActionResult Confirmacao(Guid? codigo, Carrinho carrinho)
        {
            if (codigo.HasValue)
            {
                var pedido = pedidoRepository.GetByCodigo(codigo.Value);
                if (pedido != null)
                {
                    if (pedido.ClienteID == clienteModel.ID)
                    {
                        return View(pedido);
                    }
                }
                carrinho.Limpar();
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Caminho(int posicao = 0)
        {
            ViewBag.Posicao = posicao;
            return View();
        }
    }
}
