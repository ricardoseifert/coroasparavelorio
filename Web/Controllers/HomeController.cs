﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Filter;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Coloracao> corRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<PrimeiraCompra> primeiraCompraRepository;
        
        public HomeController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            corRepository = new PersistentRepository<Coloracao>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            primeiraCompraRepository = new PersistentRepository<PrimeiraCompra>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        //[RedirectMobile]
        public ActionResult Index()
        { 
            var destaques = produtoRepository.DestaquesHome();
            ViewBag.Depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado && c.Home).Take(2).ToList();
            return View(destaques);
        }

        public ActionResult Sugestoes()
        {
            ViewBag.FaixaPreco = faixaPrecoRepository.GetAll().ToList();
            ViewBag.Tamanho = tamanhoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Relacionamento = relacionamentoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Coloracao = corRepository.GetAll().OrderBy(t => t.Nome).ToList();
            return View();
        }

        public ActionResult Depoimentos()
        {
            var depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado).ToList();
            return View(depoimentos);
        }

        [HttpPost]
        public JsonResult GetCidades(string uf, string selected = "")
        {
            var cidades = cidadeRepository.GetByExpression(c => c.Estado.Sigla.ToUpper().Trim() == uf.ToUpper().Trim()).OrderBy(c => c.Nome);

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Cidade cidade in cidades)
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result);
        }

        public ActionResult PrimeiraCompra()
        { 
            return View();
        }

        public ActionResult PrimeiraCompraForm()
        {
            return View();
        }

        public ActionResult AtendimentoIndisponivel()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PrimeiraCompra(string nome, string telefone, string email)
        {
            var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
            if (nome.Length > 0 && telefone.Length > 0 && email.Length > 0)
            {
                email = email.Trim().ToLower();
                nome = nome.ToUpper();
                var cliente = clienteRepository.GetByExpression(c => c.Email.ToLower().Trim() == email).FirstOrDefault();
                
                bool elegivel = true;
                if (cliente != null)
                    if (cliente.Pedidos.Count > 0)
                        elegivel = false;

                if (elegivel)
                {
                    var primeiracompra = new PrimeiraCompra();
                    if(cliente != null)
                    primeiracompra.ClienteID = cliente.ID;
                    primeiracompra.CupomUtilizado = configuracao.CupomPrimeiraCompra;
                    primeiracompra.Email = email;
                    primeiracompra.Nome = nome;
                    primeiracompra.TelefoneContato = telefone;
                    primeiracompra.DataCadastro = DateTime.Now;
                    primeiraCompraRepository.Save(primeiracompra);

                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cupom.htm"));
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.DOMINIO);
                    html = html.Replace("[NOME]", nome.ToUpper());
                    html = html.Replace("[TELEFONE]", telefone);
                    html = html.Replace("[EMAIL]", email.ToLower());
                    html = html.Replace("[CUPOM]", configuracao.CupomPrimeiraCompra);

                    Response.Cookies["primeiracompra"].Value = "nao";
                    Response.Cookies["primeiracompra"].Expires = DateTime.Now.AddDays(30);

                    if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", email, nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_COPIA, "[COROAS PARA VELÓRIO] - Seu cupom de desconto para sua primeira compra!", html, true))
                        return Json(new { status = "OK", mensagem = "Seu cupom foi enviado com sucesso para seu e-mail. Verifique sua caixa de entrada para utilização." });
                    else
                        return Json(new { status = "ERRO", mensagem = "Não foi possível enviar seu cupom. Anote o código para utilização na sua primeira compra : " + configuracao.CupomPrimeiraCompra });
                }
                else
                {
                    return Json(new { status = "ERRO", mensagem = "Esta não é a primeira compra para este e-mail. Esta promoção é válida somente para novos clientes." });
                }
            }
            else
                return Json(new { status = "ERRO", mensagem = "Digite todos os seus dados." });
        }

        [HttpPost]
        public JsonResult FechaPrimeiraCompra()
        {
            Response.Cookies["primeiracompra"].Value = "nao";
            Response.Cookies["primeiracompra"].Expires = DateTime.Now.AddDays(5);
            return Json("OK");
        }

        [HttpPost]
        public JsonResult GetCEP(string cep)
        {
            var resultado = new CEP.CEP(cep);
            return Json(new { resultado = resultado.Resultado, resultado_txt = resultado.ResultadoTXT, uf = resultado.UF, cidade = resultado.Cidade, bairro = resultado.Bairro, tipo_logradouro = resultado.TipoLogradouro, logradouro = resultado.Logradouro });
        }
    }
}
