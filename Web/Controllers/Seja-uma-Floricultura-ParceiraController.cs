﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;

namespace Web.Controllers
{
    public class Seja_uma_Floricultura_ParceiraController : BaseController
    {
        public Seja_uma_Floricultura_ParceiraController(ObjectContext context) : base(context)
        {
    
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var razao = form["razao"].ToUpper();
                var nome = form["nome"].ToUpper();
                var cnpj = form["cnpj"];
                var contato = form["contato"].ToUpper();
                var tel1 = form["tel1"];
                var tel2 = form["tel2"];
                var email = form["email"].ToLower();
                var cep = form["cep"];
                var endereco = form["endereco"].ToUpper();
                var bairro = form["bairro"].ToUpper();
                var cidade = form["cidade"].ToUpper();
                var uf = form["uf"].ToUpper();
                var celular = form["celular"];
                var nextel = form["nextel"];
                var site = form["site"].ToLower();
                var horario = form["horario"];
                var conheceu = form["conheceu"].ToLower();

                //ENVIAR EMAIL
                var corpo = string.Format("O seguinte usuário entrou em contato para ser uma floricultura parceira:\n\nRAZÃO SOCIAL: {0}\n\nNOME DA FLORICULTURA: {1}\n\nCNPJ: {2}\n\nCONTATO: {3}\n\nTEL1: {4}\n\nTEL2: {5}\n\nEMAIL: {6}\n\nCONTATO: {7}\n\nCEP: {8}\n\nENDEREÇO: {9}\n\nBAIRRO: {10}\n\nCIDADE: {11}\n\nUF: {12}\n\nCELULAR: {13}\n\nNEXTEL: {14}\n\nSITE: {15}\n\nHORÁRIO: {16}\n\nCOMO CONHECEU: {17}"
                    , razao, nome, cnpj, contato, tel1, tel2, email, contato, cep, endereco, bairro, cidade, uf, celular, nextel, site, horario, conheceu);

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[COROAS PARA VELÓRIO] - Seja uma floricultura parceira", corpo, false))
                    return Json(new { status = "OK", mensagem = "Sua solicitação foi enviada com sucesso! Por favor, aguarde nosso retorno." });
                else
                    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua solicitação, tente novamente mais tarde ou entre em contato através de nossos telefones." });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }
    }
}
