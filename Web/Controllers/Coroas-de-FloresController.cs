﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
    public class Coroas_de_FloresController : BaseController
    {

        private ProdutoRepository produtoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Coloracao> coloracaoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;

        public Coroas_de_FloresController(ObjectContext context) : base(context)
        {
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            coloracaoRepository = new PersistentRepository<Coloracao>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            var produtos = produtoRepository.GetByTipo(Domain.Entities.Produto.Tipos.CoroaFlor);
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Sugestoes()
        {
            int faixaPreco = 0;
            int relacionamento = 0;
            int coloracao = 0;
            int? faixaPrecoID = null;
            int? relacionamentoID = null;
            int? coloracaoID = null;
            Domain.Entities.Produto.Sexos sexo = Domain.Entities.Produto.Sexos.Ambos;

            ViewBag.TermoBusca = "<b>homens</b> e <b>mulheres</b>";
            

            if (!String.IsNullOrEmpty(Request.QueryString["sexo"]))
            {
                var sexos = Request.QueryString["sexo"];
                if (!sexos.Contains("M") || !sexos.Contains("F"))
                {
                    if (sexos.Contains("M"))
                    {
                        ViewBag.TermoBusca = "<b>homens</b>";
                        sexo = Domain.Entities.Produto.Sexos.Masculino;
                    }
                    else if (sexos.Contains("F"))
                    {
                        ViewBag.TermoBusca = "<b>mulheres</b>";
                        sexo = Domain.Entities.Produto.Sexos.Feminino;
                    }
                }
            }
            
            if (!String.IsNullOrEmpty(Request.QueryString["faixa-de-preco"]))
            {
                if (int.TryParse(Request.QueryString["faixa-de-preco"], out faixaPreco))
                {
                    var f = faixaPrecoRepository.Get(faixaPreco);
                    if (f != null)
                    {
                        ViewBag.TermoBusca += ", com valores variando de <b>" + f.PrecoInicio.ToString("C2") + "</b> até <b>" + f.PrecoFim.ToString("C2") + "</b>";
                    }
                    faixaPrecoID = faixaPreco;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["relacionamento"]))
            {
                if (int.TryParse(Request.QueryString["relacionamento"], out relacionamento))
                {
                    var r = relacionamentoRepository.Get(relacionamento);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += ", indicado para um <b>" + r.Nome.ToLower() + "</b>";
                    }
                    relacionamentoID = relacionamento;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["coloracao"]))
            {
                if (int.TryParse(Request.QueryString["coloracao"], out coloracao))
                {
                    var r = coloracaoRepository.Get(coloracao);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += " com flores <b>" + r.Nome.ToLower() + "</b>";
                    }
                    coloracaoID = coloracao;
                }
            }
            var sugestoes = produtoRepository.Sugestoes(faixaPrecoID, relacionamentoID, coloracaoID, sexo);
            if (sugestoes.Count() == 0)
            {
                ViewBag.NaoEncontrado = produtoRepository.Sugestoes(null, null, coloracaoID, sexo).OrderBy(c => Guid.NewGuid()).Take(4).ToList();
            }
            ViewBag.Sugestoes = true;
            return View(sugestoes);
        }

    }
}
