﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class Noticias_de_MercadoController : BaseController
    {
        private IPersistentRepository<Noticia> noticiaRepository;

        public Noticias_de_MercadoController(ObjectContext context) : base(context)
        {
            noticiaRepository = new PersistentRepository<Noticia>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            var resultado = noticiaRepository.GetByExpression(n => n.Publicado && n.TipoID == (int)Domain.Entities.Noticia.Tipo.MERCADO).OrderByDescending(n => n.Data).ToList();
            
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_SITE);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_SITE).Take(Domain.Core.Configuracoes.ITENS_PAGINA_SITE).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Detalhes(int id)
        {
            var noticia = noticiaRepository.GetByExpression(n => n.Publicado && n.ID == id && n.TipoID == (int)Domain.Entities.Noticia.Tipo.MERCADO).FirstOrDefault();
            if (noticia == null)
                return RedirectToAction("Index");
            ViewBag.Title = noticia.Titulo + " - Notícias de Mercado";
            ViewBag.Description = noticia.TagDescription;
            ViewBag.KeyWords = noticia.TagKeywords;
            return View(noticia);
        }
    }
}
