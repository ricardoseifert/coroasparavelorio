﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Web.Filter;

namespace Web.Controllers
{
    public class A_EmpresaController : BaseController
    {
        private IPersistentRepository<Depoimento> depoimentoRepository;

        public A_EmpresaController(ObjectContext context)
            : base(context)
        {
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        //[RedirectMobile]
        public ActionResult Index()
        {
            ViewBag.DepoimentosPF = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PF).OrderByDescending(d => d.Data).Take(1).ToList();
            ViewBag.DepoimentosPJ = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PJ).OrderByDescending(d => d.Data).Take(1).ToList();
            return View();
        }

    }
}
