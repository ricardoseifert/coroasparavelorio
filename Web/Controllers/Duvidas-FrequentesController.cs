﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class Duvidas_FrequentesController : BaseController
    {
        private IPersistentRepository<Duvida> duvidasRepository;

        public Duvidas_FrequentesController(ObjectContext context)
            : base(context)
        {
            duvidasRepository = new PersistentRepository<Duvida>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Categoria(int tipoID)
        {
            var duvidas = duvidasRepository.GetByExpression(n => n.Publicado && n.TipoID == tipoID).OrderBy(n => n.Titulo).ToList();
            ViewBag.Categoria = Domain.Helpers.EnumHelper.GetDescription((Domain.Entities.Duvida.Tipos)tipoID);
            return View(duvidas);
        }
    }
}
