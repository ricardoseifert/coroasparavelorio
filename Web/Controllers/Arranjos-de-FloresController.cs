﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;

namespace Web.Controllers
{
    public class Arranjos_de_FloresController : BaseController
    {

        private ProdutoRepository produtoRepository;

        public Arranjos_de_FloresController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            var produtos = produtoRepository.GetByTipo(Domain.Entities.Produto.Tipos.ArranjoFlor);
            return View(produtos);
        }

    }
}
