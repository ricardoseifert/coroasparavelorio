﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Service;
using Domain.Repositories;
using System.Data.Objects;

namespace Web.Controllers
{
    public class ShoplineController : Controller
    {

        private ShoplineService shoplineService;
        private PedidoRepository pedidoRepository;

        public ShoplineController(ObjectContext context)
        {
            shoplineService = new ShoplineService();
            pedidoRepository = new PedidoRepository(context);
        }

        public ActionResult Retorno(string DC)
        {
            var pedido = shoplineService.Retorno(DC);
            return RedirectToAction("confirmacao", "pedido", new { codigo = pedido.Codigo });
        }

        public ContentResult GerarDC(Guid codigoPedido)
        {
            var pedido = pedidoRepository.GetByCodigo(codigoPedido);
            if (pedido != null)
            {
                var dc = shoplineService.GerarDC(pedido);
                return Content(dc);
            }
            return Content("");
        }

        public ActionResult Pedido(string pedido)
        {
            //Consulta e atualiza pedido
            return View(ShoplineService.AtualizaSituacaoPagamento(pedido));
        }
    }
}
