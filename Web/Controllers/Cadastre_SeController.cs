﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using MvcExtensions.Security.Models;
using System.Web.Security;
using Web.Models;

namespace Web.Controllers
{
    public class Cadastre_SeController : BaseController
    {

        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private ClienteModel clienteModel;

        public Cadastre_SeController(ObjectContext context) : base(context)
        {
            estadoRepository = new PersistentRepository<Estado>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            clienteModel = new ClienteModel(context);
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (!clienteModel.Autenticado)
            {
                ViewBag.Estados = estadoRepository.GetAll();
                ViewBag.Redirect = Request.QueryString["returnUrl"];
                return View();
            }
            else
                return RedirectToAction("minha-conta", "cliente");
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            var tipo = form["cadastro-tipo"] == "1" ? Cliente.Tipos.PessoaFisica : Cliente.Tipos.PessoaJuridica;
            var prefixo = tipo == Cliente.Tipos.PessoaFisica ? "fisica" : "juridica";
            var exterior = form["cadastro-tipo-exterior"] == "1";
            var cliente = new Cliente();

            if (exterior)
            {
                prefixo = "exterior";
                cliente = new Cliente()
                {
                    ComoConheceu = form["cadastro-" + prefixo + "-como-conheceu"],
                    DataCadastro = DateTime.Now,
                    Documento = "",
                    Email = form["cadastro-" + prefixo + "-email"],
                    EmailSecundario = form["cadastro-" + prefixo + "-email-secundario"],
                    Skype = form["cadastro-" + prefixo + "-skype"],
                    Bairro = "",
                    CEP = "",
                    Complemento = "",
                    Logradouro = "",
                    Numero = "",
                    Nome = tipo == Cliente.Tipos.PessoaFisica ? form["cadastro-" + prefixo + "-nome"] : form["cadastro-" + prefixo + "-pessoa-contato"],
                    Senha = form["cadastro-" + prefixo + "-senha"],
                    TelefoneContato = form["cadastro-" + prefixo + "-telefone"],
                    PessoaContato = form["cadastro-" + prefixo + "-pessoacontato"],
                    RazaoSocial ="",
                    InscricaoEstadual = "",
                    Tipo = tipo,
                    Exterior = true
                };
            }
            else
            {
                var cidadeID = 0; //SP
                Int32.TryParse(form["cadastro-" + prefixo + "-cidade"].Replace(",",""), out cidadeID);

                var estadoID = 0; //SP
                Int32.TryParse(form["cadastro-" + prefixo + "-estado"].Replace(",", ""), out estadoID);

                var cpf = form["cadastro-" + prefixo + "-cpf"];
                if (cpf == null)
                    cpf = "";
                else
                    cpf = cpf.Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                var cnpj = form["cadastro-" + prefixo + "-cnpj"];
                if (cnpj == null)
                    cnpj = "";
                else
                    cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim();

                var ie = form["cadastro-" + prefixo + "-inscricao-estadual"];
                if (ie == null)
                    ie = "";
                var nome = form["cadastro-" + prefixo + "-nome"];
                if (nome == null)
                    nome = "";
                var contato = form["cadastro-" + prefixo + "-pessoa-contato"];
                if (contato == null)
                    contato = "";
                var razaosocial = form["cadastro-" + prefixo + "-razao-social"];
                if (razaosocial == null)
                    razaosocial = "";

                var celular = form["cadastro-" + prefixo + "-celular"];
                if (celular == null)
                    celular = "";
                var comoconheceu = form["cadastro-" + prefixo + "-como-conheceu"];
                if (comoconheceu == null)
                    comoconheceu = "";
                var email = form["cadastro-" + prefixo + "-email"];
                if (email == null)
                    email = "";
                var emailsecundario = form["cadastro-" + prefixo + "-email-secundario"];
                if (emailsecundario == null)
                    emailsecundario = "";
                var bairro = form["cadastro-" + prefixo + "-bairro"];
                if (bairro == null)
                    bairro = "";
                var cep = form["cadastro-" + prefixo + "-cep"];
                if (cep == null)
                    cep = "";
                var complemento = form["cadastro-" + prefixo + "-complemento"];
                if (complemento == null)
                    complemento = "";
                var logradouro = form["cadastro-" + prefixo + "-logradouro"];
                if (logradouro == null)
                    logradouro = "";
                var numero = form["cadastro-" + prefixo + "-numero"];
                if (numero == null)
                    numero = "";
                var senha = form["cadastro-" + prefixo + "-senha"];
                if (senha == null)
                    senha = "";
                var telefone = form["cadastro-" + prefixo + "-telefone"];
                if (telefone == null)
                    telefone = "";

                cliente = new Cliente()
                {
                    CelularContato = celular,
                    ComoConheceu = comoconheceu,
                    DataCadastro = DateTime.Now,
                    Documento = tipo == Cliente.Tipos.PessoaFisica ? cpf : cnpj,
                    Email = email,
                    EmailSecundario = emailsecundario,
                    Bairro = bairro,
                    CEP = cep.Replace("-", ""),
                    Complemento = complemento,
                    Logradouro = logradouro,
                    Numero = numero,
                    InscricaoEstadual = tipo == Cliente.Tipos.PessoaFisica ? "" : ie,
                    Nome = tipo == Cliente.Tipos.PessoaFisica ? nome.ToUpper().Trim() : contato.ToUpper().Trim(),
                    RazaoSocial = tipo == Cliente.Tipos.PessoaFisica ? "" : razaosocial,
                    Senha = senha,
                    TelefoneContato = telefone,
                    Tipo = tipo
                };

                if (cidadeID > 0)
                    cliente.CidadeID = cidadeID;

                if (estadoID > 0)
                    cliente.EstadoID = estadoID;
            }

            cliente.DataAtualizacao = DateTime.Now;
            cliente.DataUltimoLogin = DateTime.Now;
            clienteRepository.Save(cliente);

            var user = new LoggedUser();
            user.ID = cliente.ID;
            user.Name = cliente.Nome;
            user.Username = cliente.Email;
            user.ExpiresIn = DateTime.Now.AddHours(2);
            user.AcessTime = DateTime.Now;
            user.AuthenticatedAreaName = "Cliente";
            user.AccessGroups.Add("Cliente");

            FormsAuthentication.SetAuthCookie(user.ToJSON(), false);

            var returnUrl = form["cadastro-redirect"];
            if(!String.IsNullOrEmpty(returnUrl) )
                return Redirect(returnUrl);
            else
                return RedirectToAction("Index", "cadastre-se", new { msg = "sucesso" });
        }

        public ContentResult ValidarFisicaExterior(string email)
        {
            var cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

        public ContentResult ValidarFisica(string email, string cpf)
        {
            if (!Domain.Core.Funcoes.Valida_CPF(cpf))
            {
                return Content("CPF Inválido");
            }

            var cliente = clienteRepository.GetByExpression(c => c.Documento == cpf).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse CPF");
            }

            cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

        public ContentResult ValidarJuridica(string email, string cnpj)
        {
            if (!Domain.Core.Funcoes.Valida_CNPJ(cnpj))
            {
                return Content("CNPJ inválido");
            }

            var cliente = clienteRepository.GetByExpression(c => c.Documento == cnpj).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse CNPJ");
            }

            cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

    }
}
