﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Net;

namespace Web.Controllers
{
    //[RequireHttps]
    public class BaseController : Controller
    {

        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Promocao> promocaoRepository;


        public BaseController(ObjectContext context)
        {
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            promocaoRepository = new PersistentRepository<Promocao>(context);
            var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.Configuracoes = configuracao;
            ViewBag.PromocaoSemana = promocao;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                var targetSite = filterContext.Exception.TargetSite;
                if (targetSite.DeclaringType != null)
                    if (targetSite.DeclaringType.FullName == typeof(ActionDescriptor).FullName)
                        if (targetSite.Name == "ExtractParameterFromDictionary")  // Note: may be changed in future MVC versions
                        {
                            filterContext.ExceptionHandled = true;
                            filterContext.Result = new HttpStatusCodeResult((int)HttpStatusCode.BadRequest);
                            return;
                        }
            }
        }

    }
}
