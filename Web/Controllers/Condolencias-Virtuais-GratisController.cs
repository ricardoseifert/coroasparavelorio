﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Controllers
{
    public class Condolencias_Virtuais_GratisController : BaseController
    {

        public Condolencias_Virtuais_GratisController(ObjectContext context) : base(context)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Enviar(string emailDe, string nomeDe, string emailPara, string nomePara, string mensagem, string tipo)
        {
            if (emailDe.Length > 0 && nomeDe.Length > 0 && emailPara.Length > 0 && nomePara.Length > 0 && mensagem.Length > 0)
            {
                //ENVIAR EMAIL
                var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/condolencias/"+tipo+".htm"));
                html = html.Replace("[URL]", Domain.Core.Configuracoes.DOMINIO);
                html = html.Replace("[NOMEDE]", nomeDe);
                html = html.Replace("[NOMEPARA]", nomePara);
                html = html.Replace("[MENSAGEM]", mensagem);

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", emailPara, nomePara, emailDe, nomeDe, "", nomeDe.ToUpper() + " lhe enviou um cartão de condolências!", html, true))
                    return Json("Suas condolências foram enviadas com sucesso!");
                else
                    return Json("Não foi possível enviar suas condolências, tente novamente mais tarde.");
            }
            return Json("Não foi possível enviar suas condolências, tente novamente mais tarde.");
        }

    }
}
