﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class CarrinhoController : BaseController
    {

        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Desconto> descontoRepository;

        public CarrinhoController(ObjectContext context) : base(context)
        {
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            descontoRepository = new PersistentRepository<Desconto>(context);
        }

        public ActionResult Index(Carrinho carrinho)
        {
            if (carrinho.Vazio)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Desconto = descontoRepository.GetAll().FirstOrDefault();
            return View(carrinho);
        }

        public ActionResult Resumo(Carrinho carrinho)
        {
            return View(carrinho);
        }

        public ActionResult ResumoNovo(Carrinho carrinho)
        {
            return View("ResumoNovo", carrinho);
        }

        public ContentResult Adicionar(Carrinho carrinho, int produtoTamanhoID, string descricao, string mensagem, string observacoes)
        {
            var produtoTamanho = produtoTamanhoRepository.Get(produtoTamanhoID);
            if (produtoTamanho != null)
            {
                carrinho.Adicionar(produtoTamanho, descricao, mensagem, observacoes);
                return Content("OK");
            }
            return Content("ERRO");
        }

        public ContentResult Atualizar(Carrinho carrinho, int itemID, string descricao, string mensagem, string observacoes)
        {
            carrinho.Atualizar(itemID, descricao, mensagem, observacoes);
            return Content("OK");
        }

        public ContentResult AtualizarMensagem(Carrinho carrinho, int itemID, string mensagem)
        {
            carrinho.Atualizar(itemID, mensagem);
            return Content("OK");
        }

        public ContentResult Remover(Carrinho carrinho, int itemID)
        {
            carrinho.Remover(itemID);
            return Content("OK");
        }

        public ActionResult Limpar(Carrinho carrinho)
        {
            carrinho.Limpar();
            return RedirectToAction("Index", "Home");
        }
    }
}
