﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Controllers
{
    public class Como_ComprarController : BaseController
    {

        public Como_ComprarController(ObjectContext context) : base(context)
        {
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            return View();
        }

    }
}
