﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class Velorios_E_CemiteriosController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;

        public Velorios_E_CemiteriosController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            var locais = new List<Local>();
            locais.AddRange(localRepository.GetByTipo(Local.Tipos.Cidade));
            foreach (var local in locais)
                local.TotalLocaisRelacionados = localRepository.TotalLocaisRelacionados(local);

            var destaques = new List<Local>();
            destaques.AddRange(localRepository.GetByTipo(Local.Tipos.Cemiterio));
            destaques.AddRange(localRepository.GetByTipo(Local.Tipos.Velorio));
            destaques.AddRange(localRepository.GetByTipo(Local.Tipos.Capela));
            destaques.AddRange(localRepository.GetByTipo(Local.Tipos.Hospital));
            destaques.AddRange(localRepository.GetByTipo(Local.Tipos.Crematorio));

            ViewBag.Destaques = destaques.Where(c => c.Destaque).OrderBy(c => c.Titulo).Take(20).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome).ToList();
            return View(locais.Where(c => c.TotalLocaisRelacionados > 0).ToList());
        }

    }
}
