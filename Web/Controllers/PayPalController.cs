﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Service;

namespace Web.Controllers
{
    public class PayPalController : Controller
    {
        
        private PayPalService paypalService;

        public PayPalController(ObjectContext context)
        {
            paypalService = new PayPalService(context);
        }

        public ActionResult Retorno()
        {
            var codigo = Request["cm"];
            var tx = Request["tx"]; 
            paypalService.AtualizarPagamento(codigo, tx);

             
            //var querystring = "";
            //foreach (string key in Request.QueryString.Keys)
            //{
            //    querystring += key + " = " + Request.QueryString[key] + "<br>";
            //}
            //var form = "";
            //foreach (string key in Request.Form.Keys)
            //{
            //    form += key + " = " + Request.Form[key] + "<br>";
            //}
            //var log_message = string.Format("O seguinte post de PayPal ocorreu no site:\n\n<b>Controler</b>: {0}\n\n<b>Action</b>: {1}\n\n<b>Url</b>: {2}\n\n<b>User</b>: {3}\n\n<b>Querystring</b>:<br> {4}\n\n<b>Form</b>:<br> {5}\n\n<b>Exception</b>:<br> {6}\n\n<b>Stack Trace</b>:<br> {7}", "", "", System.Web.HttpContext.Current.Request.Url, "", querystring, form, "", "");
            //try
            //{
            //    if (Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO.Length > 0)
            //    {
            //        Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Não Responder", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO, "", "Coroas para Velório", "[COROAS PARA VELORIO] - POST DE PAYPAL NO SITE", log_message.Replace("\n", "<br>"), true);
            //    }
            //}
            //catch { }
            return Content("");
        }
    }
}
