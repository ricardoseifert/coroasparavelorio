﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Web.Handler;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<Redirect> redirectRepository;

        public ErrorController(ObjectContext context): base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            redirectRepository = new PersistentRepository<Redirect>(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Oops(string url)
        {
            var originalUri = url ?? Request.QueryString["aspxerrorpath"] ?? Request.Url.OriginalString;
            
            var controllerName = (string)RouteData.Values["controller"];
            var actionName = (string)RouteData.Values["action"];
            var model = new _404(new HttpException(404, "Página não encontrada"), controllerName, actionName)
            {
                RequestedUrl = originalUri.IndexOf("?") > 0?originalUri.Substring(0, originalUri.IndexOf("?")):originalUri,
                ReferrerUrl = Request.UrlReferrer == null ? "" : Request.UrlReferrer.OriginalString
            };
             
            var pagina = model.RequestedUrl.Replace(Domain.Core.Configuracoes.DOMINIO, "").Replace(":80", "").Replace(":8080", "").Replace(":443", "").Trim().ToLower();
            
            //Verificar para não mandar outros além da url recebida

            var redirect = redirectRepository.GetByExpression(c => c.Origem.Contains(pagina)).FirstOrDefault();
            if (redirect == null)
            {

                Response.StatusCode = 404;
                Response.TrySkipIisCustomErrors = true;

                var destaques = produtoRepository.DestaquesHome();
                ViewBag.Destaques = destaques;
                ViewBag.Pagina = pagina;

                return View("Oops", model);
            }
            else
            {
                return RedirectPermanent(redirect.Destino);
            }
        }

    }
}
