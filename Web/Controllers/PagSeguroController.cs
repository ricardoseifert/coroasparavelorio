﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Service;

namespace Web.Controllers
{
    public class PagSeguroController : Controller
    {

        private PagSeguroService pagseguroService;

        public PagSeguroController(ObjectContext context)
        {
            pagseguroService = new PagSeguroService(context);
        }

        public ActionResult Retorno(string transaction_id)
        {
            var codigo = pagseguroService.RetornoPagamento(transaction_id);

            return RedirectToAction("confirmacao", "pedido", new { codigo = codigo});
        }

        public ActionResult IPN()
        {
            string notificationType = Request.Form["notificationType"];
            string notificationCode = Request.Form["notificationCode"];

            if (notificationType == "transaction")
                pagseguroService.NotificarPagamento(notificationCode);

            return Content("");
        }
    }
}
