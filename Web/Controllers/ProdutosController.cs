﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Entities;

namespace Web.Controllers
{
    public class ProdutosController : BaseController
    {
        public ProdutoRepository produtoRepository;

        public ProdutosController(ObjectContext context): base(context)
        {
            produtoRepository = new ProdutoRepository(context);
        }

        public ActionResult Produtos(List<Produto> produtos)
        {
            return View(produtos);
        }

        public ActionResult VejaTambem(List<Produto> produtos)
        {
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        //[CompressFilter(Order = 1)]
        public ActionResult Detalhes(int id)
        {
            var produto = produtoRepository.Detalhes(id);
            if (produto == null)
                return RedirectToAction("Index", "Home");

            ViewBag.ProdutosVejaTambem = produtoRepository.VejaTambem(id, 4);
            return View(produto);
        }

    }
}
