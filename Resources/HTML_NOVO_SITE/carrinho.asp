<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Carrinho de compra</div>
</div>
</div>

<div id="Carrinho">
<div class="Div">

	<div class="TituloPagina">Meu carrinho</div>
	<div class="FinalizarCompra"><img src="imagens/finalizar_compra.png" class="FinalizarCompraImg" alt="Finalizar compra" /></div>

	<div class="Linha1">
		<div class="Linha1Coluna1">Produto</div>
		<div class="Linha1Coluna2">Tamanho</div>
		<div class="Linha1Coluna2">Valor</div>
		<div class="Linha1Coluna4">Remover</div>
	</div>

	<div class="Degrade">

		<div class="LinhaProdutos">
			<div class="LinhaProdutosColuna1">
				<div class="LinhaProdutosFoto"><p><img src="imagens/carrinho_foto_produto.png" class="LinhaProdutosFotoImg" alt="Foto do produto" /></p></div>
				<div class="LinhaProdutosNome"><p>Coroa de Flores Premium - 03</p></div>
			</div>
			<div class="LinhaProdutosColuna2"><p>M�dia</p></div>
			<div class="LinhaProdutosColuna3"><p>R$ 250,00</p></div>
			<div class="LinhaProdutosColuna4"><p><img src="imagens/carrinho_excluir.png" class="LinhaProdutosExcluir" alt="Excluir produto" /></p></div>
		</div>

		<div class="LinhaSubtotal">
			<div class="LinhaSubtotalColuna1">&nbsp;</div>
			<div class="LinhaSubtotalColuna2"><p>SUBTOTAL</p></div>
			<div class="LinhaSubtotalColuna3"><p>R$ 250,00</p></div>
			<div class="LinhaSubtotalColuna4">&nbsp;</div>
		</div>

		<div class="LinhaSubtotal">
			<div class="LinhaSubtotalColuna1">&nbsp;</div>
			<div class="LinhaSubtotalColuna2"><p>FRETE</p></div>
			<div class="LinhaSubtotalColuna3"><p>GR�TIS</p></div>
			<div class="LinhaSubtotalColuna4">&nbsp;</div>
		</div>

		<div class="LinhaDesconto">
			<div class="LinhaDescontoColuna1">
				<div class="LinhaDescontoCupom"><p>Cupom de desconto</p></div>
				<div class="LinhaDescontoCupom3">
				<div class="LinhaDescontoCupom2"><p><input type="text" name="numero_cupom" class="LinhaDescontoCupomInput" /></p></div>
				<div class="LinhaDescontoCupom2"><p><input type="button" value="Aplicar" class="LinhaDescontoCupomAplicar" /></p></div>
				</div>
			</div>
			<div class="LinhaDescontoColuna2"><p>DESCONTO</p></div>
			<div class="LinhaDescontoColuna3"><p>R$ 0,00</p></div>
			<div class="LinhaDescontoColuna4">&nbsp;</div>
		</div>

		<div class="LinhaTotal">
			<div class="LinhaTotalColuna1">&nbsp;</div>
			<div class="LinhaTotalColuna2"><p>TOTAL</p></div>
			<div class="LinhaTotalColuna2"><p>R$ 250,00</p></div>
			<div class="LinhaTotalColuna4">&nbsp;</div>
		</div>

	</div>

	<div class="LinhaBotoes">

		<div class="LinhaBotoesAproveite">
		<div class="LinhaBotoesAproveite1"><b>APROVEITE!</b><br />Adicione outra coroa com 10% de desconto a partir da 2�! *</div>
		<div class="LinhaBotoesAproveite2"><img src="imagens/seta_aproveite.png" class="LinhaBotoesAproveiteSeta" alt="Seta" /></div>
		</div>

		<div class="LinhaBotoes2"><img src="imagens/finalizar_compra.png" class="LinhaBotoesFinalizar" alt="Finalizar compra" /></div>
		<div class="LinhaBotoes1"><img src="imagens/adicionar_outra_coroa.png" class="LinhaBotoesAdicionar" alt="Adicionar outra coroa" /></div>

	</div>

	<div class="LinhaTextoDesconto">* O desconto de itens na promo��o da semana n�o s�o cumulativos e s�o priorit�rios aos demais descontos do site.</div>

</div>
</div>

<div id="Carrinho2">
<div class="Div">

	<div class="TituloPagina">Meu carrinho</div>
	<div class="FinalizarCompra"><img src="imagens/finalizar_compra.png" class="FinalizarCompraImg" alt="Finalizar compra" /></div>

	<div class="Linha1">
		<div class="Linha1Coluna1">Produto</div>
	</div>

	<div class="Degrade">

		<div class="LinhaProdutos">
			<div class="LinhaProdutosFoto"><img src="imagens/carrinho_foto_produto.png" class="LinhaProdutosFotoImg" alt="Foto do produto" /></div>
			<div class="LinhaProdutosDados">
				<div class="LinhaProdutosNome">Coroa de Flores Premium - 03</div>
				<div class="LinhaProdutosTamanho">M�dia 1,2 m x 1,0 m</div>
				<div class="LinhaProdutosValor">R$ 250,00</div>
				<div class="LinhaProdutosExcluir">
					<div class="LinhaProdutosExcluir1"><img src="imagens/carrinho_excluir.png" class="LinhaProdutosExcluirImg" alt="Excluir produto" /></div>
					<div class="LinhaProdutosExcluir2">Excluir produto</div>
				</div>
			</div>
		</div>

		<div class="LinhaCupom">
			<div class="LinhaCupomTexto"><p>Cupom de desconto</p></div>
			<div class="LinhaCupomDiv1">
				<div class="LinhaCupomDiv2"><p><input type="text" name="numero_cupom" class="LinhaCupomInput" /></p></div>
				<div class="LinhaCupomDiv1"><p><input type="button" value="Aplicar" class="LinhaCupomAplicar" /></p></div>
			</div>
		</div>

		<div class="LinhaSubtotal">
			<div class="LinhaSubtotalColuna1"><p>SUBTOTAL:</p></div>
			<div class="LinhaSubtotalColuna2"><p>R$ 250,00</p></div>
		</div>

		<div class="LinhaSubtotal">
			<div class="LinhaSubtotalColuna1"><p>FRETE:</p></div>
			<div class="LinhaSubtotalColuna2"><p>GR�TIS</p></div>
		</div>

		<div class="LinhaDesconto">
			<div class="LinhaDescontoColuna1"><p>DESCONTO:</p></div>
			<div class="LinhaDescontoColuna2"><p>R$ 0,00</p></div>
		</div>

		<div class="LinhaTotal">
			<div class="LinhaTotalColuna1"><p>TOTAL:</p></div>
			<div class="LinhaTotalColuna2"><p>R$ 250,00</p></div>
		</div>

	</div>

	<div class="LinhaBotoes">
		<div class="LinhaBotoesAproveite">
			<div class="LinhaBotoesAproveite1"><b>APROVEITE!</b><br />Adicione outra coroa com 10% de desconto a partir da 2�! *</div>
			<div class="LinhaBotoesAproveite2"><img src="imagens/seta_aproveite2.png" class="LinhaBotoesAproveiteSeta" alt="Seta" /></div>
		</div>
	</div>

	<div class="LinhaBotoes">
		<div class="LinhaBotoes2"><img src="imagens/finalizar_compra.png" class="LinhaBotoesFinalizar" alt="Finalizar compra" /></div>
		<div class="LinhaBotoes1"><img src="imagens/adicionar_outra_coroa.png" class="LinhaBotoesAdicionar" alt="Adicionar outra coroa" /></div>
	</div>

	<div class="LinhaTextoDesconto">* O desconto de itens na promo��o da semana n�o s�o cumulativos e s�o priorit�rios aos demais descontos do site.</div>

</div>
</div>

<!--#include file="rodape.asp" -->
