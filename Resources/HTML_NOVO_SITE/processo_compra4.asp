<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Desabilitado2">Coroas de flores</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Coroa de Flores Premium - 04</div>
</div>
</div>

<div id="ProcessoCompra">
<div class="Div">

	<div class="Div1">

		<div class="Div2">

			<div class="FotoProduto2"><p><img src="imagens/pag_produto_foto.png" class="FotoProdutoImg" alt="Foto do produto" /></p></div>

			<div class="NomeProduto">Coroa de Flores Premium - 04</div>
			<div class="TamanhoProduto">Tamanho: M | 1,2m x 1,0m</div>
			<div class="ValorProduto">R$ 250,00</div>

		</div>

		<div class="Quadro">
		<div class="QuadroTamanho">

			<div class="Texto1">Pagamento</div>
			<div class="Texto2">Todos os campos s�o obrigat�rios</div>

			<div class="Texto10">Selecione a forma de pagamento:</div>

			<div class="FormasPagamento">
				<div class="FormasPagamento2"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="Cart�o de cr�dito" /><br />Cart�o de cr�dito</div>
				<div class="FormasPagamento2"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="D�bito Ita�" /><br />D�bito Ita�</div>
				<div class="FormasPagamento2"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="Boleto" /><br />Boleto</div>
				<div class="FormasPagamento2"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="Pagseguro" /><br />Pagseguro</div>
				<div class="FormasPagamento2"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="Paypal" /><br />Paypal</div>
			</div>

			<div class="Texto9">[Informa��es sobre o pagamento escolhido]</div>

			<div class="Texto3">N� do cart�o:</div>
			<div class="DivCampo"><input type="tel" name="numero_cartao" class="Input3" onkeyup="formatar(this, '#### #### #### ####');" maxlength="19" /></div>

			<div class="FormasPagamento4"><img src="imagens/formas_pagamento2.png" class="FormasPagamento3" alt="Cart�o Visa" /><br />Cart�o Visa</div>

			<div class="Texto3">Nome impresso no cart�o:</div>
			<div class="DivCampo"><input type="text" name="nome_impresso" class="Input1" /></div>

			<div class="DivCampo3">

				<div class="DivCampo4">
					<div class="Texto3">Data de validade:</div>
					<div class="DivCampo"><input type="month" name="data_validade" class="Input2" maxlength="7" /></div>
				</div>

				<div class="DivCampo5">
					<div class="Texto3">C�digo de seguran�a:</div>
					<div class="DivCampo"><input type="tel" name="codigo_seguranca" class="Input2" maxlength="3" /></div>
				</div>

			</div>

			<div class="Texto3">Parcelar em:</div>
			<div class="DivCampo"><select name="frase" class="Select3"><option value="">1 x R$ 350,00</option><option value="">2 x R$ 175,00</option></select></div>

			<div class="DivBotao"><img src="imagens/finalizar_compra2.png" class="DivBotaoImg" alt="Finalizar compra" onclick="window.open('compra_concluida.asp','_top');" /></div>

		</div>
		</div>

	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
