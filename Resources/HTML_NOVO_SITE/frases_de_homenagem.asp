<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Frases de homenagem</div>
</div>
</div>

<div id="FrasesHomenagem">
<div class="Div">

	<div class="TituloPagina">Frases de homenagem</div>
	<div class="VejaNossasCoroas">Veja nossas <a href="" class="FiltroSelecionado">Coroas de Flores</a></div>

	<div class="Filtro">
		<div class="FiltroVer">Ver:</div>	
		<div class="FiltroOpcoes"><a href="" class="FiltroSelecionado">Todos</a></div>	
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Colegas de trabalho</a></div>	
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Familiares</a></div>	
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Amigos pr�ximos</a></div>	
	</div>

	<div class="Div1">
		<div class="Div1Texto1">

			<span class="TituloPagina2">COLEGAS DE TRABALHO</span><br /><br />

			<b>Mais usadas:</b><br />
			- Sentimentos dos Amigos da (nome da empresa)<br />
			- Homenagem dos Amigos da (nome da empresa)<br />
			- Condol�ncias dos Amigos da (nome da empresa)<br />
			- Saudades dos Amigos da (nome da empresa)<br /><br />

			Observa��o: ao inv�s de usar "Amigos", pode-se utilizar "Colegas", "Equipe", "Colaboradores", "Funcion�rios". <br /><br />

			<b>Sofisticadas:</b><br />
			- Com carinho, sinceros sentimentos dos Colegas da (nome da empresa)<br />
			- Sinceras condol�ncias dos Diretores e Funcion�rios da (nome da empresa)<br />
			- Uma sincera homenagem dos Diretores e Funcion�rios da (nome da empresa)<br />
			- Condol�ncias dos Diretores e Colaboradores da (nome da empresa)<br />
			- Foi uma honra t�-lo como amigo e colega de trabalho � Amigos da (nome da empresa)<br /><br />

			<b>Empresas:</b><br />
			- Uma homenagem da (nome da empresa)<br />
			- Sentimentos aos familiares � (nome da empresa)<br />
			- Condol�ncias aos familiares � (nome da empresa)<br />
			- Uma sincera homenagem da (nome da empresa) a todos os familiares e amigos<br /><br />

			<br /><span class="TituloPagina2">FAMILIARES</span><br /><br />

			- Uma homenagem de toda a Fam�lia (sobrenome da fam�lia)<br />
			- Uma homenagem de (nome da pessoa que est� enviando) e fam�lia (sobrenome da fam�lia)<br />
			- Sinceras condol�ncias de toda a Fam�lia (sobrenome da fam�lia)<br />
			- Condol�ncias aos amigos e parentes � Fam�lia (sobrenome da fam�lia)<br />
			- Com carinho, uma homenagem da Fam�lia (sobrenome)<br />
			- � fam�lia (sobrenome da pessoa falecida), nossas sinceras condol�ncias � Fam�lia (sobrenome)<br />
			- Saudades de todos os seus familiares � Fam�lia (sobrenome da fam�lia)<br /><br />

			<br /><span class="TituloPagina2">AMIGOS PR�XIMOS</span><br /><br />

			- Uma pessoa nunca morre quando est� viva em nossos cora��es � Amigos da (refer�ncia)<br />
			- Saudades eternas de uma pessoa especial � Amigos da (refer�ncia)<br />
			- Obrigado por fazer parte de nossas vidas � Amigos da (refer�ncia)<br />
			- Voc� sempre estar� presente em nossos cora��es � Amigos da (refer�ncia)<br />
			- Brilha uma nova estrela no c�u. Sentimentos dos Amigos da (refer�ncia)<br />
			- � fam�lia (sobrenome da pessoa falecida), nossas sinceras condol�ncias � Amigos da (refer�ncia)<br />
			- Descanse em paz e v� com Deus � Amigos da (refer�ncia)<br />
			- Foi uma honra contar com a sua amizade � Amigos da (refer�ncia)<br />
			- Ao melhor amigo do mundo � (nome da pessoa que est� enviando)<br />

		</div>
	</div>

	<div class="Div3">
		<div id="VejaMais">
			<div class="div_centraliza">
				<div class="texto"><p>VER COROAS DE FLORES</p></div>
			</div>
		</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
