<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Seja uma floricultura parceira</div>
</div>
</div>

<div id="SejaParceira">
<div class="Div">

	<div class="TituloPagina">Seja uma floricultura parceira</div>

	<div class="Div1">
		<div class="Div1Texto1">A empresa Coroas Para Vel�rio busca sempre aumentar seu quadro de floriculturas parceiras para aumento de cobertura nacional.<br /><br />Caso a sua floricultura tenha interesse em fazer parte da nossa rede, preencha o cadastro inicial abaixo.<br /><br />A floricultura passar� por um processo de homologa��o e, caso atenda a todos os atributos requisitados, poder� utilizar o selo de qualidade Coroas Para Vel�rio</div>
	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Raz�o social:</div>
			<div class="FormCampo"><input type="text" id="razao_social" name="razao_social" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nome da Floricultura:</div>
			<div class="FormCampo"><input type="text" id="nome_da_floricultura" name="nome_da_floricultura" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CNPJ:</div>
			<div class="FormCampo"><input type="text" id="cnpj" name="cnpj" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Pessoa de Contato:</div>
			<div class="FormCampo"><input type="text" id="pessoa_de_contato" name="pessoa_de_contato" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone 1:</div>
			<div class="FormCampo"><input type="text" id="telefone1" name="telefone1" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone 2:</div>
			<div class="FormCampo"><input type="text" id="telefone2" name="telefone2" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CEP:</div>
			<div class="FormCampo"><input type="text" id="cep" name="cep" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Endere�o:</div>
			<div class="FormCampo"><input type="text" id="endereco" name="endereco" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Bairro:</div>
			<div class="FormCampo"><input type="text" id="bairro" name="bairro" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Estado:</div>
			<div class="FormCampo"><select id="estado" name="estado" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Celular de Contato:</div>
			<div class="FormCampo"><input type="text" id="celular_contato" name="celular_contato" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nextel:</div>
			<div class="FormCampo"><input type="text" id="nextel" name="nextel" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Site:</div>
			<div class="FormCampo"><input type="text" id="site" name="site" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Hor�rio de funcionamento:</div>
			<div class="FormCampo"><input type="text" id="horario_funcionamento" name="horario_funcionamento" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Como nos conheceu?</div>
			<div class="FormCampo"><select id="como_nos_conheceu" name="como_nos_conheceu" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput" /></div>
				<div class="FormValidacaoTexto2"><input type="radio" id="termos" name="termos" /> Li e estou de acordo com os <a href="" class="FormValidacaoTexto3">Termos e Condi��es Comerciais.</a></div>
			</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" /></div>

	</div>

	<div class="Div1">
		<div class="Div1Texto1">Agradecemos pela prefer�ncia!<br />Equipe Coroas Para Vel�rio.</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
