<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Condol�ncias virtuais gr�tis</div>
</div>
</div>

<div id="CondolenciasVirtuaisGratis">
<div class="Div">

	<div class="TituloPagina">Condol�ncias Virtuais Gr�tis</div>

	<div class="Div1">
		<div class="Div1Texto1">Utilize a nossa ferramenta de Condol�ncias Virtuais para enviar uma mensagem a quem est� precisando. Escolha um dos cart�es dispon�veis, preencha os dados e escreva uma mensagem carinhosa a quem voc� quer transmitir suas condol�ncias.</div>
	</div>

	<div class="Formulario">

		<div class="TituloPagina2">De:</div>

		<div class="FormDiv">
			<div class="FormNomeCampo">Seu nome:</div>
			<div class="FormCampo"><input type="text" id="seu_nome_de" name="seu_nome_de" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv">
			<div class="FormNomeCampo">Seu e-mail:</div>
			<div class="FormCampo"><input type="text" id="seu_email_de" name="seu_email_de" class="FormCampoInput" /></div>
		</div>

		<div class="TituloPagina2">Para:</div>

		<div class="FormDiv">
			<div class="FormNomeCampo">Seu nome:</div>
			<div class="FormCampo"><input type="text" id="seu_nome_para" name="seu_nome_para" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv">
			<div class="FormNomeCampo">Seu e-mail:</div>
			<div class="FormCampo"><input type="text" id="seu_email_para" name="seu_email_para" class="FormCampoInput" /></div>
		</div>

		<div class="TituloPagina2">Texto que deseja inserir no cart�o:</div>

		<div class="FormDiv">
			<div class="FormNomeCampo">&nbsp;</div>
			<div class="FormCampo"><textarea id="texto" name="texto" class="FormCampoTextarea"></textarea></div>
		</div>

		<div class="TituloPagina2">Escolha abaixo a cor de fundo da mensagem:</div>

		<div class="CorFundo">
			<div class="CorFundoOpcao">
				<div class="CorFundoRadio"><input type="radio" name="opcao" value="1" /></div>
				<div class="CorFundoImagem"><img src="imagens/msg_condolencia1.png" class="CorFundoImg" alt="Imagem" /></div>
			</div>
			<div class="CorFundoOpcao">
				<div class="CorFundoRadio"><input type="radio" name="opcao" value="2" /></div>
				<div class="CorFundoImagem"><img src="imagens/msg_condolencia2.png" class="CorFundoImg" alt="Imagem" /></div>
			</div>
			<div class="CorFundoOpcao">
				<div class="CorFundoRadio"><input type="radio" name="opcao" value="3" /></div>
				<div class="CorFundoImagem"><img src="imagens/msg_condolencia3.png" class="CorFundoImg" alt="Imagem" /></div>
			</div>
		</div>

		<div class="FormDivBotao">
			<div class="Visualizar"><input type="submit" value="VISUALIZAR" class="FormBotao" /></div>
			<div class="Enviar"><input type="submit" value="ENVIAR CONDOL�NCIA" class="FormBotao2" /></div>
		</div>

	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
