<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Sugest�es de coroas</div>
</div>
</div>

<div id="SugestaoCoroas">
<div class="Div">

	<div class="TituloPagina">Coroas Sugeridas</div>

	<div class="QuadroG">

		<div class="QuadroGIdealFoto">
			<div class="QuadroGIdeal">Coroa de Flores Ideal</div>
			<div class="QuadroGFoto"><img src="imagens/sugestao_foto.jpg" class="QuadroGFotoImg" alt="Foto do produto" /></div>
		</div>

		<div class="QuadroGDados">

			<div class="QuadroGTitulo">Coroa de Flores Premium - 04</div>

			<div class="QuadroGFreteFaixa">
				<div class="QuadroGFrete"><img src="imagens/sugestao_frete.png" class="QuadroGFreteImg" alt="Frete gr�tis" /></div>
				<div class="QuadroGFaixa"><img src="imagens/sugestao_faixa.png" class="QuadroGFaixaImg" alt="Faixa de homenagem gr�tis" /></div>
			</div>

			<div class="QuadroGEscolhaT">Escolha o tamanho:</div>

			<div id="EscolhaTamanho">

				<div class="NaoSelecionado" id="PequenoSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','1');M_E_Sugestao('MedioSugestao','2');M_E_Sugestao('GrandeSugestao','2');">
					<div class="NaoSelecionado1" id="PequenoSugestao1">P</div>
					<div class="NaoSelecionado2" id="PequenoSugestao2">1,0m x 1,0m</div>
				</div>

				<div class="Selecionado" id="MedioSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','2');M_E_Sugestao('MedioSugestao','1');M_E_Sugestao('GrandeSugestao','2');">
					<div class="Selecionado1" id="MedioSugestao1">M</div>
					<div class="Selecionado2" id="MedioSugestao2">1,2m x 1,0m</div>
				</div>

				<div class="NaoSelecionado" id="GrandeSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','2');M_E_Sugestao('MedioSugestao','2');M_E_Sugestao('GrandeSugestao','1');">
					<div class="NaoSelecionado1" id="GrandeSugestao1">G</div>
					<div class="NaoSelecionado2" id="GrandeSugestao2">1,5m x 1,0m</div>
				</div>

			</div>

			<div class="Valores3" id="PequenoSugestaoValor">
				<div class="Valores1">R$ 200,00</div>
				<div class="Valores2">12x de R$ 16,66</div>
			</div>

			<div class="Valores" id="MedioSugestaoValor">
				<div class="Valores1">R$ 250,00</div>
				<div class="Valores2">12x de R$ 20,83</div>
			</div>

			<div class="Valores3" id="GrandeSugestaoValor">
				<div class="Valores1">R$ 300,00</div>
				<div class="Valores2">12x de R$ 25,00</div>
			</div>

			<div class="Comprar"><img src="imagens/sugestao_comprar.png" class="ComprarImg" alt="Comprar" /></div>

		</div>

	</div>

	<div class="OutrasOpcoes">Outras Op��es</div>

	<div class="produtos">

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="Dados">
					<div class="Dados2">
						<div class="titulo">Coroa de Flores Premium - 01</div>
						<div class="tamanho_imagens">

							<center><div class="escolha_tamanho_div">

								<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

								<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

								<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

								<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

								<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

								<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

							</div>
							</center>

						</div>

						<div id="pequeno1" class="escondido">
						<div class="tamanho_texto">1,0m x 1,0m</div>
						<div class="valor">150,00</div>
						</div>

						<div id="medio1">
						<div class="tamanho_texto">1,2m x 1,0m</div>
						<div class="valor">250,00</div>
						</div>

						<div id="grande1" class="escondido">
						<div class="tamanho_texto">1,5m x 1,0m</div>
						<div class="valor">350,00</div>
						</div>

						<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
					</div>
				</div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado2">
				<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="Dados">
					<div class="Dados2">
						<div class="titulo">Coroa de Flores Premium - 01</div>
						<div class="tamanho_imagens">
							<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
							<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
							<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
						</div>
						<div class="tamanho_texto">1,2m x 1,0m</div>
						<div class="valor">250,00</div>
						<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="VejaMaisCoroas">[+] <a href="" class="VejaMaisCoroas2">Veja mais Coroas de Flores</a></div>

	<div id="VejaMais">
		<div class="texto"><p>VEJA MAIS OP��ES DE COROAS DE FLORES</p></div>
	</div>

	<div class="BarraOpcoes">
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_frete_gratis.png" width="41" height="26" border="0" alt="Frete gr�tis" /></div>
			<div class="BarraOpcoesTexto">FRETE GR�TIS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_faixa_homenagem.png" width="27" height="27" border="0" alt="Faixa de homenagem gr�tis" /></div>
			<div class="BarraOpcoesTexto">FAIXA DE HOMENAGEM GR�TIS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_faturamento.png" width="25" height="26" border="0" alt="Faturamento para empresas" /></div>
			<div class="BarraOpcoesTexto">FATURAMENTO PARA EMPRESAS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_boleto_cartao.png" width="25" height="19" border="0" alt="Pagamento com cart�o de cr�dito" /></div>
			<div class="BarraOpcoesTexto">PAGAMENTO COM CART�O DE CR�DITO</div>
		</div>
	</div>

	<div class="Prazos">
		<div class="PrazoEntrega">
			<div class="PrazosTitulo">Prazo de entrega:</div>
			<div class="PrazoEntregaTexto">O site Coroas Para Vel�rio sabe que suas entregas precisam ser r�pidas para atender com qualidade o mercado de coroa de flores.<br />Com o objetivo de trabalharmos com transpar�ncia com nossos clientes, nossa empresa decidiu padronizar os tempos e prazos de entrega.</div>
		</div>
		<div class="PrazoMedioEntrega">
			<div class="PrazosTitulo">Prazo m�dio de entrega:</div>
			<div class="PrazoMedioEntregaDiv">
				<div class="PrazoMedioEntregaDiv2"><div class="PrazoMedioEntregaLeft">Capitais e cidades grandes</div><div class="PrazoMedioEntregaRight">1h00 a 1h30</div></div>
				<div class="PrazoMedioEntregaDiv2"><div class="PrazoMedioEntregaLeft">Cidades de menor porta</div><div class="PrazoMedioEntregaRight">1h00 a 2h00</div></div>
			</div>
		</div>
	</div>

	<div class="Separacao"></div>

	<div class="CoroasTexto">Coroas para Vel�rio atendendo voc� h� xx anos.</div>

	<div class="Aparicoes">Veja nossas apari��es na m�dia!</div>

	<div class="AparicoesDiv">
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_1.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_2.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_3.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv2"><img src="imagens/aparicoes_4.jpg" class="AparicoesImg" alt="Imagem" /></div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
