<!--#include file="topo.asp" -->

<div id="ComoComprar">
	<div class="Div">
		<div class="TituloPagina">Mais vendidas</div>
	</div>
</div>

<div id="ListagemProdutos2">
	<div class="div_centraliza" id="ListaProdutos">

		<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta" /></p></div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do Produto" /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">

					<center><div class="escolha_tamanho_div">

						<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho" /></div>

						<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

						<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" alt="Tamanho"  /></div>

						<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

						<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" alt="Tamanho"  /></div>

						<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" alt="Tamanho"  /></div>

					</div>
					</center>

				</div>

				<div id="pequeno1_m" class="escondido">
				<div class="tamanho_texto">1,0m x 1,0m</div>
				<div class="valor">150,00</div>
				</div>

				<div id="medio1_m">
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				</div>

				<div id="grande1_m" class="escondido">
				<div class="tamanho_texto">1,5m x 1,0m</div>
				<div class="valor">350,00</div>
				</div>

				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=2','ListaProdutos');" alt="Seta"  /></p></div>

	</div>
</div>

<div id="ListagemProdutos">
	<div class="div_centraliza">

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">

					<center><div class="escolha_tamanho_div">

						<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

						<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

						<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

						<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

					</div>
					</center>

				</div>

				<div id="pequeno1" class="escondido">
				<div class="tamanho_texto">1,0m x 1,0m</div>
				<div class="valor">150,00</div>
				</div>

				<div id="medio1">
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				</div>

				<div id="grande1" class="escondido">
				<div class="tamanho_texto">1,5m x 1,0m</div>
				<div class="valor">350,00</div>
				</div>

				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado2">
				<div class="foto"><img src="imagens/foto4.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

	</div>
</div>

<div id="VejaMais">
	<div class="div_centraliza">
		<div class="texto"><p>VEJA MAIS COROAS DE FLORES</p></div>
	</div>
</div>

<!--#include file="rodape.asp" -->
