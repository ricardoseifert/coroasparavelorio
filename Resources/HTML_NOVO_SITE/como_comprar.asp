<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Como comprar</div>
</div>
</div>

<div id="ComoComprar">
<div class="Div">

	<div class="TituloPagina">Como comprar</div>

	<div class="Caixa">
		<div class="Numero">1�</div>
		<div class="DivTextos">
			<div class="Titulo">Selecione a Coroa de flores de sua prefer�ncia e clique em comprar...</div>
			<div class="Texto">Ap�s a escolha, voc� ser� direcionado para o Carrinho de Compras. Siga as pr�ximas instru��es para realizar a sua compra.<br />Caso n�o tenha encontrado sua coroa utilize <a href="" class="Url">nossa ajuda</a> ou entre em contato pelo <a href="" class="Url">chat</a>.</div>
		</div>
	</div>

	<div class="Caixa">
		<div class="Numero">2�</div>
		<div class="DivTextos">
			<div class="Titulo">Insira as informa��es solicitadas...</div>
			<div class="Texto">Preencha as informa��es da entrega do produto. Caso n�o possua alguma informa��o, entre em contato com nosso <a href="" class="Url">chat</a> ou deixe em branco que n�s faremos a pesquisa com nossos contatos. Na p�gina '<a href="" class="Url">Vel�rios e Cemiterios</a>' voc� poder� encontrar informa��es importantes de qualquer vel�rios, cemit�rio ou hospital do Brasil.<br />Preencha seus dados cadastrais e escolha a faixa de homenagem gr�tis.</div>
		</div>
	</div>

	<div class="Caixa">
		<div class="Numero">3�</div>
		<div class="DivTextos">
			<div class="Titulo">Escolha a forma de pagamento...</div>
			<div class="Texto">O site Coroas Para Vel�rio aceita os principais meios de pagamento, inclusive realiza o faturamento completo para empresas. Em caso de d�vidas, <a href="" class="Url">entre em contato com a nossa Central de Atendimento</a>.</div>
		</div>
	</div>

	<div class="Div6">
	<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
