<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Indique o Coroas para Vel�rio</div>
</div>
</div>

<div id="IndiqueCoroas">
<div class="Div">

	<div class="TituloPagina">Indique o Coroas para Vel�rio</div>

	<div class="Div1">
		<div class="Div1Texto1">Indique o CPV para um amigo, parente ou algu�m que esteja precisando de Coroas ou Arranjos de Flores.</div>
	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Seu nome:</div>
			<div class="FormCampo"><input type="text" id="seu_nome" name="seu_nome" class="FormCampoInput2" onfocus="esconde_validacao('seu_nome','1');" /></div>
			<div id="aviso_seu_nome" class="FormCampoObrigatorio" onclick="esconde_validacao('seu_nome','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Seu e-mail:</div>
			<div class="FormCampo"><input type="text" id="seu_email" name="seu_email" class="FormCampoInput2" onfocus="esconde_validacao('seu_email','1');" /></div>
			<div id="aviso_seu_email" class="FormCampoObrigatorio" onclick="esconde_validacao('seu_email','1');">Campo obrigat�rio. Digite um email v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nome da pessoa:</div>
			<div class="FormCampo"><input type="text" id="nome_da_pessoa" name="nome_da_pessoa" class="FormCampoInput2" onfocus="esconde_validacao('nome_da_pessoa','1');" /></div>
			<div id="aviso_nome_da_pessoa" class="FormCampoObrigatorio" onclick="esconde_validacao('nome_da_pessoa','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">E-mail da pessoa:</div>
			<div class="FormCampo"><input type="text" id="email_da_pessoa" name="email_da_pessoa" class="FormCampoInput2" onfocus="esconde_validacao('email_da_pessoa','1');" /></div>
			<div id="aviso_email_da_pessoa" class="FormCampoObrigatorio" onclick="esconde_validacao('email_da_pessoa','1');">Campo obrigat�rio. Digite um email v�lido</div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput2" onfocus="esconde_validacao('validacao','1');" /></div>
			</div>
			<div id="aviso_validacao" class="FormCampoObrigatorio" onclick="esconde_validacao('validacao','1');">Campo obrigat�rio. Soma n�o confere.</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="INDICAR" class="FormBotao" /></div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
