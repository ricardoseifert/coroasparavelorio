<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Significado das flores</div>
</div>
</div>

<div id="SignificadoFlores">
<div class="Div">

	<div class="TituloPagina">Significado das Flores</div>

	<div class="Listagem">
		<div class="Foto"><img src="imagens/significado_flores_imagem.png" width="152" height="89" border="0" alt="Foto" /></div>
		<div class="DivTextos" onclick="mostra('Detalhes');esconde('VerMais')">
			<div class="Titulo">[Nome da flor]</div>
			<div class="Titulo2">Representam lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
			<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis purus urna. In pretium tincidunt libero, quis semper turpis volutpat id. Nulla elit arcu, facilisis id volutpat quis, elementum eu nisi. Praesent consequat non ipsum nec dapibus. Nullam eleifend est a fermentum placerat.</div>

			<div id="Detalhes" style="float:left;width:100%;text-align:left;padding-top:10px;display:none">

				<div style="float:left;width:100%;font-family:arial,verdana;font-size:13px;color:#303030;text-align:left;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis purus urna. In pretium tincidunt libero, quis semper turpis volutpat id. Nulla elit arcu, facilisis id volutpat quis, elementum eu nisi. Praesent consequat non ipsum nec dapibus. Nullam eleifend est a fermentum placerat</div>

				<div id="SignificadoFloresProd">
					<div class="div_centraliza" id="SigListaProdutos">

						<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta" /></p></div>

						<div class="produto">

							<div class="quadrado1">
								<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do Produto" /></div>
								<div class="titulo">Coroa de Flores Premium - 01</div>
								<div class="tamanho_imagens">

									<center><div class="escolha_tamanho_div">

										<div id="sig_pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho" /></div>

										<div id="sig_pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_pequeno1_m');esconde('sig_medio1_m');esconde('sig_grande1_m');mostra('sig_pequeno1_m_s');esconde('sig_pequeno1_m_ns');mostra('sig_medio1_m_ns');esconde('sig_medio1_m_s');mostra('sig_grande1_m_ns');esconde('sig_grande1_m_s');" alt="Tamanho"  /></div>

										<div id="sig_medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_medio1_m');esconde('sig_pequeno1_m');esconde('sig_grande1_m');" alt="Tamanho"  /></div>

										<div id="sig_medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_medio1_m');esconde('sig_pequeno1_m');esconde('sig_grande1_m');esconde('sig_pequeno1_m_s');mostra('sig_pequeno1_m_ns');esconde('sig_medio1_m_ns');mostra('sig_medio1_m_s');mostra('sig_grande1_m_ns');esconde('sig_grande1_m_s');" alt="Tamanho"  /></div>

										<div id="sig_grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_grande1_m');esconde('sig_pequeno1_m');esconde('sig_medio1_m');" alt="Tamanho"  /></div>

										<div id="sig_grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_grande1_m');esconde('sig_pequeno1_m');esconde('sig_medio1_m');esconde('sig_pequeno1_m_s');mostra('sig_pequeno1_m_ns');mostra('sig_medio1_m_ns');esconde('sig_medio1_m_s');esconde('sig_grande1_m_ns');mostra('sig_grande1_m_s');" alt="Tamanho"  /></div>

									</div>
									</center>

								</div>

								<div id="sig_pequeno1_m" class="escondido">
								<div class="tamanho_texto">1,0m x 1,0m</div>
								<div class="valor">150,00</div>
								</div>

								<div id="sig_medio1_m">
								<div class="tamanho_texto">1,2m x 1,0m</div>
								<div class="valor">250,00</div>
								</div>

								<div id="sig_grande1_m" class="escondido">
								<div class="tamanho_texto">1,5m x 1,0m</div>
								<div class="valor">350,00</div>
								</div>

								<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
								<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
							</div>

							<div class="quadrado2">
								<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
								<div class="titulo">Coroa de Flores Premium - 01</div>
								<div class="tamanho_imagens">
									<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
									<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
									<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
								</div>
								<div class="tamanho_texto">1,2m x 1,0m</div>
								<div class="valor">250,00</div>
								<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
								<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
							</div>

							<div class="quadrado2">
								<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
								<div class="titulo">Coroa de Flores Premium - 01</div>
								<div class="tamanho_imagens">
									<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
									<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
									<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
								</div>
								<div class="tamanho_texto">1,2m x 1,0m</div>
								<div class="valor">250,00</div>
								<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
								<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
							</div>

						</div>

						<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos2.asp?pag=2','SigListaProdutos');" alt="Seta"  /></p></div>

					</div>
				</div>

				<div class="Url"><a href="javascript:esconde('Detalhes');mostra('VerMais')" class="Url2">Minimizar t�pico</a></div>

			</div>

		</div>
		<div class="Url" id="VerMais"><a href="javascript:mostra('Detalhes');esconde('VerMais')" class="Url2">Ver mais</a></div>
	</div>

	<div class="Listagem">
		<div class="Foto"><img src="imagens/significado_flores_imagem.png" width="152" height="89" border="0" alt="Foto" /></div>
		<div class="DivTextos">
			<div class="Titulo">[Nome da flor]</div>
			<div class="Titulo2">Representam lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
			<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis purus urna. In pretium tincidunt libero, quis semper turpis volutpat id. Nulla elit arcu, facilisis id volutpat quis, elementum eu nisi. Praesent consequat non ipsum nec dapibus. Nullam eleifend est a fermentum placerat.</div>
		</div>
		<div class="Url"><a href="" class="Url2">Ver mais</a></div>
	</div>

	<div class="Listagem">
		<div class="Foto"><img src="imagens/significado_flores_imagem.png" width="152" height="89" border="0" alt="Foto" /></div>
		<div class="DivTextos">
			<div class="Titulo">[Nome da flor]</div>
			<div class="Titulo2">Representam lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
			<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis purus urna. In pretium tincidunt libero, quis semper turpis volutpat id. Nulla elit arcu, facilisis id volutpat quis, elementum eu nisi. Praesent consequat non ipsum nec dapibus. Nullam eleifend est a fermentum placerat.</div>
		</div>
		<div class="Url"><a href="" class="Url2">Ver mais</a></div>
	</div>

	<div class="Listagem">
		<div class="Foto"><img src="imagens/significado_flores_imagem.png" width="152" height="89" border="0" alt="Foto" /></div>
		<div class="DivTextos">
			<div class="Titulo">[Nome da flor]</div>
			<div class="Titulo2">Representam lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
			<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis purus urna. In pretium tincidunt libero, quis semper turpis volutpat id. Nulla elit arcu, facilisis id volutpat quis, elementum eu nisi. Praesent consequat non ipsum nec dapibus. Nullam eleifend est a fermentum placerat.</div>
		</div>
		<div class="Url"><a href="" class="Url2">Ver mais</a></div>
	</div>

	<div class="Div6">
	<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
