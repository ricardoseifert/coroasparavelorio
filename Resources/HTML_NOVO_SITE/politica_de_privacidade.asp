<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Pol�tica de privacidade</div>
</div>
</div>

<div id="TermosCondicoesComerciais">
<div class="Div">

	<div class="TituloPagina">Pol�tica de privacidade</div>

	<div class="Div1">
		<div class="Div1Texto1">

			O site Coroas Para Vel�rio.com.br vem por meio desta carta informar aos seus usu�rios sobre a Polit�ca de Privacidade das informa��es.<br /><br />

			a) Os dados cadastrados pelos clientes e usu�rios, tais como Nome Completo, Registro Geral (RG) ou Cadastro de Pessoa F�sica (CPF), permancer�o em total sigilo pela Equipe do site coroasparavelorio.com.br;<br /><br />

			b) As informa��es corporativas fornecidas para a emiss�o das notas ou cupons fiscais tamb�m ser�o tratadas de forma sigilosa;<br /><br />

			c) As informa��es a respeito de contatos dos clientes, tais como telefone fixo, telefone celular ou email, n�o ser�o passados em hip�tese alguma a terceiros;<br /><br />

			d) Os dados de cart�o de cr�dito, independente da bandeira, n�o ser�o armazenados pelos nossos servidores ou funcion�rios, cabendo aos nossos clientes a manipula��o e inser��o dos dados dentro dos dom�nios seguros das operadoras de cart�o de cr�dito;<br /><br />

			e) Qualquer tipo de comunica��o realizada pelo site Coroas Para Vel�rio.com.br, tais como novidades, promo��es ou informa��es, ser� dirigida com a permiss�o dos clientes e usu�rios;<br /><br />

			Qualquer d�vida a respeito de nossa Pol�tica de Privacidade poder� ser dirigida ao email: contato@coroasparavelorio.com.br<br /><br />

			Agradecemos a prefer�ncia,<br />
			Equipe Coroas Para Vel�rio.com.br

		</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
