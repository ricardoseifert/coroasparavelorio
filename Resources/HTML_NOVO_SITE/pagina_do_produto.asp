<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Desabilitado2">Coroas de flores</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Coroa de Flores Premium - 04</div>
</div>
</div>

<div id="PaginaProduto">
<div class="Div">

	<div class="CelQuadroGTitulo">Coroa de Flores Premium - 04</div>

	<div class="CelQuadroGFreteFaixa">
		<div class="CelQuadroGFrete"><img src="imagens/sugestao_frete2.png" class="CelQuadroGFreteImg" alt="Frete gr�tis" /></div>
		<div class="CelQuadroGFaixa"><img src="imagens/sugestao_faixa2.png" class="CelQuadroGFaixaImg" alt="Faixa de homenagem gr�tis" /></div>
	</div>

	<div class="QuadroG">

		<div class="QuadroGIdealFoto">
			<div class="QuadroGFoto"><p><img src="imagens/pag_produto_foto.png" class="QuadroGFotoImg" alt="Foto do produto" /></p></div>
		</div>

		<div class="QuadroGDados">

			<div class="QuadroGTitulo">Coroa de Flores Premium - 04</div>

			<div class="QuadroGFreteFaixa">
				<div class="QuadroGFrete"><img src="imagens/sugestao_frete.png" class="QuadroGFreteImg" alt="Frete gr�tis" /></div>
				<div class="QuadroGFaixa"><img src="imagens/sugestao_faixa.png" class="QuadroGFaixaImg" alt="Faixa de homenagem gr�tis" /></div>
			</div>

			<div class="QuadroGEscolhaT">Escolha o tamanho:</div>

			<div id="EscolhaTamanho">

				<div class="NaoSelecionado" id="PequenoSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','1');M_E_Sugestao('MedioSugestao','2');M_E_Sugestao('GrandeSugestao','2');">
					<div class="NaoSelecionado1" id="PequenoSugestao1">P</div>
					<div class="NaoSelecionado2" id="PequenoSugestao2">1,0m x 1,0m</div>
				</div>

				<div class="Selecionado" id="MedioSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','2');M_E_Sugestao('MedioSugestao','1');M_E_Sugestao('GrandeSugestao','2');">
					<div class="Selecionado1" id="MedioSugestao1">M</div>
					<div class="Selecionado2" id="MedioSugestao2">1,2m x 1,0m</div>
				</div>

				<div class="NaoSelecionado" id="GrandeSugestao" onmouseover="M_E_Sugestao('PequenoSugestao','2');M_E_Sugestao('MedioSugestao','2');M_E_Sugestao('GrandeSugestao','1');">
					<div class="NaoSelecionado1" id="GrandeSugestao1">G</div>
					<div class="NaoSelecionado2" id="GrandeSugestao2">1,5m x 1,0m</div>
				</div>

			</div>

			<div class="Valores3" id="PequenoSugestaoValor">
				<div class="Valores1">R$ 200,00</div>
				<div class="Valores2">12x de R$ 16,66</div>
			</div>

			<div class="Valores" id="MedioSugestaoValor">
				<div class="Valores1">R$ 250,00</div>
				<div class="Valores2">12x de R$ 20,83</div>
			</div>

			<div class="Valores3" id="GrandeSugestaoValor">
				<div class="Valores1">R$ 300,00</div>
				<div class="Valores2">12x de R$ 25,00</div>
			</div>

			<div class="Comprar"><img src="imagens/sugestao_comprar.png" class="ComprarImg" alt="Comprar" /></div>

			<div class="Imagens">
			<div class="Imagens1"><img src="imagens/pag_produto_site_seguro.png" class="Imagens1Img" alt="Site seguro" /></div>
			<div class="Imagens1"><img src="imagens/pag_produto_frete_gratis.png" class="Imagens2Img" alt="Frete gr�tis" /></div>
			<div class="Imagens2"><img src="imagens/pag_produto_entrega_24h.png" class="Imagens3Img" alt="Entrega 24 horas" /></div>
			</div>

		</div>

	</div>

	<div class="FloresContem">

		<div class="FloresContemTitulo">Essa coroa contem as flores</div>

		<div class="FloresContemQuadro">

			<div class="FloresContemFotos">
				<div id="Flor1T" class="FloresContemFotosSelecionado" onmouseover="M_E_Pag_Produto('Flor1','1');M_E_Pag_Produto('Flor2','2');M_E_Pag_Produto('Flor3','2');">Cris�ntemo<br /><img src="imagens/pag_produto_flores.png" class="FloresContemFotosImg" alt="Cris�ntemo" /></div>
				<div id="Flor2T" class="FloresContemFotosNaoSelecionado" onmouseover="M_E_Pag_Produto('Flor1','2');M_E_Pag_Produto('Flor2','1');M_E_Pag_Produto('Flor3','2');">G�rberas<br /><img src="imagens/pag_produto_flores.png" class="FloresContemFotosImg" alt="G�rberas" /></div>
				<div id="Flor3T" class="FloresContemFotosNaoSelecionado" onmouseover="M_E_Pag_Produto('Flor1','2');M_E_Pag_Produto('Flor2','2');M_E_Pag_Produto('Flor3','1');">Palma de Santa Rita<br /><img src="imagens/pag_produto_flores.png" class="FloresContemFotosImg" alt="Palma de Santa Rita" /></div>
			</div>

			<div id="Flor1">
				<div class="FloresContemTexto1">Cris�ntemo</div>
				<div class="FloresContemTexto2">S�o as flores mais utilizadas para homenagear falec�dos</div>
				<div class="FloresContemTexto3"><a href="" class="FloresContemUrl">continue lendo �</a></div>
			</div>

			<div id="Flor2" class="escondido">
				<div class="FloresContemTexto1">G�rberas</div>
				<div class="FloresContemTexto2">S�o as flores mais utilizadas para homenagear falec�dos</div>
				<div class="FloresContemTexto3"><a href="" class="FloresContemUrl">continue lendo �</a></div>
			</div>

			<div id="Flor3" class="escondido">
				<div class="FloresContemTexto1">Palma de Santa Rita</div>
				<div class="FloresContemTexto2">S�o as flores mais utilizadas para homenagear falec�dos</div>
				<div class="FloresContemTexto3"><a href="" class="FloresContemUrl">continue lendo �</a></div>
			</div>

		</div>

	</div>

	<div class="BarraOpcoes">
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_frete_gratis.png" width="41" height="26" border="0" alt="Frete gr�tis" /></div>
			<div class="BarraOpcoesTexto">FRETE GR�TIS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_faixa_homenagem.png" width="27" height="27" border="0" alt="Faixa de homenagem gr�tis" /></div>
			<div class="BarraOpcoesTexto">FAIXA DE HOMENAGEM GR�TIS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_faturamento.png" width="25" height="26" border="0" alt="Faturamento para empresas" /></div>
			<div class="BarraOpcoesTexto">FATURAMENTO PARA EMPRESAS</div>
		</div>
		<div class="BarraOpcoesDiv">
			<div class="BarraOpcoesImagem1"><img src="imagens/m_boleto_cartao.png" width="25" height="19" border="0" alt="Pagamento com cart�o de cr�dito" /></div>
			<div class="BarraOpcoesTexto">PAGAMENTO COM CART�O DE CR�DITO</div>
		</div>
	</div>

	<div class="Prazos">
		<div class="PrazoEntrega">
			<div class="PrazosTitulo">Prazo de entrega:</div>
			<div class="PrazoEntregaTexto">O site Coroas Para Vel�rio sabe que suas entregas precisam ser r�pidas para atender com qualidade o mercado de coroa de flores.<br />Com o objetivo de trabalharmos com transpar�ncia com nossos clientes, nossa empresa decidiu padronizar os tempos e prazos de entrega.</div>
		</div>
		<div class="PrazoMedioEntrega">
			<div class="PrazosTitulo">Prazo m�dio de entrega:</div>
			<div class="PrazoMedioEntregaDiv">
				<div class="PrazoMedioEntregaDiv2"><div class="PrazoMedioEntregaLeft">Capitais e cidades grandes</div><div class="PrazoMedioEntregaRight">1h00 a 1h30</div></div>
				<div class="PrazoMedioEntregaDiv2"><div class="PrazoMedioEntregaLeft">Cidades de menor porta</div><div class="PrazoMedioEntregaRight">1h00 a 2h00</div></div>
			</div>
		</div>
	</div>

	<div class="TrustVox"><img src="imagens/Trust.jpg" width="830" height="225" border="0" alt="TrustVox" /></div>

	<div class="Separacao"></div>

	<div class="CoroasTexto">Coroas para Vel�rio atendendo voc� h� xx anos.</div>

	<div class="Aparicoes">Veja nossas apari��es na m�dia!</div>

	<div class="AparicoesDiv">
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_1.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_2.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv1"><img src="imagens/aparicoes_3.jpg" class="AparicoesImg" alt="Imagem" /></div>
		<div class="AparicoesDiv2"><img src="imagens/aparicoes_4.jpg" class="AparicoesImg" alt="Imagem" /></div>
	</div>

	<div id="VejaMais">
		<div class="texto"><p>CARREGAR MAIS COROAS DE FLORES</p></div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
