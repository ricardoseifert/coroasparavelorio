<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Tempo e prazo de entrega</div>
</div>
</div>

<div id="TempoPrazo">
<div class="Div">

	<div class="TituloPagina">Tempo e prazo de entrega</div>

	<div class="Selo"><img src="imagens/selo-entrega.jpg" class="SeloImg" alt="Selo" /></div>

	<div class="Div1">
		<div class="Div1Texto2">

			<div	class="Div1Texto4">O site Coroas Para Vel�rio sabe que suas entregas precisam ser r�pidas para atender com qualidade o mercado de coroa de flores.<br /><br />

			Com o objetivo de trabalharmos com transpar�ncia com nossos clientes, nossa empresa decidiu padronizar os tempos e prazos de entrega. Entre em contato pelo televendas no n�mero <b>0800-777-1986</b> e encomende sua Coroa de Flores.<br /><br /></div>

			<b>Prazo M�dio de Entrega</b><br />
			<div class="PrazoEntrega"><div class="PrazoEntrega2">Capitais e Cidades Grandes</div><div class="PrazoEntrega3">1h00 a 1h30</div></div>
			<div class="PrazoEntrega"><div class="PrazoEntrega2">Cidades de Menor Porte</div><div class="PrazoEntrega3">1h00 a 2h00</div></div>

		</div>

		<div class="Div1Texto3">Nosso frete � gratuito para todo o Brasil!</div>
	</div>

	<div id="AindaTemDuvida">

		<div class="Div2Texto1">Ainda possui d�vidas?<br />Entre em contato conosco pelo <b>Chat</b> ou ligue para nossa <b>Central de Atendimento:</b> 0800-777-1986 <b>(24h)</b></div>

		<div class="Div2Texto2">Ou acesse nossa se��o de <a href="" class="Div2Texto3"><b>Perguntas Frequentes</b></a>.<br />Esses prazos s�o nossos tempos m�dios de entrega, podendo variar de acordo com a demanda, vel�rios de grande porte, condi��es do clima e mesmo nossa proximidade em rela��o ao local de entrega.<br /><br />Nosso objetivo � atender todos os nossos clientes com a maior qualidade poss�vel, e isso inclui o prazo de entrega de nossos produtos, que precisam chegar com agilidade e em excelente condi��es.<br /><br />Para realizar a sua homenagem, veja nossa rela��o de <a href="" class="Div2Texto3"><b>Coroas de Flores</b></a>.</div>

		<div class="Div2Texto4">Procure aqui o cemit�rio, vel�rio, hospital, cremat�rio ou cidade que deseja enviar coroas de flores:</div>

		<div class="DivBusca">
			<div class="DivBusca2"><input type="text" id="busca2" name="busca2" class="DivBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="DivBusca2"><img src="imagens/busca.png" class="DivBuscar" alt="Buscar" /></div>
		</div>

	</div>

	<div class="Div3">
		<div id="VejaMais">
			<div class="div_centraliza">
				<div class="texto"><p>VER COROAS DE FLORES</p></div>
			</div>
		</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
