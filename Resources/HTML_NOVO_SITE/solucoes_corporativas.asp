<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Solu��es corporativas</div>
</div>
</div>

<div id="SolucoesCorporativas">
<div class="Div">

	<div class="TituloPagina">Solu��es Corporativas</div>

	<div class="Div1">
		<div class="Div1Texto1">
			O site Coroas Para Vel�rio busca facilitar a aquisi��o de Coroa de Flores, desburocratizando o processo de compra e facilitando os tr�mites internos das empresas. Trabalhamos com Nota Fiscal Eletr�nica e todas as formas de pagamento.
		</div>
	</div>

	<div class="SubTituloPagina">Alguns de nossos clientes</div>

	<div class="DivImagens">
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/1.png" width="54" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/2.png" width="136" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/3.png" width="54" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/4.png" width="49" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/5.png" width="32" height="53" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/6.png" width="91" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/7.png" width="55" height="55" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/8.png" width="122" height="27" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/9.png" width="124" height="54" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/10.png" width="113" height="34" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/11.png" width="111" height="52" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/12.png" width="85" height="56" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/13.png" width="84" height="31" border="0" alt="Imagem" /></p></div>
		<div class="DivImagens2"><p><img src="imagens/solucoes_corporativas/14.png" width="50" height="50" border="0" alt="Imagem" /></p></div>
	</div>

	<div class="SubTituloPagina">Veja nossas solu��es:</div>

	<div class="Quadro1">
		<div class="QuadroTitulo">Compra Espor�dica - Faturamento Simples</div>
		<div class="QuadroTexto">A solu��o mais indicada � empresa � o Faturamento. <br />Ap�s a compra da Coroa de Flores (ou pela loja virtual ou pelo televendas), nossa empresa formalizar� o pedido por email e solicitar� os dados para faturamento. Os dados necess�rios s�o:<br /><br /><b>Raz�o Social<br />CNPJ<br />Endere�o Completo (Logradouro, N�mero, Completo, Bairro, Cidade, Estado e CEP)<br />Inscri��o Estadual (caso n�o seja isento)</b><br /><br />Ser� enviado Boleto Banc�rio e Nota Fiscal com vencimento padr�o de 15 dias. <br />Trabalhamos com Nota Fiscal Eletr�nica e Boleto Eletr�nico, facilitando o envio.</div>
	</div>

	<div class="Quadro2">
		<div class="QuadroTitulo">Compra Recorrente - Contrato de Presta��o de Servi�o</div>
		<div class="QuadroTexto">Caso a empresa tenha compra recorrente de Coroa de Flores, pode-se solicitar um Acordo Comercial ou Contrato de Presta��o de Servi�os. N�o existe n�mero m�nimo de aquisi��es por m�s.<br /><br />Desta forma, nossa empresa fornecer� a entrega de Coroa de Flores durante todo o m�s e o faturamento ser� realizado uma �nica vez, todo dia 30. Ser� enviada uma Nota Fiscal e um Boleto Banc�rio, com vencimento padr�o de 15 dias.<br /><br />Nossa empresa possui todas as certid�es para cadastro em sistemas corporativas (Sistemas de Compra ou Sistemas Integrados).</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
