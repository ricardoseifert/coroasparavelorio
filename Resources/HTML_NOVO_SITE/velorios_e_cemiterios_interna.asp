<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Desabilitado2">Vel�rios e cemit�rios</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Cemit�rio do Morumbi</div>
</div>
</div>

<div id="VeloriosCemiteriosInterna">
<div class="Div">

	<div class="DivTituloPagina">
	<div class="TituloPagina">Cemit�rio do Morumbi - S�o Paulo - SP</div>
	<div class="Endereco">Rua Deputado La�rcio Corte, 468 Morumbi<br />Telefone: (41) 3322-5544<br />Hor�rio de funcionamento: 08:00 as 18:00</div>
	</div>

	<div class="Quadro">
		<div class="QuadroTexto">
			<div class="QuadroTexto1">Precisa de Coroa de Flores para Curitiba?</div>
			<div class="QuadroTexto2">Entre em contato pelo <a href="" class="QuadroTextoUrl">chat</a> ou Televendas: <b>(41) 4063-9550</b></div>
		</div>
		<div class="QuadroInfo">
			<div class="QuadroInfo1">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico1.png" width="27" height="17" border="0" alt="Frete para o local" /></div>
				<div class="QuadroInfoTexto">Frete para o local: Gr�tis</div>
			</div>
			<div class="QuadroInfo2">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico2.png" width="17" height="17" border="0" alt="Prazo de entrega" /></div>
				<div class="QuadroInfoTexto">Prazo de entrega: 1:00 a 1:30</div>
			</div>
			<div class="QuadroInfo2">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico3.png" width="19" height="17" border="0" alt="Faixa de homenagem" /></div>
				<div class="QuadroInfoTexto">Faixa de homenagem gr�tis</div>
			</div>
		</div>
	</div>

	<div class="Separacao1"></div>

	<div class="produto">
		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

					<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

					<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

					<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

					<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

					<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

				</div></center>

			</div>

			<div id="pequeno1" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="medio1">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="grande1" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado3">
			<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado4">
			<div class="foto"><img src="imagens/foto4.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="VejaMaisCoroas">[+] <a href="" class="VejaMaisCoroas2">Veja mais Coroas de Flores</a></div>

	<div class="Separacao2"></div>

	<div class="Foto">
		<div class="FotoImagem"><img src="imagens/cemiterio_morumbi.png" class="FotoImg" alt="Foto do Cemit�rio do Morumbi" /></div>
		<div class="FotoNomeCidade">
			<div class="FotoNomeCidade2">Cemit�rio do Morumbi</div>
			<div class="FotoTelefone">Rua Deputado La�rcio Corte, 468 - Morumbi 05706-290 <br />Telefone: (41) 3322-5544<br />Hor�rio de funcionamento: 08:00 as 18:00</div>
		</div>
	</div>

	<div class="MapaGoogle"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d115303.55685104543!2d-49.27195302607426!3d-25.4303672747894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce3f5fc090ff1%3A0x3c7a83b0092bb747!2sCuritiba%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1405449324547" frameborder="0" class="MapaGoogle2"></iframe></div>

	<div class="DescricaoTexto2">

		O Cemit�rio Morumbi funciona h� 42 anos e est� localizado em uma das regi�es mais importantes da metr�pole. Este campo santo segue uma estrutura totalmente moderna, onde s�o aliados toda a constru��o com uma natureza dotada de muitas flores e �rvores que s�o muito bem cuidadas.<br /><br />

		Com amplo espa�o, que se aproxima dos 300 mil m� de campo verde, seus jazigos ficam dispostos de forma paralela. Por seguir o modelo de jardim parque, os t�mulos ficam cobertos de gramados e s�o identificados com placas de metal.<br /><br />

		Este modelo adotado pelo Cemit�rio Morumbi procura se distanciar das tradicionais necr�poles que tanto faz com que as pessoas se afastem ao inv�s de se aproximarem. As pessoas podem passear pelo parque jardim, uma vez que seu ambiente foi projetado para que todos se sintam a vontade

	</div>

	<div class="DescricaoTitulo">Vel�rio no Cemit�rio Morumbi</div>
	<div class="DescricaoTexto">

		O Cemit�rio Morumbi disponibiliza sete salas para vel�rio, que est�o equipadas com ar condicionado e ar quente. H� tamb�m salas de repouso e de recep��o para os convidados. Para aqueles que buscam um lugar para orar e refletir sobre a vida, existe uma capela ecum�nica totalmente equipada. Para quem desejar realizar sua visita de carro, o cemit�rio conta com um amplo estacionamento.

	</div>

	<div class="DescricaoTitulo">Floricultura no Cemit�rio Morumbi</div>
	<div class="DescricaoTexto">

		Em momentos dif�ceis n�o h� explica��es para uma dor que se torna maior � medida que o sepultamento se aproxima. No vel�rio, o clima � dos piores, pois muitos choram, outros n�o aceitam e h� aqueles que por motivos maiores nem comparecem nesta cerim�nia que � conhecida como o �ltimo adeus. As flores, por sua vez, s�o recomendadas para aliviar a ang�stia e deixar o ambiente melhor. No site Coroas para Vel�rio, os arranjos s�o acompanhados com faixas para homenagem. O frete, em qualquer lugar do Brasil, � gratuito.

	</div>

	<div class="Quadro2">

		<div class="Quadro2Titulo">Precisa de Coroa de Flores para Cemit�rio Morumbi?</div>

		<div class="Quadro2Div1">
			<div class="Quadro2Telefone">Televendas 24h: <b>0800-777-1986</b></div>
			<div class="Quadro2TelefoneImg"><img src="imagens/telefone_cel.png" class="Quadro2TelefoneImg2" alt="Telefone" /></div>
		</div>

		<div class="Quadro2Chat">
			<div class="Quadro2ChatImg"><img src="imagens/chat_cel.png" width="20" height="20" border="0" alt="Chat" /></div>
			<div class="Quadro2ChatImg2">CHAT online</div>
		</div>

	</div>

	<div class="Quadro2Opcoes">
		<div class="Quadro2Opcoes1">Frete para o local:<br />Gr�tis</div>
		<div class="Quadro2Opcoes1">Prazo de entrega:<br />1:00 a 1:30 </div>
		<div class="Quadro2Opcoes2">Faixa de<br />homenagem gr�tis</div>
	</div>

	<div id="ListagemProdutosVelorios2">
		<div id="ListaProdutos">

			<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta" /></p></div>

			<div class="produto">
				<div class="quadrado1">
					<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do Produto" /></div>
					<div class="titulo">Coroa de Flores Premium - 01</div>
					<div class="tamanho_imagens">

						<center><div class="escolha_tamanho_div">

							<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho" /></div>

							<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

							<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" alt="Tamanho"  /></div>

							<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

							<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" alt="Tamanho"  /></div>

							<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" alt="Tamanho"  /></div>

						</div>
						</center>

					</div>

					<div id="pequeno1_m" class="escondido">
					<div class="tamanho_texto">1,0m x 1,0m</div>
					<div class="valor">150,00</div>
					</div>

					<div id="medio1_m">
					<div class="tamanho_texto">1,2m x 1,0m</div>
					<div class="valor">250,00</div>
					</div>

					<div id="grande1_m" class="escondido">
					<div class="tamanho_texto">1,5m x 1,0m</div>
					<div class="valor">350,00</div>
					</div>

					<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
					<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
				</div>
			</div>

			<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=2','ListaProdutos');" alt="Seta"  /></p></div>

		</div>

		<div class="Div3">
			<div id="VejaMais">
				<div class="div_centraliza">
					<div class="texto"><p>ENVIAR COROA PARA CEMIT�RIO MORUMBI</p></div>
				</div>
			</div>
		</div>

	</div>

	<div class="NaoEncontrou">

		<div class="Texto1">N�o encontrou o seu local?</div>

		<div class="Div2">
		<div class="Texto2">Busque novamente</div>
		<div class="DivBusca">
			<div class="DivBusca2"><input type="text" id="busca2" name="busca2" class="DivBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="DivBusca2"><img src="imagens/busca.png" class="DivBuscar" alt="Buscar" /></div>
		</div>
		</div>

		<div class="Texto3">Ou fale conosco pelo nosso <a href="" class="Url">Atendimento Online</a> ou ligue para nossa Central de Atendimento 24h: 0800-777-1986</div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
