<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Depoimento de clientes</div>
</div>
</div>

<div id="Depoimentos">
<div class="Div">

	<div class="TituloPagina">Depoimento de clientes</div>

	<div class="Div1">

		<div class="Fisica">
			<div class="FisicaImg"><img src="imagens/depoimentos_pessoa_fisica.png" width="80" height="104" border="0" alt="Pessoa f�sica" /></div>
			<div class="FisicaTexto">Pessoas F�sicas</div>
		</div>	

		<div class="Depoimento">

			<div class="Quadrado">
				<div class="Aspas"><img src="imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">Agrade�o a aten��o e a confian�a depositada, pois � muito dif�cil de achar empresas que tem a confian�a e acredita no cliente, o que foi o caso desta empresa. muito obrigado.</div>
			</div>

			<div class="Seta"><img src="imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">ANTONIO ALFREDO CASTRO COIMBRA</div>
			</div>

			<div class="Info">
			<div class="Cidade">BELEM / PA</div>
			</div>

		</div>

		<div class="Depoimento2">

			<div class="Quadrado">
				<div class="Aspas"><img src="imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">Gostaria de agradecer ao Coroas para velorio pela parceria em 2013. Feliz Natal e um Pr�spero Ano Novo para todos da equipe.</div>
			</div>

			<div class="Seta"><img src="imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">BETHANIA BEZERRA / CMA CGM</div>
			</div>

			<div class="Info">
			<div class="Cidade">SANTOS / SP</div>
			</div>

		</div>

		<div class="Mais">
			<div class="Anterior"><img src="imagens/depoimentos_anteriores.png" class="AnteriorProximaImg" alt="Depoimentos anteriores" /></div>
			<div class="Proxima"><img src="imagens/depoimentos_proximos.png" class="AnteriorProximaImg" alt="Pr�ximos depoimentos" /></div>
		</div>

		<div class="DeixarDepoimento"><img src="imagens/depoimentos_deixar_fisica.png" class="DeixarDepoimentoImg" onclick="mostra('deixe_aqui_depoimento');" alt="Deixe seu depoimento como pessoa f�sica" /></div>

	</div>

	<div class="Div2">

		<div class="Fisica">
			<div class="FisicaImg"><img src="imagens/depoimentos_pessoa_juridica.png" width="79" height="105" border="0" alt="Pessoa jur�dica" /></div>
			<div class="FisicaTexto">Pessoas Jur�dicas</div>
		</div>	

		<div class="Depoimento">

			<div class="Quadrado">
				<div class="Aspas"><img src="imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">Agrade�o a aten��o e a confian�a depositada, pois � muito dif�cil de achar empresas que tem a confian�a e acredita no cliente, o que foi o caso desta empresa. muito obrigado.</div>
			</div>

			<div class="Seta"><img src="imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">ANTONIO ALFREDO CASTRO COIMBRA</div>
			</div>

			<div class="Info">
			<div class="Cidade">BELEM / PA</div>
			</div>

		</div>

		<div class="Depoimento2">

			<div class="Quadrado">
				<div class="Aspas"><img src="imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">Gostaria de agradecer ao Coroas para velorio pela parceria em 2013. Feliz Natal e um Pr�spero Ano Novo para todos da equipe.</div>
			</div>

			<div class="Seta"><img src="imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">BETHANIA BEZERRA / CMA CGM</div>
			</div>

			<div class="Info">
			<div class="Cidade">SANTOS / SP</div>
			</div>

		</div>

		<div class="Mais">
			<div class="Anterior"><img src="imagens/depoimentos_anteriores.png" class="AnteriorProximaImg" alt="Depoimentos anteriores" /></div>
			<div class="Proxima"><img src="imagens/depoimentos_proximos.png" class="AnteriorProximaImg" alt="Pr�ximos depoimentos" /></div>
		</div>

		<div class="DeixarDepoimento"><img src="imagens/depoimentos_deixar_juridica.png" class="DeixarDepoimentoImg" onclick="mostra('deixe_aqui_depoimento');" alt="Deixe seu depoimento como pessoa jur�dica" /></div>

	</div>

	<div class="Formulario">
	<div class="Formulario2" id="deixe_aqui_depoimento">

		<div class="TituloPagina2">Deixe aqui o seu Depoimento!</div>

		<div class="FormDiv1">
			<div class="FormNomeCampo">Nome:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nota para nosso servi�o:</div>
			<div class="FormCampo"><select name="nota" id="nota" class="FormCampoSelect"><option value="">Selecione uma nota</option></select></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Depoimento:</div>
			<div class="FormCampo"><textarea id="depoimento" name="depoimento" class="FormCampoTextarea" /></textarea></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput" /></div>
			</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" /></div>

	</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
