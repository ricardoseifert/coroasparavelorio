<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Termos e condi��es especiais</div>
</div>
</div>

<div id="TermosCondicoesComerciais">
<div class="Div">

	<div class="TituloPagina">Termos e condi��es especiais</div>

	<div class="Div1">
		<div class="Div1Texto1">

			O site <b>www.coroasparavelorio.com.br</b> vem por meio desta carta informar aos seus clientes e consumidores as pr�ticas legais de uso de seu portal de vendas, aqui chamadas de Termos de Uso e Condi��es Comerciais.<br /><br />

			Fica desde j� claro que o site Coroas Para Vel�rio poder� a qualquer momento alterar tais pr�ticas sem qualquer tipo de pr�vio aviso aos seus clientes e parceiros. Qualquer tipo de atualiza��o, modifica��o, revis�o ou simples altera��o ficar� a total crit�rio do site Coroas Para Vel�rio.com.br e seus propriet�rios.<br /><br />

			a) O site Coroas Para Vel�rio tem o objetivo �nico de comercializar coroas de flores ou coroas f�nebres para cemit�rios, vel�rios, hospitais, cremat�rios e lugares semelhantes;<br /><br />

			b) Os produtos comercializados pelo site Coroas Para Vel�rio.com.br bem como suas descri��es poder�o ser atualizados periodicamente sem pr�vio aviso aos clientes;<br /><br />

			c) As atualiza��es dos produtos poder�o ser feitas no sentido de melhoria, corre��o, sazonalidade de recursos ou qualquer outra necessidade identificada pelo site;<br /><br />

			d) Todas as informa��es contidas no site Coroas Para Vel�rio.com.br s�o de propriedade particular e est�o proibidas de serem utilizadas para fins p�blicos sem a autoriza��o dos propriet�rios;<br /><br />

			e) Os clientes e usu�rios poder�o utilizar as informa��es contidas no site Coroas Para Vel�rio.com.br com a finalidade particular, exclusivamente, sem qualquer tipo de altera��o ou modifica��o;<br /><br />

			f) As �nicas formas de pagamento aceitas pelo site Coroas Para Vel�rio.com.br ser�o d�bitos em conta, boleto banc�rio, cart�o de cr�dito, cheque e dinheiro. Qualquer outra forma de pagamento ser� vetada;<br /><br />

			g) Os usu�rios e clientes s�o inteiramente respons�veis pelas informa��es pessoais que inserem nos processos de compras, n�o cabendo ao site Coroas Para Vel�rio se responsabilizar pela veracidade das informa��es;<br /><br />

			Agradecemos pela compreens�o,<br />
			Equipe Coroas Para Vel�rio

		</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
