<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Desabilitado2">Coroas de flores</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Coroa de Flores Premium - 04</div>
</div>
</div>

<div id="ProcessoCompra">
<div class="Div">

	<div class="Div1">

		<div class="Div2">

			<div class="FotoProduto2"><p><img src="imagens/pag_produto_foto.png" class="FotoProdutoImg" alt="Foto do produto" /></p></div>

			<div class="NomeProduto">Coroa de Flores Premium - 04</div>
			<div class="TamanhoProduto">Tamanho: M | 1,2m x 1,0m</div>
			<div class="ValorProduto">R$ 250,00</div>

		</div>

		<div class="Quadro">

		<div class="QuadroTamanho">

			<div class="Texto1">Entrega</div>
			<div class="Texto2">Todos os campos s�o obrigat�rios</div>

			<div class="Texto3">Selecione estado e cidade de entrega:</div>
			<div class="DivCampo">
				<div class="DivCampo1"><select name="estado" class="Select1"><option value="">UF</option></select></div>
				<div class="DivCampo2"><select name="cidade" class="Select4"><option value="">Cidade</option></select></div>
			</div>

			<div class="Texto3">Local de entrega:</div>
			<div class="Texto4">Digite o nome ou endere�o do local de entrega desejado. N�o � necess�rio maiores detalhes, n�s entramos em contato com o local para certificar que tudo ocorra bem.</div>
			<div class="DivCampo"><input type="text" name="local_entrega" class="Input1" value="Nome ou endere�o do cemit�rio, vel�rio etc" onfocus="if(this.value=='Nome ou endere�o do cemit�rio, vel�rio etc') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Nome ou endere�o do cemit�rio, vel�rio etc'; }" /></div>

			<div class="Texto5"><b>Cemit�rio Morumbi</b><br />Av. Morumbi, 123 - Morumbi<br />S�o Paulo - SP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CEP: 34322-099</div>

			<div class="DivCampo3">

				<div class="DivCampo4">
					<div class="Texto3">Data da entrega:</div>
					<div class="DivCampo"><input type="date" name="data_entrega" class="Input2" /></div>
				</div>

				<div class="DivCampo5">
					<div class="Texto3">Hora da entrega desejado:</div>
					<div class="DivCampo6">
						<div class="DivCampo7"><select name="hora" class="Select2"><option value="">00</option></select></div>
						<div class="DivCampo8"><select name="minuto" class="Select2"><option value="">00</option></select></div>
					</div>
				</div>

			</div>

			<div class="DivBotao"><img src="imagens/proximo_passo.png" class="DivBotaoImg" onclick="window.open('processo_compra2.asp','_top');" alt="Pr�ximo passo" /></div>

		</div>
		</div>

	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
