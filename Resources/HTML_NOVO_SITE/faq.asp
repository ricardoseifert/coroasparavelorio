<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">D�vidas frequentes</div>
</div>
</div>

<div id="FAQ">
<div class="Div">

	<div class="TituloPagina">D�vidas frequentes</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_cadastro')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_cadastro.png" width="52" height="50" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Cadastro</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_cadastro">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto"><b>Estou no exterior, como fa�o para comprar uma Coroa de Flores?</b><br /><br />Caso voc� resida no exterior temos a op��o de cadastro para quem reside no exterior. Ser�o solicitados dados simples para cadastro (Nome completo, E-mail, telefone de contato no Brasil, Pessoa de contato no Brasil e Skype). Feito o cadastro, ser� poss�vel comprar a coroa de flores de sua prefer�ncia e conferir o status de seu pedido, al�m de tirar d�vidas com nossa Central de Atendimento por meio de Chat online.<br /><br /><b>Por que � necess�rio realizar um cadastro no site?</b><br /><br />O cadastro em nosso site � necess�rio para facilitar a compra e a entrega da coroa de flores, � feito de forma r�pida e simplificada com o objetivo de ter total agilidade na entrega da coroa de flores no local de escolha de nossos clientes.<br /><br />Com o cadastro feito, o cliente poder� ter benef�cios como iniciar um chat online com nossa Central de Atendimento, verificar o status de seus pedidos e alterar informa��es de sua conta.<br /><br /><b>Posso cadastrar minha empresa como cliente do site Coroas Para Vel�rio?</b><br /><br />Sim. Para cadastrar sua empresa � necess�rio efetuar o cadastro em nosso site, selecionar a op��o Pessoa Jur�dica e informar os dados solicitados (Raz�o Social, CNPJ, endere�o, telefone, entre outros). Com o cadastro feito � s� escolher a coroa de flores de sua prefer�ncia e local de entrega.<br /><br />O cadastro de empresas no site Coroas Para Vel�rio.com.br � f�cil, r�pido e seguro.<br /><br /><b>Quais informa��es ser�o solicitadas no Cadastro?</b><br /><br />Ser�o solicitadas informa��es b�sicas (nome completo, endere�o, telefone, entre outros) e ser� criada uma senha de acesso ao site e nome de usu�rio. A partir do primeiro acesso cada cliente poder� verificar o status de seu pedido e comprar futuramente em nosso site sem precisar de novo cadastro.<br /><br />O cadastro no site Coroas Para Vel�rio.com.br � f�cil, r�pido e seguro</div>

		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_como_comprar')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_como_comprar.png" width="54" height="45" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Como Comprar</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_como_comprar">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, felis sit amet luctus bibendum, enim metus vehicula felis, sed sodales augue tellus at felis. Mauris commodo at magna sed sagittis. Curabitur suscipit a neque commodo sagittis. Praesent vel sagittis odio, a aliquet velit. Praesent eget ante ante. Cras posuere eleifend sem at rutrum. Sed ipsum mauris, placerat vitae nibh non, imperdiet faucibus est. Vestibulum molestie justo at enim tincidunt, ut adipiscing felis gravida. Phasellus malesuada accumsan nunc vel porttitor. Sed tempor ante eros, sed mollis dolor fringilla nec. Suspendisse in ante arcu.<br /><br />Nullam eget magna eu lectus faucibus mattis. Sed lacinia facilisis quam sit amet laoreet. Morbi ac egestas ipsum. Ut vestibulum dui quis suscipit convallis. Nulla facilisi. Mauris facilisis risus quis turpis volutpat aliquam. Donec tempus enim diam, eu bibendum ante feugiat at. Suspendisse id semper dui, non rutrum massa. Pellentesque pellentesque auctor erat, quis facilisis tellus pharetra eget.</div>

		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_entrega')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_entrega.png" width="66" height="42" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Entrega</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_entrega">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, felis sit amet luctus bibendum, enim metus vehicula felis, sed sodales augue tellus at felis. Mauris commodo at magna sed sagittis. Curabitur suscipit a neque commodo sagittis. Praesent vel sagittis odio, a aliquet velit. Praesent eget ante ante. Cras posuere eleifend sem at rutrum. Sed ipsum mauris, placerat vitae nibh non, imperdiet faucibus est. Vestibulum molestie justo at enim tincidunt, ut adipiscing felis gravida. Phasellus malesuada accumsan nunc vel porttitor. Sed tempor ante eros, sed mollis dolor fringilla nec. Suspendisse in ante arcu.<br /><br />Nullam eget magna eu lectus faucibus mattis. Sed lacinia facilisis quam sit amet laoreet. Morbi ac egestas ipsum. Ut vestibulum dui quis suscipit convallis. Nulla facilisi. Mauris facilisis risus quis turpis volutpat aliquam. Donec tempus enim diam, eu bibendum ante feugiat at. Suspendisse id semper dui, non rutrum massa. Pellentesque pellentesque auctor erat, quis facilisis tellus pharetra eget.</div>

		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_pagamento')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_pagamento.png" width="41" height="50" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Pagamento</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_pagamento">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, felis sit amet luctus bibendum, enim metus vehicula felis, sed sodales augue tellus at felis. Mauris commodo at magna sed sagittis. Curabitur suscipit a neque commodo sagittis. Praesent vel sagittis odio, a aliquet velit. Praesent eget ante ante. Cras posuere eleifend sem at rutrum. Sed ipsum mauris, placerat vitae nibh non, imperdiet faucibus est. Vestibulum molestie justo at enim tincidunt, ut adipiscing felis gravida. Phasellus malesuada accumsan nunc vel porttitor. Sed tempor ante eros, sed mollis dolor fringilla nec. Suspendisse in ante arcu.<br /><br />Nullam eget magna eu lectus faucibus mattis. Sed lacinia facilisis quam sit amet laoreet. Morbi ac egestas ipsum. Ut vestibulum dui quis suscipit convallis. Nulla facilisi. Mauris facilisis risus quis turpis volutpat aliquam. Donec tempus enim diam, eu bibendum ante feugiat at. Suspendisse id semper dui, non rutrum massa. Pellentesque pellentesque auctor erat, quis facilisis tellus pharetra eget.</div>

		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_meus_pedidos')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_meus_pedidos.png" width="48" height="42" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Meus pedidos</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_meus_pedidos">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, felis sit amet luctus bibendum, enim metus vehicula felis, sed sodales augue tellus at felis. Mauris commodo at magna sed sagittis. Curabitur suscipit a neque commodo sagittis. Praesent vel sagittis odio, a aliquet velit. Praesent eget ante ante. Cras posuere eleifend sem at rutrum. Sed ipsum mauris, placerat vitae nibh non, imperdiet faucibus est. Vestibulum molestie justo at enim tincidunt, ut adipiscing felis gravida. Phasellus malesuada accumsan nunc vel porttitor. Sed tempor ante eros, sed mollis dolor fringilla nec. Suspendisse in ante arcu.<br /><br />Nullam eget magna eu lectus faucibus mattis. Sed lacinia facilisis quam sit amet laoreet. Morbi ac egestas ipsum. Ut vestibulum dui quis suscipit convallis. Nulla facilisi. Mauris facilisis risus quis turpis volutpat aliquam. Donec tempus enim diam, eu bibendum ante feugiat at. Suspendisse id semper dui, non rutrum massa. Pellentesque pellentesque auctor erat, quis facilisis tellus pharetra eget.</div>

		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Opcao" onclick="mostra_esconde('opcao_demais_infos')">
		<div class="OpcaoImagem"><p><img src="imagens/faq_demais_informacoes.png" width="42" height="42" border="0" alt="�cone" /></p></div>
		<div class="Titulo">Demais infos</div>
		<div class="SaibaMais">[+] <span class="SaibaMais2">Saiba mais</span></div>
	</div>

	<div class="OpcaoTexto" id="opcao_demais_infos">

		<div class="OpcaoTextoFita1"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

		<div class="Texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam molestie, felis sit amet luctus bibendum, enim metus vehicula felis, sed sodales augue tellus at felis. Mauris commodo at magna sed sagittis. Curabitur suscipit a neque commodo sagittis. Praesent vel sagittis odio, a aliquet velit. Praesent eget ante ante. Cras posuere eleifend sem at rutrum. Sed ipsum mauris, placerat vitae nibh non, imperdiet faucibus est. Vestibulum molestie justo at enim tincidunt, ut adipiscing felis gravida. Phasellus malesuada accumsan nunc vel porttitor. Sed tempor ante eros, sed mollis dolor fringilla nec. Suspendisse in ante arcu.<br /><br />Nullam eget magna eu lectus faucibus mattis. Sed lacinia facilisis quam sit amet laoreet. Morbi ac egestas ipsum. Ut vestibulum dui quis suscipit convallis. Nulla facilisi. Mauris facilisis risus quis turpis volutpat aliquam. Donec tempus enim diam, eu bibendum ante feugiat at. Suspendisse id semper dui, non rutrum massa. Pellentesque pellentesque auctor erat, quis facilisis tellus pharetra eget.</div>
	
		<div class="OpcaoTextoFita2"><img src="imagens/fita_lateral.png" class="OpcaoTextoImg" alt="Borda" /></div>

	</div>

	<div class="Div6">
	<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
