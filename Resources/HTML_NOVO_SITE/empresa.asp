<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">A empresa</div>
</div>
</div>

<div id="Empresa">
<div class="Div">

	<div class="TituloPagina">A empresa</div>

	<div class="Div1">
		<div class="Div1Texto1">A Coroas Para Vel�rio � a maior empresa de Homenagens F�nebres do Brasil.</div>
		<div class="Div1Texto2">A empresa possui o objetivo de oferecer uma solu��o r�pida, segura e pr�tica de entrega de Coroas de Flores em todos os cemit�rios, vel�rios, hospitais, igrejas e cremat�rios do Brasil.<br /><br />Com uma grande e qualificativa rede de floriculturas, nossa empresa possui opera��o para entregar homenagens onde quer que esteja ocorrendo um vel�rio, sempre com qualidade e pontualidade.</div>
		<div class="Div1Texto3">Equipe Coroas Para Vel�rios</div>
		<div class="Div1Texto2">Coroas de Flores para todos os cemit�rios e vel�rios do Brasil.<br />atendimento@coroasparavelorio.com.br</div>
	</div>

	<div class="Div2">
		<div class="Div2Texto1">Sobre nossos produtos</div>

		<div class="Div2Opcao">
			<div class="Div2OpcaoImg"><img src="imagens/m_frete_gratis.png" class="Div2OpcaoImg2" alt="Frete gr�tis" /></div>
			<div class="Div2OpcaoTexto">Nossas Coroas de Flores possuem FRETE GR�TIS para qualquer cidade do Brasil</div>
		</div>

		<div class="Div2Opcao">
			<div class="Div2OpcaoImg"><img src="imagens/m_faixa_homenagem.png" class="Div2OpcaoImg2" alt="Faixa de homenagem" /></div>
			<div class="Div2OpcaoTexto">Todas as Coroas de Flores acompanham uma faixa de homenagens sem nenhum custo adicional.</div>
		</div>

		<div class="Div2Opcao">
			<div class="Div2OpcaoImg"><img src="imagens/flor.png" class="Div2OpcaoImg2" alt="Flores frescas" /></div>
			<div class="Div2OpcaoTexto">Trabalhamos com flores frescas, garantindo qualidade � Coroa de Flores</div>
		</div>

		<div class="Div2Opcao">
			<div class="Div2OpcaoImg"><img src="imagens/estrela.png" class="Div2OpcaoImg2" alt="Estrela" /></div>
			<div class="Div2OpcaoTexto">Em caso de aus�ncia, as flores ser�o substitu�das por outras de mesma qualidade ou superior.</div>
		</div>

	</div>

	<div class="Div3"><img src="imagens/flor_empresa.png" class="Div3Img" alt="Flor" /></div>

	<div class="Div7">
		<div class="Div7Texto">Conhe�a um pouco mais sobre a nossa empresa assistindo ao v�deo</div>
		<div class="Div7Video"><iframe src="//www.youtube.com/embed/q-fDRJ8ObzA" frameborder="0" allowfullscreen class="Div7Video2" scrolling="no" name="video_sobre"></iframe></div>
	</div>

	<div class="Div4">

		<div class="Div4DivTextos">

			<div class="Div4DivAlinhamento">
				<div class="Div4Missao">Nossa miss�o</div>	
				<div class="Div4MissaoTexto">Possibilitar as pessoas de prestarem suas �ltimas homenagens, sempre com qualidade e pontualidade, aonde quer que seja o local de vel�rio.</div>
			</div>

			<div class="Div4DivAlinhamento">
				<div class="Div4Vissao">Nossa vis�o para 2013</div>	
				<div class="Div4VissaoTexto">Ser reconhecida por nossos clientes como a melhor empresa de homenagens f�nebres do Brasil. <br /><br />Nossos valores, Responsabilidades e diretrizes:<br /><br />&bull; Transpar�ncia, �tica e Honestidade em todas as nossas a��es;<br />&bull; Respeito a todos os nossos clientes, compreendendo o momento delicado;<br />&bull; Facilitar a compra e o pagamento, com processos r�pidos e desburocratizados;<br />&bull; Investir sempre em nossos talentos;<br />&bull; Sempre antecipar nosso cliente, oferecendo informa��es, respostas e praticidade.</div>
			</div>

		</div>

		<div class="Div5">

			<div class="Div5Titulo">Depoimentos</div>	

			<div class="Div5Depoimento">

				<div class="Div5DepoimentoQuadrado">
					<div class="Div5DepoimentoAspas"><img src="imagens/empresa_depoimentos_aspas.png" class="Div5DepoimentoAspasImg" alt="Aspas" /></div>
					<div class="Div5DepoimentoTexto">Agrade�o a aten��o e a confian�a depositada, pois � muito dif�cil de achar empresas que tem a confian�a e acredita no cliente, o que foi o caso desta empresa. muito obrigado.</div>
				</div>

				<div class="Div5DepoimentoSeta"><img src="imagens/empresa_depoimentos_seta.png" class="Div5DepoimentoSetaImg" alt="Seta" /></div>

				<div class="Div5DepoimentoInfo">
				<div class="Div5DepoimentoNome">ANTONIO ALFREDO CASTRO COIMBRA</div>
				</div>

				<div class="Div5DepoimentoInfo">
				<div class="Div5DepoimentoCidade">BELEM / PA</div>
				</div>

			</div>

			<div class="Div5Depoimento2">

				<div class="Div5DepoimentoQuadrado">
					<div class="Div5DepoimentoAspas"><img src="imagens/empresa_depoimentos_aspas.png" class="Div5DepoimentoAspasImg" alt="Aspas" /></div>
					<div class="Div5DepoimentoTexto">Gostaria de agradecer ao Coroas para velorio pela parceria em 2013. Feliz Natal e um Pr�spero Ano Novo para todos da equipe.</div>
				</div>

				<div class="Div5DepoimentoSeta"><img src="imagens/empresa_depoimentos_seta.png" class="Div5DepoimentoSetaImg" alt="Seta" /></div>

				<div class="Div5DepoimentoInfo">
				<div class="Div5DepoimentoNome">BETHANIA BEZERRA / CMA CGM</div>
				</div>

				<div class="Div5DepoimentoInfo">
				<div class="Div5DepoimentoCidade">SANTOS / SP</div>
				</div>

			</div>

			<div class="Div5DepoimentoMais">Ver mais depoimentos</div>

		</div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
