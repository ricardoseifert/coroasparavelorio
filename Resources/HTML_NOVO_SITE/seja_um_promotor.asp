<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Seja um promotor</div>
</div>
</div>

<div id="SejaParceira">
<div class="Div">

	<div class="TituloPagina">Seja um promotor</div>

	<div class="Div1">
		<div class="Div1Texto1">Voc� gostaria de ser um Promotor do Site Coroas Para Vel�rio?<br /><br />Al�m de ajudar outras pessoas com a aquisi��o deste tipo de produto em momentos delicados, voc� ainda recebe uma comiss�o para cada venda realizada.<br /><br />Para participar do nosso quadro de vendedores, basta preencher o cadastro abaixo (v�lido apenas para pessoa f�sica).<br /><br />Nossa equipe entrar� em contato com voc� para conhecer um pouco mais sobre a sua personalidade e sobre os seus atributos. Ap�s a entrevista, voc� receber� um C�digo de Identifica��o (ou C�digo Promocional).<br /><br />Toda vez que uma compra online ou pelo televendas for realizada com o seu c�digo, ser� creditada a comiss�o em sua conta.<br /><br />Preencha o formul�rio abaixo para receber mais informa��es</div>
	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Nome completo:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CPF:</div>
			<div class="FormCampo"><input type="text" id="cpf" name="cpf" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CEP:</div>
			<div class="FormCampo"><input type="text" id="cep" name="cep" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Endere�o:</div>
			<div class="FormCampo"><input type="text" id="endereco" name="endereco" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">N�mero:</div>
			<div class="FormCampo"><input type="text" id="numero" name="numero" class="FormCampoInputNC" /></div>
			<div class="FormNomeCampo2">Compl.</div>
			<div class="FormCampo"><input type="text" id="complemento" name="complemento" class="FormCampoInputNC" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Bairro:</div>
			<div class="FormCampo"><input type="text" id="bairro" name="bairro" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Estado:</div>
			<div class="FormCampo"><select id="estado" name="estado" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone residencial:</div>
			<div class="FormCampo"><input type="text" id="telefone_residencial" name="telefone_residencial" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone celular:</div>
			<div class="FormCampo"><input type="text" id="telefone_celular" name="telefone_celular" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nextel:</div>
			<div class="FormCampo"><input type="text" id="nextel" name="nextel" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Como nos conheceu?</div>
			<div class="FormCampo"><select id="como_nos_conheceu" name="como_nos_conheceu" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput" /></div>
				<div class="FormValidacaoTexto2"><input type="radio" id="termos" name="termos" /> Li e estou de acordo com os <a href="" class="FormValidacaoTexto3">Termos e Condi��es Comerciais.</a></div>
			</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="CADASTRAR" class="FormBotao" /></div>

	</div>

	<div class="Div1">
		<div class="Div1Texto1">Agradecemos pela prefer�ncia!<br />Equipe Coroas Para Vel�rio.</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
