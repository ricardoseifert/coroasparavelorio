<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Desabilitado2">Cidades e regi�es</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Curitiba</div>
</div>
</div>

<div id="CidadesRegioesInterna">
<div class="Div">

	<div class="DivTituloPagina">
	<div class="TituloPagina"><span class="TituloPagina2">Coroa de Flores para</span> Curitiba</div>
	<div class="VejaNossasCoroas"><a href="" class="FiltroSelecionado">Veja lista de locais de Curitiba</a></div>
	</div>

	<div class="Quadro">
		<div class="QuadroTexto">
			<div class="QuadroTexto1">Precisa de Coroa de Flores para Curitiba?</div>
			<div class="QuadroTexto2">Entre em contato pelo <a href="" class="QuadroTextoUrl">chat</a> ou Televendas: <b>(41) 4063-9550</b></div>
		</div>
		<div class="QuadroInfo">
			<div class="QuadroInfo1">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico1.png" width="27" height="17" border="0" alt="Frete para o local" /></div>
				<div class="QuadroInfoTexto">Frete para o local: Gr�tis</div>
			</div>
			<div class="QuadroInfo2">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico2.png" width="17" height="17" border="0" alt="Prazo de entrega" /></div>
				<div class="QuadroInfoTexto">Prazo de entrega: 1:00 a 1:30</div>
			</div>
			<div class="QuadroInfo2">
				<div class="QuadroInfoImg"><img src="imagens/cidades_ico3.png" width="19" height="17" border="0" alt="Faixa de homenagem" /></div>
				<div class="QuadroInfoTexto">Faixa de homenagem gr�tis</div>
			</div>
		</div>
	</div>

	<div class="Separacao1"></div>

	<div class="produto">
		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

					<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

					<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

					<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

					<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

					<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

				</div></center>

			</div>

			<div id="pequeno1" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="medio1">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="grande1" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado3">
			<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="produto">
		<div class="quadrado4">
			<div class="foto"><img src="imagens/foto4.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>
	</div>

	<div class="VejaMaisCoroas">[+] <a href="" class="VejaMaisCoroas2">Veja mais Coroas de Flores</a></div>

	<div class="Separacao2"></div>

	<div class="Foto">
		<div class="FotoImagem"><img src="imagens/foto_local.png" class="FotoImg" alt="Foto de Curitiba" /></div>
		<div class="FotoNomeCidade">
			<div class="FotoNomeCidade2">CURITIBA - PR</div>
			<div class="FotoTelefone">Televendas: (41) 4063-9550</div>
		</div>
	</div>

	<div class="MapaGoogle"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d115303.55685104543!2d-49.27195302607426!3d-25.4303672747894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce3f5fc090ff1%3A0x3c7a83b0092bb747!2sCuritiba%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1405449324547" frameborder="0" class="MapaGoogle2"></iframe></div>

	<div class="DescricaoTitulo1">Cidade de Curitiba</div>
	<div class="DescricaoTexto">

		O estado do Paran� � o mais novo estado da Regi�o Sul do Brasil, logo depois do Rio Grande do Sul e Santa Catarina. J� a Cidade de Curitiba � a oitava cidade mais populosa do Brasil e a maior do sul do pa�s.<br /><br />

		A capital do Paran� ao longo dos �ltimos anos tem se consolidado como a cidade mais rica do Sul do pa�s e a 4� em n�vel nacional. A Cidade de Curitiba tamb�m tem altos �ndices de educa��o, fazendo com que o �ndice de analfabetismo seja um dos menores, isso consequentemente faz com que ela tenha a melhor qualidade na educa��o b�sica entre as capitais.<br /><br /> 

		Curitiba tamb�m foi citada em uma recente pesquisa publicada pela revista Forbes, como a 3 � cidade mais sagaz do mundo, que considera esperta a cidade que se preocupa, de forma conjunta, em ser ecologicamente sustent�vel, com qualidade de vida, boa infraestrutura e dinamismo econ�mico.<br /><br />

		Curitiba � tamb�m uma das cidades brasileiras mais influentes no cen�rio global, sendo considerado um dos centros mais globalizados do planeta, recebendo a classifica��o de cidade global gama.

	</div>

	<div class="DescricaoTitulo2">Cemit�rios da Cidade de Curitiba</div>
	<div class="DescricaoTexto">

		Os Cemit�rios da Cidade de Curitiba foram projetados com uma �nica preocupa��o: garantir o conforto e seguran�a daqueles que querem prestar uma �ltima homenagem ao ente querido. A miss�o n�o s� dos Cemit�rios, m�s tamb�m dos Vel�rios � ir al�m da disponibilidade de um servi�o de qualidade aos seus clientes, seus comprometimentos com o meio ambiente e o desenvolvimento social pode ser notado por a��es pr�prias ou atrav�s do apoio a entidades filantr�picas que atuam socialmente na Cidade. O Cemit�rio Jardim da Saudade representa a uni�o entre a natureza e completas estruturas de atendimentos e servi�os, uma vez que s�o aliados conforto e modernidade.

	</div>

	<div class="DescricaoTitulo2">Floricultura Curitiba</div>
	<div class="DescricaoTexto">

		O site Coroas para Vel�rio faz entregas de arranjos de flores para os quase 2 milh�es de habitantes que se encontram em Curitiba, com o intuito de facilitar um momento t�o triste, mas que merece ser encerrado com uma homenagem especial

	</div>

	<div class="Separacao2"></div>

	<div class="ListaLocais">Veja a lista de locais em CURITIBA - PR</div>

	<div class="Filtro">
		<div class="FiltroVer">Ver:</div>
		<div class="FiltroOpcoes"><a href="" class="FiltroSelecionado">Todos</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Cemit�rios</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Capelas</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Hospitais</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Funer�rias</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Igrejas</a></div>
	</div>

	<div class="ResulBusca">
		<div class="ResulBusca2">buscar nos resultados:</div>
		<div class="ResulBusca3">
			<div class="DivBuscaResul2"><input type="text" name="busca_resultados" class="DivBuscaResulInput" value="insira palavra para busca nos resultados" onfocus="if(this.value=='insira palavra para busca nos resultados') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'insira palavra para busca nos resultados'; }" /></div>
			<div class="DivBuscaResul2"><input type="button" value="OK" class="DivBuscarResul" /></div>
		</div>
	</div>

	<div class="ListaLocaisResul">

		<div class="ListaLocaisRTitulo">Cemit�rios</div>

		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Saudade / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Parque Senhor do Bonfim / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Universal Necr�pole Ecum�nica Vertical / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Luterano / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Paz / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Saudade / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Parque Senhor do Bonfim / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Universal Necr�pole Ecum�nica Vertical / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Luterano / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Paz / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

		<div class="ListaLocaisRTitulo2">Capetas</div>

		<div class="ListaLocaisRTexto">Capela Divina Miseric�rdia / PR</div>
		<div class="ListaLocaisRTexto">Capela Mission�ria / PR</div>
		<div class="ListaLocaisRTexto">Capela Nossa Senhora Aparecida / PR</div>
		<div class="ListaLocaisRTexto">Capela da Luz / PR</div>
		<div class="ListaLocaisRTexto">Capela Imaculada da Concei��o / PR</div>
		<div class="ListaLocaisRTexto">Capela Divina Miseric�rdia / PR</div>
		<div class="ListaLocaisRTexto">Capela Mission�ria / PR</div>
		<div class="ListaLocaisRTexto">Capela Nossa Senhora Aparecida / PR</div>
		<div class="ListaLocaisRTexto">Capela da Luz / PR</div>
		<div class="ListaLocaisRTexto">Capela Imaculada da Concei��o / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

		<div class="ListaLocaisRTitulo2">Hospitais</div>

		<div class="ListaLocaisRTexto">Hospital Erasto Gaertner / PR</div>
		<div class="ListaLocaisRTexto">Hospital Santa Cruz / PR</div>
		<div class="ListaLocaisRTexto">Hospital Nossa Senhora das Gra�as / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Evang�lico de Curitiba / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Cajuru / PR</div>
		<div class="ListaLocaisRTexto">Hospital Erasto Gaertner / PR</div>
		<div class="ListaLocaisRTexto">Hospital Santa Cruz / PR</div>
		<div class="ListaLocaisRTexto">Hospital Nossa Senhora das Gra�as / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Evang�lico de Curitiba / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Cajuru / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
