<!--#include file="topo.asp" -->

<div id="ComoComprar">
	<div class="Div">
		<div class="TituloPagina">Coroas de flores</div>

		<div class="CoroasFiltro">
		<div class="CoroasFiltro1"><img src="imagens/coroas_filtro1.png" class="CoroasFiltroImg" onclick="mostra_esconde('BarraFiltro2');mostra_esconde('SetaFiltro2');esconde('BarraFiltro');esconde('SetaFiltro');" alt="Filtro" /></div>
		<div class="CoroasFiltro2"><img src="imagens/coroas_filtro2.png" class="CoroasFiltroImg" onclick="mostra_esconde('BarraFiltro');mostra_esconde('SetaFiltro');esconde('BarraFiltro2');esconde('SetaFiltro2');" alt="Filtro" /></div>
		</div>

		<div class="CoroasFiltroSeta" id="SetaFiltro"><img src="imagens/coroas_filtro_seta.png" class="CoroasFiltroSetaImg" alt="Seta" /></div>
		<div class="CoroasFiltroBarra" id="BarraFiltro">
			<div class="CoroasFiltroBarra1">PARA <b>EMPRESAS</b></div>
			<div class="CoroasFiltroBarra1">PARA <b>AMIGOS</b></div>
			<div class="CoroasFiltroBarra2">PARA <b>FAMILIARES</b></div>
		</div>

		<div class="CoroasFiltroSeta2" id="SetaFiltro2"><img src="imagens/coroas_filtro_seta.png" class="CoroasFiltroSetaImg" alt="Seta" /></div>
		<div class="CoroasFiltroBarra" id="BarraFiltro2">
			<div class="CoroasFiltroBarra1">MAIS VENDIDAS</div>
			<div class="CoroasFiltroBarra1">MENOR PRE�O</div>
			<div class="CoroasFiltroBarra2">MAIOR PRE�O</div>
		</div>

	</div>
</div>

<div id="ListagemProdutosCoroasCel">
	<div class="div_centraliza">

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">

					<center><div class="escolha_tamanho_div">

						<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

						<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

						<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

						<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

					</div>
					</center>

				</div>

				<div id="pequeno1" class="escondido">
				<div class="tamanho_texto">1,0m x 1,0m</div>
				<div class="valor">150,00</div>
				</div>

				<div id="medio1">
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				</div>

				<div id="grande1" class="escondido">
				<div class="tamanho_texto">1,5m x 1,0m</div>
				<div class="valor">350,00</div>
				</div>

				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado2">
				<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado3">
				<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado4">
				<div class="foto"><img src="imagens/foto4.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

	</div>
</div>

<div id="VejaMais">
	<div class="div_centraliza">
		<div class="texto"><p>CARREGAR MAIS COROAS DE FLORES</p></div>
	</div>
</div>

<div id="CoroasDepoimentos">
	<div class="div_centraliza">

		<div class="depoimentos">
			<div class="titulo">Depoimento de Clientes</div>
			<div class="quadrado" id="ListaDepoimentos">

				<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta"  /></p></div>

				<div class="quadrado_cor">
					<div class="texto">�Muito obrigada e quero dizer que me surpreendi com o atendimento. R�pido e muito eficiente... Num momento de dor, foi tudo que precisava. A fam�lia adorou a homenagem. Obrigada! Lucineide�</div>
					<div class="iten_avaliado"><b>Item avaliado:</b> Site Coroas para Vel�rio</div>
					<div class="nome_cidade"><b>Ant�nio - ITA�</b><br />S�O PAULO - 12/12/12</div>
				</div>

				<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('depoimentos.asp?pag=2','ListaDepoimentos');" alt="Seta"  /></p></div>

			</div>
		</div>

	</div>
</div>

<!--#include file="rodape.asp" -->
