<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Seja um promotor</div>
</div>
</div>

<div id="SejaParceira">
<div class="Div">

	<div class="TituloPagina">Seja um promotor</div>

	<div class="Div1">
		<div class="Div1Texto1">Voc� gostaria de ser um Promotor do Site Coroas Para Vel�rio?<br /><br />Al�m de ajudar outras pessoas com a aquisi��o deste tipo de produto em momentos delicados, voc� ainda recebe uma comiss�o para cada venda realizada.<br /><br />Para participar do nosso quadro de vendedores, basta preencher o cadastro abaixo (v�lido apenas para pessoa f�sica).<br /><br />Nossa equipe entrar� em contato com voc� para conhecer um pouco mais sobre a sua personalidade e sobre os seus atributos. Ap�s a entrevista, voc� receber� um C�digo de Identifica��o (ou C�digo Promocional).<br /><br />Toda vez que uma compra online ou pelo televendas for realizada com o seu c�digo, ser� creditada a comiss�o em sua conta.<br /><br />Preencha o formul�rio abaixo para receber mais informa��es</div>
	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Nome completo:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput2" onfocus="esconde_validacao('nome','1');" /></div>
			<div id="aviso_nome" class="FormCampoObrigatorio" onclick="esconde_validacao('nome','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CPF:</div>
			<div class="FormCampo"><input type="text" id="cpf" name="cpf" class="FormCampoInput2" onfocus="esconde_validacao('cpf','1');" /></div>
			<div id="aviso_cpf" class="FormCampoObrigatorio" onclick="esconde_validacao('cpf','1');">Campo obrigat�rio. Digite um CPF v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CEP:</div>
			<div class="FormCampo"><input type="text" id="cep" name="cep" class="FormCampoInput2" onfocus="esconde_validacao('cep','1');" /></div>
			<div id="aviso_cep" class="FormCampoObrigatorio" onclick="esconde_validacao('cep','1');">Campo obrigat�rio. Digite um CEP v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Endere�o:</div>
			<div class="FormCampo"><input type="text" id="endereco" name="endereco" class="FormCampoInput2" onfocus="esconde_validacao('endereco','1');" /></div>
			<div id="aviso_endereco" class="FormCampoObrigatorio" onclick="esconde_validacao('endereco','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">N�mero:</div>
			<div class="FormCampo"><input type="text" id="numero" name="numero" class="FormCampoInputNC2" onfocus="esconde_validacao('numero','1');" /></div>
			<div class="FormNomeCampo2">Compl.</div>
			<div class="FormCampo"><input type="text" id="complemento" name="complemento" class="FormCampoInputNC" /></div>
			<div id="aviso_numero" class="FormCampoObrigatorio" onclick="esconde_validacao('numero','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Bairro:</div>
			<div class="FormCampo"><input type="text" id="bairro" name="bairro" class="FormCampoInput2" onfocus="esconde_validacao('bairro','1');" /></div>
			<div id="aviso_bairro" class="FormCampoObrigatorio" onclick="esconde_validacao('bairro','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput2" onfocus="esconde_validacao('cidade','1');" /></div>
			<div id="aviso_cidade" class="FormCampoObrigatorio" onclick="esconde_validacao('cidade','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Estado:</div>
			<div class="FormCampo"><select id="estado" name="estado" class="FormCampoSelect2" onfocus="esconde_validacao('estado','4');"><option value=""></option></select></div>
			<div id="aviso_estado" class="FormCampoObrigatorio" onclick="esconde_validacao('estado','4');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone residencial:</div>
			<div class="FormCampo"><input type="text" id="telefone_residencial" name="telefone_residencial" class="FormCampoInput2" onfocus="esconde_validacao('telefone_residencial','1');" /></div>
			<div id="aviso_telefone_residencial" class="FormCampoObrigatorio" onclick="esconde_validacao('telefone_residencial','1');">Campo obrigat�rio. Digite um n�mero v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone celular:</div>
			<div class="FormCampo"><input type="text" id="telefone_celular" name="telefone_celular" class="FormCampoInput2" onfocus="esconde_validacao('telefone_celular','1');" /></div>
			<div id="aviso_telefone_celular" class="FormCampoObrigatorio" onclick="esconde_validacao('telefone_celular','1');">Campo obrigat�rio. Digite um n�mero v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nextel:</div>
			<div class="FormCampo"><input type="text" id="nextel" name="nextel" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput2" onfocus="esconde_validacao('email','1');" /></div>
			<div id="aviso_email" class="FormCampoObrigatorio" onclick="esconde_validacao('email','1');">Campo obrigat�rio.  Digite um email v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Como nos conheceu?</div>
			<div class="FormCampo"><select id="como_nos_conheceu" name="como_nos_conheceu" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput2" onfocus="esconde_validacao('validacao','1');" /></div>
				<div class="FormValidacaoTexto4" id="li_aceito_termos" onclick="esconde_validacao('li_aceito_termos','5');"><input type="radio" id="termos" name="termos" /> Li e estou de acordo com os <a href="" class="FormValidacaoTexto3">Termos e Condi��es Comerciais.</a></div>
			</div>
			<div id="aviso_validacao" class="FormCampoObrigatorio2" onclick="esconde_validacao('validacao','1');">Campo obrigat�rio. Soma n�o confere.</div>
			<div id="aviso_li_aceito_termos" class="FormCampoObrigatorio3">Campo obrigat�rio.</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="CADASTRAR" class="FormBotao" /></div>

	</div>

	<div class="Div1">
		<div class="Div1Texto1">Agradecemos pela prefer�ncia!<br />Equipe Coroas Para Vel�rio.</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
