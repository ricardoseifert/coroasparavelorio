<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Vel�rios e cemit�rios</div>
</div>
</div>

<div id="VeloriosCemiterios">
<div class="Div">

	<div class="TituloPagina">Vel�rios e cemit�rios em todo o pa�s</div>
	<div class="VejaNossasCoroas">Veja nossas <a href="" class="FiltroSelecionado">Coroas de Flores</a></div>

	<div class="Texto">Entregamos nossas Coroas de Flores gratuitamente para todo Brasil. <br />Voc� pode encontrar todos os locais cadastrados procurando por estado e cidade, ou encontrar o local desejado digitando o nome ou endere�o na busca.</div>

	<div class="Texto1">Procure por estado e veja os locais cadastrados em cada cidade</div>

	<div class="Busca">
		<div class="Busca3">
			<div class="BuscaTexto">Estado</div>
			<div class="DivOpcaoBusca"><select name="estado" class="DivOpcaoBuscaSelect">

				<option value=""></option>
				<option value="AP">Amap�</option>
				<option value="AC">Acre</option>
				<option value="AL">Alagoas</option>
				<option value="AM">Amazonas</option>
				<option value="BA">Bahia</option>
				<option value="CE">Cear�</option>
				<option value="DF">Distrito Federal</option>
				<option value="ES">Esp�rito Santo</option>
				<option value="GO">Goi�s</option>
				<option value="MA">Maranh�o</option>
				<option value="MG">Minas Gerais</option>
				<option value="MS">Mato Grosso do Sul</option>
				<option value="MT">Mato Grosso</option>
				<option value="PA">Par�</option>
				<option value="PB">Para�ba</option>
				<option value="PE">Pernambuco</option>
				<option value="PI">Piau�</option>
				<option value="PR">Paran�</option>
				<option value="RJ">Rio de Janeiro</option>
				<option value="RN">Rio Grande do Norte</option>
				<option value="RO">Rond�nia</option>
				<option value="RR">Roraima</option>
				<option value="RS">Rio Grande do Sul</option>
				<option value="SC">Santa Catarina</option>
				<option value="SE">Sergipe</option>
				<option value="SP">S�o Paulo</option>
				<option value="TO">Tocantins</option>

			</select></div>
		</div>
		<div class="Busca3">
			<div class="BuscaTexto2">Cidade</div>
			<div class="DivOpcaoBusca"><select name="cidade" class="DivOpcaoBuscaSelect"><option value=""></option></select></div>
		</div>
		<div class="DivOpcaoBusca2"><input type="button" value="BUSCAR" class="DivOpcaoBuscar" onclick="mostra('ResultadosBusca');" /></div>
	</div>

	<div class="Busca2">

		<div id="ResultadosBusca">

			<div class="ResulBLinha1">
				<div class="ResulBTitulo">Resultado de busca para  'S�o paulo'</div>
				<div class="ResulBBusca">
					<div class="ResulBBusca2">buscar nos resultados:</div>
					<div class="ResulBBusca3">
						<div class="DivBuscaResul2"><input type="text" name="busca_resultados" class="DivBuscaResulInput" value="" /></div>
						<div class="DivBuscaResul2"><input type="button" value="BUSCAR" class="DivBuscarResul" /></div>
					</div>
				</div>
			</div>

			<div class="Resultados">
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados2"><b>Benefic�ncia Portuguesa</b><br />Avenida Dr Bernardino de Campos, 47<br />Vl Belmiro 11065-003<br /><a href="" class="FiltroSelecionado">+ Veja mais</a></div>
				<div class="Resultados3"><a href="" class="FiltroSelecionado">Ver mais resultados</a></div>
			</div>

		</div>

	</div>

	<div class="NaoEncontrou">

		<div class="Texto1">N�o encontrou o seu local?</div>

		<div class="Div2">
		<div class="Texto2">Busque novamente</div>
		<div class="DivBusca">
			<div class="DivBusca2"><input type="text" id="busca2" name="busca2" class="DivBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="DivBusca2"><img src="imagens/busca.png" class="DivBuscar" alt="Buscar" /></div>
		</div>
		</div>

		<div class="Texto3">Ou fale conosco pelo nosso <a href="" class="Url">Atendimento Online</a> ou ligue para nossa Central de Atendimento 24h: 0800-777-1986</div>

	</div>

	<div class="DivBanner">
		<div class="Div6"></div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
