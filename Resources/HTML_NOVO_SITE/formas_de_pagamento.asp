<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Formas de pagamento</div>
</div>
</div>

<div id="FormasPagamento">
<div class="Div">

	<div class="TituloPagina">Formas de pagamento</div>

	<div class="Div1">
		<div class="Div1Texto1">[Inserir informa��es detalhadas de todas as formas de pagamento]  [incluir um link para Solu��es corporativas assim que citado no texto sobre]<br /><br />Oferecemos v�rias formas de pagamento. Pela internet ou telefone, voc� pode pagar com [citar todas as formas de pagamento pela internet e telefone] Boleto, Transfer�ncia Online ou com o Cart�o de Cr�dito que voc� j� trabalha.<br /><br />Temos planos especiais para empresas. Conhe�a nossas Solu��es corporativas.<br /><br />Conhe�a cada uma das formas de pagamento aceitas:</div>

		<div class="Opcoes">

			<div class="Opcoes1">
			<div class="OpcoesImg"><img src="imagens/icone_formas_pagamento.png" width="132" height="73" border="0" alt="Icone" /></div>
			<div class="OpcoesTexto"><b>Cart�o de cr�dito</b><br /><br />[texto exemplo] Pague suas compras com cart�o de cr�dito � vista, parcelando em at� 6 vezes sem juros ou em at� 12 vezes com juros. Aceitamos as bandeiras: Visa, Mastercard, American Express, Dinners Club International ou Hipercard.<br />Dispon�vel para compras de pessoas f�sicas e jur�dicas.<br />[indicar como funciona o prazo de entrega]</div>
			</div>

			<div class="Opcoes1">
			<div class="OpcoesImg"><img src="imagens/icone_formas_pagamento.png" width="132" height="73" border="0" alt="Icone" /></div>
			<div class="OpcoesTexto"><b>Cart�o de cr�dito</b><br /><br />[texto exemplo] Pague suas compras com cart�o de cr�dito � vista, parcelando em at� 6 vezes sem juros ou em at� 12 vezes com juros. Aceitamos as bandeiras: Visa, Mastercard, American Express, Dinners Club International ou Hipercard.<br />Dispon�vel para compras de pessoas f�sicas e jur�dicas.<br />[indicar como funciona o prazo de entrega]</div>
			</div>

			<div class="Opcoes2">
			<div class="OpcoesImg"><img src="imagens/icone_formas_pagamento.png" width="132" height="73" border="0" alt="Icone" /></div>
			<div class="OpcoesTexto"><b>Cart�o de cr�dito</b><br /><br />[texto exemplo] Pague suas compras com cart�o de cr�dito � vista, parcelando em at� 6 vezes sem juros ou em at� 12 vezes com juros. Aceitamos as bandeiras: Visa, Mastercard, American Express, Dinners Club International ou Hipercard.<br />Dispon�vel para compras de pessoas f�sicas e jur�dicas.<br />[indicar como funciona o prazo de entrega]</div>
			</div>

		</div>

		<div class="SiteBlindado">
		<div class="SiteBlindadoSeguranca">SEGURAN�A</div>
		<div class="SiteBlindadoImagem"><img src="imagens/site_blindado4.png" width="140" height="28" border="0" alt="Icone" /></div>
		<div class="SiteBlindadoTexto">[texto sobre seguran�a e selo site blindado] Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere purus est, id porta magna pulvinar sit amet. Curabitur vestibulum, nisi nec accumsan mattis, lacus massa aliquam elit.</div>
		</div>

	</div>

	<div id="AindaTemDuvida">

		<div class="Div2Texto1">Ainda possui d�vidas?<br />Entre em contato conosco pelo <b>Chat</b> ou ligue para nossa <b>Central de Atendimento:</b> 0800-777-1986 <b>(24h)</b></div>

		<div class="Div2Texto2">Ou acesse nossa se��o de <a href="" class="Div2Texto3"><b>Perguntas Frequentes</b></a>.<br /><br />Para realizar a sua homenagem, veja nossa rela��o de <a href="" class="Div2Texto3"><b>Coroas de Flores</b></a>.</div>

		<div class="Div2Texto4">Procure aqui o cemit�rio, vel�rio, hospital, cremat�rio ou cidade que deseja enviar coroas de flores:</div>

		<div class="DivBusca">
			<div class="DivBusca2"><input type="text" id="busca2" name="busca2" class="DivBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="DivBusca2"><img src="imagens/busca.png" class="DivBuscar" alt="Buscar" /></div>
		</div>

	</div>

	<div class="Div3">
		<div id="VejaMais">
			<div class="div_centraliza">
				<div class="texto"><p>VER COROAS DE FLORES</p></div>
			</div>
		</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
