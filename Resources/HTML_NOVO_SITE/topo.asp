<% Response.expires = 0 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Coroa de Flores para todo Brasil</title>

<meta http-equiv="Content-Language" content="pt-br" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="robots" content="noindex,nofollow,nosnippet,noarchive,noimageindex" />

<meta name="viewport" content="width=device-width" />

<link href='http://fonts.googleapis.com/css?family=Yanone Kaffeesatz' rel='stylesheet' type='text/css' />

<link rel="stylesheet" media="screen and (min-width: 1024px)" href="css/style_1024.css" />
<link rel="stylesheet" media="screen and (min-width: 800px) and (max-width:1024px)" href="css/style_800.css" />
<link rel="stylesheet" media="screen and (min-width: 460px) and (max-width:800px)" href="css/style_460.css" />
<link rel="stylesheet" media="screen and (min-width: 1px) and (max-width:460px)" href="css/style_0.css" />

<script language="JavaScript" type="text/javascript" src="js/scripts.js"></script>

</head><body style="margin:0">

<div id="menuMobile">

	<div class="Busca">
		<div class="BuscaAlign"><input type="text" id="busca" name="busca" class="BuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
		<div class="BuscaAlign"><img src="imagens/busca.png" class="BuscaBotao" alt="Buscar" /></div>
	</div>

	<div class="Opcoes">COROAS DE FLORES</div>
	<div class="Opcoes">ENTREGA</div>
	<div class="Opcoes">FORMAS DE PAGAMENTO</div>
	<div class="Opcoes">VEL�RIOS E CEMIT�RIOS</div>
	<div class="Opcoes">FRASES DE HOMENAGEM</div>
	<div class="Opcoes">MAIS VENDIDAS</div>
	<div class="Opcoes">LOGIN</div>

	<div class="Sombra">&nbsp;</div>

</div>

<div id="menuMobile2" onclick="esconde('menuMobile');esconde('menuMobile2')">&nbsp;</div>

<div id="TopoFaixaPreta">
	<div class="div_centraliza">
		<div class="div_opcoes">Entregas para todo o Brasil.</div>
		<div class="div_opcoes"><u>Veja as cidades</u></div>
		<div class="div_separacao"><img src="imagens/separacao_topo.png" class="imagem_separacao" alt="Separa��o" /></div>
		<div class="div_opcoes">Televendas 24 hs: 0800-777-1986</div>
		<div class="div_separacao"><img src="imagens/separacao_topo.png" class="imagem_separacao" alt="Separa��o" /></div>
		<div class="div_opcoes">Entrega 24h por dia</div>
		<div class="div_separacao"><img src="imagens/separacao_topo.png" class="imagem_separacao" alt="Separa��o" /></div>
		<div class="div_opcoes2">Por favor, <u>Identifique-se</u></div>
	</div>
</div>

<div class="TopoFaixaPretaTamanho"></div>

<div id="FaixaTelefone">
	<div class="Centraliza">
		<div class="Div1">
			<div class="Telefone">Televendas 24h: <b>0800-777-1986</b></div>
			<div class="TelefoneImg"><img src="imagens/telefone_cel.png" class="TelefoneImg2" alt="Telefone" /></div>
		</div>

		<div class="Chat">
			<div class="ChatImg"><img src="imagens/chat_cel.png" width="20" height="20" border="0" alt="Chat" /></div>
			<div class="ChatImg2">CHAT online</div>
		</div>
	</div>
</div>

<div id="Topo">
	<div class="div_centraliza">
		<div class="MenuCel"><img src="imagens/menu_celular2.png" class="MenuCelImg" onclick="mostra('menuMobile');mostra('menuMobile2');" alt="Menu" /></div>
		<div class="div_logo"><img src="imagens/logo.png" class="imagem_logo" alt="Logo" /></div>

		<div class="CarrinhoM">
		<img src="imagens/carrinho.png" class="CarrinhoM2" alt="Carrinho" />
		<div class="CarrinhoMQtde">1</div>
		</div>
		
		<div class="telefones">
			<font class="televendas">Televendas 24h</font><br />
			<font class="telefone">0800-777-1986</font><br />
			<font class="telefones_regiao">Veja o telefone de sua Regi�o</font>
		</div>
		<div class="carrinho">
			<div class="carrinho_flor"><img src="imagens/flor_topo.png" class="carrinho_flor_imagem" alt="Carrinho" /></div>
			<div class="carrinho_qtde">0</div>
		</div>
	</div>
</div>

<div id="Menu">
	<div class="div_centraliza">

		<div class="div_texto0" onclick="mostra_esconde('Menu2');">
			<div class="div_menu"><p>MENU</p></div>
			<div class="div_imagem_menu"><img src="imagens/menu_celular.png" class="imagem_menu" alt="Menu" /></div>
		</div>

		<div id="Menu2">
		<div class="div_texto1"><p>COROAS DE FLORES</p></div>
		<div class="div_texto2"><p>ENTREGA</p></div>
		<div class="div_texto3"><p>FORMAS DE PAGAMENTO</p></div>
		<div class="div_texto4"><p>VEL�RIOS E CEMIT�RIOS</p></div>
		<div class="div_texto5"><p>FRASES HOMENAGEM</p></div>
		<div class="div_texto6" onmouseover="mostra('MaisVendidas');" onmouseout="esconde('MaisVendidas');"><p>MAIS VENDIDAS</p></div>
		</div>

		<div id="MaisVendidas" onmouseover="mostra('MaisVendidas');" onmouseout="esconde('MaisVendidas');">
		<div class="Posiciona">

			<div class="DivProduto">
				<div class="ProdutoDivImg"><img src="imagens/foto1.png" class="ProdutoImg" alt="Foto do Produto" /></div>
				<div class="Titulo">Coroa de Flores<br />Premium - 01</div>
				<div class="Desconto">15% de desconto</div>
				<div class="ValorDe">De: R$ 250,00</div>
				<div class="ValorPor">Por: R$ 220,00</div>
				<div class="Comprar"><img src="imagens/comprar2.png" class="ComprarImg" alt="Comprar" /></div>
			</div>

			<div class="DivProduto2">
				<div class="ProdutoDivImg"><img src="imagens/foto2.png" class="ProdutoImg" alt="Foto do Produto" /></div>
				<div class="Titulo">Coroa de Flores<br />Premium - 01</div>
				<div class="Desconto">15% de desconto</div>
				<div class="ValorDe">De: R$ 250,00</div>
				<div class="ValorPor">Por: R$ 220,00</div>
				<div class="Comprar"><img src="imagens/comprar2.png" class="ComprarImg" alt="Comprar" /></div>
			</div>

			<div class="DivProduto2">
				<div class="ProdutoDivImg"><img src="imagens/foto3.png" class="ProdutoImg" alt="Foto do produto" /></div>
				<div class="Titulo">Coroa de Flores<br />Premium - 01</div>
				<div class="Desconto">15% de desconto</div>
				<div class="ValorDe">De: R$ 250,00</div>
				<div class="ValorPor">Por: R$ 220,00</div>
				<div class="Comprar"><img src="imagens/comprar2.png" class="ComprarImg" alt="Comprar" /></div>
			</div>

		</div>
		</div>

	</div>
</div>

<div id="BarraCinzaM">
	<div class="div_centraliza">

		<div class="div1">
			<div class="div1_centro">
				<div class="imagem"><p><img src="imagens/m_frete_gratis.png" width="30" height="19" border="0" alt="Frete gr�tis para todo brasil" /></p></div>
				<div class="texto"><p>FRETE GR�TIS<br /><b>PARA TODO BRASIL</b></p></div>
			</div>
		</div>

		<div class="div2">
			<div class="div2_centro">
				<div class="imagem"><p><img src="imagens/m_faixa_homenagem.png" width="22" height="22" border="0" alt="Faixa para homenagem gr�tis" /></p></div>
				<div class="texto"><p>FAIXA PARA<br /><b>HOMENAGEM GR�TIS</b></p></div>
			</div>
		</div>

		<div class="div3">
			<div class="div3_centro">
				<div class="imagem"><p><img src="imagens/m_atenimento_24h.png" width="20" height="20" border="0" alt="Atendimento 24 horas" /></p></div>
				<div class="texto"><p>ATENDIMENTO<br /><b>24 HORAS</b></p></div>
			</div>
		</div>

	</div>
</div>


<div id="BarraCinza">
	<div class="div_centraliza">

		<div class="div1">
			<div class="div1_centro">
				<div class="imagem"><p><img src="imagens/m_faturamento.png" width="25" height="26" border="0" alt="Faturamento para empresas com NF-e" /></p></div>
				<div class="texto"><p>FATURAMENTO PARA<br /><b>EMPRESAS COM NF-e</b></p></div>
			</div>
		</div>

		<div class="div2">
			<div class="div2_centro">
				<div class="imagem"><p><img src="imagens/m_faixa_homenagem.png" width="27" height="27" border="0" alt="Faixa para homenagem gr�tis" /></p></div>
				<div class="texto"><p>FAIXA PARA<br /><b>HOMENAGEM GR�TIS</b></p></div>
			</div>
		</div>

		<div class="div3">
			<div class="div3_centro">
				<div class="imagem"><p><img src="imagens/m_entrega_brasil.png" width="26" height="26" border="0" alt="Entrega para todo Brasil" /></p></div>
				<div class="texto"><p>ENTREGA PARA<br /><b>TODO O BRASIL</b></p></div>
			</div>
		</div>

		<div class="div4">
			<div class="div4_centro">
				<div class="imagem"><p><img src="imagens/m_frete_gratis.png" width="41" height="26" border="0" alt="Frete gr�tis para todo Brasil" /></p></div>
				<div class="texto"><p>FRETE GR�TIS<br /><b>PARA TODO BRASIL</b></p></div>
			</div>
		</div>

		<div class="div5">
			<div class="div5_centro">
				<div class="imagem"><p><img src="imagens/m_boleto_cartao.png" width="25" height="19" border="0" alt="Pagamento em boleto ou cart�o de cr�dito" /></p></div>
				<div class="texto"><p>PAGAMENTO EM BOLETO<br /><b>OU CART�O DE CR�DITO</b></p></div>
			</div>
		</div>

		<div class="div6">
			<div class="div6_centro">
				<div class="imagem"><p><img src="imagens/m_atenimento_24h.png" width="23" height="23" border="0" alt="Atendimento 24 horas" /></p></div>
				<div class="texto"><p>ATENDIMENTO<br /><b>24 HORAS</b></p></div>
			</div>
		</div>

	</div>
</div>