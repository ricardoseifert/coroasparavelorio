<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Entre em contato</div>
</div>
</div>

<div id="EntreEmContato">
<div class="Div">

	<div class="TituloPagina">Entre em contato</div>

	<div class="Formulario2">
	<div class="Formulario">

		<div class="FormTexto1">Envie-nos um email!</div>

		<div class="FormDiv1">
			<div class="FormNomeCampo">Nome:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone:</div>
			<div class="FormCampo"><input type="text" id="telefone" name="telefone" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Mensagem:</div>
			<div class="FormCampo"><textarea id="mensagem" name="mensagem" class="FormCampoTextarea"></textarea></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput" /></div>
			</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" /></div>

	</div>
	</div>

	<div class="Div2">
	
		<div class="FormTexto1">Ou ligue:</div>

		<div class="Div2Sac">
			<div class="Div2Sac1"><img src="imagens/telefone2.png" class="Div2SacImg" alt="SAC" /></div>
			<div class="Div2Sac2">SAC:</div>
			<div class="Div2Sac3">0800-777-1986</div>
		</div>

		<div class="FormTexto2">S�o Paulo e regi�o:</div>
		<div class="Div2TelRegiao">(11) 3311-2255</div>

		<div class="FormTexto2">Ainda em d�vida?</div>
		<div class="Div2DuvidaTexto">Acesse nossa �rea de d�vidas frequentes clicando <a href="" class="Div2DuvidaUrl">aqui</a> ou fale conosco pelo <a href="" class="Div2DuvidaUrl">atendimento online</a>.</div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
