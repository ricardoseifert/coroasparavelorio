<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Seja uma floricultura parceira</div>
</div>
</div>

<div id="SejaParceira">
<div class="Div">

	<div class="TituloPagina">Seja uma floricultura parceira</div>

	<div class="Div1">
		<div class="Div1Texto1">A empresa Coroas Para Vel�rio busca sempre aumentar seu quadro de floriculturas parceiras para aumento de cobertura nacional.<br /><br />Caso a sua floricultura tenha interesse em fazer parte da nossa rede, preencha o cadastro inicial abaixo.<br /><br />A floricultura passar� por um processo de homologa��o e, caso atenda a todos os atributos requisitados, poder� utilizar o selo de qualidade Coroas Para Vel�rio</div>
	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Raz�o social:</div>
			<div class="FormCampo"><input type="text" id="razao_social" name="razao_social" class="FormCampoInput2" onfocus="esconde_validacao('razao_social','1');" /></div>
			<div id="aviso_razao_social" class="FormCampoObrigatorio" onclick="esconde_validacao('razao_social','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nome da Floricultura:</div>
			<div class="FormCampo"><input type="text" id="nome_da_floricultura" name="nome_da_floricultura" class="FormCampoInput2" onfocus="esconde_validacao('nome_da_floricultura','1');" /></div>
			<div id="aviso_nome_da_floricultura" class="FormCampoObrigatorio" onclick="esconde_validacao('nome_da_floricultura','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CNPJ:</div>
			<div class="FormCampo"><input type="text" id="cnpj" name="cnpj" class="FormCampoInput2" onfocus="esconde_validacao('cnpj','1');" /></div>
			<div id="aviso_cnpj" class="FormCampoObrigatorio" onclick="esconde_validacao('cnpj','1');">Campo obrigat�rio. Digite um CNPJ v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Pessoa de Contato:</div>
			<div class="FormCampo"><input type="text" id="pessoa_de_contato" name="pessoa_de_contato" class="FormCampoInput2" onfocus="esconde_validacao('pessoa_de_contato','1');" /></div>
			<div id="aviso_pessoa_de_contato" class="FormCampoObrigatorio" onclick="esconde_validacao('pessoa_de_contato','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone 1:</div>
			<div class="FormCampo"><input type="text" id="telefone1" name="telefone1" class="FormCampoInput2" onfocus="esconde_validacao('telefone1','1');" /></div>
			<div id="aviso_telefone1" class="FormCampoObrigatorio" onclick="esconde_validacao('telefone1','1');">Campo obrigat�rio. Digite em telefone v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone 2:</div>
			<div class="FormCampo"><input type="text" id="telefone2" name="telefone2" class="FormCampoInput2" onfocus="esconde_validacao('telefone2','1');" /></div>
			<div id="aviso_telefone2" class="FormCampoObrigatorio" onclick="esconde_validacao('telefone2','1');">Campo obrigat�rio. Digite em telefone v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput2" onfocus="esconde_validacao('email','1');" /></div>
			<div id="aviso_email" class="FormCampoObrigatorio" onclick="esconde_validacao('email','1');">Campo obrigat�rio. Digite um email v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">CEP:</div>
			<div class="FormCampo"><input type="text" id="cep" name="cep" class="FormCampoInput2" onfocus="esconde_validacao('cep','1');" /></div>
			<div id="aviso_cep" class="FormCampoObrigatorio" onclick="esconde_validacao('cep','1');">Campo obrigat�rio. Digite um cep v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Endere�o:</div>
			<div class="FormCampo"><input type="text" id="endereco" name="endereco" class="FormCampoInput2" onfocus="esconde_validacao('endereco','1');" /></div>
			<div id="aviso_endereco" class="FormCampoObrigatorio" onclick="esconde_validacao('endereco','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Bairro:</div>
			<div class="FormCampo"><input type="text" id="bairro" name="bairro" class="FormCampoInput2" onfocus="esconde_validacao('bairro','1');" /></div>
			<div id="aviso_bairro" class="FormCampoObrigatorio" onclick="esconde_validacao('bairro','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput2" onfocus="esconde_validacao('cidade','1');" /></div>
			<div id="aviso_cidade" class="FormCampoObrigatorio" onclick="esconde_validacao('cidade','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Estado:</div>
			<div class="FormCampo"><select id="estado" name="estado" class="FormCampoSelect2" onfocus="esconde_validacao('estado','4');"><option value=""></option></select></div>
			<div id="aviso_estado" class="FormCampoObrigatorio" onclick="esconde_validacao('estado','4');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Celular de Contato:</div>
			<div class="FormCampo"><input type="text" id="celular_contato" name="celular_contato" class="FormCampoInput2" onfocus="esconde_validacao('celular_contato','1');" /></div>
			<div id="aviso_celular_contato" class="FormCampoObrigatorio" onclick="esconde_validacao('celular_contato','1');">Campo obrigat�rio. Digite um n�mero v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nextel:</div>
			<div class="FormCampo"><input type="text" id="nextel" name="nextel" class="FormCampoInput2" onfocus="esconde_validacao('nextel','1');" /></div>
			<div id="aviso_nextel" class="FormCampoObrigatorio" onclick="esconde_validacao('nextel','1');">Campo obrigat�rio. Digite um n�mero v�lido.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Site:</div>
			<div class="FormCampo"><input type="text" id="site" name="site" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Hor�rio de funcionamento:</div>
			<div class="FormCampo"><input type="text" id="horario_funcionamento" name="horario_funcionamento" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Como nos conheceu?</div>
			<div class="FormCampo"><select id="como_nos_conheceu" name="como_nos_conheceu" class="FormCampoSelect"><option value="">Selecione</option></select></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput2" onfocus="esconde_validacao('validacao','1');" /></div>
				<div class="FormValidacaoTexto4" id="li_aceito_termos" onclick="esconde_validacao('li_aceito_termos','5');"><input type="radio" id="termos" name="termos" /> Li e estou de acordo com os <a href="" class="FormValidacaoTexto3">Termos e Condi��es Comerciais.</a></div>
			</div>
			<div id="aviso_validacao" class="FormCampoObrigatorio2" onclick="esconde_validacao('validacao','1');">Campo obrigat�rio. Soma n�o confere.</div>
			<div id="aviso_li_aceito_termos" class="FormCampoObrigatorio3">Campo obrigat�rio.</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" /></div>

	</div>

	<div class="Div1">
		<div class="Div1Texto1">Agradecemos pela prefer�ncia!<br />Equipe Coroas Para Vel�rio.</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
