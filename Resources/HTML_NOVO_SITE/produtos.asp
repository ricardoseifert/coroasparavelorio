<%
Response.expires = 0
response.charset = "ISO-8859-1"

pag = request("pag")

If pag = 1 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" /></p></div>

	<div class="produto">
		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" /></div>

					<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" /></div>

					<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" /></div>

					<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" /></div>

				</div>
				</center>

			</div>

			<div id="pequeno1_m" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="medio1_m">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="grande1_m" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" /></div>
		</div>
	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=2','ListaProdutos');" /></p></div>

<% ElseIf pag = 2 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=1','ListaProdutos');" /></p></div>

	<div class="produto">
		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" /></div>
			<div class="titulo">Coroa de Flores Premium - 02</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" /></div>

					<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" /></div>

					<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" /></div>

					<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" /></div>

				</div>
				</center>

			</div>

			<div id="pequeno1_m" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="medio1_m">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="grande1_m" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" /></div>
		</div>
	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=3','ListaProdutos');" /></p></div>

<% ElseIf pag = 3 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=2','ListaProdutos');" /></p></div>

	<div class="produto">
		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" /></div>
			<div class="titulo">Coroa de Flores Premium - 03</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" /></div>

					<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" /></div>

					<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" /></div>

					<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" /></div>

					<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" /></div>

				</div>
				</center>

			</div>

			<div id="pequeno1_m" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="medio1_m">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="grande1_m" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" /></div>
		</div>
	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_d.png" width="25" height="64" border="0" /></p></div>

<% End If %>
