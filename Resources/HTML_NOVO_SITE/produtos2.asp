<%
Response.expires = 0
response.charset = "ISO-8859-1"

pag = request("pag")

If pag = 1 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta" /></p></div>

	<div class="produto">

		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do Produto" /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">

				<center><div class="escolha_tamanho_div">

					<div id="sig_pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho" /></div>

					<div id="sig_pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_pequeno1_m');esconde('sig_medio1_m');esconde('sig_grande1_m');mostra('sig_pequeno1_m_s');esconde('sig_pequeno1_m_ns');mostra('sig_medio1_m_ns');esconde('sig_medio1_m_s');mostra('sig_grande1_m_ns');esconde('sig_grande1_m_s');" alt="Tamanho"  /></div>

					<div id="sig_medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_medio1_m');esconde('sig_pequeno1_m');esconde('sig_grande1_m');" alt="Tamanho"  /></div>

					<div id="sig_medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_medio1_m');esconde('sig_pequeno1_m');esconde('sig_grande1_m');esconde('sig_pequeno1_m_s');mostra('sig_pequeno1_m_ns');esconde('sig_medio1_m_ns');mostra('sig_medio1_m_s');mostra('sig_grande1_m_ns');esconde('sig_grande1_m_s');" alt="Tamanho"  /></div>

					<div id="sig_grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_grande1_m');esconde('sig_pequeno1_m');esconde('sig_medio1_m');" alt="Tamanho"  /></div>

					<div id="sig_grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('sig_grande1_m');esconde('sig_pequeno1_m');esconde('sig_medio1_m');esconde('sig_pequeno1_m_s');mostra('sig_pequeno1_m_ns');mostra('sig_medio1_m_ns');esconde('sig_medio1_m_s');esconde('sig_grande1_m_ns');mostra('sig_grande1_m_s');" alt="Tamanho"  /></div>

				</div>
				</center>

			</div>

			<div id="sig_pequeno1_m" class="escondido">
			<div class="tamanho_texto">1,0m x 1,0m</div>
			<div class="valor">150,00</div>
			</div>

			<div id="sig_medio1_m">
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			</div>

			<div id="sig_grande1_m" class="escondido">
			<div class="tamanho_texto">1,5m x 1,0m</div>
			<div class="valor">350,00</div>
			</div>

			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos2.asp?pag=2','SigListaProdutos');" alt="Seta"  /></p></div>

<% ElseIf pag = 2 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_h.png" width="25" height="64" border="0" alt="Seta" style="cursor:pointer" onclick="abre('produtos2.asp?pag=1','SigListaProdutos');" /></p></div>

	<div class="produto">

		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos2.asp?pag=3','SigListaProdutos');" alt="Seta"  /></p></div>

<% ElseIf pag = 3 Then %>

	<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_h.png" width="25" height="64" border="0" alt="Seta" style="cursor:pointer" onclick="abre('produtos2.asp?pag=2','SigListaProdutos');" /></p></div>

	<div class="produto">

		<div class="quadrado1">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

		<div class="quadrado2">
			<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
			<div class="titulo">Coroa de Flores Premium - 01</div>
			<div class="tamanho_imagens">
				<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
			</div>
			<div class="tamanho_texto">1,2m x 1,0m</div>
			<div class="valor">250,00</div>
			<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
			<div class="comprar"><img src="imagens/comprar2.png" class="comprar_tamanho" alt="Comprar"  /></div>
		</div>

	</div>

	<div class="seta_direita"><p><img src="imagens/dep_seta_direita_d.png" width="25" height="64" border="0" alt="Seta"  /></p></div>

<% End If %>
