<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Pesquisa de satisfa��o</div>
</div>
</div>

<div id="PesquisaSatisfacao">
<div class="Div">

	<div class="TituloPagina">Pesquisa de satisfa��o</div>

	<div class="Div1">
		<div class="Div1Texto1">Gostar�amos de convid�-lo a responder esta breve pesquisa de satisfa��o sobre nossos servi�os prestados. Agradecemos antecipadamente pela sua participa��o. <br /><br />Avalie utilizando uma das op��es abaixo:</div>
	</div>

	<div class="QuadroNotas2">

		<div class="Linha1_2">
			<div class="Coluna1">&nbsp;</div>
			<div class="Coluna2">Muito Ruim</div>
			<div class="Coluna2">Ruim</div>
			<div class="Coluna2">Bom</div>
			<div class="Coluna2">Muito Bom</div>
			<div class="Coluna2">Sem Avalia��o</div>
		</div>
		<div class="Linha2">
			<div class="Coluna1">Prazo de Entrega:</div>
			<div class="Coluna2"><input type="radio" name="prazo_de_entrega" value="Muito Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="prazo_de_entrega" value="Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="prazo_de_entrega" value="Bom" /></div>
			<div class="Coluna2"><input type="radio" name="prazo_de_entrega" value="Muito Bom" /></div>
			<div class="Coluna2"><input type="radio" name="prazo_de_entrega" value="Sem Avalia��o" /></div>
		</div>
		<div class="Linha3">
			<div class="Coluna1">Qualidade do Produto:</div>
			<div class="Coluna2"><input type="radio" name="qualidade_do_produto" value="Muito Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="qualidade_do_produto" value="Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="qualidade_do_produto" value="Bom" /></div>
			<div class="Coluna2"><input type="radio" name="qualidade_do_produto" value="Muito Bom" /></div>
			<div class="Coluna2"><input type="radio" name="qualidade_do_produto" value="Sem Avalia��o" /></div>
		</div>
		<div class="Linha2">
			<div class="Coluna1">Nosso Atendimento:</div>
			<div class="Coluna2"><input type="radio" name="nosso_atendimento" value="Muito Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="nosso_atendimento" value="Ruim" /></div>
			<div class="Coluna2"><input type="radio" name="nosso_atendimento" value="Bom" /></div>
			<div class="Coluna2"><input type="radio" name="nosso_atendimento" value="Muito Bom" /></div>
			<div class="Coluna2"><input type="radio" name="nosso_atendimento" value="Sem Avalia��o" /></div>
		</div>

	</div>

	<div class="Formulario">

		<div class="FormDiv1">
			<div class="FormNomeCampo">Voc� recomendaria nossos servi�os a amigos ou conhecidos:</div>
			<div class="FormCampo2e3_2" id="recomenda_servicos">
			<div class="FormCampo2"><input type="radio" name="recomenda" value="Sim" onfocus="esconde_validacao('recomenda_servicos','6');" /> Sim</div>
			<div class="FormCampo3"><input type="radio" name="recomenda" value="N�o" onfocus="esconde_validacao('recomenda_servicos','6');" /> N�o</div>
			<div id="aviso_recomenda_servicos" class="FormCampoObrigatorio5" onclick="esconde_validacao('recomenda_servicos','6');">Campo obrigat�rio.</div>
			</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Como nos conheceu?</div>
			<div class="FormCampo"><select id="como_nos_conheceu" name="como_nos_conheceu" class="FormCampoSelect2" onfocus="esconde_validacao('como_nos_conheceu','4');"><option value=""></option><option value=""></option></select></div>
			<div id="aviso_como_nos_conheceu" class="FormCampoObrigatorio" onclick="esconde_validacao('como_nos_conheceu','4');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Sugest�es, Reclama��es ou Elogios<br />Por favor adicione qualquer coment�rio a respeito de sua compra ou de nossos servi�os:</div>
			<div class="FormCampo"><textarea id="texto" name="texto" class="FormCampoTextarea2" onfocus="esconde_validacao('texto','2');"></textarea></div>
			<div id="aviso_texto" class="FormCampoObrigatorio3" onclick="esconde_validacao('texto','2');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Estado:</div>
			<div class="FormCampo"><select id="estado" name="estado" class="FormCampoSelect2" onfocus="esconde_validacao('estado','4');"><option value=""></option><option value=""></option></select></div>
			<div id="aviso_estado" class="FormCampoObrigatorio" onclick="esconde_validacao('estado','4');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput2" onfocus="esconde_validacao('cidade','1');" /></div>
			<div id="aviso_cidade" class="FormCampoObrigatorio" onclick="esconde_validacao('cidade','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">N�mero do pedido:</div>
			<div class="FormCampo"><input type="text" id="numero_pedido" name="numero_pedido" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nome:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput2" onfocus="esconde_validacao('nome','1');" /></div>
			<div id="aviso_nome" class="FormCampoObrigatorio" onclick="esconde_validacao('nome','1');">Campo obrigat�rio.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Telefone de contato:</div>
			<div class="FormCampo"><input type="text" id="telefone_contato" name="telefone_contato" class="FormCampoInput2" onfocus="esconde_validacao('telefone_contato','1');" /></div>
			<div id="aviso_telefone_contato" class="FormCampoObrigatorio" onclick="esconde_validacao('telefone_contato','1');">Campo obrigat�rio. Dados inv�lidos.</div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">E-mail:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput2" onfocus="esconde_validacao('email','1');" /></div>
			<div id="aviso_email" class="FormCampoObrigatorio" onclick="esconde_validacao('email','1');">Campo obrigat�rio. Digite um email.</div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Valida��o:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem"><img src="imagens/captcha.png" width="200" height="70" border="0" alt="Valida��o" /></div>
				<div class="FormValidacaoTexto">Preencha com a soma</div>
				<div class="FormValidacaoCampo2"><input type="text" id="validacao" name="validacao" class="FormCampoInput2" onfocus="esconde_validacao('validacao','1');" /></div>
				<div class="FormValidacaoTexto3" id="autorizo_publicacao"><input type="checkbox" id="autorizacao" name="autorizacao" onclick="esconde_validacao('autorizo_publicacao','5');" /> Autorizo a publica��o do meu depoimento.</div>
			</div>
			<div id="aviso_validacao" class="FormCampoObrigatorio2" onclick="esconde_validacao('validacao','1');">Campo obrigat�rio. Soma n�o confere.</div>
			<div id="aviso_autorizo_publicacao" class="FormCampoObrigatorio4">Campo obrigat�rio.</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" /></div>

	</div>

	<div class="Div1">
		<div class="Div1Texto1">Agradecemos pela prefer�ncia!<br />Equipe Coroas Para Vel�rio.</div>
	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
