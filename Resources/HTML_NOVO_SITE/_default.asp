<!--#include file="topo.asp" -->

<div id="PossoAjudar">
	<div class="div_centraliza">
		<div class="fechar" onclick="esconde('PossoAjudar');">X</div>
		<div class="texto1">Precisa de ajuda para escolher a Coroa?</div>
		<div class="texto2">Te ajudamos a encontrar a coroa ideal para sua homenagem.</div>
		<div class="cliqueAqui"><input type="button" value="Clique aqui" class="cliqueAquiBotao" alt="Precisa de ajuda para escolher a Coroa? Clique aqui" /></div>
	</div>
</div>

<div id="HomeTitulo">
	<div class="div_centraliza">

		<div class="titulo"><p>Coroas de Flores</p></div>

		<div class="ordenar">
			<div class="select_texto"><p>ordenar por</p></div>
			<div class="select"><p>
				<select name="ordenar" id="ordenar" class="select_input">
				<option value="">Mais baratas</option>
				</select>
			</p></div>
		</div>

		<div class="divBusca">
			<div class="divBusca2"><input type="text" id="busca2" name="busca2" class="divBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="divBusca2"><img src="imagens/busca.png" width="36" height="30" border="0" alt="Buscar" /></div>
		</div>

	</div>
</div>

<div id="ListagemProdutos2">
	<div class="div_centraliza" id="ListaProdutos">

		<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta" /></p></div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do Produto" /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">

					<center><div class="escolha_tamanho_div">

						<div id="pequeno1_m_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho" /></div>

						<div id="pequeno1_m_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1_m');esconde('medio1_m');esconde('grande1_m');mostra('pequeno1_m_s');esconde('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

						<div id="medio1_m_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');" alt="Tamanho"  /></div>

						<div id="medio1_m_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1_m');esconde('pequeno1_m');esconde('grande1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');esconde('medio1_m_ns');mostra('medio1_m_s');mostra('grande1_m_ns');esconde('grande1_m_s');" alt="Tamanho"  /></div>

						<div id="grande1_m_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');" alt="Tamanho"  /></div>

						<div id="grande1_m_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1_m');esconde('pequeno1_m');esconde('medio1_m');esconde('pequeno1_m_s');mostra('pequeno1_m_ns');mostra('medio1_m_ns');esconde('medio1_m_s');esconde('grande1_m_ns');mostra('grande1_m_s');" alt="Tamanho"  /></div>

					</div>
					</center>

				</div>

				<div id="pequeno1_m" class="escondido">
				<div class="tamanho_texto">1,0m x 1,0m</div>
				<div class="valor">150,00</div>
				</div>

				<div id="medio1_m">
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				</div>

				<div id="grande1_m" class="escondido">
				<div class="tamanho_texto">1,5m x 1,0m</div>
				<div class="valor">350,00</div>
				</div>

				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('produtos.asp?pag=2','ListaProdutos');" alt="Seta"  /></p></div>

	</div>
</div>

<div id="ListagemProdutos">
	<div class="div_centraliza">

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto1.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">

					<center><div class="escolha_tamanho_div">

						<div id="pequeno1_s" class="escondido_left"><img src="imagens/tamanho_p_selecionado.png" class="tamanho_imagens2" alt="Tamanho"  /></div>

						<div id="pequeno1_ns" class="esquerda"><img src="imagens/tamanho_p_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('pequeno1');esconde('medio1');esconde('grande1');mostra('pequeno1_s');esconde('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="medio1_s" class="esquerda"><img src="imagens/tamanho_m_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');" alt="Tamanho"  /></div>

						<div id="medio1_ns" class="escondido_left"><img src="imagens/tamanho_m_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('medio1');esconde('pequeno1');esconde('grande1');esconde('pequeno1_s');mostra('pequeno1_ns');esconde('medio1_ns');mostra('medio1_s');mostra('grande1_ns');esconde('grande1_s');" alt="Tamanho"  /></div>

						<div id="grande1_s" class="escondido_left"><img src="imagens/tamanho_g_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');" alt="Tamanho"  /></div>

						<div id="grande1_ns" class="esquerda"><img src="imagens/tamanho_g_nao_selecionado.png" class="tamanho_imagens2" onmouseover="mostra('grande1');esconde('pequeno1');esconde('medio1');esconde('pequeno1_s');mostra('pequeno1_ns');mostra('medio1_ns');esconde('medio1_s');esconde('grande1_ns');mostra('grande1_s');" alt="Tamanho"  /></div>

					</div>
					</center>

				</div>

				<div id="pequeno1" class="escondido">
				<div class="tamanho_texto">1,0m x 1,0m</div>
				<div class="valor">150,00</div>
				</div>

				<div id="medio1">
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				</div>

				<div id="grande1" class="escondido">
				<div class="tamanho_texto">1,5m x 1,0m</div>
				<div class="valor">350,00</div>
				</div>

				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto2.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho" />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado1">
				<div class="foto"><img src="imagens/foto3.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

		<div class="produto">
			<div class="quadrado2">
				<div class="foto"><img src="imagens/foto4.png" class="foto_tamanho" alt="Foto do produto"  /></div>
				<div class="titulo">Coroa de Flores Premium - 01</div>
				<div class="tamanho_imagens">
					<img src="imagens/tamanho_p_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_m_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
					<img src="imagens/tamanho_g_nao_selecionado.png" width="42" height="37" border="0" alt="Tamanho"  />
				</div>
				<div class="tamanho_texto">1,2m x 1,0m</div>
				<div class="valor">250,00</div>
				<div class="mais_detalhes"><a href="" class="mais_detalhes2">+ detalhes da coroa</a></div>
				<div class="comprar"><img src="imagens/comprar.png" class="comprar_tamanho" alt="Comprar"  /></div>
			</div>
		</div>

	</div>
</div>

<div id="VejaMais">
	<div class="div_centraliza">
		<div class="texto"><p>VEJA MAIS COROAS DE FLORES</p></div>
	</div>
</div>

<!-- <div id="OpcoesM">
	<div class="div_centraliza">
		<div class="div_imagem"><img src="imagens/site_blindado3.png" width="140" height="85" border="0" alt="Site blindado"  /></div>
		<div class="div_imagem"><img src="imagens/frete_gratis3.png" width="140" height="85" border="0" alt="Frete gr�tis"  /></div>
		<div class="div_imagem"><img src="imagens/entrega24h3.png" width="140" height="85" border="0" alt="Entrega 24 horas"  /></div>
	</div>
</div> -->

<div id="HomeDepoimentosSobre">
	<div class="div_centraliza">

		<div class="depoimentos">
			<div class="titulo">Depoimento de Clientes</div>
			<div class="quadrado" id="ListaDepoimentos">

				<div class="seta_esquerda"><p><img src="imagens/dep_seta_esquerda_d.png" width="25" height="64" border="0" alt="Seta"  /></p></div>

				<div class="quadrado_cor">
					<div class="texto">�Muito obrigada e quero dizer que me surpreendi com o atendimento. R�pido e muito eficiente... Num momento de dor, foi tudo que precisava. A fam�lia adorou a homenagem. Obrigada! Lucineide�</div>
					<div class="iten_avaliado"><b>Item avaliado:</b> Site Coroas para Vel�rio</div>
					<div class="nome_cidade"><b>Ant�nio - ITA�</b><br />S�O PAULO - 12/12/12</div>
				</div>

				<div class="seta_direita"><p><img src="imagens/dep_seta_direita_h.png" width="25" height="64" border="0" style="cursor:pointer" onclick="abre('depoimentos.asp?pag=2','ListaDepoimentos');" alt="Seta"  /></p></div>

			</div>
		</div>

		<div class="sobre">
			<div class="titulo">Sobre a Coroas para Vel�rio</div>
			<div class="video"><iframe src="//www.youtube.com/embed/q-fDRJ8ObzA" frameborder="0" allowfullscreen class="iframe_video_sobre" scrolling="no" name="video_sobre"></iframe></div>
		</div>

	</div>
</div>

<!--#include file="rodape.asp" -->
