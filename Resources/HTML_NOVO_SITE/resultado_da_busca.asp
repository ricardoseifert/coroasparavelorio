<!--#include file="topo.asp" -->

<div id="CaminhoPagina">
<div class="Div">
	<div class="Desabilitado">Home</div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado">Resultados da busca</div>
</div>
</div>

<div id="ResultadoBusca">
<div class="Div">

	<div class="TituloPagina">Resultado de busca para 'Curitiba'</div>

	<div class="TotalResultados">103 resultados encontrados</div>

	<div class="Filtro">
		<div class="FiltroVer">Ver:</div>
		<div class="FiltroOpcoes"><a href="" class="FiltroSelecionado">Todos</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Cemit�rios</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Capelas</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Hospitais</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Funer�rias</a></div>
		<div class="FiltroOpcoes"><a href="" class="FiltroNaoSelecionado">Igrejas</a></div>
	</div>

	<div class="ResulBusca">
		<div class="ResulBusca2">buscar nos resultados:</div>
		<div class="ResulBusca3">
			<div class="DivBuscaResul2"><input type="text" name="busca_resultados" class="DivBuscaResulInput" value="" /></div>
			<div class="DivBuscaResul2"><input type="button" value="OK" class="DivBuscarResul" /></div>
		</div>
	</div>

	<div class="ListaLocaisResul">

		<div class="ListaLocaisRTitulo">Cemit�rios</div>

		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Saudade / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Parque Senhor do Bonfim / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Universal Necr�pole Ecum�nica Vertical / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Luterano / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Paz / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Saudade / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Parque Senhor do Bonfim / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Universal Necr�pole Ecum�nica Vertical / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Luterano / PR</div>
		<div class="ListaLocaisRTexto">Cemit�rio Jardim da Paz / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

		<div class="ListaLocaisRTitulo2">Capetas</div>

		<div class="ListaLocaisRTexto">Capela Divina Miseric�rdia / PR</div>
		<div class="ListaLocaisRTexto">Capela Mission�ria / PR</div>
		<div class="ListaLocaisRTexto">Capela Nossa Senhora Aparecida / PR</div>
		<div class="ListaLocaisRTexto">Capela da Luz / PR</div>
		<div class="ListaLocaisRTexto">Capela Imaculada da Concei��o / PR</div>
		<div class="ListaLocaisRTexto">Capela Divina Miseric�rdia / PR</div>
		<div class="ListaLocaisRTexto">Capela Mission�ria / PR</div>
		<div class="ListaLocaisRTexto">Capela Nossa Senhora Aparecida / PR</div>
		<div class="ListaLocaisRTexto">Capela da Luz / PR</div>
		<div class="ListaLocaisRTexto">Capela Imaculada da Concei��o / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

		<div class="ListaLocaisRTitulo2">Hospitais</div>

		<div class="ListaLocaisRTexto">Hospital Erasto Gaertner / PR</div>
		<div class="ListaLocaisRTexto">Hospital Santa Cruz / PR</div>
		<div class="ListaLocaisRTexto">Hospital Nossa Senhora das Gra�as / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Evang�lico de Curitiba / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Cajuru / PR</div>
		<div class="ListaLocaisRTexto">Hospital Erasto Gaertner / PR</div>
		<div class="ListaLocaisRTexto">Hospital Santa Cruz / PR</div>
		<div class="ListaLocaisRTexto">Hospital Nossa Senhora das Gra�as / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Evang�lico de Curitiba / PR</div>
		<div class="ListaLocaisRTexto">Hospital Universit�rio Cajuru / PR</div>

		<div class="Paginacao">
			<div class="PaginacaoPag">P�ginas</div>
			<div class="PaginacaoSelecionado">1</div>
			<div class="PaginacaoNaoSelecionado">2</div>
			<div class="PaginacaoNaoSelecionado">3</div>
			<div class="PaginacaoNaoSelecionado">4</div>
		</div>

	</div>

	<div class="NaoEncontrou">

		<div class="Texto1">N�o encontrou o seu local?</div>

		<div class="Div2">
		<div class="Texto2">Busque novamente</div>
		<div class="DivBusca">
			<div class="DivBusca2"><input type="text" id="busca2" name="busca2" class="DivBuscaInput" value="Encontre vel�rios ou cemit�rios" onfocus="if(this.value=='Encontre vel�rios ou cemit�rios') { this.value = ''; }" onblur="if(this.value == '' || this.value == ' ') { this.value = 'Encontre vel�rios ou cemit�rios'; }" /></div>
			<div class="DivBusca2"><img src="imagens/busca.png" class="DivBuscar" alt="Buscar" /></div>
		</div>
		</div>

		<div class="Texto3">Ou fale conosco pelo nosso <a href="" class="Url">Atendimento Online</a> ou ligue para nossa Central de Atendimento 24h: 0800-777-1986</div>

	</div>

	<div class="DivBanner">
		<div class="Div6">
		<div class="Div6Banner"><img src="imagens/banner_vejamais.jpg" class="Div6BannerImg" alt="Banner" /></div>
		</div>
	</div>

</div>
</div>

<!--#include file="rodape.asp" -->
