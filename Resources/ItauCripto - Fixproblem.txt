﻿Caso ocorra problema com a ItauCripto.dll
siga os procedimentos abaixo.


1 - Primeiro de tudo, se você já tiver registrado a DLL no sistema operacional x64 deve deregistrar da DLL. Para fazer isso basta digitar o seguinte no comando executar "regsvr32 / u" algo como "regsvr32 / u C: \ MyDLL \ ASXUpload.DLL". Se você já não registados, o DLL a partir do OS x64 então não há necessidade de executar esta etapa.

2 - Também certifique-se que você não tem mantido o seu DLL dentro da pasta Windows, que é normalmente C: \ Windows. Para este exemplo, guardei a DLL na seguinte pasta C: \ MyDLL.

3 - Agora precisamos adicionar o COM + Componentes usando Component Services da Microsoft. Para iniciar os Serviços componentes, vá para Painel de controle Ferramentas / Administrativas / Serviços de componentes.
Uma vez dentro dos Serviços de Componentes, expandir Computadores, em Meu computador e, em seguida Aplicativos COM +.
Em seguida, clique com o botão direito em Aplicativos COM + e escolha "Novo" -> "Application".

4 - tela em "Assistente de boas-vindas à instalação de aplicativos COM", clique em "Next>".

5 - Clique no botão "Criar um aplicativo vazio".

6 - Insira o nome. Desde o meu nome DLL é ASXUpload.dll então eu ter digitado o nome como "ASXUpload". Quando perguntado "Biblioteca ou servidor", selecione "Servidor".

7 - Clique em "Next>" botão e, em seguida, escolha "Este usuário".

8 - Digite o usuário ou clique em Procurar para selecionar o usuário. Clicando Navegar é mais seguro, para garantir que o domínio e ortografia correta são usados. Digite a senha e confirme a senha. Atenção, não se esqueça de incluir o domínio / nome de usuário, se necessário. Clique em "Finish". (Nota: Recomendamos "Este usuário", caso contrário, alguém deve estar conectado ao servidor para que DLL para ser executado.). No meu caso eu escolhi a conta de administrador de domínio. Você também pode adicionar uma conta de serviço. Se você não tem certeza consulte o seu administrador do sistema.

9 - Agora "Adicionar funções do aplicativo" tela aparecerá. Não acrescentam nada basta clicar sobre o "Next>" botão.

10 - Agora "Adicionar usuários a Função" tela aparecer. Não acrescentam nada basta clicar sobre o "Next>" botão.

11 - Agora você vai ver que, sob Component Services -> Computadores -> Meu computador -> COM + aplicativo -> você vai ver a aplicação recém-adicionado. Neste exemplo, o nome da aplicação seria "ASXUpload". Agora detalhar a aplicação recém-adicionado "ASXUpload", clicando no ícone "+" e você vai ver "Componentes".

12 - Agora, clique com o botão direito em "Componentes" e, em seguida, escolha "New Component". Em "Welcome to the aplicativo COM Assistente de instalação", clique em "Next>".

13 - Clique em "Instalar novo componente (s)" e agora selecione o DLL que você deseja registrar. No caso seria "C: \ MyDLL \ ASXUpload.DLL".

14 - Uma vez que você selecione o DLL que você vai ver que ele vai mostrar os componentes encontrados. Clique no botão "Next>" botão para continuar e, finalmente, acertar o botão "Finish" para concluir.

15 - Agora é a parte complicada. Botão direito do mouse sobre a aplicação de ter acrescentado que você vai encontrar em Serviços de Componentes -> Computadores -> Meu computador -> COM + aplicativo. No meu caso, o nome do aplicativo é "ASXUpload". Depois que você clique direito sobre o aplicativo, selecione "Propriedades". A janela de propriedades do aplicativo abrirá. Clique na aba "Segurança". Na guia Segurança se certificar de que sob a seção "Autorização" a opção "Aplicar verificações de acesso para esta aplicação" está desmarcada.

***************
1 - First of all if you have already registered the DLL in x64 Operating System the unregister the DLL. To do that just type the following in the run command "regsvr32 /u " something like "regsvr32 /u C:\MyDLL\ASXUpload.DLL". If you have already unregistered the DLL from the x64 OS then no need to run this step.

2 - Also make sure that you have not kept your DLL inside the Windows folder which is normally C:\Windows. For this example I have kept the DLL in the following folder C:\MyDLL.

3 - Now we need to add the COM+ Components using Component Services of Microsoft. To start Component Services, go to Control Panel / Administrative Tools/ Component Services. Once inside component Services, drill down into Computers, then My Computer, then COM+ Applications. Then Right-click on COM+ Applications and choose "New" -> "Application".

4 - At "Welcome to the COM Application Install Wizard" screen, click “Next >”.

5 - Click on “Create an Empty Application” button.

6 - Enter the name. Since my DLL name is ASXUpload.dll so I have typed in the name as “ASXUpload”. When asked "Library or Server", select “Server”.

7 - Click “Next >” button and then choose “This User”.

8 - Enter the User or click Browse to select the user. Clicking Browse is safer, to ensure that the correct domain and spelling are used. Enter the password and confirm the password. Warning, be sure to include the domain/username if required. Click on “Finish”. (Note: We recommend "This User", otherwise, someone must be logged onto the server in order for DLL to run.). In my case I have chosen the domain administrator account. You can also add a Service Account. If you are not sure please consult with your system administrator.

9 - Now “Add Application Roles” screen will appear. Do not add anything just click on the “Next >” button.

10 - Now “Add Users to Role” screen appear. Do not add anything just click on the “Next >” button.

11 - Now you will see that under Component Services -> Computers -> My Computer -> COM+ Application -> you will see the newly added application. In this example the application name would be "ASXUpload". Now drill down the newly added application “ASXUpload” by clicking the “+” icon and you will see “Components”.

12 - Now right-click on “Components” and then choose “New Component”. At "Welcome to the COM Application Install Wizard" screen, click “Next >”.

13 - Click on “Install new component(s)” and now select the DLL which you want to register. In the case it would be “C:\MyDLL\ASXUpload.DLL”.

14 - Once you select the DLL you will see that it will show you the components found. Click on the “Next >” button to proceed and finally hit the “Finish” button to complete.

15 - Now is the tricky part. Right click on the application you have added which you will find under Component Services -> Computers -> My Computer -> COM+ Application. In my case the application name is “ASXUpload”. After you right click on the Application select “Properties”. The application properties window will open up. Click on the “Security” tab. In the Security tab make sure that under “Authorization” section the checkbox “Enforce access checks for this application” is unchecked.