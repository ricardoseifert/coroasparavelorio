﻿@using CaptchaMvc.HtmlHelpers
@using CaptchaMvc.Interface
@model IEnumerable<Domain.Entities.Depoimento>
@{
    Layout = "~/Views/Shared/_Layout.cshtml";
    ViewBag.Title = "Depoimentos de Clientes";
    var depoimentosPF = (List<Domain.Entities.Depoimento>)ViewBag.DepoimentosPF;
    var depoimentosPJ = (List<Domain.Entities.Depoimento>)ViewBag.DepoimentosPJ;
    ICaptcha captcha = Html.MathCaptcha("Atualizar", "Preencha com a soma");
}
 
@section Script{    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#form_depoimento").validate({
                meta: "validate",
                invalidHandler: function (n, t) { var i = t.numberOfInvalids(), r; if (i) { if (r = i == 1 ? "Por favor, corrija o seguinte erro:\n" : "Por favor, corrija os " + i + " erros abaixo:\n", i = "", t.errorList.length > 0) for (x = 0; x < t.errorList.length; x++) i += "\n● " + t.errorList[x].message; alert(r + i) } t.focusInvalid() },
                submitHandler: function (form) {
                    $(".botao").hide();
                    $("#processando").show();
                    var formdata = $(form).serialize();
                    $.post("/depoimentos-de-clientes/Enviar", formdata, function (data) {
                        if (data.status == "OK") {
                            window.location = "?msg=sucesso#envie";
                          } else {      
                            alert(data.mensagem);
                            $(".botao").show();
                            $("#processando").hide();
                            $('#@captcha.BuildInfo.ImageElementId').attr('src', data.Captcha.@(captcha.BuildInfo.ImageElementId));
                            $('#@captcha.BuildInfo.TokenElementId').attr('value', data.Captcha.@(captcha.BuildInfo.TokenElementId));
                            $('#@captcha.BuildInfo.InputElementId').attr('value', '');
                            }
                    });
                },
                rules: {
                    nome: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    cidade: "required",
                    mensagem: "required",
                    nota: "required",
                    CaptchaInputText: "required"
                },
                messages: {
                    nome: "Digite o seu nome",
                    email: {
                        required: "Digite seu e-mail",
                        email: "Digite um e-mail válido"
                    },
                    cidade: "Digite a cidade",
                    mensagem: "Digite sua mensagem",
                    nota: "Selecione uma nota",
                    CaptchaInputText: "Digite o valor da soma"
                }
            });
        });
    </script>
} 
<div id="CaminhoPagina" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
<div class="Div">
	<div class="Desabilitado"><a href="/" itemprop="url"><span itemprop="title">Home</span></a></div>
	<div class="Seta">&raquo;</div>
	<div class="Habilitado" itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><b>Depoimentos de Clientes</b></span></div>
</div>
</div>
 
<div id="Depoimentos">
<div class="Div">

	<h1 class="TituloPagina">Depoimento de clientes</h1>
    
    @if (Request.QueryString["msg"] == "sucesso")
    {
    <div class="message success">Seu depoimento foi enviado com sucesso! Depois de aprovado, ele será exibido aqui.</div>
    }
    
	<div class="Div1">

		<div class="Fisica">
			<div class="FisicaImg"><img src="/content/imagens/depoimentos_pessoa_fisica.png" width="80" height="104" border="0" alt="Pessoa física" /></div>
			<div class="FisicaTexto">Pessoas Físicas</div>
		</div>	
        
        @if (depoimentosPF.Any())
        {
		<div class="Depoimento">
        
            @foreach (var item in depoimentosPF)
            {
			<div class="Quadrado">
				<div class="Aspas"><img src="/content/imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">@item.Texto</div>
			</div>

			<div class="Seta"><img src="/content/imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">@item.Nome</div>
			</div>

			<div class="Info">
			<div class="Cidade">@item.Data.ToString("dd/MM/yyyy")</div>
			</div>
            }
		</div> 

		<div class="Mais">
        @if (ViewBag.TotalPaginas > 0)
        {
			<div class="Anterior">
            @if (ViewBag.PaginaAtual > 1)
            {
            <a href="?pagina=@(ViewBag.PaginaAtual-1)"><img src="/content/imagens/depoimentos_anteriores.png" class="AnteriorProximaImg" alt="Depoimentos anteriores" /></a>
            }
            </div> 
        }
		</div>
        
        }
		<div class="DeixarDepoimento"><img src="/content/imagens/depoimentos_deixar_fisica.png" class="DeixarDepoimentoImg" onclick="mostra('deixe_aqui_depoimento');" alt="Deixe seu depoimento como pessoa física" /></div>

	</div>

	<div class="Div2">

		<div class="Fisica">
			<div class="FisicaImg"><img src="/content/imagens/depoimentos_pessoa_juridica.png" width="79" height="105" border="0" alt="Pessoa jurídica" /></div>
			<div class="FisicaTexto">Pessoas Jurídicas</div>
		</div>	
        
        @if (depoimentosPJ.Any())
        {
		<div class="Depoimento">
        
            @foreach (var item in depoimentosPJ)
            {
			<div class="Quadrado">
				<div class="Aspas"><img src="/content/imagens/empresa_depoimentos_aspas.png" class="AspasImg" alt="Aspas" /></div>
				<div class="Texto">@item.Texto</div>
			</div>

			<div class="Seta"><img src="/content/imagens/empresa_depoimentos_seta.png" class="SetaImg" alt="Seta" /></div>

			<div class="Info">
			<div class="Nome">@item.Nome</div>
			</div>

			<div class="Info">
			<div class="Cidade">@item.Data.ToString("dd/MM/yyyy")</div>
			</div>
            }
		</div> 
            
		<div class="Mais">
            @if (ViewBag.TotalPaginas > 0)
            {
			    <div class="Proxima">
                @if (ViewBag.TotalPaginas > 1 && ViewBag.PaginaAtual < ViewBag.TotalPaginas)
                {
			    <a href="?pagina=@(ViewBag.PaginaAtual+1)"><img src="/content/imagens/depoimentos_proximos.png" class="AnteriorProximaImg" alt="Próximos depoimentos" /></a>
                }
                </div> 
            } 
        </div>
        }
		<div class="DeixarDepoimento"><img src="/content/imagens/depoimentos_deixar_juridica.png" class="DeixarDepoimentoImg" onclick="mostra('deixe_aqui_depoimento');" alt="Deixe seu depoimento como pessoa jurídica" /></div>

	</div>
    
	<div class="Formulario">
	<div class="Formulario2" id="deixe_aqui_depoimento">
    @using (Html.BeginForm("Enviar", "depoimentos-de-clientes", FormMethod.Post, new { id = "form_depoimento", name = "form_depoimento", enctype = "multipart/form-data", @class = "formulario" }))
    {   

		<div class="TituloPagina2">Deixe aqui o seu Depoimento!</div>

		<div class="FormDiv1">
			<div class="FormNomeCampo">Nome:</div>
			<div class="FormCampo"><input type="text" id="nome" name="nome" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Empresa:</div>
			<div class="FormCampo"><input type="text" id="empresa" name="empresa" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Cidade/Estado:</div>
			<div class="FormCampo"><input type="text" id="cidade" name="cidade" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Email:</div>
			<div class="FormCampo"><input type="text" id="email" name="email" class="FormCampoInput" /></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Nota para nosso serviço:</div>
			<div class="FormCampo"><select name="nota" id="nota" class="FormCampoSelect">
                            <option>-- selecione uma nota -- </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select></div>
		</div>

		<div class="FormDiv2">
			<div class="FormNomeCampo">Depoimento:</div>
			<div class="FormCampo"><textarea id="mensagem" name="mensagem" class="FormCampoTextarea"></textarea></div>
		</div>

		<div class="FormDivValidacao">
			<div class="FormNomeCampo">Validação:</div>
			<div class="FormValidacaoCampo">
				<div class="FormValidacaoImagem">@captcha</div> 
			</div>
		</div>

		<div class="FormDivBotao"><input type="submit" value="ENVIAR" class="FormBotao" onmousedown="_gaq.push(['_trackEvent', 'form', 'depoimento-clientes-pj', 'enviou'])"/>
                        <span class="processando" id="processando">Enviando...</span></div>
    }
	</div>
	</div> 
</div>
 
@Html.Action("Inferior", "Banners")
</div>