function GetUrlParameter(param) {
    /*
     * Retorna o valor de um parametro na URL.
     */

    var pageURL = window.location.search.substring(1);
    var urlVariables = pageURL.split('&');

    for (var i = 0; i < urlVariables.length; i++) {
        var parameterName = urlVariables[i].split('=');
        if (parameterName[0] == param) {
            return parameterName[1];
        }
    }
}

$(function () {
    /*
     * Página de adição de respostas
     * /NPS/Respostas?guid={guid}&nota={nota}
     */

    // Mostra / esconde textarea de comentários adicionais
    $('.other-response').change(function () {
        if ($(this).is(':checked')) {
            $('.bad-rating').slideDown();
        } else {
            $('.bad-rating').slideUp();
        }
    });

    // Se o usuário escolher uma nova nota, atualiza a página
    // com a nova nota escolhida.
    var oldRatingValue = GetUrlParameter('Nota');

    $('#ratingStar').barrating({
        theme: 'bootstrap-stars',
        initialRating: oldRatingValue, // Atribui um valor inicial com a nota dada pelo Cliente no e-mail
        onSelect: function (value, text, event) {
            if (typeof (event) !== 'undefined') {
                var newRatingValue = event.target.dataset.ratingValue,
                    URL = document.URL;

                if (URL.indexOf('Nota=') != -1) {
                    URL = URL.replace('Nota=' + oldRatingValue, 'Nota=' + newRatingValue);
                }

                window.location = URL;
            }
        }
    });
});
