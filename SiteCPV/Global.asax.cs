﻿using Domain.Entities;
using DomainExtensions.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Web.Controllers;

namespace SiteCPV
{
    public class MvcApplication : System.Web.HttpApplication
    {

        public void Application_BeginRequest(object sender, EventArgs e)
        {

            string path = Request.Path;
            foreach (var Line in File.ReadAllLines(Server.MapPath("~/assets/redirect-301.csv")))
            {
                if (path == Line.Split(',')[0])
                {
                    Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", Line.Split(',')[1]);
                    Response.End();
                }
            }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = false;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.css/{*pathInfo}");
            routes.IgnoreRoute("{resource}.js/{*pathInfo}");
            routes.IgnoreRoute("{resource}.gif/{*pathInfo}");
            routes.IgnoreRoute("{resource}.jpg/{*pathInfo}");
            routes.IgnoreRoute("{resource}.png/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ico/{*pathInfo}");
            routes.IgnoreRoute("{resource}.svg/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ttf/{*pathInfo}");
            routes.IgnoreRoute("{resource}.woff/{*pathInfo}");
            routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "content" });
            routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "scripts" });
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var webnamespace = new RouteValueDictionary();
            webnamespace.Add("namespaces", new HashSet<string>(new string[] { "Web.Controllers" }));

            routes.Add
           (
               "CoroasCategoria",
               new Route("coroa-de-flores/{URL}",
               new RouteValueDictionary
               (
                   new { controller = "coroas-de-flores", action = "LoadCoroas", DataTokens = webnamespace }),
                   null,
                   webnamespace,
                   new HyphenatedRouteHandler()
               )
           );

            routes.Add
            (
                "Coroa",
                new Route("coroas-de-flores/{rotuloUrl}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "produtos", action = "Detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "CoroaSobre",
                new Route("coroa-de-flores",
                new RouteValueDictionary
                (
                    new { controller = "coroas-de-flores", action = "Sobre", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );



            routes.Add
            (
                "Arranjo",
                new Route("arranjos-de-flores/{rotuloUrl}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "produtos", action = "Detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );


            routes.Add
            (
                "Noticias",
                new Route("noticias-de-mercado/{rotuloUrl}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "noticias-de-mercado", action = "Detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Midia",
                new Route("coroas-para-velorio-na-midia/{rotuloUrl}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "coroas-para-velorio-na-midia", action = "Detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Duvidas",
                new Route("duvidas-frequentes/{rotuloUrl}/{tipoID}",
                new RouteValueDictionary
                (
                    new { controller = "duvidas-frequentes", action = "Categoria", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Flores",
                new Route("significado-das-flores/{rotuloUrl}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "significado-das-flores", action = "Detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Sugestoes",
                new Route("coroas-de-flores/sugestoes",
                new RouteValueDictionary
                (
                    new { controller = "coroas-de-flores", action = "sugestoes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Frases",
                new Route("frases-de-homenagens/{URL}",
                new RouteValueDictionary
                (
                    new { controller = "frases-de-homenagens", action = "LoadFrases", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Local",
                new Route("coroas-de-flores/{rotuloUrl}",
                new RouteValueDictionary
                (
                    new { controller = "local", action = "Index", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            //  ROTA DE CIDADES
            routes.Add
                (
                "LocalCidade",
                new Route("cidades/{rotuloUrl}",
                new RouteValueDictionary
                (
                    new { controller = "local", action = "Index", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
           );

            //  ROTA DE SERVIÇOS
            routes.Add
                (
                "Servicos",
                new Route("servicos/{TipoServico}/coroa-de-flores-{rotuloUrl}",
                new RouteValueDictionary
                (
                    new { controller = "local", action = "Index", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
           );

            routes.Add
            (
                "Bairro",
                new Route("b/{rotuloUrl}/{estado}",
                new RouteValueDictionary
                (
                    new { controller = "bairros", action = "detalhes", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "BoletoEmpresa",
                new Route("boleto/empresa/{codigo}",
                new RouteValueDictionary
                (
                    new { controller = "boleto", action = "empresa", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Telefones",
                new Route("{controller}/telefones-de-sua-regiao",
                new RouteValueDictionary
                (
                    new { controller = "Contatos", action = "Telefones", DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );

            routes.Add
            (
                "Default",
                new Route("{controller}/{action}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "Home", action = "Index", id = UrlParameter.Optional, DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );
        }

        public class HyphenatedRouteHandler : MvcRouteHandler
        {
            protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
                requestContext.RouteData.Values["controller"] = requestContext.RouteData.Values["controller"].ToString().Replace("-", "_");
                requestContext.RouteData.Values["action"] = requestContext.RouteData.Values["action"].ToString().Replace("-", "_");
                return base.GetHttpHandler(requestContext);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if(Response.Filter != null)
                Response.Filter = null;

            var httpContext = ((MvcApplication)sender).Context;

            //if (httpContext.Request.Url.AbsolutePath.Contains("%7Bimage%7D"))
            //    return;

            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = Server.GetLastError();
            var controller = new ErrorController(new COROASEntities());
            //var routeData = new RouteData();


            //httpContext.ClearError();
            //httpContext.Response.Clear();
            //httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            //httpContext.Response.TrySkipIisCustomErrors = true;


            var Error = new HandleErrorInfo(ex, currentController, currentAction);

            #region LOG

            PersistentRepository<Log> logRepository;
            logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());

            var errorStr = Error.Exception + "<br>" + Error.Exception.StackTrace.Replace("\n", "<br>");
            logRepository.Save(new Domain.Entities.Log
            {
                Data = DateTime.Now,
                Var1 = "ERRO - URL: " + httpContext.Request.Url.AbsoluteUri,
                Var2 = "<br>" + errorStr
            });

            #endregion
          
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new Web.Factories.NinjectControllerFactory());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(Domain.Entities.Carrinho), new Web.ModelBinders.CarrinhoBinder());

        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            string result = String.Empty;
            if (custom.Equals("user", StringComparison.OrdinalIgnoreCase))
            {
                HttpCookie cookie = context.Request.Cookies[".ASPXAUTH"];
                if (cookie != null)
                {
                    return cookie.Value;
                }
            }
            else { result = base.GetVaryByCustomString(context, custom); }
            return result;

        }
    }
}