<?php

class DB {

	private static $instance;

	public static function getInstance() {

		if (!self::$instance):
			try {
				self::$instance = new PDO(self::dsn(), DB_USER, DB_PASS);
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				self::$instance->query("SET NAMES 'utf8'");
				self::$instance->query('SET character_set_connection=utf8');
				self::$instance->query('SET character_set_client=utf8');
				self::$instance->query('SET character_set_results=utf8');
			} catch (PDOException $e) {
				throw new Exception('Erro ao conectar ' . $e->getMessage());
			}
		endif;

		return self::$instance;
	}

	private static function dsn() {
		return 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
	}

	public static function fetch($table, $id) {

		$db = self::getInstance();

		$query = $db->prepare("SELECT * FROM {$table} WHERE id = ?");
		$query->execute(array($id));

		return $query->fetch(PDO::FETCH_ASSOC);
	}

	public static function insert($table, $data) {

		$columns = self::describe($table);

		$fields = array();
		$values = array();

		foreach (array_keys($data) as $field):
			if (in_array($field, $columns)):
				$fields[] = $field;
				$values[] = ':' . $field;
			endif;
		endforeach;


		$db = self::getInstance();
		$insert = $db->prepare("INSERT INTO {$table}(" . join(',', $fields) . ") VALUES(" . join(',', $values) . ")");

		foreach ($fields as $field):
			$insert->bindParam(":{$field}", $data[$field]);
		endforeach;

		$insert->execute();

		return $db->lastInsertId();
	}
	
	public static function describe($table) {

		$db = self::getInstance();

		$query = $db->query('SHOW COLUMNS FROM ' . $table);
		$columns = $query->fetchAll();

		$schema = array();

		foreach ($columns as $column):
			$schema[] = $column['Field'];
		endforeach;

		return $schema;
	}

}