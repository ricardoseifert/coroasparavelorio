<?php

if($_GET) {
	
	$url = isset($_GET['url']) ? $_GET['url'] : null;
	
	if (!$url) return;
	
	unset($_GET['url']);
	
	$parametros = array();
	
	foreach($_GET as $key => $value) {
		
		$parametros[] = "{$key}={$value}";
	}
	
	$simbolo = strpos($url, '?') ? '&' : '?';
	
	$url .= $simbolo . implode('&', $parametros);
	
	header("location:{$url}");
	
	exit();
}