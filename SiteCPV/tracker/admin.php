<?php

include('config.php');
include('db.php');

session_start();

$senha = "adm@ao5#tracker";
$dtIni;
$dtFim;

if($_GET['token'] == "86119E58141E2299C1371657BEA4B") {
	$_SESSION[ADMIN] = true;	
}

if(isset($_GET['dtIni'])) {
	$dtIni = $_GET['dtIni'];	
}

if(isset($_GET['dtFim'])) {
	$dtFim = $_GET['dtFim'];	
}

if(isset($_SESSION[ADMIN]) && $_SESSION[ADMIN] == $senha) {
	
	function processarArquivo($arquivo) {
		
		$file = file_get_contents($arquivo);
		
		return preg_split('/$\R?^/m', $file);
	}
	
	$regioes = array();
	
	$regioes_arr = processarArquivo('regioes.txt');
	
	foreach($regioes_arr as $regiao) {
		
		$colunas = explode("\t", $regiao);
		
		$regioes[$colunas[0]] = $colunas[1];
	}
	
	$nomes = array();
	
	$nomes_conta_arr = processarArquivo('conta.txt');
	
	foreach($nomes_conta_arr as $nome) {
		
		$colunas = explode("\t", $nome);
		
		$nomes[$colunas[0]] = $colunas[1];
	}

	$db = db::getInstance();

	if(isset($dtIni) && isset($dtFim)){
		$query = $db->query("SELECT * FROM acessos WHERE data >=  STR_TO_DATE('" . $dtIni . "', '%Y-%m-%dT%H:%i:%s') AND data <= STR_TO_DATE('" . $dtFim . "', '%Y-%m-%dT%H:%i:%s') ORDER BY ID DESC");
	}
	else{
		$query = $db->query("SELECT * FROM acessos WHERE data BETWEEN NOW() - INTERVAL 45 DAY AND NOW() ORDER BY ID DESC");
	}

	$acessos = $query->fetchAll(PDO::FETCH_ASSOC);
}

?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<title>Tracker AO5</title>

		<style>
		
		* {
			margin: 0;
			padding: 0;
			outline: 0;
		}
		
		body {padding: 10px}
		
		a {
			color: #333;
			text-decoration: none;
		}
		
		a:hover {text-decoration: underline;}
		
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size: 11px;
			color: #333333;
			width: 100%;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
			text-align: left;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
		
		</style>
		
	</head>

	<body>
	
		<?php if(isset($acessos)):?>
		
		<table class="gridtable">
		
			<tr>
				<td>Data</td>
				<td>Palavra</td>
				<td>Grupo</td>
				<td>Campanha</td>
				<td>Região</td>
				<td>Posição</td>
				<td>GCLID</td>
				<td>IP</td>
				<td>Cookie</td>
			</tr>
		
			<?php foreach ($acessos as $acesso): ?>
			
			<tr>
				<td><?php echo date('d/m/Y - H:i:s', strtotime($acesso['data']))?></td>
				<td><?php echo $acesso['palavra']?></td>
				<td><?php echo isset($nomes[$acesso['grupo']]) ? $nomes[$acesso['grupo']] : 'Desconhecido'?></td>
				<td><?php echo isset($nomes[$acesso['campanha']]) ? $nomes[$acesso['campanha']] : 'Desconhecido'?></td>
				<td><?php echo isset($regioes[$acesso['regiao']]) ? $regioes[$acesso['regiao']] : 'Desconhecido'?></td>
				<td><?php echo $acesso['posicao']?></td>
				<td><?php echo $acesso['gclid']?></td>
				<td><?php echo $acesso['ip']?></td>
				<td><?php echo $acesso['cookie']?></td>
			</tr>
			
			<?php endforeach; ?>
		
		</table>
		
		<?php else:?>
		
		
		
		<?php endif?>
	
	</body>

</html>