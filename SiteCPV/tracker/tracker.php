<?php

ini_set('display_errors', 0);

include('config.php');
include('db.php');

if(isset($_GET['gclid'])) {

	db::insert('acessos', array(
		'palavra' => isset($_GET['palavra']) ? $_GET['palavra'] : '',
		'grupo' => isset($_GET['grupo']) ? $_GET['grupo'] : '',
		'campanha' => isset($_GET['campanha']) ? $_GET['campanha'] : '',
		'posicao' => isset($_GET['posicao']) ? $_GET['posicao'] : '',
		'regiao' => isset($_GET['regiao']) ? $_GET['regiao'] : '',
		'gclid' => $_GET['gclid'],
		'ip' => getRealIpAddr(),
		'cookie' => getCookieID()
	));
}

function getRealIpAddr() {
	
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		
		$ip = $_SERVER['HTTP_CLIENT_IP'];
		
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		
	} else {
		
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
	return $ip;
}

function getCookieID() {
	
	return isset($_COOKIE[COOKIE_NOME]) ? $_COOKIE[COOKIE_NOME] : createCookie();
}

function createCookie() {
	
	$id = randomID();
	
	setcookie(COOKIE_NOME, $id, time() + (60 * 60 * 24 * COOKIE_TEMPO), '/');
	
	return $id;
}

function randomID() {
	
	return MD5(uniqid() . '_' . mt_rand());
}

header('Content-Type: image/gif');
echo "\x47\x49\x46\x38\x37\x61\x1\x0\x1\x0\x80\x0\x0\xfc\x6a\x6c\x0\x0\x0\x2c\x0\x0\x0\x0\x1\x0\x1\x0\x0\x2\x2\x44\x1\x0\x3b";