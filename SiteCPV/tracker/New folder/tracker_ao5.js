function getParam(p) {
	
    var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

(function() {
	
	var url = 'inserir url (com https) do arquivo tracker.php';
	
	var params = ['ads_palavra', 'ads_grupo', 'ads_campanha', 'ads_posicao', 'ads_regiao'];
	var campos = ['palavra', 'grupo', 'campanha', 'posicao', 'regiao'];
	
	var gclid = getParam('gclid');
	
	if (gclid) {
		
		url += '?gclid=' + gclid;
		
		for (var i = 0; i < params.length; i++) {

			var v = getParam(params[i]);
			
			if (v) {
				
				url += '&' + campos[i] + '=' + v;
			}
		}
		
		document.body.innerHTML += '<img src="' + url + '" style="display: none" />';
	}
})();