CREATE TABLE IF NOT EXISTS `acessos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `palavra` varchar(45) NOT NULL,
  `grupo` varchar(45) NOT NULL,
  `campanha` varchar(45) NOT NULL,
  `posicao` varchar(45) NOT NULL,
  `regiao` varchar(45) NOT NULL,
  `gclid` text NOT NULL,
  `ip` varchar(45) NOT NULL,
  `cookie` varchar(45) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;