﻿namespace Web.Models
{
    public class HeaderConfigModel
    {
        public Domain.Entities.Configuracao Configuracao { get; set; }
        public string TelAd { get; set; }
        public string CidadeNome { get; set; }
    }
}