﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace Web.Models
{
    public class PedidoModel
    {

        private Pedido _pedido;

        public Guid Codigo
        {
            get { return _pedido.Codigo; }
        }

        public string Retorno
        {
            get { return _pedido.Retorno; }
        }

        public string Mensagem
        {
            get { return _pedido.Mensagem; }
        }

        public PedidoModel(Pedido pedido)
        {
            _pedido = pedido;
        }

    }
}