﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace Web.Models
{
    public class FrasesModel
    {
        public int ID { get; set; }
        public string Tipo { get; set; }
        public string Frase { get; set; }

    }
}