﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace Web.Models
{
    public class CidadeModel
    {

        private Cidade _cidade;

        public int ID
        {
            get { return _cidade.ID; }
        }

        public int EstadoID
        {
            get { return _cidade.EstadoID; }
        }

        public string Nome
        {
            get { return _cidade.Nome; }
        }

        public CidadeModel(Cidade cidade)
        {
            _cidade = cidade;
        }

    }
}