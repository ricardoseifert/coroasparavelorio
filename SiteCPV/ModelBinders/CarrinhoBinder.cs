﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;

namespace Web.ModelBinders
{
    public class CarrinhoBinder : IModelBinder
    {
        private const string SESSION_KEY = "_carrinho";
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
                throw new InvalidOperationException("Cannot update instances");

            Carrinho carrinho = new Carrinho();

            if (controllerContext.HttpContext.Session != null)
            {
                carrinho = (Carrinho)controllerContext.HttpContext.Session[SESSION_KEY];
            }
            else
            {
                carrinho = null;
            }

            if (carrinho == null)
            {
                carrinho = new Carrinho();

                if (controllerContext.HttpContext.Session != null)
                {
                    controllerContext.HttpContext.Session[SESSION_KEY] = carrinho;
                    controllerContext.HttpContext.Session.Timeout = 360;
                }

                
            }

            return carrinho;
        }
    }
}