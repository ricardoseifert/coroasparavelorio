﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Web.Filter;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class ContatoController : BaseController
    {
        public ProdutoRepository produtoRepository;
        public ContatoController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
        }
         
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            return View();
        }

        public ActionResult Telefones()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            return View();
        }

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var nome = form["nomec"].ToUpper();
                var email = form["email"].ToLower();
                var telefone = form["telefone"];
                var mensagem = form["mensagem"];
                if (nome.Length > 0 && telefone.Length > 0 && email.Length > 0 && mensagem.Length > 0)
                {
                    //ENVIAR EMAIL
                    var corpo = string.Format("O seguinte usuário entrou em contato através do fale conosco:\n\nNOME: {0}\n\nE-MAIL: {1}\n\nTELEFONE: {2}\n\nMENSAGEM:\n{3}", nome, email, telefone, mensagem);

                    if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[COROAS PARA VELÓRIO] - Contato", corpo, false))
                        return Json(new { status = "OK", mensagem = "Sua mensagem foi enviada com sucesso! Por favor, aguarde nosso retorno." });
                    else
                        return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua mensagem, tente novamente mais tarde ou entre em contato através de nossos telefones." });
                }
                else
                    return Json(new { status = "ERRO", mensagem = "Digite todos os seus dados." });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }
    }
}
