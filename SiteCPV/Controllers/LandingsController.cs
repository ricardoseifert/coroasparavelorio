﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.Web.Security;
using Web.Models;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Entities;

namespace Web.Controllers
{
    //[[RequireHttps]
    public class LandingsController : Controller
    {
        private ClienteRepository clienteRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private ClienteModel clienteModel;
        private Carrinho Carrinho;

        public LandingsController(ObjectContext context)            
        {
            clienteModel = new ClienteModel(context);
            Carrinho = new Carrinho();
        }

        //
        // GET: /Landings/

        public ActionResult Index(int? Id, int? Color)
        {
            string ReturnCSS = string.Empty;

            switch (Color)
            {
                case 01:
                    ReturnCSS = "MainRed.css";
                    break;
                case 02:
                    ReturnCSS = "MainGreen.css";
                    break;
                case 03:
                    ReturnCSS = "Exterior.css";
                    break;
            }

            ViewBag.Style = ReturnCSS;

            switch (Id)
            {
                case 01:
                    return View("Cemiterios");
                case 02:
                    return View("Cemiterio02");
                case 03:
                    return View("Cemiterio03");
                case 04:
                    return View("Cemiterio04");
                case 05:
                    return View("Cemiterio05");
                case 06:
                    return View("Exterior");   
            }

            Response.Redirect("~/");

            return View();
        }

        public ActionResult Menu(int Id)
        {
            string ReturnView = string.Empty;
            

            switch (Id)
            {
                case 01:
                    ReturnView = "Custom/Menu01";
                    break;
                case 02:
                    ReturnView = "Custom/Menu01";
                    break;
                case 03:
                    ReturnView = "Custom/Menu01";
                    break;
                case 04:
                    ReturnView = "Custom/Menu01";
                    break;
                case 05:
                    ReturnView = "Custom/Menu01";
                    break;
                case 06:
                    ReturnView = "Custom/Menu01";
                    break;
            }

           
            ViewBag.Carrinho = Carrinho;
           

            return View(ReturnView, clienteModel);
        }

    }
}
