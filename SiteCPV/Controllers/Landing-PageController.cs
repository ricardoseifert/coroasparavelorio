﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using Domain.Repositories;

namespace Web.Controllers
{
    public class Landing_PageController : BaseController
    {
        public ProdutoRepository produtoRepository;
        private ParametroRepository parametroRepository;

        public Landing_PageController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            parametroRepository = new ParametroRepository(context);
        }


        public ActionResult Index(string Name)
        {
            var Produtos = produtoRepository.GetByExpression(r => r.ID == 22|| r.ID == 21 || r.ID == 25 || r.ID == 24 || r.ID == 20 || r.ID == 1 || r.ID == 23 || r.ID == 27 || r.ID == 18 || r.ID == 26 || r.ID == 28 || r.ID == 29 || r.ID == 30 || r.ID == 34 || r.ID == 33 || r.ID == 32 || r.ID == 2 || r.ID == 3 || r.ID == 19 || r.ID == 31 || r.ID == 364).ToList();

            switch (Name)
            {
                case "a":
                    return View("~/Views/Landing_Page/Index.cshtml");
                case "b":
                    return View("~/Views/Landing_Page/Index.cshtml");
                default:
                    break;
            }

            return View(Produtos);
        }

    }
}
