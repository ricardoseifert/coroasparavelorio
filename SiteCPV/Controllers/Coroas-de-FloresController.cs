﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Coroas_de_FloresController : BaseController
    {

        private ProdutoRepository produtoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Coloracao> coloracaoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;

        public Coroas_de_FloresController(ObjectContext context) : base(context)
        {
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            coloracaoRepository = new PersistentRepository<Coloracao>(context);
            produtoRepository = new ProdutoRepository(context);

            depoimentoRepository = new PersistentRepository<Depoimento>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {

            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location","/coroa-de-flores");
            Response.End(); 
           
            return View();
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Sobre()
        {
            var produtos = produtoRepository.GetByTipo(Domain.Entities.Produto.Tipos.CoroaFlor);

            return View("~/Views/Coroas_de_Flores/Index.cshtml", produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Sugestoes()
        {
            int faixaPreco = 0;
            int relacionamento = 0;
            int coloracao = 0;
            int? faixaPrecoID = null;
            int? relacionamentoID = null;
            int? coloracaoID = null;
            Domain.Entities.Produto.Sexos sexo = Domain.Entities.Produto.Sexos.Ambos;

            ViewBag.TermoBusca = "<b>homens</b> e <b>mulheres</b>";
            

            if (!String.IsNullOrEmpty(Request.QueryString["sexo"]))
            {
                var sexos = Request.QueryString["sexo"];
                if (!sexos.Contains("M") || !sexos.Contains("F"))
                {
                    if (sexos.Contains("M"))
                    {
                        ViewBag.TermoBusca = "<b>homens</b>";
                        sexo = Domain.Entities.Produto.Sexos.Masculino;
                    }
                    else if (sexos.Contains("F"))
                    {
                        ViewBag.TermoBusca = "<b>mulheres</b>";
                        sexo = Domain.Entities.Produto.Sexos.Feminino;
                    }
                }
            }
            
            if (!String.IsNullOrEmpty(Request.QueryString["faixa-de-preco"]))
            {
                if (int.TryParse(Request.QueryString["faixa-de-preco"], out faixaPreco))
                {
                    var f = faixaPrecoRepository.Get(faixaPreco);
                    if (f != null)
                    {
                        ViewBag.TermoBusca += ", com valores variando de <b>" + f.PrecoInicio.ToString("C2") + "</b> até <b>" + f.PrecoFim.ToString("C2") + "</b>";
                    }
                    faixaPrecoID = faixaPreco;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["relacionamento"]))
            {
                if (int.TryParse(Request.QueryString["relacionamento"], out relacionamento))
                {
                    var r = relacionamentoRepository.Get(relacionamento);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += ", indicado para um <b>" + r.Nome.ToLower() + "</b>";
                    }
                    relacionamentoID = relacionamento;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["coloracao"]))
            {
                if (int.TryParse(Request.QueryString["coloracao"], out coloracao))
                {
                    var r = coloracaoRepository.Get(coloracao);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += " com flores <b>" + r.Nome.ToLower() + "</b>";
                    }
                    coloracaoID = coloracao;
                }
            }
            var sugestoes = produtoRepository.Sugestoes(faixaPrecoID, relacionamentoID, coloracaoID, sexo);
            if (sugestoes.Count() == 0)
            {
                ViewBag.NaoEncontrado = produtoRepository.Sugestoes(null, null, coloracaoID, sexo).OrderBy(c => Guid.NewGuid()).Take(4).ToList();
            }
            ViewBag.Sugestoes = true;
            return View(sugestoes);
        }

        public ActionResult LoadCoroas(string URL)
        {
            List<Produto> LstProdutos = new List<Produto>();
            string Title = "";
            string H1 = "";
            string Description = "";

            switch (URL)
            {
                case "premium":
                    Title = "Comprar Coroas de Flores Premium";
                    H1 = Title;
                    LstProdutos = produtoRepository.GetByExpression(p => p.ID == 1 || p.ID == 2 || p.ID == 3 || p.ID == 20 || p.ID == 18 || p.ID == 19).ToList();
                    Description = Title;
                    break;
                case "luxo":
                    Title = "Comprar Coroas de Flores Luxo";
                    H1 = Title;
                    LstProdutos = produtoRepository.GetByExpression(p => p.ID == 21 || p.ID == 22 || p.ID == 23 || p.ID == 24 || p.ID == 25).ToList();
                    Description = Title;
                    break;
                case "super-luxo":
                    Title = "Comprar Coroas de Flores Super Luxo";
                    H1 = Title;
                    LstProdutos = produtoRepository.GetByExpression(p => p.ID == 27 || p.ID == 28 || p.ID == 29).ToList();
                    Description = Title;
                    break;
                case "duplas":
                    Title = "Comprar Coroas de Flores Duplas";
                    H1 = Title;
                    LstProdutos = produtoRepository.GetByExpression(p => p.ID == 30 || p.ID == 31 || p.ID == 32 || p.ID == 33 || p.ID == 34).ToList();
                    Description = Title;
                    break;
            }

            ViewBag.Title = Title;
            ViewBag.H1 = H1;
            ViewBag.Description = Description + " -  Entrega de Coroas de Flores em todo Brasil, 24 horas todos os dias + frete grátis. Acesse o site e confira!";

            return View("~/Views/Coroas_de_Flores/Categoria.cshtml", LstProdutos);
        }

        public PersistentRepository<Depoimento> depoimentoRepository { get; set; }
    }
}
