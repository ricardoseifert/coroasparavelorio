﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Coroas_para_Velorio_na_MidiaController : BaseController
    {
        private IPersistentRepository<Noticia> noticiaRepository;
        public ProdutoRepository produtoRepository;

        public Coroas_para_Velorio_na_MidiaController(ObjectContext context)
            : base(context)
        {
            noticiaRepository = new PersistentRepository<Noticia>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var resultado = noticiaRepository.GetByExpression(n => n.Publicado && n.TipoID == (int)Domain.Entities.Noticia.Tipo.MIDIA).OrderByDescending(n => n.Data).ToList();

            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            if (totalItens == 0)
                totalItens = 1;

            var totalPaginas = Math.Ceiling((decimal)totalItens / Domain.Core.Configuracoes.ITENS_PAGINA_SITE);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * Domain.Core.Configuracoes.ITENS_PAGINA_SITE).Take(Domain.Core.Configuracoes.ITENS_PAGINA_SITE).ToList();
            }
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            return View(resultado);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Detalhes(int id)
        {
            var noticia = noticiaRepository.GetByExpression(n => n.Publicado && n.ID == id && n.TipoID == (int)Domain.Entities.Noticia.Tipo.MIDIA).FirstOrDefault();
            if (noticia == null)
                return RedirectToAction("Index");
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            ViewBag.Title = noticia.Titulo + " - Coroas para Velório na Mídia";
            ViewBag.Description = noticia.TagDescription;
            ViewBag.KeyWords = noticia.TagKeywords;
            return View(noticia);
        }
    }
}
