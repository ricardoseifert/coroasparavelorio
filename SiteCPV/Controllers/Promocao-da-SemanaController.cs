﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Promocao_da_SemanaController : BaseController
    {

        private IPersistentRepository<Promocao> promocaoRepository;
        public ProdutoRepository produtoRepository;

        public Promocao_da_SemanaController(ObjectContext context)
            : base(context)
        {
            promocaoRepository = new PersistentRepository<Promocao>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            ViewBag.TemPromocaoSemana = true;
            return View(promocao); 
        }
          
    }
}
