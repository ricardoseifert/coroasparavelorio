﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Esqueci_Minha_SenhaController : BaseController
    {
        private IPersistentRepository<Cliente> clienteRepository;
        public Esqueci_Minha_SenhaController(ObjectContext context) : base(context)
        {
            clienteRepository = new PersistentRepository<Cliente>(context);
        }
        public ActionResult Index()
        {
            return View();
        }
        //TODO: MUDAR EMAIL ORIGEM
        [HttpPost]
        public JsonResult Enviar(string email)
        {
            var cliente = clienteRepository.GetByExpression(a => a.Email.Trim() == email.Trim()).FirstOrDefault();

            if (cliente != null)
            {
                //ENVIAR EMAIL
                var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/esqueci-senha.htm"));
                html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                html = html.Replace("[NOME]", cliente.Nome);
                html = html.Replace("[EMAIL]", cliente.Email);
                html = html.Replace("[SENHA]", cliente.Senha);

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "COROAS PARA VELÓRIO", cliente.Email, cliente.Nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "Coroas para Velório", "", "[COROAS PARA VELÓRIO] - Seus dados para acesso", html, true))
                    return Json("Sua senha foi enviada para o e-mail " + cliente.Email.ToLower() + ". Por favor, verifique se o e-mail " + Domain.Core.Configuracoes.EMAIL_NOREPLY + " não está bloqueado como SPAM.");
                else
                    return Json("Não foi possível enviar sua senha, tente novamente mais tarde.");
            }
            else
            {
                return Json("Este e-mail não está cadastrado em nosso sistema.");
            }
        }
    }
}
