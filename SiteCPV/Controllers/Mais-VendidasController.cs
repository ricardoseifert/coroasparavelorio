﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Mais_VendidasController : BaseController
    {
         
        private IPersistentRepository<Produto> produtoRepository;

        public Mais_VendidasController(ObjectContext context)
            : base(context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);  
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var produtos = produtoRepository.GetByExpression(c => c.Disponivel && c.MaisVendida > 0 && c.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.MaisVendida).ToList();
            produtos = produtos.Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).ToList();
            return View(produtos);
        }
          
    }
}
