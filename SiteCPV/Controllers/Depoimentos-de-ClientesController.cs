﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Depoimentos_de_ClientesController : BaseController
    {
        private IPersistentRepository<Depoimento> depoimentoRepository;
        public ProdutoRepository produtoRepository;

        public Depoimentos_de_ClientesController(ObjectContext context) : base(context)
        {
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var pfs = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PF).OrderByDescending(d => d.Data).ToList();
            var pjs = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PJ).OrderByDescending(d => d.Data).ToList();

            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = (pfs.Count > pjs.Count) ? pfs.Count : pjs.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 2);
            if (totalPaginas > 1)
            {
                ViewBag.DepoimentosPF = pfs.Skip((paginaAtual - 1) * 2).Take(2).ToList();
                ViewBag.DepoimentosPJ = pfs.Skip((paginaAtual - 1) * 2).Take(2).ToList();
            }
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalPaginas = totalPaginas;
            return View();
        }
     
        [HttpPost, CaptchaVerify("A soma não é válida")]
        public JsonResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var nome = form["nome"].ToUpper();
                    var email = form["email"].ToLower();
                    var cidade = form["cidade"].ToUpper();
                    var empresa = "";
                    try
                    {
                        empresa = form["empresa"].ToUpper();
                    }
                    catch { }
                    var tipoID = Convert.ToInt32(form["tipoID"]);
                    var nota = Convert.ToInt32(form["nota"]);
                    var mensagem = form["mensagem"];

                    //ENVIAR EMAIL
                    var corpo = string.Format("O seguinte usuário enviou um novo depoimento, acesse o administrador para aprová-lo:\n\nNOME: {0}\n\nE-MAIL: {1}\n\nCIDADE: {2}\n\nMENSAGEM\n{3}"
                        , nome, email, cidade, mensagem);

                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[COROAS PARA VELÓRIO] - Depoimentos", corpo, false);

                    var depoimento = new Depoimento();
                    depoimento.Nome = nome;
                    depoimento.Email = email;
                    depoimento.Cidade = cidade;
                    depoimento.Nota = nota;
                    depoimento.Texto = mensagem;
                    depoimento.Data = DateTime.Now;
                    depoimento.TipoID = tipoID;
                    depoimento.Aprovado = false;
                    depoimentoRepository.Save(depoimento);

                    return Json(new { status = "OK", mensagem = "Seu depoimento foi enviado com sucesso! Assim que aprovado, será exibido nesta área. Obrigado." });
                }
                catch
                {
                    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar seu depoimento. Ocorreu um erro. Tente novamente mais tarde ou envie um email para contato@coroasparavelorio.com.br." });
                }
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }
    }
}
