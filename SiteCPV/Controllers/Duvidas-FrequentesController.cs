﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Duvidas_FrequentesController : BaseController
    {
        private IPersistentRepository<Duvida> duvidasRepository;
        public ProdutoRepository produtoRepository;

        public Duvidas_FrequentesController(ObjectContext context)
            : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            duvidasRepository = new PersistentRepository<Duvida>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            var duvidas = duvidasRepository.GetByExpression(n => n.Publicado).OrderBy(n => n.Titulo).ToList(); 
            return View(duvidas);
        }
         
    }
}
