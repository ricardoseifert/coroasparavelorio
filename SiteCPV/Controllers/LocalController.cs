﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Web.Models;
using Web.Handler;
using Domain.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class LocalController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<Redirect> redirectRepository;
        private IPersistentRepository<Falecido> falecidoRepository;

        public struct Resultado
        {
            public string Tipo;
            public List<Local> Locais;
        }

        public struct Local
        {
            public string Nome;
            public string Url;
        }

        public LocalController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            produtoRepository = new ProdutoRepository(context);
            redirectRepository = new PersistentRepository<Redirect>(context);
            falecidoRepository = new PersistentRepository<Falecido>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index(string rotuloUrl)
        {
            #region TRAVA LOCAIS DESEJADOS 

            // TRAVA CIDADES
            if (Request.Path.Split('/').Count() > 0)
            {
                if (Request.Path.Split('/')[1] == "cidades")
                {
                    if (Request.Path != "/cidades/coroa-de-flores-campinas-sp")
                    {
                        Response.End();
                        return null;
                    }
                }
            }

            // TRAVA LOCAIS
            if (Request.Path.Split('/').Count() > 0)
            {
                if (Request.Path.Split('/')[1] == "servicos")
                {
                    if (Request.Path != "/servicos/cemiterios/coroa-de-flores-cemiterio-campo-da-esperanca-brasilia-df")
                    {
                        Response.End();
                        return null;
                    }
                }
            }

            #endregion

            var local = localRepository.GetByRotuloUrl(rotuloUrl);
            if (local != null)
            {
                if (local.CidadeID == 5318 || local.CidadeID == 5554 || local.CidadeID == 8562)
                {
                    var DatetimTimeAddDays = DateTime.Now.AddDays(1);

                    ViewBag.Falecidos = falecidoRepository.GetByExpression(
                        r => r.CidadeID == local.CidadeID
                        &&
                        (r.DataSepultamento >= DateTime.Now
                        &&
                        r.DataSepultamento <= DatetimTimeAddDays
                        )
                        &&
                        r.DataSepultamento != null).ToList().OrderByDescending(p => p.DataSepultamento).ToList();
                }

                #region TITLE E DESCRIPTIONS

                switch (local.RotuloUrl)
                {
                    case "coroa-de-flores-sao-paulo-sp":
                        ViewBag.Title = "Coroas de Flores São Paulo | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para São Paulo. Envio imediato para todos os cemitérios e velórios de São Paulo. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-rio-de-janeiro-rj":
                        ViewBag.Title = "Coroas de Flores Rio de Janeiro | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Rio de Janeiro. Envio imediato para todos os cemitérios e velórios de Rio de Janeiro. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-belo-horizonte-mg":
                        ViewBag.Title = "Coroas de Flores Belo Horizonte | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Belo Horizonte. Envio imediato para todos os cemitérios e velórios de Belo Horizonte. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-brasilia-df":
                        ViewBag.Title = "Coroas de Flores Brasília | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Brasília. Envio imediato para todos os cemitérios e velórios de Brasília. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-porto-alegre-rs":
                        ViewBag.Title = "Coroas de Flores Porto Alegre | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Porto Alegre. Envio imediato para todos os cemitérios e velórios de Porto Alegre. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-curitiba-pr":
                        ViewBag.Title = "Coroas de Flores Curitiba | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Curitiba. Envio imediato para todos os cemitérios e velórios de Curitiba. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-goiania-go":
                        ViewBag.Title = "Coroas de Flores Goiânia | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Goiânia. Envio imediato para todos os cemitérios e velórios de Goiânia. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-recife-pe":
                        ViewBag.Title = "Coroas de Flores Recife | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Recife. Envio imediato para todos os cemitérios e velórios de Recife. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-fortaleza-ce":
                        ViewBag.Title = "Coroas de Flores Fortaleza | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Fortaleza. Envio imediato para todos os cemitérios e velórios de Fortaleza. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-salvador-ba":
                        ViewBag.Title = "Coroas de Flores Salvador | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Salvador. Envio imediato para todos os cemitérios e velórios de Salvador. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-campinas-sp":
                        ViewBag.Title = "Coroas de Flores Campinas | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Campinas. Envio imediato para todos os cemitérios e velórios de Campinas. Ligue 0800 777 1986 ou Acesse nosso site";
                        break;
                    case "coroa-de-flores-santos-sp":
                        ViewBag.Title = "Coroas de Flores Santos | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Santos. Envio imediato para todos os cemitérios e velórios de Santos. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-cuiaba-mt":
                        ViewBag.Title = "Coroas de Flores Cuiabá | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Cuiabá. Envio imediato para todos os cemitérios e velórios de Cuiabá. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-campo-grande-ms":
                        ViewBag.Title = "Coroas de Flores Campo Grande | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Campo Grande. Envio imediato para todos os cemitérios e velórios de Campo Grande. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-florianopolis-sc":
                        ViewBag.Title = "Coroas de Flores Florianópolis | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Florianópolis. Envio imediato para todos os cemitérios e velórios de Florianópolis. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-sao-jose-dos-campos-sp":
                        ViewBag.Title = "Coroas de Flores São José dos Campos | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para São José dos Campos. Envio imediato para todos os cemitérios e velórios de São José dos Campos. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-guarulhos-sp":
                        ViewBag.Title = "Coroas de Flores Guarulhos | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Guarulhos. Envio imediato para todos os cemitérios e velórios de Guarulhos. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-jundiai-sp":
                        ViewBag.Title = "Coroas de Flores Jundiaí | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Jundiaí. Envio imediato para todos os cemitérios e velórios de Jundiaí. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-niteroi-rj":
                        ViewBag.Title = "Coroas de Flores Niterói | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Niterói. Envio imediato para todos os cemitérios e velórios de Niterói. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-embu-das-artes-sp":
                        ViewBag.Title = "Coroas de Flores Embú das Artes | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Embú das Artes. Envio imediato para todos os cemitérios e velórios de Embú das Artes. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-sao-bernardo-do-campo-sp":
                        ViewBag.Title = "Coroas de Flores São Bernando do Campo | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para São Bernando do Campo . Envio imediato para todos os cemitérios e velórios de São Bernando do Campo. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-santo-andre-sp":
                        ViewBag.Title = "Coroas de Flores Santo André | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Santo André. Envio imediato para todos os cemitérios e velórios de Santo André. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-paulista-pe":
                        ViewBag.Title = "Coroas de Flores Paulista  | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Paulista. Envio imediato para todos os cemitérios e velórios de Paulista. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-ribeirao-preto-sp":
                        ViewBag.Title = "Coroas de Flores Ribeirão Preto | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Ribeirão Preto. Envio imediato para todos os cemitérios e velórios de Ribeirão Preto. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-sorocaba-sp":
                        ViewBag.Title = "Coroas de Flores Sorocaba | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Sorocaba. Envio imediato para todos os cemitérios e velórios de Sorocaba. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-contagem-mg":
                        ViewBag.Title = "Coroas de Flores Contagem | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Contagem. Envio imediato para todos os cemitérios e velórios de Contagem. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-joinville-sc":
                        ViewBag.Title = "Coroas de Flores Joinville | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Joinville. Envio imediato para todos os cemitérios e velórios de Joinville. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-itapecerica-da-serra-sp":
                        ViewBag.Title = "Coroas de Flores Itapecerica da Serra | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Itapecerica da Serra. Envio imediato para todos os cemitérios e velórios de Itapecerica da Serra. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-juiz-de-fora-mg":
                        ViewBag.Title = "Coroas de Flores Juiz de Fora | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Itapecerica da Serra. Envio imediato para todos os cemitérios e velórios de Itapecerica da Serra. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-osasco-sp":
                        ViewBag.Title = "Coroas de Flores Osasco | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Osasco. Envio imediato para todos os cemitérios e velórios de Osasco. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-santa-luzia-mg":
                        ViewBag.Title = "Coroas de Flores Santa Luzia | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Santa Luzia. Envio imediato para todos os cemitérios e velórios de Santa Luzia. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-diadema-sp":
                        ViewBag.Title = "Coroas de Flores Diadema | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Diadema. Envio imediato para todos os cemitérios e velórios de Diadema. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-taboao-da-serra-sp":
                        ViewBag.Title = "Coroas de Flores Taboão da Serra | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Taboão da Serra. Envio imediato para todos os cemitérios e velórios de Taboão da Serra. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-serra-es":
                        ViewBag.Title = "Coroas de Flores Serra | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Serra. Envio imediato para todos os cemitérios e velórios de Serra. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-sao-caetano-do-sul-sp":
                        ViewBag.Title = "Coroas de Flores São Caetano do Sul | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para São Caetano do Sul. Envio imediato para todos os cemitérios e velórios de São Caetano do Sul. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-mogi-das-cruzes":
                        ViewBag.Title = "Coroas de Flores Mogi das Cruzes | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Mogi das Cruzes. Envio imediato para todos os cemitérios e velórios de Mogi das Cruzes. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-sao-goncalo-rj":
                        ViewBag.Title = "Coroas de Flores São Gonçalo | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para São Gonçalo. Envio imediato para todos os cemitérios e velórios de São Gonçalo. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-londrina-pr":
                        ViewBag.Title = "Coroas de Flores Londrina | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Londrina. Envio imediato para todos os cemitérios e velórios de Londrina. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-taubate-sp":
                        ViewBag.Title = "coroa-de-flores-taubate-sp";
                        ViewBag.Description = "Coroa de Flores para Taubaté. Envio imediato para todos os cemitérios e velórios de Taubaté. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-taguatinga":
                        ViewBag.Title = "Coroas de Flores Taguatinga | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Taguatinga. Envio imediato para todos os cemitérios e velórios de Taguatinga. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-maringa-pr":
                        ViewBag.Title = "Coroas de Flores Maringá | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Maringá. Envio imediato para todos os cemitérios e velórios de Maringá. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-piracicaba-sp":
                        ViewBag.Title = "Coroas de Flores Piracicaba | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Piracicaba. Envio imediato para todos os cemitérios e velórios de Piracicaba. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-suzano-sp":
                        ViewBag.Title = "Coroas de Flores Suzano | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Suzano. Envio imediato para todos os cemitérios e velórios de Suzano. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-uberlandia-mg":
                        ViewBag.Title = "Coroas de Flores Uberlândia | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Uberlândia. Envio imediato para todos os cemitérios e velórios de Uberlândia. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-jaboatao-dos-guararapes-pe":
                        ViewBag.Title = "Coroas de Flores Jaboatão dos Guararapes | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Jaboatão dos Guararapes. Envio imediato para todos os cemitérios e velórios de Jaboatão dos Guararapes. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-vila-velha-es":
                        ViewBag.Title = "Coroas de Flores Vila Velha | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Vila Velha. Envio imediato para todos os cemitérios e velórios de Vila Velha. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-bauru-sp":
                        ViewBag.Title = "Coroas de Flores Bauru | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Bauru. Envio imediato para todos os cemitérios e velórios de Bauru. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-blumenau-sc":
                        ViewBag.Title = "Coroas de Flores Blumenau | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Blumenau. Envio imediato para todos os cemitérios e velórios de Blumenau. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-maua-sp":
                        ViewBag.Title = "Coroas de Flores Mauá | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Mauá. Envio imediato para todos os cemitérios e velórios de Mauá. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    case "coroa-de-flores-limeira-sp":
                        ViewBag.Title = "Coroas de Flores Limeira | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para Limeira. Envio imediato para todos os cemitérios e velórios de Limeira. Ligue 0800 777 1986 ou Acesse nosso site!";
                        break;
                    default:
                        ViewBag.Title = "Coroa de Flores para " + local.Titulo + " / " + local.Estado.Sigla + " | Coroas para Velório";
                        ViewBag.Description = "Coroa de Flores para " + local.Titulo + " / " + local.Estado.Sigla + " - Atendimento e entrega em todo Brasil, 24 horas por dia + frete grátis. Confira!";
                        break;
                }


                #endregion

                if (Request.Path == "/coroas-de-flores/cemiterio-vila-alpina-sao-pedro-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-quarta-parada-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-araca-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-vila-formosa-i-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-de-inhauma" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-jaragua-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-chora-menino-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-sao-francisco-xavier-niteroi-rj" ||
                    Request.Path == "/coroas-de-flores/cemiterio-vila-mariana-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-lapa-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/funeral-home-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-dom-bosco-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-e-crematorio-horto-da-paz-itapecerica-da-serra-sp" ||
                    Request.Path == "/coroas-de-flores/funeraria-house-belo-horizonte-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-bonfim-belo-horizonte-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-necrpole-parque-da-colina-belo-horizonte-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-iraja-rio-de-janeiro-rj" ||
                    Request.Path == "/coroas-de-flores/cemiterio-memorial-parque-paulista-embu-das-artes-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-memorial-de-goiania-goiania-go" ||
                    Request.Path == "/coroas-de-flores/crematorio-metropolitano-sao-jose-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-jardim-da-paz-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemterio-arcanjo-sao-miguel-e-almas-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-flamboyant-campinas-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-agua-verde-curitiba-pr" ||
                    Request.Path == "/coroas-de-flores/cemiterio-e-crematorio-vaticano-balneario-camboriu-sc" ||
                    Request.Path == "/coroas-de-flores/cemiterio-municipal-itacorubi-sao-francisco-de-assis-floranipolis-sc" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-carmo-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-dos-girassois-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-vale-da-paz-diadema-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-da-colina-sao-bernardo-do-campo-sp" ||
                    Request.Path == "/servicos/cemiterios/coroa-de-flores-cemiterio-campo-da-esperanca-brasilia-df" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-saudade-campinas-campinas-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-da-saudade-salvador-ba" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-bosque-da-esperanca-belo-horizonte-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-da-saudade-fortaleza-ce" ||
                    Request.Path == "/coroas-de-flores/cemiterio-vertical-memorial-do-carmo-rio-de-janeiro-rj" ||
                    Request.Path == "/coroas-de-flores/capela-jardim-cuiaba-mt" ||
                    Request.Path == "/coroas-de-flores/cemiterio-gethsemani-morumbi-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-de-congonhas-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-ecumenico-joao-xxiii-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-valle-dos-reis-taboao-da-serra-sp" ||
                    Request.Path == "/cemiterio-parque-nossa-senhora-da-conceicao-campinas-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-do-morumbi-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/crematorio-vila-alpina-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-iguaçu-curitiba-pr" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-das-flores-campinas-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-renascer-contagem-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-das-cerejeiras-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-primaveras-guarulhos-sp" ||
                    Request.Path == "/coroas-de-flores/funeraria-ossel-1-sorocaba-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-jardim-sao-vicente-canoas-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-bosque-da-paz-salvador-ba" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-da-saudade-de-paciencia-rio-de-janeiro-rj" ||
                    Request.Path == "/coroas-de-flores/cemiterio-israelita-da-vila-mariana-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-e-memorial-guararapes-jaboatao-dos-guararapes-pe" ||
                    Request.Path == "/coroas-de-flores/velorio-campos-eliseos-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-das-aleias-campinas-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-santa-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-alphacampus-jandira-sp" ||
                    Request.Path == "/coroas-de-flores/memorial-parque-dos-girassois-ribeirao-preto-sp" ||
                    Request.Path == "/coroas-de-flores/funeraria-ofebas-sorocaba-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-campo-santo-salvador-ba" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-saudade-sulacap-rio-de-janeiro-rj" ||
                    Request.Path == "/coroas-de-flores/funeraria-terra-branca-bauru-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-vila-euclides-são-bernado-do-campo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-dos-pinheiros-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-da-ressurreicao-piracicaba-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-nossa-senhora-do-carmo-curuca-santo-andre-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-parque-dos-ipes-itapecerica-da-serra-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-horto-florestal-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/hospital-beneficencia-portuguesa-santos-sp" ||
                    Request.Path == "/coroas-de-flores/velorio-sao-vicente-sp" ||
                    Request.Path == "/coroas-de-flores/memorial-necropole-ecumenica-santos-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-saint-hilare-viamao-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-municipal-de-taguatinga-brasilia-df" ||
                    Request.Path == "/coroas-de-flores/cemiterio-memorial-vertical-santos-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-gama-brasilia-df" ||
                    Request.Path == "/coroas-de-flores/cemiterio-do-campo-grande-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-saudade-vila-assuncao-santo-andre-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-bela-vista-osasco-sp" ||
                    Request.Path == "/coroas-de-flores/velorio-municipal-de-jundiai-sp" ||
                    Request.Path == "/coroas-de-flores/velorio-hospital-albert-einstein-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/funeraria-urbam-municipal-jose-dos-campos-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-pauliceia-sao-bernado-do-campo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-das-garcas-santana-do-parnaiba-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-memorial-jardim-santo-andre-santo-andre-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-gethsemani-anhanguera-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-santo-amaro-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-da-paz-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-jardim-da-paz-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/coroa-de-flores-jardim-das-palmeiras-goiania-go" ||
                    Request.Path == "/coroas-de-flores/cemiterio-morada-da-paz-recife-pe" ||
                    Request.Path == "/coroas-de-flores/cemiterio-necrpole-parque-da-colina-belo-horizonte-mg" ||
                    Request.Path == "/coroas-de-flores/cemiterio-sao-joao-batista-rio-de-janeiro-rj" ||
                    Request.Path == "/coroas-de-flores/cemiterio-comunidade-sao-jose-porto-alegre-rs" ||
                    Request.Path == "/coroas-de-flores/cemiterio-vila-rio-guarulhos-sp" ||
                    Request.Path == "/coroas-de-flores/funeraria-paz-universal-uberlandia-mg" ||
                    Request.Path == "/coroas-de-flores/funeraria-prever-maringa-pr" ||
                    Request.Path == "/coroas-de-flores/funeraria-e-crematorio-vaticano-curitiba-pr"
                    )
                {
                    ViewBag.Title = local.Titulo + " - " + local.Estado.Sigla + " | Coroas para Velório";
                    ViewBag.Description = "Coroa de Flores" + " " + local.Titulo + " - " + local.Estado.Sigla + ". " + "Envio imediato para todos os cemitérios e velórios de" + " " + local.Cidade.Nome + ". " + "Ligue 0800 777 1986 ou Acesse nosso site!";
                }

                else if (Request.Path == "/coroas-de-flores/cemiterio-sao-paulo-sao-paulo-sp" ||
                    Request.Path == "/coroas-de-flores/complexo-vale-do-cerrado-goiania-go" ||
                    Request.Path == "/coroas-de-flores/cemiterio-cristo-rei-vila-pires-santo-andre-sp" ||
                    Request.Path == "/coroas-de-flores/cemiterio-municipal-joinville-sc" ||
                    Request.Path == "/coroas-de-flores/cemiterio-jardim-da-paz-serra-es" ||
                    Request.Path == "/coroas-de-flores/cemiterio-luterano-curitiba-pr" ||
                    Request.Path == "/coroas-de-flores/cemiterio-sao-gon%C3%A7alo-sao-goncalo-rj" ||
                    Request.Path == "/coroas-de-flores/cemiterio-parque-maringa-maringa-pr" ||
                    Request.Path == "/coroas-de-flores/capela-sao-jose-guarulhos-sp")
                {
                    ViewBag.Title = local.Titulo + " | Coroas para Velório";
                    ViewBag.Description = "Coroa de Flores" + " " + local.Titulo + " - " + local.Estado.Sigla + ". " + "Atendimento e entrega para todo Brasil, 24 horas e com fretes grátis. Coroas Para Velório. Clique e Confira!";
                }

                ViewBag.Local = local.Cidade.NomeCompleto;
                ViewBag.Keywords = local.TagKeywords;
                ViewBag.Televendas = local.Televendas;

                ViewBag.Produtos = produtoRepository.Publicados().ToList();

                Session["local"] = local;

                ViewBag.LocaisCidade = localRepository.GetByExpression(c => c.CidadeID == local.CidadeID && c.Disponivel && c.ID != local.ID).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();

                // ALTERACAO PARA INVERSAO DE TEXTO x COROAS
                if(Request.Path == "/coroas-de-flores/coroa-de-flores-porto-alegre-rs" || Request.Path == "/coroas-de-flores/coroa-de-flores-brasilia-df")
                {
                    return View("~/VIews/Local/ConteudoInvertido.cshtml", local);
                }
                else
                {
                    return View(local);
                }
            }
            else
            {
                var pagina = "/coroas-de-flores/" + rotuloUrl;
                var redirect = redirectRepository.GetByExpression(c => c.Origem.Contains(pagina)).FirstOrDefault();
                if (redirect == null)
                {

                    Response.StatusCode = 404;
                    Response.TrySkipIisCustomErrors = true;

                    var destaques = produtoRepository.DestaquesHome();
                    ViewBag.Destaques = destaques;
                    ViewBag.Pagina = pagina;

                    return RedirectToAction("Oops", "Error", new { url = pagina});
                }
                else
                {
                    return RedirectPermanent(redirect.Destino);
                }
            }
        }

        [HttpGet]
        public ActionResult Buscar(string termo)
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            var resultado = new List<Domain.Entities.Local>();
            if(!String.IsNullOrEmpty(termo))
                resultado = localRepository.Buscar(termo).OrderBy(c => c.Titulo).OrderBy(c=> c.TipoID).ToList();
            else
                resultado = localRepository.GetAll().OrderBy(c => c.Titulo).OrderBy(c=> c.TipoID).ToList();

            if (Request.QueryString["cidadeID"] != null)
            {
                var cidadeID = 0;
                Int32.TryParse(Request.QueryString["cidadeID"], out cidadeID);
                if (cidadeID > 0)
                {
                    resultado = resultado.Where(c => c.CidadeID == cidadeID).ToList();
                }
            }
            if (Request.QueryString["tipo"] != null)
            {
                switch (Request.QueryString["tipo"])
                {
                    case "cemiterios":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Cemiterio).ToList();
                        break;
                    case "funerarias":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Funeraria).ToList();
                        break;
                    case "hospitais":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Hospital).ToList();
                        break;
                    case "capelas":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Capela).ToList();
                        break;
                    case "igrejas":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Igreja).ToList();
                        break;
                    case "velorios":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Velorio).ToList();
                        break;
                    case "cidades":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade).ToList();
                        break;
                    case "crematorios":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Crematorio).ToList();
                        break;
                }
            }
            if (Request.QueryString["cidades"] != null)
            {
                resultado = localRepository.Buscar(termo, (int)Domain.Entities.Local.Tipos.Cidade).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();
            }
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 100);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * 100).Take(100).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Destaques = produtoRepository.DestaquesHome(4);
            return View(resultado);
        }

        [HttpGet]
        public JsonResult BuscarRapido(string termo)
        {
            var resultado = localRepository.GetByExpression(c => c.Titulo.Contains(termo)).OrderBy(c => c.Titulo).OrderBy(c => c.Titulo).Take(10).ToList();
            var resultados = new List<Autocomplete>();
            foreach (var local in resultado)
            {

                var Lines = System.IO.File.ReadLines(Server.MapPath("~/assets/redirect-local-rotas.csv")).ToList();

                if (Lines.Where(r => r.Split(',')[0] == local.ID.ToString()).Any())
                {
                    var URL = Lines.Where(r => r.Split(',')[0] == local.ID.ToString()).First().Split(',')[1];

                    resultados.Add(new Autocomplete { label = local.Titulo + " - " + local.Cidade.NomeCompleto, category = Domain.Helpers.EnumHelper.GetDescription(local.Tipo), url = URL });
                }
                else
                {
                    resultados.Add(new Autocomplete { label = local.Titulo + " - " + local.Cidade.NomeCompleto, category = Domain.Helpers.EnumHelper.GetDescription(local.Tipo), url = Url.RouteUrl("Local", new { rotuloUrl = local.RotuloUrl }) });
                }
            }
            return Json(resultados, JsonRequestBehavior.AllowGet); 
        }

        public JsonResult Cidades(int estadoID)
        {
            var cidades = new List<CidadeModel>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null)
            {
                foreach (var cidade in estado.Cidades)
                {
                    cidades.Add(new CidadeModel(cidade));
                }
            }
            return Json(cidades);
        }

        public JsonResult CidadesPorOrdem(int estadoID)
        {
            var cidades = new List<CidadeModel>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null)
            {
                foreach (var cidade in estado.Cidades.OrderByDescending(r => r.Ordem).ThenBy(e => e.Nome))
                {
                    cidades.Add(new CidadeModel(cidade));
                }
            }
            return Json(cidades);
        }

        public JsonResult GetLocalExiste(string local, int cidadeID, int estadoID)
        {
            if (local == "LOCAL NÃO CADASTRADO")
            {
                //var _localNaoCadastrado = Json(true + "," + "0", JsonRequestBehavior.AllowGet);

                string _localNaoCadastradolId = "0";
                string _localNaoCadastradoTitulo = local;
                string _localNaoCadastradoTel = "LOCAL NÃO CADASTRADO";

                var _dataLocalNaoCadastrado = Json(true + "," + _localNaoCadastradolId + "," + _localNaoCadastradoTitulo + "," + _localNaoCadastradoTel, JsonRequestBehavior.AllowGet);

                return _dataLocalNaoCadastrado;
            }

            bool Existe = false;
            var Local = localRepository.GetByExpression(r => r.Titulo.ToUpper() == local.ToUpper() && r.CidadeID == cidadeID && r.EstadoID == estadoID).ToList();

            if (Local.Count == 0)
            {
                Existe = false;
                return Json(Existe.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                Existe = true;

                var _localEncontrado = Local.First();
                string _localId = string.IsNullOrEmpty(_localEncontrado.ID.ToString()) ? "" : _localEncontrado.ID.ToString();
                string _localTitulo = string.IsNullOrEmpty(Local.First().Titulo) ? "" : _localEncontrado.Titulo.ToString();
                string _localTel = string.IsNullOrEmpty(Local.First().Telefone) ? "" : Local.First().Telefone.ToString();

                var _dataLocalEncontrado = Json(Existe.ToString() + "," + _localId + "," + _localTitulo + "," + _localTel, JsonRequestBehavior.AllowGet);

                return _dataLocalEncontrado;
            }
        }

        [HttpPost]
        public JsonResult GetLocais(int CidadeID)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            foreach (var Result in localRepository.GetByExpression(r => r.CidadeID == CidadeID).OrderBy(r => r.Titulo).ToList())
            {
                result.Add(new SelectListItem { Text = Result.Titulo, Value = Result.ID.ToString() });
            }

            result.Insert(0, new SelectListItem { Text = "LOCAL NÃO CADASTRADO", Value = "NULL" });

            return Json(result);
        }

    }
}
