﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Cidades_e_RegioesController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;

        public Cidades_e_RegioesController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var locais = new List<Local>();
            locais.AddRange(localRepository.GetByTipo(Local.Tipos.Cidade));

            foreach (var local in locais)
                local.TotalLocaisRelacionados = localRepository.TotalLocaisRelacionados(local);
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            ViewBag.Destaques = locais.Where(c => c.Destaque).OrderBy(c => c.Titulo).Take(20).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome).ToList();
            return View(locais);
        }

    }
}
