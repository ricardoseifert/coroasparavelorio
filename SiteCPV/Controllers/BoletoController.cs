﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Net;
using Domain.Core;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class BoletoController : Controller
    {

        private PedidoRepository pedidoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;

        public BoletoController(ObjectContext context)
        {
            pedidoRepository = new PedidoRepository(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
        }

        public ActionResult Index(string codigo)
        {
            var guid = Guid.NewGuid();
            Guid.TryParse(codigo, out guid);
            var pedido = pedidoRepository.GetByCodigo(guid);
            if (pedido != null)
            {
                if(pedido.BoletoPagarMes.Count > 0)
                {
                    BoletoPagarMe BoletoPago = null;
                    BoletoPagarMe BoletoAberto = null;

                    if (pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).ToList().Count == 0)
                        BoletoPago = pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.Pago).First();

                    if (pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).ToList().Count > 0)
                        BoletoAberto = pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).First();

                    if (BoletoPago != null)
                    {
                        Response.Write("Boleto já pago. Data de pagamento: " + BoletoPago.DataPagamento + ". Para mais informações, entre em contato com nossa central de atendimento no 0800-777-1986.");
                        Response.End();
                    }

                    if (BoletoAberto != null)
                    {
                        Response.Redirect(BoletoAberto.BoletoURL);
                    }

                    return Content("");
                }
                else if (pedido.Boleto != null)
                {
                    var html = BoletoService.Gerar(pedido.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, pedido.Numero + "_boleto.pdf");
                    return Content("");
                }
                else
                {
                    return Content("Grupo Laços Flores<Br>Link inválido. Entre em contato com nossa central de atendimento no 0800-777-1986.");
                }
                    
            }
            else
                return Content("Grupo Laços Flores<Br>Link inválido, o pedido não existe ou foi cancelado. Entre em contato com nossa central de atendimento no 0800-777-1986.");
        }

        //public ActionResult DownloadPDFPagarme(string UrlPagarme)
        //{
        //    string id = "";

        //    using (WebClient client = new WebClient())
        //    {
        //        id = Guid.NewGuid().ToString();
        //        var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".pdf");

        //        string url = @"http://api.pdflayer.com/api/convert?access_key=422086415fa2a1a3913b98f08eb38172&document_url=" + UrlPagarme + "&document_name=" + id + ".pdf";

        //        client.DownloadFile(url, Path);

        //        Response.Clear();
        //        Response.ContentType = "application/pdf";
        //        Response.AppendHeader("Content-Disposition", "attachment; filename=" + id + ".pdf");
        //        Response.TransmitFile(Path);
        //        Response.End();

        //        System.IO.File.Delete(Path);
        //    }

        //    return Content("");
        //}
        public ActionResult Empresa(string codigo)
        {
            var id = Convert.ToInt32(Domain.Core.Criptografia.Decrypt(codigo));
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.Boleto != null)
                {
                    var html = BoletoService.Gerar(fatura.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                    return Content("");
                }
                else
                    return Content("Grupo Laços Flores<Br>Link inválido. Entre em contato com nossa central de atendimento no 0800-777-1986.");
            }
            else
                return Content("Grupo Laços Flores<Br>Link inválido, o pedido não existe ou foi cancelado. Entre em contato com nossa central de atendimento no 0800-777-1986.");
        }

        public ActionResult EmpresaCobranca(int P, int F)
        {
            var faturas = faturamentoRepository.GetByExpression(r => r.ID == F && r.PedidoEmpresas.Where(p => p.ID == P).Any()).ToList();

            if(faturas.Count == 1)
            {
                if (faturas.First().Boleto != null)
                {
                    var html = BoletoService.Gerar(faturas.First().Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, faturas.First().ID + "_boleto.pdf");
                    return Content("");
                }
                else
                    return Content("Grupo Laços Flores<Br>Link inválido. Entre em contato com nossa central de atendimento no 0800-777-1986.");
            }
            else
                return Content("Grupo Laços Flores<Br>Link inválido, o pedido não existe ou foi cancelado. Entre em contato com nossa central de atendimento no 0800-777-1986.");
        }

        [HttpPost]
        public ActionResult RetornoPagarme(string guid)
        {
            var pedido = pedidoRepository.GetByCodigo(Guid.Parse(guid));
            if(pedido != null)
            {
                if(Request.Params["event"] == "transaction_status_changed")
                {
                    string transactionID = Request.Params["id"];
                    var context = new COROASEntities();
                    var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);

                    var Boleto = boletoRepository.GetByExpression(r => r.Pedido.ID == pedido.ID && r.TransactionID == transactionID).First();

                    if(Request.Params["current_status"] == "paid")
                    {
                        Boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Pago;
                        Boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;
                        Boleto.DataPagamento = DateTime.Now;

                        boletoRepository.Save(Boleto);

                        pedido.StatusPagamento = Pedido.TodosStatusPagamento.Pago;
                        pedido.ValorPago = Boleto.Valor;
                        pedido.DataPagamento = DateTime.Now;
                        pedido.DataProcessamento = DateTime.Now;

                        pedidoRepository.Save(pedido);

                        // CANCELA OUTROS BOLETOS EM ABERTO
                        var BoletosAberto = boletoRepository.GetByExpression(r => r.Pedido.ID == pedido.ID).ToList();
                        foreach(var iten in BoletosAberto)
                        {
                            if(iten.StatusPagamentoID == (int)BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento)
                            {
                                iten.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Cancelado;
                                iten.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;

                                boletoRepository.Save(iten);
                            }
                        }
                    }
                }

            }
            return Content("OK");
        }
    }
}
