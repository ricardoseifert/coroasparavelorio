﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
namespace Web.Controllers
{
    public class arranjos_de_condolenciasController : BaseController
    {

        private ProdutoRepository produtoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Coloracao> coloracaoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;

        public arranjos_de_condolenciasController(ObjectContext context)
            : base(context)
        {
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            coloracaoRepository = new PersistentRepository<Coloracao>(context);
            produtoRepository = new ProdutoRepository(context);

            depoimentoRepository = new PersistentRepository<Depoimento>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var destaques = produtoRepository.DestaquesHome();
            ViewBag.Depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado && c.Home).OrderByDescending(d=>d.Data).Take(4).ToList();

            var produtos = produtoRepository.GetAll().Where(r => r.ID == 234 || r.ID == 235 || r.ID == 236 || r.ID == 237).ToList();
            return View(produtos);
        }

        public PersistentRepository<Depoimento> depoimentoRepository { get; set; }

    }
}
