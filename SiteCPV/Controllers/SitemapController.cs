﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Web.Filter;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using Domain.Repositories;
using System.Text.RegularExpressions;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class SitemapController : BaseController
    {
        public LocalRepository localRepository;
        public SitemapController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
        }
         
        public ActionResult Index()
        {
            string XMLFisico = System.IO.File.ReadAllText(Server.MapPath("~/assets/SiteMap.txt"));

            string XML = "";

            XML += "<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\">";

            // CONCATENA XML FISICO
            XML += XMLFisico;

            #region LOAD LOCAIS

            var lstLocais = localRepository.GetAll();

            foreach (var local in lstLocais)
            {
                XML += "<url>";
                XML += "<loc>https://www.coroasparavelorio.com.br/coroas-de-flores/" + local.RotuloUrl.Trim() + "</loc>";
                XML += "<image:image><image:loc>https://www.coroasparavelorio.com.br/content/locais/" + local.ID + "/" + local.Foto.Trim() + "</image:loc><image:caption>" + Regex.Replace(local.Titulo, @"(@|&|'|\(|\)|<|>|#)", "") + "</image:caption></image:image>";
                XML += "</url>";
            }

            #endregion

            XML += "</urlset>";

            return this.Content(XML, "text/xml");
        }
    }
}
