﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Xml.Linq;
using System.IO;
using System.Text;

namespace Web.Controllers
{
    public class XMLController : Controller
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Flor> florRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Noticia> noticiaRepository;
        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<Duvida> duvidaRepository;

        public XMLController(ObjectContext context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            florRepository = new PersistentRepository<Flor>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            noticiaRepository = new PersistentRepository<Noticia>(context);
            localRepository = new PersistentRepository<Local>(context);
            duvidaRepository = new PersistentRepository<Duvida>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ContentResult Sitemap()
        {
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace xsischema = "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd";
            
            string url = Domain.Core.Configuracoes.DOMINIO;
            var coroas = produtoRepository.GetByExpression(n => n.Disponivel && n.TipoID == (int)Produto.Tipos.CoroaFlor).OrderByDescending(n => n.Nome); 
            var flores = florRepository.GetByExpression(n => n.Publicado).OrderByDescending(n => n.Nome);
            var depoimentoempresas = depoimentoRepository.GetByExpression(n => n.Aprovado && n.TipoID == (int)Depoimento.Tipos.PJ).OrderByDescending(n => n.Data);
            var depoimentopessoa = depoimentoRepository.GetByExpression(n => n.Aprovado && n.TipoID == (int)Depoimento.Tipos.PF).OrderByDescending(n => n.Data);
            var noticias = noticiaRepository.GetByExpression(n => n.Publicado && n.TipoID == (int)Noticia.Tipo.MERCADO).OrderByDescending(n => n.Data);
            var midias = noticiaRepository.GetByExpression(n => n.Publicado && n.TipoID == (int)Noticia.Tipo.MIDIA).OrderByDescending(n => n.Data);
            var locais = localRepository.GetByExpression(n => n.Disponivel).OrderByDescending(n => n.Titulo);

            var declaration = new XDeclaration("1.0", "utf-8", "");
            var elements = new XElement(ns + "urlset", new XAttribute(XNamespace.Xmlns + "xsi", xsi), new XAttribute(xsi + "schemaLocation", xsischema));

            //Raíz
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Contato
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "contato")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Contato
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("telefones-de-sua-regiao", "contato")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Tempo e Prazo
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "tempo-e-prazo-de-entrega")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //formas-de-pagamento
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "formas-de-pagamento")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Coroas de Flores
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "coroas-de-flores")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Cidades e regiões
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "cidades-e-regioes")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Como comprar
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "como-comprar")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Velórios e Cemitérios
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "velorios-e-cemiterios")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Frases
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "frases-de-homenagens")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Mais Vendiades
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "mais-vendidas")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //A empresa
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "a-empresa")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Midia
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "coroas-para-velorio-na-midia")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Midia
            foreach (var item in midias)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Midia", new { rotuloUrl = item.RotuloUrl, id = item.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }

            //Depoimentos de clientes
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "depoimentos-de-clientes")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Depoimentos de clientes PF
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("pessoas-fisicas", "depoimentos-de-clientes")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Depoimentos de clientes PJ
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("pessoas-juridicas", "depoimentos-de-clientes")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seja uma floricultura parceria
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seja-uma-floricultura-parceira")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seja um promotor
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seja-um-promotor")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Termos e condições
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "termos-e-condicoes-comerciais")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Politicas de Privacidade
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "politicas-de-privacidade")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Prazo de entrega
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "tempo-e-prazo-de-entrega")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Indique
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "indique-o-coroas-para-velorio")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));
            
            //Significado
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "significado-das-flores")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Significado
            foreach (var item in flores)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Flores", new { rotuloUrl = item.RotuloUrl, id = item.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }

            //Condolencias
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "condolencias-virtuais-gratis")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seguranca
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seguranca")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Soluções corporativas
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "solucoes-corporativas")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Duvidas
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "duvidas-frequentes")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));
            
            //Duvidas
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "cadastro", tipoID = 1 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "como-comprar", tipoID = 2 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "entrega", tipoID = 3 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "pagamento", tipoID = 4 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "meus-pedidos", tipoID = 5 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Duvidas", new { rotuloUrl = "demais-infos", tipoID = 6 })), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "1")));
                       
            //Coroas
            foreach (var produto in coroas)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Coroa", new { rotuloUrl = produto.RotuloUrl, id = produto.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));
            }
             
            //Noticias
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "noticias-de-mercado")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Noticias
            foreach (var item in noticias)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Noticias", new { rotuloUrl = item.RotuloUrl, id = item.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }

            //Locais
            foreach (var item in locais)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Local", new { rotuloUrl = item.RotuloUrl})), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }

            var sitemap = new XDocument(declaration, elements);


            var xml = new StringWriter();
            sitemap.Save(xml);
            var retorno = xml.GetStringBuilder();
            retorno = retorno.Replace("utf-16", "utf-8");

            return Content(retorno.ToString(), "text/xml", Encoding.UTF8);
        }

    }
}
