﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Data.Objects;
using Domain.Repositories;

namespace Web.Controllers
{
    public class GenericaController : BaseController
    {
        //
        // GET: /Generica/

        private ParametroRepository parametroRepository;

        public GenericaController(ObjectContext context)
            : base(context)
        {
            parametroRepository = new ParametroRepository(context);
            
        }


        public ActionResult ListaIp()
        {
            ViewBag.LstIps = parametroRepository.GetAll();
            return View("~/Views/Generica/Index.cshtml");
        }

    }
}
