﻿using System.Linq;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Repositories;
using System.Data.Objects;

namespace Web.Controllers
{
    //[[RequireHttps]
    public class A_EmpresaController : BaseController
    {
        private IPersistentRepository<Depoimento> depoimentoRepository;
        public ProdutoRepository produtoRepository;

        public A_EmpresaController(ObjectContext context) : base(context)
        {
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();
            ViewBag.DepoimentosPF = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PF).OrderByDescending(d => d.Data).Take(1).ToList();
            ViewBag.DepoimentosPJ = depoimentoRepository.GetByExpression(d => d.Aprovado && d.TipoID == (int)Depoimento.Tipos.PJ).OrderByDescending(d => d.Data).Take(1).ToList();
            return View();
        }

    }
}
