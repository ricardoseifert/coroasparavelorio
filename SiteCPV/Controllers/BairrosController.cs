﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class BairrosController : BaseController
    {

        private IPersistentRepository<Bairro> bairroRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Redirect> redirectRepository;
        public ProdutoRepository produtoRepository;

        public BairrosController(ObjectContext context)
            : base(context)
        {
            bairroRepository = new PersistentRepository<Bairro>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            redirectRepository = new PersistentRepository<Redirect>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var estados = estadoRepository.GetByExpression(c => c.Sigla != null).ToList();
            ViewBag.Produtos = produtoRepository.Publicados();
            return View(estados);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Detalhes(string rotuloUrl, string estado)
        {
            var local = bairroRepository.GetByExpression(c => c.RotuloUrl == rotuloUrl && c.Estado.Sigla == estado).FirstOrDefault();
            if (local != null)
            { 
                ViewBag.Produtos = produtoRepository.Publicados().Take(8).ToList(); 
                 
                return View(local);
            }
            else
            {
                var pagina = "/coroas-de-flores/" + rotuloUrl;
                var redirect = redirectRepository.GetByExpression(c => c.Origem.Contains(pagina)).FirstOrDefault();
                if (redirect == null)
                {

                    Response.StatusCode = 404;
                    Response.TrySkipIisCustomErrors = true;

                    var destaques = produtoRepository.DestaquesHome();
                    ViewBag.Destaques = destaques;
                    ViewBag.Pagina = pagina;

                    return RedirectToAction("Oops", "Error", new { url = pagina });
                }
                else
                {
                    return RedirectPermanent(redirect.Destino);
                }
            }
        }

        public JsonResult GetBairros(string estado)
        {
            var bairros = bairroRepository.GetByExpression(c => c.Estado.Sigla == estado).Distinct().ToList();
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var bairro in bairros.OrderBy(c => c.Nome))
            {
                result.Add(new SelectListItem { Text = bairro.Nome.ToUpper(), Value = bairro.ID.ToString() });
            }
            return Json(result);
        }

    }
}
