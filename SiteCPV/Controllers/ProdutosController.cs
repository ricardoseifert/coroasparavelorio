﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class ProdutosController : BaseController
    {
        ObjectContext PublicContext;
        public ProdutoRepository produtoRepository;

        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Noticia> noticiaRepository;

        public ProdutosController(ObjectContext context): base(context)
        {
            estadoRepository = new PersistentRepository<Estado>(context);
            produtoRepository = new ProdutoRepository(context);
            noticiaRepository = new PersistentRepository<Noticia>(context);

            PublicContext = context;
        }

        public ActionResult Produtos(List<Produto> produtos)
        {
            return View(produtos);
        }

        public ActionResult VejaTambem(List<Produto> produtos)
        {
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Detalhes(int id)
        {
            Produto produto = null;

            if (id == 234 || id == 235 || id == 236 || id == 237)
            {
                produto = produtoRepository.DetalhesArranjos(id);
            }
            else
            {
                produto = produtoRepository.Detalhes(id);
            }

            if (produto == null)
                return RedirectToAction("Index", "Home");

            ViewBag.ClienteModel = new Web.Models.ClienteModel(PublicContext);

            if (Request.QueryString["v"] == "2")
                return View("Detalhes_V2", produto);

            return View(produto);
        }

    }
}
