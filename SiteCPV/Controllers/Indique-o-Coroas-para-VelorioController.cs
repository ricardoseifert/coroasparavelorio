﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Indique_o_Coroas_para_VelorioController : BaseController
    {
        public ProdutoRepository produtoRepository;
        public Indique_o_Coroas_para_VelorioController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
        }
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();; 
            return View();
        } 

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var nomeDe = form["nomeDe"].ToUpper();
                var emailDe = form["emailDe"].ToLower();
                var nomePara = form["nomePara"].ToUpper();
                var emailPara = form["emailPara"].ToLower();
                if (emailDe.Length > 0 && nomeDe.Length > 0 && emailPara.Length > 0 && nomePara.Length > 0)
                {
                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/indique.htm"));
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOMEDE]", nomeDe);
                    html = html.Replace("[NOMEPARA]", nomePara);

                    if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", emailPara, nomePara, emailDe, nomeDe, "", "[COROAS PARA VELÓRIO] - " + nomeDe.ToUpper() + " sugeriu que você visitasse o nosso site", html, true))
                        return Json(new { status = "OK", mensagem = "Sua indicação foi enviada com sucesso!" });
                    else
                        return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua indicação, tente novamente mais tarde."});
                }
                else
                    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua indicação, tente novamente mais tarde." });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }

    }
}
