﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Web.Filter;
using Domain.Repositories;

namespace Web.Controllers
{
    //[[RequireHttps]
    public class AvaliacaoController : BaseController
    {
        private IPersistentRepository<Depoimento> depoimentoRepository;
        public ProdutoRepository produtoRepository;

        public AvaliacaoController(ObjectContext context)
            : base(context)
        {
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Index()
        {
            var Produtos = produtoRepository.Publicados().Where(p => p.ID == 22 || p.ID == 24 || p.ID == 1 || p.ID == 27).ToList();

            return View(Produtos);
        }

    }
}
