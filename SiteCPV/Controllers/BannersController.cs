﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;

namespace Web.Controllers
{
    public class BannersController : Controller
    {

        public BannerRepository bannerRepository;

        public BannersController(ObjectContext context)
        {
            bannerRepository = new BannerRepository(context);
        }

        public ActionResult Inferior()
        {
            var banners = bannerRepository.GetByPosicao(Domain.Entities.Banner.Posicoes.Inferior);
            return View(banners);
        }

        public ActionResult Superior()
        {
            var banners = bannerRepository.GetByPosicao(Domain.Entities.Banner.Posicoes.Superior);
            return View(banners);
        }

    }
}
