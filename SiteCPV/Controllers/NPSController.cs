﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;

namespace Web.Controllers
{
    public class NPSController : Controller
    {
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<NPSMensagem> npsMensagemRepository;
        private IPersistentRepository<NPSPergunta> NPSPerguntarepository;
        private IPersistentRepository<NPSRating_X_NPSPergunta> NPSRating_X_NPSPerguntarepository;

        private IPersistentRepository<NPSRating_NEW> NPSRating_NEWREspository;
        private IPersistentRepository<NPSMensagem_NEW> NPSMensagem_NEWRepository;
        private IPersistentRepository<NPSPergunta_NEW> NPSPergunta_NEWRepository;
        private IPersistentRepository<NPSRating_X_NPSPergunta_NEW> NPSRating_X_NPSPergunta_NEWRepository;


        public NPSController(ObjectContext context)
        {
            npsREspository = new PersistentRepository<NPSRating>(context);
            npsMensagemRepository = new PersistentRepository<NPSMensagem>(context);
            NPSPerguntarepository = new PersistentRepository<NPSPergunta>(context);
            NPSRating_X_NPSPerguntarepository = new PersistentRepository<NPSRating_X_NPSPergunta>(context);

            NPSRating_NEWREspository = new PersistentRepository<NPSRating_NEW>(context);
            NPSMensagem_NEWRepository = new PersistentRepository<NPSMensagem_NEW>(context);
            NPSPergunta_NEWRepository = new PersistentRepository<NPSPergunta_NEW>(context);
            NPSRating_X_NPSPergunta_NEWRepository = new PersistentRepository<NPSRating_X_NPSPergunta_NEW>(context);
        }

        [HttpPost]
        public ActionResult Salvar(FormCollection form)
        {
            Guid NewGuid = Guid.Parse(Request.Form["guid"]);

            var NPS = npsREspository.GetByExpression(p => p.Guid == NewGuid).FirstOrDefault();

            if (!string.IsNullOrEmpty(Request.Form["obs"]))
            {
                NPS.Obs = Request.Form["obs"].ToString().Trim();
                NPS.Obs = NPS.Obs.Replace(System.Environment.NewLine, "<br />");
            }

            npsREspository.Save(NPS);

            foreach (var Iten in Request.Form.Keys)
            {
                if (Iten.ToString().Contains("pergunta-"))
                {
                    int PerguntaId = int.Parse(Iten.ToString().Split('-')[1].ToString());

                    NPSRating_X_NPSPerguntarepository.Save(new NPSRating_X_NPSPergunta { ID_PerguntaNPS = PerguntaId, ID_RatingNPS = NPS.ID });
                }
            }

            ViewBag.Nota = Request.QueryString["Nota"].ToString();

            return View("~/Views/NPS/Index.cshtml");
        }

        [HttpPost]
        public ActionResult SalvarNew(FormCollection form)
        {
            Guid NewGuid = Guid.Parse(Request.Form["guid"]);

            var NPS = NPSRating_NEWREspository.GetByExpression(p => p.Guid == NewGuid).FirstOrDefault();

            if (!string.IsNullOrEmpty(Request.Form["obs"]))
            {
                NPS.Obs = Request.Form["obs"].ToString().Trim();
                NPS.Obs = NPS.Obs.Replace(System.Environment.NewLine, "<br />");
            }

            NPSRating_NEWREspository.Save(NPS);

            foreach (var Iten in Request.Form.Keys)
            {
                if (Iten.ToString().Contains("pergunta-"))
                {
                    int PerguntaId = int.Parse(Iten.ToString().Split('-')[1].ToString());

                    NPSRating_X_NPSPergunta_NEWRepository.Save(new NPSRating_X_NPSPergunta_NEW { ID_PerguntaNPS = PerguntaId, ID_RatingNPS = NPS.ID });
                }
            }

            ViewBag.Nota = Request.QueryString["Nota"].ToString();

            return View("~/Views/NPS/Index.cshtml");
        }

        [HttpGet]
        public ActionResult AtualizaNPS(string Identi, string Nota)
        {
            var NPS = npsREspository.GetByExpression(p => p.Guid == new Guid(Identi)).FirstOrDefault();
            int NotaRequest = int.Parse(Nota);

            var NPSMensagem = npsMensagemRepository.GetByExpression(p => p.Nota == NotaRequest).FirstOrDefault();

            NPS.Nota = int.Parse(Nota);
            NPS.DataEnvioAvaliacao = DateTime.Now;

            ViewBag.NPSMEnsagem = NPSMensagem.MensagemNPS;
            ViewBag.NPSPerguntas =  NPSMensagem.NPSPerguntas;
            
            npsREspository.Save(NPS);

            return View("~/Views/NPS/AtualizaNPS.cshtml");
        }

        [HttpGet]
        public ActionResult AtualizaNPSNew(string Identi, string Nota)
        {
            var NPS = NPSRating_NEWREspository.GetByExpression(p => p.Guid == new Guid(Identi)).FirstOrDefault();
            int NotaRequest = int.Parse(Nota);

            var NPSMensagem = NPSMensagem_NEWRepository.GetByExpression(p => p.Nota == NotaRequest).FirstOrDefault();

            NPS.Nota = int.Parse(Nota);
            NPS.DataEnvioAvaliacao = DateTime.Now;

            ViewBag.NPSMEnsagem = NPSMensagem.MensagemNPS;
            ViewBag.NPSPerguntas = NPSMensagem.NPSPergunta_NEW;

            NPSRating_NEWREspository.Save(NPS);

            return View("~/Views/NPS/AtualizaNPSNew.cshtml");
        }
    }
}
