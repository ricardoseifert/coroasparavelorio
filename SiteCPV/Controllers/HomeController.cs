﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Filter;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Data.Entity.Core.EntityClient;

namespace Web.Controllers
{ 
    public class HomeController : BaseController
    {
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Coloracao> corRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<PrimeiraCompra> primeiraCompraRepository;
        private IPersistentRepository<Local> localRepository;
        
        public HomeController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            corRepository = new PersistentRepository<Coloracao>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            localRepository = new PersistentRepository<Local>(context);
            primeiraCompraRepository = new PersistentRepository<PrimeiraCompra>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index(){
            var destaques = produtoRepository.GetAll().Where(r => r.ID == 24 || r.ID == 1 || r.ID == 26 || r.ID == 22 || r.ID == 18 || r.ID == 23 || r.ID == 20 || r.ID == 21 || r.ID == 25 || r.ID == 27 || r.ID == 28 || r.ID == 29 || r.ID == 30 || r.ID == 32 || r.ID == 33 || r.ID == 34 || r.ID == 235 || r.ID == 234 || r.ID == 237 || r.ID == 236 || r.ID == 472 || r.ID == 467 || r.ID == 473 || r.ID == 464).ToList();
            return View(destaques);
        }

        public ActionResult Sugestoes()
        {
            ViewBag.FaixaPreco = faixaPrecoRepository.GetAll().ToList();
            ViewBag.Tamanho = tamanhoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Relacionamento = relacionamentoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Coloracao = corRepository.GetAll().OrderBy(t => t.Nome).ToList();
            return View();
        }

        public ActionResult Depoimentos()
        
            {
            var depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado).OrderByDescending(d => d.Data).ToList();
            return View(depoimentos);
        }

        public ActionResult ListaDepoimentos(int pagina)
        {
            var depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado).OrderByDescending(d => d.Data).ToList();
            var DepoimentosCoroas = depoimentos.ToList().Where(r => r.ProdutoID.HasValue).ToList().Take(10);
            var DepoimentosOutros = depoimentos.Where(r=>!r.ProdutoID.HasValue).ToList();

            List<Depoimento> DepoREsult = new List<Depoimento>();
            DepoREsult.AddRange(DepoimentosCoroas);
            DepoREsult.AddRange(DepoimentosOutros);

            int totalItens = (int)DepoREsult.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 1);
            if (totalPaginas > 1)
            {
                DepoREsult = DepoREsult.Skip((pagina - 1) * 1).Take(1).ToList();
            }
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.Pagina = pagina;
            return View(DepoREsult.FirstOrDefault());
        }

        public ActionResult Mais_Vendidas()
        {
            var produtos = produtoRepository.GetByExpression(c => c.Disponivel && c.MaisVendida > 0 && c.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.MaisVendida).Take(3).ToList();
            return View(produtos);
        }

        //----------------------------------------------------------------------
        [HttpGet]
        public JsonResult GetLocaisByTermo(string Termo)
        {
            var ProdutosFaturamento = localRepository.GetByExpression(c => c.Titulo.ToLower().Contains(Termo.ToLower())).OrderBy(r => r.Titulo).ToList().Select(p => new LocaisList { IdLocal = p.ID, Titulo = p.Tipo.ToString().ToUpper() + " - " + p.Titulo.ToUpper(), Url = p.RotuloUrl }).Take(15).ToList();
            return Json(ProdutosFaturamento, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCidades(string uf, string selected = "")
        {
            var cidades = cidadeRepository.GetByExpression(c => c.Estado.Sigla.ToUpper().Trim() == uf.ToUpper().Trim()).OrderBy(c => c.Nome);

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Cidade cidade in cidades)
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetClientePorEmail(string Email)
        {
            var Cliente = clienteRepository.GetByExpression(c => c.Email.ToLower() == Email.ToLower()).ToList();

            if(Cliente.Count > 0)
            {
                ClienteRetorno novoRetorno = new ClienteRetorno();
                novoRetorno.Documento = Cliente.First().Documento;
                novoRetorno.Telefone = Cliente.First().TelefoneContato;
                novoRetorno.Tipo = Cliente.First().TipoID;
                novoRetorno.Nome = Cliente.First().Nome;

                if(novoRetorno.Documento.Length > 4)
                {
                    string asterisco = "";
                    int QtdCaracteres = novoRetorno.Documento.Length - 4;

                    for (int i = 0; i < QtdCaracteres; i++)
                    {
                        asterisco += "*";
                    }
                    novoRetorno.Documento = novoRetorno.Documento.Substring(0, 3) + asterisco;
                }

                if (novoRetorno.Telefone.Length > 10)
                {
                    string asterisco = "";
                    int QtdCaracteres = novoRetorno.Telefone.Length - 10;

                    for (int i = 0; i < QtdCaracteres; i++)
                    {
                        asterisco += "*";
                    }
                    novoRetorno.Telefone = novoRetorno.Telefone.Substring(0, 9) + asterisco;
                }

                return Json(novoRetorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetCidadesByName(string NomeCidade)
        {
            List<CidadeList> Cidades = new List<CidadeList>();

            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                string sql = "SELECT TOP 25 C.ID, C.Nome + ' / ' + E.Sigla AS Nome From Local.Cidade C INNER JOIN Local.Estado E ON C.EstadoID = E.ID WHERE C.Nome like '%" + NomeCidade + "%' ORDER BY C.ORDEM DESC";
                Cidades = con.Query<CidadeList>(sql).ToList();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return Json(Cidades, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLocais(int idCidade)
        {
            var locais = localRepository.GetByExpression(c => c.CidadeID == idCidade).OrderBy(c => c.Titulo).ToList().Select(p => new LocaisList { IdLocal = p.ID, Titulo = p.Titulo  }).ToList();
            
            return Json(locais, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetLocaisCidadeENome(int idCidade, string Termo)
        {
            List<LocaisList> ListaLocal = new List<LocaisList>();

            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                string sql = "SELECT TOP 25 ID AS IdLocal, Titulo collate SQL_Latin1_General_Cp1251_CS_AS AS Titulo FROM Local.Local  WHERE CidadeID=" + idCidade + " AND UPPER(Titulo) collate SQL_Latin1_General_Cp1251_CS_AS LIKE '%" + Termo.ToUpper() + "%' collate SQL_Latin1_General_Cp1251_CS_AS ORDER BY Titulo ASC";
                ListaLocal = con.Query<LocaisList>(sql).ToList();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return Json(ListaLocal, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PrimeiraCompra()
        { 
            return View();
        }

        public ActionResult PrimeiraCompraForm()
        {
            return View();
        }

        public ActionResult AtendimentoIndisponivel()
        {
            return View();
        }
        //TODO: MUDAR EMAIL ORIGEM
        [HttpPost]
        public JsonResult PrimeiraCompra(string nome, string telefone, string email, string primeira)
        {
            DateTime data = new DateTime(1900,1,1);
            DateTime.TryParse(Domain.Core.Criptografia.Decrypt(primeira), out data);
            if (DateTime.Now <= data)
            {
                var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
                if (nome.Length > 0 && telefone.Length > 0 && email.Length > 0)
                {
                    email = email.Trim().ToLower();
                    nome = nome.ToUpper();
                    var cliente = clienteRepository.GetByExpression(c => c.Email.ToLower().Trim() == email).FirstOrDefault();

                    bool elegivel = true;
                    if (cliente != null)
                        if (cliente.Pedidoes.Count > 0)
                            elegivel = false;

                    if (elegivel)
                    {
                        var primeiracompra = new PrimeiraCompra();
                        if (cliente != null)
                            primeiracompra.ClienteID = cliente.ID;
                        primeiracompra.CupomUtilizado = configuracao.CupomPrimeiraCompra;
                        primeiracompra.Email = email;
                        primeiracompra.Nome = nome;
                        primeiracompra.TelefoneContato = telefone;
                        primeiracompra.DataCadastro = DateTime.Now;
                        primeiraCompraRepository.Save(primeiracompra);

                        //ENVIAR EMAIL
                        var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/cpv/cupom.htm"));
                        html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                        html = html.Replace("[NOME]", nome.ToUpper());
                        html = html.Replace("[TELEFONE]", telefone);
                        html = html.Replace("[EMAIL]", email.ToLower());
                        html = html.Replace("[CUPOM]", configuracao.CupomPrimeiraCompra);

                        Response.Cookies["primeiracompra"].Value = "nao";
                        Response.Cookies["primeiracompra"].Expires = DateTime.Now.AddDays(30);

                        //if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Coroas para Velório", email, nome, Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_COPIA, "[COROAS PARA VELÓRIO] - Seu cupom de desconto para sua primeira compra!", html, true))
                        //    return Json(new { status = "OK", mensagem = "Seu cupom foi enviado com sucesso para seu e-mail. Verifique sua caixa de entrada para utilização." });
                        //else
                        //    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar seu cupom. Anote o código para utilização na sua primeira compra : " + configuracao.CupomPrimeiraCompra });
                        return Json(new { status = "ERRO", mensagem = "Não foi possível enviar seu cupom. Anote o código para utilização na sua primeira compra" });
                    }
                    else
                    {
                        return Json(new { status = "ERRO", mensagem = "Esta não é a primeira compra para este e-mail. Esta promoção é válida somente para novos clientes." });
                    }
                }
                else
                    return Json(new { status = "ERRO", mensagem = "Digite todos os seus dados." });
            }
            else
            {
                return Json(new { status = "ERRO", mensagem = "Não foi possível o cadastro, tente novamente mais tarde." });
            }
        }

        [HttpPost]
        public JsonResult FechaPrimeiraCompra()
        {
            Response.Cookies["primeiracompra"].Value = "nao";
            Response.Cookies["primeiracompra"].Expires = DateTime.Now.AddDays(5);
            return Json("OK");
        }

        [HttpPost]
        public JsonResult GetCEP(string cep)
        {
            var resultado = new CEP.CEP(cep);
            return Json(new {
                resultado = resultado.Resultado,
                resultado_txt = resultado.ResultadoTXT,
                uf = resultado.UF,
                cidade = resultado.Cidade,
                bairro = resultado.Bairro,
                tipo_logradouro = resultado.TipoLogradouro,
                logradouro = resultado.Logradouro
            });
        }

        public ContentResult EnviaTrustVox()
        {
            var context = new COROASEntities();
            var pedidosRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            var logRepository = new PersistentRepository<Domain.Entities.Log>(context);

            var data = DateTime.Now.AddDays(-14);
            var pedidos = pedidosRepository.GetByExpression(c => c.DataEnvioPesquisa == null && c.StatusEntregaID == (int)TodosStatusEntrega.Entregue && c.DataCriacao >= data).ToList();
            Response.Buffer = false;
            Response.Write(pedidos.Count.ToString() + "<br>");
            foreach (var pedido in pedidos)
            {
                Domain.Core.TrustVox.TrustVox.EnviaTrustVox(pedido);
                Response.Write(pedido.Numero + "<br>");

                var atualiza = logRepository.GetByExpression(c => c.Pedido == pedido.Numero).Count() > 0;
                if (atualiza)
                {
                    pedido.DataEnvioPesquisa = DateTime.Now;
                    pedidosRepository.Save(pedido);
                }
            }
             
            return Content("");
        }  
    }
    public class LocaisList
    {
        public int IdLocal { get; set; }
        public string Titulo { get; set; }
        public string Url { get; set; }
    }

    public class CidadeList
    {
        public int ID { get; set; }
        public string Nome { get; set; }
    }

    public class ClienteRetorno
    {
        public int ID { get; set; }
        public string Documento { get; set; }
        public int Tipo { get; set; }
        public string Telefone { get; set; }
        public string Nome { get; set; }
    }


}
