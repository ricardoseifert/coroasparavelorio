﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Web.Models;
using System.Data.Objects;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Repositories;
using PagarMe;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace Web.Controllers
{
    public class PedidoController : BaseController
    {
        ObjectContext Globalcontext;

        private ClienteModel clienteModel;
        private PedidoService pedidoService;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private AvisoService avisoService;
        private PedidoRepository pedidoRepository;
        private RelatorioRepository relatorioRepository;
        private LocalRepository localRepository;
        private PersistentRepository<Log> logRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<PedidoNota> pedidoNotaRepository;

        public PedidoController(ObjectContext context) : base(context)
        {
            Globalcontext = context;
            clienteModel = new ClienteModel(context);
            pedidoService = new PedidoService(context);
            avisoService = new AvisoService();
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            relatorioRepository = new RelatorioRepository(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            pedidoRepository = new PedidoRepository(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            localRepository = new LocalRepository(context);
            pedidoNotaRepository = new PersistentRepository<PedidoNota>(context);
        }


        public ActionResult CompraClick(Carrinho carrinho)
        {
            if (carrinho.Vazio)
            {
                return RedirectToAction("index", "home");
            }

            var estados = estadoRepository.GetAll().ToList();
            int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
            List<Estado> lstEstados = new List<Estado>();
            foreach (int idEstado in principaisEstados)
            {
                lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
            }
            lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

            ViewBag.Estados = new SelectList(lstEstados, "ID", "Nome");

            if (clienteModel.Autenticado && clienteModel.CurrentCliente.DataAtualizacao.HasValue)
                ViewBag.Cliente = clienteModel.CurrentCliente;

            return View(carrinho);
        }

        public ActionResult Index(Carrinho carrinho)
        {
            if (carrinho.Vazio)
            {
                return RedirectToAction("index", "home");
            }
            else
            {
                logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());
                string StrCarrinho = string.Empty;

                StrCarrinho = "IDCliente = " + clienteModel.CurrentCliente.ID + ", Email = " + clienteModel.CurrentCliente.Email;

                foreach (var Iten in carrinho.Itens)
                {
                    StrCarrinho += " | ProdutoID = " + Iten.ProdutoTamanho.Tamanho.ID + ", ItemID = " + Iten.ProdutoTamanho.TamanhoID + ", Obs = " + Iten.Observacoes;
                }

                logRepository.Save(new Log { Data = DateTime.Now, Envio = "", Pedido = "", Retorno = "", Var1 = "Carrinho", Var2 = StrCarrinho });
            }
            
            if (clienteModel.Autenticado)
            {
                ViewBag.Homolog = (Domain.Core.Configuracoes.HOMOLOGACAO && clienteModel.CurrentCliente.Email == Domain.Core.Configuracoes.EMAIL_HOMOLOGACAO);

                if (clienteModel.CurrentCliente.DataAtualizacao.HasValue)
                {
                    ViewBag.Cliente = clienteModel.CurrentCliente;

                    var estados = estadoRepository.GetAll().ToList();
                    int[] principaisEstados = new[] { 26, 11, 19, 18, 7, 23, 9, 16 };
                    List<Estado> lstEstados = new List<Estado>();
                    foreach (int idEstado in principaisEstados)
                    {
                        lstEstados.Add(estados.FirstOrDefault(e => e.ID == idEstado));
                    }
                    lstEstados.AddRange(estados.Where(e => !principaisEstados.Contains(e.ID)));

                    ViewBag.Estados = new SelectList(lstEstados, "ID", "Nome");

                    // ViewBag.Estados = estadoRepository.GetAll().OrderByDescending(r => r.Ordem).ThenBy(e => e.Nome);

                    if (carrinho.Vazio)
                    {
                        return RedirectToAction("index", "home");
                    }
                    return View(carrinho);
                }
                else
                {
                    return RedirectToAction("minha-conta", "cliente", new { returnUrl = "/pedido" , atualizar = "sim"});
                }
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/pedido" });
            }

        }
     
        public JsonResult Criar(Carrinho carrinho, int cidadeID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada, int? LocalID)
        {
            var LocalDeEntrega = localRepository.GetByExpression(r => r.CidadeID == cidadeID && r.EstadoID == estadoID && r.Titulo.ToLower() == localEntrega.ToLower()).ToList();

            var localEntregaId = 0;

            var _localEntregaComplemento = "";

            var _localEntrega = "";

            if (LocalDeEntrega.Count==1)
            {
                localEntregaId = LocalDeEntrega.FirstOrDefault().ID;
                _localEntregaComplemento = "";
                _localEntrega = LocalDeEntrega.FirstOrDefault().Titulo.ToUpper().Trim();
            }
            else
            {
                localEntregaId = 0;
                _localEntregaComplemento = localEntrega.ToUpper().Trim();
                _localEntrega = "LOCAL NÃO CADASTRADO";
            }

            try
            {
                // BOLETO PAGARME
                var pedido = pedidoService.CriarPedidoPagarMe(carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, "", pessoaHomenageada, clienteModel.CurrentCliente.TelefoneContato, "SITE", "COROAS PARA VELÓRIO", "ONLINE");
                if (LocalID != null)
                {
                    if (LocalID > 0)
                    {
                        pedido.LocalID = LocalID;
                        pedidoRepository.Save(pedido);
                    }
                }
                else
                {
                    if (LocalDeEntrega.Count == 1)
                    {
                        pedido.LocalID = LocalDeEntrega.FirstOrDefault().ID;
                        pedidoRepository.Save(pedido);
                    }
                }

                if (pedido != null)
                {
                    pedido.LocalEntrega = _localEntrega;
                    pedido.ComplementoLocalEntrega = _localEntregaComplemento;

                    pedidoRepository.Save(pedido);

                    var model = new PedidoModel(pedido);

                    pedido.EnviarPedidoEmailNovo();

                    #region YOURVIEWS

                    var StrYourView = "Erro ao enviar YourReviews: ";

                    try
                    {
                        var ResultYurViews = pedido.EnviarYourViews();

                        if (ResultYurViews.HasErrors)
                        {
                            foreach (var Erro in ResultYurViews.ErrorList)
                            {
                                StrYourView += Erro.Error + " / " + Erro.Field + " || ";
                            }
                        }
                        else
                        {
                            StrYourView = "YourViews enviado com sucesso";
                        }

                    }
                    catch (Exception ex)
                    {
                        StrYourView += ex.Message;
                    }

                    var notaYourViews = new PedidoNota();

                    notaYourViews.DataCriacao = DateTime.Now;
                    notaYourViews.PedidoID = pedido.ID;

                    notaYourViews.Observacoes = StrYourView;
                    pedidoNotaRepository.Save(notaYourViews);

                    #endregion

                    if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                    {
                        pedido.EnviarPedidoEmailBoleto();
                    }

                    carrinho.Limpar();
                    return Json(model);
                }
                else
                {
                    throw new Exception("Não foi possível criar seu pedido. Verifique os dados preenchidos.");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public Cliente CriarCliente(string EmailCliente = "", string NomeCompleto = "", string Telefone = "", string Documento = "")
        {
            Cliente NovoCliente = new Cliente();

            var ClienteRetorno = clienteRepository.GetByExpression(r => r.Email.ToLower() == EmailCliente.ToLower()).ToList();

            if (ClienteRetorno.Count > 1)
            {
                NovoCliente.ErroCriacao = "Não foi possível localizar o seu cadastro. O seu cadastro está duplicado.";
            }
            else if (ClienteRetorno.Count == 1)
            {
                NovoCliente = ClienteRetorno.First();
            }
            else
            {
                NovoCliente.Email = EmailCliente.ToLower();
                NovoCliente.Nome = NomeCompleto.ToUpper();
                NovoCliente.TelefoneContato = Telefone.Replace("-", "");
                NovoCliente.Documento = Documento.Replace(".", "").Replace("-", "").Replace("/", "");

                if (NovoCliente.Documento.Length == 14)
                {
                    NovoCliente.TipoID = (int)Domain.Entities.Cliente.Tipos.PessoaJuridica;
                    NovoCliente.RazaoSocial = "NÃO CADASTRADO";
                    NovoCliente.InscricaoEstadual = "";
                }
                else if (NovoCliente.Documento.Length == 11)
                {
                    NovoCliente.TipoID = (int)Domain.Entities.Cliente.Tipos.PessoaFisica;
                    NovoCliente.RazaoSocial = "";
                    NovoCliente.InscricaoEstadual = "";
                }
                else
                {
                    NovoCliente.ErroCriacao = "O tamanho do CPF ou CNPJ é Inválido";
                }

                if (NovoCliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica)
                {
                    if (!Domain.Core.Funcoes.Valida_CPF(NovoCliente.Documento))
                        NovoCliente.ErroCriacao = "CPF Inválido";
                }
                else
                {
                    if (!Domain.Core.Funcoes.Valida_CNPJ(NovoCliente.Documento))
                        NovoCliente.ErroCriacao = "CNPJ Inválido";
                }

                NovoCliente.Senha = "123";

                var Cidade = cidadeRepository.Get(8570);
                NovoCliente.CidadeID = Cidade.ID;
                NovoCliente.EstadoID = Cidade.Estado.ID;
                NovoCliente.Exterior = false;
                NovoCliente.DataCadastro = DateTime.Now;
                NovoCliente.LacosStatusProspecaoID = 1;
                NovoCliente.ProblemaEmail = false;
                NovoCliente.TipoIE = 1;

                clienteRepository.Save(NovoCliente);
            }

            return NovoCliente;
        }

        public JsonResult PagarNovo(Carrinho carrinho, int parcela, int cidadeID, string dataSolicitada, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string pessoaHomenageada, string cardHash, string observacoes, string TipoCheckout = "", string EmailCliente = "", string NomeCompleto = "", string Documento = "", string Telefone = "", string Frases = "", int LocalID = 0)
        {
            try
            {
                Pedido PedidoRetorno = new Pedido();
               
                #region PREENCHER CLIENTE

                Cliente novoCLiente = new Cliente();

                if (TipoCheckout == Pedido.TipoCheckout.UmClick.ToString())
                {
                    novoCLiente = CriarCliente(EmailCliente, NomeCompleto, Telefone, Documento);
                }
                else
                {
                    novoCLiente = clienteModel.CurrentCliente;
                }

                if (!string.IsNullOrEmpty(novoCLiente.ErroCriacao))
                    return Json(PedidoRetorno.ErroCraicao);

                #endregion

                #region PREENCHE FRASES DE HOMENAGEM

                if (TipoCheckout == Pedido.TipoCheckout.UmClick.ToString())
                {
                    List<ListaFrases> lstFrases = new List<ListaFrases>();

                    foreach (var FraseLinha in Frases.Split('|'))
                    {
                        ListaFrases FraseRetorno = new ListaFrases();

                        FraseRetorno.Frase = FraseLinha.Split(',')[0];
                        FraseRetorno.ProdutoID = int.Parse(FraseLinha.Split(',')[1]);
                        FraseRetorno.TamanhoID = int.Parse(FraseLinha.Split(',')[2]);
                        FraseRetorno.Usado = false;

                        lstFrases.Add(FraseRetorno);
                    }

                    foreach (var iten in carrinho.Itens)
                    {
                        var listReturn = lstFrases.Where(r => r.ProdutoID == iten.ProdutoTamanho.ProdutoID && r.TamanhoID == iten.ProdutoTamanho.TamanhoID).ToList();

                        var FraseRetorno = listReturn.Where(r => r.Usado == false).First();
                        FraseRetorno.Usado = true;
                        iten.Mensagem = FraseRetorno.Frase;
                    }
                }

                #endregion

                #region CRIAR PEDIDO 

                var Cidade = cidadeRepository.Get(cidadeID);

                PedidoRetorno = pedidoService.CriarPedido(carrinho,
                    Cidade.ID,
                    novoCLiente.ID,
                    dataSolicitada,
                    Cidade.EstadoID,
                    formaPagamento,
                    localEntrega,
                    meioPagamento,
                    observacoes,
                    "",
                    pessoaHomenageada,
                    "",
                    "SITE",
                    "COROAS PARA VELÓRIO",
                    "ONLINE",
                    LocalID
                );

                if (!string.IsNullOrEmpty(PedidoRetorno.ErroCraicao))
                {
                    return Json(PedidoRetorno.ErroCraicao);
                }

                #endregion

                #region PAGAR PEDIDO

                PedidoRetorno.cardhash = cardHash;
                pedidoService.PagarPedidoNovo(PedidoRetorno);

                if (!string.IsNullOrEmpty(PedidoRetorno.ErroPagamento))
                    return Json(PedidoRetorno.ErroPagamento);

                #endregion

                var model = new PedidoModel(PedidoRetorno);

                if (PedidoRetorno.FormaPagamento == Pedido.FormasPagamento.CartaoCredito)
                {
                    if (PedidoRetorno.Retorno != "Paid")
                    {
                        return Json("Não foi possível criar seu pedido. Motivo: " + PedidoRetorno.Retorno);
                    }
                }

                if (PedidoRetorno.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                {

                    PedidoRetorno.EnviarPedidoEmailBoleto();

                }

                PedidoRetorno.EnviarPedidoEmailNovo();

                return Json(model);
            }
            catch (Exception ex)
            {
                var Inner = "";

                if(ex.InnerException != null)
                {
                    Inner = ex.InnerException.Message;
                }

                var Erros = ex.Message + " " + Inner;
                return Json(Erros);
            }
        }

        public JsonResult Pagar(Carrinho carrinho, int bandeira, string nome, string numero, string validade, string codigo, int parcela, int cidadeID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada, string cardHash, string TipoCheckout = "", string EmailCliente = "", string NomeCompleto = "", string Documento = "", string Telefone = "", string Frases = "")
        {
            try
            {
                #region CRIAR PEDIDO

                var data = validade.Split('/');
                var mes = 0;
                var ano = 0;

                Pedido PedidoRetorno = new Pedido();

                if (TipoCheckout == Pedido.TipoCheckout.UmClick.ToString())
                {
                    var Cliente = clienteRepository.GetByExpression(r => r.Email.ToLower() == EmailCliente.ToLower()).ToList();
                    var ClienteID = 0;

                    if (Cliente.Count > 1)
                    {
                        return Json("Não foi possível localizar o seu cadastro. O seu cadastro está duplicado.");
                    }
                    else if (Cliente.Count == 1)
                    {
                        ClienteID = Cliente.First().ID;
                    }
                    else
                    {
                        Cliente novoCliente = new Cliente();
                        novoCliente.Email = EmailCliente.ToLower();
                        novoCliente.Nome = NomeCompleto.ToUpper();
                        novoCliente.TelefoneContato = Telefone.Replace("-", "");
                        novoCliente.Documento = Documento.Replace(".", "").Replace("-", "").Replace("/", "");

                        if (novoCliente.Documento.Length == 14)
                        {
                            novoCliente.TipoID = (int)Domain.Entities.Cliente.Tipos.PessoaJuridica;
                            novoCliente.RazaoSocial = "NÃO CADASTRADO";
                            novoCliente.InscricaoEstadual = "";
                        }
                        else if (novoCliente.Documento.Length == 11)
                        {
                            novoCliente.TipoID = (int)Domain.Entities.Cliente.Tipos.PessoaFisica;
                            novoCliente.RazaoSocial = "";
                            novoCliente.InscricaoEstadual = "";
                        }
                        else
                        {
                            return Json("O tamanho do CPF ou CNPJ é Inválido");
                        }

                        if (novoCliente.TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaFisica)
                        {
                            if (!Domain.Core.Funcoes.Valida_CPF(novoCliente.Documento))
                                return Json("CPF Inválido");
                        }
                        else
                        {
                            if (!Domain.Core.Funcoes.Valida_CNPJ(novoCliente.Documento))
                                return Json("CNPJ Inválido");
                        }

                        novoCliente.Senha = "123";

                        var Cidade = cidadeRepository.Get(8570);
                        novoCliente.CidadeID = Cidade.ID;
                        novoCliente.EstadoID = Cidade.Estado.ID;
                        novoCliente.Exterior = false;
                        novoCliente.DataCadastro = DateTime.Now;
                        novoCliente.LacosStatusProspecaoID = 1;
                        novoCliente.ProblemaEmail = false;
                        novoCliente.TipoIE = 1;

                        clienteRepository.Save(novoCliente);

                        ClienteID = novoCliente.ID;

                    }

                    List<ListaFrases> lstFrases = new List<ListaFrases>();

                    foreach (var FraseLinha in Frases.Split('|'))
                    {
                        ListaFrases FraseRetorno = new ListaFrases();

                        FraseRetorno.Frase = FraseLinha.Split(',')[0];
                        FraseRetorno.ProdutoID = int.Parse(FraseLinha.Split(',')[1]);
                        FraseRetorno.TamanhoID = int.Parse(FraseLinha.Split(',')[2]);
                        FraseRetorno.Usado = false;

                        lstFrases.Add(FraseRetorno);
                    }

                    foreach (var iten in carrinho.Itens)
                    {
                        var listReturn = lstFrases.Where(r => r.ProdutoID == iten.ProdutoTamanho.ProdutoID && r.TamanhoID == iten.ProdutoTamanho.TamanhoID).ToList();

                        var FraseRetorno = listReturn.Where(r => r.Usado == false).First();
                        FraseRetorno.Usado = true;
                        iten.Mensagem = FraseRetorno.Frase;
                    }



                    PedidoRetorno = pedidoService.PagarPedido(bandeira, nome, numero, mes, ano, codigo, parcela, carrinho, cidadeID, ClienteID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, cardHash, observacoes, "", pessoaHomenageada, "", "SITE", "COROAS PARA VELÓRIO", "ONLINE");
                }
                else
                {
                    PedidoRetorno = pedidoService.PagarPedido(bandeira, nome, numero, mes, ano, codigo, parcela, carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, cardHash, observacoes, "", pessoaHomenageada, "", "SITE", "COROAS PARA VELÓRIO", "ONLINE");
                }

                if (PedidoRetorno != null && PedidoRetorno.Retorno == "Paid")
                {
                    var model = new PedidoModel(PedidoRetorno);
                    PedidoRetorno.EnviarPedidoEmailNovo();

                    #region ALOCAR PEDIDO

                    int? ProxAlocadoID = 0;
                    var notaAlocar = new PedidoNota();
                    notaAlocar.PedidoID = PedidoRetorno.ID;

                    try
                    {
                        ProxAlocadoID = relatorioRepository.spRetornaProxAlocado();

                    }
                    catch
                    {
                        notaAlocar.Observacoes = "Erro ao tentar alocar o pedido.";
                        notaAlocar.DataCriacao = DateTime.Now;
                    }

                    if (ProxAlocadoID > 0)
                    {                        
                        PedidoRetorno.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;

                        notaAlocar.Observacoes = "Pedido alocado automaticamente para o usuario ID: " + ProxAlocadoID;
                        notaAlocar.DataCriacao = DateTime.Now;
                    }
                    else
                    {
                        notaAlocar.Observacoes = "Pedido nao alocado automaticamente. Nao havia usuario dusponível para poder alocar o pedido";
                        notaAlocar.DataCriacao = DateTime.Now;
                    }

                    pedidoNotaRepository.Save(notaAlocar);

                    #endregion

                    #region YOURVIEWS

                    var StrYourView = "Erro ao enviar YourReviews: ";

                    try
                    {
                        var ResultYurViews = PedidoRetorno.EnviarYourViews();

                        if (ResultYurViews.HasErrors)
                        {
                            foreach (var Erro in ResultYurViews.ErrorList)
                            {
                                StrYourView += Erro.Error + " / " + Erro.Field + " || ";
                            }
                        }
                        else
                        {
                            StrYourView = "YourViews enviado com sucesso";
                        }

                    }
                    catch (Exception ex)
                    {
                        StrYourView += ex.Message;
                    }

                    var notaYourViews = new PedidoNota();

                    notaYourViews.DataCriacao = DateTime.Now;
                    notaYourViews.PedidoID = PedidoRetorno.ID;

                    notaYourViews.Observacoes = StrYourView;
                    pedidoNotaRepository.Save(notaYourViews);

                    #endregion

                    return Json(model);
                }
                else
                {
                    if(PedidoRetorno != null)
                    {
                        if(PedidoRetorno.Retorno == "Refused")
                        {
                            return Json("Não foi possível criar seu pedido. Cartão recusado.");
                        }
                        else
                        {
                            return Json("Não foi possível criar seu pedido." + PedidoRetorno.Retorno);
                        }
                    }
                    else
                    {
                        return Json("Não foi possível criar seu pedido.");
                    }
                }

                #endregion                
            }
            catch (PagarMeException e)
            {
                var Erro = e.Error;
                var erros = Erro.Errors;
                return Json(erros[0].Message);
            }

        }
        
        public ActionResult Confirmacao(Guid? codigo, Carrinho carrinho)
        {
            if (codigo.HasValue)
            {
                var pedido = pedidoRepository.GetByCodigo(codigo.Value);
                if (pedido != null)
                {
                    var Result = new Domain.Service.PedidoService().SendMYMEtrics(pedido, "VENDER");
                    ViewBag.ResultMyMetrics = Result;

                    return View(pedido);
                }
                carrinho.Limpar();
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Caminho(int posicao = 0)
        {
            ViewBag.Posicao = posicao;
            return View();
        }

    }

    public class ListaFrases
    {
        public string Frase { get; set; }
        public int ProdutoID { get; set; }
        public int TamanhoID { get; set; }
        public bool Usado { get; set; }
    }
}
