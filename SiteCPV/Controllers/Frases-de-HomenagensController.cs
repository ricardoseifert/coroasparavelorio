﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Web.Models;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Frases_de_HomenagensController : BaseController
    {
        public ProdutoRepository produtoRepository;
        public Frases_de_HomenagensController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
        }

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult LoadFrases(string URL)
        {
            List<FrasesModel> ListaDeFrases = new List<FrasesModel>();

            string Title = "";
            string H1 = "";
            string Tipo = "";
            string Description = "";

            switch (URL)
            {
                case "mensagens-para-amigo":
                    Title = "Frases e Mensagens de Luto para Amigo";
                    H1 = Title;
                    Tipo = "amigos";
                    Description = Title;
                    break;
                case "mensagens-para-familiares":
                    Title = "Frases e Mensagens de Luto para Familiares";
                    H1 = Title;
                    Tipo = "familiares";
                    Description = Title;
                    break;
                case "mensagens-para-colegas-de-trabalho":
                    Title = "Frases e Mensagens de Luto para Colegas de Trabalho";
                    H1 = Title;
                    Tipo = "colegas";
                    Description = Title;
                    break;
                case "mensagens-para-primos":
                    Title = "Frases e Mensagens de Luto para Primo";
                    H1 = Title;
                    Tipo = "primos";
                    Description = Title;
                    break;
                case "mensagens-para-pai":
                    Title = "Frases e Mensagens de Luto para Pai";
                    H1 = Title;
                    Tipo = "pai";
                    Description = Title;
                    break;
                case "mensagens-para-mae":
                    Title = "Frases e Mensagens de Luto para Mãe";
                    H1 = Title;
                    Tipo = "mae";
                    Description = Title;
                    break;
                case "mensagens-para-marido":
                    Title = "Mensagem para Marido Falecido";
                    H1 = Title;
                    Tipo = "marido";
                    Description = "A mensagem para marido falecido serve nesse momento para homenagear o ente querido, com muita ternura, amor e carinho.";
                    break;
                case "mensagens-para-tios":
                    Title = "Mensagem para Tio que Faleceu ";
                    H1 = Title;
                    Tipo = "tios";
                    Description = "A mensagem para um tio que faleceu serve para verbalizar esse momento tão triste e ao mesmo tempo especial acompanhado de um belíssimo arranjo de flores.";
                    break;
                case "mensagens-para-avos":
                    Title = "Mensagens de Luto para Avós Falecidos";
                    H1 = "Frases de falecimento de avô";
                    Tipo = "avos";
                    Description = "As frases de falecimento de avô servem nesse momento como um alento para a família e uma belíssima homenagem para esse querido homem que partiu.";
                    break;
                case "mensagens-evangelicos":
                    Title = "Mensagens de Luto Evangélicas";
                    H1 = Title;
                    Tipo = "evangelicos";
                    Description = "A mensagem de luto evangélica é acompanhada de uma belíssima coroa de flores e simboliza de maneira especial essa passagem da vida.";
                    break;
            }

            #region CARREGA LISTA DE FRASES

            foreach (var Line in System.IO.File.ReadAllLines(Server.MapPath("~/assets/frades-homenagem.csv")))
            {
                int IDReturn = int.Parse(Line.Split(',')[0]);
                string TipoReturn = Line.Split(',')[1];
                string FraseReturn = Line.Split(',')[2];

                if(TipoReturn == Tipo)
                {
                    ListaDeFrases.Add(new FrasesModel {
                        ID = IDReturn, 
                        Tipo = TipoReturn,
                        Frase = FraseReturn
                    });
                }
            }

            ViewBag.ListaDeFrases = ListaDeFrases;

            #endregion

            ViewBag.Title = Title;
            ViewBag.H1 = H1;
            ViewBag.Description = Description + " -  Entrega de Coroas de Flores em todo Brasil, 24 horas todos os dias + frete grátis. Acesse o site e confira!";

            return View("~/Views/frases_de_homenagens/frases.cshtml");
        }


        public void CadastraFrase(string Frase)
        {
            Session["Frase"] = Frase;
        }

        
    }

}
