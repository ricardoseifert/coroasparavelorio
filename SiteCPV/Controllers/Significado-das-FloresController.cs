﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Significado_das_FloresController : BaseController
    {

        private IPersistentRepository<Flor> florRepository;
        public ProdutoRepository produtoRepository;

        public Significado_das_FloresController(ObjectContext context) : base(context)
        {
            florRepository = new PersistentRepository<Flor>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            var flores = florRepository.GetByExpression(f => f.Publicado).OrderBy(f => f.Nome).ToList();
            return View(flores);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Detalhes(int id)
        {
            var flor = florRepository.GetByExpression(n => n.Publicado && n.ID == id).FirstOrDefault();
            if (flor == null)
                return RedirectToAction("Index");
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;

            ViewBag.Title = flor.Nome + " - Significado das Flores";
            ViewBag.Description = flor.Nome + " - Descubra o significado de cada flor que compõe nossas coroas fúnebres. Entregamos em todo Brasil, 24 horas todos os dias + frete grátis. Confira!";

            ViewBag.KeyWords = flor.TagKeywords;
            ViewBag.ProdutosVejaTambem = flor.ProdutoFlors.Where(c => c.Produto.Disponivel && c.Produto.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).Select(c => c.Produto).ToList();
            return View(flor);
        }

    }
}
