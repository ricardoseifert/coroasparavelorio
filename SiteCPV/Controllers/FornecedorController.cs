﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Web.Models;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using System.IO;
using Newtonsoft.Json;
using Domain.Factories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class FornecedorController : BaseController
    {
        private PersistentRepository<Log> logRepository;
        public ProdutoRepository produtoRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        public PedidoRepository pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<Local> localRepository;
        private FornecedorDashboardRepository fornecedorDashboardRepository;

        public FornecedorController(ObjectContext context) : base(context)
        {
            logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());
            produtoRepository = new ProdutoRepository(context);
            pedidoRepository = new PedidoRepository(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            localRepository = new PersistentRepository<Local>(context);
            fornecedorDashboardRepository = new FornecedorDashboardRepository(context);
        }


        [HttpPost]
        public JsonResult LocationUpdate()
        {
            var Latitude = Request.Form["Latitude"];
            var Longitude = Request.Form["Longitude"];
            var TipoPedido = Request.Form["TipoPedido"];
            var IdPEdido = int.Parse(Request.Form["guid"]);

            if (TipoPedido == "CPV")
            {
                var pedido = pedidoRepository.Get(IdPEdido);
                if (pedido.DataEntrega == null)
                {
                    pedido.LatitudeEntrega = Latitude;
                    pedido.LongitudeEntrega = Longitude;
                    pedido.DtUpdateLocEntrega = DateTime.Now;
                    pedidoRepository.Save(pedido);
                }
            }
            else
            {
                var pedido = pedidoEmpresaRepository.Get(IdPEdido);
                if (pedido.DataEntrega == null)
                {
                    pedido.LatitudeEntrega = Latitude;
                    pedido.LongitudeEntrega = Longitude;
                    pedido.DtUpdateLocEntrega = DateTime.Now;
                    pedidoEmpresaRepository.Save(pedido);
                }
            }

            return Json("OK");
        }

        [HttpPost]
        public JsonResult SalvarEntrega()
        {
            var RecebidoPor = Request.Form["RecebidoPor"];
            var Relacionamento = Request.Form["Relacionamento"];
            var OutrosDetalhes = Request.Form["OutrosDetalhes"];
            var Obs = Request.Form["Obs"];
            var TipoPedido = Request.Form["TipoPedido"];
            var PedidoID = int.Parse(Request.Form["PedidoID"]);

            if(TipoPedido == "CPV")
            {
                var pedido = pedidoRepository.Get(PedidoID);

                pedido.DataEntrega = DateTime.Now;
                pedido.RecebidoPor = RecebidoPor.ToUpper();
                pedido.ParentescoRecebidoPor = Relacionamento.ToUpper() + " - " + OutrosDetalhes;
                pedido.FornecedorObservacoes = Obs;
                pedido.FornecedorEntregou = true;
                pedido.StatusEntregaID = (int)TodosStatusEntrega.Entregue;
                pedido.DataEmailEntrega = DateTime.Now;

                pedidoRepository.Save(pedido);

                administradorPedidoRepository.RegistraLogFornecedor((int)pedido.FornecedorID, PedidoID, null, null, null, AdministradorPedido.Acao.FornecedorMarcouEntregue, pedido.FornecedorID);

                #region DIRPARAR SMS 

                try
                {
                    string Numero = "";

                    if (!string.IsNullOrEmpty(pedido.Cliente.CelularContato))
                    {
                        Numero = pedido.Cliente.CelularContato;
                    }
                    else
                    {
                        Numero = pedido.Cliente.TelefoneContato;
                    }

                    Numero = Numero.Replace(" ", "").Replace("(", "").Replace(")", "");

                    if (Numero.Length == 11)
                    {
                        string Mensagem = Domain.Core.Configuracoes.SMS_3_FORNECEDORES;

                        Mensagem = Mensagem.Replace("*|NOMEQUEMRECEBEU|*", RecebidoPor.ToUpper());

                        if (!string.IsNullOrEmpty(OutrosDetalhes))
                        {
                            Mensagem = Mensagem.Replace("*|PARENTESCO|*", Relacionamento.ToUpper() + " - " + OutrosDetalhes);
                        }
                        else
                        {
                            Mensagem = Mensagem.Replace("*|PARENTESCO|*", Relacionamento.ToUpper());
                        }
                        
                        Mensagem = Mensagem.Replace("*|HORARECEBIMENTO|*", DateTime.Now.ToString("HH:mm"));

                        dynamic ResultSMS = JsonConvert.DeserializeObject(new MovileFactory().EnviarSms("55" + Numero, Domain.Core.Funcoes.AcertaAcentos(Mensagem)));
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }


                #endregion

                pedido.EnviarEmailEntregaNovo();

            }
            else
            {
                var pedido = pedidoEmpresaRepository.Get(PedidoID);

                pedido.DataEntrega = DateTime.Now;
                pedido.RecebidoPor = RecebidoPor.ToUpper();
                pedido.ParentescoRecebidoPor = Relacionamento.ToUpper() + " - " + OutrosDetalhes;
                pedido.FornecedorObservacoes = Obs;
                pedido.FornecedorEntregou = true;
                pedido.StatusEntregaID = (int)TodosStatusEntrega.Entregue;
                pedido.DataEmailEntrega = DateTime.Now;

                pedidoEmpresaRepository.Save(pedido);

                administradorPedidoRepository.RegistraLogFornecedor((int)pedido.FornecedorID, PedidoID, null, null, null, AdministradorPedido.Acao.FornecedorMarcouEntregue, pedido.FornecedorID);

                #region DIRPARAR SMS 

                try
                {
                    string Numero = "";

                    if (!string.IsNullOrEmpty(pedido.Colaborador.TelefoneCelular))
                    {
                        Numero = pedido.Colaborador.TelefoneCelular;
                    }
                    else
                    {
                        Numero = pedido.Colaborador.Telefone;
                    }

                    Numero = Numero.Replace(" ", "").Replace("(", "").Replace(")", "");

                    if (Numero.Length == 11)
                    {
                        string Mensagem = Domain.Core.Configuracoes.SMS_3_FORNECEDORES;

                        Mensagem = Mensagem.Replace("*|NOMEQUEMRECEBEU|*", RecebidoPor.ToUpper());

                        if (!string.IsNullOrEmpty(OutrosDetalhes))
                        {
                            Mensagem = Mensagem.Replace("*|PARENTESCO|*", Relacionamento.ToUpper() + " - " + OutrosDetalhes);
                        }
                        else
                        {
                            Mensagem = Mensagem.Replace("*|PARENTESCO|*", Relacionamento.ToUpper());
                        }

                        Mensagem = Mensagem.Replace("*|HORARECEBIMENTO|*", DateTime.Now.ToString("HH:mm"));

                        dynamic ResultSMS = JsonConvert.DeserializeObject(new MovileFactory().EnviarSms("55" + Numero, Domain.Core.Funcoes.AcertaAcentos(Mensagem)));
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }

                #endregion

                pedido.EnviarEmailEntrega();
            }

            return Json("OK");
        }

        [HttpPost]
        public JsonResult EnviarFoto()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["Pedido"]))
                {
                    var PedidoItemID = int.Parse(Request.Form["guid"]);
                    var Cod = System.Guid.Parse(Request.Form["Pedido"]);
                    var Pedido = pedidoRepository.GetByCodigo(Cod);

                    #region CRIAR DIRETORIO E ENVIAR FOTO

                    var Arquivo = Request.Files[0];

                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + Pedido.Numero));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }

                    var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(Arquivo.FileName);

                    var Path = "E:\\WEB\\cdn.coroasparavelorio.com.br\\Pedidos\\" + Pedido.Numero + "\\" + PedidoItemID + ".jpg";
                    if (Domain.Core.Configuracoes.HOMOLOGACAO)
                        Path = Server.MapPath("~/Content/Pedidos/" + Pedido.Numero + "/" + PedidoItemID + ".jpg");

                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(Arquivo, Path, new ImageResizer.ResizeSettings("width=600&height=600&crop=auto;format=jpg;mode=pad;scale=both&autorotate=true"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    #endregion

                    #region ATUALIZAR STATUS DE FOTO

                    var PedidoItem = Pedido.PedidoItems.Where(r => r.ID == PedidoItemID).First();

                    // STATUS 3 - FOTO APROVADA
                    PedidoItem.StatusFotoID = 3;

                    pedidoRepository.Save(Pedido);

                    #endregion

                    #region SETA SAIR PARA ENTREGA

                    var Result = fornecedorDashboardRepository.SairEntrega(Pedido.ID + ",CPV", (int)Pedido.FornecedorID);

                    #endregion

                    // ENVIAR EMAIL DE FOTO
                    if (Pedido.FotosDeEntregaValidadas)
                        Pedido.EnviarEmailFotoProduro();

                    return Json("OK," + PedidoItemID + "," + "http://admin.coroasparavelorio.com.br/content/pedidos/" + Pedido.Numero + "/" + PedidoItemID + ".jpg");
                }

                if (!string.IsNullOrEmpty(Request.Form["PedidoEmpresa"]))
                {
                    var Cod = int.Parse(Request.Form["PedidoEmpresa"]);
                    var Pedido = pedidoEmpresaRepository.Get(Cod);

                    #region CRIAR DIRETORIO E ENVIAR FOTO

                    var Arquivo = Request.Files[0];

                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + Pedido.ID));
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }

                    var nomefoto = Domain.Core.Funcoes.GeraNomeArquivoMVC(Arquivo.FileName);

                    var Path = "E:\\WEB\\cdn.coroasparavelorio.com.br\\Pedidos\\" + Pedido.ID + "\\" + Pedido.ID + ".jpg";
                    if (Domain.Core.Configuracoes.HOMOLOGACAO)
                        Path = Server.MapPath("~/Content/Pedidos/" + Pedido.ID + "/" + Pedido.ID + ".jpg");

                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(Arquivo, Path, new ImageResizer.ResizeSettings("width=600&height=600&crop=auto;format=jpg;mode=pad;scale=both"));

                    i.CreateParentDirectory = true;
                    i.Build();

                    #endregion

                    #region ATUALIZAR STATUS DE FOTO

                    // STATUS 3 - FOTO APROVADA
                    Pedido.StatusFotoID = 3;

                    pedidoEmpresaRepository.Save(Pedido);

                    #endregion

                    #region SETA SAIR PARA ENTREGA

                    var Result = fornecedorDashboardRepository.SairEntrega(Pedido.ID + ",EMPRESA", (int)Pedido.FornecedorID);

                    #endregion

                    // ENVIAR EMAIL DE FOTO
                    if (Pedido.FotosDeEntregaValidadas)
                        Pedido.EnviarEmailFotoProduro();

                    return Json("OK," + Pedido.ID + "," + "http://admin.coroasparavelorio.com.br/content/pedidos/" + Pedido.ID + "/" + Pedido.ID + ".jpg");
                }
            }
            catch (Exception ex)
            {
                string PedidoEmpresaID = "";
                if (!string.IsNullOrEmpty(Request.Form["PedidoEmpresa"]))
                {
                    PedidoEmpresaID = Request.Form["PedidoEmpresa"].ToString();

                    logRepository.Save(new Log
                    {
                        Pedido = PedidoEmpresaID,
                        Data = DateTime.Now,
                        Var1 = "ERRO - EnvioFotoFornecedor ",
                        Var2 = "Forms: " + Request.Form.ToString() + " " + ex.Message
                    });
                }

                string PedidoID = "";
                if (!string.IsNullOrEmpty(Request.Form["Pedido"]))
                {
                    PedidoID = Request.Form["Pedido"].ToString();

                    logRepository.Save(new Log
                    {
                        Pedido = PedidoID,
                        Data = DateTime.Now,
                        Var1 = "ERRO - EnvioFotoFornecedor ",
                        Var2 = "Forms: " + Request.Form.ToString() + " " + ex.Message
                    });
                }

                return Json(ex.Message);
            }

            return Json("ERROR");
        }
        public ActionResult Index()
        {
            Pedido PedidoCPV = null;
            PedidoEmpresa PedidoCORP = null;
            Fornecedor NovoFornecedor = null;

            bool Step2 = false;

            if (!string.IsNullOrEmpty(Request.QueryString["Pedido"]))
            {
                var Cod = Guid.Parse(Request.QueryString["Pedido"].ToString());
                PedidoCPV = pedidoRepository.GetByExpression(r => r.Codigo == Cod).First();
                NovoFornecedor = PedidoCPV.Fornecedor;

                if(PedidoCPV.PedidoItems.Where(r => r.StatusFoto == PedidoItem.TodosStatusFoto.Aprovada).Count() == PedidoCPV.PedidoItems.Count)
                    Step2 = true;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PedidoEmpresa"]))
            {
                PedidoCORP = pedidoEmpresaRepository.Get(int.Parse(Request.QueryString["PedidoEmpresa"]));
                NovoFornecedor = PedidoCORP.Fornecedor;

                if (PedidoCORP.StatusFoto == PedidoEmpresa.TodosStatusFoto.Aprovada)
                    Step2 = true;
            }

            ViewBag.PedidoCPV = PedidoCPV;
            ViewBag.PedidoCORP = PedidoCORP;
            ViewBag.NovoFornecedor = NovoFornecedor;
            ViewBag.Step2 = Step2;

            var LocalDeEntrega = "";
            var Homenageado = "";
            int LocalId = 0;
            if (PedidoCPV != null)
            {
                if (PedidoCPV.LocalID != null)
                {
                    LocalId = (int)PedidoCPV.LocalID;
                    LocalDeEntrega = localRepository.Get(LocalId).Titulo;
                }
                else {
                    LocalDeEntrega = PedidoCPV.ComplementoLocalEntrega;
                }
                
                Homenageado = PedidoCPV.PessoaHomenageada;
            }
            if (PedidoCORP != null)
            {
                if (PedidoCORP.LocalID != null)
                {
                    LocalId = (int)PedidoCORP.LocalID;
                    LocalDeEntrega = localRepository.Get(LocalId).Titulo;
                }
                else
                {
                    LocalDeEntrega = PedidoCORP.ComplementoLocalEntrega;
                }
               
                Homenageado = PedidoCORP.PessoaHomenageada;
            }

            ViewBag.LocalDeEntrega = LocalDeEntrega;
            ViewBag.Homenageado = Homenageado;

            return View("~/Views/Fornecedor/Index.cshtml");
        }
    }

}
