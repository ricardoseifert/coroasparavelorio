﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using Domain.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Pesquisa_de_SatisfacaoController : BaseController
    {

        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Pesquisa> pesquisaRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        public ProdutoRepository produtoRepository;
        public Pesquisa_de_SatisfacaoController(ObjectContext context) : base(context)
        {
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pesquisaRepository = new PersistentRepository<Pesquisa>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        public ActionResult Index(string numero)
        {
            if(numero != null)
                ViewBag.Pedido = pedidoRepository.GetByExpression(c => c.Numero.Trim().ToUpper() == numero.Trim().ToUpper()).FirstOrDefault();
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            ViewBag.Estados = estadoRepository.GetAll();
            return View();
        }


        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var pesquisa = new Pesquisa();
                pesquisa.PrazoEntrega = form["entrega"].Trim().ToUpper();
                pesquisa.QualidadeProduto = form["qualidade"].Trim().ToUpper();
                pesquisa.Atendimento = form["atendimento"].Trim().ToUpper();
                pesquisa.Recomendacao = Convert.ToBoolean(form["recomendacao"]);
                pesquisa.Satisfeito = Convert.ToBoolean(form["satisfeito"]);
                pesquisa.SatisfeitoNao = form["satisfeitonaooutro"];
                pesquisa.PorqueCPV = form["porquecpv"];
                pesquisa.ComoConheceu = form["conheceu"];
                pesquisa.Sugestoes = form["sugestoes"];
                pesquisa.Nome = form["nome"].Trim().ToUpper();
                pesquisa.Telefone = form["telefone"];
                pesquisa.Email = form["email"].Trim().ToLower();
                pesquisa.CidadeID = Convert.ToInt32(form["CidadeID"]);
                pesquisa.EstadoID = Convert.ToInt32(form["EstadoID"]);
                pesquisa.Telefone = form["telefone"];
                pesquisa.AutorizouDepoimento = Convert.ToBoolean(form["depoimento"]);
                pesquisa.DataCriacao = DateTime.Now;

                try
                {
                    var numero = form["pedido"];
                    var pedido = pedidoRepository.GetByExpression(c => c.Numero.Trim().ToUpper() == numero.Trim().ToUpper()).FirstOrDefault();
                    pesquisa.PedidoID = pedido.ID;
                    pesquisa.ClienteID = pedido.ClienteID;
                }
                catch { }

                pesquisaRepository.Save(pesquisa);

                if (pesquisa.AutorizouDepoimento)
                {
                    var depoimento = new Depoimento();
                    depoimento.Cidade = pesquisa.Cidade.NomeCompleto;
                    depoimento.Email = pesquisa.Email;
                    depoimento.Data = DateTime.Now;
                    depoimento.Aprovado = false;
                    depoimento.Empresa = "";
                    depoimento.Nome = pesquisa.Nome;
                    depoimento.Nota = 5;
                    try { depoimento.ProdutoID = pesquisa.Pedido.PedidoItems.FirstOrDefault().ProdutoTamanho.ProdutoID; }
                    catch { }
                    depoimento.Texto = pesquisa.Sugestoes;
                    depoimento.Tipo = Depoimento.Tipos.PF;
                    depoimentoRepository.Save(depoimento);
                }

                return Json(new { status = "OK" });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }
    }
}
