﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Net;
using System.IO;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class BaseController : Controller
    {

        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Promocao> promocaoRepository;
        private IPersistentRepository<Parametro> parametroRepository;

        public BaseController(ObjectContext context)
        {
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            promocaoRepository = new PersistentRepository<Promocao>(context);
            parametroRepository = new PersistentRepository<Parametro>(context);

            var configuracao = configuracaoRepository.GetAll().FirstOrDefault();
            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.Configuracoes = configuracao;
            ViewBag.PromocaoSemana = promocao;
        }
    }
}
