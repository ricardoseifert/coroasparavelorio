﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Filter;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;

namespace Web.Controllers
{ 
    public class V3Controller : BaseController
    {
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Coloracao> corRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<PrimeiraCompra> primeiraCompraRepository;
        private IPersistentRepository<Local> localRepository;
        
        public V3Controller(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            corRepository = new PersistentRepository<Coloracao>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            localRepository = new PersistentRepository<Local>(context);
            primeiraCompraRepository = new PersistentRepository<PrimeiraCompra>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index(){
            ViewBag.Depoimentos = depoimentoRepository.GetByExpression(c => c.Aprovado && c.Nota >= 8 && c.ProdutoID.HasValue).OrderByDescending(r => r.Data).Take(10).ToList();

            ViewBag.Cemiterios = localRepository.GetByExpression(c => c.TipoID == (int)Local.Tipos.Cemiterio).OrderByDescending(r => r.ID).Take(4).ToList();
            ViewBag.Capelas = localRepository.GetByExpression(c => c.TipoID == (int)Local.Tipos.Capela).OrderByDescending(r => r.ID).Take(4).ToList();
            ViewBag.Velorios = localRepository.GetByExpression(c => c.TipoID == (int)Local.Tipos.Velorio).OrderByDescending(r => r.ID).Take(4).ToList();
            ViewBag.Cidades = localRepository.GetByExpression(c => c.TipoID == (int)Local.Tipos.Cidade).OrderByDescending(r => r.ID).Take(4).ToList();
           

            return View(produtoRepository.GetAll().Where(r => r.ID == 24 || r.ID == 1 || r.ID == 26 || r.ID == 22).ToList());
        }

    }
   


}
