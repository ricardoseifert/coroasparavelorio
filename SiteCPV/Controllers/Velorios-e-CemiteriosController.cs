﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class Velorios_E_CemiteriosController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;

        public Velorios_E_CemiteriosController(ObjectContext context) : base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            ViewBag.Produtos = produtoRepository.Publicados().Where(p => p.ID != 168 && p.ID != 169 && p.ID != 170 && p.ID != 171 && p.ID != 172 && p.ID != 173 && p.ID != 174 && p.ID != 175 && p.ID != 176 && p.ID != 177 && p.ID != 178 && p.ID != 179 && p.ID != 180 && p.ID != 181 && p.ID != 182 && p.ID != 183).Take(4).ToList();;
            
            var context = new COROASEntities();
            ViewBag.Cidades = (from C in context.Locals select C.Cidade).Distinct().ToList();

            return View();
        }

        public string GetResult(int CidadeId)
        {
            var ResultLst = localRepository.GetByExpression(c => c.CidadeID == CidadeId).ToList();
            var Cidade = ResultLst.FirstOrDefault().Cidade;

            var Result = string.Empty;

            if(Cidade != null){
               
           
                Result += "<div id='ResultadosBusca' style='display: block;' class='ResulCidade div-" + CidadeId + "'>";

                Result += "<div class='ResulBLinha1'><div class='ResulBTitulo'>Resultado de busca para " + Cidade.NomeCompleto + " </div></div>";

                Result += "<div class='Resultados'>";

                foreach(var Item in ResultLst){

                    if (Item.TipoID == (int)Local.Tipos.Cidade)
                    {
                        Result += "<div class='Resultados2'><b>Cidade de " + Item.Cidade.NomeCompleto + "</b><br /><br /><br /><a href='/coroas-de-flores/" + Item.RotuloUrl + "' class='FiltroSelecionado'>+ Veja mais</a></div>";
                    }
                    else
                    {
                        Result += "<div class='Resultados2'><b>" + Item.Titulo + "</b><br />" + Item.Logradouro + " " + Item.Numero + " <br />" + Item.Bairro + " " + Item.CEP + " <br /><a href='/coroas-de-flores/" + Item.RotuloUrl + "' class='FiltroSelecionado'>+ Veja mais</a></div>";
                    }

                    
                }

                Result += "<div class='Resultados3'><a href='/local/buscar?cidadeID=" + CidadeId + "&cidade=" + Cidade.Nome + "' class='FiltroSelecionado'>Veja mais resultados</a></div>";

                Result += "</div>";

                Result += "</div>";
            }

            return Result;
        }

        public JsonResult GetCidades(string estado)
        {
            var CidadeList = localRepository.GetByExpression(c => c.Cidade.Estado.Sigla == estado && c.TipoID == (int)Local.Tipos.Cidade).Select(c => c.Cidade).Distinct().ToList();

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (var cidade in CidadeList.OrderBy(c => c.Nome))
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString()});
            }
            return Json(result);
        }
    }
}
