﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Repositories;
using System.Data.SqlClient;
using System.Configuration;
using Web.Models;

namespace Web.Controllers
{
   //[[RequireHttps]
    public class CarrinhoController : BaseController
    {
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Desconto> descontoRepository;
        private ClienteRepository clienteRepository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private IPersistentRepository<Parametro> parametroRepository;
        private ClienteModel clienteModel;

        public CarrinhoController(ObjectContext context) : base(context)
        {
            clienteRepository = new ClienteRepository(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            descontoRepository = new PersistentRepository<Desconto>(context);
            colaboradorRepository = new PersistentRepository<Colaborador>(context);
            parametroRepository = new PersistentRepository<Parametro>(context);
            clienteModel = new ClienteModel(context);
        }

        public ActionResult Index(Carrinho carrinho)
        {
            if (carrinho.Vazio)
            {
                return RedirectToAction("Index", "Home");
            }
            
            ViewBag.Desconto = descontoRepository.GetAll().FirstOrDefault();
            
            ViewBag.Cliente = clienteModel.Autenticado;

            return View(carrinho);
        }

        public ActionResult Resumo(Carrinho carrinho)
        {
            return View(carrinho);
        }

        public ActionResult ResumoOld(Carrinho carrinho)
        {
            return View(carrinho);
        }

        public ActionResult ResumoMobile(Carrinho carrinho)
        {
            return View(carrinho);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult Adicionar(Carrinho carrinho, int produtoTamanhoID, string descricao, string mensagem, string observacoes)
        {
            var produtoTamanho = produtoTamanhoRepository.Get(produtoTamanhoID);
            if (produtoTamanho != null)
            {
                carrinho.Adicionar(produtoTamanho, descricao, mensagem, observacoes);
                return Content("OK");
            }
            return Content("ERRO");
        }

        [HttpGet]
        [ValidateInput(false)]
        public void AdicionarGet(Carrinho carrinho, int produtoTamanhoID, string descricao, string mensagem, string observacoes)
        {
            string Redirect = "/carrinho";
            if (!string.IsNullOrEmpty(Request.QueryString["Src"]))
            {
                if (Request.QueryString["Src"] == "LC")
                {
                    Redirect = "/carrinho?Src=LC";
                }
            }

            var produtoTamanho = produtoTamanhoRepository.Get(produtoTamanhoID);
            if (produtoTamanho != null)
            {
                carrinho.Adicionar(produtoTamanho, descricao, mensagem, observacoes);
                Response.Redirect(Redirect);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult Atualizar(Carrinho carrinho, int itemID, string descricao, string mensagem, string observacoes)
        {
            carrinho.Atualizar(itemID, descricao, mensagem, observacoes);
            return Content("OK");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult AtualizarMensagem(Carrinho carrinho, int itemID, string mensagem)
        {
            carrinho.Atualizar(itemID, mensagem);
            return Content("OK");
        }

        public ContentResult Remover(Carrinho carrinho, int itemID)
        {
            carrinho.Remover(itemID);
            return Content("OK");
        }

        public ActionResult Limpar(Carrinho carrinho)
        {
            carrinho.Limpar();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public string VerificarEmailLC(string Id, Carrinho carrinho)
        {
            string email = Id;

            if(email == "ricardo.seifertb@gmail.com")
                return "{ \"url\" : \"\" }";

            var colaborador = colaboradorRepository.GetByExpression(r => r.Email == email && r.Removido == false).ToList();
            //Cliente cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();

            if (colaborador.Count >= 1)
            {
                var ColaboradorID = colaborador.First().ID;
                var Token = Guid.NewGuid().ToString();

                string ItensArray = string.Empty;
                if (carrinho != null && carrinho.Itens.Count > 0)
                {
                    ItensArray = "|" + string.Join(",", carrinho.Itens.Select(r => r.ProdutoTamanho.ID).ToArray());
                }

                parametroRepository.Save(new Parametro
                {
                    Chave = "LoginToken,LC",
                    DataCriacao = DateTime.Now,
                    Val1 = Token,
                    Val2 = DateTime.Now.AddMinutes(5).ToString(),
                    Val3 = ColaboradorID.ToString() + ItensArray
                });

                return "{ \"url\" : \"" + Domain.Core.Configuracoes.URL_LOGIN_LC + "?token=" + Token + "\" }";
            }

            return "{ \"url\" : \"\" }";
        }

        [HttpGet]
        public ActionResult VerificarEmail(string Id){
            string email = Id;
            Cliente cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();

            if (cliente != null)
                return RedirectToAction("identificacao", "Cliente", new { email = email });

            return RedirectToAction("index", "cadastre-se", new { email = email, returnUrl = "/pedido" });
        }
    }
}
