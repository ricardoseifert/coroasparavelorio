﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEP.Entities;
using DomainExtensions.Repositories;

namespace CEP
{
    public class CEP
    { 
        string _uf;
        string _cidade;
        string _bairro;
        string _tipo_logradouro;
        string _logradouro; 
        string _resultado; 
        string _resultato_txt;
         
        public string UF
        {
            get { return _uf; }
        }
        public string Cidade
        {
            get { return _cidade; }
        }
        public string Bairro
        {
            get { return _bairro; }
        }
        public string TipoLogradouro
        {
            get { return _tipo_logradouro; }
        }
        public string Logradouro
        {
            get { return _logradouro; }
        }
        public string Resultado
        {
            get { return _resultado; }
        }
        public string ResultadoTXT
        {
            get { return _resultato_txt; }
        } 

        public CEP(string CEP)
        {
            try
            {
                CEP = CEP.Replace("_", "0").Trim();
            }
            catch
            {
                CEP = "00000-000";
            }
            _uf = "";
            _cidade = "";
            _bairro = "";
            _tipo_logradouro = "";
            _logradouro = "";
            _resultado = "0";
            _resultato_txt = "CEP não encontrado";

            CEP = CEP.Trim();

            if (CEP.Length == 8)
            {
                var context = new CEPEntities();

                var cep_unicoRepository = new PersistentRepository<cep_unico>(context);
                var ufRepository = new PersistentRepository<uf>(context);


                var prefixoCep = Convert.ToInt32(CEP.Substring(0, 5));

                var uf = ufRepository.GetByExpression(c => c.Cep1 <= prefixoCep && c.Cep2 >= prefixoCep).FirstOrDefault();

                if (uf != null)
                {
                    switch (uf.Sigla)
                    {
                        case "AC":
                            _uf = uf.Sigla;
                            var acRepository = new PersistentRepository<ac>(context);
                            var cep_ac = acRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ac != null)
                            {
                                _cidade = cep_ac.cidade.ToUpper().Trim();
                                _bairro = cep_ac.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ac.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ac.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "AL":
                            _uf = uf.Sigla;
                            var alRepository = new PersistentRepository<al>(context);
                            var cep_al = alRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_al != null)
                            {
                                _cidade = cep_al.cidade.ToUpper().Trim();
                                _bairro = cep_al.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_al.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_al.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "AM":
                            _uf = uf.Sigla;
                            var amRepository = new PersistentRepository<am>(context);
                            var cep_am = amRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_am != null)
                            {
                                _cidade = cep_am.cidade.ToUpper().Trim();
                                _bairro = cep_am.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_am.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_am.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "AP":
                            _uf = uf.Sigla;
                            var apRepository = new PersistentRepository<ap>(context);
                            var cep_ap = apRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ap != null)
                            {
                                _cidade = cep_ap.cidade.ToUpper().Trim();
                                _bairro = cep_ap.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ap.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ap.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "BA":
                            _uf = uf.Sigla;
                            var baRepository = new PersistentRepository<ba>(context);
                            var cep_ba = baRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ba != null)
                            {
                                _cidade = cep_ba.cidade.ToUpper().Trim();
                                _bairro = cep_ba.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ba.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ba.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "CE":
                            _uf = uf.Sigla;
                            var ceRepository = new PersistentRepository<ce>(context);
                            var cep_ce = ceRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ce != null)
                            {
                                _cidade = cep_ce.cidade.ToUpper().Trim();
                                _bairro = cep_ce.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ce.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ce.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "DF":
                            _uf = uf.Sigla;
                            var dfRepository = new PersistentRepository<df>(context);
                            var cep_df = dfRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_df != null)
                            {
                                _cidade = cep_df.cidade.ToUpper().Trim();
                                _bairro = cep_df.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_df.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_df.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "ES":
                            _uf = uf.Sigla;
                            var esRepository = new PersistentRepository<es>(context);
                            var cep_es = esRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_es != null)
                            {
                                _cidade = cep_es.cidade.ToUpper().Trim();
                                _bairro = cep_es.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_es.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_es.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "GO":
                            _uf = uf.Sigla;
                            var goRepository = new PersistentRepository<go>(context);
                            var cep_go = goRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_go != null)
                            {
                                _cidade = cep_go.cidade.ToUpper().Trim();
                                _bairro = cep_go.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_go.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_go.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "MA":
                            _uf = uf.Sigla;
                            var maRepository = new PersistentRepository<ma>(context);
                            var cep_ma = maRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ma != null)
                            {
                                _cidade = cep_ma.cidade.ToUpper().Trim();
                                _bairro = cep_ma.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ma.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ma.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "MG":
                            _uf = uf.Sigla;
                            var mgRepository = new PersistentRepository<mg>(context);
                            var cep_mg = mgRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_mg != null)
                            {
                                _cidade = cep_mg.cidade.ToUpper().Trim();
                                _bairro = cep_mg.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_mg.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_mg.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "MS":
                            _uf = uf.Sigla;
                            var msRepository = new PersistentRepository<ms>(context);
                            var cep_ms = msRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ms != null)
                            {
                                _cidade = cep_ms.cidade.ToUpper().Trim();
                                _bairro = cep_ms.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ms.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ms.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "MT":
                            _uf = uf.Sigla;
                            var mtRepository = new PersistentRepository<mt>(context);
                            var cep_mt = mtRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_mt != null)
                            {
                                _cidade = cep_mt.cidade.ToUpper().Trim();
                                _bairro = cep_mt.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_mt.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_mt.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "PA":
                            _uf = uf.Sigla;
                            var paRepository = new PersistentRepository<pa>(context);
                            var cep_pa = paRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_pa != null)
                            {
                                _cidade = cep_pa.cidade.ToUpper().Trim();
                                _bairro = cep_pa.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_pa.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_pa.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "PB":
                            _uf = uf.Sigla;
                            var pbRepository = new PersistentRepository<pb>(context);
                            var cep_pb = pbRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_pb != null)
                            {
                                _cidade = cep_pb.cidade.ToUpper().Trim();
                                _bairro = cep_pb.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_pb.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_pb.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "PE":
                            _uf = uf.Sigla;
                            var peRepository = new PersistentRepository<pe>(context);
                            var cep_pe = peRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_pe != null)
                            {
                                _cidade = cep_pe.cidade.ToUpper().Trim();
                                _bairro = cep_pe.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_pe.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_pe.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "PI":
                            _uf = uf.Sigla;
                            var piRepository = new PersistentRepository<pi>(context);
                            var cep_pi = piRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_pi != null)
                            {
                                _cidade = cep_pi.cidade.ToUpper().Trim();
                                _bairro = cep_pi.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_pi.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_pi.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "PR":
                            _uf = uf.Sigla;
                            var prRepository = new PersistentRepository<pr>(context);
                            var cep_pr = prRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_pr != null)
                            {
                                _cidade = cep_pr.cidade.ToUpper().Trim();
                                _bairro = cep_pr.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_pr.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_pr.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "RJ":
                            _uf = uf.Sigla;
                            var rjRepository = new PersistentRepository<rj>(context);
                            var cep_rj = rjRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_rj != null)
                            {
                                _cidade = cep_rj.cidade.ToUpper().Trim();
                                _bairro = cep_rj.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_rj.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_rj.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "RN":
                            _uf = uf.Sigla;
                            var rnRepository = new PersistentRepository<rn>(context);
                            var cep_rn = rnRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_rn != null)
                            {
                                _cidade = cep_rn.cidade.ToUpper().Trim();
                                _bairro = cep_rn.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_rn.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_rn.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "RO":
                            _uf = uf.Sigla;
                            var roRepository = new PersistentRepository<ro>(context);
                            var cep_ro = roRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_ro != null)
                            {
                                _cidade = cep_ro.cidade.ToUpper().Trim();
                                _bairro = cep_ro.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_ro.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_ro.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "RR":
                            _uf = uf.Sigla;
                            var rrRepository = new PersistentRepository<rr>(context);
                            var cep_rr = rrRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_rr != null)
                            {
                                _cidade = cep_rr.cidade.ToUpper().Trim();
                                _bairro = cep_rr.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_rr.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_rr.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "RS":
                            _uf = uf.Sigla;
                            var rsRepository = new PersistentRepository<rs>(context);
                            var cep_rs = rsRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_rs != null)
                            {
                                _cidade = cep_rs.cidade.ToUpper().Trim();
                                _bairro = cep_rs.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_rs.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_rs.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "SC":
                            _uf = uf.Sigla;
                            var scRepository = new PersistentRepository<sc>(context);
                            var cep_sc = scRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_sc != null)
                            {
                                _cidade = cep_sc.cidade.ToUpper().Trim();
                                _bairro = cep_sc.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_sc.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_sc.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "SE":
                            _uf = uf.Sigla;
                            var seRepository = new PersistentRepository<se>(context);
                            var cep_se = seRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_se != null)
                            {
                                _cidade = cep_se.cidade.ToUpper().Trim();
                                _bairro = cep_se.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_se.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_se.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "SP":
                            _uf = uf.Sigla;
                            var spRepository = new PersistentRepository<sp>(context);
                            var cep_sp = spRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_sp != null)
                            {
                                _cidade = cep_sp.cidade.ToUpper().Trim();
                                _bairro = cep_sp.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_sp.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_sp.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;

                        case "TO":
                            _uf = uf.Sigla;
                            var toRepository = new PersistentRepository<to>(context);
                            var cep_to = toRepository.GetByExpression(c => c.cep.Replace("-", "") == CEP).FirstOrDefault();
                            if (cep_to != null)
                            {
                                _cidade = cep_to.cidade.ToUpper().Trim();
                                _bairro = cep_to.bairro.ToUpper().Trim();
                                _tipo_logradouro = cep_to.tp_logradouro.ToUpper().Trim();
                                _logradouro = cep_to.logradouro.ToUpper().Trim();
                                _resultado = "1";
                                _resultato_txt = "CEP completo";
                            }
                            else
                            {
                                var cepRepository = new PersistentRepository<cep_unico>(context);
                                var cep = cepRepository.GetByExpression(c => c.Cep.Replace("-", "") == CEP).FirstOrDefault();
                                if (cep != null)
                                {
                                    
                                    _cidade = cep.Nome.ToUpper().Trim();
                                    _resultado = "1";
                                    _resultato_txt = "CEP único";
                                }
                            }
                            break;
                    }
                }

            }
        }  
    }
}
