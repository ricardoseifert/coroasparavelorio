﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using CEP.Entities; 

namespace CEP.Core
{
    public class NinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<ObjectContext>().To<CEPEntities>();
        }
    }
}
