﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Xml.Linq;

namespace Domain.Service
{
    public class TrustVoxService
    { 
        public static void EnviaTrustVox()
        {
            var context = new COROASEntities();
            var pedidosRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            var logRepository = new PersistentRepository<Domain.Entities.Log>(context);

            var data = DateTime.Now.AddDays(-14);
            var pedidos = pedidosRepository.GetByExpression(c => c.DataEnvioPesquisa == null && c.StatusEntregaID == (int)TodosStatusEntrega.Entregue && c.DataCriacao >= data).ToList();

            Console.WriteLine(pedidos.Count.ToString());
            foreach (var pedido in pedidos)
            {
                Domain.Core.TrustVox.TrustVox.EnviaTrustVox(pedido);
                Console.WriteLine(pedido.Numero);

                var atualiza = logRepository.GetByExpression(c => c.Pedido == pedido.Numero).Count() > 0;
                if (atualiza)
                {
                    pedido.DataEnvioPesquisa = DateTime.Now;
                    pedidosRepository.Save(pedido);
                }
            }
        }  
    }
}
