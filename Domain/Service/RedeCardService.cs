﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WOOWEXTENSIONS.Payment.Cielo.Entities;

namespace Domain.Service
{
    public class RedeCardService
    {
        public void ChangeObjectType(System.Type objectType, ref object selectedObject)
        {
            selectedObject = Activator.CreateInstance(objectType);
        }

        public static RetornoRedeCard PagarHomolog(int pedidoID, decimal valor, int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela)
        {
            RedeCardHomolog.KomerciWcfClient client = new RedeCardHomolog.KomerciWcfClient();
            RedeCardHomolog.GetAuthorizedCredit GetAuthRede = new RedeCardHomolog.GetAuthorizedCredit();

            if (parcela == 1)
            {
                // AUTORIZAÇÃO COM CAPTURA AUTOMATICA A VISTA
                GetAuthRede.Transacao = 04;
                GetAuthRede.Parcelas = "00";
            }
            else
            {
                // AUTORIZAÇÃO COM CAPTURA AUTOMATICA PARCELADA
                GetAuthRede.Transacao = 08;
                GetAuthRede.Parcelas = parcela.ToString("00");
            }
            GetAuthRede.Recorrente = "0";
            GetAuthRede.Origem = "01";

            GetAuthRede.Filiacao = Domain.Core.Configuracoes.RedeCard_Filiacao;
            GetAuthRede.Senha = Domain.Core.Configuracoes.RedeCard_Senha;
            GetAuthRede.NumPedido = pedidoID.ToString();
            GetAuthRede.Total = valor.ToString("0.00").Replace(",", ".");


            // CARTAO PORTADOR
            GetAuthRede.Nrcartao = numero;
            GetAuthRede.Cvc2 = codigo;
            GetAuthRede.Mes = mes.ToString("00");
            GetAuthRede.Ano = ano.ToString("00");

            var result = client.GetAuthorizedCredit(GetAuthRede);
            client.Close();

            RetornoRedeCard retorno = new RetornoRedeCard();

            if(result.Msgret != "Sucesso")
            {
                retorno.MsgRet = result.Msgret;
                retorno.Codret = 99;
            }
            else
            {
                retorno.TID = result.Tid;
                retorno.NuAuthTransacao = result.NumAutor;
                retorno.NunSeq = result.NumSqn;
                retorno.DataTran = DateTime.Parse(result.Data.Substring(0, 4) + "/" + result.Data.Substring(4, 2) + "/" + result.Data.Substring(6, 2));
                retorno.Codret = int.Parse(result.CodRet);
                retorno.MsgRet = result.Msgret;
            }

            return retorno;
        }

        public static RetornoRedeCard PagarProducao(int pedidoID, decimal valor, int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela)
        {
            RedeCardProducao.KomerciWcfClient client = new RedeCardProducao.KomerciWcfClient();
            RedeCardProducao.GetAuthorizedCredit GetAuthRede = new RedeCardProducao.GetAuthorizedCredit();

            if (parcela == 1)
            {
                // AUTORIZAÇÃO COM CAPTURA AUTOMATICA A VISTA
                GetAuthRede.Transacao = 04;
                GetAuthRede.Parcelas = "00";
            }
            else
            {
                // AUTORIZAÇÃO COM CAPTURA AUTOMATICA PARCELADA
                GetAuthRede.Transacao = 08;
                GetAuthRede.Parcelas = parcela.ToString("00");
            }
            GetAuthRede.Recorrente = "0";
            GetAuthRede.Origem = "01";

            GetAuthRede.Filiacao = Domain.Core.Configuracoes.RedeCard_Filiacao;
            GetAuthRede.Senha = Domain.Core.Configuracoes.RedeCard_Senha;
            GetAuthRede.NumPedido = pedidoID.ToString();
            GetAuthRede.Total = valor.ToString("0.00").Replace(",", ".");


            // CARTAO PORTADOR
            GetAuthRede.Nrcartao = numero;
            GetAuthRede.Cvc2 = codigo;
            GetAuthRede.Mes = mes.ToString("00");
            GetAuthRede.Ano = ano.ToString("00");

            var result = client.GetAuthorizedCredit(GetAuthRede);
            client.Close();

            RetornoRedeCard retorno = new RetornoRedeCard();

            if (result.Msgret != "Sucesso")
            {
                retorno.MsgRet = result.Msgret;
                retorno.Codret = 99;
            }
            else
            {
                retorno.TID = result.Tid;
                retorno.NuAuthTransacao = result.NumAutor;
                retorno.NunSeq = result.NumSqn;
                retorno.DataTran = DateTime.Parse(result.Data.Substring(0, 4) + "/" + result.Data.Substring(4, 2) + "/" + result.Data.Substring(6, 2));
                retorno.Codret = int.Parse(result.CodRet);
                retorno.MsgRet = result.Msgret;
            }

            return retorno;
        }

        public static WOOWEXTENSIONS.Payment.Cielo.Entities.Retorno PagarLacos(int pedidoID, decimal valor, int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela)
        {
            var portador = new Portador()
            {
                Nome = nome,
                Numero = numero,
                Indicador = Portador.Indicadores.Informado,
                MesValidade = mes,
                AnoValidade = ano,
                CodigoSeguranca = codigo
            };

            var formaPagamento = new WOOWEXTENSIONS.Payment.Cielo.Entities.FormaPagamento()
            {
                Bandeira = (WOOWEXTENSIONS.Payment.Cielo.Entities.FormaPagamento.Bandeiras)bandeira,
                Produto = parcela == 1 ? WOOWEXTENSIONS.Payment.Cielo.Entities.FormaPagamento.Produtos.CreditoAVista : WOOWEXTENSIONS.Payment.Cielo.Entities.FormaPagamento.Produtos.ParceladoLoja,
                Parcelas = parcela
            };

            var pedidoCielo = new WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido()
            {
                DataHora = DateTime.Now,
                Descricao = "Laços Corporativos",
                Idioma = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Idiomas.Portugues,
                Moeda = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Moedas.Real,
                Numero = pedidoID.ToString(),
                Valor = valor
            };

            var RedeCardService = new WOOWEXTENSIONS.Payment.Cielo.Services.CieloService();
            var retorno = RedeCardService.AutorizarTransacaoDiretamente(portador, pedidoCielo, formaPagamento, true);

            return retorno;
        }
    }
}
