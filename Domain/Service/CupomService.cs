﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Entities;

namespace Domain.Service
{
    public class CupomService
    {

        private CupomRepository cupomRepository;

        public CupomService(ObjectContext context)
        {
            cupomRepository = new CupomRepository(context);
        }

        public bool PodeUtilizar(Cliente cliente, Cupom cupom)
        {
            var pedidosComCupom = cliente.Pedidoes.Count(p => p.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Removido && p.CupomID == cupom.ID);
            return pedidosComCupom < cupom.PorCliente;
        }

        public bool PodeUtilizar(Empresa empresa, Cupom cupom)
        {
            var pedidosComCupom = empresa.PedidoEmpresas.Count(p => p.StatusID != (int)PedidoEmpresa.TodosStatus.PedidoCancelado && p.CupomID == cupom.ID);
            return pedidosComCupom < cupom.PorCliente;
        }

        public void Utilizar(string codigo)
        {
            var cupom = cupomRepository.GetByCodigo(codigo);
            if (cupom != null)
            {
                cupom.QuantidadeUtilizado++;
                cupomRepository.Save(cupom);
            }
        }

    }
}
