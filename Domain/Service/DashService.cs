﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities;
using Domain.Entities.Generics;
using Domain.MetodosExtensao;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using Dapper;
using Domain.Entities.ReportsStoredProcs;

namespace Domain.Service {
    //=================================================================================
    public static class DashService{

        #region Variaveis
        //-----------------------------------------------------------------------------
        private static List<int> lstCarteiraCidades;
        private static List<int> lstCarteiraEstados;
        private static List<int> lstCarteiraCidadesOutros;
        private static List<int> lstCarteiraEstadosOutros;
        //-----------------------------------------------------------------------------
        #endregion

        #region Metodos
        //-----------------------------------------------------------------------------
        public static List<NPSRating> NpsRatings(int? intUser, DateTime dataInicio, DateTime dataFim) {
            List<NPSRating> lstResult = new List<NPSRating>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString)) {
                con.Open();
                string sql = "SELECT * FROM Sistema.NPSRating a INNER JOIN Cliente.Pedido b ON b.ID = a.PedidoID WHERE a.Nota > 0 AND a.DataCriacao BETWEEN @ini AND @fim";
                lstResult = con.Query<NPSRating, Pedido, NPSRating>(sql, (nps, ped) => { nps.Pedido = ped; return nps; }, new { ini = dataInicio, fim = dataFim }, splitOn: "DataEnvioAvaliacao").ToList();
                if (con.State ==ConnectionState.Open){
                    con.Close();
                }
            }
            
            using (COROASEntities dal = new COROASEntities()){
                dal.ContextOptions.LazyLoadingEnabled = false;
                
                if (intUser.HasValue && intUser.Value > 0) {

                    #region filtros

                    List<int> lstCidades = new List<int>();
                    List<int> lstEstados = new List<int>();
                    List<int> lstCidadesOutros = new List<int>();
                    List<int> lstEstadosOutros = new List<int>();

                    #endregion

                    #region Carteiras

                    //busca as carteiras 
                
                        var carteiras = dal.AdministradorCarteiras
                            .Include(a=>a.Carteira)
                            .Include(a=>a.Carteira.Estadoes)
                            .Include(a => a.Carteira.Cidades)
                            .Include(a => a.Carteira.Cidades.Select(c=>c.Estado))
                            .Where(AdministradorCarteira.CarteiraComVigencia(null)).ToList();
                        //busca as carteiras do usuario
                        if (carteiras.Any(a => a.AdministradorID == intUser)) {
                            List<Carteira> lstCarteiras =
                                carteiras.Where(c => c.AdministradorID == intUser && c.Carteira != null)
                                    .Select(c => c.Carteira)
                                    .ToList();
                            foreach (Carteira carteira in lstCarteiras) {
                                if (carteira.Cidades != null) {
                                    lstCidades.AddRange(carteira.Cidades.Select(c => c.ID));
                                }
                                if (carteira.Estadoes != null) {
                                    lstEstados.AddRange(carteira.Estadoes.Select(e => e.ID));
                                }
                            }
                        }
                        //pegar os dados de carteira que não sejam dele
                        List<Carteira> lstCarteiraOutros =
                            carteiras.Where(a => a.AdministradorID != intUser && a.Carteira != null).Select(c => c.Carteira).ToList();
                        foreach (var carteira in lstCarteiraOutros) {
                            if (carteira.Cidades != null) {
                                lstCidadesOutros.AddRange(carteira.Cidades.Select(c => c.ID));
                            }
                            if (carteira.Estadoes != null) {
                                lstEstadosOutros.AddRange(carteira.Estadoes.Select(e => e.ID));
                            }
                        }

                    #endregion

                    #region Extensão dos filtros
                   
                    Func<NPSRating, bool> exp = p =>
                        lstCidades.Contains(p.Pedido.CidadeID)
                         || (!lstCidades.Contains(p.Pedido.CidadeID) && lstEstados.Contains(p.Pedido.EstadoID) && !lstCidadesOutros.Contains(p.Pedido.CidadeID))
                         || (!lstCidades.Contains(p.Pedido.CidadeID) && !lstEstados.Contains(p.Pedido.EstadoID) && !lstCidadesOutros.Contains(p.Pedido.CidadeID) && !lstEstadosOutros.Contains(p.Pedido.EstadoID));


                    lstResult = lstResult.Where(exp).ToList();
                    //primeiro consulta se ele tem a cidade vinculada atraves da carteira
                    //senão consulta se ele tem o estado vinculado e a cidade do pedido não pertence à outro
                    //caso o pedido não tenha ninguen vinculado, libera pra todos


                    #endregion

                }
            }
                
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<NPSRating> NpsRatingNew(int? intUser, DateTime dataInicio, DateTime dataFim){
            List<NPSRating> lstResult = new List<NPSRating>();
            Stopwatch stpT = new Stopwatch();
            stpT.Start();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString)) {
                con.Open();
                string sql = "exec usp_Dash_NpsCarteiras  @ini = @paramIni, @fim = @paramFim, @idAdm = @paramAdm";
                lstResult = con.Query<NPSRating>(sql, new { paramIni = dataInicio, paramFim = dataFim, paramAdm = intUser }).ToList();
                if (con.State == ConnectionState.Open) {
                    con.Close();
                }
            }
            stpT.Stop();
            string strT = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpT.Elapsed.Minutes, stpT.Elapsed.Seconds, stpT.Elapsed.Milliseconds);

            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static DashBoard CalcularDashPorProcessados(DateTime DataInicial, DateTime DataFinal, int? idUsuarioLogado, int idSistemaOrigem, int Produtos, bool bolApenasCanceladosErroFornecedores = true)
        {
            DashBoard dash = new DashBoard();

            #region Variaveis

            int QtdCoroas = 0;
            int QtdPedidosCancelados = 0;
            decimal totalFaturamento = 0;
            decimal TicketMedio = 0;
            decimal? totalRepasse = 0;
            decimal QtdPedidosBruto = 0;
            decimal NotaNPS = 0;
            int QtdPedidosNPS = 0;

            List<Pedido> LstPedidosBrutaCPV = new List<Pedido>();
            List<Pedido> LstPedidosSemCanceladosCPV = new List<Pedido>();
            List<Pedido> LstPedidosCanceladosCPV = new List<Pedido>();

            List<PedidoEmpresa> LstPedidosBrutaCORP = new List<PedidoEmpresa>();
            List<PedidoEmpresa> LstPedidosSemCanceladosCORP = new List<PedidoEmpresa>();
            List<PedidoEmpresa> LstPedidosCanceladosCORP = new List<PedidoEmpresa>();

            #endregion

            #region PREENCHE LISTAS BRUTAS

            var LstPedidosBruta = GetRelatorioDashFornecedores(DataInicial, DataFinal);

            #region FILTRA USUÁRIO LOGADO

            if (idUsuarioLogado > 0)
            {
                LstPedidosBruta = LstPedidosBruta.Where(r => r.AdministradorID == idUsuarioLogado).ToList();
            }

            #endregion

            #region FILTRA TIPO DE PRODUTO

            // 1 == COROAS
            if (Produtos == 1)
            {
                LstPedidosBruta = LstPedidosBruta.Where(r => r.TipoID == 1).ToList();
            }
            // 8 == KITS BEBE
            if (Produtos == 8)
            {
                LstPedidosBruta = LstPedidosBruta.Where(r => r.TipoID == 8).ToList();
            }
            // 9 == PERSONALIZADO
            if (Produtos == 100)
            {
                LstPedidosBruta = LstPedidosBruta.Where(r => r.TipoID == 100).ToList();
            }

            #endregion

            LstPedidosBrutaCPV = LstPedidosBruta.Where(r => r.TIPO == "CPV").Select(x => new Pedido() { ID = (int)x.PedidoID, ValorTotal = (decimal)x.ValorTotal, StatusCancelamentoID = x.StatusCancelamentoID, DataCriacao = (DateTime)x.DataCriacao, Numero = x.Numero, OrigemSite = x.OrigemSite }).ToList().GroupBy(p => p.ID).Select(g => g.First()).ToList();
            // POPULA ITENS PARA CADA PEIDO DO CPV
            foreach (var Pedido in LstPedidosBrutaCPV)
            {
                List<PedidoItem> ListItensPedido = new List<PedidoItem>();
                var Result = LstPedidosBruta.Where(r => r.PedidoID == Pedido.ID).Select(x => new PedidoItem() { ID = (int)x.ItemPedidoID, Valor = (decimal)x.Valor, PedidoID = (int)x.PedidoID, Repasse = x.ValorRepasse }).ToList();

                foreach (var Iten in Result)
                {
                    if (ListItensPedido.Where(r => r.ID == Iten.ID).Any() == false)
                    {
                        ListItensPedido.Add(Iten);
                    }
                }

                foreach (var ItemPedido in ListItensPedido)
                {
                    Pedido.PedidoItems.Add(new PedidoItem
                    {
                        ID = ItemPedido.ID,
                        Valor = ItemPedido.Valor,
                        PedidoID = ItemPedido.PedidoID,
                        Repasse = ItemPedido.Repasse
                    });
                }
            }

            LstPedidosBrutaCORP = LstPedidosBruta.Where(r => r.TIPO == "CORP").Select(x => new PedidoEmpresa() { ID = (int)x.PedidoID, Valor = (decimal)x.ValorTotal, StatusCancelamentoID = x.StatusCancelamentoID, ValorRepasse = (decimal)x.ValorRepasse, DataCriacao = (DateTime)x.DataCriacao }).ToList().GroupBy(p => p.ID).Select(g => g.First()).ToList();

            #endregion

            #region PREENCHE LISTAS SECUNDARIAS

            LstPedidosSemCanceladosCPV = LstPedidosBrutaCPV.Where(r => r.StatusCancelamentoID == null).ToList();
            LstPedidosCanceladosCPV = LstPedidosBrutaCPV.Where(r => r.StatusCancelamentoID != null).ToList();

            LstPedidosSemCanceladosCORP = LstPedidosBrutaCORP.Where(r => r.StatusCancelamentoID == null).ToList();
            LstPedidosCanceladosCORP = LstPedidosBrutaCORP.Where(r => r.StatusCancelamentoID != null).ToList();

            #endregion

            #region CALCULA METRICAS

            if (idSistemaOrigem == (int)SistemaPedido.AmarAssist)
            {
                LstPedidosSemCanceladosCPV = LstPedidosSemCanceladosCPV.Where(r => r.OrigemSite == "AMAR ASSIST").ToList();
                LstPedidosBrutaCPV = LstPedidosBrutaCPV.Where(r => r.OrigemSite == "AMAR ASSIST").ToList();
            }

            if (idSistemaOrigem == (int)SistemaPedido.Cliente)
            {
                LstPedidosSemCanceladosCPV = LstPedidosSemCanceladosCPV.Where(r => r.OrigemSite != "AMAR ASSIST").ToList();
                LstPedidosBrutaCPV = LstPedidosBrutaCPV.Where(r => r.OrigemSite != "AMAR ASSIST").ToList();
            }

            if (idSistemaOrigem == (int)SistemaPedido.Cliente || idSistemaOrigem == (int)SistemaPedido.AmarAssist)
            {
                QtdCoroas = LstPedidosSemCanceladosCPV.ToList().Sum(r => r.PedidoItems.Count);
                QtdPedidosCancelados = LstPedidosBrutaCPV.ToList().Count - LstPedidosSemCanceladosCPV.ToList().Count;
                QtdPedidosBruto = LstPedidosBrutaCPV.ToList().Count;
                totalFaturamento = LstPedidosSemCanceladosCPV.ToList().Sum(r => r.ValorTotal);
                if(totalFaturamento > 0 && QtdCoroas > 0)
                {
                    TicketMedio = totalFaturamento / QtdCoroas;
                }
                else
                {
                    TicketMedio = 0;
                }
                totalRepasse = LstPedidosSemCanceladosCPV.ToList().Sum(r => r.PedidoItems.Sum(p => p.Repasse));

                var LstPedidos = LstPedidosBruta.Where(r => r.Nota > 0 && r.StatusCancelamentoID == null).Select(m => new
                {
                    m.PedidoID,
                    m.Nota
                }).Distinct().ToList();

                try
                {
                    QtdPedidosNPS = LstPedidos.Count;
                    var PercentPromotoresFornecedor = (decimal)((decimal)LstPedidos.Where(r => r.Nota == 9 || r.Nota == 10).ToList().Count() / QtdPedidosNPS) * 100;
                    var PercentDetratoresFornecedor = (decimal)((decimal)LstPedidos.Where(r => r.Nota > 0 && r.Nota <= 6).ToList().Count() / QtdPedidosNPS) * 100;

                    NotaNPS = PercentPromotoresFornecedor - PercentDetratoresFornecedor;
                }
                catch  {}
               
            }
            else if (idSistemaOrigem == (int)SistemaPedido.Empresa)
            {
                QtdCoroas = LstPedidosSemCanceladosCORP.Count;
                QtdPedidosCancelados = LstPedidosBrutaCORP.Count - LstPedidosSemCanceladosCORP.Count;
                QtdPedidosBruto = LstPedidosBrutaCORP.Count;
                totalFaturamento = LstPedidosSemCanceladosCORP.Sum(r => r.Valor);
                if(totalFaturamento > 0 && QtdCoroas > 0)
                {
                    TicketMedio = totalFaturamento / QtdCoroas;
                }
                else
                {
                    TicketMedio = 0;
                }
                totalRepasse = LstPedidosSemCanceladosCORP.Sum(r => r.ValorRepasse);
            }
            else
            {
                QtdCoroas = LstPedidosSemCanceladosCPV.Sum(r => r.PedidoItems.Count) + LstPedidosSemCanceladosCORP.Count;
                QtdPedidosCancelados = (LstPedidosBrutaCPV.Count - LstPedidosSemCanceladosCPV.Count) + (LstPedidosBrutaCORP.Count - LstPedidosSemCanceladosCORP.Count);
                QtdPedidosBruto = LstPedidosBrutaCPV.Count + LstPedidosBrutaCORP.Count;
                totalFaturamento = LstPedidosSemCanceladosCPV.Sum(r => r.ValorTotal) + LstPedidosBrutaCORP.Sum(r => r.Valor);
                if(totalFaturamento != 0 && QtdCoroas != 0)
                {
                    TicketMedio = totalFaturamento / QtdCoroas;
                }
                totalRepasse = LstPedidosSemCanceladosCPV.Sum(r => r.PedidoItems.Sum(p => p.Repasse)) + LstPedidosSemCanceladosCORP.Sum(r => r.ValorRepasse);
            }

            #endregion

            #region POPULA PROPRIEDADES FINAIS

            dash.QtdPedidos = QtdCoroas;
            dash.QtdPedidosCancelados = QtdPedidosCancelados;
            dash.totalFaturamento = totalFaturamento;
            dash.TicketMedio = TicketMedio;

            dash.ListaPedidosCanceladosCPV = LstPedidosCanceladosCPV;
            dash.ListaPedidosCanceladosCORP = LstPedidosCanceladosCORP;
            dash.totalRepasse = (decimal)totalRepasse;
            dash.QtdPedidosBruto = QtdPedidosBruto;

            dash.NotaNPS = NotaNPS;
            dash.QtdPedidosNPS = QtdPedidosNPS;

            #endregion

            return dash;
        }
        //-----------------------------------------------------------------------------
        private static List<Repasse> ListRepassePorCarteira(DateTime DataIncial, DateTime DataFinal, COROASEntities context, int? intIdAdm) {
            Expression<Func<Repasse, bool>> exp = r =>
                r.StatusID != (int)Repasse.TodosStatus.Cancelado &&
                (r.PedidoItem.Pedido.DataCriacao >= DataIncial && r.PedidoItem.Pedido.DataCriacao <= DataFinal)
                && r.PedidoItem.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado;

            Expression<Func<Repasse, bool>> exp2 = p => p.ID > 0;
            if (intIdAdm.HasValue && intIdAdm.Value > 0){
                exp2 = exp2.And(
                        p =>
                         (lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID)
                          || (!lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID) && lstCarteiraEstados.Contains(p.PedidoItem.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.PedidoItem.Pedido.CidadeID))
                          || (!lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID) && !lstCarteiraEstados.Contains(p.PedidoItem.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.PedidoItem.Pedido.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.PedidoItem.Pedido.EstadoID)))

                    );
            }
            
            var temp = context.Repasses
                .Include(r=>r.PedidoItem)
                .Include(r => r.PedidoItem.Pedido)
                .Where(exp).ToList();
            
            return temp.Where(exp2.Compile()).ToList();
        }
        //-----------------------------------------------------------------------------
        private static List<PedidoItem> ListPedidosItemPorCarteira(DateTime DataIncial, DateTime DataFinal, COROASEntities context,int? intIdAdm){

            List<Pedido> lstPedidos=new List<Pedido>();

            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString)) {
                con.Open();
                string sql = "SELECT ID,CidadeID,EstadoID FROM Cliente.Pedido a WHERE a.DataCriacao BETWEEN @ini AND @fim";
                lstPedidos = con.Query<Pedido>(sql, new { ini = DataIncial, fim = DataFinal }).ToList();
                if (con.State == ConnectionState.Open) {
                    con.Close();
                }
            }

            //Expression<Func<Pedido, bool>> expPedido = p => (p.DataCriacao >= DataIncial && p.DataCriacao <= DataFinal);
            if (intIdAdm.HasValue && intIdAdm.Value > 0) {

                #region Extensão dos filtros
                Func<Pedido,bool> expPedido = p =>
                        (lstCarteiraCidades.Contains(p.CidadeID)
                        || (!lstCarteiraCidades.Contains(p.CidadeID) && lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID))
                        || (!lstCarteiraCidades.Contains(p.CidadeID) && !lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.EstadoID)))
                    ;
                lstPedidos = lstPedidos.Where(expPedido).ToList();
                //primeiro consulta se ele tem a cidade vinculada atraves da carteira
                //senão consulta se ele tem o estado vinculado e a cidade do pedido não pertence à outro
                //caso o pedido não tenha ninguen vinculado, libera pra todos
                //exp = exp.And(exp2);

                #endregion

            }
            
            List<int> lstIdsPedidos = lstPedidos.Select(p => p.ID).ToList();

            Expression<Func<PedidoItem, bool>> exp = pi => lstIdsPedidos.Contains(pi.PedidoID);
            
            var result = context.PedidoItems
                .Include(r => r.Pedido)
                .Include(r => r.Pedido.Cliente)
                .Include(r => r.Pedido.Cidade)
                .Include(r => r.Pedido.Estado)
                .Include(r=>r.Pedido.AdministradorPedidoes)
                .Include(r=>r.Pedido.NPSRatings)
                .Where(exp).ToList();
            return result;
        }
        //-----------------------------------------------------------------------------
        public static void BuscarCarteiraAdm(int? idAdm, out List<Cidade> lstCidades,out List<Estado> lstEstados, out string strCarteiras, out decimal dcmMetaRepasse, out decimal dcmMetaNPS, out decimal dcmMetaIndiceCanc) {
            lstEstados = new List<Estado>();
            lstCidades = new List<Cidade>();
            strCarteiras = string.Empty;
            dcmMetaNPS = dcmMetaRepasse = dcmMetaIndiceCanc = 0;
            using (COROASEntities dal = new COROASEntities()){
                dal.ContextOptions.LazyLoadingEnabled = false;

                var lstAdmCarteira = dal.AdministradorCarteiras
                    .Include(p => p.Carteira)
                    .Include(p => p.Carteira.Estadoes)
                    .Include(p => p.Carteira.Cidades)
                    .Include(p => p.Carteira.Cidades.Select(c=>c.Estado))
                    .Where(AdministradorCarteira.CarteiraComVigencia(idAdm)).ToList();
                foreach (AdministradorCarteira admCarteira in lstAdmCarteira) {
                    foreach (Estado estado in admCarteira.Carteira.Estadoes) {
                        lstEstados.Add(estado);
                    }
                    foreach (Cidade cidade in admCarteira.Carteira.Cidades) {
                        lstCidades.Add(cidade);
                    }
                    strCarteiras += admCarteira.Carteira.Nome + ", ";
                }
                if (lstAdmCarteira.Count == 1){
                    var admCarteira = lstAdmCarteira.FirstOrDefault();
                    if (admCarteira != null && admCarteira.Carteira != null) {
                        dcmMetaRepasse = admCarteira.Carteira.MetaRepasse ?? 0;
                        dcmMetaNPS = admCarteira.Carteira.MetaNPS ?? 0;
                        dcmMetaIndiceCanc = admCarteira.Carteira.MetaIndiceCancelado ?? 0;

                    }
                }
            }
            if (strCarteiras.LastIndexOf(", ") == strCarteiras.Length - 2){
                strCarteiras = strCarteiras.Substring(0, strCarteiras.Length - 2);
            }
        }
        //-----------------------------------------------------------------------------
        public static List<RelatorioAtendente_Result> GetRelatorioVendaAtendente(DateTime dataInicio, DateTime dataFim) {
            List<RelatorioAtendente_Result> lstResult = new List<RelatorioAtendente_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString)) {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.spRelatorioAtendente  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<RelatorioAtendente_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim}, commandTimeout: 600).ToList();
                
                if (con.State == ConnectionState.Open) {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<SpRelatorioDashFornecedores_Result> GetRelatorioDashFornecedores(DateTime dataInicio, DateTime dataFim)
        {
            List<SpRelatorioDashFornecedores_Result> lstResult = new List<SpRelatorioDashFornecedores_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                string sql = "exec sistema.SpRelatorioDashFornecedores  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<SpRelatorioDashFornecedores_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }

        public static List<RelatorioAtendenteTemp_Result> GetRelatorioVendaAtendenteTemp(DateTime dataInicio, DateTime dataFim)
        {
            List<RelatorioAtendenteTemp_Result> lstResult = new List<RelatorioAtendenteTemp_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.spRelatorioAtendenteTempo  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<RelatorioAtendenteTemp_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<spRelatorioFunerarias_Result> GetRelatorioFunerarias(DateTime dataInicio, DateTime dataFim)
        {
            List<spRelatorioFunerarias_Result> lstResult = new List<spRelatorioFunerarias_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.spRelatorioFunerarias  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<spRelatorioFunerarias_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<RelatorioVendasCPVFloricultura_Result> GetRelatorioVendasCPVFloricultura(DateTime dataInicio, DateTime dataFim)
        {
            List<RelatorioVendasCPVFloricultura_Result> lstResult = new List<RelatorioVendasCPVFloricultura_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.RelatorioVendasCPVFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<RelatorioVendasCPVFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<spRelatorioProdutosFloricultura_Result> GetRelatorioProdutosFloricultura(DateTime dataInicio, DateTime dataFim)
        {
            List<spRelatorioProdutosFloricultura_Result> lstResult = new List<spRelatorioProdutosFloricultura_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.spRelatorioProdutosFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<spRelatorioProdutosFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
        public static List<spRelatorioMeioPagamentoFloricultura_Result> GetMeioPagamentoFloricultura(DateTime dataInicio, DateTime dataFim)
        {
            List<spRelatorioMeioPagamentoFloricultura_Result> lstResult = new List<spRelatorioMeioPagamentoFloricultura_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
                string sql = "exec sistema.spRelatorioMeioPagamentoFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
                lstResult = con.Query<spRelatorioMeioPagamentoFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return lstResult;
        }
        #endregion

    }
    //=================================================================================
    public class DashFornecedorNPS{
        public int FornecedorID { get; set; }
        public decimal Media { get; set; }
        public int Pedidos { get; set; }
    }
    //=================================================================================
    public class HelperDapperReturn {
        public int FirstId { get; set; }
        public int SecondId { get; set; }
    }
    //=================================================================================
    public class PedidoResultDash:Pedido{
        public int TipoClienteID { get; set; }
        public int AdministradorID { get; set; }
        public bool NpsEmailEnviado { get; set; }
        public bool AceiteManual { get; set; }
    }
    //=================================================================================
    public class RepasseResult{
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdPedidoItem { get; set; }
        public string OrigemForma { get; set; }
        public string OrigemSite { get; set; }
        public decimal ValorRepasse { get; set; }
    }
    //=================================================================================
}


//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Linq;
//using System.Linq.Expressions;
//using Domain.Entities;
//using Domain.Entities.Generics;
//using Domain.MetodosExtensao;
//using System.Data.Entity;
//using System.Data.SqlClient;
//using System.Diagnostics;
//using System.Text;
//using Dapper;
//using Domain.Entities.ReportsStoredProcs;

//namespace Domain.Service
//{
//    //=================================================================================
//    public static class DashService
//    {

//        #region Variaveis
//        //-----------------------------------------------------------------------------
//        private static List<int> lstCarteiraCidades;
//        private static List<int> lstCarteiraEstados;
//        private static List<int> lstCarteiraCidadesOutros;
//        private static List<int> lstCarteiraEstadosOutros;
//        //-----------------------------------------------------------------------------
//        #endregion

//        #region Metodos
//        //-----------------------------------------------------------------------------
//        public static List<NPSRating> NpsRatings(int? intUser, DateTime dataInicio, DateTime dataFim)
//        {
//            List<NPSRating> lstResult = new List<NPSRating>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                string sql = "SELECT * FROM Sistema.NPSRating a INNER JOIN Cliente.Pedido b ON b.ID = a.PedidoID WHERE a.Nota > 0 AND a.DataCriacao BETWEEN @ini AND @fim";
//                lstResult = con.Query<NPSRating, Pedido, NPSRating>(sql, (nps, ped) => { nps.Pedido = ped; return nps; }, new { ini = dataInicio, fim = dataFim }, splitOn: "DataEnvioAvaliacao").ToList();
//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }

//            using (COROASEntities dal = new COROASEntities())
//            {
//                dal.ContextOptions.LazyLoadingEnabled = false;

//                if (intUser.HasValue && intUser.Value > 0)
//                {

//                    #region filtros

//                    List<int> lstCidades = new List<int>();
//                    List<int> lstEstados = new List<int>();
//                    List<int> lstCidadesOutros = new List<int>();
//                    List<int> lstEstadosOutros = new List<int>();

//                    #endregion

//                    #region Carteiras

//                    //busca as carteiras 

//                    var carteiras = dal.AdministradorCarteiras
//                        .Include(a => a.Carteira)
//                        .Include(a => a.Carteira.Estadoes)
//                        .Include(a => a.Carteira.Cidades)
//                        .Include(a => a.Carteira.Cidades.Select(c => c.Estado))
//                        .Where(AdministradorCarteira.CarteiraComVigencia(null)).ToList();
//                    //busca as carteiras do usuario
//                    if (carteiras.Any(a => a.AdministradorID == intUser))
//                    {
//                        List<Carteira> lstCarteiras =
//                            carteiras.Where(c => c.AdministradorID == intUser && c.Carteira != null)
//                                .Select(c => c.Carteira)
//                                .ToList();
//                        foreach (Carteira carteira in lstCarteiras)
//                        {
//                            if (carteira.Cidades != null)
//                            {
//                                lstCidades.AddRange(carteira.Cidades.Select(c => c.ID));
//                            }
//                            if (carteira.Estadoes != null)
//                            {
//                                lstEstados.AddRange(carteira.Estadoes.Select(e => e.ID));
//                            }
//                        }
//                    }
//                    //pegar os dados de carteira que não sejam dele
//                    List<Carteira> lstCarteiraOutros =
//                        carteiras.Where(a => a.AdministradorID != intUser && a.Carteira != null).Select(c => c.Carteira).ToList();
//                    foreach (var carteira in lstCarteiraOutros)
//                    {
//                        if (carteira.Cidades != null)
//                        {
//                            lstCidadesOutros.AddRange(carteira.Cidades.Select(c => c.ID));
//                        }
//                        if (carteira.Estadoes != null)
//                        {
//                            lstEstadosOutros.AddRange(carteira.Estadoes.Select(e => e.ID));
//                        }
//                    }

//                    #endregion

//                    #region Extensão dos filtros

//                    Func<NPSRating, bool> exp = p =>
//                        lstCidades.Contains(p.Pedido.CidadeID)
//                         || (!lstCidades.Contains(p.Pedido.CidadeID) && lstEstados.Contains(p.Pedido.EstadoID) && !lstCidadesOutros.Contains(p.Pedido.CidadeID))
//                         || (!lstCidades.Contains(p.Pedido.CidadeID) && !lstEstados.Contains(p.Pedido.EstadoID) && !lstCidadesOutros.Contains(p.Pedido.CidadeID) && !lstEstadosOutros.Contains(p.Pedido.EstadoID));


//                    lstResult = lstResult.Where(exp).ToList();
//                    //primeiro consulta se ele tem a cidade vinculada atraves da carteira
//                    //senão consulta se ele tem o estado vinculado e a cidade do pedido não pertence à outro
//                    //caso o pedido não tenha ninguen vinculado, libera pra todos


//                    #endregion

//                }
//            }

//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<NPSRating> NpsRatingNew(int? intUser, DateTime dataInicio, DateTime dataFim)
//        {
//            List<NPSRating> lstResult = new List<NPSRating>();
//            Stopwatch stpT = new Stopwatch();
//            stpT.Start();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                string sql = "exec usp_Dash_NpsCarteiras  @ini = @paramIni, @fim = @paramFim, @idAdm = @paramAdm";
//                lstResult = con.Query<NPSRating>(sql, new { paramIni = dataInicio, paramFim = dataFim, paramAdm = intUser }).ToList();
//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            stpT.Stop();
//            string strT = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpT.Elapsed.Minutes, stpT.Elapsed.Seconds, stpT.Elapsed.Milliseconds);

//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static DashBoard CalcularDashPorCarteira(DateTime DataIncial, DateTime DataFinal, int? idAdm, bool bolApenasCanceladosErroFornecedores = true)
//        {

//            #region Variaveis
//            decimal totalliquido = 0;
//            string str, str1;
//            decimal totalFaturamento = 0;
//            decimal totalRepasseOnline = 0;
//            decimal totalRepasseOffline = 0;
//            decimal totalRepasseOnlineCPV = 0;
//            decimal totalRepasseOfflineCPV = 0;
//            decimal totalRepasseOnlineNET = 0;
//            decimal totalRepasseOfflineNET = 0;
//            decimal totalRepasseOnlineOutros = 0;
//            decimal totalRepasseOfflineOutros = 0;

//            decimal ticketMedio;

//            DashBoard classRetorno = new DashBoard();
//            lstCarteiraCidades = new List<int>();
//            lstCarteiraEstados = new List<int>();
//            lstCarteiraCidadesOutros = new List<int>();
//            lstCarteiraEstadosOutros = new List<int>();
//            #endregion


//            using (COROASEntities dal = new COROASEntities())
//            {
//                dal.ContextOptions.LazyLoadingEnabled = false;
//                dal.CommandTimeout = 600;
//                if (idAdm.HasValue && idAdm.Value > 0)
//                {
//                    #region Carteiras
//                    //busca as carteiras 
//                    var carteiras = dal.AdministradorCarteiras
//                        .Include(a => a.Carteira)
//                        .Include(a => a.Carteira.Estadoes)
//                        .Include(a => a.Carteira.Cidades)
//                        .Include(a => a.Carteira.Cidades.Select(c => c.Estado))
//                        .Where(AdministradorCarteira.CarteiraComVigencia(null)).ToList();
//                    //busca as carteiras do usuario
//                    if (carteiras.Any(a => a.AdministradorID == idAdm.Value))
//                    {
//                        List<Carteira> lstCarteiras = carteiras.Where(c => c.AdministradorID == idAdm.Value && c.Carteira != null).Select(c => c.Carteira).ToList();
//                        foreach (Carteira carteira in lstCarteiras)
//                        {
//                            if (carteira.Cidades != null)
//                            {
//                                lstCarteiraCidades.AddRange(carteira.Cidades.Select(c => c.ID));
//                            }
//                            if (carteira.Estadoes != null)
//                            {
//                                lstCarteiraEstados.AddRange(carteira.Estadoes.Select(e => e.ID));
//                            }
//                        }
//                    }
//                    //pegar os dados de carteira que não sejam dele
//                    List<Carteira> lstCarteiraOutros = carteiras.Where(a => a.AdministradorID != idAdm.Value && a.Carteira != null).Select(c => c.Carteira).ToList();
//                    foreach (var carteira in lstCarteiraOutros)
//                    {
//                        if (carteira.Cidades != null)
//                        {
//                            lstCarteiraCidadesOutros.AddRange(carteira.Cidades.Select(c => c.ID));
//                        }
//                        if (carteira.Estadoes != null)
//                        {
//                            lstCarteiraEstadosOutros.AddRange(carteira.Estadoes.Select(e => e.ID));
//                        }
//                    }
//                    #endregion
//                }

//                Stopwatch stp = new Stopwatch();
//                stp.Start();
//                var lstPedidoItens = ListPedidosItemPorCarteira(DataIncial, DataFinal, dal, idAdm);
//                stp.Stop();
//                str = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);


//                var LstPedidoItemSemCacnelados = lstPedidoItens.Where(r => r.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();
//                var LstPedidoItemCacnelados = lstPedidoItens.Where(r => r.Pedido.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();

//                List<Pedido> lstPedidosSemCancelados = new List<Pedido>();

//                List<Pedido> lstPedidosCancelados = new List<Pedido>();

//                foreach (var item in LstPedidoItemSemCacnelados)
//                {
//                    if (lstPedidosSemCancelados.FirstOrDefault(r => r.ID == item.PedidoID) == null)
//                    {
//                        lstPedidosSemCancelados.Add(item.Pedido);
//                    }
//                }

//                foreach (var item in LstPedidoItemCacnelados)
//                {
//                    if (lstPedidosCancelados.FirstOrDefault(r => r.ID == item.PedidoID) == null)
//                    {
//                        lstPedidosCancelados.Add(item.Pedido);
//                    }
//                }
//                totalFaturamento = lstPedidosSemCancelados.Sum(p => p.ValorTotal);
//                //string temp = string.Empty;
//                //foreach (var Item in LstPedidoItemSemCacnelados) {
//                //    temp += "<tr><td>" + Item.Pedido.ValorRepasse + "</td>" + "<td>" + Item.Pedido.Numero + "</td></tr>";
//                //}

//                //temp = "<table>" + temp + "</table>";

//                foreach (var PedidoItem in LstPedidoItemSemCacnelados)
//                {
//                    if (PedidoItem.Valor > (decimal)0.01)
//                    {
//                        if (PedidoItem.Pedido.ValorTotal > 0)
//                        {
//                            totalliquido += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
//                        }
//                        else {
//                            totalliquido += PedidoItem.Valor;
//                        }
//                    }
//                    else {
//                        totalliquido += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
//                    }
//                }
//                stp.Restart();
//                var lstRepasse = ListRepassePorCarteira(DataIncial, DataFinal, dal, idAdm);
//                stp.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp.Elapsed.Minutes, stp.Elapsed.Seconds, stp.Elapsed.Milliseconds);


//                foreach (var Item in lstRepasse)
//                {
//                    if (Item.PedidoItem.Pedido.OrigemForma.Trim() == "ONLINE")
//                    {
//                        totalRepasseOnline += Item.ValorRepasse;

//                        switch (Item.PedidoItem.Pedido.OrigemSite.Trim())
//                        {
//                            case "COROAS PARA VELÓRIO":
//                                totalRepasseOnlineCPV += Item.ValorRepasse;
//                                break;

//                            case "COROAS NET":
//                                totalRepasseOnlineNET += Item.ValorRepasse;
//                                break;

//                            default:
//                                totalRepasseOnlineOutros += Item.ValorRepasse;
//                                break;
//                        }

//                    }
//                    else {
//                        totalRepasseOffline += Item.ValorRepasse;

//                        switch (Item.PedidoItem.Pedido.OrigemSite.Trim())
//                        {
//                            case "COROAS PARA VELÓRIO":
//                                totalRepasseOfflineCPV += Item.ValorRepasse;
//                                break;

//                            case "COROAS NET":
//                                totalRepasseOfflineNET += Item.ValorRepasse;
//                                break;

//                            default:
//                                totalRepasseOfflineOutros += Item.ValorRepasse;
//                                break;
//                        }
//                    }
//                }


//                var TotalItensPedidoPJ = LstPedidoItemSemCacnelados.Count(r => r.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica);
//                var TotalItensPedidoPF = LstPedidoItemSemCacnelados.Count(r => r.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaFisica);

//                if (totalliquido != 0 && TotalItensPedidoPJ != 00 && TotalItensPedidoPF != 0)
//                {
//                    ticketMedio = (totalliquido) / (TotalItensPedidoPJ + TotalItensPedidoPF);
//                }

//                else {
//                    ticketMedio = 0;
//                }

//                #region Retorno
//                classRetorno.totalliquido = totalliquido;
//                classRetorno.totalFaturamento = totalFaturamento;
//                classRetorno.totalRepasseOnline = totalRepasseOnline;
//                classRetorno.totalRepasseOffline = totalRepasseOffline;
//                classRetorno.totalRepasseOnlineCPV = totalRepasseOnlineCPV;
//                classRetorno.totalRepasseOfflineCPV = totalRepasseOfflineCPV;
//                classRetorno.totalRepasseOnlineNET = totalRepasseOnlineNET;
//                classRetorno.totalRepasseOfflineNET = totalRepasseOfflineNET;
//                classRetorno.totalRepasseOnlineOutros = totalRepasseOnlineOutros;
//                classRetorno.totalRepasseOfflineOutros = totalRepasseOfflineOutros;
//                classRetorno.totalRepasse = totalRepasseOnline + totalRepasseOffline;

//                classRetorno.TotalItensPedidoPJ = TotalItensPedidoPJ;
//                classRetorno.TotalItensPedidoPF = TotalItensPedidoPF;

//                classRetorno.TotalItensPedido = TotalItensPedidoPJ + TotalItensPedidoPF;

//                classRetorno.TicketMedio = ticketMedio;


//                classRetorno.QtdPedidos = lstPedidosSemCancelados.ToList().Count();

//                classRetorno.QtdPedidosBruto = lstPedidosSemCancelados.ToList().Count() + lstPedidosCancelados.ToList().Count();

//                classRetorno.QtdPedidosComAceiteFornecedor = lstPedidosSemCancelados.Count(p => p.FornecedorEmProcessamento);

//                classRetorno.QtdPedidosComAceiteManual = lstPedidosSemCancelados.Count(r => r.AdministradorPedido.Any(p => p.AcaoID == 21) && r.FornecedorEmProcessamento);
//                classRetorno.ListaPedidosComAceiteManual = lstPedidosSemCancelados.Where(r => r.AdministradorPedido.Any(p => p.AcaoID == 21) && r.FornecedorEmProcessamento).ToList();

//                if (bolApenasCanceladosErroFornecedores)
//                {
//                    classRetorno.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID == (int)TodosStatusCancelamento.ErroEquipeFornecedores);
//                    classRetorno.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID == (int)TodosStatusCancelamento.ErroEquipeFornecedores).ToList();
//                }
//                else {
//                    classRetorno.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste);
//                    classRetorno.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste).ToList();
//                }



//                classRetorno.QtdMailConf = lstPedidosSemCancelados.Count(p => p.NPSRatings.Any(r => r.EmailEnviado == true));
//                #endregion

//            }

//            return classRetorno;
//        }
//        //-----------------------------------------------------------------------------
//        public static DashBoard CalcularDashPorCarteiraNew(DateTime DataIncial, DateTime DataFinal, int? idAdm, bool bolApenasCanceladosErroFornecedores = true)
//        {
//            DashBoard dash = new DashBoard();

//            #region Variaveis
//            decimal totalliquido = 0;
//            decimal totalFaturamento = 0;
//            decimal totalRepasseOnline = 0;
//            decimal totalRepasseOffline = 0;
//            decimal totalRepasseOnlineCPV = 0;
//            decimal totalRepasseOfflineCPV = 0;
//            decimal totalRepasseOnlineNET = 0;
//            decimal totalRepasseOfflineNET = 0;
//            decimal totalRepasseOnlineOutros = 0;
//            decimal totalRepasseOfflineOutros = 0;
//            decimal ticketMedio;
//            List<Pedido> lstPedidos = new List<Pedido>();
//            List<PedidoItem> lstPedidoItems = new List<PedidoItem>();
//            lstCarteiraCidades = new List<int>();
//            lstCarteiraEstados = new List<int>();
//            lstCarteiraCidadesOutros = new List<int>();
//            lstCarteiraEstadosOutros = new List<int>();
//            string str1 = string.Empty;
//            List<Repasse> lstRepasse;
//            List<AdministradorPedido> lstAdmPedidos;
//            List<Pedido> lstPedidosRating;
//            List<Cliente> lstClientes;
//            #endregion

//            Stopwatch stpTotal = new Stopwatch();
//            stpTotal.Start();
//            Stopwatch stpParcial = Stopwatch.StartNew();
//            #region Consultas
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();

//                string sqlPedidos = "SELECT * FROM Cliente.Pedido a WHERE a.DataCriacao BETWEEN @ini AND @fim";
//                lstPedidos = con.Query<Pedido>(sqlPedidos, new { ini = DataIncial, fim = DataFinal }).ToList();
//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();
//                #region Filtro de Carteiras 

//                if (idAdm.HasValue && idAdm.Value > 0)
//                {
//                    string strSqlCidades = "SELECT a.AdministradorID as 'FirstId', c.IdCidade as 'SecondId' ";
//                    strSqlCidades += " FROM Sistema.AdministradorCarteira a";
//                    strSqlCidades += " INNER JOIN dbo.Carteira b ON b.Id = a.IdCarteira";
//                    strSqlCidades += " INNER JOIN dbo.CarteiraCidade c ON c.IdCarteira = b.Id ";
//                    strSqlCidades += " WHERE a.DataInicio <= GETDATE() AND (a.DataTermino IS NULL OR a.DataTermino >= GETDATE())";

//                    string strSqlEstados = "SELECT a.AdministradorID as 'FirstId', c.IdEstado as 'SecondId'";
//                    strSqlEstados += " FROM Sistema.AdministradorCarteira a";
//                    strSqlEstados += " INNER JOIN dbo.Carteira b ON b.Id = a.IdCarteira";
//                    strSqlEstados += " INNER JOIN dbo.CarteiraEstado c ON c.IdCarteira = b.Id";
//                    strSqlEstados += " WHERE a.DataInicio <= GETDATE() AND (a.DataTermino IS NULL OR a.DataTermino >= GETDATE())";

//                    var resultCidades = con.Query<HelperDapperReturn>(strSqlCidades).ToList();
//                    var resultEstados = con.Query<HelperDapperReturn>(strSqlEstados).ToList();

//                    lstCarteiraCidades.AddRange(resultCidades.Where(r => r.FirstId == idAdm.Value).Distinct().Select(c => c.SecondId));
//                    lstCarteiraCidadesOutros.AddRange(resultCidades.Where(r => r.FirstId != idAdm.Value).Distinct().Select(c => c.SecondId));

//                    lstCarteiraEstados.AddRange(resultEstados.Where(r => r.FirstId == idAdm.Value).Distinct().Select(c => c.SecondId));
//                    lstCarteiraEstadosOutros.AddRange(resultEstados.Where(r => r.FirstId != idAdm.Value).Distinct().Select(c => c.SecondId));

//                    #region Extensão dos filtros
//                    Func<Pedido, bool> expPedido = p =>
//                             (lstCarteiraCidades.Contains(p.CidadeID)
//                             || (!lstCarteiraCidades.Contains(p.CidadeID) && lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID))
//                             || (!lstCarteiraCidades.Contains(p.CidadeID) && !lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.EstadoID)))
//                        ;
//                    lstPedidos = lstPedidos.Where(expPedido).ToList();
//                    #endregion


//                }

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                #endregion

//                string sqlPedidosItens = "SELECT * FROM Cliente.PedidoItem a";
//                sqlPedidosItens += " INNER JOIN Cliente.Pedido b ON b.ID = a.PedidoID";
//                sqlPedidosItens += " WHERE b.DataCriacao BETWEEN @ini AND @fim";
//                lstPedidoItems = con.Query<PedidoItem>(sqlPedidosItens, new { ini = DataIncial, fim = DataFinal }).ToList();

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                string strRepasse = "SELECT a.* FROM Fornecedor.Repasses a";
//                strRepasse += " INNER JOIN Cliente.PedidoItem b ON b.ID = a.PedidoItemID";
//                strRepasse += " INNER JOIN Cliente.Pedido c ON c.ID = b.PedidoID";
//                strRepasse += " WHERE a.StatusID <> 3 and  c.DataCriacao BETWEEN @ini AND @fim";
//                lstRepasse = con.Query<Repasse>(strRepasse, new { ini = DataIncial, fim = DataFinal }).ToList();

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                string strAdmsPedidos = "SELECT a.* FROM Sistema.AdministradorPedido a";
//                strAdmsPedidos += " INNER JOIN Cliente.Pedido b ON b.ID = a.PedidoID";
//                strAdmsPedidos += " WHERE a.AcaoID = 21 and b.DataCriacao BETWEEN @ini AND @fim";
//                lstAdmPedidos = con.Query<AdministradorPedido>(strAdmsPedidos, new { ini = DataIncial, fim = DataFinal }).ToList();

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                string strPedidosRating = "SELECT DISTINCT PedidoID FROM Sistema.NPSRating a";
//                strPedidosRating += " WHERE a.EmailEnviado = 1";
//                lstPedidosRating = con.Query<Pedido>(strPedidosRating, new { ini = DataIncial, fim = DataFinal }).ToList();

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                string strClentes = "SELECT a.ID,a.TipoID FROM Cliente.Cliente a";
//                //strClentes += " INNER JOIN Cliente.Pedido b ON b.ClienteID = a.ID";
//                //strClentes += " WHERE b.DataCriacao BETWEEN @ini AND @fim";
//                lstClientes = con.Query<Cliente>(strClentes).ToList();

//                stpParcial.Stop();
//                str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//                stpParcial.Restart();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            #endregion

//            List<Pedido> lstPedidosCancelados = lstPedidos.Where(p => p.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();
//            List<Pedido> lstPedidosNaoCancelados = lstPedidos.Where(p => p.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();
//            List<PedidoItem> lstPedidosItensNaoCancelados = lstPedidoItems.Where(i => !lstPedidosCancelados.Select(p => p.ID).Contains(i.PedidoID)).ToList();

//            totalFaturamento = lstPedidosNaoCancelados.Sum(p => p.ValorTotal);

//            stpParcial.Stop();
//            str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//            stpParcial.Restart();

//            #region Total Liquido
//            foreach (var item in lstPedidosItensNaoCancelados)
//            {
//                var pedido = lstPedidosNaoCancelados.FirstOrDefault(p => p.ID == item.PedidoID);
//                if (item.Valor > 0.01M)
//                {
//                    if (pedido.ValorTotal > 0)
//                    {
//                        totalliquido += item.Valor - (item.Valor / (pedido.ValorTotal + pedido.ValorDesconto) * pedido.ValorDesconto);
//                    }
//                    else {
//                        totalliquido += item.Valor;
//                    }
//                }
//                else {
//                    totalliquido += pedido.ValorTotal - pedido.ValorDesconto;
//                }
//            }
//            #endregion

//            stpParcial.Stop();
//            str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//            stpParcial.Restart();

//            #region Repasse
//            foreach (var repasse in lstRepasse)
//            {
//                var item = lstPedidoItems.FirstOrDefault(i => i.ID == repasse.PedidoItemID);
//                if (item != null)
//                {
//                    var pedido = lstPedidos.FirstOrDefault(p => p.ID == item.PedidoID);
//                    if (pedido != null && pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado)
//                    {
//                        if (pedido.OrigemForma.Trim() == "ONLINE")
//                        {
//                            #region Online
//                            totalRepasseOnline += repasse.ValorRepasse;

//                            switch (pedido.OrigemSite.Trim())
//                            {
//                                case "COROAS PARA VELÓRIO":
//                                    totalRepasseOnlineCPV += repasse.ValorRepasse;
//                                    break;

//                                case "COROAS NET":
//                                    totalRepasseOnlineNET += repasse.ValorRepasse;
//                                    break;

//                                default:
//                                    totalRepasseOnlineOutros += repasse.ValorRepasse;
//                                    break;
//                            }
//                            #endregion
//                        }
//                        else {
//                            #region Offline
//                            totalRepasseOffline += repasse.ValorRepasse;

//                            switch (pedido.OrigemSite.Trim())
//                            {
//                                case "COROAS PARA VELÓRIO":
//                                    totalRepasseOfflineCPV += repasse.ValorRepasse;
//                                    break;

//                                case "COROAS NET":
//                                    totalRepasseOfflineNET += repasse.ValorRepasse;
//                                    break;

//                                default:
//                                    totalRepasseOfflineOutros += repasse.ValorRepasse;
//                                    break;
//                            }
//                            #endregion
//                        }
//                    }
//                }
//            }
//            #endregion

//            stpParcial.Stop();
//            str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//            stpParcial.Restart();

//            var TotalItensPedidoPJ = lstPedidosItensNaoCancelados.Count(i => lstPedidosNaoCancelados.OnlyPj(lstClientes).Select(p => p.ID).Contains(i.ID));
//            var TotalItensPedidoPF = lstPedidosItensNaoCancelados.Count(i => lstPedidosNaoCancelados.OnlyPf(lstClientes).Select(p => p.ID).Contains(i.ID));

//            stpParcial.Stop();
//            str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpParcial.Elapsed.Minutes, stpParcial.Elapsed.Seconds, stpParcial.Elapsed.Milliseconds);
//            stpParcial.Restart();

//            if (totalliquido != 0 && TotalItensPedidoPJ != 00 && TotalItensPedidoPF != 0)
//            {
//                ticketMedio = (totalliquido) / (TotalItensPedidoPJ + TotalItensPedidoPF);
//            }
//            else {
//                ticketMedio = 0;
//            }

//            #region Retorno
//            dash.totalliquido = totalliquido;
//            dash.totalFaturamento = totalFaturamento;
//            dash.totalRepasseOnline = totalRepasseOnline;
//            dash.totalRepasseOffline = totalRepasseOffline;
//            dash.totalRepasseOnlineCPV = totalRepasseOnlineCPV;
//            dash.totalRepasseOfflineCPV = totalRepasseOfflineCPV;
//            dash.totalRepasseOnlineNET = totalRepasseOnlineNET;
//            dash.totalRepasseOfflineNET = totalRepasseOfflineNET;
//            dash.totalRepasseOnlineOutros = totalRepasseOnlineOutros;
//            dash.totalRepasseOfflineOutros = totalRepasseOfflineOutros;
//            dash.totalRepasse = totalRepasseOnline + totalRepasseOffline;
//            dash.TotalItensPedidoPJ = TotalItensPedidoPJ;
//            dash.TotalItensPedidoPF = TotalItensPedidoPF;
//            dash.TotalItensPedido = TotalItensPedidoPJ + TotalItensPedidoPF;
//            dash.TicketMedio = ticketMedio;
//            dash.QtdPedidos = lstPedidosNaoCancelados.Count;
//            dash.QtdPedidosBruto = lstPedidosNaoCancelados.Count + lstPedidosCancelados.Count;
//            dash.QtdPedidosComAceiteFornecedor = lstPedidosNaoCancelados.Count(p => p.FornecedorEmProcessamento);

//            dash.QtdPedidosComAceiteManual = lstPedidosNaoCancelados.Count(r => lstAdmPedidos.Any(a => a.Pedido.ID == r.ID) && r.FornecedorEmProcessamento);
//            dash.ListaPedidosComAceiteManual = lstPedidosNaoCancelados.Where(r => lstAdmPedidos.Any(a => a.Pedido.ID == r.ID) && r.FornecedorEmProcessamento).ToList();

//            if (bolApenasCanceladosErroFornecedores)
//            {
//                dash.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID == (int)TodosStatusCancelamento.ErroEquipeFornecedores);
//                dash.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID == (int)TodosStatusCancelamento.ErroEquipeFornecedores).ToList();
//            }
//            else {
//                dash.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste);
//                dash.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste).ToList();
//            }

//            dash.QtdMailConf = lstPedidosNaoCancelados.Count(p => lstPedidosRating.Any(r => r.ID == p.ID));
//            #endregion

//            stpTotal.Stop();
//            string strT = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stpTotal.Elapsed.Minutes, stpTotal.Elapsed.Seconds, stpTotal.Elapsed.Milliseconds);


//            return dash;
//        }
//        //-----------------------------------------------------------------------------
//        //Melhor relação de desempenho:CalcularDashPorCarteiraNew2
//        public static DashBoard CalcularDashPorCarteiraNew2(DateTime DataInicial, DateTime DataFinal, int? idUsuarioLogado, int idSistemaOrigem, bool bolApenasCanceladosErroFornecedores = true)
//        {
//            DashBoard dash = new DashBoard();

//            #region Variaveis
//            decimal totalliquido = 0;
//            decimal totalFaturamento = 0;
//            decimal totalRepasseOnline = 0;
//            decimal totalRepasseOffline = 0;
//            decimal totalRepasseOnlineCPV = 0;
//            decimal totalRepasseOfflineCPV = 0;
//            decimal totalRepasseOnlineNET = 0;
//            decimal totalRepasseOfflineNET = 0;
//            decimal totalRepasseOnlineOutros = 0;
//            decimal totalRepasseOfflineOutros = 0;
//            decimal ticketMedio;

//            lstCarteiraCidades = new List<int>();
//            lstCarteiraEstados = new List<int>();
//            lstCarteiraCidadesOutros = new List<int>();
//            lstCarteiraEstadosOutros = new List<int>();
//            string str1 = string.Empty;

//            List<PedidoItem> lstPedidosItens = new List<PedidoItem>();
//            List<PedidoItem> lstPedidoItemSemCancelados = new List<PedidoItem>();
//            List<PedidoItem> lstPedidoItemAutomaticos = new List<PedidoItem>();
//            List<PedidoItem> lstPedidoItemCancelados = new List<PedidoItem>();
//            List<Pedido> lstTotPedidos = new List<Pedido>();
//            List<Pedido> lstPedidosSemCancelados = new List<Pedido>();
//            List<Pedido> lstPedidosCancelados = new List<Pedido>();
//            List<Pedido> lstPedidoAutomaticos = new List<Pedido>();
//            List<Pedido> lstPedidoAceiteManual = new List<Pedido>();

//            IEnumerable<Pedido> lstPedidosDash;
//            // var lstPedidosDash = null;
//            #endregion

//            Stopwatch stp1 = Stopwatch.StartNew();

//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();

//                #region Sql
//                string sql = "";

//                switch (idSistemaOrigem)
//                {
//                    case (int)SistemaPedido.Cliente:
//                        sql = "exec usp_PedidosCliente_DashService  @ini = @paramIni, @fim = @paramFim";
//                        break;
//                    case (int)SistemaPedido.Empresa:
//                        sql = "exec usp_PedidosEmpresa_DashService  @ini = @paramIni, @fim = @paramFim";
//                        break;
//                    default:
//                        sql = "exec usp_PedidosClienteEmpresa_DashService  @ini = @paramIni, @fim = @paramFim";
//                        break;
//                }

//                #region MANUPILA PROC

//                dynamic LstObject = con.Query<dynamic>
//                   (
//                       sql,
//                       new { paramIni = DataInicial, paramFim = DataFinal }, commandTimeout: 600
//                   ).ToList();

//                foreach (var Item in LstObject)
//                {
//                    IDictionary<string, object> Dynamic;
//                    Dynamic = Item;

//                    int? StatucCanID = 0;
//                    int AcaoID = 0;
//                    //bool NpsEmailEnviado = false;
//                    //bool AceiteManual = false;

//                    if (Dynamic.Where(r => r.Key == "StatusCancelamentoID").First().Value != null)
//                        StatucCanID = (int)Dynamic.Where(r => r.Key == "StatusCancelamentoID").First().Value;

//                    if (Dynamic.Where(r => r.Key == "AcaoID").First().Value != null)
//                        AcaoID = (int)Dynamic.Where(r => r.Key == "AcaoID").First().Value;

//                    lstTotPedidos.Add(new Pedido
//                    {
//                        ID = (int)Dynamic.Where(r => r.Key == "ID").First().Value,
//                        AcaoID = AcaoID,
//                        StatusProcessamentoID = (int)Dynamic.Where(r => r.Key == "StatusProcessamentoID").First().Value,
//                        DataCriacao = (DateTime)Dynamic.Where(r => r.Key == "DataCriacao").First().Value,
//                        EstadoID = (int)Dynamic.Where(r => r.Key == "EstadoID").First().Value,
//                        CidadeID = (int)Dynamic.Where(r => r.Key == "CidadeID").First().Value,
//                        ClienteID = (int)Dynamic.Where(r => r.Key == "ClienteID").First().Value,
//                        ValorTotal = (decimal)Dynamic.Where(r => r.Key == "ValorTotal").First().Value,
//                        ValorDesconto = (decimal)Dynamic.Where(r => r.Key == "ValorDesconto").First().Value,
//                        Numero = (string)Dynamic.Where(r => r.Key == "Numero").First().Value,
//                        StatusCancelamentoID = StatucCanID,
//                        FornecedorEmProcessamento = (bool)Dynamic.Where(r => r.Key == "FornecedorEmProcessamento").First().Value,
//                    });

//                    lstPedidosItens.Add(new PedidoItem
//                    {
//                        Valor = (decimal)Dynamic.Where(r => r.Key == "Valor").First().Value,
//                        PedidoID = (int)Dynamic.Where(r => r.Key == "PedidoID").First().Value,
//                        Repasse = (Dynamic.Where(r => r.Key == "ValorRepasse").First().Value == null) ? 0 : (decimal)Dynamic.Where(r => r.Key == "ValorRepasse").First().Value,
//                        Pedido = new PedidoResultDash
//                        {
//                            StatusProcessamentoID = (int)Dynamic.Where(r => r.Key == "StatusProcessamentoID").First().Value,
//                            Cliente = new Cliente
//                            {
//                                TipoID = (int)Dynamic.Where(r => r.Key == "TipoClienteId").First().Value
//                            }
//                        }
//                    });
//                }

//                #endregion

//                #endregion

//                #region Filtro de Carteiras 

//                if (idUsuarioLogado.HasValue && idUsuarioLogado.Value > 0)
//                {
//                    string strSqlCidades = "SELECT a.AdministradorID as 'FirstId', c.IdCidade as 'SecondId' ";
//                    strSqlCidades += " FROM Sistema.AdministradorCarteira a";
//                    strSqlCidades += " INNER JOIN dbo.Carteira b ON b.Id = a.IdCarteira";
//                    strSqlCidades += " INNER JOIN dbo.CarteiraCidade c ON c.IdCarteira = b.Id ";
//                    strSqlCidades += " WHERE a.DataInicio <= GETDATE() AND (a.DataTermino IS NULL OR a.DataTermino >= GETDATE())";

//                    string strSqlEstados = "SELECT a.AdministradorID as 'FirstId', c.IdEstado as 'SecondId'";
//                    strSqlEstados += " FROM Sistema.AdministradorCarteira a";
//                    strSqlEstados += " INNER JOIN dbo.Carteira b ON b.Id = a.IdCarteira";
//                    strSqlEstados += " INNER JOIN dbo.CarteiraEstado c ON c.IdCarteira = b.Id";
//                    strSqlEstados += " WHERE a.DataInicio <= GETDATE() AND (a.DataTermino IS NULL OR a.DataTermino >= GETDATE())";

//                    var resultCidades = con.Query<HelperDapperReturn>(strSqlCidades).ToList();
//                    var resultEstados = con.Query<HelperDapperReturn>(strSqlEstados).ToList();

//                    lstCarteiraCidades.AddRange(resultCidades.Where(r => r.FirstId == idUsuarioLogado.Value).Distinct().Select(c => c.SecondId));
//                    lstCarteiraCidadesOutros.AddRange(resultCidades.Where(r => r.FirstId != idUsuarioLogado.Value).Distinct().Select(c => c.SecondId));

//                    lstCarteiraEstados.AddRange(resultEstados.Where(r => r.FirstId == idUsuarioLogado.Value).Distinct().Select(c => c.SecondId));
//                    lstCarteiraEstadosOutros.AddRange(resultEstados.Where(r => r.FirstId != idUsuarioLogado.Value).Distinct().Select(c => c.SecondId));

//                    #region Extensão dos filtros
//                    Func<PedidoItem, bool> expPedidoItem = p =>
//                             (lstCarteiraCidades.Contains(p.Pedido.CidadeID)
//                             || (!lstCarteiraCidades.Contains(p.Pedido.CidadeID) && lstCarteiraEstados.Contains(p.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.Pedido.CidadeID))
//                             || (!lstCarteiraCidades.Contains(p.Pedido.CidadeID) && !lstCarteiraEstados.Contains(p.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.Pedido.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.Pedido.EstadoID)));
//                    lstPedidosItens = lstPedidosItens.Where(expPedidoItem).ToList();
//                    #endregion


//                }

//                #endregion

//                #region Manipula Dados

//                lstPedidosDash = lstTotPedidos.Where(p => p.Numero != null).GroupBy(p => p.Numero).Select(grp => grp.First());
//                lstPedidoAutomaticos = lstTotPedidos.Where(r => r.AcaoID == (int)AdministradorPedido.Acao.AceitouEntrega).ToList();
//                lstPedidoAceiteManual = lstTotPedidos.Where(r => r.AcaoID == (int)AdministradorPedido.Acao.AceiteManual).ToList();


//                lstPedidoItemSemCancelados = lstPedidosItens.Where(r => r.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();
//                lstPedidoItemCancelados = lstPedidosItens.Where(r => r.Pedido.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Cancelado).ToList();

//                foreach (var item in lstPedidoItemSemCancelados)
//                {
//                    if (lstPedidosSemCancelados.FirstOrDefault(r => r.ID == item.PedidoID) == null)
//                    {
//                        lstPedidosSemCancelados.Add(item.Pedido);
//                    }
//                }
//                foreach (var item in lstPedidoItemCancelados)
//                {
//                    if (lstPedidosCancelados.FirstOrDefault(r => r.ID == item.PedidoID) == null)
//                    {
//                        lstPedidosCancelados.Add(item.Pedido);
//                    }
//                }

//                foreach (var PedidoItem in lstPedidoItemSemCancelados)
//                {
//                    if (PedidoItem.Valor > (decimal)0.00)
//                    {
//                        if (PedidoItem.Pedido.ValorTotal > 0)
//                        {
//                            totalliquido += PedidoItem.Valor - (PedidoItem.Valor / (PedidoItem.Pedido.ValorTotal + PedidoItem.Pedido.ValorDesconto) * PedidoItem.Pedido.ValorDesconto);
//                        }
//                        else {
//                            totalliquido += PedidoItem.Valor;
//                        }
//                    }
//                    else {
//                        totalliquido += PedidoItem.Pedido.ValorTotal - PedidoItem.Pedido.ValorDesconto;
//                    }
//                }
//                string sqlRepasse = "SELECT a.ID, c.id as IdPedido,b.ID as IdPedidoItem, c.OrigemForma, c.OrigemSite, a.ValorRepasse ";
//                sqlRepasse += " FROM Fornecedor.Repasses a";
//                sqlRepasse += " INNER JOIN Cliente.PedidoItem b ON b.ID = a.PedidoItemID";
//                sqlRepasse += " INNER JOIN Cliente.Pedido c ON c.ID = b.PedidoID";
//                sqlRepasse += " WHERE a.StatusID <> 3 AND c.StatusProcessamentoID <> 4 AND c.DataCriacao BETWEEN @ini AND @fim";
//                var lstRepasse = con.Query<RepasseResult>(sqlRepasse, new { ini = DataInicial, fim = DataFinal }, commandTimeout: 600).ToList();
//                int[] idsPedidos = lstPedidoItemSemCancelados.Select(p => p.PedidoID).Distinct().ToArray();
//                lstRepasse = lstRepasse.Where(r => idsPedidos.Contains(r.IdPedido)).ToList();

//                foreach (var Item in lstRepasse)
//                {
//                    if (Item.OrigemForma.Trim() == "ONLINE")
//                    {
//                        totalRepasseOnline += Item.ValorRepasse;
//                        switch (Item.OrigemSite.Trim())
//                        {
//                            case "COROAS PARA VELÓRIO":
//                                totalRepasseOnlineCPV += Item.ValorRepasse;
//                                break;
//                            case "COROAS NET":
//                                totalRepasseOnlineNET += Item.ValorRepasse;
//                                break;
//                            default:
//                                totalRepasseOnlineOutros += Item.ValorRepasse;
//                                break;
//                        }
//                    }
//                    else {
//                        totalRepasseOffline += Item.ValorRepasse;
//                        switch (Item.OrigemSite.Trim())
//                        {
//                            case "COROAS PARA VELÓRIO":
//                                totalRepasseOfflineCPV += Item.ValorRepasse;
//                                break;
//                            case "COROAS NET":
//                                totalRepasseOfflineNET += Item.ValorRepasse;
//                                break;
//                            default:
//                                totalRepasseOfflineOutros += Item.ValorRepasse;
//                                break;
//                        }
//                    }
//                }

//                #endregion

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }

//            #region Retorno
//            var TotalItensPedidoPJ = lstPedidoItemSemCancelados.Count(r => r.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaJuridica);
//            var TotalItensPedidoPF = lstPedidoItemSemCancelados.Count(r => r.Pedido.Cliente.TipoID == (int)Cliente.Tipos.PessoaFisica);

//            if (totalliquido != 0 && TotalItensPedidoPJ != 00 && TotalItensPedidoPF != 0)
//            {
//                ticketMedio = (totalliquido) / (TotalItensPedidoPJ + TotalItensPedidoPF);
//            }

//            else {
//                ticketMedio = 0;
//            }

//            totalFaturamento = lstPedidosSemCancelados.Sum(p => p.ValorTotal);
//            dash.totalliquido = totalliquido;
//            dash.totalFaturamento = totalFaturamento;
//            dash.totalRepasseOnline = totalRepasseOnline;
//            dash.totalRepasseOffline = totalRepasseOffline;
//            dash.totalRepasseOnlineCPV = totalRepasseOnlineCPV;
//            dash.totalRepasseOfflineCPV = totalRepasseOfflineCPV;
//            dash.totalRepasseOnlineNET = totalRepasseOnlineNET;
//            dash.totalRepasseOfflineNET = totalRepasseOfflineNET;
//            dash.totalRepasseOnlineOutros = totalRepasseOnlineOutros;
//            dash.totalRepasseOfflineOutros = totalRepasseOfflineOutros;
//            dash.totalRepasse = totalRepasseOnline + totalRepasseOffline;
//            dash.TotalItensPedidoPJ = TotalItensPedidoPJ;
//            dash.TotalItensPedidoPF = TotalItensPedidoPF;
//            dash.TotalItensPedido = TotalItensPedidoPJ + TotalItensPedidoPF;
//            dash.TicketMedio = ticketMedio;
//            //dash.QtdPedidos = lstPedidosSemCancelados.ToList().Count();
//            dash.QtdPedidos = lstPedidosDash.ToList().Count();
//            dash.QtdPedidosBruto = lstPedidosSemCancelados.ToList().Count() + lstPedidosCancelados.ToList().Count();
//            dash.QtdPedidosComAceiteFornecedor = lstPedidosSemCancelados.Count(p => p.FornecedorEmProcessamento);

//            dash.QtdPedidosAutomatizados = lstPedidoAutomaticos.Count();
//            // dash.ListaPedidosComAceiteManual = lstPedidosSemCancelados.Where(r => ((PedidoResultDash)r).AceiteManual = false).ToList();

//            dash.QtdPedidosComAceiteManual = lstPedidosSemCancelados.Count(r => ((PedidoResultDash)r).AceiteManual);
//            dash.ListaPedidosComAceiteManual = lstPedidosSemCancelados.Where(r => ((PedidoResultDash)r).AceiteManual).ToList();


//            if (bolApenasCanceladosErroFornecedores)
//            {
//                dash.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.ErroEquipeFornecedores);
//                dash.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.ErroEquipeFornecedores).ToList();
//            }
//            else {
//                dash.QtdPedidosCancelados = lstPedidosCancelados.Count(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste);
//                dash.ListaPedidosCancelados = lstPedidosCancelados.Where(r => r.StatusCancelamentoID != (int)TodosStatusCancelamento.Teste).ToList();
//            }

//            dash.QtdMailConf = lstPedidosSemCancelados.Count(p => ((PedidoResultDash)p).NpsEmailEnviado);
//            #endregion

//            #region Listas
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();

//                List<Cidade> lstCidades = new List<Cidade>();
//                List<Estado> lstEstados = new List<Estado>();

//                string sqlCidades = "SELECT * FROM Local.Cidade";
//                string sqlEstados = "SELECT * FROM Local.Estado";

//                lstCidades = con.Query<Cidade>(sqlCidades).ToList();
//                lstEstados = con.Query<Estado>(sqlEstados).ToList();
//                if (dash.ListaPedidosComAceiteManual.Any())
//                {
//                    foreach (var pedido in dash.ListaPedidosComAceiteManual)
//                    {
//                        pedido.Cidade = lstCidades.FirstOrDefault(c => c.ID == pedido.CidadeID);
//                        pedido.Estado = lstEstados.FirstOrDefault(e => e.ID == pedido.EstadoID);
//                    }
//                }
//                if (dash.ListaPedidosCancelados.Any())
//                {
//                    foreach (var pedido in dash.ListaPedidosCancelados)
//                    {
//                        pedido.Cidade = lstCidades.FirstOrDefault(c => c.ID == pedido.CidadeID);
//                        pedido.Estado = lstEstados.FirstOrDefault(e => e.ID == pedido.EstadoID);
//                    }
//                }

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            #endregion

//            stp1.Stop();
//            str1 = string.Format("{0} minutos, {1} segundos, {2} milisegundos", stp1.Elapsed.Minutes, stp1.Elapsed.Seconds, stp1.Elapsed.Milliseconds);

//            return dash;
//        }
//        //-----------------------------------------------------------------------------
//        private static List<Repasse> ListRepassePorCarteira(DateTime DataIncial, DateTime DataFinal, COROASEntities context, int? intIdAdm)
//        {
//            Expression<Func<Repasse, bool>> exp = r =>
//                r.StatusID != (int)Repasse.TodosStatus.Cancelado &&
//                (r.PedidoItem.Pedido.DataCriacao >= DataIncial && r.PedidoItem.Pedido.DataCriacao <= DataFinal)
//                && r.PedidoItem.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado;

//            Expression<Func<Repasse, bool>> exp2 = p => p.ID > 0;
//            if (intIdAdm.HasValue && intIdAdm.Value > 0)
//            {
//                exp2 = exp2.And(
//                        p =>
//                         (lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID)
//                          || (!lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID) && lstCarteiraEstados.Contains(p.PedidoItem.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.PedidoItem.Pedido.CidadeID))
//                          || (!lstCarteiraCidades.Contains(p.PedidoItem.Pedido.CidadeID) && !lstCarteiraEstados.Contains(p.PedidoItem.Pedido.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.PedidoItem.Pedido.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.PedidoItem.Pedido.EstadoID)))

//                    );
//            }

//            var temp = context.Repasses
//                .Include(r => r.PedidoItem)
//                .Include(r => r.PedidoItem.Pedido)
//                .Where(exp).ToList();

//            return temp.Where(exp2.Compile()).ToList();
//        }
//        //-----------------------------------------------------------------------------
//        private static List<PedidoItem> ListPedidosItemPorCarteira(DateTime DataIncial, DateTime DataFinal, COROASEntities context, int? intIdAdm)
//        {

//            List<Pedido> lstPedidos = new List<Pedido>();

//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                string sql = "SELECT ID,CidadeID,EstadoID FROM Cliente.Pedido a WHERE a.DataCriacao BETWEEN @ini AND @fim";
//                lstPedidos = con.Query<Pedido>(sql, new { ini = DataIncial, fim = DataFinal }).ToList();
//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }

//            //Expression<Func<Pedido, bool>> expPedido = p => (p.DataCriacao >= DataIncial && p.DataCriacao <= DataFinal);
//            if (intIdAdm.HasValue && intIdAdm.Value > 0)
//            {

//                #region Extensão dos filtros
//                Func<Pedido, bool> expPedido = p =>
//                         (lstCarteiraCidades.Contains(p.CidadeID)
//                         || (!lstCarteiraCidades.Contains(p.CidadeID) && lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID))
//                         || (!lstCarteiraCidades.Contains(p.CidadeID) && !lstCarteiraEstados.Contains(p.EstadoID) && !lstCarteiraCidadesOutros.Contains(p.CidadeID) && !lstCarteiraEstadosOutros.Contains(p.EstadoID)))
//                    ;
//                lstPedidos = lstPedidos.Where(expPedido).ToList();
//                //primeiro consulta se ele tem a cidade vinculada atraves da carteira
//                //senão consulta se ele tem o estado vinculado e a cidade do pedido não pertence à outro
//                //caso o pedido não tenha ninguen vinculado, libera pra todos
//                //exp = exp.And(exp2);

//                #endregion

//            }

//            List<int> lstIdsPedidos = lstPedidos.Select(p => p.ID).ToList();

//            Expression<Func<PedidoItem, bool>> exp = pi => lstIdsPedidos.Contains(pi.PedidoID);

//            var result = context.PedidoItems
//                .Include(r => r.Pedido)
//                .Include(r => r.Pedido.Cliente)
//                .Include(r => r.Pedido.Cidade)
//                .Include(r => r.Pedido.Estado)
//                .Include(r => r.Pedido.AdministradorPedido)
//                .Include(r => r.Pedido.NPSRatings)
//                .Where(exp).ToList();
//            return result;
//        }
//        //-----------------------------------------------------------------------------
//        public static void BuscarCarteiraAdm(int? idAdm, out List<Cidade> lstCidades, out List<Estado> lstEstados, out string strCarteiras, out decimal dcmMetaRepasse, out decimal dcmMetaNPS, out decimal dcmMetaIndiceCanc)
//        {
//            lstEstados = new List<Estado>();
//            lstCidades = new List<Cidade>();
//            strCarteiras = string.Empty;
//            dcmMetaNPS = dcmMetaRepasse = dcmMetaIndiceCanc = 0;
//            using (COROASEntities dal = new COROASEntities())
//            {
//                dal.ContextOptions.LazyLoadingEnabled = false;

//                var lstAdmCarteira = dal.AdministradorCarteiras
//                    .Include(p => p.Carteira)
//                    .Include(p => p.Carteira.Estadoes)
//                    .Include(p => p.Carteira.Cidades)
//                    .Include(p => p.Carteira.Cidades.Select(c => c.Estado))
//                    .Where(AdministradorCarteira.CarteiraComVigencia(idAdm)).ToList();
//                foreach (AdministradorCarteira admCarteira in lstAdmCarteira)
//                {
//                    foreach (Estado estado in admCarteira.Carteira.Estadoes)
//                    {
//                        lstEstados.Add(estado);
//                    }
//                    foreach (Cidade cidade in admCarteira.Carteira.Cidades)
//                    {
//                        lstCidades.Add(cidade);
//                    }
//                    strCarteiras += admCarteira.Carteira.Nome + ", ";
//                }
//                if (lstAdmCarteira.Count == 1)
//                {
//                    var admCarteira = lstAdmCarteira.FirstOrDefault();
//                    if (admCarteira != null && admCarteira.Carteira != null)
//                    {
//                        dcmMetaRepasse = admCarteira.Carteira.MetaRepasse ?? 0;
//                        dcmMetaNPS = admCarteira.Carteira.MetaNPS ?? 0;
//                        dcmMetaIndiceCanc = admCarteira.Carteira.MetaIndiceCancelado ?? 0;

//                    }
//                }
//            }
//            if (strCarteiras.LastIndexOf(", ") == strCarteiras.Length - 2)
//            {
//                strCarteiras = strCarteiras.Substring(0, strCarteiras.Length - 2);
//            }
//        }
//        //-----------------------------------------------------------------------------
//        public static List<RelatorioAtendente_Result> GetRelatorioVendaAtendente(DateTime dataInicio, DateTime dataFim)
//        {
//            List<RelatorioAtendente_Result> lstResult = new List<RelatorioAtendente_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.spRelatorioAtendente  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<RelatorioAtendente_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<RelatorioAtendenteTemp_Result> GetRelatorioVendaAtendenteTemp(DateTime dataInicio, DateTime dataFim)
//        {
//            List<RelatorioAtendenteTemp_Result> lstResult = new List<RelatorioAtendenteTemp_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.spRelatorioAtendenteTempo  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<RelatorioAtendenteTemp_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<spRelatorioFunerarias_Result> GetRelatorioFunerarias(DateTime dataInicio, DateTime dataFim)
//        {
//            List<spRelatorioFunerarias_Result> lstResult = new List<spRelatorioFunerarias_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.spRelatorioFunerarias  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<spRelatorioFunerarias_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<RelatorioVendasCPVFloricultura_Result> GetRelatorioVendasCPVFloricultura(DateTime dataInicio, DateTime dataFim)
//        {
//            List<RelatorioVendasCPVFloricultura_Result> lstResult = new List<RelatorioVendasCPVFloricultura_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.RelatorioVendasCPVFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<RelatorioVendasCPVFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<spRelatorioProdutosFloricultura_Result> GetRelatorioProdutosFloricultura(DateTime dataInicio, DateTime dataFim)
//        {
//            List<spRelatorioProdutosFloricultura_Result> lstResult = new List<spRelatorioProdutosFloricultura_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.spRelatorioProdutosFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<spRelatorioProdutosFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        //-----------------------------------------------------------------------------
//        public static List<spRelatorioMeioPagamentoFloricultura_Result> GetMeioPagamentoFloricultura(DateTime dataInicio, DateTime dataFim)
//        {
//            List<spRelatorioMeioPagamentoFloricultura_Result> lstResult = new List<spRelatorioMeioPagamentoFloricultura_Result>();
//            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
//            {
//                con.Open();
//                //sistema.spRelatorioAtendente @datainicio='01/01/2010',@datafim='01/01/2017'
//                string sql = "exec sistema.spRelatorioMeioPagamentoFloricultura  @datainicio = @paramIni, @datafim = @paramFim";
//                lstResult = con.Query<spRelatorioMeioPagamentoFloricultura_Result>(sql, new { paramIni = dataInicio, paramFim = dataFim }, commandTimeout: 600).ToList();

//                if (con.State == ConnectionState.Open)
//                {
//                    con.Close();
//                }
//            }
//            return lstResult;
//        }
//        #endregion

//    }
//    //=================================================================================
//    public class DashFornecedorNPS
//    {
//        public int FornecedorID { get; set; }
//        public decimal Media { get; set; }
//        public int Pedidos { get; set; }
//    }
//    //=================================================================================
//    public class HelperDapperReturn
//    {
//        public int FirstId { get; set; }
//        public int SecondId { get; set; }
//    }
//    //=================================================================================
//    public class PedidoResultDash : Pedido
//    {
//        public int TipoClienteID { get; set; }
//        public bool NpsEmailEnviado { get; set; }
//        public bool AceiteManual { get; set; }
//    }
//    //=================================================================================
//    public class RepasseResult
//    {
//        public int Id { get; set; }
//        public int IdPedido { get; set; }
//        public int IdPedidoItem { get; set; }
//        public string OrigemForma { get; set; }
//        public string OrigemSite { get; set; }
//        public decimal ValorRepasse { get; set; }
//    }
//    //=================================================================================
//}
