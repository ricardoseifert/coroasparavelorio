﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Uol.PagSeguro;
using System.Data.Objects;
using Domain.Entities;

namespace Domain.Service
{
    public class PagSeguroService
    {

        public enum Status
        {
            AguardandoPagamento = 1,
            EmAnalise = 2,
            Paga = 3,
            Disponivel = 4,
            EmDisputa = 5,
            Devolvida = 6,
            Cancelada = 7
        }

        public enum TiposPagamento
        {
            Indefinido = 0,
            CartaoCredito = 1,
            Boleto = 2,
            DebitoOnline = 3,
            SaldoPagSeguro = 4,
            OiPaggo = 5
        }

        private PedidoService pedidoService;

        public PagSeguroService(ObjectContext context)
        {
            pedidoService = new PedidoService(context);
        }

        public string RetornoPagamento(string codigo)
        {
            AccountCredentials credentials = new AccountCredentials(Core.Configuracoes.PAGSEGURO_EMAIL, Core.Configuracoes.PAGSEGURO_TOKEN);

            // Realizando uma consulta de transação a partir do código identificador   
            // para obter o objeto Transaction  
            Transaction transaction = TransactionSearchService.SearchByCode(credentials,codigo);

            AtualizarPagamento(transaction);
            return transaction.Reference;
        }

        public void NotificarPagamento(string codigo)
        {
            AccountCredentials credentials = new AccountCredentials(Core.Configuracoes.PAGSEGURO_EMAIL, Core.Configuracoes.PAGSEGURO_TOKEN);
            Transaction transaction = NotificationService.CheckTransaction(credentials, codigo);

            AtualizarPagamento(transaction);
        }

        public void AtualizarPagamento(Transaction transaction)
        {
            var codigoPedido = Guid.Parse(transaction.Reference);
            var status = (Status)transaction.TransactionStatus;
            var tipoPagamento = (TiposPagamento)transaction.PaymentMethod.PaymentMethodType;

            var novoStatus = Pedido.TodosStatusPagamento.AguardandoPagamento;
            switch (status)
            {
                case Status.AguardandoPagamento:
                case Status.EmAnalise:
                case Status.EmDisputa:
                    novoStatus = Pedido.TodosStatusPagamento.AguardandoPagamento;
                    break;
                case Status.Disponivel:
                case Status.Paga:
                    novoStatus = Pedido.TodosStatusPagamento.Pago;
                    break;
                case Status.Cancelada:
                case Status.Devolvida:
                    novoStatus = Pedido.TodosStatusPagamento.Cancelado;
                    break;
            }

            var formaPagamento = Pedido.FormasPagamento.Indefinido;
            switch (tipoPagamento)
            {
                case TiposPagamento.Boleto:
                    formaPagamento = Pedido.FormasPagamento.Boleto;
                    break;
                case TiposPagamento.CartaoCredito:
                    formaPagamento = Pedido.FormasPagamento.CartaoCredito;
                    break;
                case TiposPagamento.DebitoOnline:
                    formaPagamento = Pedido.FormasPagamento.TransferenciaBancaria;
                    break;
                case TiposPagamento.OiPaggo:
                    formaPagamento = Pedido.FormasPagamento.Outros;
                    break;
                case TiposPagamento.SaldoPagSeguro:
                    formaPagamento = Pedido.FormasPagamento.Saldo;
                    break;
            }

            pedidoService.AtualizarPagamentoPagSeguro(codigoPedido, novoStatus, formaPagamento);
        }
    }
}
