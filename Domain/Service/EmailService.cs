﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Xml.Linq;

using System.Net;
using System.Collections.Specialized;
using System.Globalization;

namespace Domain.Service
{
    public class AvisoService
    { 
        public static void EnviaAniversariantes()
        {
            var context = new COROASEntities();
            var colaboradorRepository = new PersistentRepository<Domain.Entities.Colaborador>(context); 
            var adminRepository = new PersistentRepository<Domain.Entities.Administrador>(context); 
            
            //var admin = adminRepository.GetByExpression(c => c.PerfilID == (int)Administrador.Perfis.Master);
            var colaboradores = colaboradorRepository.GetAll().Where(d => d.DataNascimento.HasValue && (d.DataNascimento.Value.DayOfYear - DateTime.Now.DayOfYear) <= 10 && (d.DataNascimento.Value.DayOfYear - DateTime.Now.DayOfYear) >= 0).OrderBy(c => c.DataNascimento.Value.DayOfYear);

            var texto = "";
            foreach (var colaborador in colaboradores)
            {
                texto += colaborador.Nome.ToUpper() + " ( " + colaborador.Empresa.RazaoSocial + " )\n";
            }
            var corpo = string.Format("Os seguintes colaboradores fazem aniversário nos próximos 5 dias:\n\n{0}\n\nMais informações, acesse a área de Aniversariantes.", texto);

            //foreach (var user in admin)
            //{
            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "BACKOFFICE", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "ATENDIMENTO", null, null, null, "[BACKOFFICE] - Aniversariantes", corpo, false);
            //}
        } 

        public static void EnviaProximoContato()
        {
            var context = new COROASEntities();
            var clienteRepository = new PersistentRepository<Domain.Entities.Cliente>(context);
            var adminRepository = new PersistentRepository<Domain.Entities.Administrador>(context); 

            //var admin = adminRepository.GetByExpression(c => c.PerfilID == (int)Administrador.Perfis.Master);
            var clientes = clienteRepository.GetByExpression(c => c.LacosDataContato.HasValue && c.LacosDataProximoContato.Value.Day == DateTime.Now.Day && c.LacosDataProximoContato.Value.Month == DateTime.Now.Month && c.LacosDataProximoContato.Value.Year == DateTime.Now.Year).ToList();

            var texto = "";
            foreach (var cliente in clientes)
            {
                texto += cliente.Nome.ToUpper() + " ( " + cliente.RazaoSocial + " ) - atendimento: " + cliente.Administrador.Nome + "\n";
            }
            var corpo = string.Format("Os seguintes clientes tem o retorno programado para hoje:\n\n{0}\n\nMais informações, acesse a área Agenda.", texto);

            //foreach (var user in admin)
            //{
            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "BACKOFFICE", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "ATENDIMENTO", null, null, null, "[BACKOFFICE] - Contato programado para hoje", corpo, false);
            //}
        }

        public static void BuscaRetornoSendGrid()
        {
            var context = new COROASEntities();
            var clienteRepository = new PersistentRepository<Domain.Entities.Cliente>(context);
            var empresaRepository = new PersistentRepository<Domain.Entities.Empresa>(context);
            var colaboradorRepository = new PersistentRepository<Domain.Entities.Colaborador>(context);
            var retornoRepository = new PersistentRepository<Domain.Entities.RetornoEmail>(context);

            XDocument xmlDoc = XDocument.Load("https://api.sendgrid.com/api/bounces.get.xml?api_user=" + Domain.Core.Configuracoes.EMAIL_NOREPLY + "&api_key=" + Domain.Core.Configuracoes.TOKEN_SENDGRID + "&days=300&date=1");

            var q = xmlDoc.Descendants("bounce").ToList(); 


            foreach (var item in q)
            {
                var email = item.Element("email").Value;
                var data = Convert.ToDateTime(item.Element("created").Value);
                var reason = item.Element("reason").Value;
                var status = item.Element("status").Value;
                int? clienteID = null;
                int? empresaID = null;
                int? colaboradorID = null;
                var cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
                var empresa = empresaRepository.GetByExpression(c => c.EmailContato == email).FirstOrDefault();
                var colaborador = colaboradorRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
                if (cliente != null)
                {
                    clienteID = cliente.ID;
                    cliente.ProblemaEmail = true;
                    clienteRepository.Save(cliente);
                }
                if (empresa != null)
                {
                    empresaID = empresa.ID;
                    empresa.ProblemaEmail = true;
                    empresaRepository.Save(empresa);
                }
                if (colaborador != null)
                {
                    colaboradorID = colaborador.ID;
                    colaborador.ProblemaEmail = true;
                    colaboradorRepository.Save(colaborador);
                }

                var retorno = retornoRepository.GetByExpression(c => c.Data == data && c.Email == email && c.EmpresaID == empresaID && c.ColaboradorID == colaboradorID && c.ClienteID == clienteID).FirstOrDefault();
                if (retorno == null && (clienteID.HasValue || empresaID.HasValue || colaboradorID.HasValue))
                {
                    retorno = new RetornoEmail();
                    retorno.ClienteID = clienteID;
                    retorno.EmpresaID = empresaID;
                    retorno.ColaboradorID = colaboradorID;
                    retorno.Email = email;
                    retorno.Data = data;
                    retorno.Retorno = reason;
                    retorno.Codigo = status;
                    retornoRepository.Save(retorno);
                }

            }
        }

        public void addEmailMkt(Pedido pedido)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["id"] = "f77d7ec70b";
                values["email[email]"] = pedido.Cliente.Email;
                values["code"] = "us11";

                string nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pedido.Cliente.Nome.ToLower());
                string sobrenome = string.Empty;

                if (pedido.Cliente.Nome.Split(' ').Count() > 1)
                {
                    nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pedido.Cliente.Nome.Split(' ')[0].ToLower());
                    sobrenome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pedido.Cliente.Nome.Split(' ')[1].ToLower());
                }

                values["merge_vars[FNAME]"] = nome.Trim();
                values["merge_vars[LNAME]"] = sobrenome.Trim();

                if (pedido.OrigemSite == "COROAS PARA VELÓRIO")
                {
                    values["merge_vars[COROA]"] = "SIM";
                    values["merge_vars[DTULTPC]"] = pedido.DataCriacao.ToString("MM/dd/yyyy");
                }
                else
                {
                    values["merge_vars[LACOS]"] = "SIM";
                    values["merge_vars[DTULTPCL]"] = pedido.DataCriacao.ToString("MM/dd/yyyy");
                }
                try
                {
                    var response = client.UploadValues("http://grupolacosflores.com.br/api/subscribe", values);
                    //var response = client.UploadValues("http://localhost:18997/api/subscribe", values);
                }
                catch{ }
            }
        }

        public void addEmailMkt(Cliente cliente)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                string nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.ToLower());
                string sobrenome = string.Empty;
                if (cliente.Nome.Split(' ').Count() > 1)
                {
                    nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.Split(' ')[0].ToLower());
                    sobrenome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.Split(' ')[1].ToLower());
                }
                values["id"] = "f77d7ec70b";
                values["email[email]"] = cliente.Email;
                values["code"] = "us11";
                values["merge_vars[FNAME]"] = nome.Trim();
                values["merge_vars[LNAME]"] = sobrenome.Trim();
                values["merge_vars[TIPO]"] = cliente.TipoID == 1 ? "PF" : "PJ";
                values["merge_vars[RECEMAIL]"] = cliente.ReceberEmail == 1 ? "SIM" : "NAO";

                if (cliente.DataAniversario == null || cliente.DataAniversario == "")
                {
                    values["merge_vars[DATANIVER]"] = "";
                }
                else
                {
                    var Dia = cliente.DataAniversario.Split('/')[0];
                    var Mes = cliente.DataAniversario.Split('/')[1];

                    values["merge_vars[DATANIVER]"] = Mes + "/" + Dia;
                }

                var response = client.UploadValues("http://grupolacosflores.com.br/api/subscribe", values);
            }
        }

        public void addEmailMkt(Cliente cliente, string site, string OrigemSite = "")
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                string nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.ToLower());
                string sobrenome = string.Empty;
                if (cliente.Nome.Split(' ').Count() > 1)
                {
                    nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.Split(' ')[0].ToLower());
                    sobrenome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cliente.Nome.Split(' ')[1].ToLower());
                }
                values["id"] = "f77d7ec70b";
                values["email[email]"] = cliente.Email;
                values["code"] = "us11";
                values["merge_vars[FNAME]"] = nome.Trim();
                values["merge_vars[LNAME]"] = sobrenome.Trim();
                values["merge_vars[TIPO]"] = cliente.TipoID == 1 ? "PF" : "PJ";

                switch (site)
                {
                    case "COROAS PARA VELÓRIO":
                        values["merge_vars[SITEORIGEM]"] = "CPV";
                        values["merge_vars[COROA]"] = "NAO";
                        values["merge_vars[LACOS]"] = "NAO";
                        break;
                    case "ADMIN COROAS PARA VELÓRIO":

                        if (cliente.Pedidoes != null && cliente.Pedidoes.Count > 0)
                        {

                            // CLIENTE JÁ POSSUI PEDIDO
                            var PrimeiroPedido = cliente.Pedidoes.OrderByDescending(p => p.DataCriacao).Reverse().Take(1).FirstOrDefault();

                            var PedidoAtual = cliente.Pedidoes.OrderByDescending(p => p.DataCriacao).Take(1).FirstOrDefault();

                            // PREENCHE A ORIGEM
                            if (PrimeiroPedido.OrigemSite == "COROAS PARA VELÓRIO")
                            {
                                values["merge_vars[SITEORIGEM]"] = "CPV";
                            }

                            if (PrimeiroPedido.OrigemSite == "LAÇOS FLORES")
                            {
                                values["merge_vars[SITEORIGEM]"] = "LACOS";
                            }

                            // PREENCHE O ULTIMO PEDIDO
                            if (PedidoAtual.OrigemSite == "LAÇOS FLORES")
                            {
                                values["merge_vars[DTULTPCL]"] = DateTime.Now.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                values["merge_vars[DTULTPC]"] = DateTime.Now.ToString("MM/dd/yyyy");
                            }

                            // PREENCHE O COMPROU COROA
                            if (cliente.Pedidoes.Where(p => p.OrigemSite == "COROAS PARA VELÓRIO").Any())
                            {
                                values["merge_vars[COROA]"] = "SIM";
                                values["merge_vars[DTULTPC]"] = cliente.Pedidoes.Where(p => p.OrigemSite == "COROAS PARA VELÓRIO").OrderByDescending(p => p.DataCriacao).Take(1).FirstOrDefault().DataCriacao.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                values["merge_vars[COROA]"] = "NAO";
                            }

                            // PREENCHE O COMPROU LACOS   
                            if (cliente.Pedidoes.Where(p => p.OrigemSite == "LAÇOS FLORES").Any())
                            {
                                values["merge_vars[LACOS]"] = "SIM";
                                values["merge_vars[DTULTPCL]"] = cliente.Pedidoes.Where(p => p.OrigemSite == "LAÇOS FLORES").OrderByDescending(p => p.DataCriacao).Take(1).FirstOrDefault().DataCriacao.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                values["merge_vars[LACOS]"] = "NAO";
                            }
                        }
                        else
                        {
                            // CLIENTE NÃO POSSUI PEDIDO
                            if (OrigemSite == "LAÇOS FLORES")
                            {
                                values["merge_vars[SITEORIGEM]"] = "LACOS";
                                values["merge_vars[COROA]"] = "NAO";
                                values["merge_vars[LACOS]"] = "SIM";
                                values["merge_vars[DTULTPCL]"] = DateTime.Now.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                values["merge_vars[SITEORIGEM]"] = "CPV";
                                values["merge_vars[LACOS]"] = "NAO";
                                values["merge_vars[COROA]"] = "SIM";
                                values["merge_vars[DTULTPC]"] = DateTime.Now.ToString("MM/dd/yyyy");
                            }
                        }

                        break;
                    default:
                        values["merge_vars[SITEORIGEM]"] = "LACOS";
                        values["merge_vars[COROA]"] = "NAO";
                        values["merge_vars[LACOS]"] = "NAO";
                        break;
                }

                values["merge_vars[DTCAD]"] = cliente.DataCadastro.ToString("MM/dd/yyyy");
                values["merge_vars[RECEMAIL]"] = cliente.ReceberEmail == 1 ? "SIM" : "NAO";

                if (cliente.DataAniversario == null || cliente.DataAniversario == "")
                {
                    values["merge_vars[DATANIVER]"] = "";
                }
                else
                {
                    var Dia = cliente.DataAniversario.Split('/')[0];
                    var Mes = cliente.DataAniversario.Split('/')[1];

                    values["merge_vars[DATANIVER]"] = Mes + "/" + Dia;
                }
                

                var response = client.UploadValues("http://grupolacosflores.com.br/api/subscribe", values);
            }
        }

    }
}
