﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Repositories;
using Domain.Factories;
using System.Data.Objects;
using System.Linq.Expressions;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Entities.Generics;
using Domain.MetodosExtensao;
using PagarMe;
using System.Net;
using System.IO;
using System.Web;

namespace Domain.Service
{
    //=================================================================================
    public class PedidoService
    {

        #region Variáveis
        //-----------------------------------------------------------------------------
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private PersistentRepository<Log> logRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<Pedido> pedidoEntidadeRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaEntidadeRepository;
        private IPersistentRepository<AdministradorCarteira> AdministradorCarteiraRepository;
        private RelatorioRepository relatorioRepository;
        private PedidoRepository pedidoRepository;
        private CupomRepository cupomRepository;
        private IPersistentRepository<FornecedorLocal> fornecedorlocalRepository;
        private IPersistentRepository<FornecedorPush> fornecedorPushRepository;
        private PedidoFactory pedidoFactory;
        private CupomService cupomService;
        private ObjectContext context;
        Pedido PedidoCPV = new Pedido();
        PedidoEmpresa PedidoEmpresa = new PedidoEmpresa();
        Dictionary<string, string> RetornoIntervalo = new Dictionary<string, string>();
        Dictionary<int, int> Intervalo = new Dictionary<int, int>();
        Dictionary<int, bool> FornecedorRecusou = new Dictionary<int, bool>();
        Dictionary<int, bool> LSTFornecedorLeilaoExpirado = new Dictionary<int, bool>();
        List<FornecedorLocal> ListaFornecedoresAcima = new List<FornecedorLocal>();
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public PedidoService(ObjectContext context)
        {
            clienteRepository = new PersistentRepository<Cliente>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            pedidoEntidadeRepository = new PersistentRepository<Pedido>(context);
            pedidoEmpresaEntidadeRepository = new PersistentRepository<PedidoEmpresa>(context);
            pedidoRepository = new PedidoRepository(context);
            cupomRepository = new CupomRepository(context);
            AdministradorCarteiraRepository = new PersistentRepository<AdministradorCarteira>(context);
            pedidoFactory = new PedidoFactory(context);
            cupomService = new CupomService(context);
            fornecedorlocalRepository = new PersistentRepository<FornecedorLocal>(context);
            fornecedorPushRepository = new PersistentRepository<FornecedorPush>(context);
            relatorioRepository = new RelatorioRepository(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
            this.context = context;
        }

        public PedidoService()
        {

        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Metodos
        //-----------------------------------------------------------------------------
        public int RetornaMeuIndex(int IdFornecedor, int IdPedido, OrigemPedido TiPedido)
        {
            if (TiPedido == OrigemPedido.Cliente)
            {
                PedidoCPV = pedidoRepository.Get(IdPedido);

                return PedidoCPV.Local.FornecedorLocals.OrderBy(r => r.Ordem).ToList().FindIndex(r => r.FornecedorID == IdFornecedor);
            }
            else
            {
                PedidoEmpresa = pedidoEmpresaEntidadeRepository.Get(IdPedido);

                return PedidoEmpresa.Local.FornecedorLocals.OrderBy(r => r.Ordem).ToList().FindIndex(r => r.FornecedorID == IdFornecedor);
            }
        }
        //-----------------------------------------------------------------------------
        public void AtualizarPagamento(Guid codigo, Pedido.TodosStatusPagamento novoStatus, Pedido.FormasPagamento formaPagamento = Pedido.FormasPagamento.Indefinido, double valorPago = 0)
        {
            PedidoCPV = pedidoRepository.GetByCodigo(codigo);
            if (PedidoCPV != null)
            {
                if (PedidoCPV.StatusPagamento != novoStatus)
                {
                    PedidoCPV.StatusPagamento = novoStatus;
                    switch (novoStatus)
                    {
                        case Pedido.TodosStatusPagamento.Pago:
                            PedidoCPV.DataPagamento = DateTime.Now;
                            PedidoCPV.ValorPago = PedidoCPV.ValorTotal;
                            break;
                        case Pedido.TodosStatusPagamento.Cancelado:
                            PedidoCPV.StatusEntrega = TodosStatusEntrega.Cancelado;
                            break;
                    }
                }

                if (formaPagamento != Pedido.FormasPagamento.Indefinido)
                {
                    PedidoCPV.FormaPagamento = formaPagamento;
                }

                PedidoCPV.DataAtualizacao = DateTime.Now;
                pedidoRepository.Save(PedidoCPV);
            }
        }
        //-----------------------------------------------------------------------------
        public void AtualizarPagamentoPagSeguro(Guid codigo, Pedido.TodosStatusPagamento novoStatus, Pedido.FormasPagamento formaPagamento = Pedido.FormasPagamento.Indefinido)
        {
            PedidoCPV = pedidoRepository.GetByCodigo(codigo);
            if (PedidoCPV != null)
            {
                if (PedidoCPV.MeioPagamento == Pedido.MeiosPagamento.PagSeguro)
                {
                    if (PedidoCPV.StatusPagamento != novoStatus)
                    {
                        PedidoCPV.StatusPagamento = novoStatus;
                        switch (novoStatus)
                        {
                            case Pedido.TodosStatusPagamento.Pago:
                                PedidoCPV.DataPagamento = DateTime.Now;
                                PedidoCPV.ValorPago = PedidoCPV.ValorTotal;
                                break;
                            case Pedido.TodosStatusPagamento.Cancelado:
                                PedidoCPV.ObservacoesPagamento += "\nPedido recebido como cancelado pelo PagSeguro em " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                                break;
                        }
                    }

                    if (formaPagamento != Pedido.FormasPagamento.Indefinido)
                    {
                        PedidoCPV.FormaPagamento = formaPagamento;
                    }

                    PedidoCPV.DataAtualizacao = DateTime.Now;
                    pedidoRepository.Save(PedidoCPV);
                }
            }
        }
        //-----------------------------------------------------------------------------
        public Dictionary<int, bool> FornecedoresRecusaramEntrega(int IdPedido, OrigemPedido TipoPedido)
        {
            if (TipoPedido == OrigemPedido.Cliente)
            {
                PedidoCPV = pedidoRepository.Get(IdPedido);

                if (PedidoCPV.Local == null)
                    return null;

                foreach (var Fornecedor in PedidoCPV.Local.FornecedorLocals)
                {
                    if (pedidoRepository.Get(IdPedido).AdministradorPedidoes.Where(r => r.AcaoID == (int)AdministradorPedido.Acao.RecusouEntrega && r.IdFornecedor == Fornecedor.FornecedorID).Any())
                    {
                        if (!FornecedorRecusou.ContainsKey(Fornecedor.FornecedorID))
                        {
                            FornecedorRecusou.Add(Fornecedor.FornecedorID, true);
                        }
                    }
                    else
                    {
                        if (!FornecedorRecusou.ContainsKey(Fornecedor.FornecedorID))
                        {
                            FornecedorRecusou.Add(Fornecedor.FornecedorID, false);
                        }
                    }
                }

                return FornecedorRecusou;
            }
            else
            {
                PedidoEmpresa = pedidoEmpresaEntidadeRepository.Get(IdPedido);

                if (PedidoEmpresa.Local == null)
                    return null;

                foreach (var Fornecedor in PedidoEmpresa.Local.FornecedorLocals)
                {
                    if (pedidoEmpresaEntidadeRepository.Get(IdPedido).AdministradorPedidoes.Where(r => r.AcaoID == (int)AdministradorPedido.Acao.RecusouEntrega && r.IdFornecedor == Fornecedor.FornecedorID).Any())
                    {
                        if (!FornecedorRecusou.ContainsKey(Fornecedor.FornecedorID))
                        {
                            FornecedorRecusou.Add(Fornecedor.FornecedorID, true);
                        }
                    }
                    else
                    {
                        if (!FornecedorRecusou.ContainsKey(Fornecedor.FornecedorID))
                        {
                            FornecedorRecusou.Add(Fornecedor.FornecedorID, false);
                        }
                    }
                }
            }

            return FornecedorRecusou;
        }
        //-----------------------------------------------------------------------------
        public Dictionary<int, bool> FornecedoresLeilaoExpirado(int IdPedido, OrigemPedido TipoPedido)
        {
            if (TipoPedido == OrigemPedido.Cliente)
            {
                PedidoCPV = pedidoRepository.Get(IdPedido);

                foreach (var Fornecedor in PedidoCPV.Local.FornecedorLocals)
                {
                    int Prioridade = PedidoCPV.Local.FornecedorLocals.Where(r => r.FornecedorID == Fornecedor.FornecedorID).FirstOrDefault().Ordem;

                    if (Prioridade == 0)
                        Prioridade = 1;

                }

                return LSTFornecedorLeilaoExpirado;
            }
            else
            {
                PedidoEmpresa = pedidoEmpresaEntidadeRepository.GetByExpression(r => r.ID == IdPedido).First();

                if (PedidoEmpresa.Local == null)
                    return null;

                foreach (var Fornecedor in PedidoEmpresa.Local.FornecedorLocals)
                {
                    int Prioridade = PedidoEmpresa.Local.FornecedorLocals.Where(r => r.FornecedorID == Fornecedor.FornecedorID).FirstOrDefault().Ordem;

                    if (Prioridade == 0)
                        Prioridade = 1;
                }
                return LSTFornecedorLeilaoExpirado;
            }
        }
        //-----------------------------------------------------------------------------
        public Pedido CriarPedidoPagarMe(Carrinho carrinho, int cidadeID, int clienteID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "", string origemSite = "", string origemForma = "")
        {
            if (!carrinho.Vazio)
            {
                var data = DateTime.MinValue;
                if (DateTime.TryParse(dataSolicitada, out data))
                {
                    var cliente = clienteRepository.Get(clienteID);
                    var cidade = cidadeRepository.Get(cidadeID);
                    if (cliente != null)
                    {
                        if (cidade != null)
                        {
                            int? cupomID = null;
                            if (carrinho.Cupom != null)
                            {
                                var cupom = cupomRepository.GetByCodigo(carrinho.Cupom.Codigo);
                                if (cupom == null)
                                {
                                    throw new Exception("Cupom inválido");
                                }
                                else if (!cupomService.PodeUtilizar(cliente, cupom))
                                {
                                    throw new Exception("Você já utilizou esse cupom");
                                }
                                else
                                {
                                    cupomID = cupom.ID;
                                }
                            }

                            var pedido = pedidoFactory.CriarPedidoPagarMe(cidade.ID, cliente.ID, cupomID, data, cidade.EstadoID, formaPagamento, localEntrega, meioPagamento, carrinho.Subtotal, carrinho.Desconto, carrinho.Total, observacoes, parentesco, pessoaHomenageada, (String.IsNullOrEmpty(telefoneContato) ? cliente.TelefoneContato : telefoneContato), origem, origemSite, origemForma);

                            if (pedido == null)
                            {
                                throw new Exception("Erro Pedido Duplicado");
                            }

                            foreach (var item in carrinho.Itens)
                            {
                                var pedidoItem = new PedidoItem()
                                {
                                    Descricao = item.Descricao,
                                    Mensagem = item.Mensagem.ToUpper(),
                                    Observacoes = item.Observacoes,
                                    PedidoID = pedido.ID,
                                    ProdutoTamanhoID = item.ProdutoTamanho.ID,
                                    Valor = item.Valor,
                                    StatusFotoID = 1
                                };
                                pedidoItemRepository.Save(pedidoItem);
                            }
                            if (carrinho.Cupom != null)
                            {
                                cupomService.Utilizar(carrinho.Cupom.Codigo);
                            }
                            return pedido;
                        }
                        else
                        {
                            throw new Exception("Selecione uma cidade");
                        }
                    }
                    else
                    {
                        throw new Exception("É necessário estar autenticado");
                    }
                }
                else
                {
                    throw new Exception("Data/Hora solicitada inválida");
                }
            }
            else
            {
                throw new Exception("O carrinho está vazio");
            }
        }

        public string SendMYMEtrics(Pedido pedido, string Tipo) {

            var Result = "";
            string Origem = "";
            string GA = "0";
            string JsonStr = "";

            try
            {
                #region MY METRICS POST

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://mymetric.com.br/cpv/cpv.php");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                string LinkIDCallID = "";

                if (pedido.Origem == "SITE")
                {
                    Origem = "online";
                }
                else {
                    if (pedido.OrigemForma.ToLower() == "email")
                        Origem = "offline-email";

                    if (pedido.OrigemForma.ToLower() == "whats")
                        Origem = "offline-whats";

                    if (pedido.OrigemForma.ToLower() == "chat")
                        Origem = "offline-chat";

                    if (pedido.OrigemForma.ToLower() == "telefone")
                        Origem = "offline-telefone";

                    if (pedido.Pedido_X_Ligacao.Count > 0)
                    {
                        LinkIDCallID += "'linkedid': '" + pedido.Pedido_X_Ligacao.FirstOrDefault().Ligaco.IdLigacao + "',";
                        LinkIDCallID += "'destcalleridnum': '" + pedido.Pedido_X_Ligacao.FirstOrDefault().Ligaco.NrDestino + "',";
                    }
                }

                if (Tipo == "CANCELAR")
                {
                    JsonStr = "{ 'type': 'refund',";
                }
                else {
                    JsonStr = "{ 'type': '" + Origem + "',";
                }

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    JsonStr += "'ti': '" + pedido.Numero + "',";

                    if (Tipo == "CANCELAR")
                    {
                        if (Origem == "online")
                        {
                            GA = pedido.GA;
                            JsonStr += "'cid': '" + GA + "',";
                        }
                    }
                    else
                    {
                        if (HttpContext.Current.Request.Cookies["_ga"] != null && Origem == "online")
                        {
                            GA = HttpContext.Current.Request.Cookies["_ga"].Value;
                            JsonStr += "'cid': '" + GA + "',";
                        }
                    }

                    JsonStr += "'email': '" + pedido.Cliente.Email + "',";
                    JsonStr += LinkIDCallID;
                    JsonStr += "'tr': '" + pedido.ValorTotal.ToString("N2") + "',";

                    int Count = 1;

                    foreach (var item in pedido.PedidoItems)
                    {
                        var PedidoItem = (Domain.Entities.PedidoItem)item;

                        JsonStr += "'pr" + Count + "id': '" + item.ProdutoTamanho.Produto.ID + "',";
                        JsonStr += "'pr" + Count + "nm': '" + item.ProdutoTamanho.Produto.Nome + "',";
                        JsonStr += "'pr" + Count + "br': '" + item.ProdutoTamanho.Produto.Tipo + "',";
                        JsonStr += "'pr" + Count + "pr': '" + item.ProdutoTamanho.Preco + "',";
                        JsonStr += "'pr" + Count + "qt': '1'";

                        Count++;
                    }

                    JsonStr += " }";
                    JsonStr = JsonStr.Replace("'", "\"");

                    streamWriter.Write(JsonStr);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    Result = streamReader.ReadToEnd();
                }

                #endregion

            }
            catch (Exception ex)
            {
                Result = "ERROR " + ex.Message;
            }

            #region LOG

            logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());

            logRepository.Save(new Entities.Log {
                Pedido = pedido.Numero,
                Data = DateTime.Now,
                Envio = JsonStr,
                Var1 = "MyMetrics - " + Tipo + " - " + Origem,
                Retorno = Result
            });

            #endregion

            return Result;
        }

        public Pedido CriarPedidoUmClick(Carrinho carrinho, int cidadeID, int clienteID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "", string origemSite = "", string origemForma = "")
        {
            if (!carrinho.Vazio)
            {
                var data = DateTime.MinValue;
                if (DateTime.TryParse(dataSolicitada, out data))
                {
                    var cliente = clienteRepository.Get(clienteID);
                    var cidade = cidadeRepository.Get(cidadeID);
                    if (cliente != null)
                    {
                        if (cidade != null)
                        {
                            int? cupomID = null;
                            if (carrinho.Cupom != null)
                            {
                                var cupom = cupomRepository.GetByCodigo(carrinho.Cupom.Codigo);
                                if (cupom == null)
                                {
                                    throw new Exception("Cupom inválido");
                                }
                                else if (!cupomService.PodeUtilizar(cliente, cupom))
                                {
                                    throw new Exception("Você já utilizou esse cupom");
                                }
                                else {
                                    cupomID = cupom.ID;
                                }
                            }

                            var pedido = pedidoFactory.CriarPedido(cidade.ID, cliente.ID, cupomID, data, cidade.EstadoID, formaPagamento, localEntrega, meioPagamento, carrinho.Subtotal, carrinho.Desconto, carrinho.Total, observacoes, parentesco, pessoaHomenageada, (String.IsNullOrEmpty(telefoneContato) ? cliente.TelefoneContato : telefoneContato), origem, origemSite, origemForma);

                            if (pedido == null)
                            {
                                throw new Exception("Erro Pedido Duplicado");
                            }

                            foreach (var item in carrinho.Itens)
                            {
                                if (string.IsNullOrEmpty(item.Descricao))
                                    item.Descricao = "PRODUTO";
                                if (string.IsNullOrEmpty(item.Observacoes))
                                    item.Observacoes = "PRODUTO";

                                var pedidoItem = new PedidoItem()
                                {
                                    Descricao = item.Descricao,
                                    Mensagem = item.Mensagem.ToUpper(),
                                    Observacoes = item.Observacoes,
                                    PedidoID = pedido.ID,
                                    ProdutoTamanhoID = item.ProdutoTamanho.ID,
                                    Valor = item.Valor,
                                    StatusFotoID = 1
                                };
                                pedidoItemRepository.Save(pedidoItem);
                            }
                            if (carrinho.Cupom != null)
                            {
                                cupomService.Utilizar(carrinho.Cupom.Codigo);
                            }
                            return pedido;
                        }
                        else {
                            throw new Exception("Selecione uma cidade");
                        }
                    }
                    else {
                        throw new Exception("É necessário estar autenticado");
                    }
                }
                else {
                    throw new Exception("Data/Hora solicitada inválida");
                }
            }
            else {
                throw new Exception("O carrinho está vazio");
            }
        }

        public Pedido CriarPedido(Carrinho carrinho, int cidadeID, int clienteID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "", string origemSite = "", string origemForma = "", int LocalID = 0)
        {
            if (!carrinho.Vazio)
            {
                var data = DateTime.MinValue;
                if (DateTime.TryParse(dataSolicitada, out data))
                {
                    var cliente = clienteRepository.Get(clienteID);
                    var cidade = cidadeRepository.Get(cidadeID);
                    if (cliente != null)
                    {
                        if (cidade != null)
                        {
                            int? cupomID = null;
                            if (carrinho.Cupom != null)
                            {
                                var cupom = cupomRepository.GetByCodigo(carrinho.Cupom.Codigo);
                                if (cupom == null)
                                {
                                    throw new Exception("Cupom inválido");
                                }
                                else if (!cupomService.PodeUtilizar(cliente, cupom))
                                {
                                    throw new Exception("Você já utilizou esse cupom");
                                }
                                else {
                                    cupomID = cupom.ID;
                                }
                            }

                            var pedido = pedidoFactory.CriarPedido(cidade.ID, cliente.ID, cupomID, data, cidade.EstadoID, formaPagamento, localEntrega, meioPagamento, carrinho.Subtotal, carrinho.Desconto, carrinho.Total, observacoes, pessoaHomenageada, (String.IsNullOrEmpty(telefoneContato) ? cliente.TelefoneContato : telefoneContato), origem, origemSite, origemForma, null, LocalID);

                            if (pedido == null)
                            {
                                throw new Exception("Erro Pedido Duplicado");
                            }

                            foreach (var item in carrinho.Itens)
                            {
                                if (string.IsNullOrEmpty(item.Descricao))
                                    item.Descricao = "PRODUTO";
                                if (string.IsNullOrEmpty(item.Observacoes))
                                    item.Observacoes = "PRODUTO";

                                var pedidoItem = new PedidoItem()
                                {
                                    Descricao = item.Descricao,
                                    Mensagem = item.Mensagem.ToUpper(),
                                    Observacoes = item.Observacoes,
                                    PedidoID = pedido.ID,
                                    ProdutoTamanhoID = item.ProdutoTamanho.ID,
                                    Valor = item.Valor,
                                    StatusFotoID = 1
                                };
                                pedidoItemRepository.Save(pedidoItem);
                            }
                            if (carrinho.Cupom != null)
                            {
                                cupomService.Utilizar(carrinho.Cupom.Codigo);
                            }
                            return pedido;
                        }
                        else {
                            throw new Exception("Selecione uma cidade");
                        }
                    }
                    else {
                        throw new Exception("É necessário estar autenticado");
                    }
                }
                else {
                    throw new Exception("Data/Hora solicitada inválida");
                }
            }
            else {
                throw new Exception("O carrinho está vazio");
            }
        }
        //-----------------------------------------------------------------------------

        public Pedido PagarPedidoADM(int IdPedido, string cardHash, int Parcelas)
        {
            Pedido pedido = new Pedido();
            pedido = pedidoRepository.Get(IdPedido);

            // SALVAR PARCELAS SE FOR DIFERENTE
            if(pedido.Parcela != Parcelas)
            {
                pedido.Parcela = Parcelas;
                pedidoRepository.Save(pedido);
            }

            PagarMeService.DefaultApiKey = Domain.Core.Configuracoes.PagarMe_DefaultApiKey;

            Transaction transaction = new Transaction();

            transaction.Amount = int.Parse(pedido.ValorTotal.ToString().Replace(".", "").Replace(",", ""));
            transaction.Installments = Parcelas;

            transaction.AcquirerName = pedido.Cliente.Nome.ToUpper();

            transaction.Customer = new Customer
            {
                Email = pedido.Cliente.Email,
                DocumentNumber = (!string.IsNullOrEmpty(pedido.Cliente.Documento) && pedido.Cliente.Documento != "N/I") ? pedido.Cliente.Documento : null,
                Name = pedido.Cliente.Nome + " " + pedido.Cliente.RazaoSocial,
                Phone = new Phone
                {
                    Ddd = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", "") : "011",
                    Number = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Remove(0, 5) : "999999999",
                },
                Address = new Address
                {
                    City = (pedido.Cliente.Cidade != null) ? pedido.Cliente.Cidade.Nome : "N/I",
                    State = (pedido.Cliente.Estado != null) ? pedido.Cliente.Estado.Nome : "N/I",
                    Street = (!string.IsNullOrEmpty(pedido.Cliente.Logradouro)) ? pedido.Cliente.Logradouro : "N/I",
                    StreetNumber = (!string.IsNullOrEmpty(pedido.Cliente.Numero)) ? pedido.Cliente.Numero : "001",
                    Neighborhood = (!string.IsNullOrEmpty(pedido.Cliente.Bairro)) ? pedido.Cliente.Bairro : "N/I",
                    Country = "BR",
                    Zipcode = (!string.IsNullOrEmpty(pedido.Cliente.CEP)) ? pedido.Cliente.CEP : "01507000"
                }
            };

            #region META DATAS

            transaction.Metadata["IdPedido"] = pedido.ID;
            transaction.Metadata["NrPedido"] = pedido.Numero;

            string Produtos = "";
            foreach (var Produto in pedido.PedidoItems)
            {
                Produtos += Produto.ProdutoTamanho.Produto.Nome + " " + Produto.ProdutoTamanho.Tamanho.Nome + " - ";
            }

            transaction.Metadata["Produtos"] = Produtos;

            #endregion

            transaction.CardHash = cardHash;

            try
            {
                transaction.Save();

                TransactionStatus status = transaction.Status;

                switch (status)
                {
                    case TransactionStatus.Paid:
                        pedido.DataProcessamento = DateTime.Now;
                        pedido.DataPagamento = DateTime.Now;
                        pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                        pedido.ValorPago = pedido.ValorTotal;
                        pedido.Mensagem = status.ToString();
                        pedido.Retorno = status.ToString();
                        pedido.TID = transaction.Id.ToString();
                        pedidoRepository.Save(pedido);
                        break;
                    case TransactionStatus.Refused:
                        pedido.Mensagem = status.ToString();
                        pedido.TID = transaction.Id.ToString();
                        pedido.Retorno = status.ToString();

                        break;
                }

            }
            catch (PagarMeException ex)
            {
                var Erro = ex.Error;

                pedido.Retorno = "Error";

                string RetornoErros = "";

                foreach(var item in Erro.Errors)
                {
                    RetornoErros += item.Message + " / ";
                }
                pedido.Mensagem = RetornoErros;
            }

            return pedido;
        }
        public Pedido PagarPedidoRedeCard(int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela, Carrinho carrinho, int cidadeID, int clienteID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string Ambiente, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "SITE", string origemSite = "COROAS PARA VELÓRIO", string origemForma = "ONLINE")
        {
            var pedido = CriarPedido(carrinho, cidadeID, clienteID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, parentesco, pessoaHomenageada, telefoneContato);

            RetornoRedeCard retornoRede = new RetornoRedeCard();

            if (Ambiente == "Homolog")
            {
                retornoRede = RedeCardService.PagarHomolog(pedido.ID, pedido.ValorTotal, bandeira, nome, numero, mes, ano, codigo, parcela);
            }
            if (Ambiente == "Producao")
            {
                retornoRede = RedeCardService.PagarProducao(pedido.ID, pedido.ValorTotal, bandeira, nome, numero, mes, ano, codigo, parcela);
            }

            switch (retornoRede.Codret)
            {
                case 0:
                    pedido.DataProcessamento = DateTime.Now;
                    pedido.DataPagamento = DateTime.Now;
                    pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                    pedido.StatusProcessamento = Domain.Entities.Pedido.TodosStatusProcessamento.Criado;
                    pedido.ValorPago = pedido.ValorTotal;
                    pedido.Origem = origem;
                    pedido.OrigemForma = origemForma;
                    pedido.OrigemSite = origemSite;
                    pedido.TID = retornoRede.TID;
                    pedido.Retorno = retornoRede.Codret.ToString();
                    pedido.Mensagem = retornoRede.MsgRet;
                    pedido.Parcela = parcela;
                    pedidoRepository.Save(pedido);
                    break;
                case 80:
                case 58:
                case 84:
                    pedido.Retorno = retornoRede.Codret.ToString();
                    pedido.Mensagem = retornoRede.MsgRet;
                    pedido.Origem = origem;
                    pedido.OrigemForma = origemForma;
                    pedido.OrigemSite = origemSite;
                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                    pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                    pedido.StatusCancelamento = TodosStatusCancelamento.NaoAutorizado;
                    pedido.Parcela = parcela;
                    pedidoRepository.Save(pedido);
                    break;
                case 99:
                default:
                    pedido.Retorno = retornoRede.Codret.ToString();
                    pedido.Origem = origem;
                    pedido.OrigemForma = origemForma;
                    pedido.OrigemSite = origemSite;
                    pedido.Mensagem = retornoRede.MsgRet != null ? retornoRede.MsgRet : "";
                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                    pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                    pedido.StatusCancelamento = TodosStatusCancelamento.NaoAutorizado;
                    pedido.Parcela = parcela;
                    pedidoRepository.Save(pedido);
                    break;
            }

            return pedido;
        }
        public Pedido PagarPedido(int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela, Carrinho carrinho, int cidadeID, int clienteID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string cardHash, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "SITE", string origemSite = "COROAS PARA VELÓRIO", string origemForma = "ONLINE")
        {
            var pedido = CriarPedido(carrinho, cidadeID, clienteID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, parentesco, pessoaHomenageada, telefoneContato);

            PagarMeService.DefaultApiKey = Domain.Core.Configuracoes.PagarMe_DefaultApiKey;

            Transaction transaction = new Transaction();

            transaction.Amount = int.Parse(String.Format("{0:0.00}", pedido.ValorTotal).Replace(".", "").Replace(",", ""));
            transaction.Installments = parcela;

            transaction.AcquirerName = pedido.Cliente.Nome.ToUpper();

            transaction.Customer = new Customer
            {
                Email = pedido.Cliente.Email,
                DocumentNumber = (!string.IsNullOrEmpty(pedido.Cliente.Documento) && pedido.Cliente.Documento != "N/I") ? pedido.Cliente.Documento : null,
                Name = pedido.Cliente.Nome + " " + pedido.Cliente.RazaoSocial,
                Phone = new Phone
                {
                    Ddd = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", "") : "011",
                    Number = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Remove(0, 5) : "999999999",
                },
                Address = new Address
                {
                    City = (pedido.Cliente.Cidade != null) ? pedido.Cliente.Cidade.Nome : "N/I",
                    State = (pedido.Cliente.Estado != null) ? pedido.Cliente.Estado.Nome : "N/I",
                    Street = (!string.IsNullOrEmpty(pedido.Cliente.Logradouro)) ? pedido.Cliente.Logradouro : "N/I",
                    StreetNumber = (!string.IsNullOrEmpty(pedido.Cliente.Numero)) ? pedido.Cliente.Numero : "001",
                    Neighborhood = (!string.IsNullOrEmpty(pedido.Cliente.Bairro)) ?  pedido.Cliente.Bairro : "N/I",
                    Country = "BR",
                    Zipcode = (!string.IsNullOrEmpty(pedido.Cliente.CEP)) ? pedido.Cliente.CEP: "01507000"
                }
            };

            #region META DATAS

            transaction.Metadata["IdPedido"] = pedido.ID;
            transaction.Metadata["NrPedido"] = pedido.Numero;

            string Produtos = "";
            foreach (var Produto in pedido.PedidoItems)
            {
                Produtos += Produto.ProdutoTamanho.Produto.Nome + " " + Produto.ProdutoTamanho.Tamanho.Nome + " - ";
            }

            transaction.Metadata["Produtos"] = Produtos;

            #endregion

            transaction.CardHash = cardHash;
            
            transaction.Save();

            TransactionStatus status = transaction.Status;

            switch (status)
            {
                case TransactionStatus.Paid:
                    pedido.DataProcessamento = DateTime.Now;
                    pedido.DataPagamento = DateTime.Now;
                    pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                    pedido.StatusProcessamento = Domain.Entities.Pedido.TodosStatusProcessamento.Criado;
                    pedido.ValorPago = pedido.ValorTotal;
                    pedido.Origem = origem;
                    pedido.OrigemForma = origemForma;
                    pedido.OrigemSite = origemSite;
                    pedido.Mensagem = status.ToString();
                    pedido.Retorno = status.ToString();
                    pedido.Parcela = parcela;
                    pedido.TID = transaction.Id.ToString();
                    pedidoRepository.Save(pedido);
                    break;
                default:
                    pedido.Origem = origem;
                    pedido.OrigemForma = origemForma;
                    pedido.OrigemSite = origemSite;
                    pedido.Mensagem = status.ToString();
                    pedido.Retorno = status.ToString();
                    pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                    pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                    pedido.StatusCancelamento = TodosStatusCancelamento.NaoAutorizado;
                    pedido.Parcela = parcela;
                    pedido.TID = transaction.Id.ToString();
                    pedidoRepository.Save(pedido);
                    break;
            }
            
            return pedido;
        }
        public Pedido PagarPedidoNovo(Pedido pedido)
        {
            if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
            {
                try
                {
                    BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());
                    pedido.Mensagem = "Paid";
                    pedido.Retorno = "Paid";

                }
                catch (Exception ex)
                {
                    pedido.ErroPagamento = ex.Message;
                }
            }
            if (pedido.FormaPagamento == Pedido.FormasPagamento.CartaoCredito)
            {
                try
                {
                    PagarMeService.DefaultApiKey = Domain.Core.Configuracoes.PagarMe_DefaultApiKey;

                    Transaction transaction = new Transaction();

                    transaction.Amount = int.Parse(pedido.ValorTotal.ToString().Replace(".", "").Replace(",", ""));
                    transaction.Installments = pedido.Parcela;

                    transaction.AcquirerName = pedido.Cliente.Nome.ToUpper();

                    transaction.Customer = new Customer
                    {
                        Email = pedido.Cliente.Email,
                        DocumentNumber = (!string.IsNullOrEmpty(pedido.Cliente.Documento) && pedido.Cliente.Documento != "N/I") ? pedido.Cliente.Documento : null,
                        Name = pedido.Cliente.Nome + " " + pedido.Cliente.RazaoSocial,
                        Phone = new Phone
                        {
                            Ddd = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", "") : "011",
                            Number = (!string.IsNullOrEmpty(pedido.Cliente.TelefoneContato)) ? pedido.Cliente.TelefoneContato.Remove(0, 5) : "999999999",
                        },
                        Address = new Address
                        {
                            City = (pedido.Cliente.Cidade != null) ? pedido.Cliente.Cidade.Nome : "N/I",
                            State = (pedido.Cliente.Estado != null) ? pedido.Cliente.Estado.Nome : "N/I",
                            Street = (!string.IsNullOrEmpty(pedido.Cliente.Logradouro)) ? pedido.Cliente.Logradouro : "N/I",
                            StreetNumber = (!string.IsNullOrEmpty(pedido.Cliente.Numero)) ? pedido.Cliente.Numero : "001",
                            Neighborhood = (!string.IsNullOrEmpty(pedido.Cliente.Bairro)) ? pedido.Cliente.Bairro : "N/I",
                            Country = "BR",
                            Zipcode = (!string.IsNullOrEmpty(pedido.Cliente.CEP)) ? pedido.Cliente.CEP : "01507000"
                        }
                    };

                    #region META DATAS

                    transaction.Metadata["IdPedido"] = pedido.ID;
                    transaction.Metadata["NrPedido"] = pedido.Numero;

                    string Produtos = "";
                    foreach (var Produto in pedido.PedidoItems)
                    {
                        Produtos += Produto.ProdutoTamanho.Produto.Nome + " " + Produto.ProdutoTamanho.Tamanho.Nome + " - ";
                    }

                    transaction.Metadata["Produtos"] = Produtos;

                    #endregion

                    transaction.CardHash = pedido.cardhash;

                    transaction.Save();

                    TransactionStatus status = transaction.Status;

                    switch (status)
                    {
                        case TransactionStatus.Paid:
                            pedido.DataProcessamento = DateTime.Now;
                            pedido.DataPagamento = DateTime.Now;
                            pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                            pedido.StatusProcessamento = Domain.Entities.Pedido.TodosStatusProcessamento.Criado;
                            pedido.ValorPago = pedido.ValorTotal;
                            pedido.Mensagem = status.ToString();
                            pedido.Retorno = status.ToString();
                            pedido.TID = transaction.Id.ToString();
                            pedidoRepository.Save(pedido);
                            break;
                        default:
                            pedido.Mensagem = status.ToString();
                            pedido.Retorno = status.ToString();
                            pedido.StatusProcessamento = Pedido.TodosStatusProcessamento.Cancelado;
                            pedido.StatusEntrega = TodosStatusEntrega.Cancelado;
                            pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
                            pedido.StatusCancelamento = TodosStatusCancelamento.NaoAutorizado;
                            pedido.TID = transaction.Id.ToString();
                            pedidoRepository.Save(pedido);
                            break;
                    }
                }
                catch (PagarMeException ex)
                {
                    var Erros = "";

                    foreach (var item in ex.Error.Errors)
                    {
                        Erros += item.Message + " ";
                    }
                    pedido.ErroPagamento = Erros;
                }
            }

            return pedido;
        }
        //-----------------------------------------------------------------------------
        public List<FornecedorLocal> FornecedoresAcima(int IdFornecedor, int IdPedido, OrigemPedido TipoPedido)
        {
            if (TipoPedido == OrigemPedido.Cliente)
            {
                PedidoCPV = pedidoRepository.Get(IdPedido);
                var ListaFornecedores = PedidoCPV.Local.FornecedorLocals.OrderBy(r => r.Ordem).ToList();

                int CountIndex = 0;
                foreach (var Item in ListaFornecedores)
                {
                    if (CountIndex < RetornaMeuIndex(IdFornecedor, IdPedido, OrigemPedido.Cliente))
                    {
                        ListaFornecedoresAcima.Add(Item);
                        CountIndex++;
                    }
                }

                return ListaFornecedoresAcima;
            }
            else
            {
                PedidoEmpresa = pedidoEmpresaEntidadeRepository.GetByExpression(r => r.ID == IdPedido).First();
                var ListaFornecedores = PedidoEmpresa.Local.FornecedorLocals.OrderBy(r => r.Ordem).ToList();

                int CountIndex = 0;
                foreach (var Item in ListaFornecedores)
                {
                    if (CountIndex < RetornaMeuIndex(IdFornecedor, IdPedido, OrigemPedido.Empresa))
                    {
                        ListaFornecedoresAcima.Add(Item);
                        CountIndex++;
                    }
                }

                return ListaFornecedoresAcima;
            }
        }
        //-----------------------------------------------------------------------------
        public List<PedidoItem> ListPedidosItemPorEstado(DateTime DataIncial, DateTime DataFinal, List<Estado> lstEstado)
        {
            List<int> lstIdEstado = new List<int>();
            lstIdEstado = lstEstado.Select(r => r.ID).ToList();

            return pedidoItemRepository.GetByExpression(r => (r.Pedido.DataCriacao >= DataIncial && r.Pedido.DataCriacao <= DataFinal) && lstIdEstado.Contains(r.Pedido.EstadoID)).ToList();
        }
        //-----------------------------------------------------------------------------
        public List<Pedido> ListPedidosPorEstado(DateTime DataIncial, DateTime DataFinal, List<Estado> lstEstado)
        {
            List<int> lstIdEstado = new List<int>();
            lstIdEstado = lstEstado.Select(r => r.ID).ToList();

            return pedidoRepository.GetByExpression(r => (r.DataCriacao >= DataIncial && r.DataCriacao <= DataFinal) && lstIdEstado.Contains(r.EstadoID)).ToList();
        }
        //-----------------------------------------------------------------------------
        public List<Repasse> ListRepassePorEstado(DateTime DataIncial, DateTime DataFinal, List<Estado> lstEstado)
        {
            List<int> lstIdEstado = new List<int>();
            lstIdEstado = lstEstado.Select(r => r.ID).ToList();

            return repasseRepository.GetByExpression(r => (r.PedidoItem.Pedido.DataCriacao >= DataIncial && r.PedidoItem.Pedido.DataCriacao <= DataFinal) && lstIdEstado.Contains(r.PedidoItem.Pedido.EstadoID) && r.PedidoItem.Pedido.StatusProcessamentoID != (int)Pedido.TodosStatusProcessamento.Cancelado && r.StatusID != (int)Repasse.TodosStatus.Cancelado).ToList();
        }
        
        #endregion
        
    }

    //=================================================================================
}
