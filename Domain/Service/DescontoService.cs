﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Domain.Service
{
    public class DescontoService
    {

        public static decimal Calcular(Carrinho carrinho)
        {
            decimal valor = 0;
            if (!carrinho.Vazio)
            {
                if (carrinho.Cupom != null)
                {
                    switch (carrinho.Cupom.Tipo)
                    {
                        case Cupom.Tipos.Porcentagem:
                            valor = carrinho.Subtotal * carrinho.Cupom.Valor / 100;
                            break;
                        case Cupom.Tipos.Valor:
                            valor = carrinho.Cupom.Valor;
                            break;
                    }
                }
                else
                {
                    var context = new COROASEntities();

                    var temPromocao = false;
                    
                    var promocaoRepository = new PersistentRepository<Promocao>(context);
                    var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now).OrderByDescending(c => c.DataInicio).FirstOrDefault();

                    if (promocao != null)
                    {
                        foreach (var item in carrinho.Itens)
                        {
                            if (promocao.PromocaoProdutoes.Count(c => c.ProdutoID == item.ProdutoTamanho.ProdutoID) > 0)
                            {
                                temPromocao = true;
                                valor += item.Valor * promocao.Desconto / 100;
                            }
                        }
                    }

                    if (!temPromocao)
                    {
                        var descontoRepository = new PersistentRepository<Desconto>(context);
                        var desconto = descontoRepository.GetByExpression(d => d.Ativo).FirstOrDefault();
                        if (desconto != null)
                        {
                            switch (desconto.Regra)
                            {
                                case Desconto.Regras.EmTodoSite:
                                    switch (desconto.Tipo)
                                    {
                                        case Desconto.Tipos.Percentual:
                                            valor = carrinho.Subtotal * desconto.Valor / 100;
                                            break;
                                        case Desconto.Tipos.Valor:
                                            valor = desconto.Valor;
                                            break;
                                    }
                                    break;
                                case Desconto.Regras.APartirDoSegundoProdutoMenorValor:
                                    switch (desconto.Tipo)
                                    {
                                        case Desconto.Tipos.Percentual:
                                            valor = carrinho.Itens.OrderByDescending(i => i.Valor).Skip(1).Sum(i => i.Valor * desconto.Valor / 100);
                                            break;
                                        case Desconto.Tipos.Valor:
                                            valor = carrinho.Itens.OrderByDescending(i => i.Valor).Skip(1).Sum(i => desconto.Valor);
                                            break;
                                    }
                                    break;
                                case Desconto.Regras.APartirDoSegundoProdutoMaiorValor:
                                    switch (desconto.Tipo)
                                    {
                                        case Desconto.Tipos.Percentual:
                                            valor = carrinho.Itens.OrderBy(i => i.Valor).Skip(1).Sum(i => i.Valor * desconto.Valor / 100);
                                            break;
                                        case Desconto.Tipos.Valor:
                                            valor = carrinho.Itens.OrderBy(i => i.Valor).Skip(1).Sum(i => desconto.Valor);
                                            break;
                                    }
                                    break;
                                case Desconto.Regras.APartirDeValor:
                                    if (carrinho.Subtotal >= desconto.Referencia)
                                    {
                                        switch (desconto.Tipo)
                                        {
                                            case Desconto.Tipos.Percentual:
                                                valor = carrinho.Subtotal * desconto.Valor / 100;
                                                break;
                                            case Desconto.Tipos.Valor:
                                                valor = desconto.Valor;
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
                valor = valor > carrinho.Subtotal ? carrinho.Subtotal : valor;
                return valor;
            }
            return valor;
        }

    }
}
