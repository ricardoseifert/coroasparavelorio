﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Xml;
using System.Data;
using System.IO;

namespace Domain.Service
{
    public class ShoplineService
    {

        //public string GerarDC(Pedido pedido)
        //{
        //    Itaucripto.cripto cripto = new Itaucripto.cripto();
        //    return cripto.geraDados(Core.Configuracoes.SHOPLINE_CODIGO_EMPRESA, pedido.ID.ToString(), pedido.ValorTotal.ToString("N2"), pedido.Observacoes, Core.Configuracoes.SHOPLINE_CHAVE, (pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica ? pedido.Cliente.Nome : pedido.Cliente.RazaoSocial), (pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica ? "01" : "02"), pedido.Cliente.Documento.Replace(".", "").Replace("-", "").Replace("-", "").Trim(), pedido.Cliente.Logradouro + " " + pedido.Cliente.Numero + " " + pedido.Cliente.Complemento, pedido.Cliente.Bairro, pedido.Cliente.CEP.Replace("-", ""), pedido.Cliente.Cidade.Nome, pedido.Cliente.Estado.Sigla, DateTime.Now.ToString("ddMMyyyy"), "/", "", "", "");
        //}

        //public Pedido Retorno(string DC)
        //{

        //    Itaucripto.cripto cripto = new Itaucripto.cripto();
        //    cripto.decripto(DC, Domain.Core.Configuracoes.SHOPLINE_CHAVE);

        //    var pedidoID = cripto.retornaPedido();
        //    var tipPag = cripto.retornaTipPag().ToString();

        //    var context = new COROASEntities();
        //    var pedidoRepository = new PersistentRepository<Pedido>(context);
        //    var pedido = pedidoRepository.Get(pedidoID);
        //    if (pedido != null)
        //    {
        //        if (pedido.ShoplineTipPag != tipPag)
        //        {
        //            switch (tipPag)
        //            {
        //                case "01":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.TransferenciaBancaria;
        //                    break;
        //                case "02":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.Boleto;
        //                    break;
        //                case "03":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.CartaoCredito;
        //                    break;
        //            }
        //            pedido.ShoplineTipPag = tipPag;
        //            pedido.DataProcessamento = DateTime.Now;
        //            pedidoRepository.Save(pedido);
        //        }
        //    }

        //    pedido = AtualizaSituacaoPagamento(pedido.ID.ToString());
        //    return pedido;
        //}

        //public static void AtualizaSituacaoPagamento()
        //{
        //    var context = new COROASEntities();
        //    var pedidoRepository = new PersistentRepository<Pedido>(context);
        //    foreach (var pedido in pedidoRepository.GetByExpression(t => t.StatusPagamentoID == (int)Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento && t.MeioPagamentoID == (int)Pedido.MeiosPagamento.Shopline))
        //    {
        //        AtualizaSituacaoPagamento(pedido.ID.ToString());
        //        //TODO: Cancelar transações antigas
        //    }
        //}

        //public static Pedido AtualizaSituacaoPagamento(string pedidoID)
        //{
        //    var context = new COROASEntities();
        //    var pedidoRepository = new PersistentRepository<Pedido>(context);

        //    Itaucripto.cripto cripto = new Itaucripto.cripto();
        //    var dados = cripto.geraConsulta(Domain.Core.Configuracoes.SHOPLINE_CODIGO_EMPRESA, pedidoID, "1", Domain.Core.Configuracoes.SHOPLINE_CHAVE);
        //    var xmlString = Domain.Core.Funcoes.HttpPost("https://shopline.itau.com.br/shopline/consulta.aspx", "DC=" + dados);

        //    var _valor = "";
        //    var _tipPag = "";
        //    var _sitPag = "";
        //    var _dtPag = "";
        //    var _codAut = "";
        //    var _numID = "";
        //    var _compVend = "";
        //    var _tipCart = "";

        //    var settings = new XmlReaderSettings();
        //    settings.DtdProcessing = DtdProcessing.Parse;
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(XmlReader.Create(new StringReader(xmlString), settings));
        //    foreach (DataTable tbl in ds.Tables)
        //    {
        //        foreach (DataRow dr in tbl.Rows)
        //        {
        //            switch (dr[0].ToString())
        //            {
        //                case "Valor":
        //                    _valor = dr[1].ToString();
        //                    break;
        //                case "tipPag":
        //                    _tipPag = dr[1].ToString();
        //                    break;
        //                case "sitPag":
        //                    _sitPag = dr[1].ToString();
        //                    break;
        //                case "dtPag":
        //                    _dtPag = dr[1].ToString();
        //                    break;
        //                case "codAut":
        //                    _codAut = dr[1].ToString();
        //                    break;
        //                case "numId":
        //                    _numID = dr[1].ToString();
        //                    break;
        //                case "compVend":
        //                    _compVend = dr[1].ToString();
        //                    break;
        //                case "tipCart":
        //                    _tipCart = dr[1].ToString();
        //                    break;
        //            }
        //        }
        //    }

        //    var pedido = pedidoRepository.Get(Int32.Parse(pedidoID));
        //    if (pedido != null)
        //    {
        //        if (_sitPag == "00")
        //        {
        //            pedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
        //            if (_valor.Length > 0)
        //                pedido.ValorPago = Decimal.Parse(_valor, System.Globalization.NumberStyles.Currency);
        //            pedido.ShoplineCodAut = _codAut;
        //            pedido.ShoplineNumID = _numID;
        //            pedido.ShoplineCompVenda = _compVend;
        //            pedido.ShoplineTipCart = _tipCart;
        //            pedido.DataProcessamento = DateTime.Now;
        //            if (_dtPag.Length > 0)
        //            {
        //                pedido.DataPagamento = new DateTime(Int32.Parse(_dtPag.Substring(4, 4)), Int32.Parse(_dtPag.Substring(2, 2)), Int32.Parse(_dtPag.Substring(0, 2)));
        //            }

        //            //SE PAGOU, TEM Q ENVIAR EMAIL
        //            pedido.EnviarEmailPagamento();

        //        }
        //        else if (_sitPag != "00" && _sitPag != "04" && _sitPag != "05" && _sitPag != "06" && pedido.DataCriacao.AddDays(30) <= DateTime.Now && pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento)
        //        {
        //            pedido.StatusPagamento = Pedido.TodosStatusPagamento.Cancelado;
        //        }

        //        if (_tipPag.Length > 0)
        //        {
        //            pedido.ShoplineTipPag = _tipPag;
        //            switch (_tipPag)
        //            {
        //                case "01":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.TransferenciaBancaria;
        //                    break;
        //                case "02":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.Boleto;
        //                    break;
        //                case "03":
        //                    pedido.FormaPagamento = Pedido.FormasPagamento.CartaoCredito;
        //                    break;
        //            }
        //        }
        //        if (_sitPag.Length > 0)
        //            pedido.ShoplineSitPag = _sitPag;
        //        pedido.DataProcessamento = DateTime.Now;
        //        pedidoRepository.Save(pedido);


        //        if ((_sitPag == "01" || _sitPag == "03") && _tipPag == "00" && pedido.DataCriacao.AddHours(2) <= DateTime.Now && pedido.StatusPagamento == Pedido.TodosStatusPagamento.AguardandoPagamento)
        //        {
        //            var corpo = string.Format("O seguinte cliente escolheu como forma de pagamento o Shopline porém não finalizou escolhendo a forma de pagamento. É importante acompanhar ou entrar em contato com o cliente para cancelar o pedido\n\nOBS: Pedidos sem escolha de forma de pagamento no shopline, são cancelados automaticamente após 10 dias.\n\nNOME: {0}\n\nE-MAIL: {1}\n\nTELEFONE: {2}\n\nNo. DO PEDIDO:\n{3}\n\nID DO PEDIDO NO SHOPLINE:\n{4}", pedido.Cliente.Nome, pedido.Cliente.Email, pedido.Cliente.TelefoneContato, pedido.Numero, pedido.ID);
        //            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Não Responder", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FINANCEIRO, "FINANCEIRO", Domain.Core.Configuracoes.EMAIL_ATENDIMENTO, "BACKOFFICE", Domain.Core.Configuracoes.EMAIL_COPIA, "[BACKOFFICE] - Aviso de processamento Shopline", corpo, false);
        //        }
        //    }

        //    return pedido;
        //}

    }
}
