﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;

namespace Domain.Service
{
    public class Metricas
    {
        public decimal totalliquido { get; set; }
        public decimal totalRepasse { get; set; }

        public decimal TicketMedio { get; set; }

        public decimal QtdPedidos { get; set; }
        public decimal QtdPedidosBruto { get; set; }
        public decimal QtdPedidosCancelados { get; set; }

        public decimal TotalItensPedido { get; set; }
        public decimal TotalItensPedidoPJ { get; set; }
        public decimal TotalItensPedidoPF { get; set; }


        public decimal totalRepasseOnline { get; set; }
        public decimal totalRepasseOffline { get; set; }
        public decimal totalRepasseOnlineCPV { get; set; }
        public decimal totalRepasseOfflineCPV { get; set; }
        public decimal totalRepasseOnlineNET { get; set; }
        public decimal totalRepasseOfflineNET { get; set; }
        public decimal totalRepasseOnlineOutros { get; set; }
        public decimal totalRepasseOfflineOutros { get; set; }

        public decimal QtdMailConf { get; set; }

        public decimal QtdPedidosComAceiteFornecedor { get; set; }
        public decimal QtdPedidosComAceiteManual { get; set; }

        private List<Pedido> _ListaPedidosCancelados;
        public List<Pedido> ListaPedidosCancelados
        {
            get { return _ListaPedidosCancelados; }
            set { _ListaPedidosCancelados = value; }
        }

        public void PopulaMetricas(DateTime DataIncial, DateTime DataFinal)
        {



            totalliquido = 10;
        }
    }
}
