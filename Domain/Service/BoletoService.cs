﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoletoNet;
using System.IO;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.ComponentModel;
using System.Web.UI; 

namespace Domain.Service
{
    public class BoletoService
    {

        public enum Ocorrencias
        {
            INDEFINIDA = 0,
            [Description("ENTRADA CONFIRMADA")]
            ENTRADA_CONFIRMADA = 2,
            [Description("ENTRADA REJEITADA")]
            ENTRADA_REJEITADA = 3,
            [Description("ALTERAÇÃO DE DADOS - NOVA ENTRADA")]
            ALTERACAO_DE_DADOS_NOVA_ENTRADA = 4,
            [Description("ALTERAÇÃO DE DADOS – BAIXA")]
            ALTERACAO_DE_DADOS_BAIXA = 5,
            [Description("LIQUIDAÇÃO NORMAL")]
            LIQUIDACAO_NORMAL = 6,
            [Description("LIQUIDAÇÃO PARCIAL – COBRANÇA INTELIGENTE (B2B)")]
            LIQUIDACAO_PARCIAL_COBRANÇA_INTELIGENTE_B2B = 7,
            [Description("LIQUIDAÇÃO EM CARTÓRIO")]
            LIQUIDACAO_EM_CARTORIO = 8,
            [Description("BAIXA SIMPLES")]
            BAIXA_SIMPLES = 9,
            [Description("BAIXA POR TER SIDO LIQUIDADO")]
            BAIXA_POR_TER_SIDO_LIQUIDADO = 10,
            [Description("EM SER")]
            EM_SER = 11,
            [Description("ABATIMENTO CONCEDIDO")]
            ABATIMENTO_CONCEDIDO = 12,
            [Description("ABATIMENTO CANCELADO")]
            ABATIMENTO_CANCELADO = 13,
            [Description("VENCIMENTO ALTERADO")]
            VENCIMENTO_ALTERADO = 14,
            [Description("BAIXAS REJEITADAS")]
            BAIXAS_REJEITADAS = 15,
            [Description("INSTRUÇÕES REJEITADAS")]
            INSTRUCOES_REJEITADAS = 16,
            [Description("ALTERAÇÃO DE DADOS REJEITADOS")]
            ALTERACAO_DE_DADOS_REJEITADOS = 17,
            [Description("COBRANÇA CONTRATUAL - INSTRUÇÕES/ALTERAÇÕES REJEITADAS/PENDENTES")]
            COBRANÇA_CONTRATUAL_INSTRUCOES_ALTERACOES_REJEITADAS_PENDENTES = 18,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE PROTESTO")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_PROTESTO = 19,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE SUSTAÇÃO DE PROTESTO /TARIFA")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_SUSTACAO_DE_PROTESTO_TARIFA = 20,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE NÃO PROTESTAR")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_NAO_PROTESTAR = 21,
            [Description("OCORRÊNCIA NÃO IDENTIFICADA (TESTES)")]
            OCORRENCIA_NAO_IDENTIFICADA_TESTES = 22,
            [Description("TÍTULO ENVIADO A CARTÓRIO/TARIFA")]
            TITULO_ENVIADO_A_CARTORIO_TARIFA = 23,
            [Description("INSTRUÇÃO DE PROTESTO REJEITADA / SUSTADA / PENDENTE")]
            INSTRUCAO_DE_PROTESTO_REJEITADA_SUSTADA_PENDENTE = 24,
            [Description("ALEGAÇÕES DO SACADO")]
            ALEGACOES_DO_SACADO = 25,
            [Description("TARIFA DE AVISO DE COBRANÇA")]
            TARIFA_DE_AVISO_DE_COBRANCA = 26,
            [Description("TARIFA DE EXTRATO POSIÇÃO (B40X)")]
            TARIFA_DE_EXTRATO_POSICAO_B40X = 27,
            [Description("TARIFA DE RELAÇÃO DAS LIQUIDAÇÕES")]
            TARIFA_DE_RELACAO_DAS_LIQUIDACOES = 28,
            [Description("TARIFA DE MANUTENÇÃO DE TÍTULOS VENCIDOS")]
            TARIFA_DE_MANUTENCAO_DE_TITULOS_VENCIDOS = 29,
            [Description("DÉBITO MENSAL DE TARIFAS")]
            DEBITO_MENSAL_DE_TARIFAS = 30,
            [Description("BAIXA POR TER SIDO PROTESTADO")]
            BAIXA_POR_TER_SIDO_PROTESTADO = 32,
            [Description("CUSTAS DE PROTESTO")]
            CUSTAS_DE_PROTESTO = 33,
            [Description("CUSTAS DE SUSTAÇÃO")]
            CUSTAS_DE_SUSTACAO = 34,
            [Description("CUSTAS DE CARTÓRIO DISTRIBUIDOR")]
            CUSTAS_DE_CARTORIO_DISTRIBUIDOR = 35,
            [Description("CUSTAS DE EDITAL")]
            CUSTAS_DE_EDITAL = 36,
            [Description("TARIFA DE EMISSÃO DE BOLETO/TARIFA DE ENVIO DE DUPLICATA")]
            TARIFA_DE_EMISSAO_DE_BOLETO_TARIFA_DE_ENVIO_DE_DUPLICATA = 37,
            [Description("TARIFA DE INSTRUÇÃO")]
            TARIFA_DE_INSTRUCAO = 38,
            [Description("TARIFA DE OCORRÊNCIAS")]
            TARIFA_DE_OCORRENCIAS = 39,
            [Description("TARIFA MENSAL DE EMISSÃO DE BOLETO/TARIFA MENSAL DE ENVIO DE DUPLICATA")]
            TARIFA_MENSAL_DE_EMISSAO_DE_BOLETO_TARIFA_MENSAL_DE_ENVIO_DE_DUPLICATA = 40,
            [Description("DÉBITO MENSAL DE TARIFAS – EXTRATO DE POSIÇÃO (B4EP/B4OX)")]
            DEBITO_MENSAL_DE_TARIFAS_EXTRATO_DE_POSICAO_B4EP_B4OX = 41,
            [Description("DÉBITO MENSAL DE TARIFAS – OUTRAS INSTRUÇÕES")]
            DEBITO_MENSAL_DE_TARIFAS_OUTRAS_INSTRUCOES = 42,
            [Description("DÉBITO MENSAL DE TARIFAS – MANUTENÇÃO DE TÍTULOS VENCIDOS")]
            DEBITO_MENSAL_DE_TARIFAS_MANUTENCAO_DE_TITULOS_VENCIDOS = 43,
            [Description("DÉBITO MENSAL DE TARIFAS – OUTRAS OCORRÊNCIAS")]
            DEBITO_MENSAL_DE_TARIFAS_OUTRAS_OCORRENCIAS = 44,
            [Description("DÉBITO MENSAL DE TARIFAS – PROTESTO")]
            DEBITO_MENSAL_DE_TARIFAS_PROTESTO = 45,
            [Description("DÉBITO MENSAL DE TARIFAS – SUSTAÇÃO DE PROTESTO")]
            DEBITO_MENSAL_DE_TARIFAS_SUSTACAO_DE_PROTESTO = 46,
            [Description("BAIXA COM TRANSFERÊNCIA PARA DESCONTO")]
            BAIXA_COM_TRANSFERENCIA_PARA_DESCONTO = 47,
            [Description("CUSTAS DE SUSTAÇÃO JUDICIAL")]
            CUSTAS_DE_SUSTACAO_JUDICIAL = 48,
            [Description("TARIFA MENSAL REF A ENTRADAS BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_REF_A_ENTRADAS_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 51,
            [Description("TARIFA MENSAL BAIXAS NA CARTEIRA")]
            TARIFA_MENSAL_BAIXAS_NA_CARTEIRA = 52,
            [Description("TARIFA MENSAL BAIXAS EM BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_BAIXAS_EM_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 53,
            [Description("TARIFA MENSAL DE LIQUIDAÇÕES NA CARTEIRA")]
            TARIFA_MENSAL_DE_LIQUIDACOES_NA_CARTEIRA = 54,
            [Description("TARIFA MENSAL DE LIQUIDAÇÕES EM BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_DE_LIQUIDACOES_EM_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 55,
            [Description("CUSTAS DE IRREGULARIDADE")]
            CUSTAS_DE_IRREGULARIDADE = 56,
            [Description("INSTRUÇÃO CANCELADA")]
            INSTRUCAO_CANCELADA = 57,
            [Description("BAIXA POR CRÉDITO EM C/C ATRAVÉS DO SISPAG")]
            BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG = 59,
            [Description("ENTRADA REJEITADA CARNÊ")]
            ENTRADA_REJEITADA_CARNE = 60,
            [Description("TARIFA EMISSÃO AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)")]
            TARIFA_EMISSAO_AVISO_DE_MOVIMENTACAO_DE_TITULOS_2154 = 61,
            [Description("DÉBITO MENSAL DE TARIFA - AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)")]
            DEBITO_MENSAL_DE_TARIFA_AVISO_DE_MOVIMENTACAO_DE_TITULOS_2154 = 62,
            [Description("TÍTULO SUSTADO JUDICIALMENTE")]
            TITULO_SUSTADO_JUDICIALMENTE = 63,
            [Description("ENTRADA CONFIRMADA COM RATEIO DE CRÉDITO")]
            ENTRADA_CONFIRMADA_COM_RATEIO_DE_CREDITO = 64,
            [Description("CHEQUE DEVOLVIDO")]
            CHEQUE_DEVOLVIDO = 69,
            [Description("ENTRADA REGISTRADA, AGUARDANDO AVALIAÇÃO")]
            ENTRADA_REGISTRADA_AGUARDANDO_AVALIACAO = 71,
            [Description("BAIXA POR CRÉDITO EM C/C ATRAVÉS DO SISPAG SEM TÍTULO CORRESPONDENTE")]
            BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG_SEM_TITULO_CORRESPONDENTE = 72,
            [Description("CONFIRMAÇÃO DE ENTRADA NA COBRANÇA SIMPLES – ENTRADA NÃO ACEITA NA COBRANÇA CONTRATUAL")]
            CONFIRMACAO_DE_ENTRADA_NA_COBRANCA_SIMPLES_ENTRADA_NAO_ACEITA_NA_COBRANCA_CONTRATUAL = 73,
            [Description("CHEQUE COMPENSADO")]
            CHEQUE_COMPENSADO = 76
        }

        private IPersistentRepository<Domain.Entities.Boleto> boletoRepository;
        private IPersistentRepository<Domain.Entities.Pedido> pedidoRepository;
        
        private IPersistentRepository<Domain.Entities.Faturamento> faturaRepository;

        public BoletoService(ObjectContext context)
        {
            boletoRepository = new PersistentRepository<Domain.Entities.Boleto>(context);
            pedidoRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            
            faturaRepository = new PersistentRepository<Domain.Entities.Faturamento>(context);
        }

        public static string GerarCodigo(Domain.Entities.Boleto boleto)
        {
            var cedente = new Cedente(boleto.CedenteCNPJ, boleto.CedenteRazaoSocial, boleto.CedenteAgencia, boleto.CedenteContaCorrente.Substring(0, 5));
            cedente.Codigo = boleto.CedenteCodigo.ToString();

            var _boleto = new BoletoNet.Boleto(boleto.DataVencimento, (decimal)boleto.Valor, boleto.Carteira, boleto.NossoNumero, cedente);
            _boleto.NumeroDocumento = boleto.NumeroDocumento;

            _boleto.Sacado = new Sacado(boleto.SacadoDocumento, boleto.SacadoNome);
            _boleto.Sacado.Endereco.End = boleto.SacadoEndereco;
            _boleto.Sacado.Endereco.Bairro = boleto.SacadoBairro;
            _boleto.Sacado.Endereco.Cidade = boleto.SacadoCidade;
            _boleto.Sacado.Endereco.CEP = boleto.SacadoCEP;
            _boleto.Sacado.Endereco.UF = boleto.SacadoUF;
            _boleto.EspecieDocumento = new EspecieDocumento_Itau();

            var boletoBancario = new BoletoBancario();
            boletoBancario.CodigoBanco = 341;
            boletoBancario.Boleto = _boleto;
            boletoBancario.MostrarCodigoCarteira = true;
            boletoBancario.MostrarComprovanteEntrega = true;
            boletoBancario.Boleto.Valida();

            return boletoBancario.Boleto.CodigoBarra.LinhaDigitavel;
        }

        public List<Retorno> ProcessarRetorno(Stream stream)
        {
            var retornos = new List<Retorno>();

            StreamReader reader = new StreamReader(stream);

            //Valida Cabeçalho
            var header = reader.ReadLine();
            if (header.Length < 400)
            {
                reader.Close();
                throw new Exception("Arquivo de retorno inválido.");
            }
            else if (header.Substring(0, 9) != "02RETORNO")
            {
                reader.Close();
                throw new Exception("Arquivo de retorno inválido.");
            }

            var linha = 2;
            while (!reader.EndOfStream)
            {
                var conteudo = reader.ReadLine();
                if (conteudo.Length > 0)
                {
                    if (conteudo.Substring(0, 1) != "9")
                    {
                        retornos.Add(ProcessarBoleto(conteudo, linha));
                    }
                }
                linha++;
            }

            reader.Close();

            return retornos;
        }

        public Retorno ProcessarBoleto(string conteudo, int linha)
        {
            var retorno = new Retorno();
            retorno.Linha = linha;
            if (conteudo.Length < 400)
            {
                retorno.Tipo = Retorno.Tipos.Invalido;
                retorno.Mensagem = "Tamanho do registro inválido. Esperado 400 caracteres.";
            }
            else
            {
                var nossoNumero = conteudo.Substring(85, 8);
                var codigoOcorrencia = conteudo.Substring(108, 2);
                var dataOcorrencia = conteudo.Substring(110, 6);
                
                var _dataCredito = conteudo.Substring(295, 6);
                var instrucaoCancelada = conteudo.Substring(301, 4);
                var erros = conteudo.Substring(377, 8);
                var valor = Convert.ToDecimal(conteudo.Substring(253, 11) + "," + conteudo.Substring(264, 2));

                DateTime dataCredito = DateTime.MinValue;
                if (_dataCredito.Length == 6)
                {
                    var diaCredito = _dataCredito.Substring(0, 2);
                    var mesCredito = _dataCredito.Substring(2, 2);
                    var anoCredito = _dataCredito.Substring(4, 2);
                    DateTime.TryParse(diaCredito + "/" + mesCredito + "/20" + anoCredito, out dataCredito);
                }

                //Indica o canal utilizado pelo sacado para pagamento do BOLETO e, para clientes que possuem o crédito das liquidações separado em função do recurso utilizado no pagamento, indica se o crédito do valor correspondente estará “disponível” ou “a compensar” na data do lançamento em conta corrente.
                //var codigoLiquidacao = conteudo.Substring(392, 2);

                var boleto = boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).FirstOrDefault();

                retorno.Ocorrencia = (Ocorrencias)int.Parse(codigoOcorrencia);

                if (boleto != null)
                {
                    boleto.RetornoID = (int)retorno.Ocorrencia;
                    boleto.DataRetorno = DateTime.Now;
                    boleto.StatusProcessamento = Entities.Boleto.TodosStatusProcessamento.Processado;
                    boleto.RetornoErros = erros;
                    boleto.RetornoCancelamento = instrucaoCancelada;
                    boletoRepository.Save(boleto);

                    if (boleto.StatusPagamento == Entities.Boleto.TodosStatusPagamento.AguardandoPagamento)
                    {
                        switch (retorno.Ocorrencia)
                        {
                            case Ocorrencias.BAIXA_COM_TRANSFERENCIA_PARA_DESCONTO:
                            case Ocorrencias.BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG:
                            case Ocorrencias.BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG_SEM_TITULO_CORRESPONDENTE:
                            case Ocorrencias.BAIXA_POR_TER_SIDO_LIQUIDADO:
                            case Ocorrencias.BAIXA_SIMPLES:
                            case Ocorrencias.LIQUIDACAO_EM_CARTORIO:
                            case Ocorrencias.LIQUIDACAO_NORMAL:
                            case Ocorrencias.LIQUIDACAO_PARCIAL_COBRANÇA_INTELIGENTE_B2B:
                                retorno.Tipo = Retorno.Tipos.Sucesso;
                                retorno.Mensagem = "O boleto foi liquidado e o pedido atualizado.";

                                boleto.StatusPagamento = Entities.Boleto.TodosStatusPagamento.Pago;
                                if (dataCredito != DateTime.MinValue)
                                {
                                    boleto.DataPagamento = dataCredito;
                                }
                                boletoRepository.Save(boleto);

                                var pedido = boleto.Pedido;
                                if (pedido != null)
                                {
                                    pedido.StatusPagamento = Pedido.TodosStatusPagamento.Pago;                                    

                                    if (dataCredito != DateTime.MinValue)
                                    {
                                        pedido.DataPagamento = dataCredito;
                                        pedido.ValorPago = valor;
                                        pedido.DataProcessamento = DateTime.Now;
                                    }
                                    pedidoRepository.Save(pedido);
                                }

                                var fatura = boleto.Faturamento;
                                if (fatura != null)
                                {
                                    fatura.StatusPagamento = Pedido.TodosStatusPagamento.Pago;
                                    fatura.Status = Faturamento.TodosStatus.Paga;

                                    if (dataCredito != DateTime.MinValue)
                                    {
                                        fatura.DataPagamento = dataCredito;
                                        fatura.ValorPago = valor;
                                        fatura.DataProcessamento = DateTime.Now;
                                    }
                                    faturaRepository.Save(fatura);
                                }
                                break;
                            case Ocorrencias.BAIXA_POR_TER_SIDO_PROTESTADO:
                                
                                retorno.Tipo = Retorno.Tipos.Sucesso;
                                retorno.Mensagem = "O boleto foi protestado.";

                                boleto.StatusPagamento = Entities.Boleto.TodosStatusPagamento.Protestado; 
                                boletoRepository.Save(boleto);

                                if (boleto.PedidoID.HasValue)
                                {
                                    var _pedido = boleto.Pedido;
                                    _pedido.StatusPagamento = Pedido.TodosStatusPagamento.Protestado;
                                    pedidoRepository.Save(_pedido);
                                }
                                else if (boleto.FaturamentoID.HasValue)
                                {
                                    var _fatura = boleto.Faturamento;
                                    _fatura.StatusPagamento = Pedido.TodosStatusPagamento.Protestado;
                                    faturaRepository.Save(_fatura);
                                }
                                break;
                        }
                    }
                    else
                    {
                        retorno.Tipo = Retorno.Tipos.Outros;
                    }
                }
                else
                {
                    retorno.Tipo = Retorno.Tipos.Outros;
                    retorno.Mensagem = "Registro não associado a um boleto.";
                }
            }
            return retorno;
        }

        public static string  Gerar(Domain.Entities.Boleto boleto)
        {
            var cedente = new Cedente(boleto.CedenteCNPJ, boleto.CedenteRazaoSocial, boleto.CedenteAgencia, boleto.CedenteContaCorrente.Substring(0,5));
            cedente.Codigo = boleto.CedenteCodigo.ToString();

            var _boleto = new BoletoNet.Boleto(boleto.DataVencimento, boleto.Valor, boleto.Carteira, boleto.NossoNumero, cedente);            
            _boleto.NumeroDocumento = boleto.NumeroDocumento;
                        
            _boleto.Sacado = new Sacado(boleto.SacadoDocumento, boleto.SacadoNome);
            _boleto.Sacado.Endereco.End = boleto.SacadoEndereco;
            _boleto.Sacado.Endereco.Bairro = boleto.SacadoBairro;
            _boleto.Sacado.Endereco.Cidade = boleto.SacadoCidade;
            _boleto.Sacado.Endereco.CEP = boleto.SacadoCEP;
            _boleto.Sacado.Endereco.UF = boleto.SacadoUF;

            _boleto.Instrucoes.Add(new Instrucao_Itau() { Descricao = Domain.Core.Configuracoes.DOMINIO + " - " + Domain.Core.Configuracoes.SAC });


            var context = new COROASEntities();
            var pedidoEmpresaRepository = new PersistentRepository<Domain.Entities.PedidoEmpresa>(context);

            string NumeroPedidos = "";
            foreach (var PedidoReturn in pedidoEmpresaRepository.GetByExpression(r => r.FaturamentoID == boleto.FaturamentoID).ToList())
            {
                NumeroPedidos += " - " + "Ped: " + PedidoReturn.ID + " Fat: " + PedidoReturn.FaturamentoID;
            }

            _boleto.Instrucoes.Add(new Instrucao_Itau() { Descricao = Domain.Core.Configuracoes.BOLETO_INSTRUCAO + NumeroPedidos });
 
            _boleto.EspecieDocumento = new EspecieDocumento_Itau();

            var boletoBancario = new BoletoBancario();
            boletoBancario.CodigoBanco = 341;
            boletoBancario.Boleto = _boleto;
            boletoBancario.MostrarCodigoCarteira = true;
            boletoBancario.MostrarComprovanteEntrega = true;            
            boletoBancario.Boleto.Valida();

            var a = boletoBancario.Boleto.NumeroDocumento;

            var html = boletoBancario.MontaHtmlEmbedded();

            return html;
        }

        public static string GerarRemessa(Domain.Entities.Remessa remessa, Domain.Entities.Remessa.Empresa Empresa)
        {
            var indice = 1;
            var arquivo = new StringBuilder();
            
            arquivo.AppendLine(Header(remessa, indice, Empresa));
            indice++;

            foreach (var remessaBoleto in remessa.RemessaBoletoes)
            {
                try
                {
                    arquivo.AppendLine(Boleto(remessaBoleto, indice, Empresa));
                    indice++;
                }
                catch
                {
                    //TODO: Enviar erro
                }
            }

            arquivo.AppendLine(Trailer(indice));

            return arquivo.ToString();
        }

        private static string Header(Domain.Entities.Remessa remessa, int indice, Domain.Entities.Remessa.Empresa Empresa)
        {
            String BOLETO_AGENCIA = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA_LC;

            String BOLETO_CONTA_CORRENTE = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE_LC;

            String BOLETO_RAZAO_SOCIAL = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_RAZAO_SOCIAL = Core.Configuracoes.BOLETO_RAZAO_SOCIAL;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_RAZAO_SOCIAL = Core.Configuracoes.BOLETO_RAZAO_SOCIAL_LC;

            var retorno = new StringBuilder();
            retorno.Append("0");
            retorno.Append("1");
            retorno.AppendFormat("{0,-7}", "REMESSA");
            retorno.Append("01");
            retorno.AppendFormat("{0,-15}", "COBRANCA");
            retorno.AppendFormat("{0:0000}", BOLETO_AGENCIA);
            retorno.Append("00");
            retorno.AppendFormat("{0:000000}", BOLETO_CONTA_CORRENTE);
            retorno.AppendFormat("{0,-8}", "");
            retorno.AppendFormat("{0,-30}", GetTextoLimitado(BOLETO_RAZAO_SOCIAL, 30));
            retorno.Append("341");
            retorno.AppendFormat("{0,-15}", "BANCO ITAU SA");
            retorno.AppendFormat("{0:ddMMyy}", remessa.Data);
            retorno.AppendFormat("{0,-294}", "");
            retorno.AppendFormat("{0:000000}", indice);

            return retorno.ToString();
        }

        private static string Boleto(Domain.Entities.RemessaBoleto remessaBoleto, int indice, Domain.Entities.Remessa.Empresa Empresa)
        {
            Int64 BOLETO_CNPJ = 0;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_CNPJ = Int64.Parse(Core.Configuracoes.BOLETO_CNPJ);
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_CNPJ = Int64.Parse(Core.Configuracoes.BOLETO_CNPJ_LC);

            String BOLETO_AGENCIA = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA_LC;

            String BOLETO_CONTA_CORRENTE = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE_LC;

            String BOLETO_CARTEIRA = string.Empty;
            if (Empresa == Entities.Remessa.Empresa.CPV)
                BOLETO_CARTEIRA = Core.Configuracoes.BOLETO_CARTEIRA;
            if (Empresa == Entities.Remessa.Empresa.LC)
                BOLETO_CARTEIRA = Core.Configuracoes.BOLETO_CARTEIRA_LC;

            var retorno = new StringBuilder();
            retorno.Append("1");
            retorno.AppendFormat("{0:00}", 2);
            retorno.AppendFormat("{0:00000000000000}", BOLETO_CNPJ);
            retorno.AppendFormat("{0:0000}", BOLETO_AGENCIA);
            retorno.Append("00");
            retorno.AppendFormat("{0:000000}", BOLETO_CONTA_CORRENTE);
            retorno.AppendFormat("{0,-4}", "");

            retorno.AppendFormat("{0:0000}", 0);
            retorno.AppendFormat("{0,-25}", ""); //rodapé do boleto
            retorno.AppendFormat("{0:00000000}", int.Parse(remessaBoleto.Boleto.NossoNumero));
            retorno.AppendFormat("{0:0000000000000}", 0);
            retorno.AppendFormat("{0:000}", BOLETO_CARTEIRA);
            retorno.AppendFormat("{0,-21}", "");
            retorno.AppendFormat("{0,-1}", "I");
            retorno.AppendFormat("{0:00}", remessaBoleto.Boleto.ComandoID); //Comando
            retorno.AppendFormat("{0,-10}", remessaBoleto.Boleto.NumeroDocumento);
            retorno.AppendFormat("{0:ddMMyy}", remessaBoleto.Boleto.DataVencimento);
            retorno.AppendFormat("{0:0000000000000}", remessaBoleto.Boleto.Valor * 100);
            retorno.AppendFormat("{0:000}", 341);
            retorno.AppendFormat("{0:00000}", 0); //?zeros?
            retorno.AppendFormat("{0,-2}", "01"); //especie do titulo
            retorno.AppendFormat("{0,-1}", "A");
            retorno.AppendFormat("{0:ddMMyy}", remessaBoleto.Boleto.DataCriacao);
            retorno.AppendFormat("{0,-2}", "05"); //instrucao 1
            retorno.AppendFormat("{0,-2}", "24"); //instrucao 2
            retorno.AppendFormat("{0:0000000000000}", 0);
            retorno.AppendFormat("{0:000000}", 0); //data limite conc desc
            retorno.AppendFormat("{0:0000000000000}", 0 * 100);
            retorno.AppendFormat("{0:0000000000000}", 0 * 100);
            retorno.AppendFormat("{0:0000000000000}", 0 * 100);

            retorno.AppendFormat("{0:00}", remessaBoleto.Boleto.TipoID); //Tipo
            var sacado = remessaBoleto.Boleto.SacadoDocumento.Replace(",", "").Replace("/", "").Replace(".", "").Replace("-", "").Trim();
            retorno.AppendFormat("{0:00000000000000}", Int64.Parse(sacado));
            retorno.AppendFormat("{0,-30}", GetTextoLimitado(remessaBoleto.Boleto.SacadoNome.ToUpper(), 30));
            retorno.AppendFormat("{0,-10}", "");
            retorno.AppendFormat("{0,-40}", GetTextoLimitado(remessaBoleto.Boleto.SacadoEndereco.ToUpper(), 40));
            retorno.AppendFormat("{0,-12}", GetTextoLimitado(remessaBoleto.Boleto.SacadoBairro.ToUpper(), 12));
            try { retorno.AppendFormat("{0:00000000}", int.Parse(remessaBoleto.Boleto.SacadoCEP.Replace("-", "").Trim())); }
            catch { retorno.AppendFormat("{0:00000000}", "00000000"); }
            retorno.AppendFormat("{0,-15}", GetTextoLimitado(remessaBoleto.Boleto.SacadoCidade.ToUpper(), 15));
            retorno.AppendFormat("{0,-2}", GetTextoLimitado(remessaBoleto.Boleto.SacadoUF.ToUpper(), 2));
            retorno.AppendFormat("{0,-30}", "");
            retorno.AppendFormat("{0,-4}", "");
            retorno.AppendFormat("{0:000000}", 0); //data mora
            retorno.AppendFormat("{0:00}", 0);
            retorno.AppendFormat("{0,-1}", "");
            retorno.AppendFormat("{0:000000}", indice);

            return retorno.ToString();
        }

        private static string Trailer(int indice)
        {
            var retorno = new StringBuilder();
            retorno.Append("9");
            retorno.AppendFormat("{0,-393}", "");
            retorno.AppendFormat("{0:000000}", indice);
            return retorno.ToString();
        }

        private static string GetTextoLimitado(string texto, int tamanho)
        {
            var retorno = Domain.Core.Funcoes.SomenteLetrasNumeros(Domain.Core.Funcoes.AcertaAcentos(texto.Trim().ToUpper())).ToUpper();
            retorno = Core.Funcoes.SomenteAlfanumerico(retorno);

            if (retorno.Length > tamanho)
            {
                retorno = retorno.Substring(0, tamanho);
            }
            byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(retorno);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        public static void EnviaBoletoProximoVencimento()
        {
            var context = new COROASEntities();
            var pedidoRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            var boletoRepository = new PersistentRepository<Domain.Entities.Boleto>(context);
            var data = DateTime.Now.AddDays(1);
            var boletos = boletoRepository.GetByExpression(b => !b.DataEnvioLembreteVencimento.HasValue && b.StatusPagamentoID == (int)Domain.Entities.Boleto.TodosStatusPagamento.AguardandoPagamento && b.DataVencimento.Day == data.Day && b.DataVencimento.Month == data.Month && b.DataVencimento.Year == data.Year).ToList();

            foreach (var item in boletos)
            { 
                item.Pedido.EnviarLembreteVencimentoBoleto();
                item.DataEnvioLembreteVencimento = DateTime.Now;
                boletoRepository.Save(item);
            }
        }
    }
}
