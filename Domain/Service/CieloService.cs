﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WOOWEXTENSIONS.Payment.Cielo.Entities;

namespace Domain.Service
{
    public class CieloService
    {

        public static Retorno Pagar(int pedidoID, decimal valor, int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela)
        {
            var portador = new Portador()
            {
                Nome = nome,
                Numero = numero,
                Indicador = Portador.Indicadores.Informado,
                MesValidade = mes,
                AnoValidade = ano,
                CodigoSeguranca = codigo
            };

            var formaPagamento = new FormaPagamento()
            {
                Bandeira = (FormaPagamento.Bandeiras)bandeira,
                Produto = parcela == 1 ? FormaPagamento.Produtos.CreditoAVista : FormaPagamento.Produtos.ParceladoLoja,
                Parcelas = parcela
            };

            var pedidoCielo = new WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido()
            {
                DataHora = DateTime.Now,
                Descricao = "Coroas para Velório",
                Idioma = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Idiomas.Portugues,
                Moeda = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Moedas.Real,
                Numero = pedidoID.ToString(),
                Valor = valor
            };

            var cieloService = new WOOWEXTENSIONS.Payment.Cielo.Services.CieloService();
            var retorno = cieloService.AutorizarTransacaoDiretamente(portador, pedidoCielo, formaPagamento, true);

            return retorno;
        }

        public static Retorno PagarLacos(int pedidoID, decimal valor, int bandeira, string nome, string numero, int mes, int ano, string codigo, int parcela)
        {
            var portador = new Portador()
            {
                Nome = nome,
                Numero = numero,
                Indicador = Portador.Indicadores.Informado,
                MesValidade = mes,
                AnoValidade = ano,
                CodigoSeguranca = codigo
            };

            var formaPagamento = new FormaPagamento()
            {
                Bandeira = (FormaPagamento.Bandeiras)bandeira,
                Produto = parcela == 1 ? FormaPagamento.Produtos.CreditoAVista : FormaPagamento.Produtos.ParceladoLoja,
                Parcelas = parcela
            };

            var pedidoCielo = new WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido()
            {
                DataHora = DateTime.Now,
                Descricao = "Laços Corporativos",
                Idioma = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Idiomas.Portugues,
                Moeda = WOOWEXTENSIONS.Payment.Cielo.Entities.Pedido.Moedas.Real,
                Numero = pedidoID.ToString(),
                Valor = valor
            };

            var cieloService = new WOOWEXTENSIONS.Payment.Cielo.Services.CieloService();
            var retorno = cieloService.AutorizarTransacaoDiretamente(portador, pedidoCielo, formaPagamento, true);

            return retorno;
        }
    }
}
