﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities;
using Domain.Entities.Generics;
using Domain.MetodosExtensao;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using Dapper;
using Domain.Entities.ReportsStoredProcs;

namespace Domain.Service {
    //=================================================================================
    public static class LoteService{

        #region Metodos

        //-----------------------------------------------------------------------------
        public static List<spListCNABFornecedor_Result> GetRelatorioVendaAtendente(int LoteID) {
            List<spListCNABFornecedor_Result> lstResult = new List<spListCNABFornecedor_Result>();
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString)) {
                con.Open();
                string sql = "exec dbo.spListCNABFornecedor  @LoteID = @paramID";
                lstResult = con.Query<spListCNABFornecedor_Result>(sql, new { paramID = LoteID }, commandTimeout: 600).ToList();
                
                if (con.State == ConnectionState.Open) {
                    con.Close();
                }
            }
            return lstResult;
        }
        //-----------------------------------------------------------------------------
      
        #endregion

    }
}
