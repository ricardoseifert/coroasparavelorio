﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Domain.Entities;
using DomainExtensions.Repositories;
using System.Xml.Linq;
using Domain.Core.YourViews;
using Domain.Core.YourViews.Return;

namespace Domain.Service
{
    public class YourViewsService
    { 
        public static YourViewsReturn EnviaPedidoYourViews(int PedidoID)
        {
            var context = new COROASEntities();
            var pedidosRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            var logRepository = new PersistentRepository<Domain.Entities.Log>(context);

            var data = DateTime.Now.AddDays(-14);
            var pedido = pedidosRepository.Get(PedidoID);

            if (pedido.OrigemSite == "LAÇOS FLORES") {
                var LstErrosYourViews = new List<Domain.Core.YourViews.Return.ErrorList>();
                LstErrosYourViews.Add(new ErrorList {
                    Error = "Origem Lacos Flores nao Envia YourViews",
                    Field = "N/A"
                });

                return new YourViewsReturn() { HasErrors = true, ErrorList = LstErrosYourViews };
            }

            var Result = new YourViews().EnviaPedido(pedido);
            return Result;
        }

        public static YourViewsReturn AtualizarYourViewsEntrega(int PedidoID)
        {
            var context = new COROASEntities();
            var pedidosRepository = new PersistentRepository<Domain.Entities.Pedido>(context);
            var logRepository = new PersistentRepository<Domain.Entities.Log>(context);

            var data = DateTime.Now.AddDays(-14);
            var pedido = pedidosRepository.Get(PedidoID);

            var Result = new YourViews().AtualizarPedido(pedido);

            return Result;
        }
    }
}
