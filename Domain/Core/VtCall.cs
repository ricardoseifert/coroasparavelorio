﻿using Domain.Entities;
using Domain.Entities.Generics;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Domain.Core
{
    public class VtCall
    {
        private IPersistentRepository<Linha> linhasRepository;
        public string FormataTelefone(string NrTelefone)
        {
            // SE ALFANUMERICO
            if (!Regex.IsMatch(NrTelefone, "^[a-zA-Z0-9]+$"))
            {
                return NrTelefone;
            }
            else
            {
                var DDD = NrTelefone.Substring(0, 2);
                var Tel = "";
                if ((NrTelefone.Length - 2) == 8)
                {
                    Tel = NrTelefone.Substring(2, 4) + "-" + NrTelefone.Substring(6, 4);
                }
                if ((NrTelefone.Length - 2) == 9)
                {
                    Tel = NrTelefone.Substring(2, 5) + "-" + NrTelefone.Substring(7, 4);
                }
                if (NrTelefone.Length == 8)
                {
                    Tel = NrTelefone.Substring(0, 4) + "-" + NrTelefone.Substring(4, 4);
                    DDD = "XX";
                }
                if (NrTelefone.Length == 9)
                {
                    Tel = NrTelefone.Substring(0, 5) + "-" + NrTelefone.Substring(5, 4);
                    DDD = "XX";
                }
                if (NrTelefone.Length < 8 || NrTelefone.Length > 11)
                {
                    Tel = NrTelefone;
                    DDD = "XX";
                }
                return "(" + DDD + ") " + Tel;
            }
        }
        public List<Ligacao> RetornaLigacaoAtual(int RamalID)
        {
            var Response = "";

            var webRequest = System.Net.WebRequest.Create("http://centralflores.vtcall.cc/API/showpeer/?ramal=" + RamalID);
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("access_token", "ced995d0c43cfeeac8346c6bb654d668");
            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    Response = sr.ReadToEnd();
                }
            }

            List<Ligacao> Lstretorno = new List<Ligacao>();

            if (Response.Contains("\"code\":201,"))
            {
                return null;
            }
            else
            {
                var ListaObjetos = new JavaScriptSerializer().Deserialize<List<Dictionary<string, string>>>(Response);

                foreach (var Iten in ListaObjetos)
                {
                    Ligacao NovaLigacao = new Ligacao();

                    NovaLigacao.IdLigacao = Iten.Where(r => r.Key == "linkedid").First().Value;
                    NovaLigacao.NrDestino = FormataTelefone(Iten.Where(r => r.Key == "src").First().Value); 
                    NovaLigacao.NrOrigem = FormataTelefone(Iten.Where(r => r.Key == "dst").First().Value);
                    NovaLigacao.HoraInicio = DateTime.Parse(Iten.Where(r => r.Key == "start").First().Value);
                    NovaLigacao.HoraAtendimento = DateTime.Parse(Iten.Where(r => r.Key == "answer").First().Value);

                    // SE LIGAÇÃO FOIR DE ENTRADA
                    if (Iten.Where(r => r.Key == "from_did").ToList().Count > 0)
                    {
                        NovaLigacao.NrOrigem = FormataTelefone(NovaLigacao.NrOrigem);
                        NovaLigacao.TipoLigacao = "ENTRADA";

                        #region Conecta Banco Retorna Linha

                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;
                        linhasRepository = new PersistentRepository<Linha>(new ObjectContext(conStr));

                        string Tronco = Iten.Where(r => r.Key == "from_did").First().Value;
                        var Linhas = linhasRepository.GetByExpression(p => p.NrTronco == Tronco).First();

                        #endregion

                        if (Linhas != null)
                        {
                            NovaLigacao.NrEntrada = Linhas.NrLinhas;
                            NovaLigacao.IdLinha = Linhas.ID.ToString();
                            NovaLigacao.NomeLinha = Linhas.NomeLinha;
                        }
                        else
                        {
                            NovaLigacao.NomeLinha = "N/A";
                            NovaLigacao.NrEntrada = Iten.Where(r => r.Key == "from_did").First().Value;
                        }
                    }
                    else
                    {
                        NovaLigacao.NomeLinha = "N/A";
                        NovaLigacao.NrEntrada = "N/A";
                        NovaLigacao.TipoLigacao = "SAIDA";
                    }

                    Lstretorno.Add(NovaLigacao);
                }
            }

            return Lstretorno;
        }
        public string CallVtCallRecebidas(int RamalID)
        {
            var Response = "";
            var webRequest = System.Net.WebRequest.Create("http://centralflores.vtcall.cc/API/ligacoes?start_date=" + DateTime.Now.ToString("yyyy-MM-dd") + "&end_date=" + DateTime.Now.ToString("yyyy-MM-dd") + "&destino=" + RamalID + "&per=30");
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("access_token", "ced995d0c43cfeeac8346c6bb654d668");
            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    Response = sr.ReadToEnd();
                }
            }

            return Response;
        }
        public string CallVtCallEfetuadas(int RamalID)
        {
            var Response = "";
            var webRequest = System.Net.WebRequest.Create("http://centralflores.vtcall.cc/API/ligacoes?start_date=" + DateTime.Now.ToString("yyyy-MM-dd") + "&end_date=" + DateTime.Now.ToString("yyyy-MM-dd") + "&origem=" + RamalID + "&per=30");
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "application/json";
            webRequest.Headers.Add("access_token", "ced995d0c43cfeeac8346c6bb654d668");
            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    Response = sr.ReadToEnd();
                }
            }

            return Response;
        }
        public List<Ligacao> RetornaLigacoesRecebidas(int RamalID)
        {
            var LigacoesRecebidas = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(CallVtCallRecebidas(RamalID));
            System.Collections.ArrayList Itens = new System.Collections.ArrayList();
            Itens.AddRange((System.Collections.ArrayList)LigacoesRecebidas.First().Value);

            List<Ligacao> LstObjetos = new List<Ligacao>();
            foreach (var Iten in Itens)
            {
                Dictionary<string, object> data = new Dictionary<string, object>();
                data = (Dictionary<string, object>)Iten;

                Ligacao NovaData = new Ligacao();
                foreach (var Property in data)
                {
                    if (Property.Key == "time")
                    {
                        string badInput = Property.Value.ToString();
                        DateTime dateTime2;
                        if (DateTime.TryParse(badInput, out dateTime2))
                        {
                            NovaData.HoraAtendimento = dateTime2;
                        }
                    }

                    NovaData.TipoLigacao = "ENTRADA";

                    if (Property.Key == "origem")
                        NovaData.NrOrigem = FormataTelefone(Property.Value.ToString());

                    if (Property.Key == "destino")
                        NovaData.NrDestino = FormataTelefone(Property.Value.ToString());

                    //if (Property.Key == "from_did")
                    //    NovaData.NrEntrada = Property.Value.ToString();

                    if (Property.Key == "linkedid")
                        NovaData.IdLigacao = Property.Value.ToString();

                    if (Property.Key == "tempo_ligacao")
                        NovaData.TempoLigacao = Property.Value.ToString();

                    if (Property.Key == "status")
                        NovaData.Status = Property.Value.ToString();

                    if (Property.Key == "from_did")
                        NovaData.IdLinha = Property.Value.ToString();

                    if (Property.Key == "audio")
                    {
                        if (Property.Value != null)
                        {
                            NovaData.LinkAudio = Property.Value.ToString();
                        }
                    }

                }                

                LstObjetos.Add(NovaData);
            }

            return LstObjetos;
        }
        public List<Ligacao> RetornaLigacoesEfetuadas(int RamalID)
        {
            var LigacoesRecebidas = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(CallVtCallEfetuadas(RamalID));
            System.Collections.ArrayList Itens = new System.Collections.ArrayList();
            Itens.AddRange((System.Collections.ArrayList)LigacoesRecebidas.First().Value);

            List<Ligacao> LstObjetos = new List<Ligacao>();
            foreach (var Iten in Itens)
            {
                Dictionary<string, object> data = new Dictionary<string, object>();
                data = (Dictionary<string, object>)Iten;

                Ligacao NovaData = new Ligacao();
                foreach (var Property in data)
                {
                    if (Property.Key == "time")
                    {
                        string badInput = Property.Value.ToString();
                        DateTime dateTime2;
                        if (DateTime.TryParse(badInput, out dateTime2))
                        {
                            NovaData.HoraAtendimento = dateTime2;
                        }
                    }
                    NovaData.TipoLigacao = "SAIDA";
                    NovaData.IdLinha = "0";
                    if (Property.Key == "origem")
                        NovaData.NrOrigem = FormataTelefone(Property.Value.ToString());
                    if (Property.Key == "destino")
                        NovaData.NrDestino = FormataTelefone(Property.Value.ToString());
                    if (Property.Key == "from_did")
                        NovaData.NrEntrada = Property.Value.ToString();
                    if (Property.Key == "linkedid")
                        NovaData.IdLigacao = Property.Value.ToString();
                    if (Property.Key == "tempo_ligacao")
                        NovaData.TempoLigacao = Property.Value.ToString();
                    if (Property.Key == "status")
                        NovaData.Status = Property.Value.ToString();
                    if (Property.Key == "audio")
                    {
                        if (Property.Value != null)
                        {
                            NovaData.LinkAudio = Property.Value.ToString();
                        }
                    }

                }
                LstObjetos.Add(NovaData);
            }

            return LstObjetos;
        }
        public List<Ligacao> RetornaLigacoes(int RamalID)
        {
            List<Ligacao> LstObjetos = new List<Ligacao>();

            var LigacoesRecebidas = RetornaLigacoesRecebidas(RamalID).Where(r => r.Status == "ATENDIDA").Take(5).ToList();
            var LigacoesEfetuadas = RetornaLigacoesEfetuadas(RamalID).Take(5).ToList();

            #region PREENCHE NOME E NR LINHA LIGACOES RECEBIDAS

            #region ABRE CONEXAO

            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;
            linhasRepository = new PersistentRepository<Linha>(new ObjectContext(conStr));

            #endregion

            foreach (var Ligacao in LigacoesRecebidas)
            {
                if (!string.IsNullOrEmpty(Ligacao.IdLinha))
                {
                    var Linhas = linhasRepository.GetByExpression(p => p.NrTronco == Ligacao.IdLinha).ToList();

                    if (Linhas.Count() > 0)
                    {
                        var Linha = Linhas.First();

                        Ligacao.NrEntrada = Linha.NrLinhas;
                        Ligacao.IdLinha = Linha.ID.ToString();
                        Ligacao.NomeLinha = Linha.NomeLinha;
                    }
                    else
                    {
                        Ligacao.NomeLinha = "N/A";
                        Ligacao.NrEntrada = Ligacao.IdLinha;
                    }
                }
            }

            #endregion

            LstObjetos.AddRange(LigacoesRecebidas);
            LstObjetos.AddRange(LigacoesEfetuadas);

            LstObjetos = LstObjetos.OrderByDescending(r => r.HoraAtendimento).ToList();

            return LstObjetos;
        }

    }
}
