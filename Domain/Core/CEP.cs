﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Domain.Core
{
    public class WebCEP
    {
        #region "Váriavies"
        string _uf;
        string _cidade;
        string _bairro;
        string _tipo_lagradouro;
        string _lagradouro;
        string _resultado;
        string _resultato_txt;
        string _endereco_completo;
        #endregion

        #region "Propiedades"
        public string UF
        {
            get { return _uf; }
        }
        public string Cidade
        {
            get { return _cidade; }
        }
        public string Bairro
        {
            get { return _bairro; }
        }
        public string TipoLagradouro
        {
            get { return _tipo_lagradouro; }
        }
        public string Lagradouro
        {
            get { return _lagradouro; }
        }
        public string Resultado
        {
            get { return _resultado; }
        }
        public string ResultadoTXT
        {
            get { return _resultato_txt; }
        }
        public string EnderecoCompleto
        {
            get { return _endereco_completo; }
        }
        #endregion
        #region "Construtor"
        /// <summary>
        /// WebService para Busca de CEP
        ///  </summary>
        /// <param  name="CEP"></param>
        public WebCEP(string CEP)
        {
            HttpClient httpClient = new HttpClient();
            var result = httpClient.GetAsync("https://viacep.com.br/ws/" + CEP + "/json/").Result;
            var contents = result.Content.ReadAsStringAsync().Result;

            dynamic data = JObject.Parse(contents);

            try
            {
                if(data.erro != null )
                {
                    _resultado = "0";
                    return;
                }

                _uf = data.uf;
                _cidade = data.localidade;
                _bairro = data.bairro;
                _endereco_completo = data.logradouro;
                _lagradouro = data.logradouro;
                _resultado = "1";
            }
            catch 
            {
                _resultado = "0";
            }
            
        }
        #endregion
    }
}