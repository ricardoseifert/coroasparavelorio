﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Domain.Core
{
    public class DefaultMovile
    {
        public string authenticationtoken
        {
            get
            {
                return Core.Configuracoes.MOVILE_TOKEN;
            }
        }
        public string username
        {
            get
            {
                return Core.Configuracoes.MOVILE_USER;
            }
        }
        public string mensagem { get; set; }
        public string destino { get; set; }
    }

    public class Movile
    {
        public string CallMovile(DefaultMovile Movile)
        {
            var webRequest = System.Net.WebRequest.Create("https://api-messaging.movile.com/v1/send-sms?destination=" + Movile.destino + "&messageText=" + Movile.mensagem + "");

            try
            {
                if (webRequest != null)
                {
                    webRequest.Method = "GET";
                    webRequest.Timeout = 20000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("authenticationtoken", Movile.authenticationtoken);
                    webRequest.Headers.Add("username", Movile.username);

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            return jsonResponse;
                        }
                    }
                }
                else
                {
                    return "Erro no Envio do SMS";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}