﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Domain.Core
{
    public class Criptografia
    {

        private static string sEncryptionKey = "eQu!P@!dUstr!@.coM.BR";

        /// <summary>
        ///    Decrypts  a particular string with a specific Key
        /// </summary>
        /// <param name="stringToDecrypt"></param>
        /// <returns></returns>
        public static string Decrypt(string stringToDecrypt)
        {
            stringToDecrypt = Domain.Core.Funcoes.ConvertHexToString(stringToDecrypt);
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray = new byte[stringToDecrypt.Length];
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch
            {
                return (string.Empty);
            }
        }

        /// <summary>
        ///   Encrypts  a particular string with a specific Key
        /// </summary>
        /// <param name="stringToEncrypt"></param>
        /// <returns></returns>
        public static string Encrypt(string stringToEncrypt)
        {
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray;

            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Domain.Core.Funcoes.ConvertStringToHex(Convert.ToBase64String(ms.ToArray()));
            }
            catch
            {
                return (Domain.Core.Funcoes.ConvertStringToHex(string.Empty));
            }
        }
    }
}
