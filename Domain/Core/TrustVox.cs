﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using System.IO;
using System.Net;
using DomainExtensions.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Domain.Core.TrustVox
{
    public class Order
    {
        public int order_id;
        public string delivery_date;
        public Client client;
        public List<Item> items;
    }

    public class Client
    {
        public string first_name;
        public string last_name;
        public string email;
    }

    public class Item
    {
        public string name;
        public string id;
        public string url;
        public string price;
    }

    public class TrustVox
    {
        private PersistentRepository<Log> logRepository;
        private PersistentRepository<Pedido> pedidoRepository;
        private Pedido Pedido;
        private Log Log;

        private bool m_Retorno;
        public bool Retorno
        {
            get
            {
                return m_Retorno;
            }
            set
            {
                m_Retorno = value;
            }
        }

        public static bool EnviaTrustVox(Pedido pedido)
        {
            var trustVox = new TrustVox();
            trustVox.Pedido = pedido;
            return trustVox.Response();
        }

        public TrustVox()
        {
            logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());
            pedidoRepository = new PersistentRepository<Domain.Entities.Pedido>(new COROASEntities());
        }

        public bool Response()
        {
            Thread m_thread = new Thread(new ThreadStart(_Response));
            m_thread.SetApartmentState(ApartmentState.STA);
            m_thread.Start();
            m_thread.Join();
            return m_Retorno;
        }

        private void _Response()
        {
            if (Pedido.DataEntrega.HasValue)
            {
                Log = new Log();
                Log.Data = DateTime.Now;
                Log.Pedido = Pedido.Numero;

                Stream requestStream = null;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://trustvox.com.br/api/stores/61/orders");
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Accept = "application/vnd.trustvox.com; version=1";
                request.Headers.Add("Authorization", "token AUvEUyBJzxb7Snssedoq");
                request.KeepAlive = false;
                request.SendChunked = true;

                var client = new Client();
                try
                {
                    client.first_name = Pedido.Cliente.Nome.Split(' ')[0];
                }
                catch
                {
                    client.first_name = Pedido.Cliente.Nome;
                }
                try
                {
                    client.last_name = Pedido.Cliente.Nome.Split(' ')[1];
                }
                catch
                {
                    client.last_name = "";
                }
                client.email = Pedido.Cliente.Email.Trim().ToLower();

                var items = new List<Domain.Core.TrustVox.Item>();

                foreach (var itempedido in Pedido.PedidoItems)
                {
                    var item = new Domain.Core.TrustVox.Item();
                    item.id = itempedido.ProdutoTamanho.ProdutoID.ToString();
                    item.url = "http://www.coroasparavelorio.com.br/coroas-de-flores/" + itempedido.ProdutoTamanho.Produto.RotuloUrl;
                    item.price = itempedido.Valor.ToString("N", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")).Replace(",", "");
                    item.name = itempedido.ProdutoTamanho.Produto.Nome;

                    items.Add(item);
                }

                var order = new Domain.Core.TrustVox.Order();
                order.order_id = Pedido.ID;
                order.delivery_date = Pedido.DataEntrega.Value.ToString("dd/MM/yyyy HH:mm");
                order.client = client;
                order.items = items;

                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var serializedResult = serializer.Serialize(order);

                System.Text.StringBuilder urlEncoded = new System.Text.StringBuilder(serializedResult);

                Log.Envio = serializedResult;

                byte[] byteBuffer = null;

                byteBuffer = System.Text.Encoding.UTF8.GetBytes(urlEncoded.ToString());
                request.ContentLength = byteBuffer.Length;
                requestStream = request.GetRequestStream();
                requestStream.Write(byteBuffer, 0, byteBuffer.Length);
                request.Timeout = 20000;
                requestStream.Close();

                Task<WebResponse> task = Task.Factory.FromAsync(
                    request.BeginGetResponse,
                    asyncResult => request.EndGetResponse(asyncResult),
                    (object)null);

                task.ContinueWith(t => ReadStreamFromResponse(t.Result));
            }
        }

        private void ReadStreamFromResponse(WebResponse response)
        {
            System.Text.StringBuilder Dados = new System.Text.StringBuilder();
            try
            {
                using (Stream responseStream = response.GetResponseStream())
                using (StreamReader sr = new StreamReader(responseStream))
                {
                    System.Text.Encoding encoding = System.Text.Encoding.Default;

                    StreamReader reader = null;
                    reader = new StreamReader(responseStream, encoding);


                    Char[] charBuffer = new Char[256];
                    int count = reader.Read(charBuffer, 0, charBuffer.Length);

                    while (count > 0)
                    {
                        Dados.Append(new String(charBuffer, 0, count));
                        count = reader.Read(charBuffer, 0, charBuffer.Length);
                    }
                    m_Retorno = true;
                }
            }
            catch (WebException e)
            {
                var statusCode = ((HttpWebResponse)e.Response).StatusCode;
                Log.Retorno = statusCode + "\n" + Dados;
                m_Retorno = statusCode == HttpStatusCode.Created;
            }
            finally
            {
                Log.Retorno = "OK";
                logRepository.Save(Log);

                Pedido.DataEnvioPesquisa = DateTime.Now;
                pedidoRepository.Save(Pedido);
            }
        }
    }
}
