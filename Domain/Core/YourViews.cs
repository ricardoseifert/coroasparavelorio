﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using System.IO;
using System.Net;
using DomainExtensions.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Domain.Core.YourViews.Return;

namespace Domain.Core.YourViews
{
    public class Order
    {
        public int OrderId;
        public DateTime OrderDate;
        public string Status;
        public bool IsDelivered;
        public DateTime DeliveryDate;
        public decimal Total;
        public User User;
        public List<Products> Products;
    }

    public class User
    {
        public string Name;
        public string Email;
    }

    public class Products
    {
        public string ProductId;
        public string Name;
        public bool IsActive;
        public string Image;
        public string Url;
        public decimal Value;
    }

    public class YourViews
    {
        private PersistentRepository<Log> logRepository;
        private PersistentRepository<Pedido> pedidoRepository;
        
        public YourViews()
        {
            logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());
            pedidoRepository = new PersistentRepository<Domain.Entities.Pedido>(new COROASEntities());
        }

        public YourViewsReturn EnviaPedido(Pedido pedido)
        {
            #region NEW ORDER

            var NewOrder = new Order();

            NewOrder.OrderId = pedido.ID;
            NewOrder.IsDelivered = false;
            NewOrder.OrderDate = pedido.DataCriacao;
            NewOrder.Status = pedido.StatusProcessamento.ToString();
            NewOrder.Total = pedido.ValorTotal;

            List<Products> lstProdutos = new List<Products>();
            foreach(var Produto in pedido.PedidoItems)
            {
                lstProdutos.Add(new Products { IsActive = true,
                    Name = Produto.ProdutoTamanho.Produto.Nome,
                    Image = "https://www.coroasparavelorio.com.br/content/produtos/" + Produto.ProdutoTamanho.ProdutoID + "/" + Produto.ProdutoTamanho.Produto.Foto + "",
                    ProductId = Produto.ProdutoTamanho.Produto.ID.ToString(),
                    Url = "https://www.coroasparavelorio.com.br/coroas-de-flores/" + Produto.ProdutoTamanho.Produto.RotuloUrl + "/" + Produto.ProdutoTamanho.ProdutoID,
                    Value = Produto.Valor });
            }

            NewOrder.Products = lstProdutos;

            NewOrder.User = new User() {
                Name = pedido.Cliente.Nome.ToUpper(),
                Email = pedido.Cliente.Email
            };

            #endregion

            var json = JsonConvert.SerializeObject(NewOrder);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://service.yourviews.com.br/api/" + Core.Configuracoes.YourViews_Token + "/order");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(Core.Configuracoes.YourViews_User + ":" + Core.Configuracoes.YourViews_Pass));
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var StrResponse = "";
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {   
                StrResponse = streamReader.ReadToEnd();
            }

            var ClassReturn = JsonConvert.DeserializeObject<Domain.Core.YourViews.Return.YourViewsReturn>(StrResponse);

            return ClassReturn;
        }

        public YourViewsReturn AtualizarPedido(Pedido pedido)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://service.yourviews.com.br/api/" + Core.Configuracoes.YourViews_Token + "/order/SetDelivery/" + pedido.ID + "?storeStatus=Entregue");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(Core.Configuracoes.YourViews_User + ":" + Core.Configuracoes.YourViews_Pass));
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var StrResponse = "";
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                StrResponse = streamReader.ReadToEnd();
            }

            var ClassReturn = JsonConvert.DeserializeObject<Domain.Core.YourViews.Return.YourViewsReturn>(StrResponse);

            return ClassReturn;
        }

    }

}

namespace Domain.Core.YourViews.Return
{
    public class Product
    {
        public int YourviewsProductId { get; set; }
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
        public double Value { get; set; }
        public object Category { get; set; }
        public object Brand { get; set; }
        public object Sku { get; set; }
    }

    public class User
    {
        public int YourviewsUserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public object City { get; set; }
        public object State { get; set; }
        public object ZipCode { get; set; }
        public object IPAddress { get; set; }
    }

    public class Element
    {
        public int YourviewsOrderId { get; set; }
        public string OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string Status { get; set; }
        public string YourviewsStatus { get; set; }
        public bool IsDelivered { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public double Total { get; set; }
        public object TrackingCode { get; set; }
        public List<Product> Products { get; set; }
        public User User { get; set; }
    }

    public class ErrorList
    {
        public string Field { get; set; }
        public string Error { get; set; }
    }

    public class YourViewsReturn
    {
        public bool HasErrors { get; set; }
        public Element Element { get; set; }
        public List<ErrorList> ErrorList { get; set; }
        public int Total { get; set; }
        public int CurrentPage { get; set; }
    }
}
