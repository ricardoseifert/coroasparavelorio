﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Net;
using System.IO;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using Domain.Repositories;

namespace Domain.Core
{
    public class DefaultMandrillConsulta
    {
        public string key
        {
            get
            {
                return Core.Configuracoes.Mandrill_Token;
            }
        }

        public string id { get; set; }
        public string state { get; set; }
        public string opens { get; set; }
        public string clicks { get; set; }
        public string bounce_description { get; set; }
        public string subject { get; set; }
    }

    public class DefaultMandrill
    {
        public string key
        {
            get
            {
                return Core.Configuracoes.Mandrill_Token;
            }
        }
        public string template_name { get; set; }

        private List<TemplateContentMandrill> template_contentLIST = new List<TemplateContentMandrill>();
        public List<TemplateContentMandrill> template_content
        {
            get { return template_contentLIST; }
            set { template_contentLIST = value; }
        }

        public MessagemMandrill message { get; set; }
    }

    public class TemplateContentMandrill
    {
       
        public string name { get; set; }
        public string content { get; set; }
    }

    public class MessagemMandrill
    {
        public string subject { get; set; }
        public string from_email { get; set; }
        public string from_name { get; set; }

        private List<ToMandrill> toLIST = new List<ToMandrill>();
        public List<ToMandrill> to
        {
            get { return toLIST; }
            set { toLIST = value; }
        }

        public headersMandrill headers { get; set; }

        private List<merge_varsMandrill> merge_varsLIST = new List<merge_varsMandrill>();
        public List<merge_varsMandrill> merge_vars
        {
            get { return merge_varsLIST; }
            set { merge_varsLIST = value; }
        }
        private List<attachmentsMandrill> attachmentsLIST = new List<attachmentsMandrill>();
        public List<attachmentsMandrill> attachments
        {
            get { return attachmentsLIST; }
            set { attachmentsLIST = value; }
        }
    }

    public class ToMandrill
    {
        public string email { get; set; }
        public string name { get; set; }
        public string type
        {
            get
            {
                return "to";
            }
        }

    }

    public class headersMandrill
    {
        [JsonProperty(PropertyName = "Reply-To")]
        public string ReplyTo { get; set; }
    }

    public class merge_varsMandrill
    {
        public string rcpt { get; set; }

        private List<varsMandrill> varsLIST = new List<varsMandrill>();
        public List<varsMandrill> vars
        {
            get { return varsLIST; }
            set { varsLIST = value; }
        }
    }
    public class varsMandrill
    {
        public string name { get; set; }
        public string content { get; set; }
    }
    public class attachmentsMandrill
    {
        public string type { get; set; }
        public string name { get; set; }
        public string content { get; set; }
    }

    public class Mandrill
    {
        private bool ColaboradorRecebeEmailMandrill(List<ToMandrill> emailColaborador, string mandrillTemplateId, out string emailBlocked)
        {
            emailBlocked = "";
            var lstEmail = Domain.Core.Configuracoes.NAO_RECEBER_EMAIL_MANDRILL.Split(',');
                        
            foreach (var itemToCheck in emailColaborador)
            {
                foreach (var item in lstEmail)
                {
                    var email = item.Split('|')[0];

                    if (!string.IsNullOrEmpty(email))
                    {
                        var lstBlocks = item.Split('|')[1].Split('*');

                        emailBlocked = itemToCheck.email;
                        if (itemToCheck.email == email && lstBlocks.Where(r => r == mandrillTemplateId).Any())
                            return true;
                    }
                }
            }

            return false;
        }

        public string CallMandrill(DefaultMandrill Mandrill)
        {
            string emailBlocked = string.Empty;
            if(!ColaboradorRecebeEmailMandrill(Mandrill.message.to, Mandrill.template_name, out emailBlocked))
            {
                var json = JsonConvert.SerializeObject(Mandrill);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://mandrillapp.com/api/1.0/messages/send-template.json");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return streamReader.ReadToEnd();
                }
            }
            else
            {
                return JsonConvert.SerializeObject("{Error: \"Blocked Email\", Email: \"" + emailBlocked + "\", Template: \"" + Mandrill.template_name + "\" }");
            }
        }

        public DefaultMandrillConsulta AtualizaStatus(DefaultMandrillConsulta Mandrill)
        {
            var json = JsonConvert.SerializeObject(Mandrill);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://mandrillapp.com/api/1.0/messages/info.json");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var retorno = streamReader.ReadToEnd();

                dynamic dynJson = JsonConvert.DeserializeObject(retorno);

                string state = "";
                string opens = "";
                string clicks = "";
                string bounce_description = "";
                string subject = "";

                state = dynJson.state;
                opens = dynJson.opens;
                clicks = dynJson.clicks;
                bounce_description = dynJson.bounce_description;
                subject = dynJson.subject;

                Mandrill.state = state;
                Mandrill.opens = opens;
                Mandrill.clicks = clicks;
                Mandrill.bounce_description = bounce_description;
                Mandrill.subject = subject;

            }

            return Mandrill;
        }
    }

}