﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Domain.Core
{
    public class Configuracoes
    {

        public static string DOMINIO
        {
            get { return ConfigurationManager.AppSettings["DOMINIO"]; }
        }

        public static string DOMINIO_MOBILE
        {
            get { return ConfigurationManager.AppSettings["DOMINIO_MOBILE"]; }
        }

        public static string DOMINIO_LACOS
        {
            get { return ConfigurationManager.AppSettings["DOMINIO_LACOS"]; }
        }

        public static string EMAIL_NOREPLY
        {
            get { return ConfigurationManager.AppSettings["EMAIL_NOREPLY"]; }
        }

        public static string TOKEN_SENDGRID
        {
            get { return ConfigurationManager.AppSettings["TOKEN_SENDGRID"]; }
        }

        public static string EMAIL_ATENDIMENTO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_ATENDIMENTO"]; }
        }

        public static string EMAIL_RECEBIMENTO_CANCELAMENTO_PEDIDO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_CANCELAMENTO_PEDIDO"]; }
        }

        public static string EMAIL_ATENDIMENTO_LACOS
        {
            get { return ConfigurationManager.AppSettings["EMAIL_ATENDIMENTO_LACOS"]; }
        }

        public static string EMAIL_COMERCIAL_LACOS
        {
            get { return ConfigurationManager.AppSettings["EMAIL_COMERCIAL_LACOS"]; }
        }

        public static string EMAIL_RECEBIMENTO_ERRO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_ERRO"]; }
        }

        public static string EMAIL_RECEBIMENTO_FINANCEIRO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_FINANCEIRO"]; }
        }

        public static string EMAIL_RECEBIMENTO_PEDIDOS
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_PEDIDOS"]; }
        }

        public static string EMAIL_RECEBIMENTO_PEDIDOS_LACOS
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_PEDIDOS_LACOS"]; }
        }

        public static string EMAIL_RECEBIMENTO_FALECONOSCO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_RECEBIMENTO_FALECONOSCO"]; }
        }

        public static string EMAIL_HOMOLOGACAO
        {
            get { return ConfigurationManager.AppSettings["EMAIL_HOMOLOGACAO"]; }
        }

        public static string SAC
        {
            get { return ConfigurationManager.AppSettings["SAC"]; }
        }

        public static string WHATSWEB
        {
            get { return ConfigurationManager.AppSettings["WHATSWEB"]; }
        }

        public static bool HOMOLOGACAO
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["HOMOLOGACAO"]); }
        }

        public static string EMAIL_COPIA
        {
            get { return ConfigurationManager.AppSettings["EMAIL_COPIA"]; }
        }

        public static string PAGSEGURO_EMAIL
        {
            get { return ConfigurationManager.AppSettings["PAGSEGURO_EMAIL"]; }
        }

        public static string PAGSEGURO_TOKEN
        {
            get { return ConfigurationManager.AppSettings["PAGSEGURO_TOKEN"]; }
        }

        public static int BOLETO_CODIGO_CEDENTE
        {
            get { return int.Parse(ConfigurationManager.AppSettings["BOLETO_CODIGO_CEDENTE"]); }
        }

        public static int BOLETO_CODIGO_CEDENTE_LC
        {
            get { return int.Parse(ConfigurationManager.AppSettings["BOLETO_CODIGO_CEDENTE_LC"]); }
        }

        public static string BOLETO_CNPJ
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CNPJ"]; }
        }

        public static string BOLETO_CNPJ_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CNPJ_LC"]; }
        }

        public static string BOLETO_RAZAO_SOCIAL
        {
            get { return ConfigurationManager.AppSettings["BOLETO_RAZAO_SOCIAL"]; }
        }

        public static string BOLETO_RAZAO_SOCIAL_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_RAZAO_SOCIAL_LC"]; }
        }

        public static string BOLETO_AGENCIA
        {
            get { return ConfigurationManager.AppSettings["BOLETO_AGENCIA"]; }
        }

        public static string BOLETO_DIAS_PRIMIRA_RENEGOCIACAO
        {
            get { return ConfigurationManager.AppSettings["BOLETO_DIAS_PRIMIRA_RENEGOCIACAO"]; }
        }
        public static string BOLETO_AGENCIA_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_AGENCIA_LC"]; }
        }

        public static string BOLETO_CONTA_CORRENTE
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CONTA_CORRENTE"]; }
        }

        public static string BOLETO_CONTA_CORRENTE_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CONTA_CORRENTE_LC"]; }
        }

        public static string BOLETO_CARTEIRA
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CARTEIRA"]; }
        }

        public static string BOLETO_CARTEIRA_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_CARTEIRA_LC"]; }
        }

        public static string BOLETO_INSTRUCAO
        {
            get { return ConfigurationManager.AppSettings["BOLETO_INSTRUCAO"]; }
        }

        public static string BOLETO_INSTRUCAO_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_INSTRUCAO_LC"]; }
        }

        public static string FRASE_SMS
        {
            get { return ConfigurationManager.AppSettings["FRASE_SMS"]; }
        }

        public static int BOLETO_DIAS_VENCIMENTO_LC
        {
            get { return int.Parse(ConfigurationManager.AppSettings["BOLETO_DIAS_VENCIMENTO_LC"]); }
        }

        public static int BOLETO_DIAS_VENCIMENTO
        {
            get { return int.Parse(ConfigurationManager.AppSettings["BOLETO_DIAS_VENCIMENTO"]); }
        }

        public static string BOLETO_PASTA
        {
            get { return ConfigurationManager.AppSettings["BOLETO_PASTA"]; }
        }

        public static string BOLETO_PASTA_LC
        {
            get { return ConfigurationManager.AppSettings["BOLETO_PASTA_LC"]; }
        }

        public static string SHOPLINE_CODIGO_EMPRESA
        {
            get { return ConfigurationManager.AppSettings["SHOPLINE_CODIGO_EMPRESA"]; }
        }

        public static string TOKEN_MANDAE
        {
            get { return ConfigurationManager.AppSettings["TOKEN_MANDAE"]; }
        }

        public static string ENDPOINT_MANDAE
        {
            get { return ConfigurationManager.AppSettings["ENDPOINT_MANDAE"]; }
        }

        public static string URL_LOGIN_LC
        {
            get { return ConfigurationManager.AppSettings["URL_LOGIN_LC"]; }
        }

        public static string SHOPLINE_CHAVE
        {
            get { return ConfigurationManager.AppSettings["SHOPLINE_CHAVE"]; }
        }

        public static string FORNECEDORES_PRINCIPAIS_KITBB_PERSONALIZADO
        {
            get { return ConfigurationManager.AppSettings["FORNECEDORES_PRINCIPAIS_KITBB_PERSONALIZADO"]; }
        }
public static string NAO_RECEBER_EMAIL_MANDRILL
        {
            get { return ConfigurationManager.AppSettings["NAO_RECEBER_EMAIL_MANDRILL"]; }
        }
        public static string PAYPAL_EMAIL
        {
            get { return ConfigurationManager.AppSettings["PAYPAL_EMAIL"]; }
        }

        public static string YourViews_Token
        {
            get { return ConfigurationManager.AppSettings["YourViews_Token"]; }
        }
        public static string YourViews_User
        {
            get { return ConfigurationManager.AppSettings["YourViews_User"]; }
        }
        public static string YourViews_Pass
        {
            get { return ConfigurationManager.AppSettings["YourViews_Pass"]; }
        }

        public static string PAYPAL_TOKEN
        {
            get { return ConfigurationManager.AppSettings["PAYPAL_TOKEN"]; }
        }

        public static int ITENS_PAGINA_SITE
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["ITENS_PAGINA_SITE"]); }
        }

        public static int ITENS_PAGINA_ADMIN
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["ITENS_PAGINA_ADMIN"]); }
        }
        
        public static string CDN_URL
        {
            get { return ConfigurationManager.AppSettings["CDN_URL"]; }
        }
        
        public static string MOVILE_TOKEN
        {
            get { return ConfigurationManager.AppSettings["MOVILE_TOKEN"]; }
        }
        public static string MOVILE_USER
        {
            get { return ConfigurationManager.AppSettings["MOVILE_USER"]; }
        }

        public static string BOLETO_DIAS_SEGUNDA_RENEGOCIACAO
        {
            get { return ConfigurationManager.AppSettings["BOLETO_DIAS_SEGUNDA_RENEGOCIACAO"]; }
        }

        public static string CDN_IMG
        {
            get { return ConfigurationManager.AppSettings["CDN_IMG"]; }
        }
        public static bool ENVIA_TRUSTVOX
        {
            get { return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["ENVIA_TRUSTVOX"]); }
        }

        public static string RedeCard_Filiacao
        {
            get { return ConfigurationManager.AppSettings["RedeCard_Filiacao"]; }
        }

        public static string RedeCard_Senha
        {
            get { return ConfigurationManager.AppSettings["RedeCard_Senha"]; }
        }
        public static string PagarMe_DefaultApiKey
        {
            get { return ConfigurationManager.AppSettings["PagarMe_DefaultApiKey"]; }
        }
        public static string PagarMe_DefaultCriptKey
        {
            get { return ConfigurationManager.AppSettings["PagarMe_DefaultCriptKey"]; }
        }

        public static string Focus_Autorizar
        {
            get {
                if (HOMOLOGACAO)
                {
                    return "http://homologacao.acrasnfe.acras.com.br/nfe2/";
                }
                else
                {
                    return ConfigurationManager.AppSettings["Focus_Autorizar"];
                }
            }
        }

        public static string Mandrill_Token
        {
            get { return ConfigurationManager.AppSettings["Mandrill_Token"]; }
        }
        public static string Focus_Token
        {
            get { return ConfigurationManager.AppSettings["Focus_Token"]; }
        }
        public static string Focus_Token_LC
        {
            get { return ConfigurationManager.AppSettings["Focus_Token_LC"]; }
        }
        public static string NR_SMS_HOMOLOGACAO
        {
            get { return ConfigurationManager.AppSettings["NR_SMS_HOMOLOGACAO"]; }
        }
        public static string Focus_LinkNFE
        {
            get {
                if (HOMOLOGACAO)
                {
                    return "http://homologacao.acrasnfe.acras.com.br";
                }
                else
                {
                    return ConfigurationManager.AppSettings["Focus_LinkNFE"];
                }
                
            }
        }

        public static string SMS_1_FORNECEDORES
        {
            get { return ConfigurationManager.AppSettings["SMS_1_FORNECEDORES"]; }
        }
        public static string SMS_2_FORNECEDORES
        {
            get { return ConfigurationManager.AppSettings["SMS_2_FORNECEDORES"]; }
        }
        public static string SMS_3_FORNECEDORES
        {
            get { return ConfigurationManager.AppSettings["SMS_3_FORNECEDORES"]; }
        }
    }
}
