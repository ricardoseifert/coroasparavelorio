﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Net;
using System.IO;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using Domain.Repositories;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace Domain.Core
{
    public static class Mandae
    {
        public static Entities.Integracoes.MandadeRastreamentoResult RastrearEntrega(string NrRastreamento)
        {
            #region INTEGRACAO MANDAE

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", Domain.Core.Configuracoes.TOKEN_MANDAE);
            
            var result = httpClient.GetAsync(Domain.Core.Configuracoes.ENDPOINT_MANDAE + "trackings/" + NrRastreamento).Result;
            var contents = result.Content.ReadAsStringAsync().Result;

            dynamic data = JObject.Parse(contents);
            string Erro = "";

            try
            {
                if (data.error != null)
                    Erro = contents;
            }
            catch { }

            #endregion

            if (!string.IsNullOrEmpty(Erro))
                return null;

            return JsonConvert.DeserializeObject<Entities.Integracoes.MandadeRastreamentoResult>(contents);
        }

        public static Entities.Integracoes.MandaeFreteResult CotacaoEntrega(string CEP, Domain.Entities.Integracoes.MandaeFrete mandaeFrete)
        {
            #region INTEGRACAO MANDAE

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", Domain.Core.Configuracoes.TOKEN_MANDAE);

            var content = new StringContent(JsonConvert.SerializeObject(mandaeFrete), Encoding.UTF8, "application/json");
            var result = httpClient.PostAsync(Domain.Core.Configuracoes.ENDPOINT_MANDAE + "postalcodes/" + CEP + "/rates", content).Result;
            var contents = result.Content.ReadAsStringAsync().Result;

            dynamic data = JObject.Parse(contents);
            string Erro = "";

            try
            {
                if (data.error != null)
                    Erro = contents;
            }
            catch { }

            #endregion

            if (!string.IsNullOrEmpty(Erro))
                return null;

            return JsonConvert.DeserializeObject<Entities.Integracoes.MandaeFreteResult>(contents);
        }
    }
}