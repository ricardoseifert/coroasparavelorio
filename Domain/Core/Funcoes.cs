﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Web;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Globalization; 
using System.Web.Hosting;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.parser;
using System.Data.Objects.DataClasses;


namespace System.Web.Mvc
{
    public class SelectListItem2
    {
        public SelectListItem2()
        {
        }

        public bool Selected { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string Extra { get; set; }
    }
}

namespace Domain.Core
{
    public class Funcoes
    {
        [EdmFunction("Edm", "TruncateTime")]
        public static DateTime? TruncateTime(DateTime? date)
        {
            return date.HasValue ? date.Value.Date : (DateTime?)null;
        }

        public static string GeraRotuloMVC(object Title)
        {
            string strTitle = Title.ToString();
            #region Generate SEO Friendly URL based on Title
            //Trim Start and End Spaces.
            strTitle = strTitle.Trim();
            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&""./".ToCharArray();
            strTitle = strTitle.Replace("c#", "C-Sharp");
            strTitle = strTitle.Replace("vb.net", "VB-Net");
            strTitle = strTitle.Replace("asp.net", "Asp-Net");
            //Replace . with - hyphen
            strTitle = strTitle.Replace(".", "-");
            //Replace Special-Characters
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar)) { strTitle = strTitle.Replace(strChar, string.Empty); }
            }
            strTitle = strTitle.Replace("/", "-");
            strTitle = strTitle.Replace("º", "-");
            strTitle = strTitle.Replace("³", "-");
            //Replace all spaces with one "-" hyphen
            strTitle = strTitle.Replace(" ", "-");
            //Replace multiple "-" hyphen with single "-" hyphen.
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("-----", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = AcertaAcentos(strTitle);
            //Run the code again...//Trim Start and End Spaces.
            strTitle = strTitle.Trim();
            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            #endregion
            return strTitle.ToLower();
        }
        
        public DateTime CorrigirFeriado(System.DateTime source)
        {
            while (EhFeriado(source))
            {
                source = AddBusinessDays(source, 1);
            }

            return source;
        }

        public DateTime AddBusinessDays(System.DateTime source, int businessDays)
        {
            var dayOfWeek = businessDays < 0
                                ? ((int)source.DayOfWeek - 12) % 7
                                : ((int)source.DayOfWeek + 6) % 7;

            switch (dayOfWeek)
            {
                case 6:
                    businessDays--;
                    break;
                case -6:
                    businessDays++;
                    break;
            }

            return source.AddDays(businessDays + ((businessDays + dayOfWeek) / 5) * 2);
        }
        public DateTime DataVencimentoUtil(int? dias = null)
        {
            var data = DateTime.Now.AddDays(Configuracoes.BOLETO_DIAS_VENCIMENTO);
            if (dias.HasValue)
                data = DateTime.Now.AddDays(dias.Value);

            while (!EhDiaUtil(data))
            {
                data = data.AddDays(1);
            }
            return data;
        }
        public bool EhDiaUtil(DateTime data)
        {
            return (data.DayOfWeek < DayOfWeek.Saturday &&
                data.DayOfWeek > DayOfWeek.Sunday &&
                !EhFeriado(data));
        }

        public bool EhFeriado(DateTime data)
        {
            if (data.Day == 1 && data.Month == 1)
                return true;

            // CARNAVAL 2018
            else if (data.Day == 4 && data.Month == 3)
                return true;
            else if (data.Day == 5 && data.Month == 3)
                return true;
            else if (data.Day == 6 && data.Month == 3)
                return true;

            else if (data.Day == 19 && data.Month == 4)
                return true;
            else if (data.Day == 21 && data.Month == 4)
                return true;

            else if (data.Day == 1 && data.Month == 5)
                return true;

            else if (data.Day == 20 && data.Month == 6)
                return true;

            else if (data.Day == 7 && data.Month == 9)
                return true;

            else if (data.Day == 12 && data.Month == 10)
                return true;

            else if (data.Day == 2 && data.Month == 11)
                return true;
            else if (data.Day == 15 && data.Month == 11)
                return true;
            else if (data.Day == 20 && data.Month == 11)
                return true;

            else if (data.Day == 24 && data.Month == 12)
                return true;
            else if (data.Day == 25 && data.Month == 12)
                return true;
            else if (data.Day == 31 && data.Month == 12)
                return true;
            else
                return false;
        }

        public static void RotateImage(string Path, string Direction)
        {
            if (Domain.Core.Configuracoes.HOMOLOGACAO)
            {
                Path =  "/content/foto_admin_homolog_solution.jpg";
            }

            // get the full path of image url
            string path = System.Web.HttpContext.Current.Server.MapPath(Path);

            // creating image from the image url
            System.Drawing.Image i = System.Drawing.Image.FromFile(path);

            if(Direction == "left")
            {
                i.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else
            {
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            
            // save it to its actual path
            i.Save(path);

            // release Image File
            i.Dispose();
        }

        public static string ConvertImageBase64(string imagem)
        {
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(HostingEnvironment.MapPath(imagem)))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }    

        // O método toExtenso recebe um valor do tipo decimal
        public static string NumeroPorExtenso(decimal valor)
        {
            if (valor <= 0 | valor >= 1000000000000000)
                return "Valor não suportado pelo sistema.";
            else
            {
                string strValor = valor.ToString("000000000000000.00");
                string valor_por_extenso = string.Empty;

                for (int i = 0; i <= 15; i += 3)
                {
                    valor_por_extenso += escreva_parte(Convert.ToDecimal(strValor.Substring(i, 3)));
                    if (i == 0 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(0, 3)) == 1)
                            valor_por_extenso += " TRILHÃO" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(0, 3)) > 1)
                            valor_por_extenso += " TRILHÕES" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 3 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(3, 3)) == 1)
                            valor_por_extenso += " BILHÃO" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(3, 3)) > 1)
                            valor_por_extenso += " BILHÕES" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 6 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(6, 3)) == 1)
                            valor_por_extenso += " MILHÃO" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(6, 3)) > 1)
                            valor_por_extenso += " MILHÕES" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 9 & valor_por_extenso != string.Empty)
                        if (Convert.ToInt32(strValor.Substring(9, 3)) > 0)
                            valor_por_extenso += " MIL" + ((Convert.ToDecimal(strValor.Substring(12, 3)) > 0) ? " E " : string.Empty);

                    if (i == 12)
                    {
                        if (valor_por_extenso.Length > 8)
                            if (valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "BILHÃO" | valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "MILHÃO")
                                valor_por_extenso += " DE";
                            else
                                if (valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "BILHÕES" | valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "MILHÕES" | valor_por_extenso.Substring(valor_por_extenso.Length - 8, 7) == "TRILHÕES")
                                    valor_por_extenso += " DE";
                                else
                                    if (valor_por_extenso.Substring(valor_por_extenso.Length - 8, 8) == "TRILHÕES")
                                        valor_por_extenso += " DE";

                        if (Convert.ToInt64(strValor.Substring(0, 15)) == 1)
                            valor_por_extenso += " REAL";
                        else if (Convert.ToInt64(strValor.Substring(0, 15)) > 1)
                            valor_por_extenso += " REAIS";

                        if (Convert.ToInt32(strValor.Substring(16, 2)) > 0 && valor_por_extenso != string.Empty)
                            valor_por_extenso += " E ";
                    }

                    if (i == 15)
                        if (Convert.ToInt32(strValor.Substring(16, 2)) == 1)
                            valor_por_extenso += " CENTAVO";
                        else if (Convert.ToInt32(strValor.Substring(16, 2)) > 1)
                            valor_por_extenso += " CENTAVOS";
                }
                return valor_por_extenso;
            }
        }

        static string escreva_parte(decimal valor)
        {
            if (valor <= 0)
                return string.Empty;
            else
            {
                string montagem = string.Empty;
                if (valor > 0 & valor < 1)
                {
                    valor *= 100;
                }
                string strValor = valor.ToString("000");
                int a = Convert.ToInt32(strValor.Substring(0, 1));
                int b = Convert.ToInt32(strValor.Substring(1, 1));
                int c = Convert.ToInt32(strValor.Substring(2, 1));

                if (a == 1) montagem += (b + c == 0) ? "CEM" : "CENTO";
                else if (a == 2) montagem += "DUZENTOS";
                else if (a == 3) montagem += "TREZENTOS";
                else if (a == 4) montagem += "QUATROCENTOS";
                else if (a == 5) montagem += "QUINHENTOS";
                else if (a == 6) montagem += "SEISCENTOS";
                else if (a == 7) montagem += "SETECENTOS";
                else if (a == 8) montagem += "OITOCENTOS";
                else if (a == 9) montagem += "NOVECENTOS";

                if (b == 1)
                {
                    if (c == 0) montagem += ((a > 0) ? " E " : string.Empty) + "DEZ";
                    else if (c == 1) montagem += ((a > 0) ? " E " : string.Empty) + "ONZE";
                    else if (c == 2) montagem += ((a > 0) ? " E " : string.Empty) + "DOZE";
                    else if (c == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TREZE";
                    else if (c == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUATORZE";
                    else if (c == 5) montagem += ((a > 0) ? " E " : string.Empty) + "QUINZE";
                    else if (c == 6) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSEIS";
                    else if (c == 7) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSETE";
                    else if (c == 8) montagem += ((a > 0) ? " E " : string.Empty) + "DEZOITO";
                    else if (c == 9) montagem += ((a > 0) ? " E " : string.Empty) + "DEZENOVE";
                }
                else if (b == 2) montagem += ((a > 0) ? " E " : string.Empty) + "VINTE";
                else if (b == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TRINTA";
                else if (b == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUARENTA";
                else if (b == 5) montagem += ((a > 0) ? " E " : string.Empty) + "CINQUENTA";
                else if (b == 6) montagem += ((a > 0) ? " E " : string.Empty) + "SESSENTA";
                else if (b == 7) montagem += ((a > 0) ? " E " : string.Empty) + "SETENTA";
                else if (b == 8) montagem += ((a > 0) ? " E " : string.Empty) + "OITENTA";
                else if (b == 9) montagem += ((a > 0) ? " E " : string.Empty) + "NOVENTA";

                if (strValor.Substring(1, 1) != "1" & c != 0 & montagem != string.Empty) montagem += " E ";

                if (strValor.Substring(1, 1) != "1")
                    if (c == 1) montagem += "UM";
                    else if (c == 2) montagem += "DOIS";
                    else if (c == 3) montagem += "TRÊS";
                    else if (c == 4) montagem += "QUATRO";
                    else if (c == 5) montagem += "CINCO";
                    else if (c == 6) montagem += "SEIS";
                    else if (c == 7) montagem += "SETE";
                    else if (c == 8) montagem += "OITO";
                    else if (c == 9) montagem += "NOVE";

                return montagem;
            }
        }

        public static string HtmlEncode2(string text)
        {
            char[] chars = text.ToCharArray();
            StringBuilder result = new StringBuilder(text.Length + (int)(text.Length * 0.1));

            foreach (char c in chars)
            {
                int value = Convert.ToInt32(c);
                if (value >= 192 && value <= 255) // special chars
                    result.AppendFormat("&#{0};", value);
                else
                    result.Append(c);
            }

            return result.ToString();
        }

        public static void ImprimirPDF(string html, string nome)
        {

            html = html.Replace("/Content", Domain.Core.Configuracoes.DOMINIO + "/Content");

            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + nome);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            var imagePath = Domain.Core.Funcoes.GerarImagem(null, html);
            Document doc = new Document();
            PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            doc.Open();
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagePath); 
            gif.ScalePercent(24f);
            gif.ScaleToFit(879f, 1464.75f); 
            doc.Add(gif);
            doc.Close();

            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush(); 

        }

        public static byte[] GerarImagem(string url, string html)
        {
            int width = 1172;
            int height = 1953;

            int webBrowserWidth = 1172;
            int webBrowserHeight = 1953;

            System.Drawing.Bitmap bmp = WebsiteThumbnailImageGenerator.GetWebSiteThumbnail(url, html, webBrowserWidth, webBrowserHeight, width, height);
            MemoryStream stream = new MemoryStream();
            ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1); 
            myEncoderParameters.Param[0] = new EncoderParameter(myEncoder, 100L);
            bmp.Save(stream, jgpEncoder,myEncoderParameters);
            return stream.GetBuffer(); 
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public static bool IsMobile()
        {
            string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
                return true;
            else
                return false;
        }

        public static string GeraNomeArquivoMVC(object Title)
        {
            string strTitle = Title.ToString();
            #region Generate SEO Friendly URL based on Title
            //Trim Start and End Spaces.
            strTitle = strTitle.Trim();
            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&""/".ToCharArray();
            strTitle = strTitle.Replace("c#", "C-Sharp");
            strTitle = strTitle.Replace("vb.net", "VB-Net");
            strTitle = strTitle.Replace("asp.net", "Asp-Net");
            //Replace . with - hyphen
            //Replace Special-Characters
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar)) { strTitle = strTitle.Replace(strChar, string.Empty); }
            }
            strTitle = strTitle.Replace("/", "-");
            strTitle = strTitle.Replace("º", "-");
            strTitle = strTitle.Replace("³", "-");
            //Replace all spaces with one "-" hyphen
            strTitle = strTitle.Replace(" ", "-");
            //Replace multiple "-" hyphen with single "-" hyphen.
            strTitle = strTitle.Replace("--", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("-----", "-");
            strTitle = strTitle.Replace("----", "-");
            strTitle = strTitle.Replace("---", "-");
            strTitle = strTitle.Replace("--", "-");
            strTitle = AcertaAcentos(strTitle);
            //Run the code again...//Trim Start and End Spaces.
            strTitle = strTitle.Trim();
            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            #endregion
            return strTitle.ToLower();
        }
        
        public static string SomenteLetrasNumeros(object Title)
        {
            string strTitle = Title.ToString();
            #region Generate SEO Friendly URL based on Title
            //Trim Start and End Spaces.
            strTitle = strTitle.Trim();
            //Trim "-" Hyphen
            strTitle = strTitle.Trim('-');
            strTitle = strTitle.ToLower();
            char[] chars = @"$%#@!*?;:~`+=()[]{}|\'<>,/^&""/".ToCharArray();
            strTitle = strTitle.Replace("c#", "C-Sharp");
            strTitle = strTitle.Replace("vb.net", "VB-Net");
            strTitle = strTitle.Replace("asp.net", "Asp-Net");
            strTitle = strTitle.Replace("°", "");
            strTitle = strTitle.Replace("ª", "A");
            //Replace . with - hyphen
            //Replace Special-Characters
            for (int i = 0; i < chars.Length; i++)
            {
                string strChar = chars.GetValue(i).ToString();
                if (strTitle.Contains(strChar)) { strTitle = strTitle.Replace(strChar, string.Empty); }
            }
            strTitle = strTitle.Replace("/", " ");
            strTitle = strTitle.Replace("º", " ");
            strTitle = strTitle.Replace("³", " ");
            strTitle = strTitle.Replace("-", " ");
            strTitle = strTitle.Replace(".", " ");

            strTitle = AcertaAcentos(strTitle);
            strTitle = strTitle.Trim();            
            
            #endregion

            return strTitle.ToLower();
        }

        public static string EncodeNonAsciiCharacters(string value)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in value)
            {
                if (c > 127)
                {
                    // This character is too big for ASCII
                    string encodedValue = "\\u" + ((int)c).ToString("x4");
                    sb.Append(encodedValue);
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                m =>
                {
                    return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
                });
        }
        public static string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }

        public static string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }

        public static double MilliTimeStamp(DateTime data)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var time = data.Subtract(new TimeSpan(epoch.Ticks));
            return (long)(time.Ticks / 10000);
        }

        public static string AcertaAcentos(string texto)
        {
            if (!String.IsNullOrEmpty(texto))
            {
                string[] subst = { "a", "a", "a", "a", "A", "A", "A", "A", "c", "C", "e", "e", "e", "E", "E", "E", "E", "i", "I", "i", "I", "o", "o", "o", "o", "O", "O", "O", "O", "u", "u", "u", "U", "U", "U", " ", " ", " ", " " };
                string[] acentos = { "á", "à", "ã", "â", "Á", "À", "Ã", "Â", "ç", "Ç", "è", "é", "ê", "È", "É", "È", "Ê", "í", "Í", "ì", "Ì", "ò", "ó", "õ", "ô", "Ò", "Ó", "Õ", "Ô", "ù", "ú", "ü", "Ù", "Ú", "Ü", "´", "`", "~", "^" };
                for (int i = 0; i <= acentos.Length - 1; i++)
                {
                    texto = texto.Replace(acentos[i], subst[i]);
                }
            }
            return texto;
        }

        public static string SomenteAlfanumerico(string texto)
        {
            if (!String.IsNullOrEmpty(texto))
            {
                Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                texto = rgx.Replace(texto, "");
            }
            return texto;
        }

        //public static CEP _CEP = new CEP();
        //public static CEP BuscaCEP(string CEP)
        //{
        //    string ServiceUri = "http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" + CEP;
        //    WebClient proxy = new WebClient();
        //    proxy.DownloadStringCompleted += new DownloadStringCompletedEventHandler(proxy_DownloadStringCompleted);
        //    proxy.DownloadStringAsync(new Uri(ServiceUri));
        //    return _CEP;
        //}

        //private static void proxy_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        //{
        //    var valor = e.Result.Replace("\n", "")
        //        .Replace("\t", "")
        //        .Replace("\"", "")
        //        .Replace("var resultadoCEP = ", "")
        //        .Replace("'", "\"")
        //        .Replace(" : ", ":")
        //        .Replace("%E1", "á")
        //        .Replace("%E2", "â")
        //        .Replace("%E3", "ã")
        //        .Replace("%C1", "Á")
        //        .Replace("%C2", "Â")
        //        .Replace("%C3", "Ã")
        //        .Replace("%C9", "É")
        //        .Replace("%E9", "é")
        //        .Replace("%EA", "ê")
        //        .Replace("%CD", "Í")
        //        .Replace("%ED", "í")
        //        .Replace("%D3", "Ó")
        //        .Replace("%D4", "Ô")
        //        .Replace("%D5", "Õ")
        //        .Replace("%F3", "ó")
        //        .Replace("%F5", "õ")
        //        .Replace("%F4", "ô")
        //        .Replace("%DA", "Ú")
        //        .Replace("%DC", "Ü")
        //        .Replace("%FA", "ú")
        //        .Replace("%FC", "ü")
        //        .Replace("%E7", "ç")
        //        .Replace("%C7", "Ç");

        //    Stream stream = new MemoryStream(Encoding.Unicode.GetBytes(System.Web.HttpUtility.UrlDecode(valor)));
        //    stream.Position = 0;
        //    DataContractJsonSerializer obj = new DataContractJsonSerializer(typeof(CEP));
        //    var cep = (CEP)obj.ReadObject(stream);
        //    if (cep.resultado == "1")
        //    {
        //        _CEP = cep;
        //    }
        //}


        public static string Formata_Telefone(string p_telefone)
        {
            try
            {
                p_telefone = p_telefone.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Trim();
                if (p_telefone.Length == 11)
                {
                    try
                    {
                        return "(" + p_telefone.Substring(0, 2) + ") " + p_telefone.Substring(2, 5) + "-" + p_telefone.Substring(7, 4);
                    }
                    catch
                    {
                        return p_telefone;
                    }
                }
                else if (p_telefone.Length == 10)
                {
                    try
                    {
                        return "(" + p_telefone.Substring(0, 2) + ") " + p_telefone.Substring(2, 4) + "-" + p_telefone.Substring(6, 4);
                    }
                    catch
                    {
                        return p_telefone;
                    }
                }
                else
                    return p_telefone;
            }
            catch
            {
                return p_telefone;
            }
        }

        public static string Formata_CNPJ(string p_CNPJ)
        {
            try
            {
                if (p_CNPJ.Length == 14)
                {
                    try
                    {
                        return p_CNPJ.Substring(0, 2) + "." + p_CNPJ.Substring(2, 3) + "." + p_CNPJ.Substring(5, 3) + "/" + p_CNPJ.Substring(8, 4) + "-" + p_CNPJ.Substring(12, 2);
                    }
                    catch
                    {
                        return p_CNPJ;
                    }
                }
                else
                    if (p_CNPJ.Length == 11)
                    {
                        try
                        {
                            return p_CNPJ.Substring(0, 3) + "." + p_CNPJ.Substring(3, 3) + "." + p_CNPJ.Substring(6, 3) + "-" + p_CNPJ.Substring(9, 2);
                        }
                        catch
                        {
                            return p_CNPJ;
                        }
                    }
                else
                    return p_CNPJ;
            }
            catch
            {
                return p_CNPJ;
            }
        }

        public static bool Valida_CNPJ(string p_CNPJ)
        {
            bool isValid = false;

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            p_CNPJ = p_CNPJ.Trim();
            p_CNPJ = p_CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");

            if (p_CNPJ.Length != 14)
                isValid = false;
            else
            {
                tempCnpj = p_CNPJ.Substring(0, 12);

                soma = 0;
                for (int i = 0; i < 12; i++)
                    soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

                resto = (soma % 11);
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                digito = resto.ToString();

                tempCnpj = tempCnpj + digito;
                soma = 0;
                for (int i = 0; i < 13; i++)
                    soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

                resto = (soma % 11);
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                digito = digito + resto.ToString();

                isValid = p_CNPJ.EndsWith(digito);
            }
            return isValid;
        }

        public static bool Valida_CPF(string p_CPF)
        {
            bool isValid = false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            p_CPF = p_CPF.Trim();
            p_CPF = p_CPF.Replace(".", "").Replace("-", "");

            if (p_CPF.Length != 11)
                isValid = false;
            else
            {
                tempCpf = p_CPF.Substring(0, 9);
                soma = 0;
                for (int i = 0; i < 9; i++)
                    soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

                resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                digito = resto.ToString();

                tempCpf = tempCpf + digito;

                soma = 0;
                for (int i = 0; i < 10; i++)
                    soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

                resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                digito = digito + resto.ToString();

                isValid = p_CPF.EndsWith(digito);
            }
            return isValid;
        }

        public static string AcentoToSQL(string texto)
        {
            texto = texto.ToUpper();
            string[] subst = { "[a,á,à,ã,â,ä,A]", "[e,é,è,ê,ë,E]", "[i,í,ì,î,ï,I]", "[o,ó,ò,õ,ô,ö,O]", "[u,ú,ù,û,ü,U]", "[c,ç]" };
            string[] acentos = { "A", "E", "I", "O", "U", "C" };
            for (int i = 0; i <= acentos.Length - 1; i++)
            {
                texto = texto.Replace(acentos[i], subst[i]);
            }
            return texto;
        }

        private const string SenhaCaracteresValidos = "abcdefghijklmnopqrstuvwxyz1234567890@#!?";
        public static string CriarSenha(int tamanho)
        {
            int valormaximo = SenhaCaracteresValidos.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            StringBuilder senha = new StringBuilder(tamanho);

            for (int indice = 0; indice < tamanho; indice++)
                senha.Append(SenhaCaracteresValidos[random.Next(0, valormaximo)]);

            return senha.ToString();
        }

        public static bool ValidaURL(String p_URL)
        {

            string pattern = @"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";

            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            return reg.IsMatch(p_URL);
        }

        public static bool ValidaEmail(string p_Email)
        {
            if (!String.IsNullOrEmpty(p_Email))
            {
                string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(strRegex);
                if (re.IsMatch(p_Email))
                    return (true);
                else
                    return (false);
            }
            else
                return (false);
        }

        public static string ConvertUtf8ToISO(String input)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(input);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            string msg = iso.GetString(isoBytes);

            return msg;
        }


        public static string ConvertStringToHex(String input)
        {
            Byte[] stringBytes = System.Text.Encoding.Unicode.GetBytes(input);
            StringBuilder sbBytes = new StringBuilder(stringBytes.Length * 2);
            foreach (byte b in stringBytes)
            {
                sbBytes.AppendFormat("{0:X2}", b);
            }
            return sbBytes.ToString();
        }

        public static string ConvertHexToString(String hexInput)
        {
            int numberChars = hexInput.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hexInput.Substring(i, 2), 16);
            }
            return System.Text.Encoding.Unicode.GetString(bytes);
        }

        public static string HttpPost(String url, String query="")
        {
            // Declarações necessárias
            Stream requestStream = null;
            WebResponse response = null;
            StreamReader reader = null;

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Post;

                // Neste ponto, você está setando a propriedade ContentType da página 
                // para urlencoded para que o comando POST seja enviado corretamente
                request.ContentType = "application/x-www-form-urlencoded";

                StringBuilder urlEncoded = new StringBuilder();

                // Separando cada parâmetro
                Char[] reserved = { '?', '=', '&' };

                // alocando o bytebuffer
                byte[] byteBuffer = null;

                // caso a URL seja preenchida
                if (query != null)
                {
                    int i = 0, j;
                    // percorre cada caractere da url atraz das palavras reservadas para separação
                    // dos parâmetros
                    while (i < query.Length)
                    {
                        j = query.IndexOfAny(reserved, i);
                        if (j == -1)
                        {
                            urlEncoded.Append(query.Substring(i, query.Length - i));
                            break;
                        }
                        urlEncoded.Append(query.Substring(i, j - i));
                        urlEncoded.Append(query.Substring(j, 1));
                        i = j + 1;
                    }
                    // codificando em UTF8 (evita que sejam mostrados códigos malucos em caracteres especiais
                    byteBuffer = Encoding.UTF8.GetBytes(urlEncoded.ToString());

                    request.ContentLength = byteBuffer.Length;
                    requestStream = request.GetRequestStream();
                    requestStream.Write(byteBuffer, 0, byteBuffer.Length);
                    requestStream.Close();
                }
                else
                {
                    request.ContentLength = 0;
                }

                // Dados recebidos 
                response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();

                // Codifica os caracteres especiais para que possam ser exibidos corretamente
                System.Text.Encoding encoding = System.Text.Encoding.Default;

                // Preenche o reader
                reader = new StreamReader(responseStream, encoding);

                Char[] charBuffer = new Char[256];
                int count = reader.Read(charBuffer, 0, charBuffer.Length);

                StringBuilder Dados = new StringBuilder();

                // Lê cada byte para preencher meu stringbuilder
                while (count > 0)
                {
                    Dados.Append(new String(charBuffer, 0, count));
                    count = reader.Read(charBuffer, 0, charBuffer.Length);
                }

                // Imprimo o que recebi
                return Dados.ToString();
            }
            catch (Exception e)
            {
                // Ocorreu algum erro
                return "Erro: " + e.Message;
            }
            finally
            {
                // Fecha tudo
                if (requestStream != null)
                    requestStream.Close();
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image imgToResize, Size size, System.Drawing.Rectangle cropArea, int w, int h)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            if (destWidth < w)
                destWidth += (w - destWidth);
            if (destHeight < h)
                destHeight += (h - destHeight);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            Bitmap bmpCrop = b.Clone(cropArea, b.PixelFormat);

            return (System.Drawing.Image)(bmpCrop);
        }

        public static string HtmlEncode(string text)
        {
            text = System.Web.HttpUtility.HtmlEncode(text);
            text = text.Replace("&#224;", "&agrave;");
            text = text.Replace("&#225;", "&aacute;");
            text = text.Replace("&#226;", "&acirc;");
            text = text.Replace("&#227;", "&atilde;");
            text = text.Replace("&#231;", "&ccedil;");
            text = text.Replace("&#233;", "&eacute;");
            text = text.Replace("&#234;", "&ecirc;");
            text = text.Replace("&#237;", "&iacute;");
            text = text.Replace("&#241;", "&ntilde;");
            text = text.Replace("&#243;", "&oacute;");
            text = text.Replace("&#244;", "&ocirc;");
            text = text.Replace("&#245;", "&otilde;");
            text = text.Replace("&#250;", "&uacute;");
            return text;
        }

        public static string AcertaTextoUtf8(string texto)
        {
            texto = texto.Replace("Ã¡", "á");
            texto = texto.Replace("Ã£", "ã");
            // texto = texto.Replace("Ã", "í");
            texto = texto.Replace("Ã³", "é");
            texto = texto.Replace("Ã©", "é");
            texto = texto.Replace("Ã‰", "É");
            texto = texto.Replace("Ãª", "ê");
            texto = texto.Replace("Ã‡", "Ç");
            texto = texto.Replace("Ã•", "Õ");
            texto = texto.Replace("Ã§", "ç");
            texto = texto.Replace("Ãµ", "õ");
            texto = texto.Replace("Ã´", "ô");
            texto = texto.Replace("Ãº", "ú");
            texto = texto.Replace("Ã¡", "á");
            texto = texto.Replace("Ã¢", "â");
            texto = texto.Replace("Ã£", "ã");
            texto = texto.Replace("Ã©", "é");
            texto = texto.Replace("Ãª", "ê");
            texto = texto.Replace("Ã³", "ó");
            texto = texto.Replace("Ã´", "ô");
            texto = texto.Replace("Ãµ", "õ");
            texto = texto.Replace("Ãº", "ú");
            texto = texto.Replace("Ã§", "ç");
            texto = texto.Replace("Ã", "í");
            texto = texto.Replace("íƒ", "Ã");
            return texto;
        }

        public static string GetFirstImageHtml(string html)
        {
            var imgReg = new Regex("src=(?:\"|\')?(?<imgSrc>[^>]*[^/].(?:jpg|png))(?:\"|\')?");
            return imgReg.Match(html).Groups[1].Value;
        }

        public static string RemoveHtml(string html)
        {
            if (html != null)
            {
                if (html.Length > 0)
                {
                    string acceptable = "i|b|u|ol|ul|li|br|p|strong|table|tr|td";
                    string stringPattern = @"</?(?(?=" + acceptable + @")notag|[a-z,A-Z,0-9]+)(?:\s[a-z,A-Z,0-9,\-]+=?(?:(["",']?).*?\1?))*\s*/?>";
                    return HttpUtility.HtmlDecode(Regex.Replace(html, stringPattern, string.Empty));
                }
                else
                    return html;
            }
            else
                return "";
        }

        public static string RemoveTodoHtml(string html)
        {
            string stringPattern = @"<[^>]*>";
            return HttpUtility.HtmlDecode(Regex.Replace(html, stringPattern, string.Empty));
        }

        public static string ResumoSemHTML(string texto, int total)
        {

            string[] palavras = RemoveTodoHtml(texto.Replace("<br>", ". ")).Split(' ');
            texto = "";
            if (palavras.Count() > 0)
            {
                int i = 0;
                foreach (string palavra in palavras)
                {
                    texto += palavra + " ";
                    i++;
                    if (i == palavras.Count() - 1 || i > total)
                        break;
                }
            }
            return texto + " ...";
        }

        public static bool EnviaEmail(string emailDe, string nomeDe, string emailPara, string nomePara, string emailReturn, string nomeReturn, string emailCopia, string assunto, string corpo, bool html, MailPriority prioridade = MailPriority.Normal)
        {
            SmtpClient smtp = new SmtpClient();
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(emailDe, nomeDe);
            mailMessage.Priority = prioridade;

            if (!String.IsNullOrEmpty(emailReturn))
            {
                var emailsReturn = emailReturn.Split(',');
                foreach (var email in emailsReturn)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(email));
                }
            }

            if (Domain.Core.Configuracoes.HOMOLOGACAO == false)
            {
                var emails = emailPara.Split(',');
                foreach (var email in emails)
                {
                    mailMessage.To.Add(new MailAddress(email.Trim(), nomePara));
                }

                if (!string.IsNullOrEmpty(emailCopia))
                {
                    if (Funcoes.ValidaEmail(emailCopia))
                        mailMessage.CC.Add(new MailAddress(emailCopia.Trim()));
                }
            }
            else
            {
                mailMessage.To.Add(new MailAddress(Domain.Core.Configuracoes.EMAIL_HOMOLOGACAO, "HOMOLOGAÇÃO"));
            }

            mailMessage.Subject = assunto;
            mailMessage.Body = corpo;
            mailMessage.IsBodyHtml = html;
            smtp.Send(mailMessage);

            return true;
        }

    }

    [DataContract]
    public class CEP
    {
        [DataMember]
        public string uf { get; set; }
        [DataMember]
        public string cidade { get; set; }
        [DataMember]
        public string bairro { get; set; }
        [DataMember]
        public string tipo_logradouro { get; set; }
        [DataMember]
        public string logradouro { get; set; }
        [DataMember]
        public string resultado { get; set; }
        [DataMember]
        public string resultado_txt { get; set; }
    }

    [DataContract]
    public class LabelData
    {
        [DataMember]
        public string label { get; set; } 
        [DataMember]
        public int? data { get; set; }
    }
}
