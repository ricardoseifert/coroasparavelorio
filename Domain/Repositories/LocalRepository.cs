﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;

namespace Domain.Repositories
{
    public class LocalRepository : PersistentRepository<Local>
    {

        public LocalRepository(ObjectContext context) : base(context)
        {
        }

        public Local GetByRotuloUrl(string rotuloUrl)
        {
            return this.GetByExpression(l => l.RotuloUrl == rotuloUrl && l.Disponivel).FirstOrDefault();
        }

        public IEnumerable<Local> GetByTipo(Local.Tipos tipo)
        {
            return this.GetByExpression(l => l.Disponivel && l.TipoID == (int)tipo);
        }
        public IEnumerable<Local> Buscar(string termo)
        {
            return Buscar(termo, null);
        }

        public IEnumerable<Local> Buscar(string termo, int? tipoID)
        {
            if (!tipoID.HasValue)
                return this.GetByExpression(l => (l.Descricao.Contains(termo) || l.Logradouro.Contains(termo) || l.Bairro.Contains(termo) || l.Cidade.Nome.Contains(termo) || l.Estado.Nome.Contains(termo) || l.Titulo.Contains(termo) || l.RotuloUrl.Contains(termo) || l.TagDescription.Contains(termo) || l.TagKeywords.Contains(termo) || l.Titulo.Contains(termo)) && l.Disponivel);
            else
                return this.GetByExpression(l => (l.Descricao.Contains(termo) || l.Logradouro.Contains(termo) || l.Bairro.Contains(termo) || l.Cidade.Nome.Contains(termo) || l.Estado.Nome.Contains(termo) || l.Titulo.Contains(termo) || l.RotuloUrl.Contains(termo) || l.TagDescription.Contains(termo) || l.TagKeywords.Contains(termo) || l.Titulo.Contains(termo)) && l.Disponivel && l.TipoID == tipoID.Value);
        }

        public int TotalLocaisRelacionados(Local local)
        {
            return this.GetByExpression(c => c.ID != local.ID && c.CidadeID == local.CidadeID).Count();
        }
    }
}
