﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities;
using Domain.MetodosExtensao;
using DomainExtensions.Repositories;

namespace Domain.Repositories {
    //=================================================================================
    public class PedidoDashboardRepository {

        #region Variáveis
        //-----------------------------------------------------------------------------
        private readonly PersistentRepository<Pedido> pedidoRepository;
        private readonly PersistentRepository<PedidoEmpresa> pedidoEmpRepository;
        private readonly PersistentRepository<AdministradorCarteira> admCarteiraRepository;
        private readonly ObjectContext context;
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public PedidoDashboardRepository(ObjectContext context) {
            pedidoRepository = new PersistentRepository<Pedido>(context);
            pedidoEmpRepository = new PersistentRepository<PedidoEmpresa>(context);
            admCarteiraRepository = new PersistentRepository<AdministradorCarteira>(context);
            this.context = context;
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Listar Pedidos por Carteira
        //-----------------------------------------------------------------------------
        public List<IPedido> ListPedidosPorCarteira(DateTime? dataInicial, DateTime? dataFinal, IEnumerable<int> lstIdEstado) {

            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();

            if (dataInicial == null && dataFinal == null) {
                lstPedidos = pedidoRepository.GetByExpression(r => (r.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor || r.StatusEntregaID == (int)TodosStatusEntrega.EmProducao || r.StatusEntregaID == (int)TodosStatusEntrega.EntregaEmAndamento) && lstIdEstado.Contains(r.EstadoID)).ToList();
            }
            else {
                lstPedidos = pedidoRepository.GetByExpression(r => (r.DataCriacao >= dataInicial && r.DataCriacao <= dataFinal) && lstIdEstado.Contains(r.EstadoID)).ToList();
            }

            if (dataInicial == null && dataFinal == null) {
                lstPedidosEmp = pedidoEmpRepository.GetByExpression(r => (r.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor || r.StatusEntregaID == (int)TodosStatusEntrega.EmProducao || r.StatusEntregaID == (int)TodosStatusEntrega.EntregaEmAndamento) && lstIdEstado.Contains(r.EstadoID)).ToList();
            }
            else {
                lstPedidosEmp = pedidoEmpRepository.GetByExpression(r => (r.DataCriacao >= dataInicial && r.DataCriacao <= dataFinal) && lstIdEstado.Contains(r.EstadoID)).ToList();
            }

            List<IPedido> lstRetorno = new List<IPedido>();
            lstRetorno.AddRange(lstPedidos);
            lstRetorno.AddRange(lstPedidosEmp);

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public List<IPedido> ListarPedidosCarteira(int? idAdm, int sistemaId ) {
            List<IPedido> lstRetorno = new List<IPedido>();
            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();

            Expression<Func<IPedido, bool>> exp = p =>
                      (p.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor
                      || p.StatusEntregaID == (int)TodosStatusEntrega.EmProducao
                      || p.StatusEntregaID == (int)TodosStatusEntrega.EntregaEmAndamento);

            if (idAdm.HasValue && idAdm.Value > 0) {
                #region filtros
                List<int> lstCidades = new List<int>();
                List<int> lstEstados = new List<int>();
                List<int> lstCidadesOutros = new List<int>();
                List<int> lstEstadosOutros = new List<int>();
                #endregion

                #region Carteiras
                //busca as carteiras 
                var carteiras = admCarteiraRepository.GetByExpression(AdministradorCarteira.CarteiraComVigencia(idAdm));
                //busca as carteiras do usuario
                if (carteiras.Any(a => a.AdministradorID == idAdm.Value)) {
                    List<Carteira> lstCarteiras = carteiras.Where(c => c.AdministradorID == idAdm.Value && c.Carteira != null).Select(c => c.Carteira).ToList();
                    foreach (Carteira carteira in lstCarteiras) {
                        if (carteira.Cidades != null) {
                            lstCidades.AddRange(carteira.Cidades.Select(c => c.ID));
                        }
                        if (carteira.Estadoes != null) {
                            lstEstados.AddRange(carteira.Estadoes.Select(e => e.ID));
                        }
                    }
                }
                //pegar os dados de carteira que não sejam dele
                List<Carteira> lstCarteiraOutros = carteiras.Where(a => a.AdministradorID != idAdm.Value && a.Carteira != null).Select(c => c.Carteira).ToList();
                foreach (var carteira in lstCarteiraOutros) {
                    if (carteira.Cidades != null) {
                        lstCidadesOutros.AddRange(carteira.Cidades.Select(c => c.ID));
                    }
                    if (carteira.Estadoes != null) {
                        lstEstadosOutros.AddRange(carteira.Estadoes.Select(e => e.ID));
                    }
                }
                #endregion

                #region Extensão dos filtros

                Expression<Func<IPedido, bool>> exp2 = p => 
                (lstCidades.Contains(p.CidadeID) 
                || (!lstCidades.Contains(p.CidadeID) && lstEstados.Contains(p.EstadoID) && !lstCidadesOutros.Contains(p.CidadeID))
                || (!lstCidades.Contains(p.CidadeID) && !lstEstados.Contains(p.EstadoID) && !lstCidadesOutros.Contains(p.CidadeID) && !lstEstadosOutros.Contains(p.EstadoID)));

                //ve o pedido caso a cidade do pedido esteja na carteira dele
                //ver o pedido caso ele possui o estado do pedido e ninguem possui a cidade
                //ver o pedido caso ninguem possua a cidade e ninguem possui o estado
                exp = exp.And(exp2);

                #endregion
            }


            Expression converted = Expression.Convert(exp.Body, typeof(bool));
            Expression<Func<Pedido, bool>> expPedido = Expression.Lambda<Func<Pedido, bool>>(converted, exp.Parameters);
            converted = Expression.Convert(exp.Body, typeof(bool));
            Expression<Func<PedidoEmpresa, bool>> expPedidoEmpresa = Expression.Lambda<Func<PedidoEmpresa, bool>>(converted, exp.Parameters);

            switch (sistemaId)
            {
                case (int)SistemaPedido.Cliente:
                    lstPedidos = context.CreateObjectSet<Pedido>().Include("Cidade").Include("Estado").Where(expPedido).ToList();
                    lstRetorno.AddRange(lstPedidos);
                    break;

                case (int)SistemaPedido.Empresa:
                    lstPedidosEmp = context.CreateObjectSet<PedidoEmpresa>().Include("Cidade").Include("Estado").Where(expPedidoEmpresa).ToList();
                    lstRetorno.AddRange(lstPedidosEmp);
                    break;

                default:
                    lstPedidos = context.CreateObjectSet<Pedido>().Include("Cidade").Include("Estado").Where(expPedido).ToList();
                    lstPedidosEmp = context.CreateObjectSet<PedidoEmpresa>().Include("Cidade").Include("Estado").Where(expPedidoEmpresa).ToList();
                    lstRetorno.AddRange(lstPedidos);
                    lstRetorno.AddRange(lstPedidosEmp);
                    break;
            }

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public List<IPedido> ListarPedidosProcessados(int? idAdm, int sistemaId, int CategoriaDoProduto)
        {
            List<IPedido> lstRetorno = new List<IPedido>();
            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();

            Expression<Func<IPedido, bool>> exp = p =>
                      (p.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor || p.StatusEntregaID == (int)TodosStatusEntrega.EmProducao || p.StatusEntregaID == (int)TodosStatusEntrega.EntregaEmAndamento || p.StatusEntregaID == (int)TodosStatusEntrega.PendenteAtendimento);


            Expression converted = Expression.Convert(exp.Body, typeof(bool));
            converted = Expression.Convert(exp.Body, typeof(bool));

            Expression<Func<Pedido, bool>> expPedido = Expression.Lambda<Func<Pedido, bool>>(converted, exp.Parameters);
            Expression<Func<PedidoEmpresa, bool>> expPedidoEmpresa = Expression.Lambda<Func<PedidoEmpresa, bool>>(converted, exp.Parameters);

            switch (sistemaId)
            {
                case (int)SistemaPedido.Cliente:
                    lstPedidos = pedidoRepository.GetByExpression(expPedido).ToList();
                    lstRetorno.AddRange(lstPedidos);
                    break;

                case (int)SistemaPedido.Empresa:
                    lstPedidosEmp = pedidoEmpRepository.GetByExpression(expPedidoEmpresa).ToList();
                    lstRetorno.AddRange(lstPedidosEmp);
                    break;

                default:
                    lstPedidos = pedidoRepository.GetByExpression(expPedido).ToList();
                    lstPedidosEmp = pedidoEmpRepository.GetByExpression(expPedidoEmpresa).ToList();
                    lstRetorno.AddRange(lstPedidos);
                    lstRetorno.AddRange(lstPedidosEmp);
                    break;
            }

            if (idAdm.HasValue && idAdm.Value > 0)
            {

                #region Extensão dos filtros

                lstRetorno = lstRetorno.Where(r => r.AdministradorPedidoID == idAdm.Value).ToList();

                #endregion
            }

            if(CategoriaDoProduto == 1)
            {
                lstRetorno = lstRetorno.Where(r => r.CategoriaDosProdutos.Where(p => p == 1).Any()).ToList();
            }

            if (CategoriaDoProduto == 8)
            {
                lstRetorno = lstRetorno.Where(r => r.ProdutoKitBB == true).ToList();
            }

            if (CategoriaDoProduto == 100)
            {
                lstRetorno = lstRetorno.Where(r => r.CategoriaDosProdutos.Where(p => p == 100).Any()).ToList();
            }

            return lstRetorno;
        }

        #endregion
    }
    //=================================================================================
}
