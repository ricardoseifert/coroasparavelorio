﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using Domain.Repositories.Abstract;

namespace Domain.Repositories
{
    public class LogMandrillRepository : PersistentRepository<LogMandrill>
    {
        public LogMandrillRepository(ObjectContext context)
            : base(context)
        {
        }

        public void SalvaLog(string IdEmail, string TipoEmail, int? PedidoID, int? PedidoEmpresaID, string reject_reason = null)
        {

            var log = new LogMandrill();
            log.DataCadastro = DateTime.Now;

            if (PedidoID > 0)
                log.PedidoID = (int)PedidoID;

            if (PedidoEmpresaID > 0)
                log.PedidoEmpresaID = (int)PedidoEmpresaID;

            if(!string.IsNullOrEmpty(reject_reason))
                log.RejectReason = reject_reason;

            log.TipoEmailStr = TipoEmail.ToString();
            log.IdEmail = IdEmail;

            log.Aberto = false;
            log.Clicado = false;

            this.Save(log);
        }


    }
}
