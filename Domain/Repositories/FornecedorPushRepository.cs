﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using Domain.Repositories.Abstract;

namespace Domain.Repositories
{
    public class FornecedorPushRepository : PersistentRepository<FornecedorPush>, IFornecedorPushRepository
    {
        public FornecedorPushRepository(ObjectContext context)
            : base(context)
        {
        }

        public FornecedorPush RegistraPush(int administradorID, int? pedidoID, int? pedidoEmpresaID, int? pedidoFloriculturaID, int? FornecedorID)
        {
            var push = new FornecedorPush();
            try
            {
                push.AdministradorID = administradorID;
                push.PedidoID = pedidoID;
                push.PedidoEmpresaID = pedidoEmpresaID;
                push.IdFornecedor = FornecedorID;
                push.DataCriacao = DateTime.Now;

                this.Save(push);
            }
            catch (Exception)
            {
                throw;
            }

            return push;
        }
    }
}
