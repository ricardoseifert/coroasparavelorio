﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using Domain.Repositories.Abstract;

namespace Domain.Repositories
{
    public class AdministradorPedidoRepository : PersistentRepository<AdministradorPedido>, IAdministradorPedidoRepository
    {
        public AdministradorPedidoRepository(ObjectContext context)
            : base(context)
        {
        }

        public void Registra(int administradorID, int? pedidoID, int? pedidoEmpresaID, int? PedidoFloriculturaID, int? faturamentoID, AdministradorPedido.Acao acao)
        {
            var admPedido = new AdministradorPedido();
            admPedido.AdministradorID = administradorID;
            admPedido.PedidoID = pedidoID;
            admPedido.PedidoEmpresaID = pedidoEmpresaID;
            admPedido.FaturamentoID = faturamentoID;
            admPedido.AcaoID = (int)acao;
            admPedido.DataCriacao = DateTime.Now;
            this.Save(admPedido);
        }

        public void RegistraLogFornecedor(int administradorID, int? pedidoID, int? pedidoEmpresaID, int? PedidoFloriculturaID , int? faturamentoID, AdministradorPedido.Acao acao, int? FornecedorID)
        {
            var admPedido = new AdministradorPedido();
            if (administradorID != FornecedorID)
                admPedido.AdministradorID = administradorID;

            admPedido.IdFornecedor = FornecedorID;
            admPedido.PedidoID = pedidoID;
            admPedido.PedidoEmpresaID = pedidoEmpresaID;
            admPedido.PedidoFloriculturaID = PedidoFloriculturaID;
            admPedido.FaturamentoID = faturamentoID;
            admPedido.AcaoID = (int)acao;
            admPedido.DataCriacao = DateTime.Now;
            this.Save(admPedido);
        }

        public List<RelatorioHistoricoAdministrador_Result> GetHistoricoAdministrador(int administradorID, DateTime datainicio, DateTime datafim)
        {
            var historico = ((COROASEntities)this.objectSet.Context).RelatorioHistoricoAdministrador(administradorID, datainicio, datafim).ToList();
            return historico;
        }


    }
}
