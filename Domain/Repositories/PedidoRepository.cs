﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;

namespace Domain.Repositories
{
    public class PedidoRepository : PersistentRepository<Pedido>
    {

        public PedidoRepository(ObjectContext context) : base(context)
        {
        }

        public Pedido GetByCodigo(Guid codigo)
        {
            var pedido = this.GetByExpression(p => p.Codigo.CompareTo(codigo) == 0).FirstOrDefault();
            return pedido;
        }

        public bool NumeroExiste(string numero)
        {
            return this.GetByExpression(p => p.Numero == numero).Any();
        }

    }
}
