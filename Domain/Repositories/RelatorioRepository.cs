﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Data;
using Domain.Entities.ReportsStoredProcs;
using System.Configuration;
using Dapper;

namespace Domain.Repositories
{
    public class RelatorioRepository : PersistentRepository<Cliente>
    {

        public RelatorioRepository(ObjectContext context) : base(context)
        {
        }

        public List<RelatorioCidades_Result> GetRelatorioCidades(bool exibetodos, DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioCidades(exibetodos, datainicio, datafim).ToList();
            return result;
        }

        public List<RelatorioComoConheceu_Result> GetRelatorioComoConheceu(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioComoConheceu(datainicio, datafim).ToList();
            return result;
        }

        public List<RelatorioMeioPagamento_Result> GetRelatorioMeioPagamento(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioMeioPagamento(datainicio, datafim).ToList();
            return result;
        }

        public List<RelatorioProdutos_Result> GetRelatorioProdutos(bool exibetodos, DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioProdutos(exibetodos, datainicio, datafim).ToList();
            return result;
        }

        public List<vwRelatorioFornecedoresOnlineOffline> GetRelatorioFornecedoresOnlineOffline()
        {
            var result = ((COROASEntities)this.objectSet.Context).vwRelatorioFornecedoresOnlineOfflines.ToList();
            return result;
        }

        public List<RelatorioTipoPessoa> GetRelatorioTipoPessoa(DateTime datainicio, DateTime datafim){
            List<RelatorioTipoPessoa> LstRetorno = new List<RelatorioTipoPessoa>();

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["COROAS"].ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("Sistema.spRelatorioTipoPessoa", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@datainicio", datainicio));
                cmd.Parameters.Add(new SqlParameter("@datafim", datafim));


                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        RelatorioTipoPessoa novoRelatorioAtendente = new RelatorioTipoPessoa();
                        novoRelatorioAtendente.TipoID = int.Parse(rdr["TipoId"].ToString());
                        novoRelatorioAtendente.Total = int.Parse(rdr["Total"].ToString());
                        
                        LstRetorno.Add(novoRelatorioAtendente);
                    }
                }
            }

            return LstRetorno;
        }

        public int? spRetornaProxAlocado()
        {
            int? result;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("Sistema.spRetornaProxAlocado", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                result = (int?)cmd.ExecuteScalar();

            }

            return result;
        }

        //public List<spRelatorioDashFloriculturas_Result> GetRelatoriospRelatorioDashFloriculturas(DateTime datainicio, DateTime datafim)
        //{
        //    var result = ((COROASEntities)this.objectSet.Context).spRelatorioDashFloriculturas(datainicio, datafim).ToList();
        //    return result;
        //}

        public List<spRelatorioDashFinanceiroVencidos_Result> GetRelatoriospRelatorioDashFinanceiroVencidos()
        {
            var result = ((COROASEntities)this.objectSet.Context).spRelatorioDashFinanceiroVencidos().ToList();
            return result;
        }

        public List<spRelatorioDashFinanceiro_Result> GetRelatoriospRelatorioDashFinanceiro()
        {
            var result = ((COROASEntities)this.objectSet.Context).spRelatorioDashFinanceiro().ToList();
            return result;
        }

        public int? GetRelatoriospCalculaNPS(int FornecedirID, DateTime datainicio, DateTime datafim)
        {
            List<RelatorioAtendente> LstRetorno = new List<RelatorioAtendente>();
            int? Retorno = null;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["COROAS"].ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("Sistema.spCalculaNps", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@datainicio", datainicio));
                cmd.Parameters.Add(new SqlParameter("@datafim", datafim));
                cmd.Parameters.Add(new SqlParameter("@FornecedorID", FornecedirID));

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        if(!string.IsNullOrEmpty(rdr["NotaNPS"].ToString()))
                            Retorno = int.Parse(rdr["NotaNPS"].ToString());
                    }
                }

                conn.Close();
            }
            return Retorno;
        }

        public List<RelatorioVendaSite_Result> GetRelatorioVendaSite(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioVendaSite(datainicio, datafim).ToList();
            return result;
        }

        public List<RelatorioValorVendaOrigem_Result> GetRelatorioValorVendasOrigem(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioValorVendaOrigem(datainicio, datafim).ToList();
            return result;
        }

        public List<spRelatorioNFEmitirCPV_Result> GetRelatorioNFEmitirCPV()
        {
            var result = ((COROASEntities)this.objectSet.Context).spRelatorioNFEmitirCPV().ToList();
            return result;
        }

        public List<RelatorioVendaOrigem_Result> GetRelatorioVendaOrigem(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioVendaOrigem(datainicio, datafim).ToList();
            return result;
        }

        public List<RelatorioVendaCidade_Result> GetRelatorioVendaCidade(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioVendaCidade(datainicio, datafim).ToList();
            return result;
        }

        public int GetRelatorioLacosTotalVendas(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosTotalVendas(datainicio, datafim.AddDays(1).AddMilliseconds(-1)).FirstOrDefault().Total;
            return result.Value;
        }
        public decimal GetRelatorioLacosTotalFaturamento(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosTotalFaturamento(datainicio, datafim.AddDays(1).AddMilliseconds(-1)).FirstOrDefault().Total;
            return result.Value;
        }
        public List<RelatorioLacosTopEmpresas_Result> GetRelatorioLacosTopEmpresas(bool exibetodos, DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosTopEmpresas(exibetodos, datainicio, datafim.AddDays(1).AddMilliseconds(-1)).ToList();
            return result;
        }
        public List<RelatorioLacosProdutos_Result> GetRelatorioLacosTopProdutos(bool exibetodos, DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosProdutos(exibetodos, datainicio, datafim.AddDays(1).AddMilliseconds(-1)).ToList();
            return result;
        }
        public List<RelatorioLacosVendaCidade_Result> GetRelatorioLacosVendaCidade(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosVendaCidade(datainicio, datafim.AddDays(1).AddMilliseconds(-1)).ToList();
            return result;
        }

        public List<spPedidosAutomaticosFornecedor_Result> GetPedidosAutomaticosFornecedor(int? FornecedorID)
        {
            List<spPedidosAutomaticosFornecedor_Result> lstResult = new List<spPedidosAutomaticosFornecedor_Result>();
          
            using (IDbConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CoroasDapper"].ConnectionString))
            {
                con.Open();
                string sql = "exec Sistema.spPedidosAutomaticosFornecedor  @FornecedorID = @paramFornecedorID";
                lstResult = con.Query<spPedidosAutomaticosFornecedor_Result>(sql, new { paramFornecedorID = FornecedorID}).ToList();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            return lstResult;
        }

        public List<RelatorioLacosVendaOrigem_Result> GetRelatorioLacosVendaOrigem(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosVendaOrigem(datainicio, datafim.AddDays(1).AddMilliseconds(-1)).ToList();
            return result;
        }
        public List<RelatorioVendaHorario_Result> GetRelatorioVendaHorario(DateTime datainicio, DateTime datafim)
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioVendaHorario(datainicio, datafim).ToList();
            return result;
        }
        public List<RelatorioAtendente> GetRelatorioVendaAtendente(DateTime datainicio, DateTime datafim)
        {
            List<RelatorioAtendente> LstRetorno = new List<RelatorioAtendente>();

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["COROAS"].ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("Sistema.spRelatorioAtendente", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@datainicio", datainicio));
                cmd.Parameters.Add(new SqlParameter("@datafim", datafim));

                
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        RelatorioAtendente novoRelatorioAtendente = new RelatorioAtendente();

                        novoRelatorioAtendente.Nome = rdr["Nome"].ToString();
                        novoRelatorioAtendente.QtdeCancelou = int.Parse(rdr["QtdeCancelou"].ToString());
                        novoRelatorioAtendente.SomaCancelou = (!string.IsNullOrEmpty(rdr["SomaCancelou"].ToString())) ? decimal.Parse(rdr["SomaCancelou"].ToString()) : 0;
                        novoRelatorioAtendente.QtdeConcluiu = int.Parse(rdr["QtdeConcluiu"].ToString());
                        novoRelatorioAtendente.QtdeConcluiuOff = int.Parse(rdr["QtdeConcluiuOff"].ToString());
                        novoRelatorioAtendente.QtdeConcluiuOn = int.Parse(rdr["QtdeConcluiuOn"].ToString());
                        novoRelatorioAtendente.SomaConcluiu = (!string.IsNullOrEmpty(rdr["SomaConcluiu"].ToString())) ? decimal.Parse(rdr["SomaConcluiu"].ToString()) : 0;
                        novoRelatorioAtendente.SomaConcluiuOn = (!string.IsNullOrEmpty(rdr["SomaConcluiuOn"].ToString())) ? decimal.Parse(rdr["SomaConcluiuOn"].ToString()) : 0;
                        novoRelatorioAtendente.SomaConcluiu = (!string.IsNullOrEmpty(rdr["SomaConcluiu"].ToString())) ? decimal.Parse(rdr["SomaConcluiu"].ToString()) : 0;

                        LstRetorno.Add(novoRelatorioAtendente);

                    }
                }

                conn.Close();
            }

           

            return LstRetorno;
        }
        public List<RelatorioLacosEmpresas_Result> GetRelatorioLacosEmpresas()
        {
            var result = ((COROASEntities)this.objectSet.Context).RelatorioLacosEmpresas().ToList();
            return result;
        }
    }
}
