﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;

namespace Domain.Repositories
{
    public class CupomRepository : PersistentRepository<Cupom>
    {

        public CupomRepository(ObjectContext context) : base(context)
        {
        }

        public Cupom GetByCodigo(string codigo)
        {
            return this.GetByExpression(c => c.Ativo && c.Codigo.ToUpper().Trim() == codigo.ToUpper().Trim() && c.DataValidade >= DateTime.Now && c.QuantidadeDisponivel >= c.QuantidadeUtilizado).FirstOrDefault();
        }

    }
}
