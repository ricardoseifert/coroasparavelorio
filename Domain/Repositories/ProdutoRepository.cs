﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;

namespace Domain.Repositories
{
    public class ProdutoRepository : PersistentRepository<Produto>
    {

        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;

        public ProdutoRepository(ObjectContext context) : base(context)
        {
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
        }

        public IEnumerable<Produto> GetByTipo(Produto.Tipos tipo)
        {
            var produtos = this.GetByExpression(p => p.Disponivel && p.TipoID == (int)tipo).OrderBy(c => c.Ordem);
            return produtos;
        }

        public IEnumerable<Produto> Sugestoes(int? faixaPrecoID, int? relacionamentoID, int? corID, Entities.Produto.Sexos sexo)
        {
            var sugestoes = this.GetByExpression(p => p.Disponivel && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor);
            if (faixaPrecoID.HasValue)
            {
                var faixaPreco = faixaPrecoRepository.Get(faixaPrecoID.Value);
                if (faixaPreco != null)
                {
                    sugestoes = sugestoes.Where(p => p.ProdutoTamanhoes.Any(t => t.Preco >= faixaPreco.PrecoInicio && t.Preco <= faixaPreco.PrecoFim));
                }
            }
            if (relacionamentoID.HasValue)
            {
                sugestoes = sugestoes.Where(p => p.ProdutoRelacionamentoes.Any(r => r.RelacionamentoID == relacionamentoID));
            }
            if (corID.HasValue)
            {
                sugestoes = sugestoes.Where(p => p.ProdutoColoracaos.Any(f => f.ColoracaoID == corID));
            }
            return sugestoes.OrderBy(c => c.Ordem);
        }

        public IEnumerable<Produto> SugestoesLacos(int? faixaPrecoID, int? relacionamentoID, int? corID, Entities.Produto.Sexos sexo)
        {
            var sugestoes = this.GetByExpression(p => p.DisponivelLacosFlores && p.ProdutoTamanhoes.Count > 0 && p.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor);
            if (faixaPrecoID.HasValue)
            {
                var faixaPreco = faixaPrecoRepository.Get(faixaPrecoID.Value);
                if (faixaPreco != null)
                {
                    sugestoes = sugestoes.Where(p => p.ProdutoTamanhoes.Any(t => t.Preco >= faixaPreco.PrecoInicio && t.Preco <= faixaPreco.PrecoFim));
                }
            }
            if (relacionamentoID.HasValue)
            {
                sugestoes = sugestoes.Where(p => p.ProdutoRelacionamentoes.Any(r => r.RelacionamentoID == relacionamentoID));
            }
            if (corID.HasValue)
            {
                sugestoes = sugestoes.Where(p => p.ProdutoColoracaos.Any(f => f.ColoracaoID == corID));
            }
            return sugestoes.OrderBy(c => c.Ordem);
        }

        public IEnumerable<Produto> DestaquesHomeLacosFlores(int? total = null)
        {
            if (!total.HasValue)
                return this.GetByExpression(p => p.DestaqueHome && p.DisponivelLacosFlores && p.ProdutoTamanhoes.Count > 0 && p.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).ToList();
            else
                return this.GetByExpression(p => p.DestaqueHome && p.DisponivelLacosFlores && p.ProdutoTamanhoes.Count > 0 && p.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).Take(total.Value).ToList();
        }

        public IEnumerable<Produto> DestaquesHome(int? total = null)
        {
            if(!total.HasValue)
                return this.GetByExpression(p => p.DestaqueHome && p.Disponivel && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).ToList();
            else
                return this.GetByExpression(p => p.DestaqueHome && p.Disponivel && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).Take(total.Value).ToList();
        }

        public IEnumerable<Produto> PublicadosLacosFlores(int? total = null)
        {
            if (!total.HasValue)
                return this.GetByExpression(p => p.DisponivelLacosFlores && p.ProdutoTamanhoes.Count > 0 && p.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).ToList();
            else
                return this.GetByExpression(p => p.DisponivelLacosFlores && p.ProdutoTamanhoes.Count > 0 && p.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).Take(total.Value).ToList();
        }

        public IEnumerable<Produto> Publicados(int? total = null)
        {
            if (!total.HasValue)
                return this.GetByExpression(p => p.Disponivel && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).ToList();
            else
                return this.GetByExpression(p => p.Disponivel && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).OrderBy(c => c.Ordem).Take(total.Value).ToList();
        }

        public Produto Detalhes(int id)
        {
            return this.GetByExpression(p => p.Disponivel && p.ID == id && p.ProdutoTamanhoes.Count > 0 && p.TipoID == (int)Domain.Entities.Produto.Tipos.CoroaFlor).FirstOrDefault();
        }

        public Produto DetalhesArranjos(int id)
        {
            return this.GetByExpression(p => p.ID == id && p.TipoID == (int)Domain.Entities.Produto.Tipos.ArranjosCondo).FirstOrDefault();
        }

        public Produto DetalhesLacos(int id)
        {
            return this.GetByExpression(p => p.DisponivelLacosFlores && p.ID == id && p.ProdutoTamanhoes.Count > 0).FirstOrDefault();
        }

    }
}
