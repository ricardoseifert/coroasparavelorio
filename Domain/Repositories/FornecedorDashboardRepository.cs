﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities;
using Domain.MetodosExtensao;
using DomainExtensions.Repositories;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using Domain.Factories;

namespace Domain.Repositories {
    //=================================================================================
    public class FornecedorDashboardRepository
    {
        #region Variáveis
        //-----------------------------------------------------------------------------
        private readonly PersistentRepository<Pedido> pedidoRepository;
        private readonly PersistentRepository<PedidoEmpresa> pedidoEmpRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Local> localRepository;
        private readonly PersistentRepository<AdministradorCarteira> admCarteiraRepository;
        private readonly PersistentRepository<FornecedorPush> fornecedorPushRepository;
        private readonly ObjectContext context;
        //-----------------------------------------------------------------------------
        #endregion

        #region Construtor
        //-----------------------------------------------------------------------------
        public FornecedorDashboardRepository(ObjectContext context) {
            pedidoRepository = new PersistentRepository<Pedido>(context);            
            pedidoEmpRepository = new PersistentRepository<PedidoEmpresa>(context);
            localRepository = new PersistentRepository<Local>(context);
            admCarteiraRepository = new PersistentRepository<AdministradorCarteira>(context);
            fornecedorPushRepository = new PersistentRepository<FornecedorPush>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            this.context = context;
        }
        //-----------------------------------------------------------------------------
        #endregion

        #region Listar Pedidos        
        //-----------------------------------------------------------------------------
        public List<IPedido> ListPedidosPorFornecedorEmPush(int IdFornecedor)
        {
            List<FornecedorPush> lstPedidosPush = new List<FornecedorPush>();
            List<Pedido> lstPedidosCPV = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();
            List<int> IdsToRemove = new List<int>();
            List<int> IdsToRemoveEMP = new List<int>();
            List<IPedido> lstRetorno = new List<IPedido>();

            var lstPush = fornecedorPushRepository.GetByExpression(c => c.IdFornecedor == IdFornecedor).OrderByDescending(c => c.DataCriacao).ToList();

            lstPedidosPush = lstPush.GroupBy(p => p.PedidoID).Select(g => g.First()).ToList();

            #region PEDIDOS CPV
            //----------------------------------------------------------------------
            foreach (var ped in lstPedidosPush)
            {
                //var lst = pedidoRepository.GetByExpression(a => a.ID == (int)ped.PedidoID).ToList();

                var lst = pedidoRepository.GetByExpression
                (
                    r => r.ID == (int)ped.PedidoID
                    && r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                    && r.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor
                    && (r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Criado || r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando)
                ).ToList();

                lstPedidosCPV.AddRange(lst);
            }

            if (lstPedidosCPV.Count > 0)
            {
                foreach (var PedidoCPV in lstPedidosCPV)
                {
                    PedidoCPV.RepasseId = PedidoCPV.PedidoItems.FirstOrDefault().RepasseAtual.ID;
                }

                // REMOVES OS PEDIDOS DO CPV QUE JÁ PASSARAM DO PRAZO
                foreach (var Iten in IdsToRemove)
                {
                    lstPedidosCPV.Remove(lstPedidosCPV.Where(r => r.ID == Iten).FirstOrDefault());
                }
            }
            //----------------------------------------------------------------------
            #endregion

            #region PEDIDOS EMPRESA
            //----------------------------------------------------------------------
            foreach (var ped in lstPedidosPush)
            {
                //var lst = pedidoEmpRepository.GetByExpression(a => a.ID == (int)ped.PedidoID).ToList();

                var lst = pedidoEmpRepository.GetByExpression
                (
                    r => r.ID == (int)ped.PedidoID
                    && r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                    && !r.FornecedorEmProcessamento
                ).ToList();

                lstPedidosEmp.AddRange(lst);
            }

            foreach (var PedidoEmp in lstPedidosEmp)
            {
                PedidoEmp.RepasseId = PedidoEmp.RepasseAtual.ID;
            }

            // REMOVES OS PEDIDOS DO CPV QUE JÁ PASSARAM DO PRAZO
            foreach (var Iten in IdsToRemoveEMP)
            {
                lstPedidosEmp.Remove(lstPedidosEmp.Where(r => r.ID == Iten).FirstOrDefault());
            }
            //----------------------------------------------------------------------
            #endregion

            lstRetorno.AddRange(lstPedidosCPV);
            lstRetorno.AddRange(lstPedidosEmp);

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public List<IPedido> ListPedidosPorFornecedor(int IdFornecedor)
        {
            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();
            List<int> IdsToRemove = new List<int>();
            List<int> IdsToRemoveEMP = new List<int>();
            List<IPedido> lstRetorno = new List<IPedido>();

            #region PEDIDOS CPV
            //----------------------------------------------------------------------
            lstPedidos = pedidoRepository.GetByExpression
                (
                    r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                    && r.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor
                    && (r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Criado || r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando)
                    && r.LocalID != null
                    && r.FornecedorID == null
                    && r.Local.FornecedorLocals.Where(p => p.FornecedorID == IdFornecedor).Any()
                ).ToList();

            if (lstPedidos.Count > 0)
            {
                foreach (var Pedido in lstPedidos)
                {
                    var FornecedorRecusouaEntrega = new PedidoService(context).FornecedoresRecusaramEntrega(Pedido.ID, OrigemPedido.Cliente).Where(r => r.Key == IdFornecedor).First().Value;

                    Pedido.RepasseId = Pedido.PedidoItems.FirstOrDefault().RepasseAtual.ID;

                }

                // REMOVES OS PEDIDOS DO CPV QUE JÁ PASSARAM DO PRAZO
                foreach (var Iten in IdsToRemove)
                {
                    var REsult = lstPedidos.Where(r => r.ID == Iten).FirstOrDefault();
                    lstPedidos.Remove(REsult);
                }
            }
            //----------------------------------------------------------------------
            #endregion

            #region PEDIDOS EMPRESA
            //----------------------------------------------------------------------
            lstPedidosEmp = pedidoEmpRepository.GetByExpression
               (
                   r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                   && r.StatusEntregaID == (int)TodosStatusEntrega.PendenteFornecedor
                   && r.LocalID != null
                   && r.FornecedorID == null
                   && r.Local.FornecedorLocals
               .Where(
                   p => p.FornecedorID == IdFornecedor).Any()
               ).ToList();

            foreach (var Pedido in lstPedidosEmp)
            {
                var FornecedorRecusouaEntrega = new PedidoService(context).FornecedoresRecusaramEntrega(Pedido.ID, OrigemPedido.Empresa).Where(r => r.Key == IdFornecedor).First().Value;
                Pedido.RepasseId = Pedido.RepasseAtual.ID;
            }

            // REMOVES OS PEDIDOS DO CPV QUE JÁ PASSARAM DO PRAZO
            foreach (var Iten in IdsToRemoveEMP)
            {
                var REsult = lstPedidosEmp.Where(r => r.ID == Iten).FirstOrDefault();
                lstPedidosEmp.Remove(REsult);
            }
            //----------------------------------------------------------------------
            #endregion

            lstRetorno.AddRange(lstPedidos);
            lstRetorno.AddRange(lstPedidosEmp);

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public List<IPedido> ListPedidosPorFornecedorEmProducao(int IdFornecedor)
        {
            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();

            #region PEDIDOS CPV
            //----------------------------------------------------------------------
            lstPedidos = pedidoRepository.GetByExpression
                (r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                    && r.StatusEntregaID == (int)TodosStatusEntrega.EmProducao
                    // && r.LocalID != null
                    && r.FornecedorID == IdFornecedor
                    && r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando
                    && r.AdministradorPedidoes.Where(p => p.AcaoID != (int)AdministradorPedido.Acao.SaiuParaEntrega).Any()
                    //&& r.AdministradorPedidoes.Where(p => p.AcaoID != (int)AdministradorPedido.Acao.AlterouFloricultura).Any()
                )
                .OrderByDescending(r => r.DataSolicitada)
                .ToList();




            //lstPedidos = pedidoRepository.GetByExpression
            //     (r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
            //     && r.StatusEntregaID != (int)TodosStatusEntrega.Entregue
            //     && r.FornecedorID == IdFornecedor
            //     && r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando
            //     && !r.AdministradorPedido
            //     .Where(p => p.AcaoID == (int)AdministradorPedido.Acao.SaiuParaEntrega).Any())
            //     .ToList();

            // lstPedidos = pedidoRepository.GetByExpression
            //   (r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
            //   && r.StatusEntregaID != (int)TodosStatusEntrega.Entregue
            //   && r.FornecedorID == IdFornecedor).ToList();

            foreach (var Item in lstPedidos)
            {
                Item.RepasseId = Item.PedidoItems.FirstOrDefault().RepasseAtual.ID;
            }
            //----------------------------------------------------------------------
            #endregion

            #region PEDIDOS EMPRESA
            //----------------------------------------------------------------------
            lstPedidosEmp = pedidoEmpRepository.GetByExpression(r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado && r.StatusEntregaID != (int)TodosStatusEntrega.Entregue && r.LocalID != null && r.FornecedorID == IdFornecedor && r.StatusID == (int)PedidoEmpresa.TodosStatus.PedidoEmAnalise && !r.AdministradorPedidoes.Where(p => p.AcaoID == (int)AdministradorPedido.Acao.SaiuParaEntrega).Any() && !r.AdministradorPedidoes.Where(p => p.AcaoID == (int)AdministradorPedido.Acao.AlterouFloricultura).Any()).ToList();
            foreach (var Item in lstPedidosEmp)
            {
                Item.RepasseId = Item.RepasseAtual.ID;
            }
            //----------------------------------------------------------------------
            #endregion

            List<IPedido> lstRetorno = new List<IPedido>();
            lstRetorno.AddRange(lstPedidos);
            lstRetorno.AddRange(lstPedidosEmp);

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public List<IPedido> ListPedidosPorFornecedorEmEntrega(int IdFornecedor)
        {
            List<Pedido> lstPedidos = new List<Pedido>();
            List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();

            #region PEDIDOS CPV
            //----------------------------------------------------------------------
            lstPedidos = pedidoRepository.GetByExpression
                (
                    r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
                    && r.LocalID != null
                    && r.FornecedorID == IdFornecedor
                    && r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando
                    && r.AdministradorPedidoes.Where(p => p.AcaoID == (int)AdministradorPedido.Acao.SaiuParaEntrega).Any()
                    && r.DataEntrega == null
                ).ToList();

            //lstPedidos = pedidoRepository.GetByExpression
            //   (
            //       r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado
            //       && r.LocalID != null
            //       && r.FornecedorID == IdFornecedor
            //       && r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando
            //       && r.DataEntrega == null
            //   ).ToList();

            //lstPedidos = pedidoRepository.GetByExpression(r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado && r.LocalID != null && r.FornecedorID == IdFornecedor && r.StatusProcessamentoID == (int)Pedido.TodosStatusProcessamento.Processando && r.AdministradorPedidoes.Where(p => p.AcaoID == (int)AdministradorPedido.Acao.SaiuParaEntrega).Any() && r.DataEntrega == null).ToList();
            foreach (var Item in lstPedidos)
            {
                Item.RepasseId = Item.PedidoItems.FirstOrDefault().RepasseAtual.ID;
            }
            //----------------------------------------------------------------------
            #endregion

            #region PEDIDOS EMPRESA
            //----------------------------------------------------------------------
            lstPedidosEmp = pedidoEmpRepository.GetByExpression(r => r.StatusEntregaID != (int)TodosStatusEntrega.Cancelado && r.LocalID != null && r.FornecedorID == IdFornecedor && r.StatusID == (int)PedidoEmpresa.TodosStatus.PedidoEmAnalise && r.AdministradorPedidoes.Where(p => p.AcaoID == (int)AdministradorPedido.Acao.SaiuParaEntrega).Any() && r.DataEntrega == null).ToList();
            foreach (var Item in lstPedidosEmp)
            {
                Item.RepasseId = Item.RepasseAtual.ID;
            }
            //----------------------------------------------------------------------
            #endregion

            List<IPedido> lstRetorno = new List<IPedido>();
            lstRetorno.AddRange(lstPedidos);
            lstRetorno.AddRange(lstPedidosEmp);

            return lstRetorno;
        }
        //-----------------------------------------------------------------------------
        public bool SairEntrega(string Pedido, int FornecedorID)
        {
            int PedidoID = int.Parse(Pedido.Split(',')[0]);
            string Tipo = Pedido.Split(',')[1];

            if (Tipo == "CPV")
            {
                Pedido pedCliente = pedidoRepository.Get(PedidoID);

                pedCliente.FornecedorEmProcessamento = true;
                pedCliente.DataProcessamentoFornecedor = DateTime.Now;
                pedidoRepository.Save(pedCliente);

                //Registra log do usuário
                administradorPedidoRepository.RegistraLogFornecedor(FornecedorID, PedidoID, null, null, null, AdministradorPedido.Acao.SaiuParaEntrega, FornecedorID);

                #region DIRPARAR SMS 

                try
                {
                    string Numero = "";

                    if (!string.IsNullOrEmpty(pedCliente.Cliente.CelularContato))
                    {
                        Numero = pedCliente.Cliente.CelularContato;
                    }
                    else
                    {
                        Numero = pedCliente.Cliente.TelefoneContato;
                    }

                    Numero = Numero.Replace(" ", "").Replace("(", "").Replace(")", "");

                    if (Numero.Length == 11)
                    {
                        string Mensagem = Domain.Core.Configuracoes.SMS_2_FORNECEDORES;

                        if (pedCliente.LocalID != null)
                        {
                            var IdLocal = (int)pedCliente.LocalID;
                            var LocalEntrega = localRepository.Get(IdLocal);

                            Mensagem = Mensagem.Replace("*|LOCAL|*", Domain.Core.Funcoes.AcertaAcentos(LocalEntrega.Titulo.ToUpper()));
                        }
                        else
                        {
                            Mensagem = Mensagem.Replace("*|LOCAL|*", "local");
                        }

                        new MovileFactory().EnviarSms("55" + Numero, Mensagem);
                    }
                }
                catch (Exception)
                {
                }

                #endregion

            }
            else
            {
                PedidoEmpresa pedEmpresa = pedidoEmpRepository.Get(PedidoID);

                pedEmpresa.FornecedorEmProcessamento = true;
                pedEmpresa.DataProcessamentoFornecedor = DateTime.Now;
                pedidoEmpRepository.Save(pedEmpresa);

                //Registra log do usuário
                administradorPedidoRepository.RegistraLogFornecedor(FornecedorID, null, PedidoID, null, null, AdministradorPedido.Acao.SaiuParaEntrega, FornecedorID);

                #region DIRPARAR SMS 

                try
                {
                    string Numero = "";

                    if (!string.IsNullOrEmpty(pedEmpresa.Colaborador.TelefoneCelular))
                    {
                        Numero = pedEmpresa.Colaborador.TelefoneCelular;
                    }
                    else
                    {
                        Numero = pedEmpresa.Colaborador.Telefone;
                    }

                    Numero = Numero.Replace(" ", "").Replace("(", "").Replace(")", "");

                    if (Numero.Length == 11)
                    {
                        string Mensagem = Domain.Core.Configuracoes.SMS_2_FORNECEDORES;

                        if (pedEmpresa.LocalID != null)
                        {
                            var IdLocal = (int)pedEmpresa.LocalID;
                            var LocalEntrega = localRepository.Get(IdLocal);

                            Mensagem = Mensagem.Replace("*|LOCAL|*", Domain.Core.Funcoes.AcertaAcentos(LocalEntrega.Titulo.ToUpper()));
                        }
                        else
                        {
                            Mensagem = Mensagem.Replace("*|LOCAL|*", "local");
                        }

                        new MovileFactory().EnviarSms("55" + Numero, Mensagem);
                    }
                }
                catch (Exception)
                {
                }

                #endregion
            }

            return true;
        }
        //-----------------------------------------------------------------------------
        #endregion
    }
    //=================================================================================
}
