﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using Domain.Repositories.Abstract;

namespace Domain.Repositories
{
    public class AdministradorPedidoRegistroEntregaRepository : PersistentRepository<AdministradorPedidoRegistroEntrega>, IAdministradorPedidoRegistroEntregaRepository
    {
        public AdministradorPedidoRegistroEntregaRepository(ObjectContext context)
            : base(context)
        {
        }

        public void Registra(int administradorID, AdministradorPedidoRegistroEntrega.Acao acao, int? pedidoID, int? pedidoEmpresaID, int? pedidoFloriculturaID, int? fornecedorID)
        {
            var admPedido = new AdministradorPedidoRegistroEntrega();
            admPedido.AdministradorID = administradorID;
            admPedido.PedidoID = pedidoID;
            admPedido.PedidoEmpresaID = pedidoEmpresaID;
            admPedido.AcaoID = (int)acao;
            admPedido.DataCriacao = DateTime.Now;
            this.Save(admPedido);
        }

        public void RegistraLogFornecedor(int administradorID, AdministradorPedidoRegistroEntrega.Acao acao, int? pedidoID, int? pedidoEmpresaID, int? pedidoFloriculturaID, int? fornecedorID)
        {
            var admPedido = new AdministradorPedidoRegistroEntrega();
            admPedido.AdministradorID = administradorID;
            admPedido.IdFornecedor = fornecedorID;
            admPedido.PedidoID = pedidoID;
            admPedido.PedidoEmpresaID = pedidoEmpresaID;
            admPedido.AcaoID = (int)acao;
            admPedido.DataCriacao = DateTime.Now;
            this.Save(admPedido);
        }
    }
}
