﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;

namespace Domain.Repositories
{
    public class EstoqueLoteRepository : PersistentRepository<EstoqueLote>
    {

        public EstoqueLoteRepository(ObjectContext context) : base(context)
        {

        }

        public EstoqueLote GetByCodigo(Guid codigo)
        {
            var pedido = this.GetByExpression(p => p.Numero.CompareTo(codigo) == 0).FirstOrDefault();
            return pedido;
        }


        public bool NumeroExiste(string numero)
        {
            return this.GetByExpression(p => p.Numero == numero).Any();
        }
    }
}
