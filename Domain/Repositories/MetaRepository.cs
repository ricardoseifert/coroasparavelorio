﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Domain.Repositories {
    //===============================================================
    public class MetaRepository : PersistentRepository<Meta>{
        public MetaRepository(ObjectContext context) : base(context){

        }
    }
    //===============================================================
}
