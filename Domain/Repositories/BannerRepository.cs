﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;

namespace Domain.Repositories
{
    public class BannerRepository : PersistentRepository<Banner>
    {

        public BannerRepository(ObjectContext context) : base(context)
        {
        }

        public IEnumerable<Banner> GetByPosicao(Banner.Posicoes posicao)
        {
            return this.GetByExpression(b => b.PosicaoID == (int)posicao && b.Publicado).OrderBy(b => b.Ordem);
        }

    }
}
