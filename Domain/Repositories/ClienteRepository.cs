﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories;
using Domain.Entities;
using System.Data.Objects;

namespace Domain.Repositories
{
    public class ClienteRepository : PersistentRepository<Cliente>
    {

        public ClienteRepository(ObjectContext context) : base(context)
        {
        }

        public Cliente GetByEmailSenha(string email, string senha)
        {
            return this.GetByExpression(c => c.Email == email && c.Senha == senha).FirstOrDefault();
        }
         
    }
}
