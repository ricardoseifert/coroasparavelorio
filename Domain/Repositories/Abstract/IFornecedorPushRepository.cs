﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using Domain.Repositories.Abstract;
using System.Data.Objects;

namespace Domain.Repositories.Abstract
{
    public interface IFornecedorPushRepository : IPersistentRepository<FornecedorPush>
    {
        FornecedorPush RegistraPush(int administradorID, int? pedidoID, int? pedidoEmpresaID, int? pedidoFloriculturaID, int? FornecedorID);
    }

}
