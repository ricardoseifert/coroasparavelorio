﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using Domain.Repositories.Abstract;
using System.Data.Objects;

namespace Domain.Repositories.Abstract
{
    public interface IAdministradorPedidoRepository : IPersistentRepository<AdministradorPedido>
    {
        List<RelatorioHistoricoAdministrador_Result> GetHistoricoAdministrador(int administradorID, DateTime datainicio, DateTime datafim);
        void Registra(int administradorID, int? pedidoID, int? pedidoEmpresaID, int? PedidoFloriculturaID, int? faturamentoID, AdministradorPedido.Acao acao);
    }

}
