﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using Domain.Repositories.Abstract;
using System.Data.Objects;

namespace Domain.Repositories.Abstract
{
    public interface IAdministradorPedidoRegistroEntregaRepository: IPersistentRepository<AdministradorPedidoRegistroEntrega>
    {
       // List<RelatorioHistoricoAdministrador_Result> GetHistoricoAdministrador(int administradorID, DateTime datainicio, DateTime datafim);
        void Registra(int administradorID, AdministradorPedidoRegistroEntrega.Acao acao, int? pedidoID, int? pedidoEmpresaID, int? pedidoFloriculturaID, int? fornecedorID);
    }

}
