﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Tamanho : IPersistentEntity
    {
        public enum Tipos
        {
            [Description("Pequena")]
            Pequena = 1,
            [Description("Média")]
            Media = 2,
            [Description("Grande")]
            Grande = 3,
            [Description("Único")]
            Unico = 100
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public string NomeCompleto
        {
            get { return this.Nome + " - " + this.Dimensao; }
        }
    }
}
