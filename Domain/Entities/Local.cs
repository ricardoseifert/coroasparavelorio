﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Local : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Cemitério")]
            Cemiterio = 1,
            [Description("Velório")]
            Velorio = 2,
            [Description("Crematório")]
            Crematorio = 3,
            [Description("Capela")]
            Capela = 4,
            [Description("Hospital")]
            Hospital = 5,
            [Description("Funerária")]
            Funeraria = 6,
            [Description("Igreja")]
            Igreja = 7,
            [Description("Cidade")]
            Cidade = 100
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public int TotalLocaisRelacionados = 0;

        public string EnderecoMaps
        {
            get
            {
                if (this.Cidade != null && this.Estado != null )
                {
                    if(!String.IsNullOrEmpty(this.Logradouro))
                        return this.Logradouro + ", " + this.Numero + ", " + this.Cidade.Nome + "," + this.Estado.Sigla + ", Brazil";
                    else
                        return this.Cidade.Nome + "," + this.Estado.Sigla + ", Brazil";
                }
                else
                    return "";
            }
        }
    }
}
