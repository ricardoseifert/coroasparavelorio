﻿using DomainExtensions.Entities;

namespace Domain.Entities {
    public partial class StatusMeta:IPersistentEntity {
        public int ID { get { return Id; } }
    }
}
