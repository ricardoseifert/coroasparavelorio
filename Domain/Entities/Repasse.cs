﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;



namespace Domain.Entities
{
    public partial class Repasse : IPersistentEntity, IRepasse
    {

        public enum TodosStatus
        {
            [Description("Aguardando Pagamento")]
            AguardandoPagamento = 1,
            [Description("Pagamento Efetuado")]
            Pago = 2,
            [Description("Cancelado")]
            Cancelado = 3,
            [Description("Fechamento Efetuado")]
            Fechamento = 4,
            [Description("Pagamento em Processamento")]
            PagamentoEmProcessamento = 5
        }

        public TodosStatus Status
        {
            get { return (TodosStatus)this.StatusID; }
            set { this.StatusID = (int)value; }
        }

        public int PagamentoID { get { return this.ID; } }
        public int Fornecedor_ID { get { return this.Fornecedor.ID; } }
        public Pedido PedidoCPV
        {
            get
            {
                if (this.PedidoItem != null)
                {
                    return this.PedidoItem.Pedido;
                }
                else
                {
                    return null;
                }
            }
        }
        public PedidoEmpresa PedidoLacos
        {
            get
            {
                if (this.PedidoEmpresa != null)
                {
                    return this.PedidoEmpresa;
                }
                else
                {
                    return null;
                }
            }
        }
        public Lote Lote
        {
            get
            {
                if (this.Repasse_X_ItemLote.Count > 0)
                {
                    return this.Repasse_X_ItemLote.First().ItemLote.Lote;
                }
                else
                {
                    return null;
                }

            }
        }

        public List<HistoricoRetorno> LstHistorico { get; set; }
        public List<CNAB> LstCNAB { get; set; }
        public int IDStatus { get { return this.StatusID; } }

        public string NrControle { get; set; }

        public int IDFluxoDePagamento { get { return this.Fornecedor.FluxoPagamentoID; } }
        public DateTime? DataPPagamento { get { return this.DataParaPagamento; } }
        public PagamentoGenerico PagamentoFornGenerico { get { return null; } }
        public string NomeFornecedor { get { return this.Fornecedor.Nome.ToUpper(); } }
        public string NomeFavorecido { get { return this.Fornecedor.FavorecidoPrimario.ToUpper(); } }
        public string BancoFornecedor { get { return this.Fornecedor.BancoPrimario.ToUpper(); } }
        public OrigemFornecedor TipoRepasse { get { return OrigemFornecedor.Floricultura; } }
    }
}
