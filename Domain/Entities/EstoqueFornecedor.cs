﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{ 
    public partial class EstoqueFornecedor : IPersistentEntity
    {

        

        public EstoqueFornecedor()
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;
        }

       
      

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public enum Tipos
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("PF")]
            PessoaFisica = 1,
            [Description("PJ")]
            PessoaJuridica = 2
        }

    }
}
