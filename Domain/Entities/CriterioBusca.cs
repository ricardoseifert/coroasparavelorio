﻿using DomainExtensions.Entities;

namespace Domain.Entities {
    public partial class CriterioBusca :IPersistentEntity {
        public int ID { get { return Id; } }

        public enum TodosTipos{
            Cpv = 1,
            LacosCorp = 2,
            CpvELacosCorp = 3
        }
    }
}
