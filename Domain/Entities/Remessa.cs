﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;

namespace Domain.Entities
{
    public partial class Remessa : IPersistentEntity
    {
        public enum Empresa
        {
            [Description("CPV")]
            CPV = 1,
            [Description("LC")]
            LC = 2,
            [Description("AMAR")]
            AMAR = 3
        }
    }
}
