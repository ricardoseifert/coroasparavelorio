﻿using System.ComponentModel;

namespace Domain.Entities {

    public enum TodosStatusEntrega {
        
        [Description("Pendente de Fornecedor")]
        PendenteFornecedor = 1,
        [Description("Em Produção")]
        EmProducao = 2,
        [Description("Entregue")]
        Entregue = 3,
        [Description("Cancelado")]
        Cancelado = 6,
        [Description("Entrega em Andamento")]
        EntregaEmAndamento = 7,
        [Description("Pendente de Atendimento")]
        PendenteAtendimento = 8

        //[Description("Entrega não requerida")]
        //EntregaNaoRequerida = 4,
        //[Description("Devolvido")]
        //Devolvido = 5,
        //[Description("Pendente de Entrega")]
        //PendenteEntrega = 7,
    }

    public enum TodosStatusCancelamento {
        [Description("Solicitado pelo Cliente")]
        Cliente = 1,
        [Description("Erro Interno")]
        ErroInterno = 2,
        [Description("Erro Externo")]
        ErroExterno = 3,
        [Description("Erro de Sistema")]
        ErroSistema = 4,
        [Description("Teste")]
        Teste = 5,
        [Description("Não Autorizado")]
        NaoAutorizado = 6,
        [Description("Erro Fornecedor")]
        ErroFornecedor = 7,
        [Description("Erro Atendimento")]
        ErroAtendimento = 8,
        [Description("Erro Floricultura")]
        ErroFloricultura = 9,
        [Description("Erro Equipe Fornecedores")]
        ErroEquipeFornecedores = 10,
        [Description("Erro Cliente")]
        ErroCliente = 11,
        [Description("Erro Outro Fornecedor")]
        ErroOutroFornecedor = 12
    }

    public enum OrigemPedido {
        Cliente = 1,

        Empresa = 2
    }

    public enum Status { Ativo, Inativo }

    public enum SistemaPedido {
        [Description("Todos")]
        Ambos = 0,
        [Description("Coroas - CPV")]
        Cliente = 1,
        [Description("Laços - Corporativos")]
        Empresa = 2,
        [Description("Amar Assist")]
        AmarAssist = 3
    }
    public enum OrigemFornecedor
    {
        Floricultura, Generico
    }

}
