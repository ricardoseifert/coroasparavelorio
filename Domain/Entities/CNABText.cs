﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class CNABText
    {
        public string GerarHeaderArquivo()
        {
            #region HEADER DO ARQUIVO

            string HeaderDoArquivo = "";
            // COD DO BANCO
            HeaderDoArquivo += "341";
            // COD DO LOTE
            HeaderDoArquivo += "0000";
            // TIPO DO REGISTRO
            HeaderDoArquivo += "0";
            // BRANCOS
            HeaderDoArquivo += "      ";
            // LAYOUT DO ARQUIVO
            HeaderDoArquivo += "081";
            // EMPRESA
            HeaderDoArquivo += "2";
            // INSCRICAO NUMERO
            HeaderDoArquivo += "12404942000114";
            // BRANCOS
            HeaderDoArquivo += "                    ";
            // AGENCIA
            HeaderDoArquivo += "00745";
            // BRANCOS
            HeaderDoArquivo += " ";
            // CONTA
            HeaderDoArquivo += "000000060343";
            // BRANCOS
            HeaderDoArquivo += " ";
            // DAC
            HeaderDoArquivo += "1";
            // NOME EMPRESA     
            HeaderDoArquivo += "ESG COMERCIO ELETRONICO LTDA  ";
            // NOME DO BANCO
            HeaderDoArquivo += "BANCO ITAU SA                 ";
            // BRANCOS
            HeaderDoArquivo += "          ";
            // ARQUIVO CODIGO
            HeaderDoArquivo += "1";
            // DATA DE GERACAO
            HeaderDoArquivo += DateTime.Now.ToString("ddMMyyy");
            // HORA DE GERACAO
            HeaderDoArquivo += DateTime.Now.ToString("hhmmss");
            // ZEROS
            HeaderDoArquivo += "000000000";
            // DENSIDADE
            HeaderDoArquivo += "00000";
            // ZEROS
            HeaderDoArquivo += "                                                                     ";
            HeaderDoArquivo += "\r\n";

            #endregion

            return HeaderDoArquivo;
        }

        public string GerarFooterArquivo(int TotalDeLotes, int TotalDeRegistros)
        {
            #region FOOTER DO ARQUIVO

            string FooterArquivo = "";
            // COD DO BANCO
            FooterArquivo += "341";
            // COD DO LOTE
            FooterArquivo += "9999";
            // TIPO DO REGISTRO
            FooterArquivo += "9";
            // BRANCOS
            FooterArquivo += "         ";
            // TOTAL DE LOTES
            FooterArquivo += TotalDeLotes.ToString("D6");
            // TOTAL QTD DE REGISTROS
            FooterArquivo += TotalDeRegistros.ToString("D6");
            // BRANCOS
            FooterArquivo += "                                                                                                                                                                                                                   ";
            FooterArquivo += "\r\n";

            #endregion

            return FooterArquivo;
        }

        public string Gerar()
        {
            string LinasPagamento = "";


            LinasPagamento += this.Header.Gerar();
            LinasPagamento += "\r\n";

            foreach (var Linha in this.Linhas)
            {
                LinasPagamento += Linha.CodigoBanco;
                LinasPagamento += Linha.CodigoLote;
                LinasPagamento += Linha.TipoRegistro;
                LinasPagamento += Linha.NumeroRegistro;
                LinasPagamento += Linha.Segmento;
                LinasPagamento += Linha.TipoDeSegmento;
                LinasPagamento += Linha.Camara;
                LinasPagamento += Linha.NrBancoFavorecido;
                LinasPagamento += Linha.Agencia;
                LinasPagamento += " ";
                LinasPagamento += Linha.Conta;
                LinasPagamento += " ";
                LinasPagamento += Linha.ContaDigito;
                LinasPagamento += Linha.NomeFavorecido;
                LinasPagamento += Linha.SeuNumero;
                LinasPagamento += Linha.DataPagamento;
                LinasPagamento += Linha.MoedaTipo;
                LinasPagamento += Linha.CodigoISPB;
                LinasPagamento += Linha.Zeros;
                LinasPagamento += Linha.Valor;
                LinasPagamento += Linha.NossoNumero;
                LinasPagamento += Linha.Brancos1;
                LinasPagamento += Linha.DataEfetiva;
                LinasPagamento += Linha.ValorEfetivo;
                LinasPagamento += Linha.FinalidadeDetalhe;
                LinasPagamento += Linha.Brancos2;
                LinasPagamento += Linha.NumDocumento;
                LinasPagamento += Linha.DocumentoForn;
                LinasPagamento += Linha.FinalidadeDocStatusFuncionario;
                LinasPagamento += Linha.FinalidadeTed;
                LinasPagamento += Linha.Brancos3;
                LinasPagamento += Linha.Aviso;
                LinasPagamento += Linha.Ocorrencias;

                LinasPagamento += "\r\n";
            }

            LinasPagamento += this.Footer.Gerar();
            LinasPagamento += "\r\n";

            return LinasPagamento;
        }

        public List<CNABTextLinha> Linhas { get; set; }
        public CNABTextLinhaHeader Header { get; set; }
        public CNABTextLinhaFooter Footer { get; set; }

    }
}
