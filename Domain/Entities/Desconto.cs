﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Desconto : IPersistentEntity
    {

        public enum Tipos
        {
            Valor = 1,
            Percentual = 2
        }

        public enum Regras
        {
            [Description("Em Todo o Site")]
            EmTodoSite = 1,

            [Description("A Partir do Segundo Produto de Menor Valor")]
            APartirDoSegundoProdutoMenorValor = 2,

            [Description("A Partir do Segundo Produto de Maior Valor")]
            APartirDoSegundoProdutoMaiorValor = 3,

            [Description("A Partir de Determinado Valor")]
            APartirDeValor = 4
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public Regras Regra
        {
            get { return (Regras)this.RegraID; }
            set { this.RegraID = (int)value; }
        }

    }
}
