﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Depoimento : IPersistentEntity
    {
        public enum Tipos
        {
            [Description("Pessoa Física")]
            PF = 1,
            [Description("Pessoa Jurídica")]
            PJ = 2
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }
    }
}
