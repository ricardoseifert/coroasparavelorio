﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;

namespace Domain.Entities
{
    public partial class FornecedorGenerico : IPersistentEntity
    {
        decimal _ValorPagamentosEmAberto = 0;
        decimal _QtdPagamentosEmAberto = 0;

        decimal _ValorPagamentosPagos = 0;
        decimal _QtdPagamentosPagos = 0;

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public enum Tipos
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("PF")]
            PessoaFisica = 1,
            [Description("PJ")]
            PessoaJuridica = 2
        }

        public int QtdDeLotes
        {
            get
            {
                return this.ItemLotes.Where(r => r.FornecedorGenerico.ID == this.ID).ToList().Count;
            }
        }

        public decimal ValorPagamentosEmAberto
        {
            get
            {
                foreach (var Item in this.ItemLotes.Where(r => r.FornecedorGenerico.ID == this.ID).ToList().ToList())
                {
                    _ValorPagamentosEmAberto += Item.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status == Entities.PagamentoGenerico.TodosStatusPagamento.AguardandoPagamento).Sum(r => r.PagamentoGenerico.Valor);
                }
                return _ValorPagamentosEmAberto;
            }
        }
        public decimal QtdPagamentosEmAberto
        {
            get
            {
                foreach (var Item in this.ItemLotes.Where(r => r.FornecedorGenerico.ID == this.ID).ToList().ToList())
                {
                    _QtdPagamentosEmAberto += Item.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status == Entities.PagamentoGenerico.TodosStatusPagamento.AguardandoPagamento).ToList().Count;
                }

                return _QtdPagamentosEmAberto;
            }
        }

        public decimal ValorPagamentosPagos
        {
            get
            {
                foreach (var Item in this.ItemLotes.Where(r => r.FornecedorGenerico.ID == this.ID).ToList().ToList())
                {
                    _ValorPagamentosPagos += Item.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status == Entities.PagamentoGenerico.TodosStatusPagamento.Pago).Sum(r => r.PagamentoGenerico.Valor);
                }
                return _ValorPagamentosPagos;
            }
        }
        public decimal QtdPagamentosPagos
        {
            get
            {
                foreach (var Item in this.ItemLotes.Where(r => r.FornecedorGenerico.ID == this.ID).ToList().ToList())
                {
                    _QtdPagamentosPagos += Item.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status == Entities.PagamentoGenerico.TodosStatusPagamento.Pago).ToList().Count;
                }

                return _QtdPagamentosPagos;
            }
        }
    }
}
