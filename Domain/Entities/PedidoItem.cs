﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class PedidoItem : IPersistentEntity
    {
        public enum TodosTiposFaixa
        {
            [Description("PF")]
            PF = 1,
            [Description("PJ")]
            PJ = 2
        }
        public enum TodosStatusFoto
        {
            [Description("Não Aprovada")]
            NaoEnviada = 1,
            [Description("Aguardando Aprovação")]
            AguardandoAprovacao = 2,
            [Description("Aprovada")]
            Aprovada = 3
        }

        public TodosStatusFoto StatusFoto
        {
            get { return (TodosStatusFoto)this.StatusFotoID; }
            set { this.StatusFotoID = (int)value; }
        }
        
        public TodosTiposFaixa TiposFaixa
        {
            get { return (TodosTiposFaixa)this.TipoFaixaID; }
            set { this.TipoFaixaID = (int)value; }
        }

        public Repasse RepasseAtual
        {
            get
            {
                var repasse = this.Repasses.Where(c => c.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || c.Status == Domain.Entities.Repasse.TodosStatus.Pago || c.Status == Domain.Entities.Repasse.TodosStatus.Fechamento || c.Status == Domain.Entities.Repasse.TodosStatus.PagamentoEmProcessamento).FirstOrDefault();
                if (repasse != null)
                    return repasse;
                else
                    return new Repasse();
            }
        }

        public decimal ValorIdealRepasse
        {
            get
            {
                int Fator = 0;
                decimal ValorProduto = (this.Valor / 100) * 100;

                if (ValorProduto <= 188)
                    Fator = 60;
                if (ValorProduto >= 189 && ValorProduto <= 225)
                    Fator = 57;
                if (ValorProduto >= 226 && ValorProduto <= 300)
                    Fator = 53;
                if (ValorProduto >= 301 && ValorProduto <= 350)
                    Fator = 52;
                if (ValorProduto >= 351 && ValorProduto <= 400)
                    Fator = 50;
                if (ValorProduto >= 401 && ValorProduto <= 600)
                    Fator = 45;
                if (ValorProduto >= 601 && ValorProduto <= 985)
                    Fator = 40;
                if (ValorProduto >= 986 && ValorProduto <= 1250)
                    Fator = 35;
                if (ValorProduto >= 1251 && ValorProduto < 1755)
                    Fator = 30;

                // ALTERAÇÃO DE FATOR PARA FATOR -5% PEDIDA PELO BASILE. DIA 17.07.2017
                return (ValorProduto / 100) * (Fator - 5);
            }
        }

        public string FotoEntrega
        {
            get
            {
                if (Domain.Core.Configuracoes.HOMOLOGACAO)
                {
                    return "/content/foto_admin_homolog_solution.jpg";
                }
                else
                {
                    var file = new FileInfo(System.Web.Hosting.HostingEnvironment.MapPath("/content/pedidos/" + this.Pedido.Numero + "/" + this.ID + ".jpg"));
                    if (file.Exists)
                        return "/content/pedidos/" + this.Pedido.Numero + "/" + this.ID + ".jpg";
                    else
                        return "";
                }
                
            }
        }
    }
}
