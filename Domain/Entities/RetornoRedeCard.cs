﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class RetornoRedeCard
    {
        public string TID { get; set; }
        public string NuAuthTransacao { get; set; }
        public string NunSeq { get; set; }
        public DateTime DataTran { get; set; }
        public int Codret { get; set; }
        public string MsgRet { get; set; }

    }
}
