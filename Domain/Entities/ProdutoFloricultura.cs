﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class ProdutoFloricultura : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Coroa de Flores")]
            CoroaFlor = 1
        }

        public bool ProdutoPersonalizado
        {
            get
            {
                if(this.CategoriaProdutoFloricultura.Nome == "Personalizado")
                {
                    return true;
                }
                {
                    return false;
                }
            }
        }
    }
}
