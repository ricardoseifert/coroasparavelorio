﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class Bairro : IPersistentEntity
    {

        public string NomeCompleto
        {
            get
            {
                try
                {
                    return this.Nome + " - " + this.Estado.Nome;
                }
                catch
                {
                    return "";
                }
            }
        }

        public string EnderecoMaps
        {
            get
            {
                if (this.Estado != null)
                {
                    return this.Nome + "," + this.Estado.Sigla + ", Brazil";
                }
                else
                    return "";
            }
        }
    }
}
