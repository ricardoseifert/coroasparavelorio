﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Reclamacao : IPersistentEntity
    {
        public enum TodosStatus
        {
            [Description("Resolvido")]
            Resolvido = 1,
            [Description("Em Análise")]
            EmAnalise = 2,
            [Description("Pendente")]
            Pendente = 3
        }

        public TodosStatus Status
        {
            get { return (TodosStatus)this.StatusID; }
            set { this.StatusID = (int)value; }
        }
    }
}
