﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Core;
using System.Net;
using Domain.Service;
using Newtonsoft.Json;
using System.Configuration;
using System.Data.EntityClient;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;

namespace Domain.Entities
{
    public partial class PedidoEmpresa : IPersistentEntity, IPedido
    {
        private LogMandrillRepository logMandrillRepositor;
        
        public enum TodosStatus
        {
            [Description("Novo Pedido")]
            NovoPedido = 1,
            [Description("Pedido em Análise")]
            PedidoEmAnalise = 2,
            [Description("Pedido Concluído")]
            PedidoConcluido = 3,
            [Description("Pedido Cancelado")]
            PedidoCancelado = 4,
            [Description("Pedido Reprovado")]
            PedidoReprovado = 99
        }
        public enum TodosStatusNFsRemessa
        {
            [Description("Não Emitida")]
            NaoEmitida = 1,
            [Description("Processando")]
            Processando = 2,
            [Description("Emitida")]
            Emitida = 3
        }
        public enum CFOPsRemessa
        {
            [Description("Outra saída de mercadoria - 5949")]
            OutraSaidaMercadoria_5949 = 5949,
            [Description("Outra saída de mercadoria - 6949")]
            OutraSaidaMercadoria_6949 = 6949,
            [Description("Mercadoria - Ordem de terceiros - 5923")]
            RemessaMercadoriaContaOrdemTerceiros_5923 = 5923,
            [Description("Mercadoria - Ordem de terceiros - 6923")]
            RemessaMercadoriaContaOrdemTerceiros_6923 = 6923
        }


        public enum TodosStatusFoto
        {
            [Description("Não Enviada")]
            NaoEnviada = 1,
            [Description("Aguardando Aprovação")]
            AguardandoAprovacao = 2,
            [Description("Aprovada")]
            Aprovada = 3
        }

        public TodosStatusNFsRemessa StatusNFRemessa
        {
            get { return (TodosStatusNFsRemessa)this.StatusNfIDRemessa; }
            set { this.StatusNfIDRemessa = (int)value; }
        }

        public TodosStatusFoto StatusFoto
        {
            get { return (TodosStatusFoto)this.StatusFotoID; }
            set { this.StatusFotoID = (int)value; }
        }
        public enum TodosStatusContatoNao
        {
            [Description("Não Informado")]
            NI = 99,
            [Description("Não Atendeu")]
            NaoAtendeu = 1,
            [Description("Exterior")]
            Exterior = 2,
            [Description("Telefone Incorreto")]
            TelefoneIncorreto = 3,
            [Description("Falta de Tempo")]
            FaltaTempo = 4,
            [Description("Comercial")]
            Comercial = 5
        }
        public enum TodosTiposDeFrete
        {
            [Description("Emitente")]
            Emitente = 1,
            [Description("Destinatário")]
            Destinatario = 2,
            [Description("Terceiros")]
            Terceiros = 3,
            [Description("Sem Frete")]
            SemFrete = 4
        }
        public enum TodosStatusContato
        {
            [Description("Não Informado")]
            NI = 99,
            [Description("SIM")]
            Sim = 1,
            [Description("NÃO")]
            Nao = 2
        }
        public TodosTiposDeFrete TipoDeFrete
        {
            get { return (TodosTiposDeFrete)this.TipoFreteID; }
            set { this.TipoFreteID = (int)value; }
        }
        public TodosStatus Status
        {
            get { return (TodosStatus)this.StatusID; }
            set { this.StatusID = (int)value; }
        }
        public CFOPsRemessa CFOPRemessa
        {
            get { return (CFOPsRemessa)this.CFOPRemessaID; }
            set { this.CFOPRemessaID = (int)value; }
        }
        public TodosStatusEntrega StatusEntrega
        {
            get { return (TodosStatusEntrega)this.StatusEntregaID; }
            set { this.StatusEntregaID = (int)value; }
        }
        public TodosStatusCancelamento StatusCancelamento
        {
            get { return (TodosStatusCancelamento)this.StatusCancelamentoID; }
            set { this.StatusCancelamentoID = (int)value; }
        }
        public TodosStatusContato StatusContato
        {
            get { return (TodosStatusContato)this.QualidadeStatusContatoID; }
            set { this.QualidadeStatusContatoID = (int)value; }
        }
        public TodosStatusContatoNao StatusContatoNao
        {
            get { return (TodosStatusContatoNao)this.QualidadeStatusContatoNaoID; }
            set { this.QualidadeStatusContatoNaoID = (int)value; }
        }

        //public List<Repasse> Repasse
        //{

        //    return new Repasse();
        // }
        public Repasse RepasseAtual
        {
            get
            {
                var repasse = this.Repasses.Where(c => c.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || c.Status == Domain.Entities.Repasse.TodosStatus.Pago || c.Status == Domain.Entities.Repasse.TodosStatus.Fechamento || c.Status == Domain.Entities.Repasse.TodosStatus.PagamentoEmProcessamento).FirstOrDefault();
                if (repasse != null)
                    return repasse;
                else
                    return new Repasse();
            }
        }

        public decimal ValorIdealRepasse
        {
            get
            {
                int Fator = 0;
                decimal ValorProduto = (this.Valor / 100) * 100;

                if (ValorProduto <= 188)
                    Fator = 60;
                if (ValorProduto >= 189 && ValorProduto <= 225)
                    Fator = 57;
                if (ValorProduto >= 226 && ValorProduto <= 300)
                    Fator = 53;
                if (ValorProduto >= 301 && ValorProduto <= 350)
                    Fator = 52;
                if (ValorProduto >= 351 && ValorProduto <= 400)
                    Fator = 50;
                if (ValorProduto >= 401 && ValorProduto <= 600)
                    Fator = 45;
                if (ValorProduto >= 601 && ValorProduto <= 985)
                    Fator = 40;
                if (ValorProduto >= 986 && ValorProduto <= 1250)
                    Fator = 35;
                if (ValorProduto >= 1251 && ValorProduto < 1755)
                    Fator = 30;

                // ALTERAÇÃO DE FATOR PARA FATOR -5% PEDIDA PELO BASILE. DIA 17.07.2017
                return (ValorProduto / 100) * (Fator - 5);
            }
        }

        public DateTime? DataCobranca
        {
            get
            {
                if (this.Faturamento != null)
                    return this.Faturamento.DataVencimento;

                return null;
            }
        }

        public int TotalRepassesAberto
        {
            get
            {
                return this.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || c.Status == Domain.Entities.Repasse.TodosStatus.Pago);
            }
        }
        public string LatitudeEntregaReturn
        {
            get
            {
                return this.LatitudeEntrega;
            }
        }
        public string LongitudeEntregaReturn
        {
            get
            {
                return this.LongitudeEntrega;
            }
        }
        public DateTime? DtUpdateLocEntregaReturn
        {
            get
            {
                if (DtUpdateLocEntrega != null)
                {
                    return DtUpdateLocEntrega.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public string NomeFantasia
        {
            get
            {
                return this.Empresa.NomeFantasia;
            }
        }

        public bool ProdutoKitBB
        {
            get
            {
                if (this.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe || this.ProdutoTamanho.ProdutoID == 454)
                    return true;

                return false;
            }
        }

        public string NomeDoProduto
        {
            get
            {
                string Nome = this.ProdutoTamanho.Produto.Nome + " " + this.ProdutoTamanho.Tamanho.NomeCompleto;
                Nome = Nome.Replace("-", "");
                return Nome;
            }
        }

        public int TotalRepassesPagos
        {
            get
            {
                return this.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.Pago);
            }
        }
        
        public bool JaSaiuEntrega
        {
            get
            {
                if (this.AdministradorPedidoes.Where(r => r.AcaoID == (int)Domain.Entities.AdministradorPedido.Acao.SaiuParaEntrega).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int? AdministradorPedidoID
        {
            get
            {
                try
                {
                    return this.Administrador.ID;
                }
                catch
                {
                    return null;
                }
            }
        }

        public string AdministradorNome
        {
            get
            {
                try
                {
                    return this.Administrador.Nome;
                }
                catch
                {
                    return null;
                }
            }
        }

        public int QtdFornecedores
        {

            get
            {
                return this.Local.FornecedorLocals.Count;
            }
        }
        public int MinutosTotal
        {

            get
            {
                return this.Local.FornecedorLocals.Count * int.Parse(System.Configuration.ConfigurationManager.AppSettings["TempoLeiaoPorFloricultura"]);
            }
        }
        public List<int> CategoriaDosProdutos
        {
            get
            {
                List<int> result = new List<int>();
                result.Add(this.ProdutoTamanho.Produto.TipoID);
                return result;
            }
        }

        public int RepasseId { get; set; }
        public List<string> NomeProdutos
        {
            get
            {
                List<string> NomeProdutos = new List<string>();

                NomeProdutos.Add(this.ProdutoTamanho.Produto.Nome + " " + this.ProdutoTamanho.Tamanho.Nome);

                return NomeProdutos;
            }

        }
        public bool RepassesPagos
        {
            get
            {
                return this.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.Pago) == 1;
            }
        }


        public void EnviarEmailPedidoEmProducao()
        {
            #region PREENCHE RECEBERORES EMAIL

            List<ToMandrill> LstToMandril = new List<ToMandrill>();

            if (Core.Funcoes.ValidaEmail(this.Colaborador.Email.Trim()))
            {
                LstToMandril.Add(new ToMandrill { email = this.Colaborador.Email.Trim(), name = this.Colaborador.Nome.Trim() });
            }

            foreach (var colaborador in this.Empresa.Colaboradors.Where(c => c.RecebeEmailPedido && c.Email != this.Colaborador.Email))
            {
                if (Core.Funcoes.ValidaEmail(colaborador.Email.Trim()))
                {
                    LstToMandril.Add(new ToMandrill { email = this.Colaborador.Email.Trim(), name = this.Colaborador.Nome.Trim() });
                }
            }

            #endregion

            if (LstToMandril.Count > 0)
            {
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    LstToMandril.Clear();
                    LstToMandril.Add(new ToMandrill { email = Core.Configuracoes.EMAIL_HOMOLOGACAO, name = this.Colaborador.Nome.Trim() });
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-pedido-em-produ-o-lc";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Laços Corporativos",
                    content = "Seu pedido #" + this.Numero + " está em produção!"
                });

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Colaborador.Nome });
                Lstvars.Add(new varsMandrill { name = "NUMEROPEDIDO", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAENTREGA", content = this.DataSolicitada.ToString() });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });

                Lstvars.Add(new varsMandrill { name = "SUBTOTAL", content = this.Valor.ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "DESCONTO", content = this.ValorDesconto.ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "VALORTOTAL", content = this.Valor.ToString("C2") });

                var Produtos = "";
                var Frases = "";

                Produtos += "<tr> <td width='20 % '> <img src='https://www.coroasparavelorio.com.br/content/produtos/" + this.ProdutoTamanho.ProdutoID + "/thumb/" + this.ProdutoTamanho.Produto.Foto + "' style='padding-right: 10px; max-width: 100px; font-family:open sans,helvetica neue,helvetica,arial,sans-serif;'/> </td> <td width='40%'>" + this.ProdutoTamanho.Produto.Nome + "</td> <td width='20%' style='text-align:center'>" + this.ProdutoTamanho.Tamanho.Nome + "</td> <td width='20%' style='text-align:center'>" + this.Valor.ToString("C2") + "</td> </tr>";
                Frases += "<b>Frase de Homenagem: </b>" + this.Mensagem + "<br />";

                if (this.ValorDesconto > 0)
                {
                    Lstvars.Add(new varsMandrill { name = "DESCONTO", content = "<tr id='desconto'><td><b>Desconto:</b><span style='color:red'> " + this.ValorDesconto.ToString("C2") + "</span></td></tr>" });
                }

                Lstvars.Add(new varsMandrill { name = "frasedehomenagem", content = Frases });

                Lstvars.Add(new varsMandrill { name = "DESCRPRODUTO", content = Produtos });
                Lstvars.Add(new varsMandrill { name = "homenageado", content = this.PessoaHomenageada });

                #region PREENCHER DESCRITIVO MEIO DE PGT

                string FormaPagamento = "";
                if (this.Faturamento != null)
                {
                    FormaPagamento = Domain.Helpers.EnumHelper.GetDescription(this.Faturamento.MeioPagamento);
                    if (FormaPagamento == "Boleto Pagar Me")
                        FormaPagamento = "Boleto";
                }
                else
                {
                    FormaPagamento = "N/A";
                }

                Lstvars.Add(new varsMandrill { name = "formapagamento", content = FormaPagamento });

                #endregion

                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    if (LocalEntrega == null)
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        if (LocalEntrega.ToUpper() == "")
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                        }
                        else
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                        }
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.Local.Titulo.ToUpper() });
                    }
                }

                foreach(var Email in LstToMandril)
                {
                    var Lst = new List<ToMandrill>();
                    Lst.Add(Email);

                    novoEnvio.message = new MessagemMandrill()
                    {
                        subject = "Seu pedido #" + this.Numero + " está em produção!",
                        from_email = "noreply@lacoscorporativos.com.br",
                        from_name = "Laços Corporativos",
                        to = Lst,
                        headers = new headersMandrill() { ReplyTo = "comercial@lacoscorporativos.com.br" },
                        merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Email.email,
                            vars = Lstvars
                        }
                    }
                    };

                    var retorno = new Mandrill().CallMandrill(novoEnvio);

                    #region SALVAR LOG

                    try
                    {
                        dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                        string IdEmail = "";
                        string reject_reason = "";

                        foreach (var item in dynJson)
                        {
                            IdEmail = item._id;
                            reject_reason = item.reject_reason;
                        }

                        if (reject_reason == "null")
                            reject_reason = "";

                        if (!string.IsNullOrEmpty(IdEmail))
                        {
                            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                            var entityBuilder = new EntityConnectionStringBuilder();

                            entityBuilder.Provider = "System.Data.SqlClient";
                            entityBuilder.ProviderConnectionString = conStr;

                            logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                            logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.PedidoEmProdução.ToString(), this.ID, null, reject_reason);
                        }
                    }
                    catch { }

                    #endregion
                }
            }
        }

        public bool EnviarEmailEntrega()
        {
            var mensagem = "Seu produto foi entregue. Confira os dados da entrega:";

            if (this.DataEntrega != null)
                mensagem += "<br> <b>Data:</b> " + this.DataEntrega.Value.ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(this.PessoaHomenageada))
                mensagem += "<br><b>Homenageado: </b> " + this.PessoaHomenageada.ToUpper();

            if (!String.IsNullOrEmpty(this.RecebidoPor))
                mensagem += "<br><b>Recebido por: </b> " + this.RecebidoPor.ToUpper();

            if (LocalEntrega.ToLower() == "LOCAL NÃO CADASTRADO" || string.IsNullOrEmpty(LocalID.ToString()))
            {
                mensagem += "<br><b>Local de entrega: </b> " + this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto;
            }
            else
            {
                mensagem += "<br><b>Local de entrega: </b> " + this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto;
            }

            if (!String.IsNullOrEmpty(this.ComplementoLocalEntrega))
                mensagem += "<br><b>Complemento do local de entrega: </b> " + this.ComplementoLocalEntrega.ToUpper();

            if (!String.IsNullOrEmpty(this.Parentesco))
                mensagem += "<br><b>Relacionamento: </b> " + this.Parentesco.ToUpper();

            //ENVIAR EMAIL
            var html = File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/entrega.htm"));
            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOMEPARA]", this.Colaborador.Nome);
            html = html.Replace("[MENSAGEM]", mensagem);

            var dadosRetorno =
                (
                    Core.Funcoes.EnviaEmail
                    (
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        Colaborador.Email,
                        Colaborador.Nome,
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos", "",
                        "[LAÇOS CORPORATIVOS] - Seu pedido foi entregue com sucesso!",
                        html,
                        true
                    )
                );

            return dadosRetorno;
        }

        public bool EnviarEmailCancelamento()
        {
            var itenspedido = "";
            itenspedido += "<tr>";
            itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>";
            itenspedido += "<img src='" + Core.Configuracoes.DOMINIO + "/content/Produtos/" + this.ProdutoTamanho.ProdutoID + "/thumb/" + this.ProdutoTamanho.Produto.Foto + "' width='75'/>";
            itenspedido += "</td>";
            itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Produto.Nome + "</td>";
            itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Tamanho.Nome + "</td>";
            itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.Valor.ToString("C2") + "</td>";
            itenspedido += "</tr>";

            //ENVIAR EMAIL
            var html = File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/pedido-cancelado.htm"));
            html = html.Replace("[URL]", Core.Configuracoes.CDN_IMG);
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial + (!string.IsNullOrEmpty(this.Empresa.NomeFantasia) ? " (" + this.Empresa.NomeFantasia + ")" : ""));
            html = html.Replace("[HOMENAGEADO]", this.PessoaHomenageada);
            if (string.IsNullOrEmpty(LocalEntrega) || LocalEntrega == "LOCAL NÃO CADASTRADO")
            {
                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega);
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega);
                    }
                    else
                    {
                        html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper());
                    }
                }
            }
            else
            {
                html = html.Replace("[LOCAL]", this.LocalEntrega + "<br/>" + this.ComplementoLocalEntrega);
            }

            html = html.Replace("[CIDADEENTREGA]", this.Cidade.NomeCompleto);
            html = html.Replace("[STATUSPEDIDO]", Helpers.EnumHelper.GetDescription(this.Status));
            html = html.Replace("[MENSAGEM]", this.Mensagem);
            if (this.Observacoes.Contains("Horário de Entrega:"))
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " das " + this.Observacoes.Substring(20, 26));
            }
            else
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " às " + this.DataSolicitada.ToString("HH:mm"));
            }

            html = html.Replace("[NOME]", this.Colaborador.Nome);
            html = html.Replace("[NUMEROPEDIDO]", this.ID.ToString());
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy HH:mm"));
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial);
            html = html.Replace("[VALORTOTAL]", this.Valor.ToString("C2"));
            html = html.Replace("[ITENSPEDIDO]", itenspedido);

            if (Core.Funcoes.ValidaEmail(this.Colaborador.Email.Trim()))
                Core.Funcoes.EnviaEmail(
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "Laços Corporativos",
                    this.Colaborador.Email,
                    this.Colaborador.Nome,
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "Laços Corporativos",
                    "",
                    "[LAÇOS CORPORATIVOS] - Pedido cancelado",
                    html,
                    true
                );

            foreach (var colaborador in this.Empresa.Colaboradors.Where(c => c.RecebeEmailPedido && c.Email != this.Colaborador.Email))
            {
                if (Core.Funcoes.ValidaEmail(colaborador.Email.Trim()))
                    Core.Funcoes.EnviaEmail(
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        colaborador.Email,
                        colaborador.Nome,
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        "",
                        "[LAÇOS CORPORATIVOS] - O pedido do colaborador " + this.Colaborador.Nome + " #" + this.ID + " foi cancelado com sucesso!",
                        html,
                        true
                    );
            }

            Core.Funcoes.EnviaEmail(
                Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                "Laços Corporativos",
                Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                "Atendimento",
                Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                "Laços Corporativos",
                "",
                "[LAÇOS CORPORATIVOS] - O pedido #" + this.ID + " foi cancelado com sucesso!",
                html,
                true
            );

            return true;

        }

        public void EnviarEmailFotoProduro()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Colaborador.Email.Trim()))
            {
                var Emails = this.Colaborador.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "lc-foto-do-produto";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Laços Corporativos",
                    content = "Veja a foto do seu produto!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Colaborador.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                string Fotos = "";
                Fotos += "<tr><td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center; \"><img align=\"center\" alt=\"\" src=\"" + "http://admin.coroasparavelorio.com.br/content/pedidos/" + this.Numero + "/" + this.Numero + ".jpg" + "\" width=\"262\" style=\"max-width:262px; padding-bottom: 0; display: inline !important; vertical-align: bottom; \" class=\"mcnImage\"></td></tr>";

                string name = this.Colaborador.Nome;

                if (this.Colaborador.Nome.Contains(" "))
                    name = this.Colaborador.Nome.Substring(0, this.Colaborador.Nome.IndexOf(" "));

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "FOTO", content = Fotos });
                Lstvars.Add(new varsMandrill { name = "NOME", content = name });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Veja a foto do seu produto!",
                    from_email = "noreply@lacoscorporativos.com.br",
                    from_name = "Laços Corporativos",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "comercial@lacoscorporativos.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Foto.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }

                #endregion
            }
        }

        public void EnviarEmailCobranca()
        {
            #region PREENCHER EMAIL

            string Emails = "";

            if (!string.IsNullOrEmpty(this.Empresa.EmailFinanceiro))
                Emails += this.Empresa.EmailFinanceiro + "|" + this.Empresa.RazaoSocial + ",";

            if (!string.IsNullOrEmpty(this.Colaborador.Email))
                Emails += this.Colaborador.Email + "|" + this.Colaborador.Nome + ",";

            foreach (var Colaborador in this.Empresa.Colaboradors)
            {
                if (!string.IsNullOrEmpty(Colaborador.Email) && Colaborador.Tipo == Colaborador.Tipos.Master)
                    Emails += Colaborador.Email + "|" + Colaborador.Nome + ",";
            }

            Emails = Emails.Remove(Emails.Length - 1);

            #endregion

            foreach (var Email in Emails.Split(','))
            {
                string EmailsEnviar = Email.Split('|')[0];
                string NomeEnviar = Email.Split('|')[1];

                if (Domain.Core.Funcoes.ValidaEmail(EmailsEnviar.Trim()))
                {
                    if (Core.Configuracoes.HOMOLOGACAO)
                    {
                        EmailsEnviar = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                    }

                    //// REMPORARIO
                    EmailsEnviar = "ricardo.seifertb@gmail.com";

                    DefaultMandrill novoEnvio = new DefaultMandrill();
                    novoEnvio.template_name = "cobranca";
                    novoEnvio.template_content.Add(new TemplateContentMandrill
                    {
                        name = "Coroas Para Velório",
                        content = "ASSUNTO"
                    });

                    ToMandrill NovoToMandrill = new ToMandrill();
                    NovoToMandrill.email = EmailsEnviar;
                    NovoToMandrill.name = NomeEnviar;

                    List<ToMandrill> LstToMandril = new List<ToMandrill>();
                    LstToMandril.Add(NovoToMandrill);

                    List<varsMandrill> Lstvars = new List<varsMandrill>();
                    Lstvars.Add(new varsMandrill { name = "NOME", content = NomeEnviar });
                    Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = this.Numero });
                    Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });

                    #region ANEXOS


                    List<attachmentsMandrill> Anexos = new List<attachmentsMandrill>();

                    try
                    {
                        #region ANEXA NF

                        if (!string.IsNullOrEmpty(this.Faturamento.NFeLink))
                        {
                            using (WebClient client = new WebClient())
                            {
                                string StringPDF = "";

                                #region PREENCHE LINK NFEMAIS

                                if (this.Faturamento.NFeLink.Contains("nfemais"))
                                {
                                    var Index = this.Faturamento.NFeLink.IndexOf("key=");
                                    var IdDownload = this.Faturamento.NFeLink.Substring(Index + 4);
                                    StringPDF = "http://nfe2.nfemais.com.br/v3/painel/danfe.php?key=" + IdDownload;
                                }
                                else
                                {
                                    StringPDF = this.Faturamento.NFeLink;
                                }

                                #endregion

                                string id = Guid.NewGuid().ToString();
                                var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".pdf");

                                if (!string.IsNullOrEmpty(StringPDF))
                                {
                                    client.DownloadFile(StringPDF, Path);

                                    byte[] pdfBytes = File.ReadAllBytes(Path);
                                    string pdfBase64 = Convert.ToBase64String(pdfBytes);

                                    Anexos.Add(new attachmentsMandrill { type = "application/pdf", name = "NF " + this.Faturamento.NFeNumero, content = pdfBase64 });

                                    System.IO.File.Delete(Path);
                                }
                            }
                        }

                        #endregion
                    }
                    catch
                    {
                    }

                    try
                    {
                        #region ANEXA BOLETO

                        if (this.Faturamento.MeioPagamento == Pedido.MeiosPagamento.Boleto)
                        {
                            using (WebClient client = new WebClient())
                            {
                                string StringPDF = Core.Configuracoes.DOMINIO + "/boleto/empresa/" + Core.Criptografia.Encrypt(this.Faturamento.ID.ToString());

                                string id = Guid.NewGuid().ToString();
                                var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".pdf");

                                if (!string.IsNullOrEmpty(StringPDF))
                                {
                                    client.DownloadFile(StringPDF, Path);

                                    byte[] pdfBytes = File.ReadAllBytes(Path);
                                    string pdfBase64 = Convert.ToBase64String(pdfBytes);

                                    Anexos.Add(new attachmentsMandrill { type = "application/pdf", name = "BOLETO", content = pdfBase64 });

                                    System.IO.File.Delete(Path);
                                }
                            }
                        }

                        #endregion
                    }
                    catch
                    {
                    }

                    try
                    {
                        #region ANEXA XML

                        if (!string.IsNullOrEmpty(this.Faturamento.NFeLink))
                        {
                            using (WebClient client = new WebClient())
                            {
                                string StringXML = "";

                                #region PREENCHE LINK NFEMAIS

                                if (this.Faturamento.NFeLink.Contains("nfemais"))
                                {
                                    var Index = this.Faturamento.NFeLink.IndexOf("key=");
                                    var IdDownload = this.Faturamento.NFeLink.Substring(Index + 4);
                                    StringXML = "http://nfe2.nfemais.com.br/v3/painel/xml.php?key=" + IdDownload;
                                }

                                #endregion

                                string id = Guid.NewGuid().ToString();
                                var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".xml");

                                if (!string.IsNullOrEmpty(StringXML))
                                {
                                    client.DownloadFile(StringXML, Path);

                                    byte[] pdfBytes = File.ReadAllBytes(Path);
                                    string pdfBase64 = Convert.ToBase64String(pdfBytes);

                                    Anexos.Add(new attachmentsMandrill { type = "application/xml", name = "XML NF " + this.Faturamento.NFeNumero, content = pdfBase64 });

                                    System.IO.File.Delete(Path);
                                }
                            }
                        }

                        #endregion
                    }
                    catch 
                    {
                    }

                    

                    #endregion

                    novoEnvio.message = new MessagemMandrill()
                    {
                        subject = "ASSUNTO",
                        from_email = Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        from_name = "COROAS PARA VELÓRIO",
                        to = LstToMandril,
                        headers = new headersMandrill() { ReplyTo = Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS },
                        attachments = Anexos,
                        merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviar,
                            vars = Lstvars
                        }                        
                    }
                    };

                    var retorno = new Mandrill().CallMandrill(novoEnvio);

                    #region SALVAR LOG

                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new System.Data.Objects.ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Cobranca.ToString(), null, this.ID, reject_reason);
                    }

                    #endregion
                }
            }
           
        }
        public bool EnviarPedidoEmail()
        {
            var itenspedido = "";
            itenspedido += "<tr>";
            itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>";
            itenspedido += "<img src='" + Core.Configuracoes.DOMINIO + "/content/Produtos/" + this.ProdutoTamanho.ProdutoID + "/thumb/" + this.ProdutoTamanho.Produto.Foto + "' width='75'/>";
            itenspedido += "</td>";

            if (this.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
            {
                if (!String.IsNullOrEmpty(this.DescricaoProduto))
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + this.DescricaoProduto.Replace("\n", "<br>") + "</td>";
                else
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>Produto Personalizado</td>";
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Tamanho.Nome + "</td>";
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>-</td>";
            }
            else
            {
                itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Produto.Nome + "</td>";
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Tamanho.Nome + "</td>";
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.Valor.ToString("C2") + "</td>";
            }
            itenspedido += "</tr>";

            //ENVIAR EMAIL
            var html = File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/pedido-recebido.htm"));

            html = html.Replace("[URL]", Core.Configuracoes.CDN_IMG);
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial + (!string.IsNullOrEmpty(this.Empresa.NomeFantasia) ? " (" + this.Empresa.NomeFantasia + ")" : ""));
            html = html.Replace("[HOMENAGEADO]", this.PessoaHomenageada);

            if (LocalID != null || LocalID > 0)
            {
                html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper());
            }
            else
            {
                html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega);
            }

            html = html.Replace("[STATUSPEDIDO]", Helpers.EnumHelper.GetDescription(this.Status));
            html = html.Replace("[MENSAGEM]", this.Mensagem);
            if (this.Observacoes.Contains("Horário de Entrega:"))
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + this.Observacoes);
            }
            else
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " às " + this.DataSolicitada.ToString("HH:mm"));
            }
            html = html.Replace("[CIDADEENTREGA]", this.Cidade.NomeCompleto);
            html = html.Replace("[NOME]", this.Colaborador.Nome);
            html = html.Replace("[NUMEROPEDIDO]", this.ID.ToString());
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy HH:mm"));
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial);
            html = html.Replace("[VALORTOTAL]", this.Valor.ToString("C2"));
            html = html.Replace("[ITENSPEDIDO]", itenspedido);

            if (Core.Funcoes.ValidaEmail(this.Colaborador.Email.Trim()))
                Core.Funcoes.EnviaEmail
                    (
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        this.Colaborador.Email,
                        this.Colaborador.Nome,
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        "",
                        "[LAÇOS CORPORATIVOS] - Novo pedido #" + this.ID + " foi recebido.",
                        html,
                        true
                    );

            foreach (var colaborador in this.Empresa.Colaboradors.Where(c => c.RecebeEmailPedido && c.Tipo == Colaborador.Tipos.Master && c.Email != this.Colaborador.Email))
            {
                if (Core.Funcoes.ValidaEmail(colaborador.Email.Trim()))
                    Core.Funcoes.EnviaEmail
                        (
                            Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                            "Laços Corporativos",
                            colaborador.Email,
                            colaborador.Nome,
                            Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                            "Laços Corporativos",
                            "",
                            "[LAÇOS CORPORATIVOS] - O pedido do colaborador " + this.Colaborador.Nome + " #" + this.ID + " foi recebido com sucesso!",
                            html,
                            true
                        );
            }

            Core.Funcoes.EnviaEmail
                (
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "Laços Corporativos",
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "Atendimento",
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "Laços Corporativos",
                    "",
                    "[LAÇOS CORPORATIVOS] - Novo pedido #" + this.ID + " foi recebido.",
                    html,
                    true
                );

            return true;

        }

        public bool EnviarPedidoEmailSolicitante()
        {
            var itenspedido = "";
            itenspedido += "<tr>";
            itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>";
            itenspedido += "<img src='" + Core.Configuracoes.DOMINIO + "/content/Produtos/" + this.ProdutoTamanho.ProdutoID + "/thumb/" + this.ProdutoTamanho.Produto.Foto + "' width='75'/>";
            itenspedido += "</td>";
            itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Produto.Nome + "</td>";
            itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.ProdutoTamanho.Tamanho.Nome + "</td>";
            itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + this.Valor.ToString("C2") + "</td>";
            itenspedido += "</tr>";

            //ENVIAR EMAIL
            var html = File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/pedido-recebido.htm"));
            html = html.Replace("[URL]", Core.Configuracoes.CDN_IMG);
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial + (!string.IsNullOrEmpty(this.Empresa.NomeFantasia) ? " (" + this.Empresa.NomeFantasia + ")" : ""));
            html = html.Replace("[HOMENAGEADO]", this.PessoaHomenageada);
            if (string.IsNullOrEmpty(LocalEntrega) || LocalEntrega == "LOCAL NÃO CADASTRADO")
            {
                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega);
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega);
                    }
                    else
                    {
                        html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper());
                    }
                }
            }
            else
            {
                html = html.Replace("[LOCAL]", this.LocalEntrega + "<br/>" + this.ComplementoLocalEntrega);
            }

            html = html.Replace("[CIDADEENTREGA]", this.Cidade.NomeCompleto);
            html = html.Replace("[STATUSPEDIDO]", Helpers.EnumHelper.GetDescription(this.Status));
            html = html.Replace("[MENSAGEM]", this.Mensagem);
            if (this.Observacoes.Contains("Horário de Entrega:"))
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " das " + this.Observacoes.Substring(20, 26));
            }
            else
            {
                html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " às " + this.DataSolicitada.ToString("HH:mm"));
            }
            html = html.Replace("[NOME]", this.Colaborador.Nome);
            html = html.Replace("[NUMEROPEDIDO]", this.ID.ToString());
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy HH:mm"));
            html = html.Replace("[EMPRESA]", this.Empresa.RazaoSocial);
            html = html.Replace("[VALORTOTAL]", this.Valor.ToString("C2"));
            html = html.Replace("[ITENSPEDIDO]", itenspedido);


            if (Core.Funcoes.ValidaEmail(this.Colaborador.Email.Trim()))
                Core.Funcoes.EnviaEmail
                    (
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        this.Colaborador.Email,
                        this.Colaborador.Nome,
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "Laços Corporativos",
                        Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                        "[LAÇOS CORPORATIVOS] - Seu pedido #" + this.ID + " foi recebido com sucesso!",
                        html,
                        true
                    );

            return true;

        }

        public bool EnviarEmailFornecedor()
        {
            var itenspedido = "";
            itenspedido += "<tr>";
            itenspedido += "<td>";
            itenspedido += "<img src='[URL]/Produtos/" + this.ProdutoTamanho.ProdutoID + "/thumb/" + this.ProdutoTamanho.Produto.Foto + "' width='75'/>";
            itenspedido += "</td>";
            itenspedido += "<td><b>" + this.ProdutoTamanho.Produto.Nome + "</b><br><b>" + this.ProdutoTamanho.Tamanho.NomeCompleto + "</b><br>" + this.ProdutoTamanho.Produto.Descricao + "</td>";
            itenspedido += "<td style='text-align: center;'>" + (this.RepasseAtual.ValorRepasse > 0 ? this.RepasseAtual.ValorRepasse.ToString("C2") : "A confirmar") + "</td>";
            itenspedido += "</tr>";
            itenspedido += "<tr>";
            itenspedido += "<td>";
            itenspedido += "<font style='color: #757575; font-weight: bold;'>Frase :</font>";
            itenspedido += "</td>";
            itenspedido += "<td colspan='2'>";
            itenspedido += this.Mensagem;
            itenspedido += "</td>";
            itenspedido += "</tr>";
            itenspedido += "<tr>";
            itenspedido += "<td colspan='3' style='border-bottom: 1px solid #ecebe9;'>&nbsp;</td>";
            itenspedido += "</tr>";

            //ENVIAR EMAIL
            var html = File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/fornecedor/fornecedor.htm"));
            html = html.Replace("[SITE]", "Laços Corporativos");
            html = html.Replace("[NOMEPARA]", this.Fornecedor.Nome);

            if (string.IsNullOrEmpty(LocalEntrega) || LocalEntrega == "LOCAL NÃO CADASTRADO")
            {
                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
                    }
                    else
                    {
                        html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper() + " " + this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
                    }
                }
            }
            else
            {
                html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper() + " " + this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
            }

            html = html.Replace("[FALECIDO]", this.PessoaHomenageada);
            html = html.Replace("[ITENSPEDIDO]", itenspedido);
            html = html.Replace("[DATAHORA]", this.DataSolicitada.ToString("dd/MM/yyyy HH:mm"));
            html = html.Replace("[URL]", Core.Configuracoes.CDN_IMG);

            if (Domain.Core.Funcoes.ValidaEmail(this.Fornecedor.EmailContato.Trim()))
                return Domain.Core.Funcoes.EnviaEmail(
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "GRUPO LAÇOS FLORES",
                    this.Fornecedor.EmailContato,
                    this.Fornecedor.Nome,
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "GRUPO LAÇOS FLORES",
                    Core.Configuracoes.EMAIL_COPIA,
                    "[GRUPO LAÇOS FLORES] - Novo pedido para entrega",
                    html,
                    true
                );
            else
                return false;
        }

        public string FotoEntrega
        {
            get
            {
                if (Domain.Core.Configuracoes.HOMOLOGACAO)
                {
                    return "/content/foto_admin_homolog_solution.jpg";
                }
                else
                {
                    var file = new FileInfo(System.Web.Hosting.HostingEnvironment.MapPath("/content/pedidos/" + this.ID + "/" + this.ID + ".jpg"));
                    if (file.Exists)
                        return "/content/pedidos/" + this.ID + "/" + this.ID + ".jpg";
                    else
                        return "";
                }
                
            }
        }

        #region IPedido
        public string Numero
        {
            get { return ID.ToString(); }
            set { ID = int.Parse(value); }
        }
        public OrigemPedido TipoPedido { get { return OrigemPedido.Empresa; } }
        public bool Cancelado()
        {
            return StatusCancelamentoID.HasValue;
        }

        public Fornecedor FornecedorAutomatico
        {
            get
            {
                Fornecedor ForncedorReturn = null;
                
                if (this.Local != null &&  this.Local.FornecedorLocals.Count > 0)
                {
                    var List = this.Local.FornecedorLocals.Where(r => r.Ordem == 1).ToList();
                    if (List.Count > 0)
                    {
                        ForncedorReturn = new Fornecedor();
                        ForncedorReturn = List.First().Fornecedor;

                        return ForncedorReturn;
                    }
                }

                //if (ForncedorReturn == null)
                //{
                //    var cidadeRepository = new PersistentRepository<Cidade>(new COROASEntities());
                //    var Locais = cidadeRepository.Get(this.CidadeID).Locals.Where(r => r.TipoID == 100).ToList();

                //    if (Locais.Count > 0)
                //    {
                //        if (Locais.First().Fornecedor != null)
                //        {
                //            ForncedorReturn = new Fornecedor();
                //            return Locais.First().Fornecedor;
                //        }
                //    }
                //}

                return null;
            }
        }

        public bool FornecedorRecusouEntrega
        {
            get
            {
                return AdministradorPedidoes.Where(r => r.AcaoID == 22).Any();
            }

        }

        public string LinkPedido
        {
            get
            {
                return "/PedidosEmpresa/Visualizar/" + this.ID;
            }

        }
        public string LocalDeEntregaDetalhado { get; set; }
        public List<LogMandrill> LstLogsMandrill { get; set; }
        public List<PedidoEmpresa> LstPedidosDaFatura { get; set; }
        public int BoletosVencidosCorp { get; set; }
        public int FaturasVencidas { get; set; }
        public int BoletosVencidos { get; set; }
        public string NomeCliente
        {
            get
            {
                return this.Colaborador.Nome;
            }
        }
        public string RazaoSocial
        {
            get
            {
                return this.Empresa.RazaoSocial;
            }
        }
        public string EmailCliente
        {
            get
            {
                return this.Colaborador.Email;
            }
        }

        public string NomeEmpresa
        {
            get
            {
                return this.Empresa.RazaoSocial;
            }
        }

        public int IDEmpresa
        {
            get
            {
                return this.Empresa.ID;
            }
        }
        public int MeioPgtID
        {
            get
            {
                return this.Faturamento.MeioPagamentoID;
            }
        }

        public bool PossuiFotoEntrega
        {
            get
            {
                if(this.FotoEntrega != null && this.FotoEntrega != "")
                    return true;

                return false;
            }
        }
        public bool PossuiFotoDeEntregaParaValidar
        {
            get
            {
                if (this.StatusFotoID == 2)
                    return true;

                return false;
            }
        }

        public bool FotosDeEntregaValidadas
        {
            get
            {
                if (this.StatusFotoID == 3)
                    return true;

                return false;
            }
        }

        #endregion
    }
}
