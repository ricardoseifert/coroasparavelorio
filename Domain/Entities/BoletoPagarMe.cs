﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class BoletoPagarMe : IPersistentEntity
    {

        public enum Comandos
        {
            [Description("Adicionar à Remessa")]
            CriarRemessa = 1,
            [Description("Cancelar")]
            Cancelar = 2,
            [Description("Modificar Vencimento")]
            ModificarVencimento = 6,
            [Description("Protestar")]
            Protestar = 9,
            [Description("Modificar Dados Cadastrais")]
            ModificarDados = 31
        }

        public enum TodosStatusPagamento
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("Aguardando Pagamento")]
            AguardandoPagamento = 1,
            [Description("Pagamento Efetuado")]
            Pago = 2,
            [Description("Vencido")]
            Vencido = 3,
            [Description("Cancelado")]
            Cancelado = 4,
            [Description("Protestado")]
            Protestado = 5,
            [Description("Prejuízo")]
            Prejuizo = 6
        }

        public enum TodosStatusProcessamento
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("Pendente/Novo")]
            Criado = 1,
            [Description("Em processamento")]
            Processando = 2,
            [Description("Processado")]
            Processado = 3
        }

        public enum TodosTipos
        {
            [Description("PF")]
            PF = 1,
            [Description("PJ")]
            PJ = 2
        }

        public enum TodosRetornos
        {
            INDEFINIDA = 0,
            [Description("ENTRADA CONFIRMADA")]
            ENTRADA_CONFIRMADA = 2,
            [Description("ENTRADA REJEITADA")]
            ENTRADA_REJEITADA = 3,
            [Description("ALTERAÇÃO DE DADOS - NOVA ENTRADA")]
            ALTERACAO_DE_DADOS_NOVA_ENTRADA = 4,
            [Description("ALTERAÇÃO DE DADOS – BAIXA")]
            ALTERACAO_DE_DADOS_BAIXA = 5,
            [Description("LIQUIDAÇÃO NORMAL")]
            LIQUIDACAO_NORMAL = 6,
            [Description("LIQUIDAÇÃO PARCIAL – COBRANÇA INTELIGENTE (B2B)")]
            LIQUIDACAO_PARCIAL_COBRANÇA_INTELIGENTE_B2B = 7,
            [Description("LIQUIDAÇÃO EM CARTÓRIO")]
            LIQUIDACAO_EM_CARTORIO = 8,
            [Description("BAIXA SIMPLES")]
            BAIXA_SIMPLES = 9,
            [Description("BAIXA POR TER SIDO LIQUIDADO")]
            BAIXA_POR_TER_SIDO_LIQUIDADO = 10,
            [Description("EM SER")]
            EM_SER = 11,
            [Description("ABATIMENTO CONCEDIDO")]
            ABATIMENTO_CONCEDIDO = 12,
            [Description("ABATIMENTO CANCELADO")]
            ABATIMENTO_CANCELADO = 13,
            [Description("VENCIMENTO ALTERADO")]
            VENCIMENTO_ALTERADO = 14,
            [Description("BAIXAS REJEITADAS")]
            BAIXAS_REJEITADAS = 15,
            [Description("INSTRUÇÕES REJEITADAS")]
            INSTRUCOES_REJEITADAS = 16,
            [Description("ALTERAÇÃO DE DADOS REJEITADOS")]
            ALTERACAO_DE_DADOS_REJEITADOS = 17,
            [Description("COBRANÇA CONTRATUAL - INSTRUÇÕES/ALTERAÇÕES REJEITADAS/PENDENTES")]
            COBRANÇA_CONTRATUAL_INSTRUCOES_ALTERACOES_REJEITADAS_PENDENTES = 18,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE PROTESTO")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_PROTESTO = 19,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE SUSTAÇÃO DE PROTESTO /TARIFA")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_SUSTACAO_DE_PROTESTO_TARIFA = 20,
            [Description("CONFIRMA RECEBIMENTO DE INSTRUÇÃO DE NÃO PROTESTAR")]
            CONFIRMA_RECEBIMENTO_DE_INSTRUCAO_DE_NAO_PROTESTAR = 21,
            [Description("OCORRÊNCIA NÃO IDENTIFICADA (TESTES)")]
            OCORRENCIA_NAO_IDENTIFICADA_TESTES = 22,
            [Description("TÍTULO ENVIADO A CARTÓRIO/TARIFA")]
            TITULO_ENVIADO_A_CARTORIO_TARIFA = 23,
            [Description("INSTRUÇÃO DE PROTESTO REJEITADA / SUSTADA / PENDENTE")]
            INSTRUCAO_DE_PROTESTO_REJEITADA_SUSTADA_PENDENTE = 24,
            [Description("ALEGAÇÕES DO SACADO")]
            ALEGACOES_DO_SACADO = 25,
            [Description("TARIFA DE AVISO DE COBRANÇA")]
            TARIFA_DE_AVISO_DE_COBRANCA = 26,
            [Description("TARIFA DE EXTRATO POSIÇÃO (B40X)")]
            TARIFA_DE_EXTRATO_POSICAO_B40X = 27,
            [Description("TARIFA DE RELAÇÃO DAS LIQUIDAÇÕES")]
            TARIFA_DE_RELACAO_DAS_LIQUIDACOES = 28,
            [Description("TARIFA DE MANUTENÇÃO DE TÍTULOS VENCIDOS")]
            TARIFA_DE_MANUTENCAO_DE_TITULOS_VENCIDOS = 29,
            [Description("DÉBITO MENSAL DE TARIFAS")]
            DEBITO_MENSAL_DE_TARIFAS = 30,
            [Description("BAIXA POR TER SIDO PROTESTADO")]
            BAIXA_POR_TER_SIDO_PROTESTADO = 32,
            [Description("CUSTAS DE PROTESTO")]
            CUSTAS_DE_PROTESTO = 33,
            [Description("CUSTAS DE SUSTAÇÃO")]
            CUSTAS_DE_SUSTACAO = 34,
            [Description("CUSTAS DE CARTÓRIO DISTRIBUIDOR")]
            CUSTAS_DE_CARTORIO_DISTRIBUIDOR = 35,
            [Description("CUSTAS DE EDITAL")]
            CUSTAS_DE_EDITAL = 36,
            [Description("TARIFA DE EMISSÃO DE BOLETO/TARIFA DE ENVIO DE DUPLICATA")]
            TARIFA_DE_EMISSAO_DE_BOLETO_TARIFA_DE_ENVIO_DE_DUPLICATA = 37,
            [Description("TARIFA DE INSTRUÇÃO")]
            TARIFA_DE_INSTRUCAO = 38,
            [Description("TARIFA DE OCORRÊNCIAS")]
            TARIFA_DE_OCORRENCIAS = 39,
            [Description("TARIFA MENSAL DE EMISSÃO DE BOLETO/TARIFA MENSAL DE ENVIO DE DUPLICATA")]
            TARIFA_MENSAL_DE_EMISSAO_DE_BOLETO_TARIFA_MENSAL_DE_ENVIO_DE_DUPLICATA = 40,
            [Description("DÉBITO MENSAL DE TARIFAS – EXTRATO DE POSIÇÃO (B4EP/B4OX)")]
            DEBITO_MENSAL_DE_TARIFAS_EXTRATO_DE_POSICAO_B4EP_B4OX = 41,
            [Description("DÉBITO MENSAL DE TARIFAS – OUTRAS INSTRUÇÕES")]
            DEBITO_MENSAL_DE_TARIFAS_OUTRAS_INSTRUCOES = 42,
            [Description("DÉBITO MENSAL DE TARIFAS – MANUTENÇÃO DE TÍTULOS VENCIDOS")]
            DEBITO_MENSAL_DE_TARIFAS_MANUTENCAO_DE_TITULOS_VENCIDOS = 43,
            [Description("DÉBITO MENSAL DE TARIFAS – OUTRAS OCORRÊNCIAS")]
            DEBITO_MENSAL_DE_TARIFAS_OUTRAS_OCORRENCIAS = 44,
            [Description("DÉBITO MENSAL DE TARIFAS – PROTESTO")]
            DEBITO_MENSAL_DE_TARIFAS_PROTESTO = 45,
            [Description("DÉBITO MENSAL DE TARIFAS – SUSTAÇÃO DE PROTESTO")]
            DEBITO_MENSAL_DE_TARIFAS_SUSTACAO_DE_PROTESTO = 46,
            [Description("BAIXA COM TRANSFERÊNCIA PARA DESCONTO")]
            BAIXA_COM_TRANSFERENCIA_PARA_DESCONTO = 47,
            [Description("CUSTAS DE SUSTAÇÃO JUDICIAL")]
            CUSTAS_DE_SUSTACAO_JUDICIAL = 48,
            [Description("TARIFA MENSAL REF A ENTRADAS BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_REF_A_ENTRADAS_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 51,
            [Description("TARIFA MENSAL BAIXAS NA CARTEIRA")]
            TARIFA_MENSAL_BAIXAS_NA_CARTEIRA = 52,
            [Description("TARIFA MENSAL BAIXAS EM BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_BAIXAS_EM_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 53,
            [Description("TARIFA MENSAL DE LIQUIDAÇÕES NA CARTEIRA")]
            TARIFA_MENSAL_DE_LIQUIDACOES_NA_CARTEIRA = 54,
            [Description("TARIFA MENSAL DE LIQUIDAÇÕES EM BANCOS CORRESPONDENTES NA CARTEIRA")]
            TARIFA_MENSAL_DE_LIQUIDACOES_EM_BANCOS_CORRESPONDENTES_NA_CARTEIRA = 55,
            [Description("CUSTAS DE IRREGULARIDADE")]
            CUSTAS_DE_IRREGULARIDADE = 56,
            [Description("INSTRUÇÃO CANCELADA")]
            INSTRUCAO_CANCELADA = 57,
            [Description("BAIXA POR CRÉDITO EM C/C ATRAVÉS DO SISPAG")]
            BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG = 59,
            [Description("ENTRADA REJEITADA CARNÊ")]
            ENTRADA_REJEITADA_CARNE = 60,
            [Description("TARIFA EMISSÃO AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)")]
            TARIFA_EMISSAO_AVISO_DE_MOVIMENTACAO_DE_TITULOS_2154 = 61,
            [Description("DÉBITO MENSAL DE TARIFA - AVISO DE MOVIMENTAÇÃO DE TÍTULOS (2154)")]
            DEBITO_MENSAL_DE_TARIFA_AVISO_DE_MOVIMENTACAO_DE_TITULOS_2154 = 62,
            [Description("TÍTULO SUSTADO JUDICIALMENTE")]
            TITULO_SUSTADO_JUDICIALMENTE = 63,
            [Description("ENTRADA CONFIRMADA COM RATEIO DE CRÉDITO")]
            ENTRADA_CONFIRMADA_COM_RATEIO_DE_CREDITO = 64,
            [Description("CHEQUE DEVOLVIDO")]
            CHEQUE_DEVOLVIDO = 69,
            [Description("ENTRADA REGISTRADA, AGUARDANDO AVALIAÇÃO")]
            ENTRADA_REGISTRADA_AGUARDANDO_AVALIACAO = 71,
            [Description("BAIXA POR CRÉDITO EM C/C ATRAVÉS DO SISPAG SEM TÍTULO CORRESPONDENTE")]
            BAIXA_POR_CREDITO_EM_CC_ATRAVES_DO_SISPAG_SEM_TITULO_CORRESPONDENTE = 72,
            [Description("CONFIRMAÇÃO DE ENTRADA NA COBRANÇA SIMPLES – ENTRADA NÃO ACEITA NA COBRANÇA CONTRATUAL")]
            CONFIRMACAO_DE_ENTRADA_NA_COBRANCA_SIMPLES_ENTRADA_NAO_ACEITA_NA_COBRANCA_CONTRATUAL = 73,
            [Description("CHEQUE COMPENSADO")]
            CHEQUE_COMPENSADO = 76
        }

        public TodosStatusPagamento StatusPagamento
        {
            get { return (TodosStatusPagamento)this.StatusPagamentoID; }
            set { this.StatusPagamentoID = (int)value; }
        }

        public TodosStatusProcessamento StatusProcessamento
        {
            get { return (TodosStatusProcessamento)this.StatusProcessamentoID; }
            set { this.StatusProcessamentoID = (int)value; }
        }
        public string Erro { get; set; }
    }
}
