﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Colaborador : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Master")]
            Master = 1,
            [Description("Usuário")]
            Usuario = 2
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public string PrimeiroNome
        {
            get { return this.Nome.Split(' ')[0]; }
        }

        public bool EnviarEmailCadastro()
        {
            string html = "<table><thead><tr><td><h1>Novo Cadastro no Site Laços Corporativos</h1></td></tr></thead><tbody><tr><td><b>Nome Fantasia</b></td><td><b>" + this.Empresa.NomeFantasia + "</b></td></tr><tr><td><b>Cnpj</b></td><td><b>" + this.Empresa.CNPJ + "</b></td></tr><tr><td><b>Nome Colaborador</b></td><td><b>" + this.Nome + "</b></td></tr><tr><td><b>Email Colaborador</b></td><td><b>" + this.Email + "</b></td></tr><tr><td><b>Telefone Colaborador</b></td><td><b>" + this.Telefone + "</b></td></tr><tr><td><b>Celular Colaborador</b></td><td><b>" + this.TelefoneCelular + "</b></td></tr><tr><td><b>Data</b></td><td><b>" + DateTime.Now + "</b></td></tr></tbody></table>";
            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Corporativos", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "Atendimento", "", "", "", "Novo Cadastro pelo Site", html, true);
            return true;
        }
    }
}
