﻿using System;
using System.Collections.Generic;

namespace Domain.Entities {
    public interface IPedido {

        #region Requeridos
        int ID { get; set; }
        TodosStatusEntrega StatusEntrega { get; set; }
        bool FornecedorEmProcessamento { get; set; }
        int? FornecedorID { get; set; }
        DateTime DataSolicitada { get; set; }
        string Numero { get; set; }
        TodosStatusCancelamento StatusCancelamento { get; set; }
        DateTime? DataEntrega { get; set; }
        OrigemPedido TipoPedido { get; }
        bool Cancelado();
        DateTime DataCriacao { get; set; }
        int CidadeID { get; set; }
        int EstadoID { get; set; }
        int StatusEntregaID { get; set; }
        Guid Codigo { get; set; }
        DateTime DataAtualizacao { get; set; }
        DateTime? DataCobranca { get; }
        List<string> NomeProdutos { get; }        
        bool JaSaiuEntrega { get; }
        int QtdFornecedores { get; }
        int RepasseId { get; set; }
        int? AdministradorPedidoID { get; }
        string AdministradorNome { get;  }
        List<int> CategoriaDosProdutos { get; }
        Fornecedor FornecedorAutomatico { get; }
        bool FornecedorRecusouEntrega { get; }
        string LinkPedido { get; }
        int FaturasVencidas { get; set; }
        int BoletosVencidos { get; set; }
        string LocalDeEntregaDetalhado { get; set; }
        List<LogMandrill> LstLogsMandrill { get; set; }
        List<PedidoEmpresa> LstPedidosDaFatura { get; set; }
        string NomeCliente { get; }
        string RazaoSocial { get; }
        string EmailCliente { get; }
        string NomeEmpresa { get; }
        int IDEmpresa { get; }
        int MeioPgtID { get; }
        bool PossuiFotoEntrega { get; }
        bool PossuiFotoDeEntregaParaValidar { get; }
        bool FotosDeEntregaValidadas { get; }
        string LatitudeEntregaReturn { get; }
        string LongitudeEntregaReturn { get; }
        DateTime? DtUpdateLocEntregaReturn { get; }
        string NomeFantasia { get; }
        string NomeDoProduto { get; }
        bool ProdutoKitBB { get; }

        #endregion

    }

    
}