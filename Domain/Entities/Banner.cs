﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Banner : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Imagem em JPG")]
            Imagem = 1,
            [Description("Flash")]
            Flash = 2
        }

        public enum Posicoes
        {
            [Description("Superior na Home")]
            Superior = 1,
            [Description("Rodapé em todas as páginas")]
            Inferior = 2
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public Posicoes Posicao
        {
            get { return (Posicoes)this.PosicaoID; }
            set { this.PosicaoID = (int)value; }
        }

    }
}
