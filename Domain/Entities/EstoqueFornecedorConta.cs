﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class EstoqueFornecedorConta : IPersistentEntity
    {        
       public string DescConta
        {
            get
            {
                if (!string.IsNullOrEmpty(this.AgenciaDigito))
                {
                    return this.EstoqueFornecedorTipoConta.Sigla + " " + this.Agencia + "-" + this.AgenciaDigito + " / " + this.Conta + "-" + this.ContaDigito;
                }
                else
                {
                    return this.EstoqueFornecedorTipoConta.Sigla + " " + this.Agencia + " / " + this.Conta + "-" + this.ContaDigito;
                }
                
            }
        }
    }
}
