﻿using DomainExtensions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public partial class LogMandrill : IPersistentEntity
    {
        public enum TipoEmail
        {
            [Description("Pedido")]
            Pedido = 1,
            [Description("Primeiro Boleto")]
            PrimeiroBoleto = 2,
            [Description("Entrega")]
            Entrega =3,
            [Description("NFE")]
            NFE = 4,
            [Description("Cancelar NFE")]
            CancelarNFE = 5,
            [Description("Cobrança")]
            Cobranca = 6,
            [Description("Cobrança Robo Primeira Vez")]
            CobrancaRoboPrimeiraVez = 7,
            [Description("Renegociação Robo")]
            RenegociacaoRobro = 8,
            [Description("Pedido Em Produção Robo")]
            PedidoEmProdução = 9,
            [Description("Foto")]
            Foto = 3,
            [Description("Outros")]
            Outros = 99
        }
    }
}
