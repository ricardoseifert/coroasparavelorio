﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using YamlDotNet.Serialization;
using System.Net;
using Domain.Core;
using System.Globalization;

namespace Domain.Entities
{
    public partial class DadosNFEProduto
    {
        public string numero_item { get; set; }
        public string codigo_produto { get; set; }
        public string descricao { get; set; }
        public string codigo_ncm { get; set; }
        public string cfop { get; set; }
        public string unidade_comercial { get; set; }
        public string quantidade_comercial { get; set; }
        public string valor_unitario_comercial { get; set; }
        public string unidade_tributavel { get; set; }
        public string quantidade_tributavel { get; set; }
        public string valor_unitario_tributavel { get; set; }
        public string valor_bruto { get; set; }
        public string valor_frete { get; set; }

        public string valor_desconto { get; set; }
        public string inclui_no_total { get; set; }
        public string icms_origem { get; set; }
        public string icms_situacao_tributaria { get; set; }
        public string icms_aliquota { get; set; }
        public string icms_valor { get; set; }
        public string pis_situacao_tributaria { get; set; }
        public string cofins_situacao_tributaria { get; set; }
    }

    public partial class DadosNFEVolume
    {
        public string quantidade { get; set; }
    }

    public partial class DadosNFE
    {
        private List<DadosNFEProduto> lisItems = new List<DadosNFEProduto>();
        private List<DadosNFEVolume> lstVolumes = new List<DadosNFEVolume>();
        public string natureza_operacao { get; set; }
        public string forma_pagamento { get; set; }
        public string data_emissao { get; set; }
        public string data_entrada_saida { get; set; }
        public string tipo_documento { get; set; }
        public string finalidade_emissao { get; set; }
        public string cnpj_emitente { get; set; }
        public string nome_emitente { get; set; }
        public string logradouro_emitente { get; set; }
        public string numero_emitente { get; set; }
        public string bairro_emitente { get; set; }
        public string municipio_emitente { get; set; }
        public string uf_emitente { get; set; }
        public string cep_emitente { get; set; }
        public string telefone_emitente { get; set; }
        public string inscricao_estadual_emitente { get; set; }
        public string regime_tributario_emitente { get; set; }

        public string cpf_destinatario { get; set; }
        public string cnpj_destinatario { get; set; }
        public string inscricao_estadual_destinatario { get; set; }
        public string indicador_inscricao_estadual_destinatario { get; set; }
        public string nome_destinatario { get; set; }
        public string logradouro_destinatario { get; set; }
        public string numero_destinatario { get; set; }
        public string bairro_destinatario { get; set; }
        public string municipio_destinatario { get; set; }
        public string codigo_municipio_destinatario { get; set; }
        public string uf_destinatario { get; set; }
        public string local_destino { get; set; }
        public string cep_destinatario { get; set; }
        public string telefone_destinatario { get; set; }
        public string pais_destinatario { get; set; }
        public string icms_base_calculo { get; set; }
        public string icms_valor_total { get; set; }
        public string icms_base_calculo_st { get; set; }
        public string icms_valor_total_st { get; set; }
        public string valor_produtos { get; set; }
        public string valor_frete { get; set; }
        public string valor_seguro { get; set; }
        public string valor_desconto { get; set; }
        public string valor_total_ii { get; set; }
        public string valor_ipi { get; set; }
        public string valor_outras_despesas { get; set; }
        public string valor_pis { get; set; }
        public string valor_cofins { get; set; }
        public string valor_total { get; set; }
        public string modalidade_frete { get; set; }
        public string nome_transportador { get; set; }
        public string cnpj_transportador { get; set; }
        public string cpf_transportador { get; set; }
        public string endereco_transportador { get; set; }
        public string municipio_transportador { get; set; }
        public string uf_transportador { get; set; }
        public string inscricao_estadual_transportador { get; set; }
        public string informacoes_adicionais_contribuinte { get; set; }

        public List<DadosNFEVolume> volumes
        {
            get { return lstVolumes; }
            set { lstVolumes = value; }
        }

        public List<DadosNFEProduto> items
        {
            get { return lisItems; }
            set { lisItems = value; }
        }
    }

    public partial class RetornoResult
    {
        public string IdPedido { get; set; }
        public OrigemPedido TipoPedido { get; set; }
        public string LinkNF { get; set; }
        public string Msg { get; set; }
    }

    public partial class NFE
    {
        public enum TodosStatusProcessamento
        {
            [Description("erro_autorizacao")]
            erro_autorizacao = 1,
            [Description("processando_autorizacao")]
            processando_autorizacao = 2,
            [Description("autorizado")]
            autorizado = 3,
            [Description("cancelado")]
            cancelado = 3
        }

        public enum TiposConsulta
        {
            emissao = 1,
            cancelamento = 2
        }

        public int PedidoCPV { get; set; }
        public int IdFat { get; set; }
        public int PedidoCORP { get; set; }
        public string CFOP { get; set; }
        public string mensagem_sefaz { get; set; }
        public string status_sefaz { get; set; }
        public TodosStatusProcessamento statusNF { get; set; }
        public string caminhoDanfe { get; set; }
        public string caminhoXML { get; set; }
        public string caminhoXMLCancelamento { get; set; }
        public string chaveNFe { get; set; }
        public string numeroNFe { get; set; }
        public string NrReferenciaNF { get; set; }

    }
}
