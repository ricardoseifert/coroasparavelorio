﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{ 
    public partial class EstoqueFornecedorTipoConta : IPersistentEntity
    {
        public Tipos Tipo
        {
            get { return (Tipos)this.ID; }
            set { this.ID = (int)value; }
        }

        public enum Tipos
        {
            [Description("CC")]
            Indefinido = 1,
            [Description("CP")]
            PessoaFisica = 2,
        }

    }
}
