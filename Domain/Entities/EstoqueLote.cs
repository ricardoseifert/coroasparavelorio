﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class EstoqueLote : IPersistentEntity
    {
        private IPersistentRepository<EstoqueLote> estoqueLoteRepository;

        public EstoqueLote()
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            estoqueLoteRepository = new PersistentRepository<EstoqueLote>(new ObjectContext(conStr));
        }

        public decimal ValorTotal
        {
            get
            {
                return this.EstoqueLoteItems.Sum(r => r.ValorUnitario * r.Qtd);
            }
        }

      

      


        public string CriarNumero()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(5);
                for (int indice = 0; indice < 5; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                string NumeroFinal = numero.ToString();
                NumeroFinal = numero.ToString() + "-" + DateTime.Now.ToString("MM") + "-" + DateTime.Now.ToString("yy");

                if (!NumeroExiste(NumeroFinal))
                {
                    return NumeroFinal;
                }

            } while (true);
            return "";
        }

        public bool NumeroExiste(string numero)
        {
            return estoqueLoteRepository.GetByExpression(p => p.Numero == numero).Any();
        }
    }
}
