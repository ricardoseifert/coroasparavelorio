﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Service;

namespace Domain.Entities
{
    public class Carrinho
    {

        public List<CarrinhoItem> Itens;

        public Cupom Cupom;

        public bool Vazio
        {
            get { return Itens.Count == 0; }
        }

        public decimal Subtotal
        {
            get { return Itens.Sum(i => i.Valor); }
        }

        private decimal _Desconto;
        public decimal Desconto
        {
            get { return _Desconto; }
        }

        public decimal Total
        {
            get
            {
                var total = this.Subtotal - this.Desconto;
                return total > 0 ? total : 0;
            }
        }

        public Carrinho()
        {
            _Desconto = 0;
            Itens = new List<CarrinhoItem>();
        }

        public void Limpar()
        {
            _Desconto = 0;
            Itens = new List<CarrinhoItem>();
        }

        public void Adicionar(ProdutoTamanho produtoTamanho, string descricao, string mensagem, string observacoes)
        {
            Itens.Add(new CarrinhoItem()
            {
                Descricao = descricao,
                ID = Itens.Count,
                Mensagem = mensagem,
                Observacoes = observacoes,
                ProdutoTamanho = produtoTamanho,
                Valor = produtoTamanho.Preco
            });
            _Desconto = DescontoService.Calcular(this);
        }

        public void Remover(int itemID)
        {
            var item = Itens.FirstOrDefault(i => i.ID == itemID);
            if (item != null)
            {
                Itens.Where(i => i.ID > item.ID).ToList().ForEach(i => i.ID--);
                Itens.Remove(item);
                _Desconto = DescontoService.Calcular(this);
            }
        }

        public void Atualizar(int itemID, string descricao, string mensagem, string observacoes)
        {
            var item = Itens.FirstOrDefault(i => i.ID == itemID);
            if (item != null)
            {
                item.Descricao = descricao;
                item.Mensagem = mensagem;
                item.Observacoes = observacoes;
            }
        }

        public void Atualizar(int itemID, string mensagem)
        {
            var item = Itens.FirstOrDefault(i => i.ID == itemID);
            if (item != null)
            {
                item.Mensagem = mensagem;
            }
        }

        public void Atualizar()
        {
            _Desconto = DescontoService.Calcular(this);
        }

    }
}
