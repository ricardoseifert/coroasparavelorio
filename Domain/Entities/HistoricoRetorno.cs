﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class HistoricoRetorno : IPersistentEntity
    {
        //private IPersistentRepository<PagamentoGenerico> pagamentoGenericoRepository;
        //private IPersistentRepository<Repasse> repasseRepository;

        //public HistoricoRetorno()
        //{
        //    var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
        //    var entityBuilder = new EntityConnectionStringBuilder();

        //    entityBuilder.Provider = "System.Data.SqlClient";
        //    entityBuilder.ProviderConnectionString = conStr;

        //    pagamentoGenericoRepository = new PersistentRepository<PagamentoGenerico>(new ObjectContext(conStr));
        //    repasseRepository = new PersistentRepository<Repasse>(new ObjectContext(conStr));
        //}

        //public PagamentoGenerico PagamentoGenerico {
        //    get {
        //        if(this.IdPagamentoGenerico != null)
        //        {
        //            var Retorno = pagamentoGenericoRepository.Get((int)this.IdPagamentoGenerico);
        //            return Retorno;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}
        //public Repasse Repasse
        //{
        //    get
        //    {
        //        if (this.IdRepasse != null)
        //        {
        //            var Retorno = repasseRepository.Get((int)this.IdRepasse);
        //            return Retorno;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        public enum TodosStatusProcessamento
        {
            [Description("Pago")]
            Pago = 1,
            [Description("Erro")]
            Erro = 2,
            [Description("Adicionado ao CNAB")]
            AdicionadoAoCNAB = 3,
            [Description("Removido do CNAB")]
            RemovidoAoCNAB = 4,
            [Description("Arquivo Rt Importado")]
            ArquivRetornoImportado = 5,
            [Description("Pagamento Efetuado")]
            PagamentoEfetuado = 6,
            [Description("Pagamento Devolvido")]
            PagamentoDevolvido = 7
        }

        public TodosStatusProcessamento StatusProcessamento
        {
            get { return (TodosStatusProcessamento)this.Codigo; }
            set { this.Codigo = (int)value; }
        }
    }
}
