﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Core;
using System.Data.EntityClient;
using System.Configuration;

namespace Domain.Entities
{
    public partial class Faturamento : IPersistentEntity
    {

        private IPersistentRepository<Faturamento> faturamentoRepository;
        private IPersistentRepository<PedidoEmpresa> peidoEmpresaRepository;
        private IPersistentRepository<Boleto> boletoRepository;
        private IPersistentRepository<Empresa>empresaRepository;
        private IPersistentRepository<Colaborador> colaboradorRepository;
        private PersistentRepository<Log> logRepository;
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;

        public Faturamento()
        {
        }

        public TodosStatusNFs StatusNF
        {
            get { return (TodosStatusNFs)this.StatusNfID; }
            set { this.StatusNfID = (int)value; }
        }
        public enum TodosStatusNFs
        {
            [Description("Não Emitida")]
            NaoEmitida = 1,
            [Description("Processando")]
            Processando = 2,
            [Description("Emitida")]
            Emitida = 3
        }

        public enum TodosStatus
        { 
            [Description("Aguardando Pagamento")]
            AguardandoPagamento = 1,
            [Description("Pagamento Efetuado")]
            Paga = 2,
            [Description("Cancelada")]
            Cancelada = 3,
            [Description("Estornado")]
            Estornado = 4,
            [Description("Prejuízo")]
            Prejuizo = 5,
            [Description("Protestado")]
            Protestado = 6
        }

        public TodosStatus Status
        {
            get { return (TodosStatus)this.StatusID; }
            set { this.StatusID = (int)value; }
        }

        public Domain.Entities.Pedido.TodosStatusPagamento StatusPagamento
        {
            get { return (Domain.Entities.Pedido.TodosStatusPagamento)this.StatusPagamentoID; }
            set { this.StatusPagamentoID = (int)value; }
        }

        public Domain.Entities.Pedido.MeiosPagamento MeioPagamento
        {
            get { return (Domain.Entities.Pedido.MeiosPagamento)this.MeioPagamentoID; }
            set { this.MeioPagamentoID = (int)value; }
        }

        public Domain.Entities.Pedido.FormasPagamento FormaPagamento
        {
            get { return (Domain.Entities.Pedido.FormasPagamento)this.FormaPagamentoID; }
            set { this.FormaPagamentoID = (int)value; }
        }

        public Boleto BoletoAtual
        {
            get
            {
                return this.Boletoes.FirstOrDefault(b => b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento || b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.Vencido);
            }
        }

        public Boleto Boleto
        {
            get
            {
                return this.Boletoes.FirstOrDefault(b => b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.AguardandoPagamento || b.StatusPagamentoID == (int)Boleto.TodosStatusPagamento.Vencido);
            }
        }
        public void EmitirNF()
        {
        }
        public Faturamento Faturar(PedidoEmpresa pedidoEmp)
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            faturamentoRepository = new PersistentRepository<Faturamento>(new ObjectContext(conStr));
            peidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(new ObjectContext(conStr));
            boletoRepository = new PersistentRepository<Boleto>(new ObjectContext(conStr));

            var fatura = new Faturamento();
            fatura.EmpresaID = pedidoEmp.Empresa.ID;
            fatura.Titulo = "automática referente ao pedido #" + pedidoEmp.ID;
            fatura.Observacoes = "";
            fatura.Status = Faturamento.TodosStatus.AguardandoPagamento;
            fatura.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.AguardandoPagamento;
            fatura.FormaPagamento = Domain.Entities.Pedido.FormasPagamento.Boleto;
            fatura.MeioPagamento = Domain.Entities.Pedido.MeiosPagamento.Boleto;
            fatura.ValorTotal = pedidoEmp.Valor;
            fatura.ValorDesconto = pedidoEmp.ValorDesconto;
            fatura.ValorEstorno = 0;
            fatura.ValorPago = 0;
            fatura.NFeValor = pedidoEmp.Valor;
            fatura.DataCriacao = DateTime.Now;
            fatura.DataAtualizacao = DateTime.Now;
            fatura.DataVencimento = new Funcoes().DataVencimentoUtil(pedidoEmp.Empresa.DiasParaVencimentoBoleto);
            fatura.DataVencimentoOriginal = fatura.DataVencimento;
            fatura.StatusNfID = (int)Domain.Entities.Faturamento.TodosStatusNFs.NaoEmitida;
            faturamentoRepository.Save(fatura);

            var Pedido = peidoEmpresaRepository.Get(pedidoEmp.ID);
            Pedido.FaturamentoID = fatura.ID;

            peidoEmpresaRepository.Save(Pedido);

            #region GERAR BOLETO

            if (fatura.ValorTotal > 0)
            {
                if (fatura.FormaPagamento == Domain.Entities.Pedido.FormasPagamento.Boleto)
                {
                    var random = new Random();
                    var nossoNumero = "";
                    do
                    {
                        nossoNumero = random.Next(99999999).ToString("00000000");
                    }
                    while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

                    var numeroDocumento = "20" + pedidoEmp.ID.ToString("00000");

                    var boleto = new Boleto();
                    boleto.FaturamentoID = fatura.ID;
                    boleto.TipoID = (int)Boleto.TodosTipos.PJ;
                    boleto.Carteira = Configuracoes.BOLETO_CARTEIRA_LC;
                    boleto.CedenteAgencia = Configuracoes.BOLETO_AGENCIA_LC;
                    boleto.CedenteCNPJ = Configuracoes.BOLETO_CNPJ_LC;
                    boleto.CedenteCodigo = Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                    boleto.CedenteContaCorrente = Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                    boleto.CedenteRazaoSocial = Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                    boleto.DataCriacao = DateTime.Now;
                    boleto.DataVencimento = fatura.DataVencimento;
                    boleto.DataEnvioLembreteVencimento = null;

                    string NumeroPedidos = "";
                    foreach(var PedidoReturn in peidoEmpresaRepository.GetByExpression(r => r.FaturamentoID == fatura.ID).ToList())
                    {
                        NumeroPedidos += " - " + "Ped: " + PedidoReturn.ID + " Fat: " + PedidoReturn.FaturamentoID;
                    }

                    boleto.Instrucao = Configuracoes.BOLETO_INSTRUCAO_LC + " | " + NumeroPedidos;
                    boleto.NossoNumero = nossoNumero;
                    boleto.NumeroDocumento = numeroDocumento;
                    boleto.SacadoDocumento = pedidoEmp.Empresa.CNPJ.Replace("-", "").Replace(".", "").Replace("/", "").Trim();
                    boleto.SacadoNome = pedidoEmp.Empresa.RazaoSocial;
                    if (String.IsNullOrEmpty(pedidoEmp.Empresa.Bairro))
                        boleto.SacadoBairro = "";
                    else
                        boleto.SacadoBairro = pedidoEmp.Empresa.Bairro;

                    if (String.IsNullOrEmpty(pedidoEmp.Empresa.CEP))
                        boleto.SacadoCEP = "";
                    else
                        boleto.SacadoCEP = pedidoEmp.Empresa.CEP;

                    if (pedidoEmp.Empresa.Cidade == null)
                        boleto.SacadoCidade = "";
                    else
                        boleto.SacadoCidade = pedidoEmp.Empresa.Cidade.Nome;

                    if (String.IsNullOrEmpty(pedidoEmp.Empresa.Logradouro))
                        boleto.SacadoEndereco = "";
                    else
                        boleto.SacadoEndereco = pedidoEmp.Empresa.Logradouro + " " + pedidoEmp.Empresa.Numero + " " + pedidoEmp.Empresa.Complemento;

                    if (pedidoEmp.Empresa.Estado == null)
                        boleto.SacadoUF = "";
                    else
                        boleto.SacadoUF = pedidoEmp.Empresa.Estado.Sigla;

                    boleto.Comando = Boleto.Comandos.CriarRemessa;
                    boleto.Processado = false;
                    boleto.StatusPagamento = Boleto.TodosStatusPagamento.AguardandoPagamento;
                    boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Criado;
                    boleto.Valor = fatura.ValorTotal;
                    boleto.DataVencimentoOriginal = boleto.DataVencimento;

                    boletoRepository.Save(boleto);
                }
            }
            #endregion

            //#region ENVIAR NF

            //if((Pedido.Empresa.OrdemDeCompra == true && !string.IsNullOrEmpty(Pedido.OrdemDeCompra)) || Pedido.Empresa.OrdemDeCompra == false)
            //{
            //    var NF = new NFE();
            //    NF.IdFat = fatura.ID;

            //    var ResultEmissao = Domain.Factories.NFEFactory.Emitir(NF, OrigemPedido.Empresa);

            //    if (!string.IsNullOrEmpty(ResultEmissao.NrReferenciaNF))
            //    {
            //        fatura.NrReferenciaNF = ResultEmissao.NrReferenciaNF;
            //        fatura.StatusNF = Faturamento.TodosStatusNFs.Processando;
            //    }
            //    else
            //    {
            //        fatura.StatusNF = Faturamento.TodosStatusNFs.NaoEmitida;
            //    }

            //    faturamentoRepository.Save(fatura);
            //}

            //#endregion

            EnviarBoleto(fatura.ID);

            return fatura;
        }
        public void EnviarBoleto(int FaturaID)
        {
            #region VARIAVEIS

            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            faturamentoRepository = new PersistentRepository<Faturamento>(new ObjectContext(conStr));
            peidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(new ObjectContext(conStr));
            boletoRepository = new PersistentRepository<Boleto>(new ObjectContext(conStr));
            empresaRepository = new PersistentRepository<Empresa>(new ObjectContext(conStr));
            colaboradorRepository = new PersistentRepository<Colaborador>(new ObjectContext(conStr));
            logRepository = new PersistentRepository<Log>(new ObjectContext(conStr));

            produtoRepository = new PersistentRepository<Produto>(new ObjectContext(conStr));
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(new ObjectContext(conStr));
            tamanhoRepository = new PersistentRepository<Tamanho>(new ObjectContext(conStr));

            var Fatura = faturamentoRepository.Get(FaturaID);
            var Empresa = empresaRepository.Get(Fatura.EmpresaID);
            var Colaboradores = colaboradorRepository.GetByExpression(r => r.EmpresaID == Empresa.ID).ToList();
            var Pedidos = peidoEmpresaRepository.GetByExpression(r => r.FaturamentoID == Fatura.ID).ToList();

            #endregion

            #region ADICIONAR EMAILS PARA ENVIO

            List<Colaborador> colaboradores = new List<Colaborador>();
            List<string> Emails = new List<string>();

            // Adicionar todos os colaboradoes MASTER e Que precisam receber emails de pedido da empresa
            colaboradores.AddRange(Colaboradores.Where(c => c.Tipo == Domain.Entities.Colaborador.Tipos.Master && c.RecebeEmailPedido).ToList());

            // Adicionar Colaborador que fez cada pedido
            foreach (var Pedido in Pedidos) {
                int ColaboradorID = (int)Pedido.ColaboradorID;
                var Colaborador = colaboradorRepository.Get(ColaboradorID);

                colaboradores.Add(Colaborador);
            }

            // Adicionar emails dos Colaboradores que fizeram o pedido
            foreach (var Colaborador in colaboradores)
                Emails.Add(Colaborador.Email);

            // Adicionar emails do Financeiro
            Emails.Add(Empresa.EmailFinanceiro);
            Emails.Add(Empresa.EmailFinanceiroCopia);

            // Remover Emails Dusplicados
            Emails = Emails.Distinct().ToList();

            #endregion

            foreach (var Email in Emails)
            {
                foreach (var Pedido in Pedidos)
                {
                    #region DADOS DO PEDIDO

                    var Boleto = boletoRepository.GetByExpression(r => r.FaturamentoID == Fatura.ID).First();

                    int ColaboradorID = (int)Pedido.ColaboradorID;
                    var colaborador = colaboradorRepository.Get(ColaboradorID);

                    var ProdutoTamanhoID = (int)Pedido.ProdutoTamanhoID;
                    var ProdutoTamanho = produtoTamanhoRepository.Get(ProdutoTamanhoID);
                    var Produto = produtoRepository.Get(ProdutoTamanho.ProdutoID);
                    var Tamanho = tamanhoRepository.Get(ProdutoTamanho.TamanhoID);

                    #endregion

                    var html = System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("/content/html/lacos/boleto.htm"));

                    html = html.Replace("[URL]", Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOME]", colaborador.Nome);
                    html = html.Replace("[DATAVENCIMENTO]", Boleto.DataVencimento.ToString("dd/MM/yyyy"));
                    html = html.Replace("[FATURA]", Fatura.Titulo);

                    int IdFatura = ID;

                    Produto produto = new Produto();

                    html = html.Replace("[IDPEDIDO]", Pedido.ID.ToString());

                    html = html.Replace("[IMAGEMPRODUTO]", Core.Configuracoes.DOMINIO + "/Content/Produtos/" + Produto.ID + "/" + Produto.Foto);
                    html = html.Replace("[NOMEPRODUTO]", Produto.Nome);
                    html = html.Replace("[TAMANHOPRODUTO]", Tamanho.Nome);
                    html = html.Replace("[QUANTIDADE]", "1");
                    html = html.Replace("[VALORPRODUTO]", ProdutoTamanho.Preco.ToString());
                    html = html.Replace("[VALORTOTAL]", Fatura.ValorTotal.ToString());

                    html = html.Replace("[LINK]", Core.Configuracoes.DOMINIO + "/boleto/empresa/" + Core.Criptografia.Encrypt(Fatura.ID.ToString()));

                    if (Core.Funcoes.ValidaEmail(Email))
                    {
                        Core.Funcoes.EnviaEmail(
                            Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                            "Laços Corporativos",
                            Email,
                            colaborador.Nome,
                            Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                            "Laços Corporativos",
                            null,
                            "[LAÇOS CORPORATIVOS] - Este é o boleto para pagamento da fatura " + Fatura.Titulo,
                            html,
                            true
                        );
                    }
                }
            }
        }

        public void EnviarEmailNfSubstituta(string To, string NameTo)
        {
            if (Core.Configuracoes.HOMOLOGACAO)
                To = Core.Configuracoes.EMAIL_HOMOLOGACAO;

            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/NfSubstituta.htm"));

            html = html.Replace("[NRPEDIDO]", this.ID.ToString());
            html = html.Replace("[NOMERAZAO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.NomeRazao);
            html = html.Replace("[DOCUMENTO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento);

            html = html.Replace("[IE]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE);
            html = html.Replace("[LOGRADOURO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Logradouro);
            html = html.Replace("[NR]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Numero);
            html = html.Replace("[COMPLEMENTO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Complemento);
            html = html.Replace("[CEP]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.CEP);

            html = html.Replace("[CIDADE]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade.Nome);
            html = html.Replace("[ESTADO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade.Estado.Nome);

            Domain.Core.Funcoes.EnviaEmail(
                    Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    "GRUPO LAÇOS FLORES",
                    To,
                    NameTo,
                    Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS,
                    NameTo,
                    "",
                    "[LACOS CORPORATIVOS] - NF Substituta Emitida p/ #" + this.ID,
                    html,
                    true
                );
        }

        public void EnviarNFe()
        {
          

            #region ADICIONAR EMAILS PARA ENVIO

            var colaboradores = this.Empresa.Colaboradors.Where(c => c.Tipo == Colaborador.Tipos.Master && c.RecebeEmailNFe).ToList();

            List<string> EmailsFinanceiro = new List<string>();

            foreach (var Pedido in this.PedidoEmpresas)
            {
                // Adiciona Email dos Colaboradores 
                if (colaboradores.Where(r => r.Email == Pedido.Colaborador.Email).Count() == 0)
                {
                    colaboradores.Add(Pedido.Colaborador);
                }

                // Adiciona email Financeiro
                if (EmailsFinanceiro.Where(r => r == Pedido.Empresa.EmailFinanceiro).Count() == 0)
                {
                    EmailsFinanceiro.Add(Pedido.Empresa.EmailFinanceiro);
                }

                // Adicionar email Fianceiro Copia
                if (EmailsFinanceiro.Where(r => r == Pedido.Empresa.EmailFinanceiroCopia).Count() == 0)
                {
                    EmailsFinanceiro.Add(Pedido.Empresa.EmailFinanceiroCopia);
                }

            }

            #endregion

            foreach (var colaborador in colaboradores)
            {
                //ENVIAR EMAIL
                var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/nfe.htm"));
                html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                html = html.Replace("[NOME]", colaborador.Nome);
                html = html.Replace("[FATURA]", this.Titulo);
                html = html.Replace("[LINK]", this.NFeLink);

                if (Domain.Core.Funcoes.ValidaEmail(colaborador.Email))
                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", colaborador.Email, colaborador.Nome, Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", null, "[LAÇOS CORPORATIVOS] - Esta é a Nota Fiscal da fatura " + this.Titulo, html, true);

                foreach(var EmailFinanceiro in EmailsFinanceiro)
                {
                    if(EmailFinanceiro != null && !string.IsNullOrEmpty(EmailFinanceiro))
                    {
                        var _email = EmailFinanceiro.Trim().ToLower();
                        if (Domain.Core.Funcoes.ValidaEmail(_email))
                            Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", _email, _email, Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", "", "[LAÇOS CORPORATIVOS] - Esta é a Nota Fiscal da fatura " + this.Titulo, html, true);
                    }
                }
                
            }

            foreach (var pedido in this.PedidoEmpresas.Where(c=> c.Status == Entities.PedidoEmpresa.TodosStatus.PedidoConcluido).ToList())
            {
                var _colaborador = pedido.Colaborador;

                //ENVIAR EMAIL
                var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacos/nfe.htm"));
                html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                html = html.Replace("[NOME]", _colaborador.Nome);
                html = html.Replace("[FATURA]", this.Titulo);
                html = html.Replace("[LINK]", this.NFeLink);

                if (Domain.Core.Funcoes.ValidaEmail(_colaborador.Email))
                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", _colaborador.Email, _colaborador.Nome, Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS_LACOS, "Laços Corporativos", "", "[LAÇOS CORPORATIVOS] - Esta é a Nota Fiscal da fatura " + this.Titulo, html, true);
            }
        }
    }
}
