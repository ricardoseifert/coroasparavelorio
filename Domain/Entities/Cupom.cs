﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using DomainExtensions.Repositories.Interfaces;

namespace Domain.Entities
{
    public partial class Cupom : IPersistentEntity
    {

        public enum Tipos
        {
            Porcentagem = 1,
            Valor = 2
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
        }

    }
}