﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities.Generics
{
    public class DashBoard
    {
        public decimal totalliquido { get; set; }
        public decimal totalRepasse { get; set; }
        public decimal totalFaturamento { get; set; }
        public decimal TicketMedio { get; set; }
        
        public decimal QtdPedidos { get; set; }
        public decimal QtdPedidosBruto { get; set; }
        public decimal QtdPedidosCancelados { get; set; }
        public decimal QtdPedidosAutomatizados { get; set; }

        public decimal TotalItensPedido { get; set; }
        public decimal TotalItensPedidoPJ { get; set; }
        public decimal TotalItensPedidoPF { get; set; }


        public decimal totalRepasseOnline { get; set; }
        public decimal totalRepasseOffline { get; set; }
        public decimal totalRepasseOnlineCPV { get; set; }
        public decimal totalRepasseOfflineCPV { get; set; }
        public decimal totalRepasseOnlineNET { get; set; }
        public decimal totalRepasseOfflineNET { get; set; }
        public decimal totalRepasseOnlineOutros { get; set; }
        public decimal totalRepasseOfflineOutros { get; set; }

        public decimal QtdMailConf { get; set; }

        public decimal QtdPedidosComAceiteFornecedor { get; set; }
        public decimal QtdPedidosComAceiteManual { get; set; }

        public decimal NotaNPS { get; set; }
        public int QtdPedidosNPS { get; set; }

        private List<Pedido> _ListaPedidosCanceladosCPV;
        public List<Pedido> ListaPedidosCanceladosCPV
        {
            get { return _ListaPedidosCanceladosCPV; }
            set { _ListaPedidosCanceladosCPV = value; }
        }

        private List<PedidoEmpresa> _ListaPedidosCanceladosCORP;
        public List<PedidoEmpresa> ListaPedidosCanceladosCORP
        {
            get { return _ListaPedidosCanceladosCORP; }
            set { _ListaPedidosCanceladosCORP = value; }
        }

        private List<Pedido> _ListaPedidosComAceiteManual;
        public List<Pedido> ListaPedidosComAceiteManual
        {
            get { return _ListaPedidosComAceiteManual; }
            set { _ListaPedidosComAceiteManual = value; }
        }

    }
}
