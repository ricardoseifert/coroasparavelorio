﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain.Entities.Generics
{
    public class WebHookLigacao
    {
        public string @event { get; set; }
        public string membername { get; set; }
        public string linkedid { get; set; }
        public string queue { get; set; }
        public string calleridnum { get; set; }
        public string destchannel { get; set; }
        public string holdtime { get; set; }
        public string destcalleridnum { get; set; }

    }

    //public class data
    public class Ligacao
    {
        public string IdLigacao { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraAtendimento { get; set; }
        public string NrOrigem { get; set; }
        public string NrDestino { get; set; }
        public string TipoLigacao { get; set; }
        public string NrEntrada { get; set; }
        public string IdLinha { get; set; }
        public string NomeLinha { get; set; }
        public string TempoLigacao { get; set; }
        public string Status { get; set; }
        public string LinkAudio { get; set; }
    }
}