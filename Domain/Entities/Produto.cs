﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Produto : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Coroa de Flores")]
            CoroaFlor = 1,
            [Description("Arranjo Maternidade")]
            ArranjoFlor = 2,
            [Description("Flor de Aniversário")]
            Aniversario = 3,
            [Description("Orquídea e Buquê")]
            OrquideaBuque = 4,
            [Description("Datas Comemorativas")]
            Datas = 5,
            [Description("Eventos")]
            Eventos = 6,
            [Description("ArranjosCondo")]
            ArranjosCondo = 7,
            [Description("Kitsbebe")]
            Kitsbebe = 8,
            [Description("Personalizado")]
            Personalizado = 100
        }

        public enum CodigosNCM
        {
            [Description("06039000")]
            Flores = 1
        }

        public enum Sexos
        {
            [Description("Masculino")]
            Masculino = 1,
            [Description("Feminino")]
            Feminino = 2,
            [Description("Masculino e Feminino")]
            Ambos = 3
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public Sexos Sexo
        {
            get { return (Sexos)this.SexoID; }
            set { this.SexoID = (int)value; }
        }

        public IEnumerable<Flor> Flores
        {
            get { return this.ProdutoFlors.Select(p => p.Flor); }
        }

        public decimal? PrecoMinimo
        {
            get { return this.ProdutoTamanhoes.Any() ? (decimal?)this.ProdutoTamanhoes.Min(t => t.Preco) : null; }
        }

        public decimal? PrecoMaximo
        {
            get { return this.ProdutoTamanhoes.Any() ? (decimal?)this.ProdutoTamanhoes.Max(t => t.Preco) : null; }
        }

    }
}
