﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;

namespace Domain.Entities
{
    public partial class RepasseFloricultura : IPersistentEntity
    {

        public enum TodosStatus
        { 
            [Description("Pagamento Pendente")]
            PagamentoPendente =1,
            [Description("Pagamento Efetuado")]
            Pago = 2,
            [Description("Cancelado")]
            Cancelado = 3
        }
         
        public TodosStatus Status
        {
            get { return (TodosStatus)this.StatusRepasseID; }
            set { this.StatusRepasseID = (int)value; }
        }

    }
}
