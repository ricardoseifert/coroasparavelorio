﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities
{
    public class CarrinhoItem
    {

        public int ID;
        public ProdutoTamanho ProdutoTamanho;
        public string Descricao;
        public string Mensagem;
        public string Observacoes;
        public decimal Valor;

    }
}
