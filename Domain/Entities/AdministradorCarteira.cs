﻿using System;
using System.Linq.Expressions;
using Domain.MetodosExtensao;
using DomainExtensions.Entities;

namespace Domain.Entities {
    //=================================================================================
    public partial class AdministradorCarteira : IPersistentEntity {
        
        #region Metodos
        //-----------------------------------------------------------------------------
        public static Expression<Func<AdministradorCarteira, bool>> CarteiraComVigencia(int? intIdAdm) {
            Expression<Func<AdministradorCarteira, bool>> exp =
                p => p.DataInicio <= DateTime.Now && (!p.DataTermino.HasValue || p.DataTermino.Value >= DateTime.Now);

            if (intIdAdm.HasValue && intIdAdm.Value > 0) {
                exp = exp.And(a => a.AdministradorID == intIdAdm.Value);
            }

            return exp;
        }
        //-----------------------------------------------------------------------------
        #endregion

    }
    //=================================================================================
}
