﻿using System.Collections.Generic;
using System.Linq;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;
using DomainExtensions.Repositories.Interfaces;

namespace Domain.Entities {
    public partial class Administrador : IPersistentEntity {

        public enum Perfis {
            [Description("Financeiro")]
            Financeiro = 1,
            [Description("Atendimento")]
            Atendimento = 2,
            [Description("Fornecedores")]
            Fornecedores = 3,
            [Description("Laços Corporativos")]
            LacosCorporativos = 4,
            [Description("Comunicação")]
            Comunicacao = 5,
            [Description("Qualidade")]
            Qualidade = 6,
            [Description("TI")]
            TI = 7,
            [Description("RH")]
            RH = 8,
            [Description("Coordenadores")]
            Coordenadores = 9,
            [Description("Sócios")]
            Socios = 10,
            [Description("AdWords")]
            AdWords = 11,
            [Description("Laços Corporativos Financeiro")]
            LacosCorporativosFinanceiros = 12,
            [Description("Floricultura")]
            Floricultura = 13,
            [Description("FloriculturaMisto")]
            FloriculturaMisto = 14
        }

        public Perfis Perfil {
            get { return (Perfis)this.PerfilID; }
            set { this.PerfilID = (int)value; }
        }

        public string Foto {
            get {
                FileInfo foto = new FileInfo(HttpContext.Current.Server.MapPath("/content/perfis/" + this.ID + "/profile.jpg"));
                if (foto.Exists)
                    return "/content/perfis/" + this.ID + "/profile.jpg";
                else
                    return "/content/perfis/profile.jpg";
            }
        }
        
    }
}
