﻿using DomainExtensions.Entities;

namespace Domain.Entities {
    public partial class Metrica : IPersistentEntity {
        public int ID { get { return (int)Id; } }

        public enum TodosTiposMetrica{
            Valor = 1,
            Numerico = 2,
            Porcentagem = 3
        }

    }
}
