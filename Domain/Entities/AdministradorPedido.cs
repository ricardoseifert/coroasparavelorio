﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class AdministradorPedido : IPersistentEntity
    {
        public enum Acao
        {
            [Description("Processou Pedido")]
            ProcessouPedido = 1,
            [Description("Cancelou Pedido")]
            CancelouPedido = 2,
            [Description("Enviou e-mail de entrega")]
            EnviouEmailEntrega = 3,
            [Description("Imprimiu Boleto")]
            ImprimiuBoleto = 4,
            [Description("Preencheu NF")]
            PreencheuNF = 5,
            [Description("Alterou Forma de Pagamento")]
            AlterouFormaPagamento = 6,
            [Description("Preencheu Repasse")]
            PreencheuRepasse = 7,
            [Description("Alterou Data Vencimento")]
            AlterouDataVencimento = 8,
            [Description("Concluiu Pedido")]
            ConcluiuPedido = 9,
            [Description("Alterou Floricultura")]
            AlterouFloricultura = 10,
            [Description("Alterou Repasse")]
            AlterouRepasse = 11,
            [Description("Enviou e-mail para fornecedor")]
            EnviouEmailFornecedor = 12,
            [Description("Reenviou e-mail pedido")]
            ReenviouEmailPedido = 13,
            [Description("Enviou e-mail boleto")]
            EnviouEmailBoleto = 14,
            [Description("Enviou e-mail NFe")]
            EnviouEmailNFe = 15,
            [Description("Alterou para prejuízo")]
            AlterouPrejuizo = 16,
            [Description("Alterou para pagto efetuado")]
            AlterouPago = 17,
            [Description("Cancelou fatura")]
            CancelouFatura = 18,
            [Description("Aprovou a Foto")]
            ValidouFoto = 19,
            [Description("Reprovou a Foto")]
            ReprovouFoto = 20,
            [Description("Fez o Aceite Manual")]
            AceiteManual = 21,
            [Description("Fornecedor Recusou Entrega")]
            RecusouEntrega = 22,
            [Description("Fornecedor aceitou Entrega")]
            AceitouEntrega = 23,
            [Description("Saiu p/ Entrega")]
            SaiuParaEntrega = 24,
            [Description("Alterou o Local de Entrega")]
            AlterouLocalIDEntrega = 25,
            [Description("Email Automatico de NFe enviado")]
            EnviouEmailAutomaticoNFe = 26,
            [Description("Email Automatico de cancelamento de NFe enviado")]
            EnviouEmailAutomaticoCancelamentoNFe = 27,
            [Description("Cancelou NFe")]
            CancelouNFE = 28,
            [Description("Negociação Automatica Boleto Enviada")]
            NegociacaoAutomaticaBoletoEnviada = 29,
            [Description("Email Automatico de pedido em Produção enviado")]
            EnviouEmailDePedidoEmProducao = 30,
            [Description("NFEnviadaParaFocus")]
            NFEnviadaParaFocus = 31,
            [Description("Fornecedor Marcou Como Entregue")]
            FornecedorMarcouEntregue = 32
        }

        public string NomeFornecedor { get; set; }

        public Acao AcaoEnum
        {
            get { return (Acao)this.AcaoID; }
            set { this.AcaoID = (int)value; }
        }
    }
}
