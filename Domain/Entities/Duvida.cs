﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Duvida : IPersistentEntity
    {
        public enum Tipos
        {
            [Description("Cadastro")]
            Cadastro = 1,
            [Description("Como Comprar")]
            ComoComprar = 2,
            [Description("Entrega")]
            Entrega = 3,
            [Description("Pagamento")]
            Pagamento = 4,
            [Description("Meus Pedidos")]
            MeusPedidos = 5,
            [Description("Demais Informações")]
            Outros = 6
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }
    }
}
