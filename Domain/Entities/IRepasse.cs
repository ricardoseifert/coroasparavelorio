﻿using System;
using System.Collections.Generic;

namespace Domain.Entities {
    public interface IRepasse {

        #region Requeridos
        int ID { get; set; }
        int PagamentoID { get;  }
        int Fornecedor_ID { get;  }
        Pedido PedidoCPV { get; }
        PedidoEmpresa PedidoLacos { get; }
        PagamentoGenerico PagamentoFornGenerico { get; }
        string NomeFornecedor { get; }
        string NomeFavorecido { get; }
        string BancoFornecedor { get; }
        OrigemFornecedor TipoRepasse { get; }
        DateTime DataCriacao { get; set; }
        DateTime? DataPPagamento { get; }
        
        int IDFluxoDePagamento { get; }
        Lote Lote { get; }
        int IDStatus { get; }
        List<HistoricoRetorno> LstHistorico { get; set; }
        List<CNAB> LstCNAB { get; set; }

        #endregion

    }

    
}