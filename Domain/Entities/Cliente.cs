﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using DomainExtensions.Repositories;

namespace Domain.Entities
{
    public partial class Cliente : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("PF")]
            PessoaFisica = 1,
            [Description("PJ")]
            PessoaJuridica = 2
        }
        public enum TipoInscricaoEstadual
        {
            [Description("Isento")]
            Isento = 1,
            [Description("Não Contribuinte")]
            NaoContribuinte = 2,
            [Description("Contribuinte")]
            Contribuinte = 3
        }

        public enum LacosStatusProspecao
        {
            [Description("Não Prospectado")]
            NaoProspectado = 1,
            [Description("Em Prospecção")]
            EmProspeccao = 2,
            [Description("Concluído")]
            Concluido = 3,
            [Description("Não tem Interesse")]
            SemInteresse = 4
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public TipoInscricaoEstadual TiposInscricao
        {
            get { return (TipoInscricaoEstadual)this.TipoIE; }
            set { this.TipoIE = (int)value; }
        }

        public LacosStatusProspecao LacosStatusProspecaoAtual
        {
            get { return (LacosStatusProspecao)this.LacosStatusProspecaoID; }
            set { this.LacosStatusProspecaoID = (int)value; }
        }

        public string Endereco
        {
            get { return this.Logradouro + ", " + this.Numero + " " + this.Complemento; }
        }
        
        public string EnderecoCompleto
        {
            get
            {
                var endereco = this.Logradouro + ", " + this.Numero + " " + this.Complemento;
                if (this.Cidade != null)
                    endereco += " " + this.Cidade.NomeCompleto;
                return endereco;
            }
        }
        public string CelularContatoSMS
        {
            get
            {
                if (!String.IsNullOrEmpty(CelularContato))
                {
                    return "55" + CelularContato.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim();
                }
                else
                    return "";
            }
        }

        public string ErroCriacao { get; set; }
    }
}
