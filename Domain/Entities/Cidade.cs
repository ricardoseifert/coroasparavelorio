﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class Cidade : IPersistentEntity
    {
        public string NomeCompleto
        {
            get
            {
                try
                {
                    return this.Nome + " / " + this.Estado.Sigla;
                }
                catch
                {
                    return "";
                }
            }
        }
    }
}
