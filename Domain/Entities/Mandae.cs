﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using YamlDotNet.Serialization;
using System.Net;
using Domain.Core;
using System.Globalization;


namespace Domain.Entities.Integracoes
{
    public class Mandae
    {
        public string customerId { get; set; }
        public DateTime scheduling { get; set; }
        public List<Item> items { get; set; }
        public Sender sender { get; set; }
        public string vehicle { get; set; }
    }

    public partial class Dimensions
    {
        public int height { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int weight { get; set; }
    }

    public partial class Sku
    {
        public string skuId { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public double freight { get; set; }
        public int quantity { get; set; }
    }

    public partial class invoice
    {
        public int id { get; set; }
        public string key { get; set; }
        public string type { get; set; }
    }

    public partial class Address
    {
        public string postalCode { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string addressLine2 { get; set; }
        public string neighborhood { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string reference { get; set; }
    }

    public partial class Recipient
    {
        public string fullName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
    }

    public partial class Item
    {
        public invoice invoice { get; set; }
        public Dimensions dimensions { get; set; }
        public List<Sku> skus { get; set; }
        public Recipient recipient { get; set; }
        public string shippingService { get; set; }
        public string store { get; set; }
        public string partnerItemId { get; set; }
        public double totalValue { get; set; }
        public double totalFreight { get; set; }
    }

    public partial class Sender
    {
        public string fullName { get; set; }
        public Address address { get; set; }
    }

}

namespace Domain.Entities.Integracoes
{
    public class MandaeFrete
    {
        public string declaredValue { get; set; }
        public string weight { get; set; }
        public string height { get; set; }
        public string width { get; set; }
        public string length { get; set; }
    }
}

namespace Domain.Entities.Integracoes
{
    public class ShippingService
    {
        public object id { get; set; }
        public string name { get; set; }
        public int days { get; set; }
        public double price { get; set; }
    }

    public class MandaeFreteResult
    {
        public string postalCode { get; set; }
        public List<ShippingService> shippingServices { get; set; }
    }

    public class Event
    {
        public object id { get; set; }
        public string date { get; set; }
        public string timestamp { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    public class MandadeRastreamentoResult
    {
        public object idItemParceiro { get; set; }
        public object partnerItemId { get; set; }
        public string trackingCode { get; set; }
        public string carrierCode { get; set; }
        public string carrierName { get; set; }
        public List<Event> events { get; set; }
    }
}