﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class ClienteFloricultura : IPersistentEntity
    {
        private IPersistentRepository<ClienteFloricultura> clienteFloriculturaRepository;

        public ClienteFloricultura()
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            clienteFloriculturaRepository = new PersistentRepository<ClienteFloricultura>(new ObjectContext(conStr));
        }

        public enum Tipos
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("PF")]
            PessoaFisica = 1,
            [Description("PJ")]
            PessoaJuridica = 2
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

        public string Endereco
        {
            get { return this.Logradouro + ", " + this.Numero + " " + this.Complemento; }
        }

        public string NomeCompleto
        {
            get
            {
                string Nome = "";

                if(this.TipoID == 2)
                {
                    Nome = this.Nome + " - " + this.RazaoSocial;
                }
                else
                {
                    Nome = this.Nome;
                
                }
                return Nome;
            }
        }

        public string DocumentoFormatado
        {
            get
            {
                if (Documento != null)
                {
                    return Domain.Core.Funcoes.Formata_CNPJ(this.Documento);
                }
                else
                {
                    return "";
                }

            }
        }

        public string EnderecoCompleto
        {
            get
            {
                var endereco = this.Logradouro + ", " + this.Numero + " " + this.Complemento;
                if (this.Cidade != null)
                    endereco += " " + this.Cidade.NomeCompleto;
                return endereco;
            }
        }
    }
}
