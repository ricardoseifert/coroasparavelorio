﻿using DomainExtensions.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public partial class Banco : IPersistentEntity
    {
        public Int32 ID { get; set; }
        public String CodCompensacao { get; set; }
        public String NomeBanco { get; set; }
        public String CNPJ { get; set; }
        public String Site{ get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public Int32 Status { get; set; }

    }
}
