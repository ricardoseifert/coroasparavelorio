﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class Lote : IPersistentEntity
    {
        public decimal ValorTotal {
            get {

                decimal ValorPagamentoGenerico = this.ItemLotes.Sum(r => r.PagamentoGenerico_X_ItemLote.Sum(p => p.PagamentoGenerico.Valor));
                decimal ValorRepasse = this.ItemLotes.Sum(r => r.Repasse_X_ItemLote.Sum(p => p.Repasse.ValorRepasse));
                return ValorPagamentoGenerico + ValorRepasse;
            }
        }

        public decimal ValorTerceiros
        {
            get
            {
               return this.ItemLotes.Sum(r => r.PagamentoGenerico_X_ItemLote.Sum(p => p.PagamentoGenerico.Valor));
            }
        }

        public int QtdPagamentos
        {
            get
            {
                var a = this.ItemLotes.Sum(r => r.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status != PagamentoGenerico.TodosStatusPagamento.Pago && p.PagamentoGenerico.Status != PagamentoGenerico.TodosStatusPagamento.Cancelado).ToList().Count);
                var b = this.ItemLotes.Sum(r => r.Repasse_X_ItemLote.Where(p => p.Repasse.Status != Repasse.TodosStatus.Cancelado && p.Repasse.Status != Repasse.TodosStatus.Pago).ToList().Count);
                return a + b;
            }
        }

        public int QtdPagamentosPagos
        {
            get
            {
                var a = this.ItemLotes.Sum(r => r.PagamentoGenerico_X_ItemLote.Where(p => p.PagamentoGenerico.Status == PagamentoGenerico.TodosStatusPagamento.Pago).ToList().Count);
                var b = this.ItemLotes.Sum(r => r.Repasse_X_ItemLote.Where(p => p.Repasse.Status == Repasse.TodosStatus.Pago).ToList().Count);
                return a + b;
            }
        }

        public int PagamentosComCNAB
        {
            get
            {
                int count = 0;
                foreach(var CNAB in this.CNABs)
                {
                    foreach(var ItemCNAB in CNAB.CNAB_ItemCnab)
                    {
                        count++;
                    }
                }
                return count;
            }
        }
        //public string GerarCNAB()
        //{

        //}

        public decimal ValorFloriculturas
        {
            get
            {
                
                return this.ItemLotes.Sum(r => r.Repasse_X_ItemLote.Sum(p => p.Repasse.ValorRepasse));                
            }
        }
    }
}
