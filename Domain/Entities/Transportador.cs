﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;

namespace Domain.Entities
{
    public partial class Transportador : IPersistentEntity
    {
        public enum Tipos
        {
            [Description("N/I")]
            Indefinido = 0,
            [Description("PF")]
            PessoaFisica = 1,
            [Description("PJ")]
            PessoaJuridica = 2
        }
        
    }
}
