﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;

namespace Domain.Entities
{
    public partial class Agendum : IPersistentEntity
    {

        public enum Tipos
        {
            [Description("Reunião")]
            Reuniao = 1
        }

        public Tipos Tipo
        {
            get { return (Tipos)this.TipoID; }
            set { this.TipoID = (int)value; }
        }
    }
}
