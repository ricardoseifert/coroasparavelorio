﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;

namespace Domain.Entities
{
    public partial class AdministradorGrupo : IPersistentEntity
    {

        public enum Grupos
        {
            [Description("Financeiro")]
            Financeiro = 1,
            [Description("Atendimento")]
            Atendimento = 2,
            [Description("Fornecedores")]
            Fornecedores = 3,
            [Description("Laços Corporativos")]
            LacosCorporativos = 4,
            [Description("Comunicação")]
            Comunicacao = 5,
            [Description("Qualidade")]
            Qualidade = 6,
            [Description("TI")]
            TI = 7,
            [Description("RH")]
            RH = 8,
            [Description("Coordenadores")]
            Coordenadores = 9,
            [Description("Sócios")]
            Socios = 10
        }

        public Grupos Grupo
        {
            get { return (Grupos)this.GrupoID; }
            set { this.GrupoID = (int)value; }
        }
         
    }
}
