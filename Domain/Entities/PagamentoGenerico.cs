﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class PagamentoGenerico : IPersistentEntity, IRepasse
    {
        //private IPersistentRepository<HistoricoRetorno> historicoRetornoRepository;
        //private IPersistentRepository<CNAB_ItemCnab> cnab_ItemCnabReposotory;
        //private IPersistentRepository<CNAB> cnabReposotory;
        public PagamentoGenerico()
        {
            //var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            //var entityBuilder = new EntityConnectionStringBuilder();

            //entityBuilder.Provider = "System.Data.SqlClient";
            //entityBuilder.ProviderConnectionString = conStr;

            //historicoRetornoRepository = new PersistentRepository<HistoricoRetorno>(new ObjectContext(conStr));
            //cnab_ItemCnabReposotory = new PersistentRepository<CNAB_ItemCnab>(new ObjectContext(conStr));
            //cnabReposotory = new PersistentRepository<CNAB>(new ObjectContext(conStr));
        }
        public enum TodosStatusPagamento
        {
            [Description("Aguardando Pagamento")]
            AguardandoPagamento = 1,
            [Description("Pagamento Efetuado")]
            Pago = 2,
            [Description("Cancelado")]
            Cancelado = 3,
            [Description("Fechamento Efetuado")]
            Fechamento = 4
        }

        public TodosStatusPagamento Status
        {
            get { return (TodosStatusPagamento)this.StatusPagamentoFornecedorGenerico.ID; }
            set { this.StatusPagamentoFornecedorGenerico.ID = (int)value; }
        }

        public int RepasseId
        {
            get; set;
        }

        public int PagamentoID { get { return this.ID; } }

        public int Fornecedor_ID { get { return this.FornecedorGenerico.ID; } }

        public decimal ValorTotal { get { return this.Valor; } }
        public Pedido PedidoCPV { get { return null; } }
        public PedidoEmpresa PedidoLacos { get { return null; } }
        public PagamentoGenerico PagamentoFornGenerico { get { return this; } }
        public Lote Lote
        {
            get
            {
                if (this.PagamentoGenerico_X_ItemLote.Count > 0)
                {
                    return this.PagamentoGenerico_X_ItemLote.First().ItemLote.Lote;
                }
                else
                {
                    return null;
                }

            }
        }
        public int IDFluxoDePagamento { get { return 99; } }
        public List<HistoricoRetorno> LstHistorico { get; set; }
        public List<CNAB> LstCNAB { get; set; }
        public string NrControle { get; set; }
        public int IDStatus { get { return this.StatusPagamentoFornecedorGenerico.ID; } }
        public DateTime? DataPPagamento { get { return this.DataDePagamento; } }
        public string NomeFornecedor { get { return this.FornecedorGenerico.NomeFornecedor.ToUpper(); } }
        public string NomeFavorecido { get { return this.FornecedorGenerico.FornecedorGenericoContas.First().Favorecido.ToUpper(); } }
        public string BancoFornecedor { get { return this.FornecedorGenerico.FornecedorGenericoContas.First().FornecedorGenericoBanco.NomeBanco.ToUpper(); } }
        public OrigemFornecedor TipoRepasse { get { return OrigemFornecedor.Generico; } }

        //public List<HistoricoRetorno> HistoricoRetorno
        //{
        //    get
        //    {
        //        var Historicos = historicoRetornoRepository.GetByExpression(r => r.IdPagamentoGenerico == this.ID).ToList();

        //        if (Historicos.Count > 0)
        //        {
        //            return Historicos;
        //        }
        //        else {
        //            return null;
        //        }
        //    }
        //}

        //public CNAB CNAB
        //{
        //    get
        //    {
        //        var cnab = cnab_ItemCnabReposotory.GetByExpression(r => r.PagamentoGenericoID == this.ID);

        //        if (cnab.ToList().Count > 0)
        //        {
        //            return cnabReposotory.Get((int)cnab.First().CNABID);
        //        }
        //        else {
        //            return null;
        //        }
        //    }
        //}
    }
}
