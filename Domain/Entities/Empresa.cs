﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class Empresa : IPersistentEntity
    {
        public enum CondicoesFaturamento
        {
            //[Description("Cartão Corporativo")]
            //NI = 1,
            [Description("Imediato")]
            Imediato = 2,
            [Description("Mensal")]
            Mensal = 3
            //[Description("Pontual")]
            //Pontual = 4
        }

        public enum FormasPagamento
        {
            [Description("Boleto Bancário")]
            Boleto = 1,
            [Description("Crédito em Conta")]
            CreditoEmConta = 2,
            [Description("Cartão de Crédito")]
            CartaoDeCredito = 3
        }

        public CondicoesFaturamento? CondicaoFaturamento
        {
            get {
                if(this.CondicaoFaturamentoID != null)
                {
                    return (CondicoesFaturamento)this.CondicaoFaturamentoID;
                }
                else
                {
                    return null;
                }
            }
            set {
                this.CondicaoFaturamentoID = (int)value;
            }
        }

        public string Endereco
        {
            get { return this.Logradouro + ", " + this.Numero + " " + this.Complemento; }
        }

        public string NomeCompleto
        {
            get { return this.RazaoSocial + " - " + Domain.Core.Funcoes.Formata_CNPJ(this.CNPJ); }
        }
        
        public int DiasParaVencimentoBoleto
        {
            get
            {
                if (this.DiasParaVencimento > 0)
                    return this.DiasParaVencimento;
                else
                    return Domain.Core.Configuracoes.BOLETO_DIAS_VENCIMENTO;
            }
        }
    }
}
