﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.IO;
using System.Web;

namespace Domain.Entities
{
    public partial class AgendaItem : IPersistentEntity
    {

        public enum Status
        {
            [Description("Pendente")]
            Pendente = 1,
            [Description("Concluído")]
            Concluido = 2
        }

        public Status StatusItem
        {
            get { return (Status)this.StatusID; }
            set { this.StatusID = (int)value; }
        }
    }
}
