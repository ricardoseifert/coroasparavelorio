﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class CNABTextLinhaHeader
    {
        public string Gerar()
        {
            string LinasPagamento = "";

            LinasPagamento += this.CodigoBanco;
            LinasPagamento += this.CodigoLote;
            LinasPagamento += this.TipoRegistro;
            LinasPagamento += this.TipoOperacao;
            LinasPagamento += this.TipoPagamento;
            LinasPagamento += this.FormaPagamento;
            LinasPagamento += this.LayoutLote;
            LinasPagamento += this.Brancos1;
            LinasPagamento += this.Empresa;
            LinasPagamento += this.InscricaoNumero;
            LinasPagamento += this.IdentificacaoLancamento;
            LinasPagamento += this.Brancos2;
            LinasPagamento += this.Agencia;
            LinasPagamento += this.Brancos3;
            LinasPagamento += this.Conta;
            LinasPagamento += this.Brancos4;
            LinasPagamento += this.Dac;
            LinasPagamento += this.NomeEmpresa;
            LinasPagamento += this.FinalidadeLote;
            LinasPagamento += this.HistoricoCC;
            LinasPagamento += this.EnderecoEmpresa;
            LinasPagamento += this.Numero;
            LinasPagamento += this.Complemento;
            LinasPagamento += this.Cidade;
            LinasPagamento += this.Cep;
            LinasPagamento += this.Estado;
            LinasPagamento += this.Brancos5;
            LinasPagamento += this.Ocorrencias;            

            return LinasPagamento;
        }

        public string CodigoBanco { get; set; }
        public string CodigoLote { get; set; }
        public string TipoRegistro { get; set; }
        public string TipoOperacao { get; set; }
        public string TipoPagamento { get; set; }
        public string FormaPagamento { get; set; }
        public string LayoutLote { get; set; }
        public string Brancos1 { get; set; }
        public string Empresa { get; set; }
        public string InscricaoNumero { get; set; }
        public string IdentificacaoLancamento { get; set; }
        public string Brancos2 { get; set; }
        public string Agencia { get; set; }
        public string Brancos3 { get; set; }
        public string Conta { get; set; }
        public string Brancos4 { get; set; }
        public string Dac { get; set; }
        public string NomeEmpresa { get; set; }
        public string FinalidadeLote { get; set; }
        public string HistoricoCC { get; set; }
        public string EnderecoEmpresa { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Cidade { get; set; }
        public string Cep { get; set; }
        public string Estado { get; set; }
        public string Brancos5 { get; set; }
        public string Ocorrencias { get; set; }

    }
}
