﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities.ReportsStoredProcs
{
    public class RelatorioTipoPessoa
    {
        public int TipoID { get; set; }
        public int Total { get; set; }
    }
}
