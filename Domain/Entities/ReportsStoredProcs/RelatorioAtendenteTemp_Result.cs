﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ReportsStoredProcs
{
    public class RelatorioAtendenteTemp_Result
    {
        public string Nome { get; set; }
        public int QtdPedido { get; set; }
        public int TempoSecTotal { get; set; }
        public int TimeSec { get; set; }
    }
}
