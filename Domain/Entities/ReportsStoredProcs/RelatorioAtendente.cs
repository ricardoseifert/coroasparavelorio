﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities.ReportsStoredProcs
{
    public class RelatorioAtendente
    {
        public string Nome { get; set; }
        public int QtdeConcluiu { get; set; }
        public int QtdeConcluiuOn { get; set; }
        public int QtdeConcluiuOff { get; set; }
        public int QtdeCancelou { get; set; }
        public decimal SomaConcluiu { get; set; }
        public decimal SomaConcluiuOn { get; set; }
        public decimal SomaConcluiuOff { get; set; }
        public decimal SomaCancelou { get; set; }
    }
}
