﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public partial class LogMandrillEntidade : Domain.Entities.LogMandrill
    {
        public enum TipoEmail
        {
            [Description("Pedido")]
            Pedido = 1,
            [Description("Primeiro Boleto")]
            PrimeiroBoleto = 2,
            [Description("Entrega")]
            Entrega = 3,
            [Description("NFE")]
            NFE = 4,
            [Description("Cancelar NFE")]
            CancelarNFE = 5,
            [Description("Cobrança")]
            Cobranca = 6,
            [Description("Cobrança Robo Primeira Vez")]
            CobrancaRoboPrimeiraVez = 7,
            [Description("Renegociação Robo")]
            RenegociacaoRobro = 8,

            [Description("Robo LC - Boleto - Vence em 2 Dias")]
            RoboLCBoletoVence2Dias = 9,
            [Description("Robo LC - Boleto - Vence Hoje")]
            RoboLCBoletoVenceHoje = 10,
            [Description("Robo LC - Boleto - Venceu a 1 Dia")]
            RoboLCBoletoVenceu1Dia = 11,
            [Description("Robo LC - Boleto - Segundo Boleto Renegociado")]
            RoboLCBoletoSegundoBoletoRenegociado = 12,
            [Description("Robo LC - Boleto - Inadimplencia")]
            RoboLCBoletoInadimplencia = 13,

            [Description("Robo LC - Fatura - Vence em 2 Dias")]
            RoboLCFaturaVence2Dias = 14,
            [Description("Robo LC - Fatura - Vence Hoje")]
            RoboLCFaturaVenceHoje = 15,
            [Description("Robo LC - Fatura - Venceu a 1 Dia")]
            RoboLCFaturaVenceu1Dia = 16,
            [Description("Robo LC - Fatura - Segundo Fatura Renegociado")]
            RoboLCFaturaSegundoFaturaRenegociado = 17,
            [Description("Robo LC - Fatura - Inadimplencia")]
            RoboLCFaturaInadimplencia = 18,


            [Description("Outros")]
            Outros = 99
        }
        public void SalvaLog(string IdEmail, string TipoEmail, int? PedidoID, int? PedidoEmpresaID, string reject_reason = null)
        {
            var log = new LogMandrill();
            log.DataCadastro = DateTime.Now;

            if (PedidoID > 0)
                log.PedidoID = (int)PedidoID;

            if (PedidoEmpresaID > 0)
                log.PedidoEmpresaID = (int)PedidoEmpresaID;

            if (!string.IsNullOrEmpty(reject_reason))
                log.RejectReason = reject_reason;

            log.TipoEmailStr = TipoEmail.ToString();
            log.IdEmail = IdEmail;

            log.Aberto = false;
            log.Clicado = false;

            var Context = new COROASEntities();
            Context.LogMandrills.AddObject(log);

            Context.SaveChanges();
        }
    }
}
