﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using System.Configuration;
using System.Data.EntityClient;

namespace Domain.Entities
{
    public partial class Funeraria : IPersistentEntity
    {
        public int QuantidadePedidosNaoCancelados
        {
            get
            {
                return this.ContatoFunararias.Sum(r => r.PedidoFloriculturas.Count);
            }
        }

        public string DocumentoFormatado
        {
            get
            {
                if(Documento != null)
                {
                    return Domain.Core.Funcoes.Formata_CNPJ(this.Documento);
                }
                else
                {
                    return "";
                }
                
            }
        }

    }
}
