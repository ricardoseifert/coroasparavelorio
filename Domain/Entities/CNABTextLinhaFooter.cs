﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class CNABTextLinhaFooter
    {
        public string Gerar()
        {
            string LinasPagamento = "";

            LinasPagamento += this.CodigoBanco;
            LinasPagamento += this.CodigoLote;
            LinasPagamento += this.TipoRegistro;
            LinasPagamento += this.Brancos1;
            LinasPagamento += this.TotalQtdeRegistros;
            LinasPagamento += this.TotalValorPagtos;
            LinasPagamento += this.Zeros;
            LinasPagamento += this.Brancos2;
            LinasPagamento += this.Ocorrencias;

            return LinasPagamento;
        }

        public string CodigoBanco { get; set; }
        public string CodigoLote { get; set; }
        public string TipoRegistro { get; set; }
        public string Brancos1 { get; set; }
        public string TotalQtdeRegistros { get; set; }
        public string TotalValorPagtos { get; set; }
        public string Zeros { get; set; }
        public string Brancos2 { get; set; }
        public string Ocorrencias { get; set; }

    }
}
