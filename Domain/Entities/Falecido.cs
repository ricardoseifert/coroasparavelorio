﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class Falecido : IPersistentEntity
    {
        public string HoraSepultamento
        {
            get
            {
                try
                {
                    return this.DataSepultamento.Value.ToShortTimeString();
                }
                catch
                {
                    return "";
                }
                
            }
        }

        public DateTime? DataSepultamentoShort
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.DataSepultamento.Value.ToShortDateString());
                }
                catch
                {
                    return null;
                }

            }
        }
    }
      
}
