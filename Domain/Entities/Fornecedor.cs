﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Configuration;
using System.Data.EntityClient;
using Domain.Repositories;
using DomainExtensions.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using System.Drawing;

namespace Domain.Entities
{
    public partial class Fornecedor : IPersistentEntity
    {
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<Administrador> administradorRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<NPSRating_NEW> npsNewREspository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        private IPersistentRepository<PedidoItem> peidoItensRepository;
        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;

        public enum FluxosPagamento
        {
            [Description("Não Informado")]
            NI = 99, 
            [Description("Quinzenal")]
            Quinzenal = 1,
            [Description("Mensal")]
            Mensal = 2,
            [Description("Semanal")]
            Semanal = 3,
            [Description("Diário")]
            Diario = 4
        }
        public FluxosPagamento FluxoPagamento
        {
            get { return (FluxosPagamento)this.FluxoPagamentoID; }
            set { this.FluxoPagamentoID = (int)value; }
        }
        public int TotalRepassesAberto
        {
            get
            { 
                return this.Repasses.Count(c => c.Status == Entities.Repasse.TodosStatus.AguardandoPagamento);
            }
        }

        public int Distancia;
        public string Caracteristicas
        {
            get
            {
                var retorno = "";
                var comma = "";
                foreach (var item in this.FornecedorCaracteristicas.ToList())
                {
                    retorno += comma + Domain.Helpers.EnumHelper.GetDescription(item.Caracteristica);
                    comma = ", ";
                }
                return retorno;
            }
        }
        public void AlocaFornecedorCPV(int PedidoID, int fornecedorID, int? AdministradorId, string Tipo)
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            administradorPedidoRepository = new AdministradorPedidoRepository(new ObjectContext(conStr));

            administradorRepository = new PersistentRepository<Administrador>(new ObjectContext(conStr));
            pedidoRepository = new PersistentRepository<Pedido>(new ObjectContext(conStr));
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(new ObjectContext(conStr));
            npsNewREspository = new PersistentRepository<NPSRating_NEW>(new ObjectContext(conStr));
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(new ObjectContext(conStr));
            repasseRepository = new PersistentRepository<Repasse>(new ObjectContext(conStr));
            peidoItensRepository = new PersistentRepository<PedidoItem>(new ObjectContext(conStr));
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(new ObjectContext(conStr));

            List<IPedido> Lst = new List<IPedido>();

            if (Tipo == "Cliente")
            {
                Lst.Add(pedidoRepository.Get(PedidoID));                
            }
            else
            {
                Lst.Add(pedidoEmpresaRepository.Get(PedidoID));                
            }

            var pedido = Lst.First();

            #region NOVO FORNECEDOR
            //----------------------------------------------------------------------
            if (!pedido.FornecedorID.HasValue)
            {
                pedido.FornecedorID = fornecedorID;

                #region INSERE PEDIDO PRODUCAO FORNECEDORES
                //----------------------------------------------------------------------
                // *** coloca o pedido diretamente em producao no dash do fornecedor
                pedido.StatusEntregaID = (int)TodosStatusEntrega.EntregaEmAndamento;
                pedido.DataAtualizacao = DateTime.Now;
                pedido.FornecedorEmProcessamento = false;

                if (Tipo == "Cliente")
                {
                    var PedidoSave = (Pedido)pedido;

                    if (PedidoSave.StatusProcessamento == Pedido.TodosStatusProcessamento.Criado)
                    {
                        PedidoSave.StatusProcessamento = Pedido.TodosStatusProcessamento.Processando;
                        PedidoSave.DataProcessamento = DateTime.Now;
                        PedidoSave.DataAtualizacao = DateTime.Now;
                    }

                    pedidoRepository.Save(PedidoSave);
                }
                else
                {
                    var PedidoSave = (PedidoEmpresa)pedido;

                    if (PedidoSave.Status == PedidoEmpresa.TodosStatus.NovoPedido)
                    {
                        PedidoSave.Status = PedidoEmpresa.TodosStatus.PedidoEmAnalise;
                        PedidoSave.DataProcessamento = DateTime.Now;
                        PedidoSave.DataAtualizacao = DateTime.Now;
                    }

                    pedidoEmpresaRepository.Save((PedidoEmpresa)pedido);
                }
                
                //----------------------------------------------------------------------
                #endregion


                if(AdministradorId != null &&  Tipo == "Cliente")
                {
                    #region DADOS DE NPS NEW
                    //----------------------------------------------------------------------
                    npsNewREspository.Save(new NPSRating_NEW { PedidoID = pedido.ID, AdministradorID = (int)AdministradorId, DataCriacao = DateTime.Now, Nota = 0, Guid = pedido.Codigo, FornecedorID = fornecedorID, EmailEnviado = false });
                    //----------------------------------------------------------------------
                    #endregion
                }

                #region FORNECEDOR PREÇOS
                //----------------------------------------------------------------------
                var fornecedorprecos = fornecedorPrecoRepository.GetByExpression(c => c.FornecedorID == fornecedorID).ToList();

                if (fornecedorprecos != null)
                {
                    Dictionary<int, int> LstProdutoTamanhoIDPedidoItemID = new Dictionary<int, int>();

                    if (Tipo == "Cliente")
                    {
                        foreach (var item in peidoItensRepository.GetByExpression(r => r.PedidoID == pedido.ID).ToList())
                        {
                            LstProdutoTamanhoIDPedidoItemID.Add(item.ProdutoTamanhoID, item.ID);
                        }
                    }
                    else
                    {
                        var PedidoEmpresa = (PedidoEmpresa)pedido;

                        LstProdutoTamanhoIDPedidoItemID.Add((int)PedidoEmpresa.ProdutoTamanhoID, PedidoEmpresa.ID);
                    }
                    
                    foreach (var ProdutoTamanhoIDPedidoItemID in LstProdutoTamanhoIDPedidoItemID)
                    {
                        #region FORNECEDOR REPASSE
                        //----------------------------------------------------------------------
                        var repasseRetorno = new List<Repasse>();

                        if (Tipo == "Cliente")
                        {
                            repasseRetorno.AddRange(repasseRepository.GetByExpression(c => c.PedidoItemID == ProdutoTamanhoIDPedidoItemID.Value && c.StatusID != (int)Repasse.TodosStatus.Cancelado).ToList());
                        }
                        else
                        {
                            repasseRetorno.AddRange(repasseRepository.GetByExpression(c => c.PedidoEmpresaID == ProdutoTamanhoIDPedidoItemID.Value && c.StatusID != (int)Repasse.TodosStatus.Cancelado).ToList());
                        }
                        
                        if (repasseRetorno.Count() == 0)
                        {
                            var repasse = new Repasse();
                            repasse.FornecedorID = pedido.FornecedorID;

                            if (Tipo == "Cliente")
                            {
                                repasse.PedidoItemID = ProdutoTamanhoIDPedidoItemID.Value;
                            }
                            else
                            {
                                repasse.PedidoEmpresaID = pedido.ID;
                            }

                            repasse.Status = Domain.Entities.Repasse.TodosStatus.AguardandoPagamento;
                            repasse.AdministradorID = AdministradorId;
                            repasse.DataCriacao = DateTime.Now;
                            repasse.Observacao = "\nRepasse criado de " + repasse.ValorRepasse.ToString("C2");

                            var ProdutoTamanho = produtoTamanhoRepository.Get(ProdutoTamanhoIDPedidoItemID.Key);

                            var fornecedorpreco = fornecedorprecos.FirstOrDefault(c => c.ProdutoID == ProdutoTamanho.ProdutoID && c.ProdutoTamanhoID == ProdutoTamanhoIDPedidoItemID.Key);
                            if (fornecedorpreco != null)
                            {
                                if (fornecedorpreco.Repasse.HasValue)
                                {
                                    if (fornecedorpreco.Repasse.Value > 0)
                                    {                                        
                                        repasse.ValorRepasse = fornecedorpreco.Repasse.Value;
                                    }
                                }
                            }
                            repasseRepository.Save(repasse);
                            //----------------------------------------------------------------------

                        }

                        #endregion
                    }

                    //#region INTEGRAÇÃO LINS

                    //PedidoFloricultura NovoPedidoFloricultura = new PedidoFloricultura();
                    //if (fornecedorID == 388)
                    //{
                    //    #region INSERE PEIDO LINS


                    //    NovoPedidoFloricultura.PedidoFloriculturaOrigem = PedidoFloriculturaOrigemRepository.Get(2);
                    //    NovoPedidoFloricultura.Codigo = Guid.NewGuid();
                    //    NovoPedidoFloricultura.Numero = CriarNumeroFloricultura();
                    //    NovoPedidoFloricultura.ClienteFloricultura = ClienteFloriculturaRepository.Get(31);
                    //    NovoPedidoFloricultura.StatusProcessamentoID = (int)PedidoFloricultura.TodosStatusProcessamento.Concluido;
                    //    NovoPedidoFloricultura.StatusPagamentoID = (int)PedidoFloricultura.TodosStatusPagamento.AguardandoPagamento;
                    //    NovoPedidoFloricultura.MeioPagamentoID = (int)PedidoFloricultura.MeiosPagamento.Faturamento;
                    //    NovoPedidoFloricultura.FormaPagamentoID = (int)PedidoFloricultura.FormasPagamento.Indefinido;
                    //    NovoPedidoFloricultura.StatusEntregaID = (int)PedidoFloricultura.TodosStatusEntrega.Pendente;
                    //    NovoPedidoFloricultura.ValorTotal = 0;
                    //    NovoPedidoFloricultura.HorarioEntrega = pedido.DataEntrega;
                    //    NovoPedidoFloricultura.CobrarNoLocal = false;
                    //    NovoPedidoFloricultura.ValorDesconto = 0;
                    //    NovoPedidoFloricultura.CidadeID = 8570;
                    //    NovoPedidoFloricultura.EstadoID = 26;
                    //    NovoPedidoFloricultura.DataCriacao = DateTime.Now;
                    //    NovoPedidoFloricultura.Obs = "Pedido automático inserido pelo sistema do CPV. NR = " + pedido.Numero;
                    //    NovoPedidoFloricultura.IdPedidoCPV = pedido.ID;

                    //    pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                    //    pedidoFloriculturaNotaRepository.Save(new PedidoFloriculturaNota
                    //    {
                    //        PedidoFloricultura = NovoPedidoFloricultura,
                    //        DataCriacao = DateTime.Now,
                    //        Observacoes = "Pedido Inserido automaticamente por conta da seleção da Flor Da Lins como Fornecedor."
                    //    });

                    //    #endregion

                    //    #region ADICIONA ITENS DO PEDIDO DA LINS

                    //    foreach (var Coroa in pedido.PedidoItens)
                    //    {
                    //        var ProdutoFloricultura = produtoFloriculturaRepository.GetByExpression(r => r.IdProdutoCPV == Coroa.ProdutoTamanho.Produto.ID).FirstOrDefault();

                    //        if (ProdutoFloricultura != null)
                    //        {
                    //            var IdTamanhoProdFloricultura = 0;
                    //            switch (Coroa.ProdutoTamanho.Tamanho.ID)
                    //            {
                    //                // PEQUENO
                    //                case 1:
                    //                case 4:
                    //                case 8:
                    //                case 11:
                    //                case 14:
                    //                case 17:
                    //                case 20:
                    //                    IdTamanhoProdFloricultura = 2;
                    //                    break;
                    //                // MEDIO
                    //                case 2:
                    //                case 5:
                    //                case 21:
                    //                case 9:
                    //                case 12:
                    //                case 15:
                    //                case 18:
                    //                    IdTamanhoProdFloricultura = 3;
                    //                    break;
                    //                // BANCO DO ESTADO DO RIO GRANDE DO SUL | 41
                    //                case 3:
                    //                case 6:
                    //                case 10:
                    //                case 13:
                    //                case 16:
                    //                case 19:
                    //                case 22:
                    //                    IdTamanhoProdFloricultura = 4;
                    //                    break;
                    //                default:
                    //                    IdTamanhoProdFloricultura = 5;
                    //                    break;
                    //            }

                    //            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == ProdutoFloricultura.ID && r.ProdutoTamanhoFloricultura.ID == IdTamanhoProdFloricultura).FirstOrDefault();
                    //            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                    //            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                    //            {
                    //                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                    //                Descricao = ProdutoFloricultura.Nome,
                    //                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                    //                Valor = (decimal)ValorTblRepasse.Repasses,
                    //                Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem
                    //            });
                    //        }
                    //        else
                    //        {
                    //            var ProdutoTamanhoFloricultura = ProdutoTamanhoPrecoFloriculturaRepository.GetByExpression(r => r.ProdutoFloricultura.ID == 26).FirstOrDefault();
                    //            var ValorTblRepasse = fornecedorPrecoRepository.GetByExpression(r => r.Fornecedor.ID == 388 && r.ProdutoID == Coroa.ProdutoTamanho.Produto.ID && r.ProdutoTamanho.Tamanho.ID == Coroa.ProdutoTamanho.Tamanho.ID).FirstOrDefault();

                    //            decimal ValorRepasse = 0;
                    //            if (ValorTblRepasse != null)
                    //            {
                    //                ValorRepasse = (decimal)ValorTblRepasse.Repasses;
                    //            }

                    //            PedidoItemFloriculturaRepository.Save(new PedidoItemFloricultura
                    //            {
                    //                PedidoFloricultura = pedidoFloriculturaRepository.Get(NovoPedidoFloricultura.ID),
                    //                Descricao = Coroa.ProdutoTamanho.Produto.Nome.ToUpper(),
                    //                ProdutoTamanhoPrecoFloricultura = ProdutoTamanhoFloricultura,
                    //                Valor = ValorRepasse,
                    //                Mensagem = "NR = " + pedido.Numero + " / " + Coroa.Mensagem,
                    //                Obs = "Produto inserido como personalizado por que não foi encontrado na base da floricultura. Produto CPV: " + Coroa.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + Coroa.ProdutoTamanho.Produto.Descricao.ToUpper()
                    //            });
                    //        }
                    //    }

                    //    #endregion

                    //    #region ADICIONAR OS VALORES DOS ITENS AO PEDIDO DA LINS
                    //    //----------------------------------------------------------------------
                    //    var SomaItenPedidoFloricultura = NovoPedidoFloricultura.PedidoItemFloricultura.Sum(r => r.Valor);
                    //    NovoPedidoFloricultura.ValorTotal = SomaItenPedidoFloricultura;
                    //    //----------------------------------------------------------------------
                    //    #endregion

                    //}

                    //#region  ADICIONA DADOS DE ENTREGA PEDIDO LINS
                    ////----------------------------------------------------------------------
                    //NovoPedidoFloricultura.PessoaHomenageada = pedido.PessoaHomenageada;
                    //NovoPedidoFloricultura.EstadoID = pedido.Estado.ID;
                    //NovoPedidoFloricultura.CidadeID = pedido.Cidade.ID;
                    //NovoPedidoFloricultura.ComplementoLocalEntrega = pedido.ComplementoLocalEntrega;

                    //if (pedido.LocalID != null)
                    //{
                    //    NovoPedidoFloricultura.LocalID = pedido.LocalID;
                    //}
                    //pedidoFloriculturaRepository.Save(NovoPedidoFloricultura);

                    //NovoPedidoFloricultura.EnviarPedidoEmail();
                    ////----------------------------------------------------------------------
                    //#endregion

                    //#endregion
                }
                //----------------------------------------------------------------------
                #endregion
                
            }
            //----------------------------------------------------------------------
            #endregion
        }
        public Bitmap GerarQrCodeEntraga(int PedidoID, string TipoPedido)
        {
            var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
            var entityBuilder = new EntityConnectionStringBuilder();

            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            administradorPedidoRepository = new AdministradorPedidoRepository(new ObjectContext(conStr));

            pedidoRepository = new PersistentRepository<Pedido>(new ObjectContext(conStr));
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(new ObjectContext(conStr));

            Pedido PedidoCPV = null;
            PedidoEmpresa PedidoEmpresa = null;
            string Link = "https://www.coroasparavelorio.com.br/Fornecedor";

            if (TipoPedido == "Cliente")
            {
                PedidoCPV = pedidoRepository.Get(PedidoID);
                Link += "?Pedido=" + PedidoCPV.Codigo;
            }
            else
            {
                PedidoEmpresa = pedidoEmpresaRepository.Get(PedidoID);
                Link += "?PedidoEmpresa=" + PedidoEmpresa.ID;
            }

            if(PedidoCPV != null)
            {
                return new Domain.Core.QRCode().GerarQRCode(1000, 1000, Link);
            }
            else
            {
                return new Domain.Core.QRCode().GerarQRCode(1000, 1000, Link);
            }
        }
    }
}
