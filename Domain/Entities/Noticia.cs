﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class Noticia : IPersistentEntity
    {
        public enum Tipo
        {
            MERCADO = 1,
            MIDIA = 2
        }

        public Tipo Tipos
        {
            get { return (Tipo)this.TipoID; }
            set { this.TipoID = (int)value; }
        }

    }
}
