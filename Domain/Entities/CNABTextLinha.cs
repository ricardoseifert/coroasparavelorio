﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;

namespace Domain.Entities
{
    public partial class CNABTextLinha
    {
     
        public string CodigoBanco { get; set; }
        public string CodigoLote { get; set; }
        public string TipoRegistro { get; set; }
        public string NumeroRegistro { get; set; }
        public string Segmento { get; set; }
        public string TipoDeSegmento { get; set; }
        public string Camara { get; set; }
        public string NrBancoFavorecido { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string ContaDigito { get; set; }
        public string NomeFavorecido { get; set; }
        public string SeuNumero { get; set; }
        public string DataPagamento { get; set; }
        public string MoedaTipo { get; set; }
        public string CodigoISPB { get; set; }
        public string Zeros { get; set; }
        public string Valor { get; set; }
        public string NossoNumero { get; set; }
        public string Brancos1 { get; set; }
        public string DataEfetiva { get; set; }
        public string ValorEfetivo { get; set; }
        public string FinalidadeDetalhe { get; set; }
        public string Brancos2 { get; set; }
        public string NumDocumento { get; set; }
        public string NumInscricao { get; set; }
        public string DocumentoForn { get; set; }
        public string FinalidadeDocStatusFuncionario { get; set; }
        public string FinalidadeTed { get; set; }
        public string Brancos3 { get; set; }
        public string Aviso { get; set; }
        public string Ocorrencias { get; set; }
    }
}
