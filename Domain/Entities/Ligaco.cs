﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;
using Domain.Core;

namespace Domain.Entities
{
    public partial class Ligaco : IPersistentEntity
    {
        public enum TodosStatusLigacoes
        {
            [Description("Agente Conectado")]
            AgenteConectado = 1,
            [Description("Finalizada")]
            Finalizada = 2,
            [Description("Não Atendida")]
            NaoAtendida = 3
        }
    }
}
