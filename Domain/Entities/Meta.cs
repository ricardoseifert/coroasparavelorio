﻿using DomainExtensions.Entities;

namespace Domain.Entities {
    public partial class Meta : IPersistentEntity {
        public int ID { get { return Id; } }

        public enum TodosStatus{
            Ativa = 1,
            Desativada = 2,
            Pausada = 3

        }

    }

}
