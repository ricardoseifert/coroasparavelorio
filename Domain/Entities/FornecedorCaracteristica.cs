﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;

namespace Domain.Entities
{
    public partial class FornecedorCaracteristica : IPersistentEntity
    {

        public enum Todas
        {
            [Description("24 Horas")]
            VINTEQUARTOHORAS = 1,
            [Description("Entrega própria")]
            ENTREGAPROPRIA = 2,
            [Description("Faixa impressa")]
            FAIXAIMPRESSA = 3,
            [Description("Frete grátis")]
            FRETEGRATIS = 4,
            [Description("Manda foto por Whatsapp")]
            FOTOWHATSAPP = 5,
            [Description("Bloco de confirmação")]
            BLOCOCONFIRMACAO = 6,
            [Description("Opção de cor de faixa")]
            CORFAIXA = 7,
            [Description("Acesso ao sistema")]
            ACESSOSISTEMA = 8
        }

        public Todas? Caracteristica
        {
            get { return (Todas)this.CaracteristicaID; }
            set { this.CaracteristicaID = (int)value; }
        }

    }
}