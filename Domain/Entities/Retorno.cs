﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Service;

namespace Domain.Entities
{
    public class Retorno
    {

        public enum Tipos
        {
            Outros = 0,
            Invalido = 1,
            Erro = 2,
            Sucesso = 3
        }

        public Tipos Tipo;
        public BoletoService.Ocorrencias Ocorrencia;
        public int Linha;
        public string Mensagem;

    }
}
