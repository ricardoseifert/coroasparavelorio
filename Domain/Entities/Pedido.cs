﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainExtensions.Entities;
using System.ComponentModel;
using System.Web;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Configuration;
using System.Data.EntityClient;
using Domain.Core;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Dynamic;
using System.Net;
using System.IO;
using Domain.Core.YourViews.Return;

namespace Domain.Entities
{
    public partial class Pedido : IPersistentEntity, IPedido
    {

        private IPersistentRepository<Pedido> pedidoRepository;
        private LogMandrillRepository logMandrillRepositor;
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<NPSRating_NEW> npsNewREspository;

        private string conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
        private EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

        public Pedido()
        {
        }
        public int AcaoID { get; set; }
        public enum TodosStatusPagamento
        {
            [Description("Aguardando Pagamento")]
            AguardandoPagamento = 1,
            [Description("Pagamento Efetuado")]
            Pago = 2,
            [Description("Cancelado")]
            Cancelado = 3,
            [Description("Estornado")]
            Estornado = 4,
            [Description("Prejuízo")]
            Prejuizo = 5,
            [Description("Protestado")]
            Protestado = 6,
            [Description("Serasa")]
            Serasa = 7
        }
        public enum TodosStatusNFs
        {
            [Description("Não Emitida")]
            NaoEmitida = 1,
            [Description("Processando")]
            Processando = 2,
            [Description("Emitida")]
            Emitida = 3
        }
        public enum TodosStatusProcessamento
        {
            [Description("Pendente/Novo")]
            Criado = 1,
            [Description("Em processamento")]
            Processando = 2,
            [Description("Concluído")]
            Concluido = 3,
            [Description("Cancelado")]
            Cancelado = 4,
            [Description("Em produção")]
            Producao = 5,
            [Description("Removido")]
            Removido = 99
        }
        public enum TodosStatusContatoNao
        {
            [Description("Não Informado")]
            NI = 99,
            [Description("Não Atendeu")]
            NaoAtendeu = 1,
            [Description("Exterior")]
            Exterior = 2,
            [Description("Telefone Incorreto")]
            TelefoneIncorreto = 3,
            [Description("Falta de Tempo")]
            FaltaTempo = 4,
            [Description("Comercial")]
            Comercial = 5
        }
        public enum TodosStatusContato
        {
            [Description("Não Informado")]
            NI = 99,
            [Description("SIM")]
            Sim = 1,
            [Description("NÃO")]
            Nao = 2
        }
        public enum TipoCheckout
        {
            [Description("UmClick")]
            UmClick = 0,
            [Description("Regular")]
            Regular = 1
        }
        public enum MeiosPagamento
        {
            [Description("(não informado)")]
            Indefinido = 0,
            [Description("Cartão de Crédito via Cielo")]
            Cielo = 1,
            [Description("Cartão de Crédito via Redeshop")]
            RedeShop = 2,
            [Description("PagSeguro")]
            PagSeguro = 3,
            [Description("PayPal")]
            PayPal = 4,
            [Description("Boleto Bancário Itaú")]
            Boleto = 5,
            [Description("Itaú Shopline")]
            Shopline = 6,
            [Description("Cartão de Crédito manual")]
            CartaoManual = 7,
            [Description("Depósito Bancário Bradesco")]
            DepositoBradesco = 8,
            [Description("Depósito Bancário Banco do Brasil")]
            DepositoBB = 9,
            [Description("Depósito Bancário Itaú")]
            DepositoItau = 10,
            [Description("Depósito Santander")]
            DepositoSantander = 11,
            [Description("Depósito Caixa Econômica")]
            DepositoCaixaEconomica = 12,
            [Description("Dinheiro/Cheque")]
            DinheiroCheque = 13,
            [Description("Permuta")]
            Tradaq = 14,
            [Description("Cartão de Crédito via RedeCard")]
            RedeCard = 15,
            [Description("Cartão de Crédito via PagarME")]
            PagarMe = 16,
            [Description("Boleto Pagar Me")]
            BoletoPagarMe = 17
        }
        public enum FormasPagamento
        {
            [Description("(não informado)")]
            Indefinido = 0,
            [Description("Cartão de Crédito")]
            CartaoCredito = 1,
            [Description("Transferência Bancária")]
            TransferenciaBancaria = 2,
            [Description("Boleto Bancário")]
            Boleto = 3,
            [Description("Saldo PagSeguro/PayPal")]
            Saldo = 4,
            [Description("Outros")]
            Outros = 5,
            [Description("Envio via e-mail")]
            Envio = 6,
            [Description("Depósito Bancário")]
            DepositoBancario = 7,
            [Description("Dinheiro/Cheque")]
            DinheiroCheque = 8,
            [Description("Tradaq")]
            Tradaq = 9,
            [Description("Boleto Pagar Me")]
            BoletoPagarMe = 10
        }
        public TodosStatusPagamento StatusPagamento
        {
            get { return (TodosStatusPagamento)this.StatusPagamentoID; }
            set { this.StatusPagamentoID = (int)value; }
        }
        public TodosStatusNFs StatusNF
        {
            get { return (TodosStatusNFs)this.StatusNfID; }
            set { this.StatusNfID = (int)value; }
        }
        public TodosStatusProcessamento StatusProcessamento
        {
            get { return (TodosStatusProcessamento)this.StatusProcessamentoID; }
            set { this.StatusProcessamentoID = (int)value; }
        }

        public TodosStatusCancelamento StatusCancelamento
        {
            get { return (TodosStatusCancelamento)this.StatusCancelamentoID; }
            set { this.StatusCancelamentoID = (int)value; }
        }

        public TodosStatusEntrega StatusEntrega
        {
            get { return (TodosStatusEntrega)this.StatusEntregaID; }
            set { this.StatusEntregaID = (int)value; }
        }

        public MeiosPagamento MeioPagamento
        {
            get { return (MeiosPagamento)this.MeioPagamentoID; }
            set { this.MeioPagamentoID = (int)value; }
        }

        public FormasPagamento FormaPagamento
        {
            get { return (FormasPagamento)this.FormaPagamentoID; }
            set { this.FormaPagamentoID = (int)value; }
        }

        public TodosStatusContato StatusContato
        {
            get { return (TodosStatusContato)this.QualidadeStatusContatoID; }
            set { this.QualidadeStatusContatoID = (int)value; }
        }

        public TodosStatusContatoNao StatusContatoNao
        {
            get { return (TodosStatusContatoNao)this.QualidadeStatusContatoNaoID; }
            set { this.QualidadeStatusContatoNaoID = (int)value; }
        }
        public Boleto Boleto
        {
            get
            {
                return this.Boletoes.FirstOrDefault(b => b.StatusPagamentoID == (int)Entities.Boleto.TodosStatusPagamento.AguardandoPagamento || b.StatusPagamentoID == (int)Entities.Boleto.TodosStatusPagamento.Vencido);
            }
        }

        public BoletoPagarMe BoletoPagarMeReturn
        {
            get
            {
                return this.BoletoPagarMes.FirstOrDefault(b => b.StatusPagamentoID == (int)Entities.Boleto.TodosStatusPagamento.AguardandoPagamento || b.StatusPagamentoID == (int)Entities.Boleto.TodosStatusPagamento.Vencido);
            }
        }

        public DateTime? DataCobranca
        {
            get
            {                
                if(this.BoletoPagarMeReturn != null && this.BoletoPagarMeReturn.DataVencimento != null)
                    return this.BoletoPagarMeReturn.DataVencimento.Value;

                return null;
            }
        }

        public List<Repasse> Repasse
        {
            get
            {
                List<Repasse> repasses = new List<Repasse>();
                foreach (var item in this.PedidoItems)
                {
                    repasses = item.Repasses.ToList(); ;
                }
                return repasses;
            }
        }
        public int TotalRepassesAberto
        {
            get
            {
                int total = 0;
                foreach (var item in this.PedidoItems)
                {
                    total += item.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || c.Status == Domain.Entities.Repasse.TodosStatus.Pago || c.Status == Domain.Entities.Repasse.TodosStatus.PagamentoEmProcessamento || c.Status == Domain.Entities.Repasse.TodosStatus.Fechamento);
                }
                return total;
            }
        }
        public int TotalRepassesPagos
        {
            get
            {
                int total = 0;
                foreach (var item in this.PedidoItems)
                {
                    total += item.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.Pago);
                }
                return total;
            }
        }
        public bool RepassesPagos
        {
            get
            {
                int total = 0;
                foreach (var item in this.PedidoItems)
                {
                    total += item.Repasses.Count(c => c.Status == Domain.Entities.Repasse.TodosStatus.Pago);
                }
                return total == this.PedidoItems.Count;
            }
        }
        
        public bool JaSaiuEntrega
        {
            get
            {
                if (this.AdministradorPedidoes.Where(r => r.AcaoID == (int)Domain.Entities.AdministradorPedido.Acao.SaiuParaEntrega).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
       
        public int QtdFornecedores
        {

            get
            {
                if (this.Local != null)
                {
                    return this.Local.FornecedorLocals.Count;
                }
                else {
                    return 1;
                }
            }
        }
        public List<int> CategoriaDosProdutos
        {
            get {
                return this.PedidoItems.Select(r => r.ProdutoTamanho.Produto.TipoID).ToList<int>();
            }            
        }

        public int? AdministradorPedidoID
        {
            get
            {
                try
                {
                    return this.Administrador.ID;
                }
                catch
                {
                    return null;
                }
            }
        }

        public string AdministradorNome
        {
            get
            {
                try
                {
                    return this.Administrador.Nome;
                }
                catch
                {
                    return null;
                }
            }
        }

        public int RepasseId { get; set; }
        public List<string> NomeProdutos
        {
            get
            {
                List<string> NomeProdutos = new List<string>();

                foreach (var item in this.PedidoItems)
                {
                    NomeProdutos.Add(item.ProdutoTamanho.Produto.Nome + " " + item.ProdutoTamanho.Tamanho.Nome);
                }

                return NomeProdutos;
            }

        }

        public string NomeFantasia
        {
            get
            {
                return this.Cliente.NomeFantasia + this.Cliente.Nome;
            }
        }

        public string NomeDoProduto
        {
            get
            {
                string Produtos = "";
                foreach (var item in this.PedidoItems)
                {
                    Produtos += item.ProdutoTamanho.Produto.Nome + " " + item.ProdutoTamanho.Tamanho.NomeCompleto + " ";
                }

                return Produtos;
            }
        }

        public bool ProdutoKitBB
        {
            get
            {
                return false;
            }
        }

        public decimal ValorRepasseTotal
        {
            get
            {
                decimal total = 0;
                foreach (var item in this.PedidoItems)
                {
                    var repasse = item.Repasses.Where(c => c.Status == Domain.Entities.Repasse.TodosStatus.AguardandoPagamento || c.Status == Domain.Entities.Repasse.TodosStatus.Pago).FirstOrDefault();
                    if (repasse != null)
                    {
                        total += repasse.ValorRepasse;
                    }
                }
                return total;
            }
        }
        //public void EnviarPedidoEmail()
        //{
        //    EnviarPedidoEmail(true, true);
        //}
        public void EnviarEmailCancelamento()
        {
            var itenspedido = "";
            foreach (var item in this.PedidoItems)
            {
                itenspedido += "<tr>";
                itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>";
                itenspedido += "<img src='[URL]/Produtos/" + item.ProdutoTamanho.ProdutoID + "/thumb/" + item.ProdutoTamanho.Produto.Foto + "' width='75'/>";
                itenspedido += "</td>";
                if (item.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                {
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>Produto Personalizado</td>";
                }
                else
                {
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + item.ProdutoTamanho.Produto.Nome + "</td>";
                }
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + item.ProdutoTamanho.Tamanho.Nome + "</td>";
                if (item.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Personalizado)
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>-</td>";
                }
                else
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + item.Valor.ToString("C2") + "</td>";
                }
                itenspedido += "</tr>";
                itenspedido += "<tr>";
                if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;' colspan='4'><b>Mensagem do Cartão:</b> " + item.Mensagem + "</td>";
                }
                else
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;' colspan='4'><b>Faixa de Homenagem:</b> " + item.Mensagem + "</td>";
                }
                itenspedido += "</tr>";
            }

            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/pedido-cancelado.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/pedido-cancelado.htm"));
                remetente = "Laços Flores";
            }

            if (String.IsNullOrEmpty(this.NomeSolicitante))
                html = html.Replace("[NOME]", this.Cliente.Nome.ToUpper());
            else
                html = html.Replace("[NOME]", this.NomeSolicitante.ToUpper());
            if (String.IsNullOrEmpty(this.EmailSolicitante))
                html = html.Replace("[EMAIL]", this.Cliente.Email.ToLower());
            else
                html = html.Replace("[EMAIL]", this.EmailSolicitante.ToLower());
            var celular = "";
            if (!String.IsNullOrEmpty(this.Cliente.CelularContato))
                celular = "<br /><br /><font style='color: #757575; font-weight: bold;'>Celular:</font> " + (this.Cliente.CelularContato.Length == 13 ? this.Cliente.CelularContato.Insert(9, "-") : this.Cliente.CelularContato.Insert(10, "-"));
            html = html.Replace("[TELEFONE]", (this.Cliente.TelefoneContato.Length == 13 ? this.Cliente.TelefoneContato.Insert(9, "-") : this.Cliente.TelefoneContato) + celular);
            html = html.Replace("[NUMEROPEDIDO]", this.Numero);
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy") + " às" + this.DataCriacao.ToString("HH:mm"));
            html = html.Replace("[STATUSPEDIDO]", Domain.Helpers.EnumHelper.GetDescription(this.StatusPagamento));
            html = html.Replace("[NOMERAZAO]", (this.Cliente.Tipo == Entities.Cliente.Tipos.PessoaFisica) ? this.Cliente.Nome.ToUpper() : this.Cliente.RazaoSocial.ToUpper());
            if (this.Cliente.Exterior)
            {
                html = html.Replace("[DOCUMENTO]", "Não informado");
                html = html.Replace("[ENDERECO]", "Exterior");
            }
            else
            {
                string doc = "";
                if (Cliente.Tipo == Cliente.Tipos.PessoaJuridica)
                {
                    if (this.Cliente.Documento.Length == 18 || string.IsNullOrWhiteSpace(this.Cliente.Documento))
                    {
                        doc = this.Cliente.Documento;
                    }
                    else {
                        doc = this.Cliente.Documento.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-");
                    }
                }
                else {
                    if (this.Cliente.Documento.Length == 14 || string.IsNullOrWhiteSpace(this.Cliente.Documento))
                    {
                        doc = this.Cliente.Documento;
                    }
                    else
                    {
                        doc = this.Cliente.Documento.Insert(3, ".").Insert(7, ".").Insert(11, "-");
                    }
                }
                html = html.Replace("[DOCUMENTO]", doc + ((this.Cliente.Tipo == Entities.Cliente.Tipos.PessoaJuridica) ? " IE " + this.Cliente.InscricaoEstadual.ToUpper() : ""));
                html = html.Replace("[ENDERECO]", Cliente.EnderecoCompleto + " - CEP " + (Cliente.CEP.Length == 9 ? Cliente.CEP : (!string.IsNullOrWhiteSpace(Cliente.CEP) ? Cliente.CEP.Insert(5, "-") : "")));
            }
            html = html.Replace("[HOMENAGEADO]", this.PessoaHomenageada);
            html = html.Replace("[DATAENTREGA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " às " + this.DataSolicitada.ToString("HH:mm"));
            if (this.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO")
            {
                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    if (this.LocalEntrega != null)
                    {
                        html = html.Replace("[LOCAL]", this.LocalEntrega.ToUpper());
                    }
                    else
                    {
                        html = html.Replace("[LOCAL]", "");
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        html = html.Replace("[LOCAL]", this.LocalEntrega.ToUpper());
                    }
                    else
                    {
                        html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper());
                    }
                }
            }
            else
            {
                if (this.OrigemSite.ToUpper() == "COROAS PARA VELÓRIO")
                {
                    if (string.IsNullOrEmpty(LocalID.ToString()))
                    {
                        html = html.Replace("[LOCAL]", this.LocalEntrega.ToUpper());
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Local.ToString()))
                        {
                            html = html.Replace("[LOCAL]", this.LocalEntrega.ToUpper());
                        }
                        else
                        {
                            html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper());
                        }
                    }
                }
            }
            html = html.Replace("[CIDADEENTREGA]", this.Cidade.NomeCompleto);
            html = html.Replace("[VALORSUBTOTAL]", (this.ValorTotal + this.ValorDesconto).ToString("C2"));
            html = html.Replace("[VALORDESCONTO]", this.ValorDesconto.ToString("C2"));
            html = html.Replace("[VALORTOTAL]", this.ValorTotal.ToString("C2"));
            html = html.Replace("[FORMAPAGAMENTO]", Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento));
            html = html.Replace("[IMGFORMAPAGAMENTO]", Domain.Core.Funcoes.GeraRotuloMVC(Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento)));

            html = html.Replace("[ITENSPEDIDO]", itenspedido);
            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);

            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        remetente,
                        this.Cliente.Email,
                        this.Cliente.Nome,
                        Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_CANCELAMENTO_PEDIDO,
                        remetente,
                        this.Cliente.EmailSecundario,
                        "[" + remetente.ToUpper() + "] - O pedido #" + this.Numero + " foi cancelado com sucesso!",
                        html,
                        true
                    );
            }

            if (!String.IsNullOrEmpty(this.EmailSolicitante))
            {
                if (Domain.Core.Funcoes.ValidaEmail(this.EmailSolicitante.Trim()))
                {
                    if (this.Cliente.Email != this.EmailSolicitante)
                    {
                        Domain.Core.Funcoes.EnviaEmail
                            (
                                Domain.Core.Configuracoes.EMAIL_NOREPLY,
                                remetente,
                                this.EmailSolicitante,
                                this.NomeSolicitante,
                                Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                                remetente,
                                "",
                                "[" + remetente.ToUpper() + "] - O pedido #" + this.Numero + " foi cancelado com sucesso!",
                                html,
                                true
                            );
                    }
                }
            }
            Core.Funcoes.EnviaEmail
                (
                    Core.Configuracoes.EMAIL_NOREPLY,
                    remetente,
                    Core.Configuracoes.EMAIL_RECEBIMENTO_PEDIDOS,
                    "Atendimento",
                    null,
                    remetente,
                    "",
                    "[" + remetente.ToUpper() + "] - O pedido #" + this.Numero + " foi cancelado com sucesso!",
                    html,
                    true
                );
        }

        public void EnviarPedidoEmailBoleto()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var Emails = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-boleto";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Seu boleto do pedido #" + this.Numero + "."
                    //content = "Seu Boleto Vence dia " + this.BoletoPagarMeReturn.DataVencimento.Value.Day.ToString("00") + "/" + this.BoletoPagarMeReturn.DataVencimento.Value.Month.ToString("00") + "!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();

                string name = this.Cliente.Nome;

                if (this.Cliente.Nome.Contains(" "))
                    name = this.Cliente.Nome.Substring(0, this.Cliente.Nome.IndexOf(" "));

                Lstvars.Add(new varsMandrill { name = "NOME", content = name });
                Lstvars.Add(new varsMandrill { name = "NUMEROPEDIDO", content = "#" + this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString("dd/MM/yyyy") });
                Lstvars.Add(new varsMandrill { name = "DIAVENCIMENTO", content = this.BoletoPagarMeReturn.DataVencimento.Value.ToString("dd/MM/yyyy") });

                var Produtos = "";
                foreach(var Itens in this.PedidoItems)
                {
                    Produtos += Itens.ProdutoTamanho.Produto.Nome + " " + Itens.ProdutoTamanho.Tamanho.Nome + "<br />";
                }

                Lstvars.Add(new varsMandrill { name = "PRODUTO", content = Produtos });
                Lstvars.Add(new varsMandrill { name = "VALORPEDIDO", content = this.ValorTotal.ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "CODIGOBOLETO", content = this.BoletoPagarMeReturn.BarCode });
                Lstvars.Add(new varsMandrill { name = "BOLETO", content = "http://www.coroasparavelorio.com.br/boleto?codigo=" + this.Codigo });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Seu boleto do pedido #" + this.Numero + ".",
                    //subject = "Seu Boleto Vence dia " + this.BoletoPagarMeReturn.DataVencimento.Value.Day.ToString("00") + "/" + this.BoletoPagarMeReturn.DataVencimento.Value.Month.ToString("00") + "!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {

                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.PrimeiroBoleto.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }


                #endregion
            }
        }
        public void EnviarPedidoEmailNovo()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var Emails = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-pedido-recebido2";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Seu pedido #" + this.Numero + " foi recebido com sucesso!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Cliente.Nome });
                Lstvars.Add(new varsMandrill { name = "NUMEROPEDIDO", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAENTREGA", content = this.DataSolicitada.ToString() });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });

                Lstvars.Add(new varsMandrill { name = "SUBTOTAL", content = this.PedidoItems.Sum(r => r.Valor).ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "DESCONTO", content = this.ValorDesconto.ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "VALORTOTAL", content = this.ValorTotal.ToString("C2") });

                var Produtos = "";
                var Frases = "";
                foreach(var Iten in this.PedidoItems)
                {
                    Produtos += "<tr> <td width='20 % '> <img src='https://www.coroasparavelorio.com.br/content/produtos/" + Iten.ProdutoTamanho.ProdutoID + "/thumb/" + Iten.ProdutoTamanho.Produto.Foto + "' style='padding-right: 10px; max-width: 100px; font-family:open sans,helvetica neue,helvetica,arial,sans-serif;'/> </td> <td width='40%'>" + Iten.ProdutoTamanho.Produto.Nome + "</td> <td width='20%' style='text-align:center'>" + Iten.ProdutoTamanho.Tamanho.Nome + "</td> <td width='20%' style='text-align:center'>" + Iten.Valor.ToString("C2") + "</td> </tr>";
                    Frases += "<b>Frase de Homenagem: </b>" + Iten.Mensagem + "<br />";
                }

                if(this.ValorDesconto > 0)
                {
                    Lstvars.Add(new varsMandrill { name = "DESCONTO", content = "<tr id='desconto'><td><b>Desconto:</b><span style='color:red'> " + this.ValorDesconto.ToString("C2") + "</span></td></tr>" });
                }

                Lstvars.Add(new varsMandrill { name = "frasedehomenagem", content = Frases });

                Lstvars.Add(new varsMandrill { name = "DESCRPRODUTO", content = Produtos });
                Lstvars.Add(new varsMandrill { name = "homenageado", content = this.PessoaHomenageada });

                var FormaPagamento = Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento);
                if (FormaPagamento == "Boleto Pagar Me")
                    FormaPagamento = "Boleto";

                Lstvars.Add(new varsMandrill { name = "formapagamento", content = FormaPagamento });

                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    if (LocalEntrega == null)
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        if (LocalEntrega.ToUpper() == "")
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                        }
                        else
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                        }
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.Local.Titulo.ToUpper() });
                    }
                }


                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Seu pedido #" + this.Numero + " foi recebido com sucesso!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "atendimento@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Pedido.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }
                

                #endregion
            }
        }

        public void EnviarEmailPagamento()
        {
            var itenspedido = "";
            foreach (var item in this.PedidoItems)
            {

                itenspedido += "<tr>";
                itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>";
                itenspedido += "<img src='[URL]/Produtos/" + item.ProdutoTamanho.ProdutoID + "/thumb/" + item.ProdutoTamanho.Produto.Foto + "' width='75'/>";
                itenspedido += "</td>";
                if (item.ProdutoTamanho.Produto.Tipo == Domain.Entities.Produto.Tipos.Personalizado)
                {
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>Produto Personalizado</td>";
                }
                else
                {
                    itenspedido += "<td style='border-bottom: 1px solid #ecebe9;'>" + item.ProdutoTamanho.Produto.Nome + "</td>";
                }
                itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + item.ProdutoTamanho.Tamanho.Nome + "</td>";
                if (item.ProdutoTamanho.Produto.Tipo == Domain.Entities.Produto.Tipos.Personalizado)
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>-</td>";
                }
                else
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;'>" + item.Valor.ToString("C2") + "</td>";
                }
                itenspedido += "</tr>";
                itenspedido += "<tr>";
                if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;' colspan='4'><b>Mensagem do Cartão:</b> " + item.Mensagem + "</td>";
                }
                else
                {
                    itenspedido += "<td style='text-align: center; border-bottom: 1px solid #ecebe9;' colspan='4'><b>Faixa de Homenagem:</b> " + item.Mensagem + "</td>";
                }
                itenspedido += "</tr>";
            }

            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/pedido-pago.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/pedido-pago.htm"));
                remetente = "Laços Flores";
            }
            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOME]", this.Cliente.Nome);
            html = html.Replace("[NUMEROPEDIDO]", this.Numero);
            html = html.Replace("[STATUSPEDIDO]", Domain.Helpers.EnumHelper.GetDescription(this.StatusPagamento));
            html = html.Replace("[NOMERESPONSAVEL]", this.Cliente.Nome);
            html = html.Replace("[ENDERECO]", Cliente.Endereco + " , " + Cliente.Numero + " - " + Cliente.Bairro + " - " + Cliente.Cidade.Nome + " / " + Cliente.Estado.Sigla);
            html = html.Replace("[CEP]", this.Cliente.CEP.Insert(5, "-"));
            html = html.Replace("[VALORSUBTOTAL]", (this.ValorTotal + this.ValorDesconto).ToString("C2"));
            html = html.Replace("[VALORDESCONTO]", this.ValorDesconto.ToString("C2"));
            html = html.Replace("[VALORTOTAL]", this.ValorTotal.ToString("C2"));
            html = html.Replace("[FORMAPAGAMENTO]", Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento));
            html = html.Replace("[IMGFORMAPAGAMENTO]", Domain.Core.Funcoes.GeraRotuloMVC(Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento)));
            html = html.Replace("[ITENSPEDIDO]", itenspedido);

            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
                Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        remetente,
                        this.Cliente.Email,
                        this.Cliente.Nome,
                        Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                        "Coroas para Velório",
                        this.Cliente.EmailSecundario,
                        "[" + remetente.ToUpper() + "] - O pagamento para o pedido #" + this.Numero + " foi recebido com sucesso!",
                        html,
                        true
                    );

        }

        public void EnviarEmailPedidoEmProducao()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var Emails = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-pedido-em-produ-o";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Seu pedido #" + this.Numero + " está em produção!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Cliente.Nome });
                Lstvars.Add(new varsMandrill { name = "NUMEROPEDIDO", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAENTREGA", content = this.DataSolicitada.ToString() });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });

                Lstvars.Add(new varsMandrill { name = "SUBTOTAL", content = this.PedidoItems.Sum(r => r.Valor).ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "DESCONTO", content = this.ValorDesconto.ToString("C2") });
                Lstvars.Add(new varsMandrill { name = "VALORTOTAL", content = this.ValorTotal.ToString("C2") });

                var Produtos = "";
                var Frases = "";
                foreach (var Iten in this.PedidoItems)
                {
                    Produtos += "<tr> <td width='20 % '> <img src='https://www.coroasparavelorio.com.br/content/produtos/" + Iten.ProdutoTamanho.ProdutoID + "/thumb/" + Iten.ProdutoTamanho.Produto.Foto + "' style='padding-right: 10px; max-width: 100px; font-family:open sans,helvetica neue,helvetica,arial,sans-serif;'/> </td> <td width='40%'>" + Iten.ProdutoTamanho.Produto.Nome + "</td> <td width='20%' style='text-align:center'>" + Iten.ProdutoTamanho.Tamanho.Nome + "</td> <td width='20%' style='text-align:center'>" + Iten.Valor.ToString("C2") + "</td> </tr>";
                    Frases += "<b>Frase de Homenagem: </b>" + Iten.Mensagem + "<br />";
                }

                if (this.ValorDesconto > 0)
                {
                    Lstvars.Add(new varsMandrill { name = "DESCONTO", content = "<tr id='desconto'><td><b>Desconto:</b><span style='color:red'> " + this.ValorDesconto.ToString("C2") + "</span></td></tr>" });
                }

                Lstvars.Add(new varsMandrill { name = "frasedehomenagem", content = Frases });

                Lstvars.Add(new varsMandrill { name = "DESCRPRODUTO", content = Produtos });
                Lstvars.Add(new varsMandrill { name = "homenageado", content = this.PessoaHomenageada });

                var FormaPagamento = Domain.Helpers.EnumHelper.GetDescription(this.MeioPagamento);
                if (FormaPagamento == "Boleto Pagar Me")
                    FormaPagamento = "Boleto";

                Lstvars.Add(new varsMandrill { name = "formapagamento", content = FormaPagamento });

                if (string.IsNullOrEmpty(LocalID.ToString()))
                {
                    if (LocalEntrega == null)
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Local.ToString()))
                    {
                        if (LocalEntrega.ToUpper() == "")
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.ComplementoLocalEntrega.ToUpper() });
                        }
                        else
                        {
                            Lstvars.Add(new varsMandrill { name = "localentrega", content = this.LocalEntrega.ToUpper() });
                        }
                    }
                    else
                    {
                        Lstvars.Add(new varsMandrill { name = "localentrega", content = this.Local.Titulo.ToUpper() });
                    }
                }

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Seu pedido #" + this.Numero + " está em produção!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "atendimento@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.PedidoEmProdução.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }

                

                #endregion
            }
        }

        public void EnviarEmailEntregaNovo()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var Emails = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-confirma-o-de-entrega";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Seu pedido foi entregue com sucesso. Agora, avalie a nossa empresa!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                string name = this.Cliente.Nome;

                if (this.Cliente.Nome.Contains(" "))
                    name = this.Cliente.Nome.Substring(0, this.Cliente.Nome.IndexOf(" "));

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = name });
                Lstvars.Add(new varsMandrill { name = "NUMEROPEDIDO", content = "#" + this.Numero });
                Lstvars.Add(new varsMandrill { name = "RECEBIDOPOR", content = this.RecebidoPor });
                Lstvars.Add(new varsMandrill { name = "DATAENTREGA", content = this.DataEntrega.Value.ToString("dd/MM/yyyy") });
                Lstvars.Add(new varsMandrill { name = "GRAUPARENTESCO", content = this.ParentescoRecebidoPor });

                if(this.DataEntrega != null)
                    Lstvars.Add(new varsMandrill { name = "HORARIOENTREGA", content = this.DataEntrega.Value.ToString("HH:mm") });

                for (int i = 0; i < 11; i++)
                {
                    Lstvars.Add(new varsMandrill { name = "LINK" + i, content = "https://www.coroasparavelorio.com.br/NPS/AtualizaNPSNew?Identi=" + this.Codigo + "&Nota=" + i });
                }

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Seu pedido foi entregue com sucesso. Agora, avalie a nossa empresa!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "atendimento@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region GRAVA TABLE NPSRating

                entityBuilder.Provider = "System.Data.SqlClient";
                entityBuilder.ProviderConnectionString = conStr;
                npsREspository = new PersistentRepository<NPSRating>(new ObjectContext(conStr));
                npsNewREspository = new PersistentRepository<NPSRating_NEW>(new ObjectContext(conStr));

                var NPS = npsREspository.GetByExpression(p => p.PedidoID == this.ID).FirstOrDefault();
                if (NPS != null)
                {

                    NPS.DataUltimoEnvioEmail = DateTime.Now;
                    NPS.EmailEnviado = true;
                    npsREspository.Save(NPS);
                }
                var NPSNEW = npsNewREspository.GetByExpression(p => p.PedidoID == this.ID).FirstOrDefault();
                if (NPSNEW != null)
                {
                    NPSNEW.DataUltimoEnvioEmail = DateTime.Now;
                    NPSNEW.EmailEnviado = true;
                    npsNewREspository.Save(NPSNEW);
                }

                #endregion

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Entrega.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }
                

                #endregion
            }
        }

        public void EnviarEmailFotoProduro()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var Emails = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Emails = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-foto-do-produto";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Veja a foto do seu produto!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = Emails;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                string Fotos = "";
                foreach(var item in this.PedidoItems)
                {
                    Fotos += "<tr><td class=\"mcnImageContent\" valign=\"top\" style=\"padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center; \"><img align=\"center\" alt=\"\" src=\"" + "http://admin.coroasparavelorio.com.br/content/pedidos/" + this.Numero + "/" + item.ID + ".jpg" + "\" width=\"262\" style=\"max-width:262px; padding-bottom: 0; display: inline !important; vertical-align: bottom; \" class=\"mcnImage\"></td></tr>";
                }

                string name = this.Cliente.Nome;

                if (this.Cliente.Nome.Contains(" "))
                    name = this.Cliente.Nome.Substring(0, this.Cliente.Nome.IndexOf(" "));

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "FOTO", content = Fotos });
                Lstvars.Add(new varsMandrill { name = "NOME", content = name });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Veja a foto do seu produto!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "atendimento@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Emails,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Foto.ToString(), this.ID, null, reject_reason);
                    }
                }
                catch { }

                

                #endregion
            }
        }

        public void EnviarEmailEntrega()
        {
            var msg = "";

            string LocalDeEntrega = "LOCAL NÃO CADASTRADO";

            if (string.IsNullOrEmpty(LocalID.ToString()))
            {
                LocalDeEntrega = ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto;
            }
            else
            {
                LocalDeEntrega = Local.Titulo.ToUpper() + " - " + this.Cidade.NomeCompleto;
            }
            //if (!String.IsNullOrEmpty(LocalEntrega))
            //{
            //    LocalDeEntrega = LocalEntrega + " - " + this.Cidade.NomeCompleto;
            //}

            string RecebidaPor = "* não informado";
            if (!String.IsNullOrEmpty(this.RecebidoPor))
            {
                RecebidaPor = this.RecebidoPor;
            }

            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                msg = "<b>" +
                    this.Cliente.Nome.ToUpper() +
                    "</b>,<br><br>  O seu pedido foi entregue no local <b>" +
                    LocalDeEntrega.ToUpper() +
                    "</b>. Ele foi recebido por <b>" +
                    RecebidaPor.ToUpper() +
                    "</b>, no dia <b>" +
                    this.DataEntrega.Value.ToString("dd/MM/yyyy") +
                    "</b>";
            }
            else
            {
                if (this.PedidoItems.Count == 1)
                {
                    msg = "<b>" +
                        this.Cliente.Nome.ToUpper() +
                        "</b>,<br><br>  A sua coroa de flores foi entregue no local <b>" +
                        LocalDeEntrega.ToUpper() +
                        "</b>. Ela foi recebida por <b>" +
                        RecebidaPor.ToUpper() +
                        "</b>, no dia <b>" +
                        this.DataEntrega.Value.ToString("dd/MM/yyyy") +
                        "</b>";
                }
                else
                {
                    msg = "<b>" +
                        this.Cliente.Nome.ToUpper() +
                        "</b>,<br><br>  As suas coroas de flores foram entregues no local <b>" +
                        LocalDeEntrega.ToUpper() +
                        "</b>. Elas foram recebidas por <b>" +
                        RecebidaPor.ToUpper() +
                        "</b>, no dia <b>" +
                        this.DataEntrega.Value.ToString("dd/MM/yyyy") +
                        "</b>";
                }

            }

            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/entrega.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/entrega.htm"));
                remetente = "Laços Flores";
            }

            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOMEPARA]", this.Cliente.Nome);

            var NrPedido = this.PedidoItems.FirstOrDefault().PedidoID;

            var context = new COROASEntities();
            var NPSRepository = new PersistentRepository<Domain.Entities.NPSRating>(context);

            var IdPedido = this.PedidoItems.FirstOrDefault().PedidoID;

            var NPS = NPSRepository.GetByExpression(r => r.PedidoID == IdPedido).FirstOrDefault();

            if (NPS != null)
            {
                var Guid = NPS.Guid.ToString();

                html = html.Replace("[GUID]", Guid);
            }

            html = html.Replace("[MENSAGEM]", msg);

            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
                Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        remetente,
                        this.Cliente.Email,
                        this.Cliente.Nome,
                        Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                        remetente,
                        this.Cliente.EmailSecundario,
                        "Seu pedido foi entregue com sucesso. Agora, avalie a nossa empresa!",
                        html,
                        true
                    );
        }
        public void EnviarBoleto()
        {
            //ENVIAR EMAIL
            var assunto = "[COROAS PARA VELÓRIO] - Boleto para pagamento";
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/boleto.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/boleto.htm"));
                remetente = "Laços Flores";
            }

            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOME]", this.Cliente.Nome);
            html = html.Replace("[DATAVENCIMENTO]", this.Boleto.DataVencimento.ToString("dd/MM/yyyy"));
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy") + " às " + this.DataCriacao.ToString("HH:mm"));
            if (!String.IsNullOrEmpty(this.NFeLink))
            {
                assunto = "[" + remetente.ToUpper() + "] - Nota Fiscal e boleto para pagamento";
                html = html.Replace("[LINKNFE]", "<br><br>Este é o link para sua Nota Fiscal Eletrônica. Clique <a href='" + this.NFeLink + "'>aqui</a> para visualizar.<br>");
            }
            else
            {
                html = html.Replace("[LINKNFE]", "");
            }
            html = html.Replace("[LINK]", Domain.Core.Configuracoes.DOMINIO + "/boleto?codigo=" + this.Codigo);

            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
                Domain.Core.Funcoes.EnviaEmail("cobranca@lacosflores.com.br", remetente, this.Cliente.Email, this.Cliente.Nome, "cobranca@lacosflores.com.br", remetente, this.Cliente.EmailSecundario, assunto, html, true);

        }

        public YourViewsReturn EnviarYourViews()
        {
            return Domain.Service.YourViewsService.EnviaPedidoYourViews(this.ID);
        }

        public YourViewsReturn AtualizarYourViewsEntrega()
        {
            return Domain.Service.YourViewsService.AtualizarYourViewsEntrega(this.ID);
        }

        public void EnviarNFe()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var EmailsEnviarNFE = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    EmailsEnviarNFE = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-nfe";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Esta é sua Nota Fiscal"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = EmailsEnviarNFE;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Cliente.Nome });
                Lstvars.Add(new varsMandrill { name = "LINKNFE", content = this.NFeLink });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });
                Lstvars.Add(new varsMandrill { name = "NUMERONF", content = this.NFeNumero.ToString() });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Sua Nota Fiscal",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviarNFE,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.NFE.ToString(), this.ID, null, reject_reason);
                    }

                }
                catch { }

               
                #endregion
            }
        }

        public void EnviarEmailNfSubstituta(string To, string NameTo) {

            if (Core.Configuracoes.HOMOLOGACAO)
                To = Core.Configuracoes.EMAIL_HOMOLOGACAO;

            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/NfSubstituta.htm"));

            html = html.Replace("[NRPEDIDO]", this.Numero);
            html = html.Replace("[NOMERAZAO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.NomeRazao);
            html = html.Replace("[DOCUMENTO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento);

            html = html.Replace("[IE]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE);
            html = html.Replace("[LOGRADOURO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Logradouro);
            html = html.Replace("[NR]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Numero);
            html = html.Replace("[COMPLEMENTO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Complemento);
            html = html.Replace("[CEP]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.CEP);

            html = html.Replace("[CIDADE]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade.Nome);
            html = html.Replace("[ESTADO]", this.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Cidade.Estado.Nome);

            Domain.Core.Funcoes.EnviaEmail(
                    Domain.Core.Configuracoes.EMAIL_NOREPLY,
                    "GRUPO LAÇOS FLORES",
                    To,
                    NameTo,
                    Domain.Core.Configuracoes.EMAIL_NOREPLY,
                    NameTo,
                    "",
                    "[GRUPO LAÇOS FLORES] - NF Substituta Emitida p/ #" + this.Numero,
                    html,
                    true
                );
        }

        public void EnviarEmailCobranca()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var EmailsEnviarNFE = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    EmailsEnviarNFE = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                //// REMPORARIO
                EmailsEnviarNFE = "ricardo.seifertb@gmail.com";

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "cobranca";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "ASSUNTO"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = EmailsEnviarNFE;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Cliente.Nome });
                Lstvars.Add(new varsMandrill { name = "LINKNFE", content = this.NFeLink });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });
                Lstvars.Add(new varsMandrill { name = "NUMERONF", content = this.NFeNumero.ToString() });

                #region ANEXOS

                List<attachmentsMandrill> Anexos = new List<attachmentsMandrill>();

                try
                {
                    #region ANEXA NF

                    if (!string.IsNullOrEmpty(this.NFeLink))
                    {
                        using (WebClient client = new WebClient())
                        {
                            string StringPDF = "";

                            #region PREENCHE LINK NFEMAIS

                            if (this.NFeLink.Contains("nfemais"))
                            {
                                var Index = this.NFeLink.IndexOf("key=");
                                var IdDownload = this.NFeLink.Substring(Index + 4);
                                StringPDF = "http://nfe2.nfemais.com.br/v3/painel/danfe.php?key=" + IdDownload;
                            }
                            else
                            {
                                StringPDF = this.NFeLink;
                            }

                            #endregion

                            string id = Guid.NewGuid().ToString();
                            var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".pdf");

                            if (!string.IsNullOrEmpty(StringPDF))
                            {
                                client.DownloadFile(StringPDF, Path);

                                byte[] pdfBytes = File.ReadAllBytes(Path);
                                string pdfBase64 = Convert.ToBase64String(pdfBytes);

                                Anexos.Add(new attachmentsMandrill { type = "application/pdf", name = "NF " + this.NFeNumero, content = pdfBase64 });

                                System.IO.File.Delete(Path);
                            }
                        }
                    }

                    #endregion
                }
                catch
                {
                }

                try
                {
                    #region ANEXA BOLETO

                    if (this.MeioPagamento == Pedido.MeiosPagamento.BoletoPagarMe)
                    {
                        using (WebClient client = new WebClient())
                        {
                            string StringPDF = this.BoletoPagarMeReturn.BoletoURL;

                            string id = Guid.NewGuid().ToString();
                            var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/temp/" + id + ".pdf");

                            if (!string.IsNullOrEmpty(StringPDF))
                            {
                                string url = @"http://api.pdflayer.com/api/convert?access_key=422086415fa2a1a3913b98f08eb38172&document_url=" + StringPDF + "&document_name=" + id + ".pdf";

                                client.DownloadFile(url, Path);

                                byte[] pdfBytes = File.ReadAllBytes(Path);
                                string pdfBase64 = Convert.ToBase64String(pdfBytes);

                                Anexos.Add(new attachmentsMandrill { type = "application/pdf", name = "BOLETO", content = pdfBase64 });

                                System.IO.File.Delete(Path);
                            }
                        }
                    }

                    #endregion
                }
                catch
                {
                }


                #endregion



                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "ASSUNTO",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    attachments = Anexos,
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviarNFE,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {
                        var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                        var entityBuilder = new EntityConnectionStringBuilder();

                        entityBuilder.Provider = "System.Data.SqlClient";
                        entityBuilder.ProviderConnectionString = conStr;

                        logMandrillRepositor = new LogMandrillRepository(new System.Data.Objects.ObjectContext(conStr));

                        logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.Cobranca.ToString(), this.ID, null, reject_reason);
                    }

                }
                catch { }

                

                #endregion
            }
        }
        public void EnviarCancelamentoNFe()
        {
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email.Trim()))
            {
                var EmailsEnviarNFE = this.Cliente.Email;
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    EmailsEnviarNFE = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-nfe-cancel";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Nota Fiscal Cancelada"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = EmailsEnviarNFE;
                NovoToMandrill.name = this.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();
                Lstvars.Add(new varsMandrill { name = "NOME", content = this.Cliente.Nome });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = this.Numero });
                Lstvars.Add(new varsMandrill { name = "DATAPEDIDO", content = this.DataCriacao.ToString() });
                Lstvars.Add(new varsMandrill { name = "NUMERONF", content = this.DataCriacao.ToString() });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Sua Nota Fiscal",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviarNFE,
                            vars = Lstvars
                        }
                    }
                };

                var retorno =  new Mandrill().CallMandrill(novoEnvio);

                #region SALVAR LOG

                dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                string IdEmail = "";
                string reject_reason = "";

                foreach (var item in dynJson)
                {
                    IdEmail = item._id;
                    reject_reason = item.reject_reason;
                }

                if (reject_reason == "null")
                    reject_reason = "";

                if (!string.IsNullOrEmpty(IdEmail))
                {
                    var conStr = ConfigurationManager.ConnectionStrings["COROASEntities"].ConnectionString;
                    var entityBuilder = new EntityConnectionStringBuilder();

                    entityBuilder.Provider = "System.Data.SqlClient";
                    entityBuilder.ProviderConnectionString = conStr;

                    logMandrillRepositor = new LogMandrillRepository(new ObjectContext(conStr));

                    logMandrillRepositor.SalvaLog(IdEmail, LogMandrill.TipoEmail.CancelarNFE.ToString(), this.ID, null, reject_reason);
                }

                #endregion
            }
        }

        public bool EnviarEmailFornecedor()
        {
            var itenspedido = "";
            foreach (var item in this.PedidoItems)
            {
                itenspedido += "<tr>";
                itenspedido += "<td>";
                itenspedido += "<img src='[URL]/Produtos/" + item.ProdutoTamanho.ProdutoID + "/thumb/" + item.ProdutoTamanho.Produto.Foto + "' width='75'/>";
                itenspedido += "</td>";
                itenspedido += "<td><b>" + item.ProdutoTamanho.Produto.Nome + "</b><br><b>" + item.ProdutoTamanho.Tamanho.NomeCompleto + "</b><br>" + item.ProdutoTamanho.Produto.Descricao + "</td>";
                itenspedido += "<td style='text-align: center;'>" + (item.RepasseAtual.ValorRepasse > 0 ? item.RepasseAtual.ValorRepasse.ToString("C2") : "A confirmar") + "</td>";
                itenspedido += "</tr>";
                itenspedido += "<tr>";
                itenspedido += "<td>";
                itenspedido += "<font style='color: #757575; font-weight: bold;'>Frase :</font>";
                itenspedido += "</td>";
                itenspedido += "<td colspan='2'>";
                itenspedido += item.Mensagem;
                itenspedido += "</td>";
                itenspedido += "</tr>";
                itenspedido += "<tr>";
                itenspedido += "<td colspan='3' style='border-bottom: 1px solid #ecebe9;'>&nbsp;</td>";
                itenspedido += "</tr>";
            }

            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/fornecedor/fornecedor.htm"));
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
                html = html.Replace("[SITE]", "Laços Flores");
            else
                html = html.Replace("[SITE]", "Coroas para Velório");

            html = html.Replace("[NOMEPARA]", this.Fornecedor.Nome);

            if (string.IsNullOrEmpty(LocalID.ToString()))
            {
                html = html.Replace("[LOCAL]", this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
            }
            else
            {
                html = html.Replace("[LOCAL]", this.Local.Titulo.ToUpper() + " " + this.ComplementoLocalEntrega + " - " + this.Cidade.NomeCompleto);
            }
            html = html.Replace("[FALECIDO]", this.PessoaHomenageada);
            html = html.Replace("[ITENSPEDIDO]", itenspedido);
            html = html.Replace("[DATAHORA]", this.DataSolicitada.ToString("dd/MM/yyyy") + " às " + this.DataSolicitada.ToString("HH:mm"));
            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);

            if (Domain.Core.Funcoes.ValidaEmail(this.Fornecedor.EmailContato.Trim()))
                return Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        "GRUPO LAÇOS FLORES",
                        this.Fornecedor.EmailContato,
                        this.Fornecedor.Nome,
                        Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                        "GRUPO LAÇOS FLORES",
                        Domain.Core.Configuracoes.EMAIL_COPIA,
                        "[GRUPO LAÇOS FLORES] - Novo pedido para entrega",
                        html,
                        true
                    );
            else
                return false;
        }
        public void EnviarEmailPesquisa()
        {
            var msgEmail = "Gostaríamos de convidá-lo a responder esta breve pesquisa de satisfação sobre nossos serviços prestados. Agradecemos antecipadamente pela sua participação. ";
            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/cpv/pesquisa.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/pesquisa.htm"));
                remetente = "Laços Flores";
            }
            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOMEPARA]", this.Cliente.Nome);
            html = html.Replace("[URLPESQUISA]", Domain.Core.Configuracoes.DOMINIO + "/pesquisa-de-satisfacao?numero=" + this.Numero);
            html = html.Replace("[MENSAGEM]", msgEmail);

            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email))
                Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        remetente,
                        this.Cliente.Email,
                        this.Cliente.Nome,
                        Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                        remetente,
                        this.Cliente.EmailSecundario,
                        "[" + remetente.ToUpper() + "] - Pesquisa de satisfação!",
                        html,
                        true
                    );
        }
        public void EnviarLembreteVencimentoBoleto()
        {
            //ENVIAR EMAIL
            var html = System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("/content/html/cpv/pedido-lembrete-vencimento.htm"));
            var remetente = "Coroas para Velório";
            if (this.OrigemSite.ToUpper() == "LAÇOS FLORES")
            {
                html = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/content/html/lacosflores/pedido-lembrete-vencimento.htm"));
                remetente = "Laços Flores";
            }

            html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
            html = html.Replace("[NOME]", this.Cliente.Nome);
            html = html.Replace("[DATAPEDIDO]", this.DataCriacao.ToString("dd/MM/yyyy"));
            html = html.Replace("[DATAVENCIMENTO]", this.Boleto.DataVencimento.ToString("dd/MM/yyyy"));
            html = html.Replace("[LINK]", Domain.Core.Configuracoes.DOMINIO + "/boleto?codigo=" + this.Codigo);
            if (Domain.Core.Funcoes.ValidaEmail(this.Cliente.Email))
                Domain.Core.Funcoes.EnviaEmail
                    (
                        Domain.Core.Configuracoes.EMAIL_NOREPLY,
                        remetente,
                        this.Cliente.Email,
                        this.Cliente.Nome,
                        Domain.Core.Configuracoes.EMAIL_ATENDIMENTO,
                        remetente,
                        Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FINANCEIRO,
                        "[" + remetente.ToUpper() + "] - Lembrete de vencimento de boleto",
                        html,
                        true
                    );
        }
        public bool NumeroExiste(string numero)
        {
            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = conStr;

            pedidoRepository = new PersistentRepository<Pedido>(new ObjectContext(conStr));            

            return pedidoRepository.GetByExpression(p => p.Numero == numero).Any();
        }
        public string cardhash { get; set; }

        public string ErroCraicao { get; set; }
        public string ErroPagamento { get; set; }

        #region IPedido
        public OrigemPedido TipoPedido { get { return OrigemPedido.Cliente; } }
        public bool Cancelado()
        {
            return StatusProcessamento == TodosStatusProcessamento.Cancelado;
        }

        public Fornecedor FornecedorAutomatico
        {
            get
            {
                Fornecedor ForncedorReturn = new Fornecedor();

                if (this.Local != null)
                {
                    var List = this.Local.FornecedorLocals.Where(r => r.Ordem == 1).ToList();
                    if (List.Count > 0)
                    {
                        return List.First().Fornecedor;
                    }
                }

                return null;

            }

        }

        public bool FornecedorRecusouEntrega
        {
            get
            {                
                return AdministradorPedidoes.Where(r => r.AcaoID == 22).Any();
            }
                
        }

        public string LinkPedido
        {
            get
            {
                return "/PedidosSite/Visualizar/" + this.ID;
            }

        }

        public string LocalDeEntregaDetalhado { get; set; }

        public List<LogMandrill> LstLogsMandrill { get; set; }

        public List<PedidoEmpresa> LstPedidosDaFatura { get; set; }

        public int BoletosVencidosCorp { get; set; }
        public int FaturasVencidas { get; set; }
        public int BoletosVencidos { get; set; }

        public string NomeCliente {
            get {
                return this.Cliente.Nome;
            }
        }
        public string RazaoSocial
        {
            get
            {
                return this.Cliente.RazaoSocial;
            }
        }
        public string EmailCliente
        {
            get
            {
                return this.Cliente.Email;
            }
        }

        public string NomeEmpresa
        {
            get
            {
                return this.Cliente.Nome;
            }
        }

        public int IDEmpresa
        {
            get
            {
                return 0;
            }
        }

        public int MeioPgtID
        {
            get
            {
                return this.MeioPagamentoID;
            }
        }

        public bool PossuiFotoEntrega
        {
            get
            {
                var Foto = this.PedidoItems.Where(r => r.FotoEntrega != "").Any();
                return Foto;
            }
        }
        public bool PossuiFotoDeEntregaParaValidar
        {
            get
            {
                var Foto = this.PedidoItems.Where(r => r.StatusFotoID == 2).Any();
                return Foto;
            }
        }

        public string LatitudeEntregaReturn
        {
            get
            {
                return this.LatitudeEntrega;
            }
        }
        public string LongitudeEntregaReturn
        {
            get
            {
                return this.LongitudeEntrega;
            }
        }
        public DateTime? DtUpdateLocEntregaReturn
        {
            get
            {
                if(DtUpdateLocEntrega != null)
                {
                    return DtUpdateLocEntrega.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool FotosDeEntregaValidadas
        {
            get
            {
                if(this.PedidoItems.Where(r => r.StatusFotoID == 3).Count() == this.PedidoItems.Count())
                {
                    return true;
                }
                return false;
            }
        }

      
        #endregion
    }

}
