﻿using Domain.Entities;
using DomainExtensions.Repositories;
using HangFire.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangFire.Jobs
{
    public class LC
    {
        private PersistentRepository<Log> logRepository;

        public void AtualizaLogMandrill()
        {
            var Context = new COROASEntities();

            var LstEmailsAtualizar = Context.spAtualizaLogMandrill().ToList();

            foreach (var item in LstEmailsAtualizar)
            {
                try
                {
                    var Log = Context.LogMandrills.Where(r => r.IdEmail == item.IdEmail).First();

                    var Result = new Domain.Core.Mandrill().AtualizaStatus(new Domain.Core.DefaultMandrillConsulta
                    {
                        id = item.IdEmail
                    });

                    if (!string.IsNullOrEmpty(Result.state))
                        Log.Status = Result.state;

                    if (!string.IsNullOrEmpty(Result.opens))
                    {
                        if (int.Parse(Result.opens) > 0)
                        {
                            Log.Aberto = true;
                        }
                        else
                        {
                            Log.Aberto = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(Result.clicks))
                    {
                        if (int.Parse(Result.clicks) > 0)
                        {
                            Log.Clicado = true;
                        }
                        else
                        {
                            Log.Clicado = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(Result.bounce_description))
                        Log.RejectReason = Result.bounce_description;

                    if (!string.IsNullOrEmpty(Result.subject))
                        Log.Titulo = Result.subject;

                    Log.DataAtualizacao = DateTime.Now;

                    Context.SaveChanges();

                    Console.WriteLine("Mandrill Atualizado: " + item.IdEmail);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void RoboCobrancaLC()
        {
            try
            {
                logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());

                #region ROBO COBRANCA

                var Cobranca = new CobrancaLC();

                var LstCobranca = Cobranca.PropulaLstCobranca();

                var LstCobrancaBoleto = LstCobranca.Where(r => r.MeioPagamentoID == 5 && r.StatusPagamentoIDBoleto == 1).ToList();
                var LstCobrancaOutros = LstCobranca.Where(r => r.MeioPagamentoID != 5 && r.StatusPagamentoIDFatura == 1).ToList();

                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                    return;

                if(new Domain.Core.Funcoes().EhFeriado(DateTime.Now))
                {
                    logRepository.Save(new Log
                    {
                        Data = DateTime.Now,
                        Var1 = "INFO - RoboCobrancaLC",
                        Var2 = "Envio nao ocorreu devido ao feriado"
                    });

                    return;
                }

                string DataHoje = DateTime.Now.ToShortDateString();

                foreach (var CobrancaBoleto in LstCobrancaBoleto)
                {
                    Cobranca.SendEmail(DataHoje, CobrancaBoleto);
                }

                foreach (var CobrancaOutros in LstCobrancaOutros)
                {
                    Cobranca.SendEmail(DataHoje, CobrancaOutros);
                }

                #endregion

            }
            catch (Exception ex)
            {
                logRepository.Save(new Log
                {
                    Data = DateTime.Now,
                    Var1 = "ERRO - RoboCobrancaLC",
                    Var2 = ex.Message + ex.InnerException
                });
            }

        }
    }
}