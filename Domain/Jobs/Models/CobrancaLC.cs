﻿using Domain.Core;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace HangFire.Jobs.Models
{
    public class CobrancaLC
    {
        public int? ColaboradorID { get; set; }
        public string ColaboradorTelefone { get; set; }
        public DateTime? DataVencimentoBoleto { get; set; }
        public DateTime? DataVencimentoBoletoOriginal { get; set; }
        public DateTime? DataVencimentoOriginal { get; set; }
        public string Nome { get; set; }
        public string ColaboradorNome { get; set; }
        public string NomeEmpresa { get; set; }
        public string EmailColaborador { get; set; }
        public string EmailFinanceiro { get; set; }
        public string EmailFinanceiroCopia { get; set; }
        public string LinkBoleto { get; set; }
        public decimal? Valor { get; set; }
        public decimal ValorFatura { get; set; }
        public int FaturaID { get; set; }
        public int PedidoEmpresaID { get; set; }
        public string Produto { get; set; }

        public bool PrimeiroRenegociado { get; set; }
        public bool SegundoRenegociado { get; set; }

        public bool PrimeiroRenegociadoFatura { get; set; }
        public bool SegundoRenegociadoFatuar { get; set; }

        public int MeioPagamentoID { get; set; }
        public int? StatusPagamentoIDBoleto { get; set; }
        public int StatusPagamentoIDFatura { get; set; }
        public DateTime DataVencimentoFatura { get; set; }
        public string NFeLink { get; set; }
        public int? NFeNumero { get; set; }

        public void SendEmail(string DataHoje, CobrancaLC Cobranca)
        {
            var culture = new CultureInfo("pt-BR");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            DateTime DataComparativa = DateTime.Now;

            if (Domain.Core.Configuracoes.HOMOLOGACAO)
                DataHoje = new DateTime(2018, 12, 31).ToShortDateString();

            if (Cobranca.MeioPagamentoID == 5)
            {
                DataComparativa = Cobranca.DataVencimentoBoletoOriginal.Value;
            }
            else if (Cobranca.MeioPagamentoID != 5)
            {
                DataComparativa = Cobranca.DataVencimentoOriginal.Value;
            }
            else
            {
                if (Cobranca.DataVencimentoOriginal != null)
                    DataComparativa = Cobranca.DataVencimentoOriginal.Value;
            }

            DateTime DataVenceEmDoisDias = new Funcoes().AddBusinessDays(DataComparativa, -2);
            DateTime DataVenceHoje = new Funcoes().AddBusinessDays(DataComparativa, 0);
            DateTime DataVenceu_A_umDia = new Funcoes().AddBusinessDays(DataComparativa, 1);

            DateTime DataVenceuNovamente;
            DateTime DataVenceuInadimplencia;

            if (Cobranca.MeioPagamentoID == 5)
            {
                DataVenceuNovamente = new Funcoes().AddBusinessDays((DateTime)Cobranca.DataVencimentoBoleto, 1);
                DataVenceuInadimplencia = new Funcoes().AddBusinessDays((DateTime)Cobranca.DataVencimentoBoleto, 1);
            }
            else
            {
                DataVenceuNovamente = new Funcoes().AddBusinessDays((DateTime)Cobranca.DataVencimentoFatura, 1);
                DataVenceuInadimplencia = new Funcoes().AddBusinessDays((DateTime)Cobranca.DataVencimentoFatura, 1);
            }

            var DiasVencidos = DateTime.Parse(DataHoje) - Cobranca.DataVencimentoBoleto;
            var DiasVencidosFatura = DateTime.Parse(DataHoje) - Cobranca.DataVencimentoOriginal;

            #region BOLETO VENCE EM 2 DIAS

            if (DataHoje == DataVenceEmDoisDias.ToShortDateString())
            {
                #region SMS

                // ENVIA SMS
                //dynamic ResultSMS = JsonConvert.DeserializeObject(new Model.Factory.MovileFactory().EnviarSmsLC(Cobranca, "1"));
                //foreach (var data in ResultSMS)
                //{
                //    Console.WriteLine("SMS " + Cobranca.ColaboradorTelefone + " - " + ((Newtonsoft.Json.Linq.JProperty)data).First.ToString());
                //}

                #endregion

                #region EMAIL

                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "1"));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }

                #endregion
            }

            #endregion

            #region BOLETO VENCE HOJE

            if (DataHoje == DataVenceHoje.ToShortDateString())
            {
                #region EMAIL

                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "2"));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }

                #endregion
            }

            #endregion

            #region BOLETO VENCEU A 1 DIA

            if (DataHoje == DataVenceu_A_umDia.ToShortDateString() && Cobranca.MeioPagamentoID == 5 && Cobranca.PrimeiroRenegociado == false)
            {
                var Context = new COROASEntities();

                #region BOLETO

                if (Cobranca.MeioPagamentoID == 5)
                {
                    var NovaData = new Funcoes().CorrigirFeriado(new Funcoes().AddBusinessDays(DateTime.Parse(DataHoje), int.Parse(Domain.Core.Configuracoes.BOLETO_DIAS_PRIMIRA_RENEGOCIACAO)));

                    new Domain.Factories.BoletoItauFactory().AlterarVencimentoBoleto(Cobranca.FaturaID, NovaData.ToShortDateString(), null);
                    Cobranca.DataVencimentoBoleto = NovaData;

                    #region ATUALIZA FLA RENEGOCIADO

                    var fatura = Context.Faturamentoes.Where(r => r.ID == Cobranca.FaturaID).First();
                    var boleto = fatura.Boletoes.Where(b => b.StatusPagamentoID == 1 || b.StatusPagamentoID == 3).First();
                    boleto.PrimeiroRenegociado = true;
                    Context.SaveChanges();

                    #endregion

                    #region EMAIL

                    dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "3"));
                    foreach (var data in Result)
                    {
                        Console.WriteLine(data.email + " - " + data.status);
                    }

                    #endregion
                }

                #endregion
            }

            #endregion

            #region FATURA VENCEU A 1 DIA

            if (DataHoje == DataVenceu_A_umDia.ToShortDateString() && Cobranca.MeioPagamentoID != 5 && Cobranca.PrimeiroRenegociadoFatura == false)
            {
                var Context = new COROASEntities();

                #region FATURA

                if (Cobranca.MeioPagamentoID != 5)
                {
                    var NovaData = new Funcoes().CorrigirFeriado(new Funcoes().AddBusinessDays(DateTime.Parse(DataHoje), int.Parse(Domain.Core.Configuracoes.BOLETO_DIAS_PRIMIRA_RENEGOCIACAO)));
                    Cobranca.DataVencimentoFatura = NovaData;


                    #region ATUALIZA FLA RENEGOCIADO

                    var fatura = Context.Faturamentoes.Where(r => r.ID == Cobranca.FaturaID).First();
                    fatura.DataVencimento = NovaData;
                    fatura.PrimeiroRenegociado = true;

                    Context.SaveChanges();

                    #endregion

                    #region EMAIL

                    dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "3"));
                    foreach (var data in Result)
                    {
                        Console.WriteLine(data.email + " - " + data.status);
                    }

                    #endregion
                }

                #endregion
            }

            #endregion

            #region SEGUNDO BOLETO RENEGOCIADO

            if (DataHoje == DataVenceuNovamente.ToShortDateString() && Cobranca.MeioPagamentoID == 5 && Cobranca.PrimeiroRenegociado == true && Cobranca.SegundoRenegociado == false)
            {
                var Context = new COROASEntities();

                #region ATUALIZAÇÃO DE BOLETO

                if (Cobranca.MeioPagamentoID == 5)
                {
                    var NovaData = new Funcoes().CorrigirFeriado(new Funcoes().AddBusinessDays(DateTime.Parse(DataHoje), int.Parse(Domain.Core.Configuracoes.BOLETO_DIAS_SEGUNDA_RENEGOCIACAO)));

                    #region ATUALIZA JUROS

                    // MULTA DE 2%
                    decimal NovoValor = Cobranca.Valor.Value + (((decimal)Cobranca.Valor.Value / 100) * 2);

                    // JUROS DE 5,9% AO DIA (0,19% DIA)
                    NovoValor = ((NovoValor / 100) * (decimal)(DiasVencidos.Value.Days * 0.19)) + NovoValor;

                    Cobranca.Valor = NovoValor;

                    #endregion

                    new Domain.Factories.BoletoItauFactory().AlterarVencimentoBoleto(Cobranca.FaturaID, NovaData.ToShortDateString(), NovoValor);
                    Cobranca.DataVencimentoBoleto = NovaData;

                    #region ATUALIZA FLA RENEGOCIADO
                   
                    var fatura = Context.Faturamentoes.Where(r => r.ID == Cobranca.FaturaID).First();
                    var boleto = fatura.Boletoes.Where(b => b.StatusPagamentoID == 1 || b.StatusPagamentoID == 3).First();
                    boleto.SegundoRenegociado = true;
                    Context.SaveChanges();

                    #endregion

                    #region EMAIL

                    dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "4"));
                    foreach (var data in Result)
                    {
                        Console.WriteLine(data.email + " - " + data.status);
                    }

                    #endregion
                }

                #endregion
            }

            #endregion

            #region SEGUNDO FATURA RENEGOCIADA

            if (DataHoje == DataVenceuNovamente.ToShortDateString() && Cobranca.MeioPagamentoID != 5 && Cobranca.PrimeiroRenegociadoFatura == true && Cobranca.SegundoRenegociadoFatuar == false)
            {
                var Context = new COROASEntities();

                var NovaData = new Funcoes().CorrigirFeriado(new Funcoes().AddBusinessDays(DateTime.Parse(DataHoje), int.Parse(Domain.Core.Configuracoes.BOLETO_DIAS_SEGUNDA_RENEGOCIACAO)));

                #region ATUALIZA JUROS

                // MULTA DE 2%
                decimal NovoValor = Cobranca.ValorFatura + (((decimal)Cobranca.ValorFatura / 100) * 2);

                // JUROS DE 5,9% AO DIA (0,19% DIA)
                NovoValor = ((NovoValor / 100) * (decimal)(DiasVencidosFatura.Value.Days * 0.19)) + NovoValor;

                Cobranca.Valor = NovoValor;
                Cobranca.ValorFatura = NovoValor;

                #endregion

                Cobranca.DataVencimentoFatura = NovaData;

                var fatura = Context.Faturamentoes.Where(r => r.ID == Cobranca.FaturaID).First();
                fatura.DataVencimento = NovaData;
                fatura.ValorTotal = NovoValor;
                fatura.SegundoRenegociado = true;

                Context.SaveChanges();

                #region EMAIL

                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "4"));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }

                #endregion
            }

            #endregion

            #region INADIMPLENCIA BOLETO

            if (DataHoje == DataVenceuInadimplencia.ToShortDateString() && Cobranca.MeioPagamentoID == 5 && Cobranca.PrimeiroRenegociado == true && Cobranca.SegundoRenegociado == true)
            {
                #region EMAIL

                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "5"));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }

                #endregion
            }

            #endregion

            #region INADIMPLENCIA FATURA

            if (DataHoje == DataVenceuInadimplencia.ToShortDateString() && Cobranca.MeioPagamentoID != 5 && Cobranca.PrimeiroRenegociadoFatura == true && Cobranca.SegundoRenegociadoFatuar == true)
            {
                #region EMAIL

                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.CobrancaFactory().EnviarCobrancaLC(Cobranca, "5"));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }

                #endregion
            }

            #endregion
        }

        public List<CobrancaLC> PropulaLstCobranca()
        {
            List<CobrancaLC> LstRetonro = new List<CobrancaLC>();
           
            var LstCobrancaLC = new COROASEntities().spRelatorioRoboCobrancaLC().ToList();

            if (Domain.Core.Configuracoes.HOMOLOGACAO)
                LstCobrancaLC = LstCobrancaLC.Where(r => r.ColaboradorID == 26714 || r.ColaboradorID == 423).ToList();

            foreach (var Item in LstCobrancaLC)
            {
                CobrancaLC novaCobranca = new CobrancaLC();
                novaCobranca.ColaboradorID = Item.ColaboradorID;
                novaCobranca.ColaboradorTelefone = Item.ColaboradorTelefone;
                novaCobranca.DataVencimentoBoleto = Item.DataVencimentoBoleto;
                novaCobranca.DataVencimentoBoletoOriginal = Item.DataVencimentoBoletoOriginal;
                novaCobranca.Nome = Item.Nome;
                novaCobranca.EmailColaborador = Item.EmailColaborador;
                novaCobranca.EmailFinanceiro = Item.EmailFinanceiro;
                novaCobranca.EmailFinanceiroCopia = Item.EmailFinanceiroCopia;
                novaCobranca.Valor = Item.Valor;
                novaCobranca.FaturaID = Item.FaturaID;
                novaCobranca.PedidoEmpresaID = Item.PedidoEmpresaID;
                novaCobranca.Produto = Item.Produto;
                novaCobranca.MeioPagamentoID = Item.MeioPagamentoID;
                novaCobranca.StatusPagamentoIDFatura = Item.StatusPagamentoIDFatura;
                novaCobranca.StatusPagamentoIDBoleto = Item.StatusPagamentoIDBoleto;
                novaCobranca.DataVencimentoFatura = Item.DataVencimentoFatura;
                novaCobranca.ValorFatura = Item.ValorFatura;
                novaCobranca.DataVencimentoOriginal = Item.DataVencimentoOriginal;
                novaCobranca.NFeLink = Item.NFeLink;
                novaCobranca.NFeNumero = Item.NFeNumero;
                novaCobranca.ColaboradorNome = Item.ColaboradorNome;
                novaCobranca.NomeEmpresa = Item.NomeEmpresa;

                #region RENEGOCIADO BOLETO

                if (Item.PrimeiroRenegociado != null)
                {
                    if (Item.PrimeiroRenegociado == true)
                    {
                        novaCobranca.PrimeiroRenegociado = true;
                    }
                    else
                    {
                        novaCobranca.PrimeiroRenegociado = false;
                    }
                }
                else
                {
                    novaCobranca.PrimeiroRenegociado = false;
                }

                if (Item.SegundoRenegociado != null)
                {
                    if (Item.SegundoRenegociado == true)
                    {
                        novaCobranca.SegundoRenegociado = true;
                    }
                    else
                    {
                        novaCobranca.SegundoRenegociado = false;
                    }
                }
                else
                {
                    novaCobranca.SegundoRenegociado = false;
                }

                #endregion

                #region RENEGOCIADO FATURA

                if (Item.PrimeiroRenegociadoFatura != null)
                {
                    if (Item.PrimeiroRenegociadoFatura == true)
                    {
                        novaCobranca.PrimeiroRenegociadoFatura = true;
                    }
                    else
                    {
                        novaCobranca.PrimeiroRenegociadoFatura = false;
                    }
                }
                else
                {
                    novaCobranca.PrimeiroRenegociadoFatura = false;
                }

                if (Item.SegundoRenegociadoFatuar != null)
                {
                    if (Item.SegundoRenegociadoFatuar == true)
                    {
                        novaCobranca.SegundoRenegociadoFatuar = true;
                    }
                    else
                    {
                        novaCobranca.SegundoRenegociadoFatuar = false;
                    }
                }
                else
                {
                    novaCobranca.SegundoRenegociadoFatuar = false;
                }

                #endregion

                LstRetonro.Add(novaCobranca);
            }

            return LstRetonro;
        }
    }

   
}