﻿using Domain.Core;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace HangFire.Jobs.Models
{
    public class CobrancaCPV
    {
        public int? ColaboradorID { get; set; }
        public string ColaboradorTelefone { get; set; }
        public DateTime? DataVencimentoBoleto { get; set; }
        public DateTime? DataVencimentoBoletoOriginal { get; set; }
        public DateTime? DataVencimentoOriginal { get; set; }
        public string Nome { get; set; }
        public string ColaboradorNome { get; set; }
        public string NomeEmpresa { get; set; }
        public string EmailColaborador { get; set; }
        public string EmailFinanceiro { get; set; }
        public string EmailFinanceiroCopia { get; set; }
        public string LinkBoleto { get; set; }
        public decimal? Valor { get; set; }
        public decimal ValorFatura { get; set; }
        public int FaturaID { get; set; }
        public int PedidoEmpresaID { get; set; }
        public string Produto { get; set; }

        public bool PrimeiroRenegociado { get; set; }
        public bool SegundoRenegociado { get; set; }

        public bool PrimeiroRenegociadoFatura { get; set; }
        public bool SegundoRenegociadoFatuar { get; set; }

        public int MeioPagamentoID { get; set; }
        public int? StatusPagamentoIDBoleto { get; set; }
        public int StatusPagamentoIDFatura { get; set; }
        public DateTime DataVencimentoFatura { get; set; }
        public string NFeLink { get; set; }
        public int? NFeNumero { get; set; }

        public void SendEmailBoleto(string DataHoje, spCobrancaBoleto_Result Cobranca)
        {
            var Context = new COROASEntities();

            var Pedido = Context.Pedidoes.Where(r => r.ID == Cobranca.PedidoID).First();
            var BoletoResult = Pedido.BoletoPagarMes.Where(b => b.ID == Cobranca.BoletoID).First();

            // SEU BOLETO VENCE HOJE
            dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.BoletoPagarMeFactory().EnviarCobranca(BoletoResult, Cobranca.BarCode, Cobranca.BoletoURL, Pedido.Codigo));
            foreach (var data in Result)
            {
                Console.WriteLine(data.email + " - " + data.status);
            }
        }

        public void SendEmailBoletoVenceu(string DataHoje, spRenegociaBoleto_Result Cobranca)
        {
            if (Domain.Core.Configuracoes.HOMOLOGACAO)
                DataHoje = new DateTime(2018, 12, 31).ToShortDateString();

            var Context = new COROASEntities();

            var pedido = Context.Pedidoes.Where(r => r.ID == Cobranca.PedidoID).First();
            var ValorBoleto = pedido.BoletoPagarMes.Where(r => r.StatusPagamentoID == 1).First().Valor;

            // CANCELA BOLETOS PAGARME
            Domain.Factories.BoletoPagarMeFactory.CancelarBoletos(pedido);

            // CRIAR UM NOVO BOLETO
            var NovoBoleto = Domain.Factories.BoletoPagarMeFactory.Criar(pedido.Cliente.Bairro, pedido.Cliente.CEP, pedido.Cliente.Cidade.Nome, pedido.Cliente.Documento, pedido.Cliente.Logradouro + ", " + pedido.Cliente.Numero + " " + pedido.Cliente.Complemento, pedido.Cliente.TipoID == 1 ? pedido.Cliente.Nome : pedido.Cliente.RazaoSocial, (pedido.Cliente.Estado != null) ? pedido.Cliente.Estado.Sigla : pedido.Estado.Sigla, pedido.ValorTotal, pedido.ID, ValorBoleto.ToString());

            Context.SaveChanges();

            if (NovoBoleto != null)
            {
                dynamic Result = JsonConvert.DeserializeObject(new Domain.Factories.BoletoPagarMeFactory().EnviarRenegociacao(NovoBoleto, pedido.Codigo));
                foreach (var data in Result)
                {
                    Console.WriteLine(data.email + " - " + data.status);
                }
            }
            else
            {
                pedido.PedidoNotas.Add(new PedidoNota()
                {
                    Pedido = pedido,
                    DataCriacao = DateTime.Now,
                    Observacoes = "Erro na criação ou envio de boleto de renogociação."
                });

                Console.WriteLine(pedido.Cliente.Email + " - " + "Erro na Criação ou envio do Boleto.");
            }

        }

        public void EnviarSMS(spCobrancaSMS_Result SMS)
        {
            var Context = new COROASEntities();

            var Pedido = Context.Pedidoes.Where(r => r.ID == SMS.PedidoID).First();
            var BoletoResult = Pedido.BoletoPagarMes.Where(b => b.ID == SMS.BoletoID).First();

            dynamic ResultSMS = JsonConvert.DeserializeObject(new Domain.Factories.MovileFactory().EnviarSms(BoletoResult));
            foreach (var data in ResultSMS)
            {
                Console.WriteLine("SMS " + Pedido.Cliente.CelularContato + " " + Pedido.Cliente.TelefoneContato + " - " + ((Newtonsoft.Json.Linq.JProperty)data).First.ToString());
            }
        }

        public List<spCobrancaSMS_Result> PropulaLstCobrancaSMS()
        {
            return new COROASEntities().spCobrancaSMS().ToList();
        }

        public List<spCobrancaBoleto_Result> PropulaLstCobrancaBoleto()
        {
            return new COROASEntities().spCobrancaBoleto().ToList();
        }

        public List<spRenegociaBoleto_Result> PropulaLstCobrancaBoletoRenegociar()
        {
            return new COROASEntities().spRenegociaBoleto().ToList();
        }
    }

   
}