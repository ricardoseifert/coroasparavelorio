﻿using Domain.Entities;
using DomainExtensions.Repositories;
using HangFire.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangFire.Jobs
{
    public class CPV
    {
        private PersistentRepository<Log> logRepository;

        public void AtualizaLogMandrill()
        {
            var Context = new COROASEntities();

            var LstEmailsAtualizar = Context.spAtualizaLogMandrill().ToList();

            foreach (var item in LstEmailsAtualizar)
            {
                try
                {
                    var Log = Context.LogMandrills.Where(r => r.IdEmail == item.IdEmail).First();

                    var Result = new Domain.Core.Mandrill().AtualizaStatus(new Domain.Core.DefaultMandrillConsulta
                    {
                        id = item.IdEmail
                    });

                    if (!string.IsNullOrEmpty(Result.state))
                        Log.Status = Result.state;

                    if (!string.IsNullOrEmpty(Result.opens))
                    {
                        if (int.Parse(Result.opens) > 0)
                        {
                            Log.Aberto = true;
                        }
                        else
                        {
                            Log.Aberto = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(Result.clicks))
                    {
                        if (int.Parse(Result.clicks) > 0)
                        {
                            Log.Clicado = true;
                        }
                        else
                        {
                            Log.Clicado = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(Result.bounce_description))
                        Log.RejectReason = Result.bounce_description;

                    if (!string.IsNullOrEmpty(Result.subject))
                        Log.Titulo = Result.subject;

                    Log.DataAtualizacao = DateTime.Now;

                    Context.SaveChanges();

                    Console.WriteLine("Mandrill Atualizado: " + item.IdEmail);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void RoboCobrancaCPV_SMS()
        {
            try
            {
                logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());

                var Cobranca = new CobrancaCPV();
                var LstSms = Cobranca.PropulaLstCobrancaSMS();

                if (new Domain.Core.Funcoes().EhFeriado(DateTime.Now))
                {
                    logRepository.Save(new Log
                    {
                        Data = DateTime.Now,
                        Var1 = "INFO - RoboCobrancaCPV_SMS",
                        Var2 = "Envio nao ocorreu devido ao feriado"
                    });

                    return;
                }

                #region ENVIA SMS   

                foreach (var SMS in LstSms)
                {
                    try
                    {
                        Cobranca.EnviarSMS(SMS);
                    }
                    catch (Exception ex)
                    {
                        logRepository.Save(new Log
                        {
                            Data = DateTime.Now,
                            Var1 = "ERRO - RoboCobrancaCPV_SMS - Email para pedidoID - " + SMS.PedidoID,
                            Var2 = ex.Message + ex.InnerException
                        });

                        throw;
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                logRepository.Save(new Log
                {
                    Data = DateTime.Now,
                    Var1 = "ERRO - RoboCobrancaCPV_SMS",
                    Var2 = ex.Message + ex.InnerException
                });
            }
        }

        public void RoboCobrancaCPV()
        {
            try
            {
                logRepository = new PersistentRepository<Domain.Entities.Log>(new COROASEntities());

                var Cobranca = new CobrancaCPV();

                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                    return;

                if (new Domain.Core.Funcoes().EhFeriado(DateTime.Now))
                {
                    logRepository.Save(new Log
                    {
                        Data = DateTime.Now,
                        Var1 = "INFO - RoboCobrancaCPV",
                        Var2 = "Envio nao ocorreu devido ao feriado"
                    });

                    return;
                }

                string DataHoje = DateTime.Now.ToShortDateString();

                var LstBoleto = Cobranca.PropulaLstCobrancaBoleto();
                var LstBoletoNegociar = Cobranca.PropulaLstCobrancaBoletoRenegociar();

                // SEU BOLETO VENCE HOJE
                #region ENVIA EMAIL

                foreach (var Boleto in LstBoleto)
                {
                    try
                    {
                        Cobranca.SendEmailBoleto(DataHoje, Boleto);
                    }
                    catch (Exception ex)
                    {
                        logRepository.Save(new Log
                        {
                            Data = DateTime.Now,
                            Var1 = "ERRO - RoboCobrancaCPV_VenceHoje - Email para pedidoID - " + Boleto.PedidoID,
                            Var2 = ex.Message + ex.InnerException
                        });

                        throw;
                    }
                }

                #endregion

                // SEU BOLETO VENCEU
                #region ENVIA EMAIL

                foreach (var Boleto in LstBoletoNegociar)
                {
                    try
                    {
                        Cobranca.SendEmailBoletoVenceu(DataHoje, Boleto);
                    }
                    catch (Exception ex)
                    {
                        logRepository.Save(new Log
                        {
                            Data = DateTime.Now,
                            Var1 = "ERRO - RoboCobrancaCPV_Renegociar - Email para pedidoID - " + Boleto.PedidoID,
                            Var2 = ex.Message + ex.InnerException
                        });

                        throw;
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                logRepository.Save(new Log {
                    Data = DateTime.Now,
                    Var1 = "ERRO - RoboCobrancaCPV",
                    Var2 = ex.Message + ex.InnerException
                });
            }
        }
    }
}