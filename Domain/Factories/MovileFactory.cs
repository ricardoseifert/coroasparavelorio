﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using System.IO;
using System.Web;
using Domain.Core;
using System.Globalization;

namespace Domain.Factories
{
    public class MovileFactory
    {
        public string EnviarSms(string Telefone, string Mensagem)
        {
            if (Telefone.ToString().Replace(" ", "").Length < 11)
            {
                return "{ \"erro\": \"Nr de Telefone Inválido.\"}";
            }
            else
            {
                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    Telefone = Core.Configuracoes.NR_SMS_HOMOLOGACAO;
                }

                DefaultMovile novoEnvio = new DefaultMovile();
                novoEnvio.destino = Telefone;

                novoEnvio.mensagem = Mensagem;

                return new Movile().CallMovile(novoEnvio);
            }
        }

        public string EnviarSms(BoletoPagarMe Boleto)
        {
            if (!string.IsNullOrEmpty(Boleto.Pedido.Cliente.CelularContato) || !string.IsNullOrEmpty(Boleto.Pedido.Cliente.TelefoneContato))
            {
                var Telefone = "";
                if (!string.IsNullOrEmpty(Boleto.Pedido.Cliente.CelularContato))
                {
                    Telefone = "55" + Boleto.Pedido.Cliente.CelularContato.Replace("(", "").Replace(")", "").Replace(" ", "");
                }
                else
                {
                    Telefone = "55" + Boleto.Pedido.Cliente.TelefoneContato.Replace("(", "").Replace(")", "").Replace(" ", "");
                }

                if (Telefone.ToString().Replace(" ", "").Length < 11)
                {
                    return "Nr de Telefone Inválido. Menos de 11 Char.";
                }
                else
                {

                    if (Core.Configuracoes.HOMOLOGACAO)
                    {
                        Telefone = Core.Configuracoes.NR_SMS_HOMOLOGACAO;
                    }

                    DefaultMovile novoEnvio = new DefaultMovile();
                    novoEnvio.destino = Telefone;

                    var Mensagem = Core.Configuracoes.FRASE_SMS;
                    var Nome = Boleto.Pedido.Cliente.Nome.Split(' ')[0];
                    Nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Nome.ToLower());

                    Mensagem = Mensagem.Replace("[NOME]", Nome);
                    Mensagem = Mensagem.Replace("[VALOR]", "R$ " + Boleto.Valor.ToString().Replace(".", ","));
                    Mensagem = Mensagem.Replace("[COD]", Boleto.BarCode);
                    Mensagem = Mensagem.Replace("[VENC]", Boleto.DataVencimento.Value.Day + "/" + Boleto.DataVencimento.Value.Month);


                    novoEnvio.mensagem = Mensagem;

                    return new Movile().CallMovile(novoEnvio);
                }
            }
            else
            {
                return "{ \"erro\": \"Nr de Telefone Inválido.\"}";
            }
        }
    }
}
