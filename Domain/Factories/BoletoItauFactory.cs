﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Factories
{
    public class BoletoItauFactory
    {
        public string AlterarVencimentoBoleto(int FaturaID, string data, decimal? Valor)
        {
            var Context = new COROASEntities();
            var fatura = Context.Faturamentoes.Where(r => r.ID == FaturaID).First();

            if (fatura != null)
            {
                var datavencimento = new DateTime();
                DateTime.TryParse(data, out datavencimento);

                if (datavencimento != new DateTime())
                {
                    //Registra log do usuário
                    //administradorPedidoRepository.Registra(adminModel.ID, null, null, null, fatura.ID, AdministradorPedido.Acao.AlterouDataVencimento);

                    var boleto = fatura.Boletoes.Where(b => b.StatusPagamentoID == 1 || b.StatusPagamentoID == 3).First();
                    boleto.Processado = false;
                    if (boleto.RemessaBoletoes.Count > 0)
                        boleto.ComandoID = (int)Boleto.Comandos.ModificarVencimento;
                    boleto.DataVencimento = datavencimento;
                    boleto.DataEnvioLembreteVencimento = null;
                    fatura.DataVencimento = datavencimento;

                    if (Valor != null)
                        boleto.Valor = (decimal)Valor;

                    var Fatura = boleto.Faturamento;
                    Fatura.DataVencimento = datavencimento;

                    if (Valor != null)
                        fatura.ValorTotal = (decimal)Valor;

                    Context.SaveChanges();

                    return "OK";
                }
                else
                    return "Data inválida";
            }
            else
                return "A fatura não pode ser alterada. Tente novamente mais tarde";
        }
    }
}
