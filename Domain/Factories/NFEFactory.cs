﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using System.IO;
using System.Web;
using PagarMe;
using System.Net;
using YamlDotNet.Serialization;
using Domain.Core;
using System.Globalization;
using YamlDotNet.RepresentationModel;
using System.Threading;
using System.Text.RegularExpressions;

namespace Domain.Factories
{
    public class NFEFactory
    {
        public static NFE CallCPV(NFE Nf)
        {
            var context = new COROASEntities();
            var PedidoRepository = new PersistentRepository<Pedido>(context);

            var Pedido = PedidoRepository.Get(Nf.PedidoCPV);

            #region PREENCHE NF

            var dadosNFe = new DadosNFE();

            dadosNFe.natureza_operacao = "Venda";
            dadosNFe.forma_pagamento = "0";
            dadosNFe.data_emissao = DateTime.Now.ToString("yyyy-MM-dd");
            dadosNFe.data_entrada_saida = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            dadosNFe.tipo_documento = "1";
            dadosNFe.finalidade_emissao = "1";

            #region EMITENTE

            dadosNFe.cnpj_emitente = "12404942000114";
            dadosNFe.nome_emitente = "ESG COMERCIO ELETRONICO LTDA ME";
            dadosNFe.logradouro_emitente = "Av. Lins de Vasconcelos";
            dadosNFe.numero_emitente = "1975";
            dadosNFe.bairro_emitente = "Vila Mariana";
            dadosNFe.municipio_emitente = "São Paulo";
            dadosNFe.uf_emitente = "SP";
            dadosNFe.cep_emitente = "01537-001";
            dadosNFe.telefone_emitente = "1140979405";
            dadosNFe.inscricao_estadual_emitente = "147409290110";
            dadosNFe.regime_tributario_emitente = "1";

            #endregion

            #region DESTINATARIO


            if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica)
                dadosNFe.cpf_destinatario = Pedido.Cliente.Documento;

            if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaJuridica)
                dadosNFe.cnpj_destinatario = Pedido.Cliente.Documento;

            if (Pedido.Cliente.TipoIE == (int)Cliente.TipoInscricaoEstadual.NaoContribuinte)
            {
                dadosNFe.indicador_inscricao_estadual_destinatario = "9";
            }
            else
            {
                if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaJuridica && (!string.IsNullOrEmpty(Pedido.Cliente.InscricaoEstadual) && Pedido.Cliente.InscricaoEstadual != "ISENTO"))
                {
                    dadosNFe.inscricao_estadual_destinatario = Pedido.Cliente.InscricaoEstadual.ToString();
                }
                else
                {
                    dadosNFe.inscricao_estadual_destinatario = "ISENTO";
                }
            }

            if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaJuridica)
            {
                dadosNFe.nome_destinatario = Pedido.Cliente.RazaoSocial;
            }
            else
            {
                dadosNFe.nome_destinatario = Pedido.Cliente.Nome;

            }

            dadosNFe.logradouro_destinatario = Pedido.Cliente.Logradouro + " " + Pedido.Cliente.Complemento;
            if (dadosNFe.logradouro_destinatario.Length > 60)
                dadosNFe.logradouro_destinatario = dadosNFe.logradouro_destinatario.Substring(0, 59);

            if (dadosNFe.nome_destinatario.Length > 60)
                dadosNFe.nome_destinatario = dadosNFe.nome_destinatario.Substring(0, 59);

            if (string.IsNullOrEmpty(dadosNFe.logradouro_destinatario))
                throw new Exception("Rejeição: Logradouro Não Informado - Cod: XX");

            int Numero;
            bool successfullyParsed = int.TryParse(Pedido.Cliente.Numero, out Numero);
            if (successfullyParsed)
            {
                dadosNFe.numero_destinatario = Numero.ToString();
            }
            else
            {
                dadosNFe.numero_destinatario = "0";
            }

            dadosNFe.bairro_destinatario = Pedido.Cliente.Bairro;
            if (string.IsNullOrEmpty(dadosNFe.bairro_destinatario))
                throw new Exception("Rejeição: Bairro do destinatário não informado - Cod: XX");

            dadosNFe.municipio_destinatario = Pedido.Cliente.Cidade.Nome;
            dadosNFe.uf_destinatario = Pedido.Cliente.Estado.Sigla;

            if (Pedido.Cliente.Estado.Sigla == "SP")
            {
                dadosNFe.local_destino = "1";
            }
            else
            {
                dadosNFe.local_destino = "2";
            }

            if (Pedido.Cliente.Estado.Sigla == "DF")
            {
                dadosNFe.municipio_destinatario = "BRASÍLIA";
            }

            dadosNFe.cep_destinatario = Pedido.Cliente.CEP;
            dadosNFe.pais_destinatario = "Brasil";
            dadosNFe.telefone_destinatario = Pedido.Cliente.TelefoneContato.Replace("(", "").Replace(")", "").Replace(" ", "");

            #endregion

            #region VALOR TOTAL

            decimal ValorProdutos = Pedido.PedidoItems.Sum(r => r.Valor);

            dadosNFe.icms_base_calculo = "0.00";
            dadosNFe.icms_valor_total = "0.00";
            dadosNFe.icms_base_calculo_st = "0.00";
            dadosNFe.icms_valor_total_st = "0.00";
            dadosNFe.valor_produtos = ValorProdutos.ToString().Replace(",", ".");
            dadosNFe.valor_frete = "0.00";
            dadosNFe.valor_seguro = "0.00";
            dadosNFe.valor_desconto = Pedido.ValorDesconto.ToString().Replace(",", ".");
            dadosNFe.valor_total_ii = "0.00";
            dadosNFe.valor_ipi = "0.00";
            dadosNFe.valor_pis = "0.00";
            dadosNFe.valor_cofins = "0.00";
            dadosNFe.valor_outras_despesas = "0.00";
            dadosNFe.valor_total = Pedido.ValorTotal.ToString().Replace(",", ".");
            dadosNFe.modalidade_frete = "0";
            dadosNFe.informacoes_adicionais_contribuinte = "OPTANTE PELO SIMPLES NACIONAL. PEDIDO: #" + Pedido.Numero;

            #endregion

            #region PRODUTO

            int Count = 1;
            var ValorDescontoIten = Pedido.ValorDesconto / Pedido.PedidoItems.Count;
            foreach (var Item in Pedido.PedidoItems)
            {
                DadosNFEProduto novoDadosNFEProduto = new DadosNFEProduto();

                novoDadosNFEProduto.numero_item = Count.ToString();
                novoDadosNFEProduto.codigo_produto = Item.ProdutoTamanho.Produto.ID.ToString();
                novoDadosNFEProduto.descricao = Item.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + Item.ProdutoTamanho.Tamanho.Nome.ToUpper();

                string CodNCM = Item.ProdutoTamanho.Produto.CodigoNcm;
                decimal Aliquota = 0;
                if (!string.IsNullOrEmpty(Item.ProdutoTamanho.Produto.CodigoNcm))
                {
                    novoDadosNFEProduto.codigo_ncm = Item.ProdutoTamanho.Produto.CodigoNcm;

                    if (CodNCM == Helpers.EnumHelper.GetDescription(Entities.Produto.CodigosNCM.Flores))
                    {
                        Aliquota = decimal.Parse("31,45");
                    }
                }
                else
                {
                    throw new System.ArgumentException("NCM Não Preenchido ", "codigo_ncm");
                }

                if (Pedido.Cliente.Estado.Sigla == "SP")
                {
                    novoDadosNFEProduto.cfop = "5102";
                }
                else
                {
                    novoDadosNFEProduto.cfop = "6102";
                }

                string ValorItemStr = Item.Valor.ToString().Replace(",", ".");
                double ValorItemDou = (double)Item.Valor;

                novoDadosNFEProduto.unidade_comercial = "UN";
                novoDadosNFEProduto.quantidade_comercial = "1";
                novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                novoDadosNFEProduto.unidade_tributavel = "UN";
                novoDadosNFEProduto.quantidade_tributavel = "1";
                novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                novoDadosNFEProduto.valor_bruto = ValorItemStr;
                novoDadosNFEProduto.valor_desconto = ValorDescontoIten.ToString().Replace(",", ".");
                novoDadosNFEProduto.inclui_no_total = "1";
                novoDadosNFEProduto.icms_origem = "0";
                novoDadosNFEProduto.icms_situacao_tributaria = "102";
                novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);
                novoDadosNFEProduto.pis_situacao_tributaria = "99";
                novoDadosNFEProduto.cofins_situacao_tributaria = "99";

                dadosNFe.items.Add(novoDadosNFEProduto);

                Count++;
            }

            #endregion

            #endregion

            #region PREENCHE NF SUBSTITUTA

            if (Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
            {
                dadosNFe.nome_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.NomeRazao;

                #region IE

                dadosNFe.indicador_inscricao_estadual_destinatario = null;
                dadosNFe.inscricao_estadual_destinatario = null;

                if (Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "não contribuinte")
                    dadosNFe.indicador_inscricao_estadual_destinatario = "9";

                if (Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "isento")
                    dadosNFe.inscricao_estadual_destinatario = "ISENTO";

                if (Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "isento" && Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "não contribuinte")
                    dadosNFe.inscricao_estadual_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE.ToString();

                #endregion

                if (Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento.Length > 11)
                {
                    dadosNFe.cnpj_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                    dadosNFe.cpf_destinatario = null;
                }
                else
                {
                    dadosNFe.cpf_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                    dadosNFe.cnpj_destinatario = null;
                }

                dadosNFe.logradouro_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Logradouro + " " + Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Complemento;
                dadosNFe.numero_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Numero;
                dadosNFe.cep_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.CEP;
                dadosNFe.bairro_destinatario = Pedido.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Bairro;

                dadosNFe.informacoes_adicionais_contribuinte += " | " + Pedido.ObservacaoNFE + ".";
            }

            #endregion

            if (Configuracoes.HOMOLOGACAO)
                dadosNFe.nome_destinatario = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";

            Nf.NrReferenciaNF = CallFocus(TipoChamada.Emissao, OrigemPedido.Cliente, dadosNFe, null);

            return Nf;
        }

        public static NFE CallCORP_NEW_Remessa(NFE Nf)
        {
            var context = new COROASEntities();
            var pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            var faturamnetoRepository = new PersistentRepository<PedidoEmpresa>(context);

            PedidoEmpresa Pedido = null;
            string TipoDeNota = "";

            Pedido = pedidoEmpresaRepository.GetByExpression(r => r.ID == Nf.PedidoCORP).First();
            TipoDeNota = "Remessa";

            #region PREENCHE NF

            var dadosNFe = new DadosNFE();

            #region VOLUMES

            if (Pedido.NfeVolumeRemessa == null || Pedido.NfeVolumeRemessa == 0)
            {
                dadosNFe.volumes = new List<DadosNFEVolume>()
                {
                    new DadosNFEVolume()
                    {
                        quantidade = "1"
                    }
                };
            }
            else
            {
                dadosNFe.volumes = new List<DadosNFEVolume>()
                {
                    new DadosNFEVolume()
                    {
                        quantidade = Pedido.NfeVolumeRemessa.ToString()
                    }
                };
            }

            #endregion

            #region TIPO DE OPERACAO NF

            if (Nf.CFOP == "5923")
                dadosNFe.natureza_operacao = "Mercadoria - Ordem de terceiros - 5923";
            if (Nf.CFOP == "5949")
                dadosNFe.natureza_operacao = "Outra saída de mercadoria - 5949";
            if (Nf.CFOP == "6923")
                dadosNFe.natureza_operacao = "Mercadoria - Ordem de terceiros - 6923";
            if (Nf.CFOP == "6949")
                dadosNFe.natureza_operacao = "Outra saída de mercadoria - 6949";

            #endregion

            #region DADOS BASICOS 

            dadosNFe.forma_pagamento = "0";
            dadosNFe.data_emissao = DateTime.Now.ToString("yyyy-MM-dd");
            dadosNFe.data_entrada_saida = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            dadosNFe.tipo_documento = "1";
            dadosNFe.finalidade_emissao = "1";

            #endregion

            #region EMITENTE

            dadosNFe.cnpj_emitente = "26124808000174";
            dadosNFe.nome_emitente = "LAÇOS CORPORATIVOS COMERCIO DE PRESENTES EIRELI - ME";
            dadosNFe.logradouro_emitente = "Av. Lins de Vasconcelos";
            dadosNFe.numero_emitente = "1975";
            dadosNFe.bairro_emitente = "Vila Mariana";
            dadosNFe.municipio_emitente = "São Paulo";
            dadosNFe.uf_emitente = "SP";
            dadosNFe.cep_emitente = "01537-001";
            dadosNFe.telefone_emitente = "1140979405";
            dadosNFe.inscricao_estadual_emitente = "141229945118";
            dadosNFe.regime_tributario_emitente = "1";

            #endregion

            #region DESTINATARIO

            dadosNFe.cnpj_destinatario = Pedido.Empresa.CNPJ;

            if (Pedido.Empresa.InscricaoEstadual == "não contribuinte")
                dadosNFe.indicador_inscricao_estadual_destinatario = "9";

            if (Pedido.Empresa.InscricaoEstadual == "isento")
                dadosNFe.inscricao_estadual_destinatario = "ISENTO";

            if (Pedido.Empresa.InscricaoEstadual != "isento" && Pedido.Empresa.InscricaoEstadual != "não contribuinte")
                dadosNFe.inscricao_estadual_destinatario = Pedido.Empresa.InscricaoEstadual.ToString();

            dadosNFe.nome_destinatario = Pedido.Empresa.RazaoSocial;

            dadosNFe.logradouro_destinatario = Pedido.Empresa.Logradouro + " " + Pedido.Empresa.Complemento;
            if (dadosNFe.logradouro_destinatario.Length > 60)
                dadosNFe.logradouro_destinatario = dadosNFe.logradouro_destinatario.Substring(0, 59);

            if (dadosNFe.nome_destinatario.Length > 60)
                dadosNFe.nome_destinatario = dadosNFe.nome_destinatario.Substring(0, 59);

            if (string.IsNullOrEmpty(dadosNFe.logradouro_destinatario))
                throw new Exception("Rejeição: Logradouro Não Informado - Cod: XX");

            int Numero;
            bool successfullyParsed = int.TryParse(Pedido.Empresa.Numero, out Numero);
            if (successfullyParsed)
            {
                dadosNFe.numero_destinatario = Numero.ToString();
            }
            else
            {
                dadosNFe.numero_destinatario = "0";
            }

            dadosNFe.bairro_destinatario = Pedido.Empresa.Bairro;
            if (string.IsNullOrEmpty(dadosNFe.bairro_destinatario))
                throw new Exception("Rejeição: Bairro do destinatário não informado - Cod: XX");

            dadosNFe.municipio_destinatario = Pedido.Empresa.Cidade.Nome;
            dadosNFe.uf_destinatario = Pedido.Empresa.Estado.Sigla;

            if (Pedido.Empresa.Estado.Sigla == "SP")
            {
                dadosNFe.local_destino = "1";
            }
            else
            {
                dadosNFe.local_destino = "2";
            }

            if (Pedido.Empresa.Estado.Sigla == "DF")
            {
                dadosNFe.municipio_destinatario = "BRASÍLIA";
            }

            dadosNFe.cep_destinatario = Pedido.Empresa.CEP;
            dadosNFe.pais_destinatario = "Brasil";
            dadosNFe.telefone_destinatario = Pedido.Empresa.TelefoneContato.Replace("(", "").Replace(")", "").Replace(" ", "");

            #endregion

            #region TRANSPORTADOR E FRETE

            if (Pedido.TransportadorID != null)
            {
                // SE PJ
                if (Pedido.Transportador.TipoID == 2)
                {
                    dadosNFe.nome_transportador = Pedido.Transportador.RazaoSocial;
                    dadosNFe.cnpj_transportador = Pedido.Transportador.Documento;

                    if (Pedido.Transportador.IE != null)
                        dadosNFe.inscricao_estadual_transportador = Pedido.Transportador.IE;
                }
                else
                {
                    dadosNFe.nome_transportador = Pedido.Transportador.Nome;
                    dadosNFe.cpf_transportador = Pedido.Transportador.Documento;
                }

                dadosNFe.endereco_transportador = Pedido.Transportador.Logradouro + ", " + Pedido.Transportador.Numero;
                dadosNFe.municipio_transportador = Pedido.Transportador.Cidade.Nome.ToUpper();
                dadosNFe.uf_transportador = Pedido.Transportador.Cidade.Estado.Sigla;
            }

            if (Pedido.TipoFreteID != null)
            {
                switch (Pedido.TipoDeFrete)
                {
                    case PedidoEmpresa.TodosTiposDeFrete.Emitente:
                        dadosNFe.modalidade_frete = "0";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.Destinatario:
                        dadosNFe.modalidade_frete = "1";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.Terceiros:
                        dadosNFe.modalidade_frete = "2";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.SemFrete:
                        dadosNFe.modalidade_frete = "9";
                        break;
                }
            }
            else
            {
                dadosNFe.modalidade_frete = "9";
            }

            #endregion

            #region VALOR TOTAL

            decimal Frete = 0;
            decimal Desconto = 0;
            decimal SubTotal = 0;

            if (Pedido.NFeValorRemessaSubstitutivo == null || Pedido.NFeValorRemessaSubstitutivo == 0)
            {
                if (Pedido.ValorFrete != null)
                    Frete = (decimal)Pedido.ValorFrete;
                Desconto = Pedido.ValorDesconto;
                SubTotal = Pedido.Valor + Desconto;
            }
            else
            {
                if (Pedido.ValorFrete != null)
                    Frete = (decimal)Pedido.ValorFrete;
                Desconto = Pedido.ValorDesconto;
                SubTotal = (decimal)Pedido.NFeValorRemessaSubstitutivo;
            }

            dadosNFe.icms_base_calculo = "0.00";
            dadosNFe.icms_valor_total = "0.00";
            dadosNFe.icms_base_calculo_st = "0.00";
            dadosNFe.icms_valor_total_st = "0.00";
            dadosNFe.valor_seguro = "0.00";

            dadosNFe.valor_produtos = (SubTotal - Frete).ToString().Replace(",", ".");
            dadosNFe.valor_frete = Frete.ToString().Replace(",", ".");
            dadosNFe.valor_desconto = Desconto.ToString().Replace(",", ".");
            dadosNFe.valor_total = (SubTotal - Desconto).ToString().Replace(",", ".");

            dadosNFe.valor_total_ii = "0.00";
            dadosNFe.valor_ipi = "0.00";
            dadosNFe.valor_pis = "0.00";
            dadosNFe.valor_cofins = "0.00";
            dadosNFe.valor_outras_despesas = "0.00";

            string TextoPadraoNFPedido = "";

            var Local = "";
            if (Pedido.Local != null)
                Local += Pedido.Local.Titulo;

            //TextoPadraoNFPedido += "Colaborador: " + Pedido.Colaborador.PrimeiroNome.ToUpper() + " " + Pedido.Colaborador.Telefone + " " + Pedido.Colaborador.TelefoneCelular + "\r\n";

            var TelHomenagedo = "";
            if (Pedido.PedidoEmpresaNotas.Where(r => r.Observacoes.Contains("Telefone de Contato do Homenageado:")).Count() > 0)
            {
                TelHomenagedo = Pedido.PedidoEmpresaNotas.Where(r => r.Observacoes.Contains("Telefone de Contato do Homenageado:")).First().Observacoes.Replace("Telefone de Contato do Homenageado: ", "");
            }

            TextoPadraoNFPedido += "Homenageado: " + Pedido.PessoaHomenageada.ToUpper() + TelHomenagedo + "\r\n";
            //TextoPadraoNFPedido += "Funcionário: " + Pedido.NomeFuncionario.ToUpper() + "\r\n";

            if (!string.IsNullOrEmpty(Local))
            {
                TextoPadraoNFPedido += "Entrega: " + Local + " " + Pedido.ComplementoLocalEntrega + "\r\n";
            }
            else
            {
                TextoPadraoNFPedido += "Entrega: " + Pedido.Cidade.NomeCompleto + " " + Pedido.ComplementoLocalEntrega + "\r\n";
            }

            dadosNFe.informacoes_adicionais_contribuinte = "OPTANTE PELO SIMPLES NACIONAL. PEDIDO: " + Pedido.ID + ". \r\n" + TextoPadraoNFPedido;

            #endregion

            #region CALCULA DESCONTO ITEM E VALOR FRETE ITEM

            var QtdPedidosCoroaKit = 0;
            var QtdPedidosPersonalizados = 0;

            if (Pedido.ProdutoTamanho.Produto.Tipo == Produto.Tipos.CoroaFlor || Pedido.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe)
                QtdPedidosCoroaKit = 1;

            if (Pedido.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor && Pedido.ProdutoTamanho.Produto.Tipo != Produto.Tipos.Kitsbebe)
                QtdPedidosPersonalizados = 1;

            var QtdItensDadNF = QtdPedidosCoroaKit + QtdPedidosPersonalizados;

            var ValorFreteIten = Frete / QtdItensDadNF;

            #endregion

            List<PedidoEmpresa> LstPedidosEmpresa = new List<PedidoEmpresa>();

            LstPedidosEmpresa.Add(Pedido);

            int Count = 1;
            foreach (var PedidoResult in LstPedidosEmpresa)
            {
                if (PedidoResult.ProdutoTamanho.Produto.Tipo == Produto.Tipos.CoroaFlor || PedidoResult.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe)
                {
                    DadosNFEProduto novoDadosNFEProduto = new DadosNFEProduto();

                    novoDadosNFEProduto.numero_item = Count.ToString();
                    novoDadosNFEProduto.codigo_produto = PedidoResult.ProdutoTamanho.Produto.ID.ToString();
                    novoDadosNFEProduto.descricao = PedidoResult.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + PedidoResult.ProdutoTamanho.Tamanho.Nome.ToUpper();

                    string CodNCM = PedidoResult.ProdutoTamanho.Produto.CodigoNcm;
                    decimal Aliquota = 0;
                    if (!string.IsNullOrEmpty(PedidoResult.ProdutoTamanho.Produto.CodigoNcm))
                    {
                        novoDadosNFEProduto.codigo_ncm = PedidoResult.ProdutoTamanho.Produto.CodigoNcm;

                        if (CodNCM == Helpers.EnumHelper.GetDescription(Entities.Produto.CodigosNCM.Flores))
                        {
                            Aliquota = decimal.Parse("31,45");
                        }
                    }
                    else
                    {
                        throw new System.ArgumentException("NCM Não Preenchido ", "codigo_ncm");
                    }

                    novoDadosNFEProduto.cfop = Nf.CFOP;

                    // ADICIONA PREÇO
                    decimal ValorProduto = 0;
                    //ValorProduto = PedidoResult.Valor + PedidoResult.ValorDesconto;

                    // VERIFICA SE TEM PREÇO PERSONALIZADO
                    if (PedidoResult.Empresa.EmpresaProdutoTamanhoes.Where(r => r.ProdutoTamanhoID == PedidoResult.ProdutoTamanhoID && r.Disponivel == true).Count() > 0)
                    {
                        ValorProduto = PedidoResult.Empresa.EmpresaProdutoTamanhoes.Where(r => r.ProdutoTamanhoID == PedidoResult.ProdutoTamanhoID && r.Disponivel == true).First().Valor;
                    }
                    else
                    {
                        ValorProduto = PedidoResult.ProdutoTamanho.Preco;
                    }

                    string ValorItemStr = ValorProduto.ToString().Replace(",", ".");
                    double ValorItemDou = (double)ValorProduto;

                    if (Pedido.NFeValorRemessaSubstitutivo != null && Pedido.NFeValorRemessaSubstitutivo > 0)
                    {
                        ValorItemStr = Pedido.NFeValorRemessaSubstitutivo.ToString().Replace(",", ".");
                        ValorItemDou = (double)Pedido.NFeValorRemessaSubstitutivo;
                    }

                    novoDadosNFEProduto.quantidade_comercial = "1";
                    novoDadosNFEProduto.quantidade_tributavel = "1";

                    novoDadosNFEProduto.unidade_comercial = "UN";
                    novoDadosNFEProduto.unidade_tributavel = "UN";

                    // VALORES
                    novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                    novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                    novoDadosNFEProduto.valor_bruto = ValorItemStr.ToString().Replace(",", ".");
                    novoDadosNFEProduto.valor_frete = ValorFreteIten.ToString().Replace(",", ".");

                    // DESCONTO
                    novoDadosNFEProduto.valor_desconto = PedidoResult.ValorDesconto.ToString().Replace(",", ".");

                    novoDadosNFEProduto.inclui_no_total = "1";
                    novoDadosNFEProduto.icms_origem = "0";
                    novoDadosNFEProduto.icms_situacao_tributaria = "102";
                    novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                    novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);
                    novoDadosNFEProduto.pis_situacao_tributaria = "99";
                    novoDadosNFEProduto.cofins_situacao_tributaria = "99";

                    dadosNFe.items.Add(novoDadosNFEProduto);

                    Count++;
                }
                else
                {
                    List<PedidoEmpresaNF> LstPeidoEmpresaNF = new List<PedidoEmpresaNF>();

                    if (PedidoResult.PedidoEmpresaNFs != null && PedidoResult.PedidoEmpresaNFs.Count > 0)
                    {
                        foreach (var ProdutoNFPedido in PedidoResult.PedidoEmpresaNFs)
                        {
                            LstPeidoEmpresaNF.Add(ProdutoNFPedido);
                        }
                    }
                    else
                    {
                        PedidoEmpresaNF NovoPedidoEmpresaNF = new PedidoEmpresaNF();
                        LstPeidoEmpresaNF.Add(NovoPedidoEmpresaNF);
                    }

                    // ITEM DO PEDIDO
                    foreach (var ProdutoNFPedido in LstPeidoEmpresaNF)
                    {
                        DadosNFEProduto novoDadosNFEProduto = new DadosNFEProduto();
                        decimal Aliquota = decimal.Parse("31,45");

                        novoDadosNFEProduto.cfop = Nf.CFOP;

                        novoDadosNFEProduto.numero_item = Count.ToString();
                        novoDadosNFEProduto.codigo_produto = Pedido.ProdutoTamanho.Produto.ID.ToString();
                        novoDadosNFEProduto.descricao = Pedido.ProdutoTamanho.Produto.Nome + " - " + Pedido.DescricaoProduto;
                        novoDadosNFEProduto.codigo_ncm = Pedido.ProdutoTamanho.Produto.CodigoNcm;

                        string ValorItemStr = Pedido.Valor.ToString().Replace(",", ".");
                        double ValorItemDou = (double)Pedido.Valor;

                        if (Pedido.NFeValorRemessaSubstitutivo != null && Pedido.NFeValorRemessaSubstitutivo > 0)
                        {
                            ValorItemStr = Pedido.NFeValorRemessaSubstitutivo.ToString().Replace(",", ".");
                            ValorItemDou = (double)Pedido.NFeValorRemessaSubstitutivo;
                        }

                        // VALORES
                        novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                        novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                        novoDadosNFEProduto.valor_bruto = (ValorItemDou * 1).ToString().Replace(",", ".");
                        novoDadosNFEProduto.valor_frete = ValorFreteIten.ToString().Replace(",", ".");

                        // QUANTIDADE
                        novoDadosNFEProduto.quantidade_tributavel = "1";
                        novoDadosNFEProduto.quantidade_comercial = "1";

                        // TRIBUTAÇÃO
                        novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                        novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);


                        // DESCONTO
                        novoDadosNFEProduto.valor_desconto = "0.00";

                        // UNIDADES
                        novoDadosNFEProduto.unidade_tributavel = "UN";
                        novoDadosNFEProduto.unidade_comercial = "UN";
                        novoDadosNFEProduto.inclui_no_total = "1";

                        novoDadosNFEProduto.icms_origem = "0";
                        novoDadosNFEProduto.icms_situacao_tributaria = "102";
                        novoDadosNFEProduto.pis_situacao_tributaria = "99";
                        novoDadosNFEProduto.cofins_situacao_tributaria = "99";

                        dadosNFe.items.Add(novoDadosNFEProduto);

                        Count++;
                    }
                }
            }

            #endregion

            #region PREENCHE NF SUBSTITUTA

            if (Pedido.Faturamento != null && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
            {
                dadosNFe.nome_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.NomeRazao;

                #region IE

                dadosNFe.indicador_inscricao_estadual_destinatario = null;
                dadosNFe.inscricao_estadual_destinatario = null;

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "não contribuinte")
                    dadosNFe.indicador_inscricao_estadual_destinatario = "9";

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "isento")
                    dadosNFe.inscricao_estadual_destinatario = "ISENTO";

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "isento" && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "não contribuinte")
                    dadosNFe.inscricao_estadual_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE.ToString();

                #endregion

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento.Length > 11)
                {
                    dadosNFe.cnpj_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                }
                else
                {
                    dadosNFe.cpf_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                }

                dadosNFe.logradouro_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Logradouro + " " + Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Complemento;
                dadosNFe.numero_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Numero;
                dadosNFe.cep_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.CEP;
                dadosNFe.bairro_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Bairro;
            }

            #endregion

            #region HOMOLOGACAO

            if (Configuracoes.HOMOLOGACAO)
                dadosNFe.nome_destinatario = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";

            // REMVOER ANTES DA PRODUCAO
            //dadosNFe.nome_destinatario = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";

            #endregion

            Nf.NrReferenciaNF = CallFocus(TipoChamada.Emissao, OrigemPedido.Empresa, dadosNFe, null);

            return Nf;
        }


        public static NFE CallCORP(NFE Nf)
        {
            var context = new COROASEntities();
            var pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            var faturamnetoRepository = new PersistentRepository<PedidoEmpresa>(context);

            PedidoEmpresa Pedido = null;
            string TipoDeNota = "";

            if (Nf.IdFat > 0)
            {
                Pedido = pedidoEmpresaRepository.GetByExpression(r => r.FaturamentoID == Nf.IdFat).First();
                TipoDeNota = "Venda";
            }

            if (Nf.PedidoCORP > 0)
            {
                Pedido = pedidoEmpresaRepository.GetByExpression(r => r.ID == Nf.PedidoCORP).First();
                TipoDeNota = "Remessa";
            }

            #region PREENCHE NF

            var dadosNFe = new DadosNFE();

            if (TipoDeNota == "Venda")
            {
                dadosNFe.natureza_operacao = "Venda";
            }
            else
            {
                #region VOLUMES

                if (Pedido.NfeVolumeRemessa == null || Pedido.NfeVolumeRemessa == 0)
                {
                    dadosNFe.volumes = new List<DadosNFEVolume>()
                    {
                        new DadosNFEVolume()
                        {
                            quantidade = "1"
                        }
                    };
                }
                else
                {
                    dadosNFe.volumes = new List<DadosNFEVolume>()
                    {
                        new DadosNFEVolume()
                        {
                            quantidade = Pedido.NfeVolumeRemessa.ToString()
                        }
                    };
                }

                #endregion

                if (Nf.CFOP == "5923")
                    dadosNFe.natureza_operacao = "Mercadoria - Ordem de terceiros - 5923";
                if (Nf.CFOP == "5949")
                    dadosNFe.natureza_operacao = "Outra saída de mercadoria - 5949";
                if (Nf.CFOP == "6923")
                    dadosNFe.natureza_operacao = "Mercadoria - Ordem de terceiros - 6923";
                if (Nf.CFOP == "6949")
                    dadosNFe.natureza_operacao = "Outra saída de mercadoria - 6949";
            }

            dadosNFe.forma_pagamento = "0";
            dadosNFe.data_emissao = DateTime.Now.ToString("yyyy-MM-dd");
            dadosNFe.data_entrada_saida = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            dadosNFe.tipo_documento = "1";
            dadosNFe.finalidade_emissao = "1";

            #region EMITENTE

            dadosNFe.cnpj_emitente = "26124808000174";
            dadosNFe.nome_emitente = "LAÇOS CORPORATIVOS COMERCIO DE PRESENTES EIRELI - ME";
            dadosNFe.logradouro_emitente = "Av. Lins de Vasconcelos";
            dadosNFe.numero_emitente = "1975";
            dadosNFe.bairro_emitente = "Vila Mariana";
            dadosNFe.municipio_emitente = "São Paulo";
            dadosNFe.uf_emitente = "SP";
            dadosNFe.cep_emitente = "01537-001";
            dadosNFe.telefone_emitente = "1140979405";
            dadosNFe.inscricao_estadual_emitente = "141229945118";
            dadosNFe.regime_tributario_emitente = "1";

            #endregion

            #region DESTINATARIO

            dadosNFe.cnpj_destinatario = Pedido.Empresa.CNPJ;

            if (Pedido.Empresa.InscricaoEstadual == "não contribuinte")
                dadosNFe.indicador_inscricao_estadual_destinatario = "9";

            if (Pedido.Empresa.InscricaoEstadual == "isento")
                dadosNFe.inscricao_estadual_destinatario = "ISENTO";

            if (Pedido.Empresa.InscricaoEstadual != "isento" && Pedido.Empresa.InscricaoEstadual != "não contribuinte")
                dadosNFe.inscricao_estadual_destinatario = Pedido.Empresa.InscricaoEstadual.ToString();

            dadosNFe.nome_destinatario = Pedido.Empresa.RazaoSocial;

            dadosNFe.logradouro_destinatario = Pedido.Empresa.Logradouro + " " + Pedido.Empresa.Complemento;
            if (dadosNFe.logradouro_destinatario.Length > 60)
                dadosNFe.logradouro_destinatario = dadosNFe.logradouro_destinatario.Substring(0, 59);

            if (dadosNFe.nome_destinatario.Length > 60)
                dadosNFe.nome_destinatario = dadosNFe.nome_destinatario.Substring(0, 59);

            if (string.IsNullOrEmpty(dadosNFe.logradouro_destinatario))
                throw new Exception("Rejeição: Logradouro Não Informado - Cod: XX");

            int Numero;
            bool successfullyParsed = int.TryParse(Pedido.Empresa.Numero, out Numero);
            if (successfullyParsed)
            {
                dadosNFe.numero_destinatario = Numero.ToString();
            }
            else
            {
                dadosNFe.numero_destinatario = "0";
            }

            dadosNFe.bairro_destinatario = Pedido.Empresa.Bairro;
            if (string.IsNullOrEmpty(dadosNFe.bairro_destinatario))
                throw new Exception("Rejeição: Bairro do destinatário não informado - Cod: XX");

            dadosNFe.municipio_destinatario = Pedido.Empresa.Cidade.Nome;
            dadosNFe.uf_destinatario = Pedido.Empresa.Estado.Sigla;

            if (Pedido.Empresa.Estado.Sigla == "SP")
            {
                dadosNFe.local_destino = "1";
            }
            else
            {
                dadosNFe.local_destino = "2";
            }

            if (Pedido.Empresa.Estado.Sigla == "DF")
            {
                dadosNFe.municipio_destinatario = "BRASÍLIA";
            }

            dadosNFe.cep_destinatario = Pedido.Empresa.CEP;
            dadosNFe.pais_destinatario = "Brasil";
            dadosNFe.telefone_destinatario = Pedido.Empresa.TelefoneContato.Replace("(", "").Replace(")", "").Replace(" ", "");

            #endregion

            #region TRANSPORTADOR E FRETE

            if (Pedido.TransportadorID != null)
            {
                // SE PJ
                if (Pedido.Transportador.TipoID == 2)
                {
                    dadosNFe.nome_transportador = Pedido.Transportador.RazaoSocial;
                    dadosNFe.cnpj_transportador = Pedido.Transportador.Documento;

                    if (Pedido.Transportador.IE != null)
                        dadosNFe.inscricao_estadual_transportador = Pedido.Transportador.IE;
                }
                else
                {
                    dadosNFe.nome_transportador = Pedido.Transportador.Nome;
                    dadosNFe.cpf_transportador = Pedido.Transportador.Documento;
                }

                dadosNFe.endereco_transportador = Pedido.Transportador.Logradouro + ", " + Pedido.Transportador.Numero;
                dadosNFe.municipio_transportador = Pedido.Transportador.Cidade.Nome.ToUpper();
                dadosNFe.uf_transportador = Pedido.Transportador.Cidade.Estado.Sigla;
            }

            if (Pedido.TipoFreteID != null)
            {
                switch (Pedido.TipoDeFrete)
                {
                    case PedidoEmpresa.TodosTiposDeFrete.Emitente:
                        dadosNFe.modalidade_frete = "0";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.Destinatario:
                        dadosNFe.modalidade_frete = "1";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.Terceiros:
                        dadosNFe.modalidade_frete = "2";
                        break;
                    case PedidoEmpresa.TodosTiposDeFrete.SemFrete:
                        dadosNFe.modalidade_frete = "9";
                        break;
                }
            }
            else
            {
                dadosNFe.modalidade_frete = "9";
            }

            #endregion

            #region VALOR TOTAL

            decimal Frete = 0;
            decimal Desconto = 0;
            decimal SubTotal = 0;

            if (TipoDeNota == "Venda")
            {
                Frete = (decimal)Pedido.Faturamento.PedidoEmpresas.Sum(r => r.ValorFrete);
                Desconto = Pedido.Faturamento.PedidoEmpresas.Sum(r => r.ValorDesconto);
                SubTotal = Pedido.Faturamento.ValorTotal + Desconto;
            }
            else
            {
                if (Pedido.NFeValorRemessaSubstitutivo == null || Pedido.NFeValorRemessaSubstitutivo == 0)
                {
                    if (Pedido.ValorFrete != null)
                        Frete = (decimal)Pedido.ValorFrete;
                    Desconto = Pedido.ValorDesconto;
                    SubTotal = Pedido.Valor + Desconto;
                }
                else
                {
                    if (Pedido.ValorFrete != null)
                        Frete = (decimal)Pedido.ValorFrete;
                    Desconto = Pedido.ValorDesconto;
                    SubTotal = (decimal)Pedido.NFeValorRemessaSubstitutivo;
                }
            }

            dadosNFe.icms_base_calculo = "0.00";
            dadosNFe.icms_valor_total = "0.00";
            dadosNFe.icms_base_calculo_st = "0.00";
            dadosNFe.icms_valor_total_st = "0.00";
            dadosNFe.valor_seguro = "0.00";

            dadosNFe.valor_produtos = (SubTotal - Frete).ToString().Replace(",", ".");
            dadosNFe.valor_frete = Frete.ToString().Replace(",", ".");
            dadosNFe.valor_desconto = Desconto.ToString().Replace(",", ".");
            dadosNFe.valor_total = (SubTotal - Desconto).ToString().Replace(",", ".");

            dadosNFe.valor_total_ii = "0.00";
            dadosNFe.valor_ipi = "0.00";
            dadosNFe.valor_pis = "0.00";
            dadosNFe.valor_cofins = "0.00";
            dadosNFe.valor_outras_despesas = "0.00";

            string TextoPadraoNFPedido = "";
            string TextoOCPedido = "";
            string TextoPedidos = "";

            if (TipoDeNota == "Venda")
            {
                foreach (var PedidoFat in Pedido.Faturamento.PedidoEmpresas)
                {
                    if (!string.IsNullOrEmpty(PedidoFat.TextoPadraoNF))
                        TextoPadraoNFPedido += PedidoFat.TextoPadraoNF + " | ";

                    if (!string.IsNullOrEmpty(PedidoFat.OrdemDeCompra))
                        TextoOCPedido += PedidoFat.OrdemDeCompra + " | ";

                    TextoPedidos += "#" + PedidoFat.ID + " / ";
                }

                dadosNFe.informacoes_adicionais_contribuinte = "OPTANTE PELO SIMPLES NACIONAL. PEDIDOS: " + TextoPedidos + ". " + TextoPadraoNFPedido + " - " + TextoOCPedido;
            }
            else
            {
                var Local = "";
                if (Pedido.Local != null)
                    Local += Pedido.Local.Titulo;

                //TextoPadraoNFPedido += "Colaborador: " + Pedido.Colaborador.PrimeiroNome.ToUpper() + " " + Pedido.Colaborador.Telefone + " " + Pedido.Colaborador.TelefoneCelular + "\r\n";

                var TelHomenagedo = "";
                if (Pedido.PedidoEmpresaNotas.Where(r => r.Observacoes.Contains("Telefone de Contato do Homenageado:")).Count() > 0)
                {
                    TelHomenagedo = Pedido.PedidoEmpresaNotas.Where(r => r.Observacoes.Contains("Telefone de Contato do Homenageado:")).First().Observacoes.Replace("Telefone de Contato do Homenageado: ", "");
                }

                TextoPadraoNFPedido += "Homenageado: " + Pedido.PessoaHomenageada.ToUpper() + TelHomenagedo + "\r\n";
                //TextoPadraoNFPedido += "Funcionário: " + Pedido.NomeFuncionario.ToUpper() + "\r\n";

                if (!string.IsNullOrEmpty(Local))
                {
                    TextoPadraoNFPedido += "Entrega: " + Local + " " + Pedido.ComplementoLocalEntrega + "\r\n";
                }
                else
                {
                    TextoPadraoNFPedido += "Entrega: " + Pedido.Cidade.NomeCompleto + " " + Pedido.ComplementoLocalEntrega + "\r\n";
                }

                dadosNFe.informacoes_adicionais_contribuinte = "OPTANTE PELO SIMPLES NACIONAL. PEDIDO: " + Pedido.ID + ". \r\n" + TextoPadraoNFPedido;
            }

            #endregion

            #region CALCULA DESCONTO ITEM E VALOR FRETE ITEM

            var QtdPedidosCoroaKit = 0;
            var QtdPedidosPersonalizados = 0;

            if (Pedido.Faturamento == null || TipoDeNota != "Venda")
            {
                if (Pedido.ProdutoTamanho.Produto.Tipo == Produto.Tipos.CoroaFlor || Pedido.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe)
                    QtdPedidosCoroaKit = 1;

                if (Pedido.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor && Pedido.ProdutoTamanho.Produto.Tipo != Produto.Tipos.Kitsbebe)
                    QtdPedidosPersonalizados = 1;
            }
            else
            {
                QtdPedidosCoroaKit = Pedido.Faturamento.PedidoEmpresas.Where(r => r.ProdutoTamanho.Produto.Tipo == Produto.Tipos.CoroaFlor || r.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe).Count();

                foreach (var item in Pedido.Faturamento.PedidoEmpresas.Where(r => r.ProdutoTamanho.Produto.Tipo != Produto.Tipos.CoroaFlor && r.ProdutoTamanho.Produto.Tipo != Produto.Tipos.Kitsbebe).ToList())
                {
                    QtdPedidosPersonalizados += item.PedidoEmpresaNFs.Count;
                }
            }

            var QtdItensDadNF = QtdPedidosCoroaKit + QtdPedidosPersonalizados;

            var ValorFreteIten = Frete / QtdItensDadNF;

            #endregion

            List<PedidoEmpresa> LstPedidosEmpresa = new List<PedidoEmpresa>();

            if (Pedido.Faturamento != null && TipoDeNota == "Venda")
            {
                foreach (var PedidoResult in Pedido.Faturamento.PedidoEmpresas)
                {
                    LstPedidosEmpresa.Add((PedidoEmpresa)PedidoResult);
                }
            }
            else
            {
                LstPedidosEmpresa.Add(Pedido);
            }

            int Count = 1;
            foreach (var PedidoResult in LstPedidosEmpresa)
            {







                if (PedidoResult.ProdutoTamanho.Produto.Tipo == Produto.Tipos.CoroaFlor || PedidoResult.ProdutoTamanho.Produto.Tipo == Produto.Tipos.Kitsbebe)
                {
                    DadosNFEProduto novoDadosNFEProduto = new DadosNFEProduto();

                    novoDadosNFEProduto.numero_item = Count.ToString();
                    novoDadosNFEProduto.codigo_produto = PedidoResult.ProdutoTamanho.Produto.ID.ToString();
                    novoDadosNFEProduto.descricao = PedidoResult.ProdutoTamanho.Produto.Nome.ToUpper() + " - " + PedidoResult.ProdutoTamanho.Tamanho.Nome.ToUpper();

                    string CodNCM = PedidoResult.ProdutoTamanho.Produto.CodigoNcm;
                    decimal Aliquota = 0;
                    if (!string.IsNullOrEmpty(PedidoResult.ProdutoTamanho.Produto.CodigoNcm))
                    {
                        novoDadosNFEProduto.codigo_ncm = PedidoResult.ProdutoTamanho.Produto.CodigoNcm;

                        if (CodNCM == Helpers.EnumHelper.GetDescription(Entities.Produto.CodigosNCM.Flores))
                        {
                            Aliquota = decimal.Parse("31,45");
                        }
                    }
                    else
                    {
                        throw new System.ArgumentException("NCM Não Preenchido ", "codigo_ncm");
                    }

                    if (TipoDeNota == "Venda")
                    {
                        if (PedidoResult.Empresa.Estado.Sigla == "SP")
                        {
                            novoDadosNFEProduto.cfop = "5102";
                        }
                        else
                        {
                            novoDadosNFEProduto.cfop = "6102";
                        }
                    }
                    else
                    {
                        novoDadosNFEProduto.cfop = Nf.CFOP;
                    }

                    // ADICIONA PREÇO
                    decimal ValorProduto = 0;
                    //ValorProduto = PedidoResult.Valor + PedidoResult.ValorDesconto;

                    // VERIFICA SE TEM PREÇO PERSONALIZADO
                    if (PedidoResult.Empresa.EmpresaProdutoTamanhoes.Where(r => r.ProdutoTamanhoID == PedidoResult.ProdutoTamanhoID && r.Disponivel == true).Count() > 0)
                    {
                        ValorProduto = PedidoResult.Empresa.EmpresaProdutoTamanhoes.Where(r => r.ProdutoTamanhoID == PedidoResult.ProdutoTamanhoID && r.Disponivel == true).First().Valor;
                    }
                    else
                    {
                        ValorProduto = PedidoResult.ProdutoTamanho.Preco;
                    }

                    string ValorItemStr = ValorProduto.ToString().Replace(",", ".");
                    double ValorItemDou = (double)ValorProduto;

                    if (Pedido.NFeValorRemessaSubstitutivo != null && Pedido.NFeValorRemessaSubstitutivo > 0)
                    {
                        ValorItemStr = Pedido.NFeValorRemessaSubstitutivo.ToString().Replace(",", ".");
                        ValorItemDou = (double)Pedido.NFeValorRemessaSubstitutivo;
                    }

                    novoDadosNFEProduto.quantidade_comercial = "1";
                    novoDadosNFEProduto.quantidade_tributavel = "1";

                    novoDadosNFEProduto.unidade_comercial = "UN";
                    novoDadosNFEProduto.unidade_tributavel = "UN";

                    // VALORES
                    novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                    novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                    novoDadosNFEProduto.valor_bruto = ValorItemStr.ToString().Replace(",", ".");
                    novoDadosNFEProduto.valor_frete = ValorFreteIten.ToString().Replace(",", ".");

                    // DESCONTO
                    novoDadosNFEProduto.valor_desconto = PedidoResult.ValorDesconto.ToString().Replace(",", ".");

                    novoDadosNFEProduto.inclui_no_total = "1";
                    novoDadosNFEProduto.icms_origem = "0";
                    novoDadosNFEProduto.icms_situacao_tributaria = "102";
                    novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                    novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);
                    novoDadosNFEProduto.pis_situacao_tributaria = "99";
                    novoDadosNFEProduto.cofins_situacao_tributaria = "99";

                    dadosNFe.items.Add(novoDadosNFEProduto);

                    Count++;
                }
                else
                {
                    List<PedidoEmpresaNF> LstPeidoEmpresaNF = new List<PedidoEmpresaNF>();

                    if (PedidoResult.PedidoEmpresaNFs != null && PedidoResult.PedidoEmpresaNFs.Count > 0)
                    {
                        foreach (var ProdutoNFPedido in PedidoResult.PedidoEmpresaNFs)
                        {
                            LstPeidoEmpresaNF.Add(ProdutoNFPedido);
                        }
                    }
                    else
                    {
                        PedidoEmpresaNF NovoPedidoEmpresaNF = new PedidoEmpresaNF();
                        LstPeidoEmpresaNF.Add(NovoPedidoEmpresaNF);
                    }

                    // ITEM DO PEDIDO
                    foreach (var ProdutoNFPedido in LstPeidoEmpresaNF)
                    {
                        DadosNFEProduto novoDadosNFEProduto = new DadosNFEProduto();
                        decimal Aliquota = decimal.Parse("31,45");

                        if (TipoDeNota == "Venda")
                        {
                            if (PedidoResult.Empresa.Estado.Sigla == "SP")
                            {
                                novoDadosNFEProduto.cfop = "5102";
                            }
                            else
                            {
                                novoDadosNFEProduto.cfop = "6102";
                            }

                            novoDadosNFEProduto.numero_item = Count.ToString();
                            novoDadosNFEProduto.codigo_produto = ProdutoNFPedido.ProdutoFaturamento.ID.ToString();
                            novoDadosNFEProduto.descricao = ProdutoNFPedido.ProdutoFaturamento.NomeProdutoFaturamento.ToUpper();
                            novoDadosNFEProduto.codigo_ncm = ProdutoNFPedido.ProdutoFaturamento.NCM.CodigoNCM;

                            string ValorItemStr = ProdutoNFPedido.ValorUnitario.ToString().Replace(",", ".");
                            double ValorItemDou = (double)ProdutoNFPedido.ValorUnitario;

                            if (Pedido.NFeValorRemessaSubstitutivo != null && Pedido.NFeValorRemessaSubstitutivo > 0)
                            {
                                ValorItemStr = Pedido.NFeValorRemessaSubstitutivo.ToString().Replace(",", ".");
                                ValorItemDou = (double)Pedido.NFeValorRemessaSubstitutivo;
                            }

                            // VALORES
                            novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                            novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                            novoDadosNFEProduto.valor_bruto = (ValorItemDou * ProdutoNFPedido.Qtd).ToString().Replace(",", ".");
                            novoDadosNFEProduto.valor_frete = ValorFreteIten.ToString().Replace(",", ".");

                            // QUANTIDADE
                            novoDadosNFEProduto.quantidade_tributavel = ProdutoNFPedido.Qtd.ToString();
                            novoDadosNFEProduto.quantidade_comercial = ProdutoNFPedido.Qtd.ToString();

                            // TRIBUTAÇÃO
                            novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                            novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            novoDadosNFEProduto.cfop = Nf.CFOP;

                            novoDadosNFEProduto.numero_item = Count.ToString();
                            novoDadosNFEProduto.codigo_produto = Pedido.ProdutoTamanho.Produto.ID.ToString();
                            novoDadosNFEProduto.descricao = Pedido.ProdutoTamanho.Produto.Nome + " - " + Pedido.DescricaoProduto;
                            novoDadosNFEProduto.codigo_ncm = Pedido.ProdutoTamanho.Produto.CodigoNcm;

                            string ValorItemStr = Pedido.Valor.ToString().Replace(",", ".");
                            double ValorItemDou = (double)Pedido.Valor;

                            if (Pedido.NFeValorRemessaSubstitutivo != null && Pedido.NFeValorRemessaSubstitutivo > 0)
                            {
                                ValorItemStr = Pedido.NFeValorRemessaSubstitutivo.ToString().Replace(",", ".");
                                ValorItemDou = (double)Pedido.NFeValorRemessaSubstitutivo;
                            }

                            // VALORES
                            novoDadosNFEProduto.valor_unitario_comercial = ValorItemStr;
                            novoDadosNFEProduto.valor_unitario_tributavel = ValorItemStr;
                            novoDadosNFEProduto.valor_bruto = (ValorItemDou * 1).ToString().Replace(",", ".");
                            novoDadosNFEProduto.valor_frete = ValorFreteIten.ToString().Replace(",", ".");

                            // QUANTIDADE
                            novoDadosNFEProduto.quantidade_tributavel = "1";
                            novoDadosNFEProduto.quantidade_comercial = "1";

                            // TRIBUTAÇÃO
                            novoDadosNFEProduto.icms_aliquota = Aliquota.ToString().Replace(",", ".");
                            novoDadosNFEProduto.icms_valor = Math.Round(((double)Aliquota / 100) * ValorItemDou, 2).ToString(" 0.00", CultureInfo.InvariantCulture);
                        }


                        // DESCONTO
                        novoDadosNFEProduto.valor_desconto = "0.00";

                        // UNIDADES
                        novoDadosNFEProduto.unidade_tributavel = "UN";
                        novoDadosNFEProduto.unidade_comercial = "UN";
                        novoDadosNFEProduto.inclui_no_total = "1";

                        novoDadosNFEProduto.icms_origem = "0";
                        novoDadosNFEProduto.icms_situacao_tributaria = "102";
                        novoDadosNFEProduto.pis_situacao_tributaria = "99";
                        novoDadosNFEProduto.cofins_situacao_tributaria = "99";

                        dadosNFe.items.Add(novoDadosNFEProduto);

                        Count++;
                    }
                }
            }

            #endregion

            #region PREENCHE NF SUBSTITUTA

            if (Pedido.Faturamento != null && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.Any() && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta != null)
            {
                dadosNFe.nome_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.NomeRazao;


                #region IE

                dadosNFe.indicador_inscricao_estadual_destinatario = null;
                dadosNFe.inscricao_estadual_destinatario = null;

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "não contribuinte")
                    dadosNFe.indicador_inscricao_estadual_destinatario = "9";

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE == "isento")
                    dadosNFe.inscricao_estadual_destinatario = "ISENTO";

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "isento" && Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE != "não contribuinte")
                    dadosNFe.inscricao_estadual_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.IE.ToString();

                #endregion

                if (Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento.Length > 11)
                {
                    dadosNFe.cnpj_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                }
                else
                {
                    dadosNFe.cpf_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Documento;
                }

                dadosNFe.logradouro_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Logradouro + " " + Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Complemento;
                dadosNFe.numero_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Numero;
                dadosNFe.cep_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.CEP;
                dadosNFe.bairro_destinatario = Pedido.Faturamento.NFSubstituta_X_Pedido_PedidoEmpresa.First().NFSubstituta.Bairro;
            }

            #endregion

            #region HOMOLOGACAO

            if (Configuracoes.HOMOLOGACAO)
                dadosNFe.nome_destinatario = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";

            // REMVOER ANTES DA PRODUCAO
            //dadosNFe.nome_destinatario = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";

            #endregion

            Nf.NrReferenciaNF = CallFocus(TipoChamada.Emissao, OrigemPedido.Empresa, dadosNFe, null);

            return Nf;
        }
        public static void CallCPVCancelar(string NrReferencia)
        {
            CallFocus(TipoChamada.Cancelamento, OrigemPedido.Cliente, null, NrReferencia);
        }
        public static string CallFocus(TipoChamada novaChamada, OrigemPedido OrigemPedido, object dadosNFe = null, string NrReferenciaNFParam = null)
        {
            Random generator = new Random();
            string NrReferenciaNF = "";

            if (string.IsNullOrEmpty(NrReferenciaNFParam))
            {
                NrReferenciaNF = generator.Next(0, 1000000000).ToString("D9");
            }
            else
            {
                NrReferenciaNF = NrReferenciaNFParam;
            }

            StringWriter sw = new StringWriter();
            var serializer = new Serializer();

            byte[] bytes = null;
            if (dadosNFe != null)
            {
                serializer.Serialize(sw, dadosNFe);
                bytes = System.Text.Encoding.UTF8.GetBytes(sw.ToString());
            }

            using (WebClient webClient = new WebClient())
            {
                string URL = "";
                byte[] response = null;

                string Token = "";

                if (OrigemPedido == OrigemPedido.Cliente)
                    Token = Configuracoes.Focus_Token;

                if (OrigemPedido == OrigemPedido.Empresa)
                    Token = Configuracoes.Focus_Token_LC;

                if (novaChamada == TipoChamada.Emissao)
                {
                    if (OrigemPedido == OrigemPedido.Empresa)
                    {
                        URL = Configuracoes.Focus_Autorizar + "autorizar?token=" + Token + "&ref=" + NrReferenciaNF;
                        //URL = "http://homologacao.acrasnfe.acras.com.br/nfe2/" + "autorizar?token=" + Token + "&ref=" + NrReferenciaNF;
                    }
                    else
                    {
                        URL = Configuracoes.Focus_Autorizar + "autorizar?token=" + Token + "&ref=" + NrReferenciaNF;
                    }

                    try
                    {
                        response = webClient.UploadData(URL, "POST", bytes);
                    }
                    catch (WebException wex)
                    {
                        if (wex.Response != null)
                        {
                            using (var errorResponse = (HttpWebResponse)wex.Response)
                            {
                                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                {
                                    throw new Exception(reader.ReadToEnd());
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("Erro NFEFactory.CS Round Line 685");
                        }
                    }
                }
                if (novaChamada == TipoChamada.Cancelamento)
                {
                    URL = Configuracoes.Focus_Autorizar + "cancelar?token=" + Token + "&ref=" + NrReferenciaNF;

                    System.Collections.Specialized.NameValueCollection valores = new System.Collections.Specialized.NameValueCollection();
                    valores.Add("justificativa", "testandoambientedehomolog");

                    response = webClient.UploadValues(URL, valores);
                }
            }

            return NrReferenciaNF;
        }
        public static NFE Emitir(NFE Nf, OrigemPedido OrigemPedido)
        {
            if (OrigemPedido == OrigemPedido.Cliente)
            {
                Nf = CallCPV(Nf);
            }
            else
            {
                Nf = CallCORP(Nf);
            }

            return Nf;
        }
        public static void Cancelar(string NrReferencia, OrigemPedido OrigemPedido)
        {
            if (OrigemPedido == OrigemPedido.Cliente)
            {
                CallCPVCancelar(NrReferencia);
            }
        }
        public static NFE Consultar(string NrReferenciaNF, NFE.TiposConsulta tipodeConsulta, OrigemPedido OrigemPedido, int? PedidoID)
        {
            var NFRetorno = new NFE();

            #region CALL FOCUS  

            // DELAY NO INÍCIO
            Thread.Sleep(2000);

            string statusNF = "processando_autorizacao";
            while (statusNF == "processando_autorizacao")
            {
                string Token = "";

                if (OrigemPedido == OrigemPedido.Cliente)
                    Token = Configuracoes.Focus_Token;

                if (OrigemPedido == OrigemPedido.Empresa)
                    Token = Configuracoes.Focus_Token_LC;

                string URL = "";
                if (OrigemPedido == OrigemPedido.Empresa)
                {
                    URL = Configuracoes.Focus_Autorizar + "consultar?token=" + Token + "&ref=" + NrReferenciaNF;
                    //URL = "http://homologacao.acrasnfe.acras.com.br/nfe2/" + "consultar?token=" + Token + "&ref=" + NrReferenciaNF;
                }
                else
                {
                    URL = Configuracoes.Focus_Autorizar + "consultar?token=" + Token + "&ref=" + NrReferenciaNF;
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string RespostaAPI = reader.ReadToEnd();

                #region REGISTRA LOG DE RETORNO FOCUS

                string FileName = "";

                if (PedidoID != null)
                {
                    FileName = PedidoID.ToString();
                }
                else
                {
                    FileName = "SEM_NR_PEDIDO";
                }

                FileName = FileName + "_" + NrReferenciaNF + "_" + DateTime.Now.ToString();
                FileName = FileName.Replace(" ", "_").Replace(":", "_").Replace(".", "_").Replace("/", "_");
                FileName = FileName + ".txt";

                var Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/logs/NFs/" + FileName);

                using (StreamWriter sw = new StreamWriter(Path, true))
                {
                    sw.Write(RespostaAPI);
                }

                #endregion



                YamlStream yaml = new YamlStream();
                yaml.Load(new StringReader(RespostaAPI));

                var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

                if (mapping.Children[new YamlScalarNode("status")].ToString() != "processando_autorizacao")
                {
                    if (tipodeConsulta == NFE.TiposConsulta.cancelamento && mapping.Children[new YamlScalarNode("status")].ToString() == "autorizado")
                    {
                        Thread.Sleep(500);
                    }
                    else
                    {
                        statusNF = mapping.Children[new YamlScalarNode("status")].ToString();

                        if (statusNF == "erro_autorizacao")
                        {
                            NFRetorno.statusNF = NFE.TodosStatusProcessamento.erro_autorizacao;
                            NFRetorno.mensagem_sefaz = mapping.Children[new YamlScalarNode("mensagem_sefaz")].ToString();
                            NFRetorno.status_sefaz = mapping.Children[new YamlScalarNode("status_sefaz")].ToString();
                        }
                        else
                        {
                            if (tipodeConsulta == NFE.TiposConsulta.emissao)
                            {
                                if (statusNF == "autorizado")
                                {
                                    NFRetorno.statusNF = NFE.TodosStatusProcessamento.autorizado;
                                    NFRetorno.caminhoDanfe = mapping.Children[new YamlScalarNode("caminho_danfe")].ToString();
                                    NFRetorno.caminhoXML = mapping.Children[new YamlScalarNode("caminho_xml_nota_fiscal")].ToString();
                                    NFRetorno.chaveNFe = mapping.Children[new YamlScalarNode("chave_nfe")].ToString();
                                    NFRetorno.numeroNFe = mapping.Children[new YamlScalarNode("numero")].ToString();
                                    NFRetorno.status_sefaz = mapping.Children[new YamlScalarNode("status_sefaz")].ToString();
                                    NFRetorno.mensagem_sefaz = mapping.Children[new YamlScalarNode("mensagem_sefaz")].ToString();
                                    NFRetorno.NrReferenciaNF = NrReferenciaNF;
                                }
                            }
                            if (tipodeConsulta == NFE.TiposConsulta.cancelamento)
                            {
                                if (statusNF == "cancelado")
                                {
                                    NFRetorno.statusNF = NFE.TodosStatusProcessamento.cancelado;
                                    NFRetorno.caminhoXMLCancelamento = mapping.Children[new YamlScalarNode("caminho_xml_cancelamento")].ToString();
                                    NFRetorno.chaveNFe = mapping.Children[new YamlScalarNode("chave_nfe")].ToString();
                                    NFRetorno.status_sefaz = mapping.Children[new YamlScalarNode("status_sefaz")].ToString();
                                    NFRetorno.mensagem_sefaz = mapping.Children[new YamlScalarNode("mensagem_sefaz")].ToString();
                                    NFRetorno.NrReferenciaNF = NrReferenciaNF;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // DELAY DE 3 SEG PARA A PROXIMA REQUISICAO
                    Thread.Sleep(3000);
                }
            }

            #endregion

            return NFRetorno;
        }
        public enum TipoChamada
        {
            Emissao = 1,

            Cancelamento = 2
        }
    }
}





