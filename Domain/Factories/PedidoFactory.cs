﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Entities;
using System.Web;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Domain.Factories
{
    internal class PedidoFactory
    {

        private PedidoRepository pedidoRepository;
        private RelatorioRepository relatorioRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<PedidoNota> pedidoNotaRepository;

        public PedidoFactory(ObjectContext context)
        {
            pedidoRepository = new PedidoRepository(context);
            relatorioRepository = new RelatorioRepository(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            pedidoNotaRepository = new PersistentRepository<PedidoNota>(context);

        }
        public Pedido CriarPedidoPagarMe(int cidadeID, int clienteID, int? cupomID, DateTime dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, decimal subtotal, decimal desconto, decimal valorTotal, string observacoes = "", string parentesco = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "", string origemSite = "", string origemForma = "")
        {
            bool Duplicado = false;

            var PedidoOld = pedidoRepository.GetByExpression(p => p.ClienteID == clienteID && p.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.Pago).OrderByDescending(p => p.ID).FirstOrDefault();

            if (PedidoOld != null)
            {
                if (PedidoOld.PessoaHomenageada.ToLower() == pessoaHomenageada.ToLower())
                    Duplicado = true;

                if (PedidoOld.LocalEntrega != null && !Duplicado)
                    if (PedidoOld.LocalEntrega.ToLower() == localEntrega.ToLower())
                        Duplicado = true;

                if (PedidoOld.Observacoes.ToLower() == observacoes.ToLower() && !Duplicado)
                    Duplicado = true;

            }

            if (Duplicado == true)
            {
                return null;
            }

            string GA = "";
            if (HttpContext.Current.Request.Cookies["_ga"] != null)
            {
                GA = HttpContext.Current.Request.Cookies["_ga"].Value;
            }

            var pedido = new Pedido()
            {
                CidadeID = cidadeID,
                ClienteID = clienteID,
                CupomID = cupomID,
                Codigo = Guid.NewGuid(),
                Numero = CriarNumero(),
                TID = "",
                Origem = origem,
                OrigemSite = origemSite,
                OrigemForma = origemForma,
                Retorno = "",
                Mensagem = "",
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                EstadoID = estadoID,
                FormaPagamento = formaPagamento,
                LocalEntrega = localEntrega,
                MeioPagamento = meioPagamento,
                Observacoes = observacoes,
                Parentesco = parentesco,
                PessoaHomenageada = pessoaHomenageada,
                StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento,
                StatusProcessamento = Pedido.TodosStatusProcessamento.Criado,
                StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                TelefoneContato = telefoneContato,
                ValorDesconto = desconto,
                ValorTotal = valorTotal,
                NFeValor = valorTotal,
                QualidadeStatusContatoID = (int)Domain.Entities.Pedido.TodosStatusContato.NI,
                QualidadeStatusContatoNaoID = (int)Domain.Entities.Pedido.TodosStatusContatoNao.NI,
                StatusNfID = 1,
                GA = GA
            };

            #region ALOCAR PEDIDO

            int? ProxAlocadoID = 0;
            var notaAlocar = new PedidoNota();
            notaAlocar.PedidoID = pedido.ID;

            try
            {
                ProxAlocadoID = relatorioRepository.spRetornaProxAlocado();
            }
            catch {}

            if (ProxAlocadoID > 0)
            {
                pedido.AdministradorID = (int)ProxAlocadoID;
                pedidoRepository.Save(pedido);
                
                pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
            }
            else {
                pedido.AdministradorID = 1043;
                pedidoRepository.Save(pedido);
            }

            #endregion

            pedidoRepository.Save(pedido);

            try
            {
                if (pedido.FormaPagamento == Pedido.FormasPagamento.BoletoPagarMe)
                {
                    BoletoPagarMeFactory.Criar(pedido, Remessa.Empresa.CPV, pedido.ValorTotal.ToString());
                }
            }
            catch { }

            return pedido;
        }

        public Pedido CriarPedido(int cidadeID, int clienteID, int? cupomID, DateTime dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, decimal subtotal, decimal desconto, decimal valorTotal, string observacoes = "", string pessoaHomenageada = "", string telefoneContato = "", string origem = "", string origemSite = "", string origemForma = "", string parcela = "", int LocalID = 0)
        {
            bool Duplicado = false;

            var PedidoOld = pedidoRepository.GetByExpression(p => p.ClienteID == clienteID && p.StatusPagamentoID == (int)Pedido.TodosStatusPagamento.Pago).OrderByDescending(p => p.ID).FirstOrDefault();

            if (PedidoOld != null)
            {
                if (PedidoOld.PessoaHomenageada.ToLower() == pessoaHomenageada.ToLower())
                    Duplicado = true;

                if (PedidoOld.LocalEntrega != null && !Duplicado)
                    if (PedidoOld.LocalEntrega.ToLower() == localEntrega.ToLower())
                        Duplicado = true;

                if (PedidoOld.Observacoes.ToLower() == observacoes.ToLower() && !Duplicado)
                    Duplicado = true;

            }

            if (Duplicado == true)
                return null;
            if (string.IsNullOrEmpty(observacoes))
                observacoes = "";
            if (string.IsNullOrEmpty(parcela))
                parcela = "0";

            int? LocalIDFinal = null;
            if(LocalID != 0)
            {
                LocalIDFinal = LocalID;
            }

            var pedido = new Pedido()
            {
                CidadeID = cidadeID,
                ClienteID = clienteID,
                CupomID = cupomID,
                Codigo = Guid.NewGuid(),
                Numero = CriarNumero(),
                TID = "",
                Origem = origem,
                OrigemSite = origemSite,
                OrigemForma = origemForma,
                Retorno = "",
                Mensagem = "",
                DataAtualizacao = DateTime.Now,
                DataCriacao = DateTime.Now,
                DataSolicitada = dataSolicitada,
                EstadoID = estadoID,
                FormaPagamento = formaPagamento,
                LocalEntrega = localEntrega,
                MeioPagamento = meioPagamento,
                Parentesco = "",
                Observacoes = observacoes,
                PessoaHomenageada = pessoaHomenageada,
                StatusPagamento = Pedido.TodosStatusPagamento.AguardandoPagamento,
                StatusProcessamento = Pedido.TodosStatusProcessamento.Criado,
                StatusEntrega = TodosStatusEntrega.PendenteFornecedor,
                TelefoneContato = telefoneContato,
                ValorDesconto = desconto,
                ValorTotal = valorTotal,
                NFeValor = valorTotal,
                QualidadeStatusContatoID = (int)Domain.Entities.Pedido.TodosStatusContato.NI,
                QualidadeStatusContatoNaoID = (int)Domain.Entities.Pedido.TodosStatusContatoNao.NI,
                Parcela = int.Parse(parcela),
                LocalID = LocalIDFinal,
                StatusNfID = 1
            };

            #region ALOCAR PEDIDO

            int? ProxAlocadoID = 0;
            var notaAlocar = new PedidoNota();
            notaAlocar.PedidoID = pedido.ID;

            try
            {
                ProxAlocadoID = relatorioRepository.spRetornaProxAlocado();
            }
            catch { }

            if (ProxAlocadoID > 0)
            {
                pedido.AdministradorID = (int)ProxAlocadoID;
                pedidoRepository.Save(pedido);
                
                pedido.StatusProcessamentoID = (int)Pedido.TodosStatusProcessamento.Processando;
            }
            else
            {
                pedido.AdministradorID = 1043;
                pedidoRepository.Save(pedido);
            }

            #endregion

            pedidoRepository.Save(pedido);

            return pedido;
        }
        
        public string CriarNumero()
        {
            string caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int valormaximo = caracteres.Length;

            Random random = new Random(DateTime.Now.Millisecond);

            do
            {
                StringBuilder numero = new StringBuilder(10);
                for (int indice = 0; indice < 10; indice++)
                    numero.Append(caracteres[random.Next(0, valormaximo)]);

                if (!pedidoRepository.NumeroExiste(numero.ToString()))
                {
                    return numero.ToString();
                }
            } while (true);
        }

    }
}
