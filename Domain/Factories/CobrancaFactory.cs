﻿using Domain.Core;
using Domain.Entities;
using DomainExtensions.Repositories;
using HangFire.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Factories
{
    public class CobrancaFactory
    {
        private PersistentRepository<Log> logRepository;

        private string TemplateID { get; set; }
        private string TemplateContentName { get; set; }
        private string TemplateContentContent { get; set; }
        private string FromEmail { get; set; }
        private string AreaDoCliente { get; set; }

        public string EnviarCobrancaLC(CobrancaLC cobranca, string Template)
        {
            var Context = new COROASEntities();

            var culture = new CultureInfo("pt-BR");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            List<string> LstEmailsEnviar = new List<string>();

            if (Configuracoes.HOMOLOGACAO)
            {
                LstEmailsEnviar.Add(Core.Configuracoes.EMAIL_HOMOLOGACAO);
            }
            else
            {
                LstEmailsEnviar.Add(cobranca.EmailColaborador);

                if (!string.IsNullOrEmpty(cobranca.EmailFinanceiro))
                    LstEmailsEnviar.Add(cobranca.EmailFinanceiro);

                if (!string.IsNullOrEmpty(cobranca.EmailFinanceiroCopia))
                    LstEmailsEnviar.Add(cobranca.EmailFinanceiroCopia);
            }

            foreach (var Email in LstEmailsEnviar)
            {
                List<varsMandrill> Lstvars = new List<varsMandrill>();

                if (Core.Funcoes.ValidaEmail(Email))
                {

                    #region PREENCHE DADOS TEMPLATE

                    #region COBRANÇA BOLETO

                    var MandrillLog = "";
                    if (cobranca.MeioPagamentoID == 5)
                    {
                        if (Template == "1")
                        {
                            TemplateID = "lc-boleto-vence-em-dois-dias";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Seu boleto vence em dois dias";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCBoletoVence2Dias.ToString();
                        }
                        if (Template == "2")
                        {
                            TemplateID = "lc-boleto-d-5";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Seu boleto vence hoje!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCBoletoVenceHoje.ToString();
                        }
                        if (Template == "3")
                        {
                            TemplateID = "lc-boletovencido";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Seu boleto venceu!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCBoletoVenceu1Dia.ToString();
                        }
                        if (Template == "4")
                        {
                            TemplateID = "lc-boletovencido-2";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Seu boleto venceu novamente!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCBoletoSegundoBoletoRenegociado.ToString();
                        }
                        if (Template == "5")
                        {
                            TemplateID = "lc-inadimplencia";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Informe de Inadimplência";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCBoletoInadimplencia.ToString();
                        }
                    }
                    else
                    {
                        if (Template == "1")
                        {
                            TemplateID = "fatura-vence-em-dois-dias";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "A sua fatura vence em dois dias";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCFaturaVence2Dias.ToString();
                        }
                        if (Template == "2")
                        {
                            TemplateID = "lc-fatura-5";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Sua fatura vence hoje!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCFaturaVenceHoje.ToString();
                        }
                        if (Template == "3")
                        {
                            TemplateID = "lc-fatura-vencida";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Sua fatura venceu!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCFaturaVenceu1Dia.ToString();
                        }
                        if (Template == "4")
                        {
                            TemplateID = "lc-fatura-vencida-novamente";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Sua fatura venceu novamente!";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCFaturaSegundoFaturaRenegociado.ToString();
                        }
                        if (Template == "5")
                        {
                            TemplateID = "lc-inadimplencia-fatura";
                            TemplateContentName = "Laços Corporativos";
                            TemplateContentContent = "Informe de Inadimplência";
                            FromEmail = "financeiro@lacoscorporativos.com.br";
                            MandrillLog = LogMandrillEntidade.TipoEmail.RoboLCFaturaInadimplencia.ToString();
                        }

                    }

                    #endregion

                    #endregion

                    DefaultMandrill novoEnvio = new DefaultMandrill();
                    novoEnvio.template_name = TemplateID;
                    novoEnvio.template_content.Add(new TemplateContentMandrill
                    {
                        name = TemplateContentName,
                        content = TemplateContentContent
                    });

                    ToMandrill NovoToMandrill = new ToMandrill();
                    NovoToMandrill.email = Email;
                    NovoToMandrill.name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cobranca.Nome.ToLower());

                    List<ToMandrill> LstToMandril = new List<ToMandrill>();
                    LstToMandril.Add(NovoToMandrill);

                    #region VARIAVEIS

                    var Nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cobranca.Nome.ToLower());
                    var ColaboradorNome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cobranca.ColaboradorNome.ToLower());

                    Lstvars.Add(new varsMandrill { name = "NOME", content = Nome.Split(' ')[0] });
                    Lstvars.Add(new varsMandrill { name = "SOLICITANTE", content = ColaboradorNome.Split(' ')[0] });

                    #region COBRANÇA BOLETO

                    if (cobranca.MeioPagamentoID == 5)
                    {
                        Lstvars.Add(new varsMandrill { name = "DIAVENCIMENTO", content = cobranca.DataVencimentoBoleto.Value.ToString("dd/MM/yyyy") });

                        Lstvars.Add(new varsMandrill { name = "BOLETO", content = "https://www.coroasparavelorio.com.br/boleto/EmpresaCobranca?P=" + cobranca.PedidoEmpresaID + "&F=" + cobranca.FaturaID });

                        if (Domain.Core.Configuracoes.HOMOLOGACAO)
                            Lstvars.Add(new varsMandrill { name = "BOLETO", content = "http://localhost:29436/boleto/EmpresaCobranca?P=" + cobranca.PedidoEmpresaID + "&F=" + cobranca.FaturaID });


                        Lstvars.Add(new varsMandrill { name = "VALOR", content = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", cobranca.Valor) });

                        
                        var LstCobrancaLC = Context.Faturamentoes.Where(r => r.ID == cobranca.FaturaID).First();
                        var LinhaDigitavel = Domain.Service.BoletoService.GerarCodigo(LstCobrancaLC.BoletoAtual);

                        Lstvars.Add(new varsMandrill { name = "VENCIMENTOORIGINAL", content = cobranca.DataVencimentoBoletoOriginal.Value.ToString("dd/MM/yyyy") });
                        Lstvars.Add(new varsMandrill { name = "CODBARRAS", content = LinhaDigitavel });

                    }
                    else
                    {
                        // CARTAO DE CREDITO - INSTRUÇÕES
                        if (cobranca.MeioPagamentoID == 7)
                        {
                            Lstvars.Add(new varsMandrill { name = "TEXTO", content = "Texto explicativo para o pagameto via cartao de credito." });
                        }
                        // DEPOSITO BANCARIO - INSTRUÇÕES
                        if (cobranca.MeioPagamentoID == 8)
                        {
                            Lstvars.Add(new varsMandrill { name = "TEXTO", content = "Texto explicativo para o pagameto via deposito / transferencia bancaria." });
                        }

                        Lstvars.Add(new varsMandrill { name = "DIAVENCIMENTO", content = cobranca.DataVencimentoFatura.ToString("dd/MM/yyyy") });
                        Lstvars.Add(new varsMandrill { name = "VALOR", content = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", cobranca.ValorFatura) });
                        Lstvars.Add(new varsMandrill { name = "NOMEDAEMPRESA", content = cobranca.NomeEmpresa.ToUpper() });
                        Lstvars.Add(new varsMandrill { name = "VENCIMENTOORIGINAL", content = cobranca.DataVencimentoOriginal.Value.ToString("dd/MM/yyyy") });
                    }

                    #endregion

                    Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = cobranca.PedidoEmpresaID.ToString() });
                    Lstvars.Add(new varsMandrill { name = "DIAHOJE", content = DateTime.Now.ToString("dd/MM/yyyy") });
                    Lstvars.Add(new varsMandrill { name = "Produto", content = cobranca.Produto });
                    Lstvars.Add(new varsMandrill { name = "DIASCORRIDOS", content = Core.Configuracoes.BOLETO_DIAS_SEGUNDA_RENEGOCIACAO });

                    Lstvars.Add(new varsMandrill { name = "LINKNFE", content = cobranca.NFeLink });

                    #endregion

                    #region MENSAGEM

                    novoEnvio.message = new MessagemMandrill()
                    {
                        subject = TemplateContentContent,
                        from_email = FromEmail,
                        from_name = "Laços Corporativos",
                        to = LstToMandril,
                        headers = new headersMandrill() { ReplyTo = "financeiro@lacoscorporativos.com.br" },
                        merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = Email,
                            vars = Lstvars
                        }
                    }
                    };

                    #endregion

                    var retorno = new Mandrill().CallMandrill(novoEnvio);

                    #region LOG DE ENVIO

                    try
                    {
                        logRepository = new PersistentRepository<Log>(Context);
                        
                        logRepository.Save(new Log
                        {
                            Pedido = cobranca.PedidoEmpresaID.ToString(),
                            Data = DateTime.Now,
                            Var1 = "MSG - RoboCobrancaLC - Email enviado para: " + Email,
                            Var2 = "Titulo: " + TemplateContentContent
                        });

                    }
                    catch { }

                    #endregion

                    #region SALVAR LOG

                    try
                    {
                        dynamic dynJson = Newtonsoft.Json.JsonConvert.DeserializeObject(retorno);
                        string IdEmail = "";
                        string reject_reason = "";

                        foreach (var item in dynJson)
                        {
                            IdEmail = item._id;
                            reject_reason = item.reject_reason;
                        }

                        if (reject_reason == "null")
                            reject_reason = "";

                        if (!string.IsNullOrEmpty(IdEmail))
                        {
                            new LogMandrillEntidade().SalvaLog(IdEmail, MandrillLog, null, cobranca.PedidoEmpresaID, reject_reason);
                        }
                    }
                    catch { }

                    #endregion

                    return retorno;
                }
                else
                {
                    return "Email Inválido";
                }

            }

            return null;
        }
    }
}
