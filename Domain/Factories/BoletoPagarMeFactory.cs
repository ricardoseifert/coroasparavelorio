﻿using Domain.Core;
using Domain.Entities;
using DomainExtensions.Repositories;
using Newtonsoft.Json;
using PagarMe;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PagarMe
{
    public class TransactionNew : Transaction
    {
        public string Error { get; set; }
    }
}

namespace Domain.Factories
{
    public class BoletoPagarMeFactory
    {
        private PersistentRepository<Log> logRepository;

        public static BoletoPagarMe Criar(Pedido pedido, Domain.Entities.Remessa.Empresa Empresa, string Valor)
        {
            return Criar(pedido.Cliente.Bairro, pedido.Cliente.CEP, (pedido.Cliente.Cidade != null)?pedido.Cliente.Cidade.Nome:pedido.Cidade.Nome, pedido.Cliente.Documento, pedido.Cliente.Endereco, pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica?pedido.Cliente.Nome:pedido.Cliente.RazaoSocial, (pedido.Cliente.Estado !=null)?pedido.Cliente.Estado.Sigla:pedido.Estado.Sigla, pedido.ValorTotal, pedido.ID, null, Empresa, Valor);
        }

        public static BoletoPagarMe Criar(Faturamento faturamento, Domain.Entities.Remessa.Empresa Empresa, string Valor)
        {
            return Criar(faturamento.Empresa.Bairro, faturamento.Empresa.CEP, faturamento.Empresa.Cidade.Nome, faturamento.Empresa.CNPJ, faturamento.Empresa.Endereco, faturamento.Empresa.RazaoSocial, faturamento.Empresa.Estado.Sigla, faturamento.ValorTotal, null, faturamento.ID, Empresa, Valor);
        }

        public string EnviarCobranca(BoletoPagarMe Boleto, string BarCod, string BoletoURL, Guid GuidPedido)
        {
            if (Core.Funcoes.ValidaEmail(Boleto.Pedido.Cliente.Email.Trim()))
            {
                var EmailsEnviarNFE = Boleto.Pedido.Cliente.Email;

                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    EmailsEnviarNFE = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-boleto-d-5";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Lembrete: Seu boleto vence hoje!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = EmailsEnviarNFE;
                NovoToMandrill.name = Boleto.Pedido.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();

                var Nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Boleto.Pedido.Cliente.Nome.ToLower());

                Lstvars.Add(new varsMandrill { name = "NOME", content = Nome.Split(' ')[0] });
                Lstvars.Add(new varsMandrill { name = "BOLETO", content = "http://www.coroasparavelorio.com.br/boleto?codigo=" + GuidPedido });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = Boleto.Pedido.Numero });
                Lstvars.Add(new varsMandrill { name = "DIAHOJE", content = DateTime.Now.ToString("dd/MM/yyyy") });
                Lstvars.Add(new varsMandrill { name = "CODIGOBOLETO", content = Boleto.BarCode });
                Lstvars.Add(new varsMandrill { name = "VALOR", content = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Boleto.Valor) });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = "#" + Boleto.Pedido.Numero });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Lembrete: Seu boleto vence hoje!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviarNFE,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region LOG DE ENVIO

                try
                {
                    var Context = new COROASEntities();

                    logRepository = new PersistentRepository<Log>(Context);

                    logRepository.Save(new Log
                    {
                        Pedido = Boleto.Pedido.Numero.ToString(),
                        Data = DateTime.Now,
                        Var1 = "MSG - RoboCobrancaCPV_VenceHoje - Email enviado para: " + Boleto.Pedido.Cliente.Email.Trim(),
                        Var2 = "Titulo: " + "Lembrete: Seu boleto vence hoje!"
                    });

                }
                catch { }

                #endregion

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {

                        var Context = new COROASEntities();
                        new LogMandrillEntidade().SalvaLog(IdEmail, LogMandrillEntidade.TipoEmail.CobrancaRoboPrimeiraVez.ToString(), Boleto.Pedido.ID, null, reject_reason);
                    }
                }
                catch { }

                

                #endregion

                return retorno;
            }
            else
            {
                return "Email Inválido";
            }
        }

        public string EnviarRenegociacao(BoletoPagarMe Boleto, Guid GuidPedido)
        {
           

            if (Core.Funcoes.ValidaEmail(Boleto.Pedido.Cliente.Email.Trim()))
            {
                var EmailsEnviarNFE = Boleto.Pedido.Cliente.Email;

                if (Core.Configuracoes.HOMOLOGACAO)
                {
                    EmailsEnviarNFE = Core.Configuracoes.EMAIL_HOMOLOGACAO;
                }

                DefaultMandrill novoEnvio = new DefaultMandrill();
                novoEnvio.template_name = "new-boleto-vencido";
                novoEnvio.template_content.Add(new TemplateContentMandrill
                {
                    name = "Coroas Para Velório",
                    content = "Seu boleto encontra-se vencido!"
                });

                ToMandrill NovoToMandrill = new ToMandrill();
                NovoToMandrill.email = EmailsEnviarNFE;
                NovoToMandrill.name = Boleto.Pedido.Cliente.Nome;

                List<ToMandrill> LstToMandril = new List<ToMandrill>();
                LstToMandril.Add(NovoToMandrill);

                List<varsMandrill> Lstvars = new List<varsMandrill>();

                var Nome = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Boleto.Pedido.Cliente.Nome.ToLower());

                Lstvars.Add(new varsMandrill { name = "NOME", content = Nome.Split(' ')[0] });
                Lstvars.Add(new varsMandrill { name = "BOLETO", content = "http://www.coroasparavelorio.com.br/boleto?codigo=" + GuidPedido });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = Boleto.Pedido.Numero });
                Lstvars.Add(new varsMandrill { name = "DIAHOJE", content = DateTime.Now.ToString("dd/MM/yyyy") });
                Lstvars.Add(new varsMandrill { name = "CODIGOBOLETO", content = Boleto.BarCode });
                Lstvars.Add(new varsMandrill { name = "DIAVENCIMENTO", content = Boleto.DataVencimento.Value.ToString("dd/MM/yyyy") });
                Lstvars.Add(new varsMandrill { name = "VALOR", content = string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C}", Boleto.Valor) });
                Lstvars.Add(new varsMandrill { name = "PEDIDOID", content = "#" + Boleto.Pedido.Numero });

                novoEnvio.message = new MessagemMandrill()
                {
                    subject = "Seu boleto encontra-se vencido!",
                    from_email = "noreply@coroasparavelorio.com.br",
                    from_name = "COROAS PARA VELÓRIO",
                    to = LstToMandril,
                    headers = new headersMandrill() { ReplyTo = "financeiro@coroasparavelorio.com.br" },
                    merge_vars = new List<merge_varsMandrill>() {
                        new merge_varsMandrill()
                        {
                            rcpt = EmailsEnviarNFE,
                            vars = Lstvars
                        }
                    }
                };

                var retorno = new Mandrill().CallMandrill(novoEnvio);

                #region LOG DE ENVIO

                try
                {
                    var Context = new COROASEntities();

                    logRepository = new PersistentRepository<Log>(Context);

                    logRepository.Save(new Log
                    {
                        Pedido = Boleto.Pedido.Numero.ToString(),
                        Data = DateTime.Now,
                        Var1 = "MSG - RoboCobrancaCPV_Renegociar - Email enviado para: " + Boleto.Pedido.Cliente.Email.Trim(),
                        Var2 = "Titulo: " + "Seu boleto encontra-se vencido!"
                    });

                }
                catch { }

                #endregion

                #region SALVAR LOG

                try
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(retorno);
                    string IdEmail = "";
                    string reject_reason = "";

                    foreach (var item in dynJson)
                    {
                        IdEmail = item._id;
                        reject_reason = item.reject_reason;
                    }

                    if (reject_reason == "null")
                        reject_reason = "";

                    if (!string.IsNullOrEmpty(IdEmail))
                    {

                        var Context = new COROASEntities();
                        new LogMandrillEntidade().SalvaLog(IdEmail, LogMandrillEntidade.TipoEmail.RenegociacaoRobro.ToString(), Boleto.Pedido.ID, null, reject_reason);
                    }
                }
                catch { }

                

                #endregion

                return retorno;
            }
            else
            {
                return "Email Inválido";
            }
        }

        public static BoletoPagarMe Criar(string bairro, string cep, string cidade, string documento, string endereco, string nome, string estado, decimal valor, int pedidoID, string Valor)
        {
            var Context = new COROASEntities();
            Pedido Pedido = null;

            Pedido = Context.Pedidoes.Where(r => r.ID == pedidoID).First();

            // CRIA UM NOVO BOLETO
            var boleto = new BoletoPagarMe();
            boleto.PedidoID = pedidoID;
            boleto.DataCriacao = DateTime.Now;
            boleto.StatusPagamentoID = 1;
            boleto.StatusProcessamentoID = 1;

            var NomeCliente = "";
            if (Pedido.Cliente.TipoID == 1)
            {
                NomeCliente = Pedido.Cliente.Nome;
            }
            else if (Pedido.Cliente.TipoID == 2)
            {
                NomeCliente = Pedido.Cliente.RazaoSocial;
            }

            boleto.SacadoNome = NomeCliente;

            var EnderecoCompleto = Pedido.Cliente.Logradouro + ", " + Pedido.Cliente.Numero + " " + Pedido.Cliente.Complemento;
            if (Pedido.Cidade != null)
            {
                EnderecoCompleto += " " + Pedido.Cliente.Cidade.Nome + " / " + Pedido.Cliente.Estado.Sigla;
            }

            boleto.SacadoEndereco = EnderecoCompleto;
            boleto.SacadoBairro = Pedido.Cliente.Bairro;
            boleto.SacadoCidade = Pedido.Cliente.Cidade.Nome;
            boleto.SacadoCEP = Pedido.Cliente.CEP;
            boleto.SacadoUF = Pedido.Cliente.Estado.Sigla;
            boleto.SacadoDocumento = Pedido.Cliente.Documento;
            boleto.Instrucao = "N/A";

            if (Valor != null)
                valor = decimal.Parse(Valor);

            boleto.Valor = valor;

            // SALVA NOVO BOLETO
            Pedido.BoletoPagarMes.Add(boleto);

            #region CALL PAGARME 

            var Retorno = TrasacaoPagarMe(Valor, Pedido.Codigo, Pedido.Cliente.Email, Pedido.Cliente.Documento, NomeCliente, Pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", ""), Pedido.Cliente.TelefoneContato.Remove(0, 5), Pedido.Cliente.Cidade.Nome, Pedido.Estado.Sigla, Pedido.Cliente.Logradouro, Pedido.Cliente.Numero, Pedido.Cliente.Bairro, Pedido.Cliente.CEP, new Core.Funcoes().DataVencimentoUtil(), Pedido);

            if (Retorno == null)
            {
                return null;
            }
            else
            {
                boleto.BoletoURL = Retorno.BoletoUrl;
                boleto.BarCode = Retorno.BoletoBarcode;
                boleto.TransactionID = Retorno.Id;
                boleto.DataVencimento = Retorno.BoletoExpirationDate;

                // SALVA NOVO BOLETO
                Context.SaveChanges();

                Pedido.PedidoNotas.Add(new PedidoNota()
                {
                    Pedido = Pedido,
                    DataCriacao = DateTime.Now,
                    Observacoes = "Boleto de ID: " + boleto.ID + " / " + boleto.TransactionID + " com a data de vencimento prevista para " + boleto.DataVencimento + ", foi criado automaticamente. Motivo: Robo de Negociação."
                });

                // SALVA NOVA NOTA
                Context.SaveChanges();

                return boleto;
            }

            #endregion

        }

        public static void CancelarBoletos(Pedido pedidoParam)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);
            var Pedido = pedidoRepository.Get(pedidoParam.ID);

            // CANCELA BOLETOS EXISTENTES
            foreach (var Boleto in Pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).ToList())
            {
                Boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Cancelado;
                Boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;
                boletoRepository.Save(Boleto);
            }
        }

        public static void CancelarBoletosInclusivePagos(Pedido pedidoParam)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);
            var Pedido = pedidoRepository.Get(pedidoParam.ID);

            // CANCELA BOLETOS EXISTENTES
            foreach (var Boleto in Pedido.BoletoPagarMes.ToList())
            {
                Boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Cancelado;
                Boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;
                boletoRepository.Save(Boleto);
            }
        }

        public static BoletoPagarMe Alterar(BoletoPagarMe boleto, Domain.Entities.Remessa.Empresa Empresa)
        {
            var BoletoRetorno = new BoletoPagarMe();

            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);
            var datavencimento = (DateTime)boleto.DataVencimento;
            var Pedido = pedidoRepository.Get(boleto.PedidoID.Value);

            // CANCELA BOLETOS EXISTENTES
            CancelarBoletos(Pedido);

            // CRIA UM NOVO BOLETO
            BoletoRetorno.PedidoID = boleto.PedidoID;            
            BoletoRetorno.Valor = boleto.Valor;
            BoletoRetorno.DataCriacao = DateTime.Now;
            BoletoRetorno.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento;
            BoletoRetorno.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Criado;
            BoletoRetorno.SacadoDocumento = boleto.SacadoDocumento;
            BoletoRetorno.SacadoBairro = boleto.SacadoBairro;
            BoletoRetorno.SacadoCEP = boleto.SacadoCEP;
            BoletoRetorno.SacadoCidade = boleto.SacadoCidade;
            BoletoRetorno.SacadoEndereco = boleto.SacadoEndereco;
            BoletoRetorno.SacadoNome = boleto.SacadoNome;
            BoletoRetorno.SacadoUF = boleto.SacadoUF;
            BoletoRetorno.Instrucao = "N/A";

            // SAVAL BOLETO CRIADO
            boletoRepository.Save(BoletoRetorno);

            #region CALL PAGARME 

            var Retorno = TrasacaoPagarMe(BoletoRetorno.Valor.ToString(), Pedido.Codigo, Pedido.Cliente.Email, BoletoRetorno.SacadoDocumento, BoletoRetorno.SacadoNome, Pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", ""), Pedido.Cliente.TelefoneContato.Remove(0, 5), BoletoRetorno.SacadoCidade, BoletoRetorno.SacadoUF, BoletoRetorno.SacadoEndereco, Pedido.Cliente.Numero, BoletoRetorno.SacadoBairro, BoletoRetorno.SacadoCEP, datavencimento, Pedido);

            // ATUALIZA BOLETO PAGARME
            BoletoRetorno.BoletoURL = Retorno.BoletoUrl;
            BoletoRetorno.BarCode = Retorno.BoletoBarcode;
            BoletoRetorno.TransactionID = Retorno.Id;
            BoletoRetorno.DataVencimento = Retorno.BoletoExpirationDate;

            boletoRepository.Save(BoletoRetorno);

            #endregion

            return BoletoRetorno;
        }

        private static BoletoPagarMe Criar(string bairro, string cep, string cidade, string documento, string endereco, string nome, string estado, decimal valor, int? pedidoID, int? faturamentoID, Domain.Entities.Remessa.Empresa Empresa, string Valor)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);
            var datavencimento = new Core.Funcoes().DataVencimentoUtil();

            var tipoID = Boleto.TodosTipos.PJ;
            Pedido Pedido = null;

            if (pedidoID.HasValue)
            {
                Pedido = pedidoRepository.Get(pedidoID.Value);
                tipoID = (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica) ? Boleto.TodosTipos.PF : Boleto.TodosTipos.PJ;
            }

            // CANCELA BOLETOS EXISTENTES
            foreach (var Boleto in Pedido.BoletoPagarMes.Where(r => r.StatusPagamento == BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento).ToList())
            {
                Boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.Cancelado;
                Boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Processado;
                boletoRepository.Save(Boleto);
            }

            // CRIA UM NOVO BOLETO
            var boleto = new BoletoPagarMe();
            boleto.PedidoID = pedidoID;
            boleto.FaturamentoID = faturamentoID;
            boleto.DataCriacao = DateTime.Now;
            boleto.StatusPagamento = BoletoPagarMe.TodosStatusPagamento.AguardandoPagamento;
            boleto.StatusProcessamento = BoletoPagarMe.TodosStatusProcessamento.Criado;

            var NomeCliente = "";
            if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica)
            {
                NomeCliente = Pedido.Cliente.Nome;
            }
            else if (Pedido.Cliente.Tipo == Cliente.Tipos.PessoaJuridica)
            {
                NomeCliente = Pedido.Cliente.RazaoSocial;
            }

            boleto.SacadoNome = NomeCliente;
            boleto.SacadoEndereco = Pedido.Cliente.EnderecoCompleto;
            boleto.SacadoBairro = Pedido.Cliente.Bairro;
            boleto.SacadoCidade = Pedido.Cliente.Cidade.Nome;
            boleto.SacadoCEP = Pedido.Cliente.CEP;
            boleto.SacadoUF = Pedido.Cliente.Estado.Sigla;
            boleto.SacadoDocumento = Pedido.Cliente.Documento;
            boleto.Instrucao = "N/A";

            if (Valor != null)
                valor = decimal.Parse(Valor);

            boleto.Valor = valor;

            // SALVA NOVO BOLETO
            boletoRepository.Save(boleto);

            #region CALL PAGARME 

            var Retorno = TrasacaoPagarMe(Valor, Pedido.Codigo, Pedido.Cliente.Email, Pedido.Cliente.Documento, NomeCliente, Pedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", ""), Pedido.Cliente.TelefoneContato.Remove(0, 5), Pedido.Cliente.Cidade.Nome, Pedido.Estado.Sigla, Pedido.Cliente.Logradouro, Pedido.Cliente.Numero, Pedido.Cliente.Bairro, Pedido.Cliente.CEP, new Core.Funcoes().DataVencimentoUtil(), Pedido);

            if(Retorno.Error != null)
            {
                if (Domain.Core.Configuracoes.HOMOLOGACAO)
                {
                    boleto.BoletoURL = "https://www.coroasparavelorio.com.br/";
                    boleto.BarCode = "00000000000000000000000000000";
                    boleto.TransactionID = "12321312";
                    boleto.DataVencimento = DateTime.Now.AddDays(5);

                    // SALVA NOVO BOLETO
                    boletoRepository.Save(boleto);

                    return new BoletoPagarMe { Erro = "Erro na comunicacao com o pagarme: " + Retorno.Error + ". Por ser homologacao, o boleto foi preenchido com dados ficticios." };
                }
                else
                {
                    return new BoletoPagarMe { Erro = Retorno.Error };

                }

               
            }
            else {
                boleto.BoletoURL = Retorno.BoletoUrl;
                boleto.BarCode = Retorno.BoletoBarcode;
                boleto.TransactionID = Retorno.Id;
                boleto.DataVencimento = Retorno.BoletoExpirationDate;
                
                // SALVA NOVO BOLETO
                boletoRepository.Save(boleto);

                return boleto;
            }

            #endregion

        }

        public static TransactionNew TrasacaoPagarMe(string Valor, Guid GuidPedido, string EmailCliente, string DocCliente, string NomeCliente, string DDD, string Telefone, string CidadeCliente, string EstadoCliente, string LogradouroCliente, string NumeroCliente, string BairroCliente, string CEPCliente, DateTime DataDeVencimento, Pedido Pedido)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<BoletoPagarMe>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);

            PagarMeService.DefaultApiKey = Domain.Core.Configuracoes.PagarMe_DefaultApiKey;

            TransactionNew transaction = new TransactionNew();

            if (!Valor.Contains(",") && !Valor.Contains("."))
            {
                Valor = Valor + "," + "00";
            }

            if (Valor.Contains(","))
            {
                var Result = Valor.Split(',')[1];
                if(Result.Length == 4)
                {
                    Valor = Valor.Split(',')[0] + "," + Result.Remove(2, 2);
                }
            }

            transaction.Amount = int.Parse(Valor.ToString().Replace(".", "").Replace(",", ""));
            transaction.PaymentMethod = PaymentMethod.Boleto;
            transaction.BoletoExpirationDate = DataDeVencimento;
            transaction.PostbackUrl = "https://www.coroasparavelorio.com.br/boleto/RetornoPagarme?guid=" + GuidPedido;

            transaction["boleto_instructions"] = "Em caso de não pagamento do boleto, o cliente está propenso a negativação via Serasa. Pedido: " + Pedido.Numero + " #ID: " + Pedido.BoletoPagarMeReturn.ID + ". Sacador/Avalista: ESG COMÉRCIO ELETRÔNICO LTDA CNPJ 12.404.942/0001-14";

            transaction.Customer = new Customer
            {
                //Email = EmailCliente,
                DocumentNumber = DocCliente,
                Name = NomeCliente,
                //Phone = new Phone
                //{
                //    Ddd = DDD,
                //    Number = Telefone,
                //},
                Address = new Address
                {
                    City = string.IsNullOrEmpty(CidadeCliente) ? "N/A" : CidadeCliente,
                    State = string.IsNullOrEmpty(EstadoCliente) ? "N/A" : EstadoCliente,
                    Street =  string.IsNullOrEmpty(LogradouroCliente) ? "N/A" : LogradouroCliente,
                    StreetNumber = string.IsNullOrEmpty(NumeroCliente) ? "0" : NumeroCliente,
                    Neighborhood = string.IsNullOrEmpty(BairroCliente) ? "N/A" : BairroCliente,
                    Country = "BR",
                    Zipcode = CEPCliente
                }
            };

            try
            {
                transaction.Save();
                return transaction;
            }
            catch (PagarMe.PagarMeException ex)
            {
                var Erro = ex.Error;
                var ErrorStr = "";
                foreach (var Error in Erro.Errors)
                {
                    ErrorStr += Error.Message + " ";
                }

                transaction.Error = ErrorStr;

                if (Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO.Length > 0)
                    Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Não Responder", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO, "Coroas para Velório", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO, "Coroas para Velório", "", "[COROAS PARA VELORIO] - ERRO ENCONTRADO NO SITE", ErrorStr, true);

                return transaction;
            }
            catch(Exception ex)
            {
                transaction.Error = ex.Message;

                return transaction;
            }
        }

    }
}
