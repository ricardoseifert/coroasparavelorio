﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using System.IO;
using System.Web;

namespace Domain.Factories
{
    public class BoletoFactory
    {

        public static Boleto Criar(Pedido pedido, Domain.Entities.Remessa.Empresa Empresa)
        {
            return Criar(pedido.Cliente.Bairro, pedido.Cliente.CEP, (pedido.Cliente.Cidade != null)?pedido.Cliente.Cidade.Nome:pedido.Cidade.Nome, pedido.Cliente.Documento, pedido.Cliente.Endereco, pedido.Cliente.Tipo == Cliente.Tipos.PessoaFisica?pedido.Cliente.Nome:pedido.Cliente.RazaoSocial, (pedido.Cliente.Estado !=null)?pedido.Cliente.Estado.Sigla:pedido.Estado.Sigla, pedido.ValorTotal, pedido.ID, null, Empresa);
        }

        public static Boleto Criar(Faturamento faturamento, Domain.Entities.Remessa.Empresa Empresa)
        {
            return Criar(faturamento.Empresa.Bairro, faturamento.Empresa.CEP, faturamento.Empresa.Cidade.Nome, faturamento.Empresa.CNPJ, faturamento.Empresa.Endereco, faturamento.Empresa.RazaoSocial, faturamento.Empresa.Estado.Sigla, faturamento.ValorTotal, null, faturamento.ID, Empresa);
        }

        private static Boleto Criar(string bairro, string cep, string cidade, string documento, string endereco, string nome, string estado, decimal valor, int? pedidoID, int? faturamentoID, Domain.Entities.Remessa.Empresa Empresa)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<Boleto>(context);
            var pedidoRepository = new PersistentRepository<Pedido>(context);
            var faturamentoRepository = new PersistentRepository<Faturamento>(context);
            var datavencimento = new Core.Funcoes().DataVencimentoUtil();

            var random = new Random();
            var nossoNumero = "";
            do
            {
                nossoNumero = random.Next(99999999).ToString("00000000");
            }
            while (boletoRepository.GetByExpression(b => b.NossoNumero == nossoNumero).Any());

            var numeroDocumento = "0";
            if (faturamentoID.HasValue)
            {
                // 24/02/17 - Rodrigo - devido a problemas no cnab 
                //Mudanca da forma de gerar , devido aos id de pedido terem passdo de 5 caracteres 
                //Informação ITAU - "No. do Documento" informado pelo Beneficiário é composto por: 10 dígitos
                // numeroDocumento = "1" + DateTime.Now.Year + faturamentoID.Value.ToString("00000");

                numeroDocumento = "10" + faturamentoID.Value.ToString("00000");

                var faturamento = faturamentoRepository.Get(faturamentoID.Value);

                if (faturamento != null)
                {
                    datavencimento = new Domain.Core.Funcoes().DataVencimentoUtil(faturamento.Empresa.DiasParaVencimentoBoleto); 
                }
            }
            else if (pedidoID.HasValue)
            {
                // 24/02/17 - Rodrigo - devido a problemas no cnab 
                //Mudanca da forma de gerar , devido aos id de pedido terem passdo de 5 caracteres 
                //Informação ITAU - "No. do Documento" informado pelo Beneficiário é composto por: 10 dígitos
                //var numeroDocumento = "2" + DateTime.Now.Year + pedidoID.Value.ToString("00000");

                numeroDocumento = "20" + pedidoID.Value.ToString("00000");
            }
            else
            {
                throw new Exception("É necessário associar um boleto a um pedido ou faturamento");
            }

            var tipoID = Boleto.TodosTipos.PJ;

            if (pedidoID.HasValue)
                tipoID = (pedidoRepository.Get(pedidoID.Value).Cliente.Tipo == Cliente.Tipos.PessoaFisica) ? Boleto.TodosTipos.PF : Boleto.TodosTipos.PJ;

            var boleto = new Boleto();
            boleto.PedidoID = pedidoID;
            boleto.FaturamentoID = faturamentoID;
            boleto.TipoID = (int)tipoID;

            string BOLETO_CARTEIRA = string.Empty;
            string BOLETO_AGENCIA = string.Empty;
            string BOLETO_CNPJ = string.Empty;
            string BOLETO_CONTA_CORRENTE = string.Empty;
            string BOLETO_RAZAO_SOCIAL = string.Empty;
            string BOLETO_INSTRUCAO = string.Empty;
            int BOLETO_CODIGO_CEDENTE = 0;

            if (Empresa == Entities.Remessa.Empresa.CPV)
            {
                BOLETO_CARTEIRA = Core.Configuracoes.BOLETO_CARTEIRA;
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA;
                BOLETO_CNPJ = Core.Configuracoes.BOLETO_CNPJ;
                BOLETO_CODIGO_CEDENTE = Core.Configuracoes.BOLETO_CODIGO_CEDENTE;
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE;
                BOLETO_RAZAO_SOCIAL = Core.Configuracoes.BOLETO_RAZAO_SOCIAL;
                BOLETO_INSTRUCAO = Core.Configuracoes.BOLETO_INSTRUCAO;
            }

            if (Empresa == Entities.Remessa.Empresa.LC)
            {
                BOLETO_CARTEIRA = Core.Configuracoes.BOLETO_CARTEIRA_LC;
                BOLETO_AGENCIA = Core.Configuracoes.BOLETO_AGENCIA_LC;
                BOLETO_CNPJ = Core.Configuracoes.BOLETO_CNPJ_LC;
                BOLETO_CODIGO_CEDENTE = Core.Configuracoes.BOLETO_CODIGO_CEDENTE_LC;
                BOLETO_CONTA_CORRENTE = Core.Configuracoes.BOLETO_CONTA_CORRENTE_LC;
                BOLETO_RAZAO_SOCIAL = Core.Configuracoes.BOLETO_RAZAO_SOCIAL_LC;
                BOLETO_INSTRUCAO = Core.Configuracoes.BOLETO_INSTRUCAO_LC;
            }

            boleto.Carteira = BOLETO_CARTEIRA;
            boleto.CedenteAgencia = BOLETO_AGENCIA;
            boleto.CedenteCNPJ = BOLETO_CNPJ;
            boleto.CedenteCodigo = BOLETO_CODIGO_CEDENTE;
            boleto.CedenteContaCorrente = BOLETO_CONTA_CORRENTE;
            boleto.CedenteRazaoSocial = BOLETO_RAZAO_SOCIAL;
            boleto.DataCriacao = DateTime.Now;
            boleto.DataVencimento = datavencimento;
            boleto.Instrucao = BOLETO_INSTRUCAO;
            boleto.NossoNumero = nossoNumero;
            boleto.NumeroDocumento = numeroDocumento;
            boleto.SacadoDocumento = documento;
            boleto.SacadoNome = nome;
            if (String.IsNullOrEmpty(bairro))
                boleto.SacadoBairro = "";
            else
                boleto.SacadoBairro = bairro;

            if (String.IsNullOrEmpty(cep))
                boleto.SacadoCEP = "";
            else
                boleto.SacadoCEP = cep;

            if (String.IsNullOrEmpty(cidade))
                boleto.SacadoCidade = "";
            else
                boleto.SacadoCidade = cidade;

            if (String.IsNullOrEmpty(endereco))
                boleto.SacadoEndereco = "";
            else
                boleto.SacadoEndereco = endereco;

            if (String.IsNullOrEmpty(estado))
                boleto.SacadoUF = "";
            else
                boleto.SacadoUF = estado;

            boleto.Comando = Boleto.Comandos.CriarRemessa;
            boleto.Processado = false;
            boleto.StatusPagamento = Boleto.TodosStatusPagamento.AguardandoPagamento;
            boleto.StatusProcessamento = Boleto.TodosStatusProcessamento.Criado;
            boleto.Valor = valor;

            boletoRepository.Save(boleto);

            return boleto;
        }

        public static int CriarRemessa(Remessa.Empresa EmpresaRemessa)
        {
            var context = new COROASEntities();
            var boletoRepository = new PersistentRepository<Boleto>(context);
            var remessaRepository = new PersistentRepository<Remessa>(context);
            var remessaBoletoRepository = new PersistentRepository<RemessaBoleto>(context);

            List<Boleto> LstBoletos = new List<Boleto>();

            if(EmpresaRemessa == Remessa.Empresa.CPV)
            {
                LstBoletos = boletoRepository.GetByExpression(b => !b.Processado && b.CedenteCodigo == Domain.Core.Configuracoes.BOLETO_CODIGO_CEDENTE).ToList();
            }
            if (EmpresaRemessa == Remessa.Empresa.LC)
            {
                LstBoletos = boletoRepository.GetByExpression(b => !b.Processado && b.CedenteCodigo == Domain.Core.Configuracoes.BOLETO_CODIGO_CEDENTE_LC).ToList();
            }

            if (LstBoletos.Any())
            {
                var remessa = new Remessa()
                {
                    Arquivo = "",
                    Data = DateTime.Now,
                    IdEmpresa = (int)EmpresaRemessa
                };
                remessaRepository.Save(remessa);

                foreach (var boleto in LstBoletos)
                {
                    boleto.Processado = true;
                    boletoRepository.Save(boleto);

                    var remessaBoleto = new RemessaBoleto()
                    {
                        BoletoID = boleto.ID,
                        ComandoID = boleto.ComandoID,
                        RemessaID = remessa.ID
                    };
                    remessaBoletoRepository.Save(remessaBoleto);
                }

                remessa.Arquivo = remessa.ID + ".txt";
                remessaRepository.Save(remessa);

                var conteudo = BoletoService.GerarRemessa(remessa, EmpresaRemessa);
                var arquivo = HttpContext.Current.Server.MapPath("/Content/Remessas/" + remessa.Arquivo);
                File.AppendAllText(arquivo, conteudo);


                return remessa.RemessaBoletoes.Count;
            }

            return 0;
        }

    }
}
