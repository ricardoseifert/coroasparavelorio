﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities;

namespace Domain.MetodosExtensao {
    //=================================================================================
    public static class MetodosExtensao {

        #region Extensions
        //-----------------------------------------------------------------------------
        public static IEnumerable<Pedido> OnlyPf(this IEnumerable<Pedido> pedidos, List<Cliente> clientes) {
            if (pedidos == null){
                throw new Exception("Pedidos está nulo");
            }
            if (pedidos.Any(p=>p.Cliente == null) && (clientes == null || pedidos.Any(p=> !clientes.Select(c=>c.ID).Contains(p.ClienteID)))){
                throw new Exception("Pedido sem cliente vinculado ao objeto");
            }

            return pedidos.Where(p=> clientes.FirstOrDefault(c=>c.ID == p.ClienteID).TipoID == (int)Cliente.Tipos.PessoaFisica);
        }
        //-----------------------------------------------------------------------------
        public static IEnumerable<Pedido> OnlyPj(this IEnumerable<Pedido> pedidos, List<Cliente> clientes) {
            if (pedidos == null) {
                throw new Exception("Pedidos está nulo");
            }
            if (pedidos.Any(p => p.Cliente == null) && (clientes == null || pedidos.Any(p => !clientes.Select(c => c.ID).Contains(p.ClienteID)))) {
                throw new Exception("Pedido sem cliente vinculado ao objeto");
            }
            return pedidos.Where(p => clientes.FirstOrDefault(c => c.ID == p.ClienteID).TipoID == (int)Cliente.Tipos.PessoaJuridica);
        }
        //-----------------------------------------------------------------------------
        #endregion

    }
    //=================================================================================
    public static class PredicateBuilder {

        #region Metodos
        //-----------------------------------------------------------------------------
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b) {

            ParameterExpression p = a.Parameters[0];

            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[b.Parameters[0]] = p;

            Expression body = Expression.AndAlso(a.Body, visitor.Visit(b.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }
        //-----------------------------------------------------------------------------
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b) {

            ParameterExpression p = a.Parameters[0];

            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[b.Parameters[0]] = p;

            Expression body = Expression.OrElse(a.Body, visitor.Visit(b.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }
        //-----------------------------------------------------------------------------
        #endregion

    }
    //=================================================================================
    internal class SubstExpressionVisitor : ExpressionVisitor {
        
        #region Variaveis
        //-----------------------------------------------------------------------------
        public Dictionary<Expression, Expression> subst = new Dictionary<Expression, Expression>();
        //-----------------------------------------------------------------------------
        #endregion

        #region Metodos
        //-----------------------------------------------------------------------------
        protected override Expression VisitParameter(ParameterExpression node) {
            Expression newValue;
            if (subst.TryGetValue(node, out newValue)) {
                return newValue;
            }
            return node;
        }
        //-----------------------------------------------------------------------------
        #endregion
        
    }
    //=================================================================================
}
