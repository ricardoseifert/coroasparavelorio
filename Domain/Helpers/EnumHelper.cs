﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Helpers
{
    public static class EnumHelper
    {
        public static string GetDescription(Enum _enum)
        {
            if (_enum == null) {
                return "";
            }
            Type type = _enum.GetType();
            MemberInfo[] info = type.GetMember(_enum.ToString());

            if (info != null && info.Length > 0)
            {
                object[] attributes = info[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            return _enum.ToString();
        }

        public static string GetDescriptionByName(Type type, string name)
        {
            MemberInfo[] info = type.GetMember(name);

            if (info != null && info.Length > 0)
            {
                object[] attributes = info[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            return name;
        }
    }

    public struct Autocomplete
    {
        string _label;
        public String label
        {
            get { return _label; }
            set { _label = value; }
        }
        string _category;
        public String category
        {
            get { return _category; }
            set { _category = value; }
        }
        string _url;
        public String url
        {
            get { return _url; }
            set { _url = value; }
        }
    }
}