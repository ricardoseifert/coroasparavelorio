/*!
 * Thumbnail helper for fancyBox
 * version: 1.0.7 (Mon, 01 Oct 2012)
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             thumbs: {
 *                 width  : 50,
 *                 height : 50
 *             }
 *         }
 *     });
 *
 */
(function(n){var t=n.fancybox;t.helpers.thumbs={defaults:{width:50,height:50,position:"bottom",source:function(t){var i;return t.element&&(i=n(t.element).find("img").attr("src")),!i&&t.type==="image"&&t.href&&(i=t.href),i}},wrap:null,list:null,width:0,init:function(t,i){for(var o=this,r=t.width,u=t.height,s=t.source,e="",f=0;f<i.group.length;f++)e+='<li><a style="width:'+r+"px;height:"+u+'px;" href="javascript:jQuery.fancybox.jumpto('+f+');"><\/a><\/li>';this.wrap=n('<div id="fancybox-thumbs"><\/div>').addClass(t.position).appendTo("body"),this.list=n("<ul>"+e+"<\/ul>").appendTo(this.wrap),n.each(i.group,function(t){var f=s(i.group[t]);f&&n("<img />").load(function(){var i=this.width,f=this.height,e,s,h;o.list&&i&&f&&(e=i/r,s=f/u,h=o.list.children().eq(t).find("a"),e>=1&&s>=1&&(e>s?(i=Math.floor(i/s),f=u):(i=r,f=Math.floor(f/e))),n(this).css({width:i,height:f,top:Math.floor(u/2-f/2),left:Math.floor(r/2-i/2)}),h.width(r).height(u),n(this).hide().appendTo(h).fadeIn(300))}).attr("src",f)}),this.width=this.list.children().eq(0).outerWidth(!0),this.list.width(this.width*(i.group.length+1)).css("left",Math.floor(n(window).width()*.5-(i.index*this.width+this.width*.5)))},beforeLoad:function(n,t){if(t.group.length<2){t.helpers.thumbs=!1;return}t.margin[n.position==="top"?0:2]+=n.height+15},afterShow:function(n,t){if(this.list)this.onUpdate(n,t);else this.init(n,t);this.list.children().removeClass("active").eq(t.index).addClass("active")},onUpdate:function(t,i){this.list&&this.list.stop(!0).animate({left:Math.floor(n(window).width()*.5-(i.index*this.width+this.width*.5))},150)},beforeClose:function(){this.wrap&&this.wrap.remove(),this.wrap=null,this.list=null,this.width=0}}})(jQuery);