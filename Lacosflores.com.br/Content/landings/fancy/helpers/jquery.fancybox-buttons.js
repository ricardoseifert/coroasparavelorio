/*!
 * Buttons helper for fancyBox
 * version: 1.0.5 (Mon, 15 Oct 2012)
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         helpers : {
 *             buttons: {
 *                 position : 'top'
 *             }
 *         }
 *     });
 *
 */
(function(n){var t=n.fancybox;t.helpers.buttons={defaults:{skipSingle:!1,position:"top",tpl:'<div id="fancybox-buttons"><ul><li><a class="btnPrev" title="Previous" href="javascript:;"><\/a><\/li><li><a class="btnPlay" title="Start slideshow" href="javascript:;"><\/a><\/li><li><a class="btnNext" title="Next" href="javascript:;"><\/a><\/li><li><a class="btnToggle" title="Toggle size" href="javascript:;"><\/a><\/li><li><a class="btnClose" title="Close" href="javascript:;"><\/a><\/li><\/ul><\/div>'},list:null,buttons:null,beforeLoad:function(n,t){if(n.skipSingle&&t.group.length<2){t.helpers.buttons=!1,t.closeBtn=!0;return}t.margin[n.position==="bottom"?2:0]+=30},onPlayStart:function(){this.buttons&&this.buttons.play.attr("title","Pause slideshow").addClass("btnPlayOn")},onPlayEnd:function(){this.buttons&&this.buttons.play.attr("title","Start slideshow").removeClass("btnPlayOn")},afterShow:function(i,r){var u=this.buttons;u||(this.list=n(i.tpl).addClass(i.position).appendTo("body"),u={prev:this.list.find(".btnPrev").click(t.prev),next:this.list.find(".btnNext").click(t.next),play:this.list.find(".btnPlay").click(t.play),toggle:this.list.find(".btnToggle").click(t.toggle),close:this.list.find(".btnClose").click(t.close)}),r.index>0||r.loop?u.prev.removeClass("btnDisabled"):u.prev.addClass("btnDisabled"),r.loop||r.index<r.group.length-1?(u.next.removeClass("btnDisabled"),u.play.removeClass("btnDisabled")):(u.next.addClass("btnDisabled"),u.play.addClass("btnDisabled")),this.buttons=u;this.onUpdate(i,r)},onUpdate:function(n,t){var i;this.buttons&&(i=this.buttons.toggle.removeClass("btnDisabled btnToggleOn"),t.canShrink?i.addClass("btnToggleOn"):t.canExpand||i.addClass("btnDisabled"))},beforeClose:function(){this.list&&this.list.remove(),this.list=null,this.buttons=null}}})(jQuery);