﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcExtensions.Infrastructure;
using Ninject;
using Domain.Core;
using System.Web.Mvc;

namespace Web.Factories
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel = new StandardKernel(new NinjectModule());

        public NinjectControllerFactory()
        {
            //SITE
            kernel.Bind<Web.Controllers.A_EmpresaController>().ToSelf();  
            kernel.Bind<Web.Controllers.BaseController>().ToSelf();
            kernel.Bind<Web.Controllers.BoletoController>().ToSelf();
            kernel.Bind<Web.Controllers.Cadastre_SeController>().ToSelf();
            kernel.Bind<Web.Controllers.CarrinhoController>().ToSelf();
            kernel.Bind<Web.Controllers.Cidades_e_RegioesController>().ToSelf();
            kernel.Bind<Web.Controllers.ClienteController>().ToSelf();
            kernel.Bind<Web.Controllers.Como_ComprarController>().ToSelf(); 
            kernel.Bind<Web.Controllers.ContatoController>().ToSelf();
            kernel.Bind<Web.Controllers.FloresController>().ToSelf(); 
            kernel.Bind<Web.Controllers.CupomController>().ToSelf(); 
            kernel.Bind<Web.Controllers.ErrorController>().ToSelf();
            kernel.Bind<Web.Controllers.Esqueci_Minha_SenhaController>().ToSelf();
            kernel.Bind<Web.Controllers.Mensagens_para_CartaoController>().ToSelf();
            kernel.Bind<Web.Controllers.HomeController>().ToSelf();
            kernel.Bind<Web.Controllers.Indique_o_Lacos_FloresController>().ToSelf();
            kernel.Bind<Web.Controllers.LocalController>().ToSelf(); 
            kernel.Bind<Web.Controllers.PagSeguroController>().ToSelf();
            kernel.Bind<Web.Controllers.PayPalController>().ToSelf();
            kernel.Bind<Web.Controllers.PedidoController>().ToSelf();
            kernel.Bind<Web.Controllers.Pesquisa_de_SatisfacaoController>().ToSelf();
            kernel.Bind<Web.Controllers.Politicas_de_PrivacidadeController>().ToSelf();
            kernel.Bind<Web.Controllers.ProdutosController>().ToSelf();
            kernel.Bind<Web.Controllers.SegurancaController>().ToSelf();
            kernel.Bind<Web.Controllers.Seja_um_PromotorController>().ToSelf();
            kernel.Bind<Web.Controllers.Seja_uma_Floricultura_ParceiraController>().ToSelf();
            kernel.Bind<Web.Controllers.ShoplineController>().ToSelf();
            kernel.Bind<Web.Controllers.Significado_das_FloresController>().ToSelf();
            kernel.Bind<Web.Controllers.Solucoes_CorporativasController>().ToSelf();
            kernel.Bind<Web.Controllers.Tempo_e_Prazo_de_EntregaController>().ToSelf();
            kernel.Bind<Web.Controllers.Termos_e_Condicoes_ComerciaisController>().ToSelf(); 
            kernel.Bind<Web.Controllers.XMLController>().ToSelf(); 

            
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, string.Format("The controller for path '{0}' could not be found or it does not implement IController.", requestContext.HttpContext.Request.Path));
            }

            return (IController)kernel.Get(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;

            if (disposable != null)
                disposable.Dispose();
        }

    }
}