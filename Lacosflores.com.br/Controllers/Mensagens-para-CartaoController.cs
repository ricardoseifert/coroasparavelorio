﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Mensagens_para_CartaoController : BaseController
    {

        public Mensagens_para_CartaoController(ObjectContext context) : base(context)
        {
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            return View();
        }

    }
}
