﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Web.Models;
using Domain.Repositories;
using Domain.Service;

namespace Web.Controllers
{
    //[RequireHttps]
    public class CupomController : BaseController
    {

        private ClienteModel clienteModel;
        private CupomRepository cupomRepository;
        private CupomService cupomService;

        public CupomController(ObjectContext context) : base(context)
        {
            clienteModel = new ClienteModel(context);
            cupomRepository = new CupomRepository(context);
            cupomService = new CupomService(context);
        }

        public ContentResult Aplicar(Carrinho carrinho, string codigo)
        {
            var cupom = cupomRepository.GetByCodigo(codigo);
            if (cupom != null)
            {
                if (clienteModel.Autenticado)
                {
                    if (!cupomService.PodeUtilizar(clienteModel.CurrentCliente, cupom))
                    {
                        return Content("Você já utilizou esse cupom.");
                    }
                }
                carrinho.Cupom = cupom;
                carrinho.Atualizar();
                return Content("OK");
            }
            else
            {
                return Content("Cupom inválido.");
            }
        }

        public ContentResult Remover(Carrinho carrinho)
        {
            carrinho.Cupom = null;
            carrinho.Atualizar();
            return Content("OK");
        }

    }
}
