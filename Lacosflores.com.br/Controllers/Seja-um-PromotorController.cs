﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Seja_um_PromotorController : BaseController
    {
        public Seja_um_PromotorController(ObjectContext context) : base(context)
        {
    
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var nome = form["nome"].ToUpper();
                var cpf = form["cpf"];
                var cep = form["cep"];
                var endereco = form["endereco"].ToUpper();
                var numero = form["numero"].ToUpper();
                var complemento = form["complemento"].ToUpper();
                var bairro = form["bairro"].ToUpper();
                var cidade = form["cidade"].ToUpper();
                var uf = form["uf"].ToUpper();
                var telefone = form["telefone"];
                var celular = form["celular"];
                var nextel = form["nextel"];
                var conheceu = form["conheceu"];
                var email = form["email"].ToLower();

                //ENVIAR EMAIL
                var corpo = string.Format("O seguinte usuário entrou em contato para ser um promotor:\n\nNOME: {0}\n\nCPF: {1}\n\nCEP: {2}\n\nENDEREÇO: {3}\n\nNÚMERO: {4}\n\nCOMPLEMENTO: {5}\n\nBAIRRO: {6}\n\nCIDADE: {7}\n\nUF: {8}\n\nTEL. RESIDENCIAL: {9}\n\nTEL. CELULAR: {10}\n\nNEXTEL: {11}\n\nE-MAIL: {12}\n\nCOMO CONHECEU: {13}"
                    , nome, cpf, cep, endereco, numero, complemento, bairro, cidade, uf, telefone, celular, nextel, email, conheceu);

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Flores", Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_FALECONOSCO, "ATENDIMENTO", email, nome, Domain.Core.Configuracoes.EMAIL_COPIA, "[LAÇOS FLORES] - Seja um promoto", corpo, false))
                    return Json(new { status = "OK", mensagem = "Sua solicitação foi enviada com sucesso! Por favor, aguarde nosso retorno." });
                else
                    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua solicitação, tente novamente mais tarde ou entre em contato através de nossos telefones." });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }

    }
}
