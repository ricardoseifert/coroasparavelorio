﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Web.Models;
using Domain.Repositories;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace Web.Controllers
{
    //[RequireHttps]
    public class CarrinhoController : BaseController
    {

        private IPersistentRepository<ProdutoTamanho> produtoTamanhoRepository;
        private IPersistentRepository<Desconto> descontoRepository;
        private IPersistentRepository<EmailsRemarketing> emailsRemarkRepository; 
        private ClienteRepository clienteRepository;
        private ClienteModel clienteModel;

        public CarrinhoController(ObjectContext context) : base(context)
        {
            clienteRepository = new ClienteRepository(context);
            produtoTamanhoRepository = new PersistentRepository<ProdutoTamanho>(context);
            descontoRepository = new PersistentRepository<Desconto>(context);
            emailsRemarkRepository = new PersistentRepository<EmailsRemarketing>(context);
            clienteModel = new ClienteModel(context);
        }

        public ActionResult Index(Carrinho carrinho)
        {
            if (carrinho.Vazio)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Desconto = descontoRepository.GetAll().FirstOrDefault();
            ViewBag.Cliente = clienteModel.Autenticado;
            return View(carrinho);
        }

        public ActionResult Resumo(Carrinho carrinho)
        {
            return View(carrinho);
        }

        public ActionResult ResumoMobile(Carrinho carrinho)
        {
            return View(carrinho);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult Adicionar(Carrinho carrinho, int produtoTamanhoID, string descricao, string mensagem, string observacoes)
        {
            var produtoTamanho = produtoTamanhoRepository.Get(produtoTamanhoID);
            if (produtoTamanho != null)
            {
                carrinho.Adicionar(produtoTamanho, descricao, mensagem, observacoes);
                return Content("OK");
            }
            return Content("ERRO");
        }

        [HttpGet]
        [ValidateInput(false)]
        public void AdicionarGet(Carrinho carrinho, int produtoTamanhoID, string descricao, string mensagem, string observacoes)
        {
            string Redirect = "/carrinho";
            if (!string.IsNullOrEmpty(Request.QueryString["Src"])){
                if(Request.QueryString["Src"] == "LC")
                {
                    Redirect = "/carrinho?Src=LC";
                }
            }

            var produtoTamanho = produtoTamanhoRepository.Get(produtoTamanhoID);
            if (produtoTamanho != null)
            {
                carrinho.Adicionar(produtoTamanho, descricao, mensagem, observacoes);
                Response.Redirect(Redirect);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult Atualizar(Carrinho carrinho, int itemID, string descricao, string mensagem, string observacoes)
        {
            carrinho.Atualizar(itemID, descricao, mensagem, observacoes);
            return Content("OK");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ContentResult AtualizarMensagem(Carrinho carrinho, int itemID, string mensagem)
        {
            carrinho.Atualizar(itemID, mensagem);
            return Content("OK");
        }

        public ContentResult Remover(Carrinho carrinho, int itemID)
        {
            carrinho.Remover(itemID);
            return Content("OK");
        }

        public ActionResult Limpar(Carrinho carrinho)
        {
            carrinho.Limpar();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult VerificarEmail(string id){
            string email = id;
            
            var cliente = clienteRepository.GetByExpression(c => c.Email == email).FirstOrDefault();
            if (cliente != null){
                return RedirectToAction("identificacao", "Cliente", new { email = email });
            }
            else{
                #region Old
                ////estava dando erro, pois o server rejeita conexoes diretas usnado TCP
                //StringBuilder stbSql = new StringBuilder();
                //stbSql.Append("IF NOT EXISTS(SELECT [Email] FROM [Cliente].[EmailsRemarketing] WHERE [Cliente].[EmailsRemarketing].[Email] = '{0}')");
                //stbSql.Append("INSERT INTO [Cliente].[EmailsRemarketing] ([Email],[DataCadastro]) VALUES ('{1}', GETDATE())");
                //string sql = string.Format(stbSql.ToString(), email, email);
                //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["COROASString"].ConnectionString);
                //SqlCommand command = new SqlCommand(sql, conn);

                //conn.Open();
                //command.ExecuteNonQuery();
                //conn.Close();
                #endregion
                //registra o email do usuario como recebido de uma campanha
                if (!emailsRemarkRepository.GetByExpression(e=>e.Email == email).Any()){
                    emailsRemarkRepository.Save(new EmailsRemarketing {Email = email, DataCadastro = DateTime.Now});
                }
                
                return RedirectToAction("index", "cadastre-se", new { email = email, returnUrl = "/carrinho" });
            }
        }
    }
}
