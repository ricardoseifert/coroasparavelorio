﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Cidades_e_RegioesController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;

        public Cidades_e_RegioesController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var locais = new List<Local>();
            locais.AddRange(localRepository.GetByTipo(Local.Tipos.Cidade));

            foreach (var local in locais)
                local.TotalLocaisRelacionados = localRepository.TotalLocaisRelacionados(local);

            ViewBag.Destaques = locais.Where(c => c.Destaque).OrderBy(c => c.Titulo).Take(20).ToList();
            ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome).ToList();
            return View(locais);
        }

    }
}
