﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Tempo_e_Prazo_de_EntregaController : BaseController
    {
        public Tempo_e_Prazo_de_EntregaController(ObjectContext context) : base(context)
        {
    
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            return View();
        }

    }
}
