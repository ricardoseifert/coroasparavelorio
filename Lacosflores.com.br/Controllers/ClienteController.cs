﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.Web.Security;
using Web.Models;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Entities;
using Domain.Service;

namespace Web.Controllers
{
    //[RequireHttps]
    public class ClienteController : BaseController
    {

        private ClienteRepository clienteRepository;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private AvisoService avisoService;
        private ClienteModel clienteModel;

        public ClienteController(ObjectContext context) : base(context)
        {
            clienteRepository = new ClienteRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            avisoService = new AvisoService();
            clienteModel = new ClienteModel(context);
        }

        public ActionResult Menu()
        {
            return View(clienteModel);
        }

        public ActionResult MenuMobile()
        {
            return View(clienteModel);
        }

        public ActionResult Identificacao()
        {
            return View();
        }

        public ActionResult Meus_Pedidos()
        {
            if (clienteModel.Autenticado)
            {
                var pedidos = clienteModel.CurrentCliente.Pedidoes.OrderByDescending(p => p.DataCriacao).ToList();
                return View(pedidos);
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/cliente/meus-pedidos" });
            }
        }

        public ActionResult Minha_Conta()
        {
            if (clienteModel.Autenticado)
            {
                var cliente = clienteModel.CurrentCliente;
                ViewBag.Cidades = cidadeRepository.GetByExpression(e => e.EstadoID == cliente.EstadoID).OrderBy(e => e.Nome);
                ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                return View(cliente);
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/cliente/minha-conta" });
            }
        }

        [HttpPost]
        public ActionResult Minha_Conta(FormCollection form)
        {           
            var ID = clienteModel.ID;
            var cliente = clienteRepository.Get(ID);
            if (cliente != null)
            {
                var Nome = form["Nome"];
                var Email = form["Email"];
                var TipoID = Convert.ToInt32(form["TipoID"]);
                var EmailSecundario = form["EmailSecundario"];
                var Documento = form["Documento"];

                var RazaoSocial = "";
                var InscricaoEstadual = "";
                if (TipoID == (int)Domain.Entities.Cliente.Tipos.PessoaJuridica)
                {
                    RazaoSocial = form["RazaoSocial"];
                    InscricaoEstadual = form["InscricaoEstadual"];
                }

                var Senha = form["Senha"].Trim();
                var TelefoneContato = form["TelefoneContato"];
                var CelularContato = form["CelularContato"];
                var Skype = form["Skype"];

                var Exterior = (form["Exterior"] == "on");
                var CEP = "";
                int? EstadoID = null;
                int? CidadeID = null;
                var Logradouro = "";
                var Numero = "";
                var Complemento = "";
                var Bairro = "";

                if (!Exterior)
                {
                    CEP = form["CEP"];
                    Logradouro = form["Logradouro"];
                    Numero = form["Numero"];
                    Complemento = form["Complemento"];
                    Bairro = form["Bairro"];
                    EstadoID = Convert.ToInt32(form["EstadoID"]);
                    CidadeID = Convert.ToInt32(form["CidadeID"]);
                }

                cliente.Nome = Nome;
                cliente.Email = Email;
                cliente.TipoID = TipoID;
                cliente.EmailSecundario = EmailSecundario;
                cliente.Documento = Documento;
                cliente.RazaoSocial = RazaoSocial;
                cliente.InscricaoEstadual = InscricaoEstadual;
                cliente.Senha = Senha;
                cliente.TelefoneContato = TelefoneContato;
                cliente.CelularContato = CelularContato;
                cliente.Skype = Skype;
                cliente.Exterior = Exterior;
                cliente.CEP = CEP;
                cliente.Logradouro = Logradouro;
                cliente.Numero = Numero;
                cliente.Complemento = Complemento;
                cliente.Bairro = Bairro;
                cliente.EstadoID = EstadoID;
                cliente.CidadeID = CidadeID;
                cliente.DataAtualizacao = DateTime.Now;
                
                clienteRepository.Save(cliente);
                avisoService.addEmailMkt(cliente);
                if (form["redirect"].Length > 0)
                    return Redirect(form["redirect"]);
                else
                    return RedirectToAction("minha-conta", "cliente", new { msg = "sucesso" });
            }
            else
            {
                return RedirectToAction("minha-conta", "cliente", new { msg = "erro" });
            }
        }

        public ContentResult ValidarFisicaExterior(string email)
        {
            var cliente = clienteRepository.GetByExpression(c => c.Email == email && c.ID != clienteModel.ID).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

        public ContentResult ValidarFisica(string email, string cpf)
        {
            if (!Domain.Core.Funcoes.Valida_CPF(cpf))
            {
                return Content("CPF Inválido");
            }

            var cliente = clienteRepository.GetByExpression(c => c.Documento == cpf && c.ID != clienteModel.ID).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse CPF");
            }

            cliente = clienteRepository.GetByExpression(c => c.Email == email && c.ID != clienteModel.ID).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

        public ContentResult ValidarJuridica(string email, string cnpj)
        {
            if (!Domain.Core.Funcoes.Valida_CNPJ(cnpj))
            {
                return Content("CNPJ inválido");
            }

            var cliente = clienteRepository.GetByExpression(c => c.Documento == cnpj && c.ID != clienteModel.ID).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse CNPJ");
            }

            cliente = clienteRepository.GetByExpression(c => c.Email == email && c.ID != clienteModel.ID).FirstOrDefault();
            if (cliente != null)
            {
                return Content("Já existe outro cliente com esse e-mail");
            }

            return Content("OK");
        }

        public ContentResult Entrar(string email, string senha)
        {
            var cliente = clienteRepository.GetByEmailSenha(email, senha);
            if (cliente != null)
            {
                var user = new LoggedUser();
                user.ID = cliente.ID;
                user.Name = cliente.Nome;
                user.Username = cliente.Email;
                user.ExpiresIn = DateTime.Now.AddHours(2);
                user.AcessTime = DateTime.Now;
                user.AuthenticatedAreaName = "Cliente";
                user.AccessGroups.Add("Cliente");

                cliente.DataUltimoLogin = DateTime.Now;
                clienteRepository.Save(cliente);

                FormsAuthentication.SetAuthCookie(user.ToJSON(), false);

                return Content("OK");
            }
            return Content("Login e/ou senha inválido(s)");
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return Redirect("/");
        }

    }
}
