﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
    //[RequireHttps]
    public class FloresController : BaseController
    {

        private ProdutoRepository produtoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Coloracao> coloracaoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;

        public FloresController(ObjectContext context) : base(context)
        {
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            coloracaoRepository = new PersistentRepository<Coloracao>(context);
            produtoRepository = new ProdutoRepository(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var produtos = produtoRepository.GetByExpression(p => p.DisponivelLacosFlores && p.Tipo == Produto.Tipos.Kitsbebe).OrderBy(c => c.Ordem).ToList();
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)]
        public ActionResult Categoria(string categoria)
        {
            var produtos = produtoRepository.GetByExpression(p => p.DisponivelLacosFlores).OrderBy(c => c.Ordem).ToList();
            switch(categoria)
            {
                case "coroa-de-flores":
                    produtos = produtos.Where(c=> c.Tipo == Produto.Tipos.CoroaFlor).ToList();
                    ViewBag.Title = "Coroa de Flores";
                    ViewBag.Titulo = "Coroa de Flores";
                    break;
                //case "arranjo-maternidade":
                //    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.ArranjoFlor).ToList();
                //    ViewBag.Title = "Arranjos de Maternidade";
                //    ViewBag.Titulo = "Arranjos de Maternidade";
                //    break;
                //case "flor-de-aniversario":
                //    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Aniversario).ToList();
                //    ViewBag.Title = "Flores de Aniversário";
                //    ViewBag.Titulo = "Flores de Aniversário";
                //    break;
                //case "orquidea-e-buque":
                //    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.OrquideaBuque).ToList();
                //    ViewBag.Title = "Orquídeas e Buquês";
                //    ViewBag.Titulo = "Orquídeas e Buquês";
                //    break;
                case "datas-comemorativas":
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Datas).ToList();
                    ViewBag.Title = "Datas Comemorativas";
                    ViewBag.Titulo = "Datas Comemorativas";
                    break;
                //case "eventos":
                //    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Eventos).ToList();
                //    ViewBag.Title = "Eventos";
                //    ViewBag.Titulo = "Eventos";
                //    break;
                case "kits-bebe":
                    produtos = produtos.Where(c => c.Tipo == Produto.Tipos.Kitsbebe).ToList();
                    ViewBag.Title = "Kits Bebê";
                    ViewBag.Titulo = "Kits Bebê";
                    break;
            }
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Sugestoes()
        {
            int faixaPreco = 0;
            int relacionamento = 0;
            int coloracao = 0;
            int? faixaPrecoID = null;
            int? relacionamentoID = null;
            int? coloracaoID = null;
            Domain.Entities.Produto.Sexos sexo = Domain.Entities.Produto.Sexos.Ambos;

            ViewBag.TermoBusca = "<b>homens</b> e <b>mulheres</b>";
            

            if (!String.IsNullOrEmpty(Request.QueryString["sexo"]))
            {
                var sexos = Request.QueryString["sexo"];
                if (!sexos.Contains("M") || !sexos.Contains("F"))
                {
                    if (sexos.Contains("M"))
                    {
                        ViewBag.TermoBusca = "<b>homens</b>";
                        sexo = Domain.Entities.Produto.Sexos.Masculino;
                    }
                    else if (sexos.Contains("F"))
                    {
                        ViewBag.TermoBusca = "<b>mulheres</b>";
                        sexo = Domain.Entities.Produto.Sexos.Feminino;
                    }
                }
            }
            
            if (!String.IsNullOrEmpty(Request.QueryString["faixa-de-preco"]))
            {
                if (int.TryParse(Request.QueryString["faixa-de-preco"], out faixaPreco))
                {
                    var f = faixaPrecoRepository.Get(faixaPreco);
                    if (f != null)
                    {
                        ViewBag.TermoBusca += ", com valores variando de <b>" + f.PrecoInicio.ToString("C2") + "</b> até <b>" + f.PrecoFim.ToString("C2") + "</b>";
                    }
                    faixaPrecoID = faixaPreco;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["relacionamento"]))
            {
                if (int.TryParse(Request.QueryString["relacionamento"], out relacionamento))
                {
                    var r = relacionamentoRepository.Get(relacionamento);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += ", indicado para um <b>" + r.Nome.ToLower() + "</b>";
                    }
                    relacionamentoID = relacionamento;
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["coloracao"]))
            {
                if (int.TryParse(Request.QueryString["coloracao"], out coloracao))
                {
                    var r = coloracaoRepository.Get(coloracao);
                    if (r != null)
                    {
                        ViewBag.TermoBusca += " com flores <b>" + r.Nome.ToLower() + "</b>";
                    }
                    coloracaoID = coloracao;
                }
            }
            var sugestoes = produtoRepository.SugestoesLacos(faixaPrecoID, relacionamentoID, coloracaoID, sexo);
            if (sugestoes.Count() == 0)
            {
                ViewBag.NaoEncontrado = produtoRepository.SugestoesLacos(null, null, coloracaoID, sexo).OrderBy(c => Guid.NewGuid()).Take(4).ToList();
            }
            ViewBag.Sugestoes = true;
            return View(sugestoes);
        }

    }
}
