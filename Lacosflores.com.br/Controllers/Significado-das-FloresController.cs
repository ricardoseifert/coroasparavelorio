﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Significado_das_FloresController : BaseController
    {

        private IPersistentRepository<Flor> florRepository;

        public Significado_das_FloresController(ObjectContext context) : base(context)
        {
            florRepository = new PersistentRepository<Flor>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var flores = florRepository.GetByExpression(f => f.Publicado).OrderBy(f => f.Nome).ToList();
            return View(flores);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Detalhes(int id)
        {
            var flor = florRepository.GetByExpression(n => n.Publicado && n.ID == id).FirstOrDefault();
            if (flor == null)
                return RedirectToAction("Index");
            ViewBag.Title = flor.Nome + " - Significado das Flores";
            ViewBag.Description = flor.TagDescription;
            ViewBag.KeyWords = flor.TagKeywords;
            ViewBag.ProdutosVejaTambem = flor.ProdutoFlors.Where(c => c.Produto.DisponivelLacosFlores).Select(c => c.Produto).ToList();
            return View(flor);
        }

    }
}
