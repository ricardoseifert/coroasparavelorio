﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoletoNet;
using System.IO;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    //[RequireHttps]
    public class BoletoController : Controller
    {

        private PedidoRepository pedidoRepository;
        private IPersistentRepository<Faturamento> faturamentoRepository;

        public BoletoController(ObjectContext context)
        {
            pedidoRepository = new PedidoRepository(context);
            faturamentoRepository = new PersistentRepository<Faturamento>(context);
        }

        public ActionResult Index(Guid codigo)
        {
            var pedido = pedidoRepository.GetByCodigo(codigo);
            if (pedido != null)
            {
                if (pedido.Boleto != null)
                {
                    var html = BoletoService.Gerar(pedido.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, pedido.Numero + "_boleto.pdf");
                    return Content("");
                }
                else
                    return Content("Grupo Laços Flores<Br>Link inválido. Entre em contato com nossa central de atendimento no (11) 4097-9400");
            }
            else
                return Content("Grupo Laços Flores<Br>Link inválido, o pedido não existe ou foi cancelado. Entre em contato com nossa central de atendimento no (11) 4097-9400");
        }

        [HttpGet]
        public ActionResult Empresa(string codigo)
        {
            var id = Convert.ToInt32(Domain.Core.Criptografia.Decrypt(codigo));
            var fatura = faturamentoRepository.Get(id);
            if (fatura != null)
            {
                if (fatura.Boleto != null)
                {
                    var html = BoletoService.Gerar(fatura.Boleto);
                    Domain.Core.Funcoes.ImprimirPDF(html, fatura.ID + "_boleto.pdf");
                    return Content("");
                }
                else
                    return Content("Grupo Laços Flores<Br>Link inválido. Entre em contato com nossa central de atendimento no 0800-777-1986.");
            }
            else
                return Content("Grupo Laços Flores<Br>Link inválido, o pedido não existe ou foi cancelado. Entre em contato com nossa central de atendimento no 0800-777-1986.");
        }


        public ActionResult Retorno()
        {
            var retorno = new ArquivoRetornoCNAB400();
            var fileStream = new FileStream(Server.MapPath("/Content/Boleto/retorno.txt"), FileMode.Open, FileAccess.Read);
            retorno.LerArquivoRetorno(new BoletoNet.Banco(341), fileStream);
            foreach (var item in retorno.ListaDetalhe)
            {
                Response.Write(item.NumeroDocumento + " - " + item.NomeSacado + " - " + item.ValorTitulo.ToString("c2") + " - " + item.DataVencimento.ToShortDateString());
            }
            return Content("");
        }
    }
}
