﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Mais_VendidasController : BaseController
    {
         
        private IPersistentRepository<Produto> produtoRepository;

        public Mais_VendidasController(ObjectContext context)
            : base(context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);  
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var produtos = produtoRepository.GetByExpression(c => c.DisponivelLacosFlores && c.MaisVendida > 0 ).OrderBy(c => c.MaisVendida).ToList();
            return View(produtos);
        }
          
    }
}
