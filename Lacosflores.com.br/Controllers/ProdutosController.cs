﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;

namespace Web.Controllers
{
    //[RequireHttps]
    public class ProdutosController : BaseController
    {
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<Noticia> noticiaRepository;

        public ProdutosController(ObjectContext context): base(context)
        {
            produtoRepository = new ProdutoRepository(context);
            noticiaRepository = new PersistentRepository<Noticia>(context);
        }

        public ActionResult Produtos(List<Produto> produtos)
        {
            return View(produtos);
        }

        public ActionResult VejaTambem(List<Produto> produtos)
        {
            return View(produtos);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Detalhes(int id)
        {
            var produto = produtoRepository.DetalhesLacos(id);
            if (produto == null)
                return RedirectToAction("Index", "Home");
             
            return View(produto);
        }

    }
}
