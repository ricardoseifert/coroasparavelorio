﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Entities;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Web.Models;
using Web.Handler;
using Domain.Helpers;

namespace Web.Controllers
{
    //[RequireHttps]
    public class LocalController : BaseController
    {

        private LocalRepository localRepository;
        private IPersistentRepository<Estado> estadoRepository;
        public ProdutoRepository produtoRepository;
        private IPersistentRepository<Redirect> redirectRepository;

        public struct Resultado
        {
            public string Tipo;
            public List<Local> Locais;
        }

        public struct Local
        {
            public string Nome;
            public string Url;
        }

        public LocalController(ObjectContext context) : base(context)
        {
            localRepository = new LocalRepository(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            produtoRepository = new ProdutoRepository(context);
            redirectRepository = new PersistentRepository<Redirect>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index(string rotuloUrl)
        {
            var local = localRepository.GetByRotuloUrl(rotuloUrl);
            if (local != null)
            {
                ViewBag.Titulo = local.Titulo
                    ;
                ViewBag.Title = local.Titulo 
                    ;
                ViewBag.Local = local.Cidade.NomeCompleto;
                ViewBag.Descricao = local.DescricaoRodape;
                ViewBag.Description = local.TagDescription;
                ViewBag.Keywords = local.TagKeywords;
                ViewBag.Televendas = local.Televendas;

                ViewBag.Produtos = produtoRepository.PublicadosLacosFlores().Take(4).ToList();

                Session["local"] = local;

                ViewBag.LocaisCidade = localRepository.GetByExpression(c => c.CidadeID == local.CidadeID && c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade && c.Disponivel && c.ID != local.ID).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();

                return View(local);
            }
            else
            {
                var pagina = "/flores/" + rotuloUrl;
                var redirect = redirectRepository.GetByExpression(c => c.Origem.Contains(pagina)).FirstOrDefault();
                if (redirect == null)
                {

                    Response.StatusCode = 404;
                    Response.TrySkipIisCustomErrors = true;

                    var destaques = produtoRepository.DestaquesHomeLacosFlores();
                    ViewBag.Destaques = destaques;
                    ViewBag.Pagina = pagina;

                    return RedirectToAction("Oops", "Error", new { url = pagina});
                }
                else
                {
                    return RedirectPermanent(redirect.Destino);
                }
            }
        }

        [HttpGet]
        public ActionResult Buscar(string termo)
        {
            var resultado = new List<Domain.Entities.Local>();
            if(!String.IsNullOrEmpty(termo))
                resultado = localRepository.Buscar(termo).Where(c=> c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade).OrderBy(c => c.Titulo).OrderBy(c=> c.TipoID).ToList();
            else
                resultado = localRepository.GetByExpression(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();

            if (Request.QueryString["cidadeID"] != null)
            {
                var cidadeID = 0;
                Int32.TryParse(Request.QueryString["cidadeID"], out cidadeID);
                if (cidadeID > 0)
                {
                    resultado = resultado.Where(c => c.CidadeID == cidadeID).ToList();
                }
            }
            if (Request.QueryString["tipo"] != null)
            {
                switch (Request.QueryString["tipo"])
                { 
                    case "cidades":
                        resultado = resultado.Where(c => c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade).ToList();
                        break; 
                }
            }
            if (Request.QueryString["cidades"] != null)
            {
                resultado = localRepository.Buscar(termo, (int)Domain.Entities.Local.Tipos.Cidade).OrderBy(c => c.Titulo).OrderBy(c => c.TipoID).ToList();
            }
            //PAGINAÇÃO
            var paginaAtual = 0;
            Int32.TryParse(Request.QueryString["pagina"], out paginaAtual);
            if (paginaAtual == 0)
                paginaAtual = 1;

            var totalItens = resultado.Count;
            var totalPaginas = Math.Ceiling((decimal)totalItens / 100);
            if (totalPaginas > 1)
            {
                resultado = resultado.Skip((paginaAtual - 1) * 100).Take(100).ToList();
            }
            ViewBag.PaginaAtual = paginaAtual;
            ViewBag.TotalItens = totalItens;
            ViewBag.TotalPaginas = totalPaginas;
            ViewBag.TituloPaginacao = "Exibindo página " + paginaAtual + " de " + totalPaginas + " páginas no total de " + totalItens + " registros.";

            ViewBag.Destaques = produtoRepository.DestaquesHomeLacosFlores(4);
            return View(resultado);
        }

        [HttpGet]
        public JsonResult BuscarRapido(string termo)
        {
            var resultado = localRepository.GetByExpression(c => c.Titulo.Contains(termo) && c.TipoID == (int)Domain.Entities.Local.Tipos.Cidade).OrderBy(c => c.Titulo).OrderBy(c => c.Titulo).Take(10).ToList();
            var resultados = new List<Autocomplete>();
            foreach (var local in resultado)
            {
                resultados.Add(new Autocomplete { label = local.Titulo + " - " + local.Cidade.NomeCompleto, category = Domain.Helpers.EnumHelper.GetDescription(local.Tipo), url = Url.RouteUrl("Local", new { rotuloUrl = local.RotuloUrl }) });
            }
            return Json(resultados, JsonRequestBehavior.AllowGet); 
        }

        public JsonResult Cidades(int estadoID)
        {
            var cidades = new List<CidadeModel>();
            var estado = estadoRepository.Get(estadoID);
            if (estado != null)
            {
                foreach (var cidade in estado.Cidades)
                {
                    cidades.Add(new CidadeModel(cidade));
                }
            }
            return Json(cidades);
        }

    }
}
