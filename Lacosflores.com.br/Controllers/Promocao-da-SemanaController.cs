﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;
using DomainExtensions.Repositories.Interfaces;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Promocao_da_SemanaController : BaseController
    {

        private IPersistentRepository<Promocao> promocaoRepository;

        public Promocao_da_SemanaController(ObjectContext context)
            : base(context)
        {
            promocaoRepository = new PersistentRepository<Promocao>(context);  
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var promocao = promocaoRepository.GetByExpression(c => c.Publicado && c.DataInicio <= DateTime.Now && c.DataFim >= DateTime.Now && c.PromocaoProdutoes.Count(p => p.Produto.TipoID != (int)Domain.Entities.Produto.Tipos.CoroaFlor)> 0).OrderByDescending(c => c.DataInicio).FirstOrDefault();
            ViewBag.TemPromocaoSemana = true;
            return View(promocao); 
        }
          
    }
}
