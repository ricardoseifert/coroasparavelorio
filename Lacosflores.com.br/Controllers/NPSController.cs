﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Domain.Entities;

namespace Web.Controllers
{
    public class NPSController : Controller
    {
        private IPersistentRepository<NPSRating> npsREspository;
        private IPersistentRepository<NPSMensagem> npsMensagemRepository;
        private IPersistentRepository<NPSPergunta> NPSPerguntarepository;
        private IPersistentRepository<NPSRating_X_NPSPergunta> NPSRating_X_NPSPerguntarepository;


        public NPSController(ObjectContext context)
        {
            npsREspository = new PersistentRepository<NPSRating>(context);
            npsMensagemRepository = new PersistentRepository<NPSMensagem>(context);
            NPSPerguntarepository = new PersistentRepository<NPSPergunta>(context);
            NPSRating_X_NPSPerguntarepository = new PersistentRepository<NPSRating_X_NPSPergunta>(context);
        }

        [HttpPost]
        public ActionResult Salvar(FormCollection form)
        {
            Guid NewGuid = Guid.Parse(Request.Form["guid"]);

            var NPS = npsREspository.GetByExpression(p => p.Guid == NewGuid).FirstOrDefault();

            if (!string.IsNullOrEmpty(Request.Form["obs"]))
            {
                NPS.Obs = Request.Form["obs"].ToString().Trim();
                NPS.Obs = NPS.Obs.Replace(System.Environment.NewLine, "<br />");
            }

            npsREspository.Save(NPS);

            foreach (var Iten in Request.Form.Keys)
            {
                if (Iten.ToString().Contains("pergunta-"))
                {
                    int PerguntaId = int.Parse(Iten.ToString().Split('-')[1].ToString());

                    NPSRating_X_NPSPerguntarepository.Save(new NPSRating_X_NPSPergunta { ID_PerguntaNPS = PerguntaId, ID_RatingNPS = NPS.ID });
                }
            }

            ViewBag.Nota = Request.QueryString["Nota"].ToString();

            return View("~/Views/NPS/Index.cshtml");
        }

        [HttpGet]
        public ActionResult AtualizaNPS(string Identi, string Nota)
        {
            var NPS = npsREspository.GetByExpression(p => p.Guid == new Guid(Identi)).FirstOrDefault();
            int NotaRequest = int.Parse(Nota);

            var NPSMensagem = npsMensagemRepository.GetByExpression(p => p.Nota == NotaRequest).FirstOrDefault();

            NPS.Nota = int.Parse(Nota);

            ViewBag.NPSMEnsagem = NPSMensagem.MensagemNPS;
            ViewBag.NPSPerguntas = NPSMensagem.NPSPerguntas;

            npsREspository.Save(NPS);

            return View("~/Views/NPS/AtualizaNPS.cshtml");
        }
    }
}
