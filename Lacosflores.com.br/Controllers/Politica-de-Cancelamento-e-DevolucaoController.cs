﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Politica_de_Cancelamento_e_DevolucaoController : BaseController
    {
        public Politica_de_Cancelamento_e_DevolucaoController(ObjectContext context) : base(context)
        {
    
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            return View();
        }

    }
}
