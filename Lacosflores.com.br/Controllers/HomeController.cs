﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Domain.Repositories;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using DomainExtensions.Repositories;
using Domain.Service;
using Web.Filter;

namespace Web.Controllers
{
    //[RequireHttps]
    public class HomeController : BaseController
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<FaixaPreco> faixaPrecoRepository;
        private IPersistentRepository<Tamanho> tamanhoRepository;
        private IPersistentRepository<Relacionamento> relacionamentoRepository;
        private IPersistentRepository<Cidade> cidadeRepository;
        private IPersistentRepository<Coloracao> corRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Configuracao> configuracaoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private IPersistentRepository<PrimeiraCompra> primeiraCompraRepository;
        private IPersistentRepository<Local> localRepository;
        
        public HomeController(ObjectContext context) : base(context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            faixaPrecoRepository = new PersistentRepository<FaixaPreco>(context);
            tamanhoRepository = new PersistentRepository<Tamanho>(context);
            cidadeRepository = new PersistentRepository<Cidade>(context);
            relacionamentoRepository = new PersistentRepository<Relacionamento>(context);
            corRepository = new PersistentRepository<Coloracao>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            configuracaoRepository = new PersistentRepository<Configuracao>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            localRepository = new PersistentRepository<Local>(context);
            primeiraCompraRepository = new PersistentRepository<PrimeiraCompra>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ActionResult Index()
        {
            var destaques = produtoRepository.GetByExpression(c => c.DestaqueHome && c.DisponivelLacosFlores && c.ProdutoTamanhoes.Count > 0 ).OrderBy(c => c.Ordem).ToList();
            return View(destaques);
        }

        public ActionResult Sugestoes()
        {
            ViewBag.FaixaPreco = faixaPrecoRepository.GetAll().ToList();
            ViewBag.Tamanho = tamanhoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Relacionamento = relacionamentoRepository.GetAll().OrderBy(t => t.Nome).ToList();
            ViewBag.Coloracao = corRepository.GetAll().OrderBy(t => t.Nome).ToList();
            return View();
        }

        public ActionResult Mais_Vendidas()
        {
            var produtos = produtoRepository.GetByExpression(c => c.DisponivelLacosFlores && c.MaisVendida > 0 ).OrderBy(c => c.MaisVendida).Take(3).ToList();
            return View(produtos);
        }

        [HttpPost]
        public JsonResult GetCidades(string uf, string selected = "")
        {
            var cidades = cidadeRepository.GetByExpression(c => c.Estado.Sigla.ToUpper().Trim() == uf.ToUpper().Trim()).OrderBy(c => c.Nome);

            List<SelectListItem> result = new List<SelectListItem>();
            foreach (Cidade cidade in cidades)
            {
                result.Add(new SelectListItem { Text = cidade.Nome.ToUpper(), Value = cidade.ID.ToString(), Selected = Domain.Core.Funcoes.AcertaAcentos(cidade.Nome).ToUpper() == Domain.Core.Funcoes.AcertaAcentos(selected).ToUpper() });
            }
            return Json(result);
        }
        
        public ActionResult AtendimentoIndisponivel()
        {
            return View();
        }
         
        [HttpPost]
        public JsonResult GetCEP(string cep)
        {
            var resultado = new CEP.CEP(cep);
            return Json(new { resultado = resultado.Resultado, resultado_txt = resultado.ResultadoTXT, uf = resultado.UF, cidade = resultado.Cidade, bairro = resultado.Bairro, tipo_logradouro = resultado.TipoLogradouro, logradouro = resultado.Logradouro });
        }
    }
}
