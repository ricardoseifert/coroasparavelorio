﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using System.Xml.Linq;
using System.IO;
using System.Text;

namespace Web.Controllers
{
    public class XMLController : Controller
    {
        private IPersistentRepository<Produto> produtoRepository;
        private IPersistentRepository<Flor> florRepository;
        private IPersistentRepository<Depoimento> depoimentoRepository;
        private IPersistentRepository<Noticia> noticiaRepository;
        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<Duvida> duvidaRepository;

        public XMLController(ObjectContext context)
        {
            produtoRepository = new PersistentRepository<Produto>(context);
            florRepository = new PersistentRepository<Flor>(context);
            depoimentoRepository = new PersistentRepository<Depoimento>(context);
            noticiaRepository = new PersistentRepository<Noticia>(context);
            localRepository = new PersistentRepository<Local>(context);
            duvidaRepository = new PersistentRepository<Duvida>(context);
        }

        [OutputCache(CacheProfile = "CacheSite")]
        [CompressFilter(Order = 1)] 
        public ContentResult Sitemap()
        {
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace xsischema = "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd";
            
            string url = Domain.Core.Configuracoes.DOMINIO;
            var produtos = produtoRepository.GetByExpression(p => p.DisponivelLacosFlores).OrderBy(c => c.Ordem).ToList(); 
            var flores = florRepository.GetByExpression(n => n.Publicado).OrderByDescending(n => n.Nome); 
            var locais = localRepository.GetByExpression(n => n.Disponivel).OrderByDescending(n => n.Titulo);

            var declaration = new XDeclaration("1.0", "utf-8", "");
            var elements = new XElement(ns + "urlset", new XAttribute(XNamespace.Xmlns + "xsi", xsi), new XAttribute(xsi + "schemaLocation", xsischema));

            //Raíz
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Contato
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "contato")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Tempo e Prazo
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "tempo-e-prazo-de-entrega")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Mais Vendidas
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "mais-vendidas")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Formas Pagto
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "formas-de-pagamento")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //Cidades e regiões
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "cidades-e-regioes")), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));

            //A empresa
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "a-empresa")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seja uma floricultura parceria
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seja-uma-floricultura-parceira")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seja um promotor
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seja-um-promotor")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Termos e condições
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "termos-e-condicoes-comerciais")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Politicas de Privacidade
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "politicas-de-privacidade")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Como comprar
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "como-comprar")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Indique
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "indique-o-lacos-flores")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));
            
            //Significado
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "significado-das-flores")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Mensagens
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "mensagens-para-cartao")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Seguranca
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "seguranca")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Promoção da Semana
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "promocao-da-semana")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Soluções corporativas
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "solucoes-corporativas")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));
                         
            //Pesquisa
            elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.Action("Index", "pesquisa-de-satisfacao")), new XElement(ns + "changefreq", "monthly"), new XElement(ns + "priority", "0.8")));

            //Flores
            foreach (var produto in produtos)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Produto", new { rotuloUrl = produto.RotuloUrl, id = produto.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "1")));
            }
             
            //Significado
            foreach (var item in flores)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Flores", new { rotuloUrl = item.RotuloUrl, id = item.ID })), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }
      
            //Locais
            foreach (var item in locais)
            {
                elements.Add(new XElement(ns + "url", new XElement(ns + "loc", url + Url.RouteUrl("Local", new { rotuloUrl = item.RotuloUrl})), new XElement(ns + "changefreq", "daily"), new XElement(ns + "priority", "0.8")));
            }

            var sitemap = new XDocument(declaration, elements);


            var xml = new StringWriter();
            sitemap.Save(xml);
            var retorno = xml.GetStringBuilder();
            retorno = retorno.Replace("utf-16", "utf-8");

            return Content(retorno.ToString(), "text/xml", Encoding.UTF8);
        }

    }
}
