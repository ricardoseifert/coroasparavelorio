﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class Datas_ComemorativasController : Controller
    {
        //
        // GET: /Data-Comemorativa/

        public ActionResult Index(int id, string descricao)
        {
            ViewBag.produtoTamanhoID = id;
            ViewBag.descricao = descricao;
            ViewBag.mensagem = "";
            ViewBag.observacoes = "";
            return View();
        }

        public ActionResult DiaNamorados()
        {
            return View();
        }

        public ActionResult DiaDosPais()
        {
            return View();
        }

        public ActionResult OutubroRosa()
        {
            return View();
        }
    }
}
