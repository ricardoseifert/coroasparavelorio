﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Web.Models;
using System.Data.Objects;
using Domain.Service;
using DomainExtensions.Repositories.Interfaces;
using DomainExtensions.Repositories;
using Domain.Repositories;
using PagarMe;

namespace Web.Controllers
{
    //[RequireHttps]
    public class PedidoController : BaseController
    {

        private ClienteModel clienteModel;
        private PedidoService pedidoService;
        private IPersistentRepository<Estado> estadoRepository;
        private IPersistentRepository<Cliente> clienteRepository;
        private AvisoService avisoService;
        private PedidoRepository pedidoRepository;

        public PedidoController(ObjectContext context) : base(context)
        {
            clienteModel = new ClienteModel(context);
            pedidoService = new PedidoService(context);
            estadoRepository = new PersistentRepository<Estado>(context);
            clienteRepository = new PersistentRepository<Cliente>(context);
            avisoService = new AvisoService();
            pedidoRepository = new PedidoRepository(context);
        }

        
        public ActionResult Index(Carrinho carrinho)
        {
            if (clienteModel.Autenticado)
            {
                ViewBag.Homolog = (Domain.Core.Configuracoes.HOMOLOGACAO && clienteModel.CurrentCliente.Email == Domain.Core.Configuracoes.EMAIL_HOMOLOGACAO);

                if (clienteModel.CurrentCliente.DataAtualizacao.HasValue)
                {
                    ViewBag.Cliente = clienteModel.CurrentCliente;
                    ViewBag.Estados = estadoRepository.GetAll().OrderBy(e => e.Nome);
                    if (carrinho.Vazio)
                    {
                        return RedirectToAction("index", "home");
                    }
                    return View(carrinho);
                }
                else
                {
                    return RedirectToAction("minha-conta", "cliente", new { returnUrl = "/pedido" , atualizar = "sim"});
                }
            }
            else
            {
                return RedirectToAction("identificacao", "cliente", new { returnUrl = "/pedido" });
            }
        }

        
        public JsonResult Criar(Carrinho carrinho, int cidadeID, string dataSolicitada, int estadoID, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada)
        {
            var pedido = pedidoService.CriarPedido(carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, "", pessoaHomenageada, clienteModel.CurrentCliente.TelefoneContato, "SITE", "LAÇOS FLORES", "ONLINE");
            if (pedido != null)
            {
                var model = new PedidoModel(pedido);
                avisoService.addEmailMkt(pedido);
                //pedido.EnviarPedidoEmail();
                carrinho.Limpar();
                return Json(model);
            }
            else
            {
                return Json("Não foi possível criar seu pedido. Verifique os dados preenchidos.");
            }
        }

        
        public JsonResult Pagar(Carrinho carrinho, int bandeira, string nome, string numero, string validade, string codigo, int parcela, string dataSolicitada, Pedido.FormasPagamento formaPagamento, string localEntrega, Pedido.MeiosPagamento meioPagamento, string observacoes, string pessoaHomenageada, string cardHash, int cidadeID, int estadoID)
        {
            var NovoPedido = pedidoService.CriarPedido(carrinho, cidadeID, clienteModel.ID, dataSolicitada, estadoID, formaPagamento, localEntrega, meioPagamento, observacoes, "", pessoaHomenageada, clienteModel.CurrentCliente.TelefoneContato, "SITE", "LAÇOS FLORES", "ONLINE");
            //NovoPedido.EnviarPedidoEmailNovo();

            #region PAGARME

            PagarMeService.DefaultApiKey = Domain.Core.Configuracoes.PagarMe_DefaultApiKey;

            Transaction transaction = new Transaction();

            transaction.Amount = int.Parse(NovoPedido.ValorTotal.ToString().Replace(".", "").Replace(",", ""));
            transaction.Installments = parcela;

            transaction.AcquirerName = NovoPedido.Cliente.Nome.ToUpper();

            transaction.Customer = new Customer
            {
                Email = NovoPedido.Cliente.Email,
                DocumentNumber = (!string.IsNullOrEmpty(NovoPedido.Cliente.Documento) && NovoPedido.Cliente.Documento != "N/I") ? NovoPedido.Cliente.Documento : null,
                Name = NovoPedido.Cliente.Nome + " " + NovoPedido.Cliente.RazaoSocial,
                Phone = new Phone
                {
                    Ddd = (!string.IsNullOrEmpty(NovoPedido.Cliente.TelefoneContato)) ? NovoPedido.Cliente.TelefoneContato.Substring(0, 3).Replace("(", "").Replace(")", "") : "011",
                    Number = (!string.IsNullOrEmpty(NovoPedido.Cliente.TelefoneContato)) ? NovoPedido.Cliente.TelefoneContato.Remove(0, 5) : "999999999",
                },
                Address = new Address
                {
                    City = (NovoPedido.Cliente.Cidade != null) ? NovoPedido.Cliente.Cidade.Nome : "N/I",
                    State = (NovoPedido.Cliente.Estado != null) ? NovoPedido.Cliente.Estado.Nome : "N/I",
                    Street = (!string.IsNullOrEmpty(NovoPedido.Cliente.Logradouro)) ? NovoPedido.Cliente.Logradouro : "N/I",
                    StreetNumber = (!string.IsNullOrEmpty(NovoPedido.Cliente.Numero)) ? NovoPedido.Cliente.Numero : "001",
                    Neighborhood = (!string.IsNullOrEmpty(NovoPedido.Cliente.Bairro)) ? NovoPedido.Cliente.Bairro : "N/I",
                    Country = "BR",
                    Zipcode = (!string.IsNullOrEmpty(NovoPedido.Cliente.CEP)) ? NovoPedido.Cliente.CEP : "01507000"
                }
            };

            transaction.CardHash = cardHash;

            try
            {
                transaction.Save();

                TransactionStatus status = transaction.Status;
                var model = new PedidoModel(NovoPedido);

                switch (status)
                {
                    case TransactionStatus.Paid:
                        NovoPedido.DataProcessamento = DateTime.Now;
                        NovoPedido.DataPagamento = DateTime.Now;
                        NovoPedido.StatusPagamento = Domain.Entities.Pedido.TodosStatusPagamento.Pago;
                        NovoPedido.ValorPago = NovoPedido.ValorTotal;
                        NovoPedido.Mensagem = status.ToString();
                        NovoPedido.Retorno = status.ToString();
                        NovoPedido.TID = transaction.Id.ToString();
                        pedidoRepository.Save(NovoPedido);

                      
                        NovoPedido.Retorno = "Paid";
                        NovoPedido.Codigo = Guid.NewGuid();

                        break;

                    case TransactionStatus.Refused:
                        NovoPedido.Mensagem = status.ToString();
                        NovoPedido.TID = transaction.Id.ToString();
                        NovoPedido.Retorno = status.ToString();

                        NovoPedido.Retorno = "Refused";
                        NovoPedido.Codigo = Guid.NewGuid();

                        break;
                    default:
                        return Json("Pagamento não Aprovado.");
                }

                return Json(model);
            }
            catch (PagarMeException e)
            {
                var Erro = e.Error;
                var erros = Erro.Errors;
                return Json(erros[0].Message);
            }

            #endregion

        }

        
        public ActionResult Confirmacao(Guid? codigo, Carrinho carrinho)
        {
            if (codigo.HasValue)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Caminho(int posicao = 0)
        {
            ViewBag.Posicao = posicao;
            return View();
        }
    }
}
