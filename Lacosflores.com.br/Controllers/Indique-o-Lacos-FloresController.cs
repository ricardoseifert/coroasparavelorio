﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;

namespace Web.Controllers
{
    //[RequireHttps]
    public class Indique_o_Lacos_FloresController : BaseController
    {
        public Indique_o_Lacos_FloresController(ObjectContext context)
            : base(context)
        {
    
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, CaptchaVerify("A soma não é válida")]
        public ActionResult Enviar(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var nomeDe = form["nomeDe"].ToUpper();
                var emailDe = form["emailDe"].ToLower();
                var nomePara = form["nomePara"].ToUpper();
                var emailPara = form["emailPara"].ToLower();
                if (emailDe.Length > 0 && nomeDe.Length > 0 && emailPara.Length > 0 && nomePara.Length > 0)
                {
                    //ENVIAR EMAIL
                    var html = System.IO.File.ReadAllText(Server.MapPath("/content/html/lacosflores/indique.htm"));
                    html = html.Replace("[URL]", Domain.Core.Configuracoes.CDN_IMG);
                    html = html.Replace("[NOMEDE]", nomeDe);
                    html = html.Replace("[NOMEPARA]", nomePara);

                    if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "Laços Flores", emailPara, nomePara, emailDe, nomeDe, "", "[LAÇOS FLORES] - " + nomeDe.ToUpper() + " sugeriu que você visitasse o nosso site", html, true))
                        return Json(new { status = "OK", mensagem = "Sua indicação foi enviada com sucesso!" });
                    else
                        return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua indicação, tente novamente mais tarde."});
                }
                else
                    return Json(new { status = "ERRO", mensagem = "Não foi possível enviar sua indicação, tente novamente mais tarde." });
            }
            else
            {
                IUpdateInfoModel captchaValue = this.GenerateMathCaptchaValue();
                return Json(new
                {
                    status = "ERRO",
                    mensagem = "Digite a soma correta.",
                    Captcha =
                        new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }
        }

    }
}
