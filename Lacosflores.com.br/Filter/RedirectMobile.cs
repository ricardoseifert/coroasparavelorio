﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace Web.Filter
{
    public class RedirectMobile : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string agent = HttpContext.Current.Request.ServerVariables.Get("HTTP_USER_AGENT");
            string wapprofile = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_WAP_PROFILE"); 
            var originalUri = HttpContext.Current.Request.Url.OriginalString;
            var requestedUrl = originalUri.IndexOf("?") > 0 ? originalUri.Substring(0, originalUri.IndexOf("?")) : originalUri;
            var path = requestedUrl.Replace(Domain.Core.Configuracoes.DOMINIO, "").Replace(":80", "").Replace(":8080", "").Replace(":443", "").Trim().ToLower();

            if (path != "/a-empresa" && path != "/contato")
                path = "";

            if (HttpContext.Current.Request.QueryString["mobile"] == "no")
            {
                HttpContext.Current.Response.Cookies["mobile"].Value = "no";
            }

            if (HttpContext.Current.Request.Cookies["mobile"] == null)
            {
                if ((agent.Contains("Mobile") || agent.Contains("BlackBerry") || agent.Contains("Blackberry") || agent.Contains("Android") || agent.Contains("android") || agent.Contains("iPad")))
                {
                    if ((agent.Contains("Pingdom") || agent.Contains("BlackBerry") || agent.Contains("Blackberry") || agent.Contains("Android") || agent.Contains("android") || agent.Contains("Mozilla") || agent.Contains("Windows NT") || agent.Contains("Macintosh")) && (string.IsNullOrEmpty(wapprofile) || wapprofile.Contains("UAProf") || wapprofile.Contains("uaprof")))
                    {
                        filterContext.Result = new RedirectResult(Domain.Core.Configuracoes.DOMINIO_MOBILE + path);
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(Domain.Core.Configuracoes.DOMINIO_MOBILE + path);
                    }
                }
            }
            return;
        }
    }
}