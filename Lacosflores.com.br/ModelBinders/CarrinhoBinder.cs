﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;

namespace Web.ModelBinders
{
    public class CarrinhoBinder : IModelBinder
    {
        private const string SESSION_KEY = "_carrinho";
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
                throw new InvalidOperationException("Cannot update instances");

            var carrinho = (Carrinho)controllerContext.HttpContext.Session[SESSION_KEY];

            if (carrinho == null)
            {
                carrinho = new Carrinho();
                controllerContext.HttpContext.Session[SESSION_KEY] = carrinho;
                controllerContext.HttpContext.Session.Timeout = 360;
            }
            else
            {
                if(carrinho.Itens.Where(r => r.ProdutoTamanho.Produto.ID == 286
                || r.ProdutoTamanho.Produto.ID == 287
                 || r.ProdutoTamanho.Produto.ID == 288
                  || r.ProdutoTamanho.Produto.ID == 289
                   || r.ProdutoTamanho.Produto.ID == 290
                    || r.ProdutoTamanho.Produto.ID == 291
                    ).Any())
                {
                    controllerContext.HttpContext.Session["ORIGEM"] = "LP";
                }
                else
                {
                    controllerContext.HttpContext.Session["ORIGEM"] = "";
                }
            }            
            return carrinho;
        }
    }
}