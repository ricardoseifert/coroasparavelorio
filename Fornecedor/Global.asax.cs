﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Fornecedor.Controllers;
using Domain.Entities;
using Fornecedor.Infraestructure;

namespace Fornecedor
{ 

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.RouteExistingFiles = false;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.css/{*pathInfo}");
            routes.IgnoreRoute("{resource}.js/{*pathInfo}");
            routes.IgnoreRoute("{resource}.gif/{*pathInfo}");
            routes.IgnoreRoute("{resource}.jpg/{*pathInfo}");
            routes.IgnoreRoute("{resource}.png/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ico/{*pathInfo}");
            routes.IgnoreRoute("{resource}.svg/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ttf/{*pathInfo}");
            routes.IgnoreRoute("{resource}.woff/{*pathInfo}");
            routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "content" });
            routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "scripts" });
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}"); 

            var webnamespace = new RouteValueDictionary();
            webnamespace.Add("namespaces", new HashSet<string>(new string[] { "Fornecedor.Controllers" }));


            routes.Add
            (
                "Default",
                new Route("{controller}/{action}/{id}",
                new RouteValueDictionary
                (
                    new { controller = "Home", action = "Index", id = UrlParameter.Optional, DataTokens = webnamespace }),
                    null,
                    webnamespace,
                    new HyphenatedRouteHandler()
                )
            );
        }

        public class HyphenatedRouteHandler : MvcRouteHandler
        {
            protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
                requestContext.RouteData.Values["controller"] = requestContext.RouteData.Values["controller"].ToString().Replace("-", "_");
                requestContext.RouteData.Values["action"] = requestContext.RouteData.Values["action"].ToString().Replace("-", "_");
                return base.GetHttpHandler(requestContext);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Domain.Core.Configuracoes.EMAIL_RECEBIMENTO_ERRO.Length > 0)
            {
                var httpContext = ((MvcApplication)sender).Context;
                var currentController = " ";
                var currentAction = " ";
                var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                if (currentRouteData != null)
                {
                    if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    {
                        currentController = currentRouteData.Values["controller"].ToString();
                    }

                    if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    {
                        currentAction = currentRouteData.Values["action"].ToString();
                    }
                }

                var ex = Server.GetLastError();
                var controller = new ErrorController(new COROASEntities());
                var routeData = new RouteData();
                var action = "Index";

                if (ex is HttpException)
                {
                    var httpEx = ex as HttpException;

                    switch (httpEx.GetHttpCode())
                    {
                        case 404:
                            action = "Oops";
                            break;

                        case 401:
                            action = "Acesso-Negado";
                            break;
                    }
                }


                httpContext.ClearError();
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
                httpContext.Response.TrySkipIisCustomErrors = true;

                routeData.Values["controller"] = "Error";
                routeData.Values["action"] = action;

                controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());

        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            string result = String.Empty;
            if (custom.Equals("user", StringComparison.OrdinalIgnoreCase))
            {
                HttpCookie cookie = context.Request.Cookies[".ASPXAUTH"];
                if (cookie != null)
                {
                    return cookie.Value;
                }
            }
            else { result = base.GetVaryByCustomString(context, custom); }
            return result;

        }
    }
}