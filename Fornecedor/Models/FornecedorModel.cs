﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainExtensions.Repositories.Interfaces; 
using DomainExtensions.Repositories;
using System.Data.Objects;
using MvcExtensions.Security.Models;
using System.IO;

namespace Fornecedor.ViewModels
{
    public class FornecedorModel
    {
        private IPersistentRepository<Domain.Entities.Fornecedor> fornecedorRepository;

        public int ID { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Nome { get; set; } 
        public Domain.Entities.Fornecedor CurrentFornecedor { get; set; }

        public FornecedorModel(ObjectContext context)
        {
            fornecedorRepository = new PersistentRepository<Domain.Entities.Fornecedor>(context);

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var loggeduser = LoggedUser.GetFromJSON(HttpContext.Current.User.Identity.Name);
                var fornecedor = fornecedorRepository.GetByExpression(c => c.ID == loggeduser.ID).FirstOrDefault();
                if (fornecedor != null)
                {
                    ID = fornecedor.ID;
                    IsAuthenticated = true;
                    Nome = fornecedor.Nome;
                    CurrentFornecedor = fornecedor;
                }
            }
        }

    }

}