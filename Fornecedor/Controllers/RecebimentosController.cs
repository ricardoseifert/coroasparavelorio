﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using System.Web.Security;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Fornecedor.ViewModels;
using System.IO;
using MvcExtensions.Security.Filters;


namespace Fornecedor.Controllers
{
    //===========================================================================
    [AreaAuthorization("Fornecedor", "/login")]
    public class RecebimentosController : Controller
    {
        #region Variaveis
        //----------------------------------------------------------------------
        private FornecedorModel fornecedorModel;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<Repasse> repasseRepository;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public RecebimentosController(ObjectContext context)
        {
            fornecedorModel = new FornecedorModel(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            repasseRepository = new PersistentRepository<Repasse>(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index()
        {
            DateTime datainicialped = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicialped"], out datainicialped);

            var datafinalped = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinalped"], out datafinalped);

            DateTime dataInicialAux = new DateTime(datainicialped.Year, datainicialped.Month, datainicialped.Day);
            DateTime dataFinalAux = new DateTime(datafinalped.Year, datafinalped.Month, datafinalped.Day).AddDays(1).AddSeconds(-1);

            var resultado = repasseRepository.GetByExpression(c => (c.PedidoItemID.HasValue || c.PedidoEmpresaID.HasValue) && (c.StatusID != (int)Repasse.TodosStatus.Cancelado && c.Fornecedor.ID == fornecedorModel.CurrentFornecedor.ID) && ((c.PedidoEmpresa.DataCriacao >= dataInicialAux && c.PedidoEmpresa.DataCriacao <= dataFinalAux) || (c.PedidoItem.Pedido.DataCriacao >= dataInicialAux && c.PedidoItem.Pedido.DataCriacao <= dataFinalAux))).OrderByDescending(c => c.DataCriacao).ToList();

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            var status = 0;
            Int32.TryParse(Request.QueryString["status"], out status);

            if (datainicial != new DateTime())
                resultado = resultado.Where(c => c.DataRepasse >= datainicial).ToList();

            if (datafinal != new DateTime())
            {
                datafinal = datafinal.AddDays(1).AddSeconds(-1);
                resultado = resultado.Where(c => c.DataRepasse <= datafinal).ToList();
            }

            if (status > 0)
                resultado = resultado.Where(c => c.StatusID == status).ToList();

            ViewBag.Pedidos = resultado.Count(c => (c.PedidoItemID.HasValue || c.PedidoEmpresaID.HasValue) && (c.StatusID != (int)Repasse.TodosStatus.Cancelado));
            ViewBag.Pago = resultado.Where(c => (c.PedidoItemID.HasValue || c.PedidoEmpresaID.HasValue) && (c.StatusID == (int)Repasse.TodosStatus.Pago)).Sum(c => c.ValorRepasse).ToString("C2");
            ViewBag.EmAberto = resultado.Where(c => (c.PedidoItemID.HasValue || c.PedidoEmpresaID.HasValue) && (c.StatusID == (int)Repasse.TodosStatus.AguardandoPagamento || c.StatusID == (int)Repasse.TodosStatus.Fechamento)).Sum(c => c.ValorRepasse).ToString("C2");
 
            ViewBag.ExibeLogin = true;
            if (!String.IsNullOrEmpty(fornecedorModel.CurrentFornecedor.SenhaFinanceira))
            {
                if (Request.QueryString["token"] != null)
                {
                    try
                    {
                        var token = Domain.Core.Criptografia.Decrypt(Request.QueryString["token"]);
                        var data = Convert.ToInt64(token.Split(',')[0]);
                        var senha = token.Split(',')[1];

                        if (data > DateTime.Now.Ticks && fornecedorModel.CurrentFornecedor.SenhaFinanceira == senha)
                            ViewBag.ExibeLogin = false;
                    }
                    catch { }
                }
            }
            else
                ViewBag.ExibeLogin = false;


            return View(resultado);
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult Login(string senha)
        {
            if (senha.Length > 0)
            {
                if(fornecedorModel.CurrentFornecedor.SenhaFinanceira.Trim().ToLower() == senha.Trim().ToLower())
                {
                    var FluxoDePagamento = fornecedorModel.CurrentFornecedor.FluxoPagamento;

                    DateTime DataInicialFiltro = DateTime.Now;
                    DateTime DataFinalFiltro = DateTime.Now;

                    if (FluxoDePagamento == Domain.Entities.Fornecedor.FluxosPagamento.Mensal)
                    {
                        DataInicialFiltro = DateTime.Parse("01/" + DateTime.Now.AddMonths(-1).Month.ToString("d2") + "/" + DateTime.Now.AddMonths(-1).Year);
                        DataFinalFiltro = DateTime.Parse("" + DateTime.DaysInMonth(DataInicialFiltro.Year, DataInicialFiltro.Month) + "/" + DataInicialFiltro.Month.ToString("d2") + "/" + DataInicialFiltro.Year).AddDays(1).AddMilliseconds(-1);
                        return Json("?datainicialped=" + DataInicialFiltro.ToString("dd/MM/yyyy").Replace('-', '/') + "&datafinalped=" + DataFinalFiltro.ToString("dd/MM/yyyy").Replace('-', '/') + "&token=" + Domain.Core.Criptografia.Encrypt(DateTime.Now.AddHours(2).Ticks + "," + senha));
                    }

                    if (FluxoDePagamento == Domain.Entities.Fornecedor.FluxosPagamento.Quinzenal)
                    {
                        if(DateTime.Now.Day < 16)
                        {
                            // SEGUNDA QUINZENA DO MES ANTERIOR
                            DataInicialFiltro = DateTime.Parse("16/" + DateTime.Now.AddMonths(-1).Month.ToString("d2") + "/" + DateTime.Now.AddMonths(-1).Year);
                            DataFinalFiltro = DateTime.Parse("" + DateTime.DaysInMonth(DataInicialFiltro.Year, DataInicialFiltro.Month) + "/" + DataInicialFiltro.Month.ToString("d2") + "/" + DataInicialFiltro.Year).AddDays(1).AddMilliseconds(-1);
                        }
                        else
                        {
                            // PRIMEIRA QUINZENA DO MES ATUAL
                            DataInicialFiltro = DateTime.Parse("01/" + DateTime.Now.Month.ToString("d2") + "/" + DateTime.Now.Year);
                            DataFinalFiltro = DateTime.Parse("15/" + DataInicialFiltro.Month.ToString("d2") + "/" + DataInicialFiltro.Year).AddDays(1).AddMilliseconds(-1);
                        }
                    }

                    return Json("?datainicialped=" + DataInicialFiltro.ToString("dd/MM/yyyy").Replace('-', '/') + "&datafinalped=" + DataFinalFiltro.ToString("dd/MM/yyyy").Replace('-', '/') + "&token=" + Domain.Core.Criptografia.Encrypt(DateTime.Now.AddHours(2).Ticks + "," + senha));
                }
                else
                {
                    return Json("ERRO");
                }
            }
            else
            {
                return Json("ERRO");
            }
        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}