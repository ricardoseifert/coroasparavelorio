﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
using Fornecedor.Handler;

namespace Fornecedor.Controllers
{
    //===========================================================================
    public class ErrorController : Controller
    {
        #region Construtor
        //----------------------------------------------------------------------
        public ErrorController(ObjectContext context)
        {

        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index()
        {
            return View();
        }
        //----------------------------------------------------------------------
        public ActionResult Oops(string url)
        {
            var originalUri = url ?? Request.QueryString["aspxerrorpath"] ?? Request.Url.OriginalString;

            var controllerName = (string)RouteData.Values["controller"];
            var actionName = (string)RouteData.Values["action"];
            var model = new _404(new HttpException(404, "Página não encontrada"), controllerName, actionName)
            {
                RequestedUrl = originalUri,
                ReferrerUrl = Request.UrlReferrer == null ? "" : Request.UrlReferrer.OriginalString
            };

            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;

            return View("Oops", model);
        }
        //----------------------------------------------------------------------
#endregion

    }
    //===========================================================================
}