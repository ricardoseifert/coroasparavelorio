﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using System.Web.Security;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Fornecedor.ViewModels;
using MvcExtensions.Security.Filters;

namespace Fornecedor.Controllers
{
    //===========================================================================
    [AreaAuthorization("Fornecedor", "/login")]
    public class Meus_DadosController : Controller
    {
        #region Variaveis
        //----------------------------------------------------------------------
        private FornecedorModel fornecedorModel;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public Meus_DadosController(ObjectContext context)
        { 
            fornecedorModel = new FornecedorModel(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index()
        {
            return View(fornecedorModel.CurrentFornecedor);
        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}
