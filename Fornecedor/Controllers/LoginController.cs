﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcExtensions.Security.Models;
using System.Web.Security;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;

namespace Fornecedor.Controllers
{
    //===========================================================================
    public class LoginController : Controller
    {
        #region Variaveis
        //----------------------------------------------------------------------
        private IPersistentRepository<Domain.Entities.Fornecedor> fornecedorRepository;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public LoginController(ObjectContext context)
        {
            fornecedorRepository = new PersistentRepository<Domain.Entities.Fornecedor>(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index()
        {
            return View();
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult Login(string email, string senha, string ReturnUrl = null)
        {
            if (email.Length > 0 && senha.Length > 0)
            {
                var fornecedor = fornecedorRepository.GetByExpression(a => a.EmailContato == email && a.Senha == senha).FirstOrDefault();

                if (fornecedor != null)
                {
                    if (fornecedor.Liberado)
                    {
                        var user = new LoggedUser();
                        user.ID = fornecedor.ID;
                        user.Name = fornecedor.Nome;
                        user.Username = fornecedor.EmailContato;
                        user.ExpiresIn = DateTime.Now.AddHours(2);
                        user.AcessTime = DateTime.Now;
                        user.AuthenticatedAreaName = "Fornecedor";
                        user.AccessGroups.Add("Fornecedor");
                        FormsAuthentication.SetAuthCookie(user.ToJSON(), true);

                        //fornecedor.DataUltimoLogin = DateTime.Now;
                        //fornecedorRepository.Save(fornecedor);

                        if (!String.IsNullOrEmpty(ReturnUrl))
                        {
                            if (ReturnUrl != "/Home/AccessDenied")
                            {
                                return Json(ReturnUrl);
                            }
                        }
                        return Json("/");
                    }
                    else
                    {
                        return Json("ERRO2");
                    }
                }
                else
                {
                    return Json("ERRO");
                }
            }
            else
            {
                return Json("ERRO");
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult Enviar_Senha(string email)
        {
            var fornecedor = fornecedorRepository.GetByExpression(a => a.EmailContato == email).FirstOrDefault();
            if (fornecedor != null)
            {
                var mensagem = "Olá " + fornecedor.Nome + ",\n\nEstes são seus dados para acesso:";
                mensagem += "\nLogin: " + fornecedor.EmailContato;
                mensagem += "\nSenha: " + fornecedor.Senha;

                if (Domain.Core.Funcoes.EnviaEmail(Domain.Core.Configuracoes.EMAIL_NOREPLY, "GRUPO LAÇOS FLORES", email, email, null, "GRUPO LAÇOS FLORES", null, "[GRUPO LAÇOS FLORES] - Acesso ao sistema de fornecedores", mensagem, false))
                {
                    return Json("Seus dados de acesso foram enviados para seu email.");
                }
                else
                {
                    return Json("Não foi possível enviar seus dados de acesso. Tente novamente mais tarde.");
                }
            }
            else
            {
                return Json("Nenhum acesso cadastrado com esse endereço de email.");
            }
        }
        //----------------------------------------------------------------------
        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}