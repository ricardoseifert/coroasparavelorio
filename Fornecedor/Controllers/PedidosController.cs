﻿using System;
using System.Linq;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Fornecedor.ViewModels;
using System.IO;
using MvcExtensions.Security.Filters;
using System.Globalization;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain.MetodosExtensao;
using Domain.Core;
using Domain.Repositories;

namespace Fornecedor.Controllers {
    //===========================================================================
    [AreaAuthorization("Fornecedor", "/login")]
    public class PedidosController : Controller {

        #region Variaveis
        //----------------------------------------------------------------------
        private FornecedorModel fornecedorModel;
        private IPersistentRepository<PedidoEmpresa> pedidoEmpresaRepository;
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<Local> localRepository;
        private IPersistentRepository<PedidoItem> pedidoItemRepository;
        private IPersistentRepository<Repasse> pedidoRepasseRepository;
        private IPersistentRepository<Domain.Entities.Fornecedor> fornecedorRepository;
        private RelatorioRepository relatorioRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;

        List<Pedido> lstPedidos = new List<Pedido>();
        List<PedidoEmpresa> lstPedidosEmp = new List<PedidoEmpresa>();
        List<PedidoItem> lstPedidosItem = new List<PedidoItem>();

        List<IPedido> lstRetorno = new List<IPedido>();
        Repasse repasse = new Repasse();
        PedidoItem _pedidoItem = new PedidoItem();
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public PedidosController(ObjectContext context) {
            fornecedorModel = new FornecedorModel(context);
            pedidoEmpresaRepository = new PersistentRepository<PedidoEmpresa>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            localRepository = new PersistentRepository<Local>(context);
            pedidoItemRepository = new PersistentRepository<PedidoItem>(context);
            fornecedorRepository = new PersistentRepository<Domain.Entities.Fornecedor>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);

            relatorioRepository = new RelatorioRepository(context);
    }
        //----------------------------------------------------------------------
        #endregion

        #region Retorna Pedidos Dinamicos

        [HttpGet]
        public string RelatorioNovosPedidos()
        {
            List<IPedido> NovosPedidos = new List<IPedido>();

            var PedidosReport = relatorioRepository.GetPedidosAutomaticosFornecedor(fornecedorModel.CurrentFornecedor.ID);
            var LstPedidosSTEPONEIds = PedidosReport.Where(r => r.Step == 1).ToList().Select(r => r.IdPedido).Distinct().ToList();
            var LstPedidosSTEPTWOIds = PedidosReport.Where(r => r.Step == 2 && r.FornecedorEmProcessamento == false).ToList().Select(r => r.IdPedido).Distinct().ToList();
            var LstPedidosSTEPTHREEIds = PedidosReport.Where(r => r.Step == 2 && r.FornecedorEmProcessamento == true && (r.FornecedorEntregou == false || r.FornecedorEntregou == null)).ToList().Select(r => r.IdPedido).Distinct().ToList();

            #region POPULA STEP ONE

            var Table = "<tbody class='SETPONE'>";

            if (LstPedidosSTEPONEIds.Count == 0)
            {
                Table += "<tr class='odd'><td style='text-align: center; vertical-align: middle' colspan='13' class='dataTables_empty'>Não há itens para serem exibidos</td></tr>";
            }

            foreach (var PedidoID in LstPedidosSTEPONEIds)
            {
                var PedidoReturn = PedidosReport.Where(r => r.IdPedido == PedidoID).First();

                Table += "<tr>";
                Table += "<td>#" + PedidoReturn.Numero + "</td>";

                Table += "<td><b>Data: </b>" + PedidoReturn.DataSolicitada + "<br /><b>Local: </b>" + PedidoReturn.Titulo + " - " + PedidoReturn.NomeCompleto + " - " + PedidoReturn.ComplementoLocalEntrega + "<br /><b>Homenageado: </b>" + PedidoReturn.PessoaHomenageada + "</td>";

                #region PREENCHE ITENS DO PEDIDO

                Table += "<td>";
                var Count = 1;

                foreach (var item in PedidosReport.Where(r => r.IdPedido == PedidoID).ToList())
                {
                    Table += "<p>";
                    Table += "<b>Produto - " + Count + "</b>";
                    Table += "</p>";

                    Table += "<ul>";

                    Table += "<li>";
                    Table += "<b>Produto: </b>" + item.NomeDoProduto.Replace("Coroa de Flores", "Coroa") + " / " + item.NomeDoTamanho + " - <a href ='http://cdn.coroasparavelorio.com.br/produtos/" + item.ProdutoID + "/thumb/" + item.Foto + "' target ='_blank'>Foto</a>";
                    Table += "</li>";

                    Table += "</ul>";
                    Count++;
                }

                Table += "</td>";

                #endregion

                #region PREENCHE BOTOES

                Table += " <td> <input style='width: 100px; margin-bottom: 5px; ' type='submit' class='btn btn-success AceitarEntrega' value='Aceitar' rel='" + PedidoID + "," + PedidoReturn.Tipo + "'> <br /> <input style='width: 100px; ' type='submit' class='btn btn-danger RecusarEntrega' value='Recusar' rel='" + PedidoID + "," + PedidoReturn.Tipo + "'> </td>";

                #endregion

                Table += "</tr>";
            }

            Table += "</tbody>";

            #endregion

            #region POPULA STEP TWO

            Table += "<tbody class='SETPTWO'>";

            if (LstPedidosSTEPTWOIds.Count == 0)
            {
                Table += "<tr class='odd'><td style='text-align: center; vertical-align: middle' colspan='13' class='dataTables_empty'>Não há itens para serem exibidos</td></tr>";
            }

            foreach (var PedidoID in LstPedidosSTEPTWOIds)
            {
                var PedidoReturn = PedidosReport.Where(r => r.IdPedido == PedidoID).First();

                Table += "<tr class='ClasseReplace'>";
                Table += "<td>#" + PedidoReturn.Numero + "</td>";

                Table += "<td><b>Data: </b>" + PedidoReturn.DataSolicitada + "<br /><b>Local: </b>" + PedidoReturn.Titulo + " - " + PedidoReturn.NomeCompleto + " - " + PedidoReturn.ComplementoLocalEntrega + "<br /><b>Homenageado: </b>" + PedidoReturn.PessoaHomenageada + "</td>";

                #region PREENCHE ITENS DO PEDIDO

                var FotosValidadas = false;
                var FotosEnviadas = false;
                var TemLogDeFotoRecusada = false;

                #region VERIFICA SE TEM FOTOS ENVIADAS

                var CountFotosEnviadas = 0;
                foreach (var item in PedidosReport.Where(r => r.IdPedido == PedidoID).ToList())
                {
                    var Path = "E:\\WEB\\cdn.coroasparavelorio.com.br\\Pedidos\\" + item.Numero + "\\" + item.PedidoItemID + ".jpg";
                    if (System.IO.File.Exists(Path))
                    {
                        CountFotosEnviadas++;
                    }
                }

                #endregion

                #region PREENCHE FLAGS

                if (PedidosReport.Where(r => r.IdPedido == PedidoID).ToList().Where(r => r.StatusFotoID == (int)PedidoItem.TodosStatusFoto.Aprovada).Count() == PedidosReport.Where(r => r.IdPedido == PedidoID).ToList().Count())
                    FotosValidadas = true;

                if (CountFotosEnviadas == PedidosReport.Where(r => r.IdPedido == PedidoID).ToList().Count())
                    FotosEnviadas = true;

                if (PedidoReturn.QtdReprovacaoFoto > 0)
                    TemLogDeFotoRecusada = true;

                #endregion

                Table += "<td>";

                #region PREENCHE COLUNAS

                var Count = 1;
                foreach (var item in PedidosReport.Where(r => r.IdPedido == PedidoID).ToList())
                {
                    Table += "<p>";
                    Table += "<b>Produto - " + Count + "</b>";
                    Table += "</p>";

                    Table += "<ul>";

                    Table += "<li>";
                    Table += "<b>Produto: </b>" + item.NomeDoProduto.Replace("Coroa de Flores", "Coroa") + " / " + item.NomeDoTamanho + " - <a href ='http://cdn.coroasparavelorio.com.br/produtos/" + item.ProdutoID + "/thumb/" + item.Foto + "' target ='_blank'>Foto</a> - <a href ='/Pedidos/editar/" + item.RepasseID + "' target ='_blank'>Ver Coroa</a>";
                    Table += "</li>";

                    Table += "</ul>";

                    Count++;
                }

                #endregion

                Table += "</td>";

                #endregion

                #region GERENCIAR COR DA LINHA E ABLE DO BTN

                string Diabled = "disabled";
                string Class = "White";
                if (FotosEnviadas == true && FotosValidadas == false)
                {
                    Class = "Yellow";
                }
                if (FotosEnviadas == true && FotosValidadas == true)
                {
                    Class = "Green";
                    Diabled = "";
                }
                if (TemLogDeFotoRecusada == true && FotosEnviadas == false)
                {
                    Class = "Red";
                }

                // ALTERAR COR DA LINHA
                Table = Table.Replace("ClasseReplace", Class);

                #endregion

                #region PREENCHE BOTOES

                if (Configuracoes.HOMOLOGACAO)
                {
                    Diabled = "";
                }

                //Table += " <td> <input style='width: 150px; margin-bottom: 5px;' " + Diabled + " type='submit' class='btn btn-success SairEntrega' value='Sair p/ Entrega' rel='" + PedidoID + "," + PedidoReturn.Tipo + "'> </td>";

                #endregion

                Table += "</tr>";
            }

            Table += "</tbody>";

            #endregion

            #region POPULA STEP THREE

            Table += "<tbody class='SETPTHREE'>";

            if (LstPedidosSTEPTHREEIds.Count == 0)
            {
                Table += "<tr class='odd'><td style='text-align: center; vertical-align: middle' colspan='13' class='dataTables_empty'>Não há itens para serem exibidos</td></tr>";
            }

            foreach (var PedidoID in LstPedidosSTEPTHREEIds)
            {
                var PedidoReturn = PedidosReport.Where(r => r.IdPedido == PedidoID).First();

                Table += "<tr>";
                Table += "<td>#" + PedidoReturn.Numero + "</td>";

                Table += "<td><b>Data: </b>" + PedidoReturn.DataSolicitada + "<br /><b>Local: </b>" + PedidoReturn.Titulo + " - " + PedidoReturn.NomeCompleto + " - " + PedidoReturn.ComplementoLocalEntrega + "<br /><b>Homenageado: </b>" + PedidoReturn.PessoaHomenageada + "</td>";

                #region PREENCHE ITENS DO PEDIDO

                Table += "<td>";
                var Count = 1;

                foreach (var item in PedidosReport.Where(r => r.IdPedido == PedidoID).ToList())
                {
                    Table += "<p>";
                    Table += "<b>Produto - " + Count + "</b>";
                    Table += "</p>";

                    Table += "<ul>";

                    Table += "<li>";
                    Table += "<b>Produto: </b>" + item.NomeDoProduto.Replace("Coroa de Flores", "Coroa") + " / " + item.NomeDoTamanho + " - <a href ='http://cdn.coroasparavelorio.com.br/produtos/" + item.ProdutoID + "/thumb/" + item.Foto + "' target ='_blank'>Foto</a> - <a href ='/Pedidos/editar/" + item.RepasseID + "' target ='_blank'>Ver Coroa</a>";
                    Table += "</li>";

                    Table += "</ul>";
                    Count++;
                }

                Table += "</td>";

                #endregion

                //#region PREENCHE BOTOES

                //Table += " <td> <input style='width: 100px; margin-bottom: 5px; ' type='submit' class='btn btn-success AceitarEntrega' value='Aceitar' rel='" + PedidoID + "," + PedidoReturn.Tipo + "'> <br /> <input style='width: 100px; ' type='submit' class='btn btn-danger RecusarEntrega' value='Recusar' rel='" + PedidoID + "," + PedidoReturn.Tipo + "'> </td>";

                //#endregion

                Table += "</tr>";
            }

            Table += "</tbody>";

            #endregion

            return Table;
        }

        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index(params object[] args)
        {
            var numero = Request.QueryString["numero"];
            var pessoahomenageada = Request.QueryString["pessoahomenageada"];

            #region FILTROS

            var datainicial = new DateTime();
            DateTime.TryParse(Request.QueryString["datainicial"], out datainicial);

            var datafinal = new DateTime();
            DateTime.TryParse(Request.QueryString["datafinal"], out datafinal);

            List<Repasse> lstPedidos = new List<Repasse>();
            datafinal = datafinal.AddDays(1).AddSeconds(-1);

            #endregion

            #region LISTA PEDIDOS

            if (!String.IsNullOrEmpty(numero))
            {
                lstPedidos = fornecedorModel.CurrentFornecedor.Repasses
                    .Where
                    (c => c.DataCriacao >= datainicial && c.DataCriacao <= datafinal && (c.PedidoItem != null ? c.PedidoItem.Pedido.Numero.Equals(numero.ToUpper()) : c.PedidoEmpresa.ID == int.Parse(numero))).ToList();
            }
            else
            {
                if (!String.IsNullOrEmpty(pessoahomenageada))
                    lstPedidos = fornecedorModel.CurrentFornecedor.Repasses.Where(c => c.PedidoItem.Pedido.PessoaHomenageada.ToLower().Contains(pessoahomenageada.ToLower()) || c.PedidoEmpresa.PessoaHomenageada.ToLower().Contains(pessoahomenageada.ToLower()) && c.StatusID != (int)Repasse.TodosStatus.Cancelado).OrderByDescending(c => c.DataCriacao).ToList();

                lstPedidos = fornecedorModel.CurrentFornecedor.Repasses.Where(p => p.DataCriacao >= datainicial && p.DataCriacao <= datafinal && p.StatusID != (int)Repasse.TodosStatus.Cancelado).OrderByDescending(p => p.DataCriacao).ToList();
            }

            #endregion

            var totalItens = lstPedidos.Count;
            ViewBag.Fornecedor = fornecedorModel.CurrentFornecedor;

            return View(lstPedidos);
        }
        //----------------------------------------------------------------------
        public ActionResult PrintEntrega(int PedidoID, string TipoPedido)
        {
            if(TipoPedido == "Cliente")
            {
                var PedidoCPV = pedidoRepository.Get(PedidoID);

                if (PedidoCPV.Fornecedor.ID != fornecedorModel.CurrentFornecedor.ID)
                    return null;

                ViewBag.Numero = "#" + PedidoCPV.Numero;

                if(PedidoCPV.LocalID != null)
                {
                    var Local = localRepository.Get((int)PedidoCPV.LocalID);

                    ViewBag.LocalExt += Local.Titulo;
                }
                else
                {
                    ViewBag.LocalExt += PedidoCPV.LocalEntrega;
                }

                ViewBag.ComplementoLocalEntrega = PedidoCPV.ComplementoLocalEntrega;
                ViewBag.Observacoes = PedidoCPV.Observacoes;

                ViewBag.CidadeEstado = PedidoCPV.Cidade.NomeCompleto;
                ViewBag.PessoaHomenageada = PedidoCPV.PessoaHomenageada;
                ViewBag.TelHomenageado = "N/A";
                ViewBag.NomeFuncionario = "N/A";
                ViewBag.DataSolicitada = PedidoCPV.DataSolicitada;

                var Produtos = "";

                foreach (var item in PedidoCPV.PedidoItems)
                {
                    Produtos += "<tr><td style='padding: 2px; '><b>" + item.ProdutoTamanho.Produto.Nome + " " + item.ProdutoTamanho.Tamanho.Nome + "</b><p><p><b>Frase:</b>" + item.Mensagem.ToUpper() + "</p></td></tr><hr />";
                }
                ViewBag.Produtos = Produtos;

                ViewBag.LinkDownload = "https://wwww.coroasparavelorio.com.br/Fornecedor?Pedido=" + PedidoCPV.Codigo;
            }
            else
            {
                var PedidoCORP = pedidoEmpresaRepository.Get(PedidoID);

                if (PedidoCORP.Fornecedor.ID != fornecedorModel.CurrentFornecedor.ID)
                    return null;

                ViewBag.Numero = "#" + PedidoCORP.Numero;

                if (PedidoCORP.LocalID != null)
                {
                    var Local = localRepository.Get((int)PedidoCORP.LocalID);

                    ViewBag.LocalExt += Local.Titulo;
                }
                else
                {
                    ViewBag.LocalExt += PedidoCORP.LocalEntrega;
                }

                ViewBag.ComplementoLocalEntrega = PedidoCORP.ComplementoLocalEntrega;
                ViewBag.Observacoes = PedidoCORP.Observacoes;

                ViewBag.CidadeEstado = PedidoCORP.Cidade.NomeCompleto;
                ViewBag.PessoaHomenageada = PedidoCORP.PessoaHomenageada;
                ViewBag.TelHomenageado = "N/A";
                ViewBag.NomeFuncionario = PedidoCORP.NomeFuncionario;
                ViewBag.DataSolicitada = PedidoCORP.DataSolicitada;

                var Produtos = "";
                Produtos += "<tr><td style='padding: 2px; '><b>" + PedidoCORP.ProdutoTamanho.Produto.Nome + " " + PedidoCORP.ProdutoTamanho.Tamanho.Nome + "</b><p><p><b>Frase:</b>" + PedidoCORP.Mensagem.ToUpper() + "</p></td></tr><hr />";
                ViewBag.Produtos = Produtos;

                ViewBag.LinkDownload = "https://wwww.coroasparavelorio.com.br/Fornecedor?PedidoEmpresa=" + PedidoCORP.ID;
            }

            return View();
        }
        //----------------------------------------------------------------------
        public ActionResult Editar(int id)
        {
            try
            {
                var _repasseId = id;

                repasse = fornecedorModel.CurrentFornecedor.Repasses.FirstOrDefault(c => c.ID == _repasseId);

                if (repasse != null)
                {
                    ViewBag.PodeEditar = true;

                    if (repasse != null && repasse.PedidoEmpresa != null)
                    {
                        if (repasse.PedidoEmpresa.StatusEntrega != TodosStatusEntrega.EntregaEmAndamento)
                            ViewBag.PodeEditar = false;
                    }
                    else
                    {
                        if (repasse != null && repasse.PedidoItem != null && repasse.PedidoItem.Pedido.StatusEntrega == TodosStatusEntrega.EmProducao)
                            ViewBag.PodeEditar = false;
                    }

                    //Informa o Status do Pedido
                    if (repasse.PedidoEmpresa != null)
                        ViewBag.StatusEntregaPedido = repasse.PedidoEmpresa.StatusEntrega;
                    else
                        ViewBag.StatusEntregaPedido = repasse.PedidoItem.Pedido.StatusEntrega;

                    return View(repasse);
                }
                else
                {
                    return RedirectToAction("Index", new { cod = "SaveFailure", msg = "Falha na leituta do Pepasse do Pedido !" });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", new { cod = "SaveFailure", msg = "Falha na leituta do Pepasse do Pedido! " + ex.Message });
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Editar(FormCollection form)
        {
            try
            {
                var _repasseId = Convert.ToInt32(form["id"]);
                var TipoPedido = form["TipoPedido"];
                var DataEntrega = Convert.ToDateTime(form["dataentrega"] + " " + form["horaentrega"] + ":00");
                var RecebidoPor = form["recebidopor"].ToUpper();
                var ParentescoRecebidoPor = form["parentesco"].ToUpper();
                var Observacoes = form["observacao"];


                repasse = fornecedorModel.CurrentFornecedor.Repasses.FirstOrDefault(c => c.ID == _repasseId);

                if (repasse != null)
                {
                    if (repasse.PedidoEmpresaID.HasValue)
                    {
                        //if (repasse.PedidoEmpresaID.Value.ToString() == form["PedidoEmpresaID"])
                        //{
                            CultureInfo culture = new CultureInfo("pt-BR");

                            var pedidoempresa = repasse.PedidoEmpresa;
                            pedidoempresa.DataEntrega = Convert.ToDateTime(form["dataentrega"] + " " + form["horaentrega"] + ":00", culture);
                            pedidoempresa.RecebidoPor = form["recebidopor"].ToUpper();
                            pedidoempresa.ParentescoRecebidoPor = form["parentesco"].ToUpper();
                            pedidoempresa.FornecedorObservacoes = form["observacao"];
                            pedidoempresa.FornecedorEntregou = form["entregou"] == "on";
                            pedidoEmpresaRepository.Save(pedidoempresa);

                            if (Request.Files.Count > 0)
                            {
                                var foto = Request.Files["File"];
                                if (foto.ContentLength > 0)
                                {
                                    DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedidoempresa.ID));
                                    if (!dir.Exists)
                                    {
                                        dir.Create();
                                    }

                                    //try
                                    //{

                                    var nomefoto = Funcoes.GeraNomeArquivoMVC(foto.FileName);
                                    //FOTO
                                    ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedidoempresa.ID + "/" + pedidoempresa.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                            "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=canvas"));

                                    i.CreateParentDirectory = true;
                                    i.Build();

                                    //}
                                    //catch { }
                                }
                            }
                        //}
                    }
                    else if (repasse.PedidoItemID.HasValue)
                    {
                        //if (repasse.PedidoItemID.Value.ToString() == form["PedidoItemID"])
                        //{
                            CultureInfo culture = new CultureInfo("pt-BR");

                            var pedido = repasse.PedidoItem.Pedido;
                            pedido.DataEntrega = Convert.ToDateTime(form["dataentrega"] + " " + form["horaentrega"] + ":00", culture);
                            pedido.RecebidoPor = form["recebidopor"].ToUpper();
                            pedido.ParentescoRecebidoPor = form["parentesco"].ToUpper();
                            pedido.FornecedorObservacoes = form["observacao"];
                            pedido.FornecedorEntregou = form["entregou"] == "on";
                            pedidoRepository.Save(pedido);

                            if (Request.Files.Count > 0)
                            {
                                foreach (var item in pedido.PedidoItems)
                                {
                                    var foto = Request.Files["foto_" + item.ID];
                                    if (foto.ContentLength > 0)
                                    {
                                        DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/content/pedidos/" + pedido.Numero));
                                        if (!dir.Exists)
                                        {
                                            dir.Create();
                                        }

                                        //try
                                        //{

                                        var nomefoto = Funcoes.GeraNomeArquivoMVC(foto.FileName);
                                        //FOTO
                                        ImageResizer.ImageJob i = new ImageResizer.ImageJob(foto, Server.MapPath("/content/pedidos/" + pedido.Numero + "/" + item.ID + ".jpg"), new ImageResizer.ResizeSettings(
                                                                "width=600&height=600&crop=auto;format=jpg;mode=pad;scale=canvas"));

                                        i.CreateParentDirectory = true;
                                        i.Build();

                                        //}
                                        //catch { }
                                    }
                                }
                            }
                        //}
                    }
                    
                    //Informa o Status do Pedido
                    if (repasse.PedidoEmpresa != null)
                        ViewBag.StatusEntregaPedido = repasse.PedidoEmpresa.StatusEntrega;
                    else
                        ViewBag.StatusEntregaPedido = repasse.PedidoItem.Pedido.StatusEntrega;

                    return RedirectToAction("index", new { id = _repasseId, cod = "SaveSucessConfEnt", msg = "Sucesso no Preenchimento de Confirmação de Entrega do Pedido!" , datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy") });
                }
                else
                {
                    return RedirectToAction("index", new { cod = "SaveFailure", msg = "Falha na leituta do Repasse do Pedido !" , datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy") });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("index", new { cod = "SaveFailure", msg = "Falha na leituta do Repasse do Pedido! " + ex.Message , datainicial = DateTime.Now.ToString("dd/MM/yyyy"), datafinal = DateTime.Now.ToString("dd/MM/yyyy") });
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SalvarStatusEntrega(FormCollection form)
        {
            if (!String.IsNullOrEmpty(form["id"]))
            {
                var idPedidoItem = Convert.ToInt32(form["id"]);

                if (!String.IsNullOrEmpty(form["TipoPedido"]))
                {
                    var tipoPedido = form["TipoPedido"];

                    if (tipoPedido == "Cliente")
                    {
                        _pedidoItem = pedidoItemRepository.Get(idPedidoItem);
                        repasse = fornecedorModel.CurrentFornecedor.Repasses.FirstOrDefault(c => c.PedidoItemID == _pedidoItem.ID);

                        if (repasse != null)
                        {
                            if (repasse.PedidoItemID.HasValue)
                            {
                                CultureInfo culture = new CultureInfo("pt-BR");

                                var pedido = repasse.PedidoItem.Pedido;
                                pedido.DataEntrega = Convert.ToDateTime(form["dataentrega"] + " " + form["horaentrega"], culture);
                                pedido.RecebidoPor = form["recebidopor"].ToUpper();
                                pedido.ParentescoRecebidoPor = form["parentesco"].ToUpper();
                                pedido.FornecedorObservacoes = form["observacao"];
                                pedido.FornecedorEntregou = form["entregou"] == "on";
                                pedidoRepository.Save(pedido);
                            }
                        }
                    }
                    else
                    {
                        lstPedidosEmp = pedidoEmpresaRepository.GetByExpression(p => p.ID == idPedidoItem).ToList();
                        repasse = fornecedorModel.CurrentFornecedor.Repasses.FirstOrDefault(c => c.PedidoEmpresaID == idPedidoItem);

                        if (repasse != null)
                        {
                            if (repasse.PedidoEmpresaID.HasValue)
                            {
                                var pedidoempresa = repasse.PedidoEmpresa;
                                pedidoempresa.DataEntrega = Convert.ToDateTime(form["dataentrega"] + " " + form["horaentrega"]);
                                pedidoempresa.RecebidoPor = form["recebidopor"].ToUpper();
                                pedidoempresa.ParentescoRecebidoPor = form["parentesco"].ToUpper();
                                pedidoempresa.FornecedorObservacoes = form["observacao"];
                                pedidoempresa.FornecedorEntregou = form["entregou"] == "on";
                                pedidoEmpresaRepository.Save(pedidoempresa);
                            }
                        }
                    }
                }
            }
            return RedirectToAction("index", new { cod = "SaveSucess" });
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult AlteraProcessamento(int id, bool valor) {
            var repasse = fornecedorModel.CurrentFornecedor.Repasses.FirstOrDefault(c => c.ID == id);

            if (repasse.PedidoEmpresa != null) {
                if (repasse.PedidoEmpresa.StatusEntrega == TodosStatusEntrega.PendenteFornecedor) {
                    var pedidoempresa = repasse.PedidoEmpresa;
                    pedidoempresa.FornecedorEmProcessamento = valor;
                    pedidoempresa.DataProcessamentoFornecedor = DateTime.Now;
                    pedidoempresa.StatusEntrega = TodosStatusEntrega.EmProducao;
                    pedidoEmpresaRepository.Save(pedidoempresa);
                    return Json("OK");
                }
                else {
                    return Json("Não é possível alterar o processamento de um pedido em operação");
                }
            }
            else {
                if (repasse.PedidoItem.Pedido.StatusEntrega == TodosStatusEntrega.PendenteFornecedor) {
                    var pedido = repasse.PedidoItem.Pedido;
                    pedido.FornecedorEmProcessamento = valor;
                    pedido.DataProcessamentoFornecedor = DateTime.Now;
                    pedido.StatusEntrega = TodosStatusEntrega.EmProducao;
                    pedidoRepository.Save(pedido);
                    return Json("OK");
                }
                else {
                    return Json("Não é possível alterar o processamento de um pedido em operação");
                }
            }
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public JsonResult RecusarEntrega(string Pedido)
        {
            int PedidoID = int.Parse(Pedido.Split(',')[0]);
            string Tipo = Pedido.Split(',')[1];

            if(Tipo == "Cliente")
            {
                var Result = pedidoRepository.Get(PedidoID);

                //Registra log do usuário
                administradorPedidoRepository.RegistraLogFornecedor(fornecedorModel.CurrentFornecedor.ID, PedidoID, null, null, null, AdministradorPedido.Acao.RecusouEntrega, fornecedorModel.CurrentFornecedor.ID);
            }
            else
            {
                var Result = pedidoEmpresaRepository.Get(PedidoID);

                //Registra log do usuário
                administradorPedidoRepository.RegistraLogFornecedor(fornecedorModel.CurrentFornecedor.ID, null, PedidoID, null, null, AdministradorPedido.Acao.RecusouEntrega, fornecedorModel.CurrentFornecedor.ID);
            }

            return Json("OK");            
        }
        //----------------------------------------------------------------------
        [HttpGet]
        public void BaixarQrCode(int PedidoID, string TipoPedido)
        {
            var Image = new Domain.Entities.Fornecedor().GerarQrCodeEntraga(PedidoID, TipoPedido);

            MemoryStream memoryStream = new MemoryStream();
            Image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] bytesInStream = memoryStream.ToArray();
            memoryStream.Close();

            Response.Clear();
            Response.ContentType = "image/jpeg";
            //Response.AddHeader("content-disposition", "attachment; filename=" + PedidoID + ".jpg");
            Response.BinaryWrite(bytesInStream);
            Response.End();
        }
        #endregion
    }
    //===========================================================================
}
