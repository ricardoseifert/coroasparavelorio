﻿using System.Linq;
using System.Web.Mvc;
using DomainExtensions.Repositories.Interfaces;
using Domain.Entities;
using System.Data.Objects;
using DomainExtensions.Repositories;
using Fornecedor.ViewModels;
using MvcExtensions.Security.Filters;
using Domain.Repositories;
using System;

namespace Fornecedor.Controllers
{
    //===========================================================================
    [AreaAuthorization("Fornecedor", "/login")]
    public class HomeController : Controller
    {
        #region Variaveis
        //----------------------------------------------------------------------
        private IPersistentRepository<Domain.Entities.Fornecedor> fornecedorRepository;
        private AdministradorPedidoRepository administradorPedidoRepository;
        private IPersistentRepository<FornecedorPreco> fornecedorPrecoRepository;
        //private IPersistentRepository<Repasse> repasseRepository;        
        private IPersistentRepository<Pedido> pedidoRepository;
        private IPersistentRepository<NPSRating> npsREspository;
        private FornecedorModel fornecedorModel;
        private FornecedorDashboardRepository fornecedorDashboardRepository;
        //----------------------------------------------------------------------
        #endregion

        #region Construtor
        //----------------------------------------------------------------------
        public HomeController(ObjectContext context)
        {
            fornecedorRepository = new PersistentRepository<Domain.Entities.Fornecedor>(context);
            pedidoRepository = new PersistentRepository<Pedido>(context);
            administradorPedidoRepository = new AdministradorPedidoRepository(context);
            fornecedorPrecoRepository = new PersistentRepository<FornecedorPreco>(context);
            fornecedorModel = new FornecedorModel(context);
            npsREspository = new PersistentRepository<NPSRating>(context);
            fornecedorDashboardRepository = new FornecedorDashboardRepository(context);
        }
        //----------------------------------------------------------------------
        #endregion

        #region Actions
        //----------------------------------------------------------------------
        public ActionResult Index()
        {
            
            Response.Redirect("/Pedidos?datainicial=" + DateTime.Now.ToString("dd/MM/yyy") + "&datafinal=" + DateTime.Now.ToString("dd/MM/yyy"));

            ViewBag.Cidades = fornecedorModel.CurrentFornecedor.FornecedorCidades.ToList();
            return View();
        }
        //----------------------------------------------------------------------
        [HttpPost]
        public void SetarAtividade()
        {
            var Fornecedor = fornecedorRepository.Get(fornecedorModel.CurrentFornecedor.ID);
            Fornecedor.LastActivity = DateTime.Now;
            fornecedorRepository.Save(Fornecedor);
        }
        public ActionResult Saudacao()
        {
            return View(fornecedorModel);
        }
        //----------------------------------------------------------------------
        #endregion

    }
    //===========================================================================
}